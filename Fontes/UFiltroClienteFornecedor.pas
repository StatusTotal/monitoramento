{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroClienteFornecedor.pas
  Descricao:   Tela de Filtro de Clientes e Fornecedores
  Author   :   Cristina
  Date:        18-mar-2016
  Last Update: 08-nov-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroClienteFornecedor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, System.StrUtils, Vcl.Mask, JvExMask, JvToolEdit, JvMaskEdit,
  Vcl.StdCtrls, UDM, frxClass, frxDBSet;

type
  TFFiltroClienteFornecedor = class(TFFiltroSimplesPadrao)
    rgTipoPessoa: TRadioGroup;
    ntbMenu: TNotebook;
    lblNomePessoaFis: TLabel;
    edtNome: TEdit;
    lblCPF: TLabel;
    medCPF: TJvMaskEdit;
    rgSituacaoFis: TRadioGroup;
    chbBloqueadasFis: TCheckBox;
    lblNomeFantasia: TLabel;
    lblRazaoSocial: TLabel;
    edtNomeFantasia: TEdit;
    edtRazaoSocial: TEdit;
    lblCNPJ: TLabel;
    medCNPJ: TJvMaskEdit;
    rgSituacaoJur: TRadioGroup;
    chbBloqueadasJur: TCheckBox;
    qryGridPaiPadraoID: TIntegerField;
    qryGridPaiPadraoTIPO_CLIENTE_FORNECEDOR: TStringField;
    qryGridPaiPadraoNOME_FANTASIA: TStringField;
    qryGridPaiPadraoRAZAO_SOCIAL: TStringField;
    qryGridPaiPadraoTIPO_PESSOA: TStringField;
    qryGridPaiPadraoCPF_CNPJ: TStringField;
    qryGridPaiPadraoINSCRICAO_ESTADUAL: TStringField;
    qryGridPaiPadraoINSCRICAO_MUNICIPAL: TStringField;
    qryGridPaiPadraoUF_LOGR: TStringField;
    qryGridPaiPadraoTELEFONE_1: TStringField;
    qryGridPaiPadraoTELEFONE_2: TStringField;
    qryGridPaiPadraoTELEFONE_3: TStringField;
    qryGridPaiPadraoCELULAR_1: TStringField;
    qryGridPaiPadraoCELULAR_2: TStringField;
    qryGridPaiPadraoCELULAR_3: TStringField;
    qryGridPaiPadraoEMAIL_1: TStringField;
    qryGridPaiPadraoEMAIL_2: TStringField;
    qryGridPaiPadraoSITE: TStringField;
    qryGridPaiPadraoOBSERVACAO: TStringField;
    qryGridPaiPadraoNOME_REPRESENTANTE: TStringField;
    qryGridPaiPadraoSALDO_CREDOR: TBCDField;
    qryGridPaiPadraoSALDO_DEVEDOR: TBCDField;
    qryGridPaiPadraoFLG_ATIVO: TStringField;
    qryGridPaiPadraoDATA_DESATIVACAO: TDateField;
    qryGridPaiPadraoFLG_BLOQUEADO: TStringField;
    qryGridPaiPadraoDATA_BLOQUEIO: TDateField;
    rgSituacaoGeral: TRadioGroup;
    chbBloqueadasGeral: TCheckBox;
    procedure rgTipoPessoaExit(Sender: TObject);
    procedure rgSituacaoFisExit(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure chbBloqueadasFisKeyPress(Sender: TObject; var Key: Char);
    procedure chbBloqueadasJurKeyPress(Sender: TObject; var Key: Char);
    procedure rgSituacaoJurExit(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure dbgGridPaiPadraoDrawDataCell(Sender: TObject; const Rect: TRect;
      Field: TField; State: TGridDrawState);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure rgTipoPessoaClick(Sender: TObject);
    procedure edtNomeKeyPress(Sender: TObject; var Key: Char);
    procedure medCPFKeyPress(Sender: TObject; var Key: Char);
    procedure edtNomeFantasiaKeyPress(Sender: TObject; var Key: Char);
    procedure edtRazaoSocialKeyPress(Sender: TObject; var Key: Char);
    procedure medCNPJKeyPress(Sender: TObject; var Key: Char);
    procedure chbBloqueadasGeralKeyPress(Sender: TObject; var Key: Char);
    procedure rgSituacaoGeralExit(Sender: TObject);
    procedure chbBloqueadasGeralClick(Sender: TObject);
    procedure chbBloqueadasFisClick(Sender: TObject);
    procedure chbBloqueadasJurClick(Sender: TObject);
    procedure rgSituacaoJurClick(Sender: TObject);
    procedure rgSituacaoFisClick(Sender: TObject);
    procedure rgSituacaoGeralClick(Sender: TObject);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }

    procedure SelecionarMenu;
  public
    { Public declarations }
  end;

var
  FFiltroClienteFornecedor: TFFiltroClienteFornecedor;

  TpCliFor: TTipoClienteFornecedor;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UGDM, UCadastroClienteFornecedor,
  UVariaveisGlobais;

{ TFFiltroClienteFornecedor }

procedure TFFiltroClienteFornecedor.btnEditarClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao   := E;
  vgIdConsulta := qryGridPaiPadrao.FieldByName('ID').AsInteger;

  try
    Application.CreateForm(TFCadastroClienteFornecedor, FCadastroClienteFornecedor);

    FCadastroClienteFornecedor.TipoCliFor := TpCliFor;

    FCadastroClienteFornecedor.ShowModal;
  finally
    FCadastroClienteFornecedor.Free;
  end;

  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroClienteFornecedor.btnExcluirClick(Sender: TObject);
var
  qryExclusao: TFDQuery;
  sTipo: String;
begin
  inherited;

  sTipo := IfThen((TpCliFor = CLIE), 'Cliente', 'Fornecedor');

  if lPodeExcluir then
  begin
    qryExclusao := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      with qryExclusao, SQL do
      begin
        Close;
        Clear;
        Text := 'DELETE FROM CLIENTE_FORNECEDOR WHERE ID_CLIENTE_FORNECEDOR = :ID_CLIENTE_FORNECEDOR';
        Params.ParamByName('ID_CLIENTE_FORNECEDOR').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      GravarLog;
      Application.MessageBox(PChar(Trim(sTipo) + ' exclu�do com sucesso!'), 'Aviso', MB_OK)
    except
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Application.MessageBox(PChar('Erro na exclus�o do ' + Trim(sTipo) + '.'), 'Erro', MB_OK + MB_ICONERROR)
    end;

    FreeAndNil(qryExclusao);

    btnFiltrar.Click;
  end;
end;

procedure TFFiltroClienteFornecedor.btnFiltrarClick(Sender: TObject);
var
  sCPF_CNPJ: String;
begin
  sCPF_CNPJ := '';

  with qryGridPaiPadrao, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_CLIENTE_FORNECEDOR AS ID, ' +
            '       TIPO_CLIENTE_FORNECEDOR, ' +
            '       NOME_FANTASIA, ' +
            '       RAZAO_SOCIAL, ' +
            '       TIPO_PESSOA, ' +
            '       CPF_CNPJ, ' +
            '       INSCRICAO_ESTADUAL, ' +
            '       INSCRICAO_MUNICIPAL, ' +
            '       UF_LOGR, ' +
            '       TELEFONE_1, ' +
            '       TELEFONE_2, ' +
            '       TELEFONE_3, ' +
            '       CELULAR_1, ' +
            '       CELULAR_2, ' +
            '       CELULAR_3, ' +
            '       EMAIL_1, ' +
            '       EMAIL_2, ' +
            '       SITE, ' +
            '       OBSERVACAO, ' +
            '       NOME_REPRESENTANTE, ' +
            '       SALDO_CREDOR, ' +
            '       SALDO_DEVEDOR, ' +
            '       FLG_ATIVO, ' +
            '       DATA_DESATIVACAO, ' +
            '       FLG_BLOQUEADO, ' +
            '       DATA_BLOQUEIO ' +
            '  FROM CLIENTE_FORNECEDOR ' +
            ' WHERE TIPO_CLIENTE_FORNECEDOR = :TIPO_CLIENTE_FORNECEDOR ';

    Params.ParamByName('TIPO_CLIENTE_FORNECEDOR').Value := IfThen((TpCliFor = CLIE), 'C', 'F');

    if rgTipoPessoa.ItemIndex = 0 then  //Todas
    begin
      if rgSituacaoGeral.ItemIndex = 1 then
        Add(' AND FLG_ATIVO = ' + QuotedStr('S'))
      else if rgSituacaoGeral.ItemIndex = 2 then
        Add(' AND FLG_ATIVO = ' + QuotedStr('N'));

      if chbBloqueadasGeral.Checked then
        Add(' AND FLG_BLOQUEADO = ' + QuotedStr('S'));
    end
    else if rgTipoPessoa.ItemIndex = 1 then  //Fisica
    begin
      Add(' AND TIPO_PESSOA = ' + QuotedStr('F'));

      if Trim(edtNome.Text) <> '' then
        Add(' AND UPPER(NOME_FANTASIA) LIKE ' + QuotedStr('%' + Trim(UpperCase(edtNome.Text)) + '%'));

      sCPF_CNPJ := StringReplace(StringReplace(medCPF.Text,
                                               '.',
                                               '',
                                               [rfReplaceAll, rfIgnoreCase]),
                                 '-',
                                 '',
                                 [rfReplaceAll, rfIgnoreCase]);

      if Trim(sCPF_CNPJ) <> '' then
        Add(' AND CPF_CNPJ = ' + QuotedStr(Trim(medCPF.Text)));

      if rgSituacaoFis.ItemIndex = 1 then
        Add(' AND FLG_ATIVO = ' + QuotedStr('S'))
      else if rgSituacaoFis.ItemIndex = 2 then
        Add(' AND FLG_ATIVO = ' + QuotedStr('N'));

      if chbBloqueadasFis.Checked then
        Add(' AND FLG_BLOQUEADO = ' + QuotedStr('S'));
    end
    else if rgTipoPessoa.ItemIndex = 2 then  //Juridica
    begin
      Add(' AND TIPO_PESSOA = ' + QuotedStr('J'));

      if Trim(edtNomeFantasia.Text) <> '' then
        Add(' AND UPPER(NOME_FANTASIA) LIKE ' + QuotedStr('%' + Trim(UpperCase(edtNomeFantasia.Text)) + '%'));

      if Trim(edtRazaoSocial.Text) <> '' then
        Add(' AND UPPER(RAZAO_SOCIAL) LIKE ' + QuotedStr('%' + Trim(UpperCase(edtRazaoSocial.Text)) + '%'));

      sCPF_CNPJ := StringReplace(StringReplace(StringReplace(medCNPJ.Text,
                                                             '.',
                                                             '',
                                                             [rfReplaceAll, rfIgnoreCase]),
                                               '/',
                                               '',
                                               [rfReplaceAll, rfIgnoreCase]),
                                 '-',
                                 '',
                                 [rfReplaceAll, rfIgnoreCase]);

      if Trim(sCPF_CNPJ) <> '' then
        Add(' AND CPF_CNPJ = ' + QuotedStr(Trim(medCNPJ.Text)));

      if rgSituacaoJur.ItemIndex = 1 then
        Add(' AND FLG_ATIVO = ' + QuotedStr('S'))
      else if rgSituacaoJur.ItemIndex = 2 then
        Add(' AND FLG_ATIVO = ' + QuotedStr('N'));

      if chbBloqueadasJur.Checked then
        Add(' AND FLG_BLOQUEADO = ' + QuotedStr('S'));
    end;

    Add('ORDER BY NOME_FANTASIA, RAZAO_SOCIAL');
    Open;
  end;

  inherited;
end;

procedure TFFiltroClienteFornecedor.btnIncluirClick(Sender: TObject);
begin
  inherited;

  vgOperacao   := I;
  vgIdConsulta := 0;

  try
    Application.CreateForm(TFCadastroClienteFornecedor, FCadastroClienteFornecedor);

    FCadastroClienteFornecedor.TipoCliFor := TpCliFor;

    FCadastroClienteFornecedor.ShowModal;
  finally
    FCadastroClienteFornecedor.Free;
  end;

  btnFiltrar.Click;
end;

procedure TFFiltroClienteFornecedor.btnLimparClick(Sender: TObject);
begin
  edtNome.Clear;
  medCPF.Clear;
  rgSituacaoFis.ItemIndex := 0;
  chbBloqueadasFis.Checked := False;

  edtNomeFantasia.Clear;
  edtRazaoSocial.Clear;
  medCNPJ.Clear;
  rgSituacaoJur.ItemIndex := 0;
  chbBloqueadasJur.Checked := False;

  inherited;

  if rgTipoPessoa.CanFocus then
    rgTipoPessoa.SetFocus;
end;

procedure TFFiltroClienteFornecedor.chbBloqueadasFisClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroClienteFornecedor.chbBloqueadasFisKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroClienteFornecedor.chbBloqueadasGeralClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroClienteFornecedor.chbBloqueadasGeralKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroClienteFornecedor.chbBloqueadasJurClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroClienteFornecedor.chbBloqueadasJurKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroClienteFornecedor.dbgGridPaiPadraoDblClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao   := C;
  vgIdConsulta := qryGridPaiPadrao.FieldByName('ID').AsInteger;

  try
    Application.CreateForm(TFCadastroClienteFornecedor, FCadastroClienteFornecedor);

    FCadastroClienteFornecedor.TipoCliFor := TpCliFor;

    FCadastroClienteFornecedor.ShowModal;
  finally
    FCadastroClienteFornecedor.Free;
  end;

  DefinirPosicaoGrid;
end;

procedure TFFiltroClienteFornecedor.dbgGridPaiPadraoDrawDataCell(
  Sender: TObject; const Rect: TRect; Field: TField; State: TGridDrawState);
begin
  inherited;

  if qryGridPaiPadrao.FieldByName('FLG_ATIVO').AsString = 'N' then
  begin
    dbgGridPaiPadrao.Canvas.Font.Color := clGray;
    dbgGridPaiPadrao.Canvas.FillRect(Rect);
    dbgGridPaiPadrao.DefaultDrawDataCell(Rect, Field, State);
  end;

  if qryGridPaiPadrao.FieldByName('FLG_BLOQUEADO').AsString = 'S' then
  begin
    dbgGridPaiPadrao.Canvas.Font.Color := clMaroon;
    dbgGridPaiPadrao.Canvas.FillRect(Rect);
    dbgGridPaiPadrao.DefaultDrawDataCell(Rect, Field, State);
  end;
end;

procedure TFFiltroClienteFornecedor.edtNomeFantasiaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroClienteFornecedor.edtNomeKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroClienteFornecedor.edtRazaoSocialKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroClienteFornecedor.FormShow(Sender: TObject);
var
  sCPF_CNPJ: String;
begin
  sCPF_CNPJ := '';

  rgTipoPessoa.ItemIndex := 0;

  ntbMenu.PageIndex := 0;

  with qryGridPaiPadrao, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_CLIENTE_FORNECEDOR AS ID, ' +
            '       TIPO_CLIENTE_FORNECEDOR, ' +
            '       NOME_FANTASIA, ' +
            '       RAZAO_SOCIAL, ' +
            '       TIPO_PESSOA, ' +
            '       CPF_CNPJ, ' +
            '       INSCRICAO_ESTADUAL, ' +
            '       INSCRICAO_MUNICIPAL, ' +
            '       UF_LOGR, ' +
            '       TELEFONE_1, ' +
            '       TELEFONE_2, ' +
            '       TELEFONE_3, ' +
            '       CELULAR_1, ' +
            '       CELULAR_2, ' +
            '       CELULAR_3, ' +
            '       EMAIL_1, ' +
            '       EMAIL_2, ' +
            '       SITE, ' +
            '       OBSERVACAO, ' +
            '       NOME_REPRESENTANTE, ' +
            '       SALDO_CREDOR, ' +
            '       SALDO_DEVEDOR, ' +
            '       FLG_ATIVO, ' +
            '       DATA_DESATIVACAO, ' +
            '       FLG_BLOQUEADO, ' +
            '       DATA_BLOQUEIO ' +
            '  FROM CLIENTE_FORNECEDOR ' +
            ' WHERE TIPO_CLIENTE_FORNECEDOR = :TIPO_CLIENTE_FORNECEDOR ';

    Params.ParamByName('TIPO_CLIENTE_FORNECEDOR').Value := IfThen((TpCliFor = CLIE), 'C', 'F');

    if rgSituacaoGeral.ItemIndex = 1 then
      Add(' AND FLG_ATIVO = ' + QuotedStr('S'))
    else if rgSituacaoGeral.ItemIndex = 2 then
      Add(' AND FLG_ATIVO = ' + QuotedStr('N'));

    if chbBloqueadasGeral.Checked then
      Add(' AND FLG_BLOQUEADO = ' + QuotedStr('S'));

    Add('ORDER BY NOME_FANTASIA, RAZAO_SOCIAL');
    Open;
  end;

  inherited;

  btnImprimir.Visible := False;

  if edtNome.CanFocus then
    edtNome.SetFocus;
end;

procedure TFFiltroClienteFornecedor.GravarLog;
var
  sObservacao, sTipo: String;
begin
  inherited;

  sObservacao := '';

  sTipo := IfThen((TpCliFor = CLIE), 'Cliente', 'Fornecedor');

  case vgOperacao of
    X:  //EXCLUSAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da exclus�o desse ' + sTipo + ':', '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          qryGridPaiPadrao.FieldByName('ID').AsInteger, 'CLIENTE_FORNECEDOR');
    end;
    P:  //IMPRESSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente impress�o de Lista de ' + sTipo,
                          qryGridPaiPadrao.FieldByName('ID').AsInteger, 'CLIENTE_FORNECEDOR');
    end;
    T:  //EXPORTACAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente exporta��o de Lista de ' + sTipo,
                          qryGridPaiPadrao.FieldByName('ID').AsInteger, 'CLIENTE_FORNECEDOR');
    end;
  end;
end;

procedure TFFiltroClienteFornecedor.medCNPJKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroClienteFornecedor.medCPFKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

function TFFiltroClienteFornecedor.PodeExcluir(var Msg: String): Boolean;
var
  QryVerif: TFDQuery;
begin
  inherited;

  Result := True;

  QryVerif := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryVerif, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT * FROM LANCAMENTO WHERE ID_CLIENTE_FORNECEDOR_FK = :ID_CLIENTE_FORNECEDOR';
    Params.ParamByName('ID_CLIENTE_FORNECEDOR').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
    Open;
  end;

  if QryVerif.RecordCount > 0 then
  begin
    if TpCliFor = CLIE then
      msg := 'N�o � poss�vel excluir o Cliente informado, pois o mesmo j� possui hist�rico no sistema.'
    else
      msg := 'N�o � poss�vel excluir o Fornecedor informado, pois o mesmo j� possui hist�rico no sistema.';
  end;

  Result := (QryVerif.RecordCount = 0) and (qryGridPaiPadrao.RecordCount > 0);

  FreeAndNil(QryVerif);
end;

procedure TFFiltroClienteFornecedor.rgSituacaoFisClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;

  if chbBloqueadasFis.CanFocus then
    chbBloqueadasFis.SetFocus;
end;

procedure TFFiltroClienteFornecedor.rgSituacaoFisExit(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;;

  if chbBloqueadasFis.CanFocus then
    chbBloqueadasFis.SetFocus;
end;

procedure TFFiltroClienteFornecedor.rgSituacaoGeralClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;

  if chbBloqueadasGeral.CanFocus then
    chbBloqueadasGeral.SetFocus;
end;

procedure TFFiltroClienteFornecedor.rgSituacaoGeralExit(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;;

  if chbBloqueadasGeral.CanFocus then
    chbBloqueadasGeral.SetFocus;
end;

procedure TFFiltroClienteFornecedor.rgSituacaoJurClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;

  if chbBloqueadasJur.CanFocus then
    chbBloqueadasJur.SetFocus;
end;

procedure TFFiltroClienteFornecedor.rgSituacaoJurExit(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;

  if chbBloqueadasJur.CanFocus then
    chbBloqueadasJur.SetFocus;
end;

procedure TFFiltroClienteFornecedor.rgTipoPessoaClick(Sender: TObject);
begin
  inherited;

  SelecionarMenu;
end;

procedure TFFiltroClienteFornecedor.rgTipoPessoaExit(Sender: TObject);
begin
  inherited;

  SelecionarMenu;
end;

procedure TFFiltroClienteFornecedor.SelecionarMenu;
begin
  edtNome.Clear;
  medCPF.Clear;
  rgSituacaoFis.ItemIndex := 0;
  chbBloqueadasFis.Checked := False;

  edtNomeFantasia.Clear;
  edtRazaoSocial.Clear;
  medCNPJ.Clear;
  rgSituacaoJur.ItemIndex := 0;
  chbBloqueadasJur.Checked := False;

  if rgTipoPessoa.ItemIndex = 0 then  //Geral
  begin
    ntbMenu.PageIndex := 0;

    btnFiltrar.Click;

    if rgSituacaoGeral.CanFocus then
      rgSituacaoGeral.SetFocus;
  end
  else if rgTipoPessoa.ItemIndex = 1 then  //Pessoa Fisica
  begin
    ntbMenu.PageIndex := 1;

    btnFiltrar.Click;

    if edtNome.CanFocus then
      edtNome.SetFocus;
  end
  else  //Pessoa Juridica
  begin
    ntbMenu.PageIndex := 2;

    btnFiltrar.Click;

    if edtNomeFantasia.CanFocus then
      edtNomeFantasia.SetFocus;
  end;
end;

procedure TFFiltroClienteFornecedor.VerificarPermissoes;
begin
  inherited;

  if TpCliFor = CLIE then
  begin
    btnIncluir.Enabled  := vgPrm_IncCli;
    btnEditar.Enabled   := vgPrm_EdCli and (qryGridPaiPadrao.RecordCount > 0);
    btnExcluir.Enabled  := vgPrm_ExcCli and (qryGridPaiPadrao.RecordCount > 0);
    btnImprimir.Enabled := vgPrm_ImpCli and (qryGridPaiPadrao.RecordCount > 0);
  end;

  if TpCliFor = FORN then
  begin
    btnIncluir.Enabled  := vgPrm_IncForn;
    btnEditar.Enabled   := vgPrm_EdForn and (qryGridPaiPadrao.RecordCount > 0);
    btnExcluir.Enabled  := vgPrm_ExcForn and (qryGridPaiPadrao.RecordCount > 0);
    btnImprimir.Enabled := vgPrm_ImpForn and (qryGridPaiPadrao.RecordCount > 0);
  end;
end;

end.
