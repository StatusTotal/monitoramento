inherited FCadastroOficioFormaEnvio: TFCadastroOficioFormaEnvio
  Caption = 'Cadastro de Forma de Envio do Of'#237'cio'
  ClientHeight = 128
  ClientWidth = 488
  ExplicitWidth = 494
  ExplicitHeight = 157
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 482
    Height = 122
    ExplicitWidth = 482
    ExplicitHeight = 122
    inherited pnlDados: TPanel
      Width = 368
      Height = 116
      ExplicitWidth = 368
      ExplicitHeight = 116
      object lblNome: TLabel [0]
        Left = 5
        Top = 8
        Width = 32
        Height = 14
        Caption = 'Nome'
      end
      inherited pnlMsgErro: TPanel
        Top = 55
        Width = 368
        ExplicitTop = 55
        ExplicitWidth = 368
        inherited lblMensagemErro: TLabel
          Width = 368
        end
        inherited lbMensagemErro: TListBox
          Width = 368
          OnDblClick = lbMensagemErroDblClick
          ExplicitWidth = 368
        end
      end
      object edtNome: TDBEdit
        Left = 5
        Top = 24
        Width = 361
        Height = 22
        CharCase = ecUpperCase
        Color = 16114127
        DataField = 'DESCR_OFICIO_FORMAENVIO'
        DataSource = dsCadastro
        MaxLength = 250
        TabOrder = 1
        OnKeyPress = edtNomeKeyPress
      end
    end
    inherited pnlMenu: TPanel
      Height = 116
      ExplicitHeight = 116
      inherited btnOk: TJvTransparentButton
        Top = 26
        ExplicitTop = 42
      end
      inherited btnCancelar: TJvTransparentButton
        Top = 71
        ExplicitTop = 87
      end
    end
  end
  inherited dsCadastro: TDataSource
    DataSet = cdsOfcFEnv
    Left = 430
    Top = 69
  end
  object qryOfcFEnv: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM OFICIO_FORMAENVIO'
      ' WHERE ID_OFICIO_FORMAENVIO = :ID_OFICIO_FORMAENVIO')
    Left = 244
    Top = 69
    ParamData = <
      item
        Position = 1
        Name = 'ID_OFICIO_FORMAENVIO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspOfcFEnv: TDataSetProvider
    DataSet = qryOfcFEnv
    Left = 306
    Top = 69
  end
  object cdsOfcFEnv: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_OFICIO_FORMAENVIO'
        ParamType = ptInput
      end>
    ProviderName = 'dspOfcFEnv'
    Left = 369
    Top = 69
    object cdsOfcFEnvID_OFICIO_FORMAENVIO: TIntegerField
      FieldName = 'ID_OFICIO_FORMAENVIO'
      Required = True
    end
    object cdsOfcFEnvDESCR_OFICIO_FORMAENVIO: TStringField
      FieldName = 'DESCR_OFICIO_FORMAENVIO'
      Size = 150
    end
  end
end
