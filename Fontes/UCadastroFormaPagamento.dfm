inherited FCadastroFormaPagamento: TFCadastroFormaPagamento
  Caption = 'Cadastro de Forma de Pagamento'
  ClientHeight = 208
  ClientWidth = 482
  ExplicitWidth = 488
  ExplicitHeight = 237
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 476
    Height = 202
    ExplicitWidth = 476
    ExplicitHeight = 202
    inherited pnlDados: TPanel
      Width = 362
      Height = 196
      ExplicitWidth = 362
      ExplicitHeight = 196
      object lblDescricao: TLabel [0]
        Left = 0
        Top = 5
        Width = 51
        Height = 14
        Caption = 'Descri'#231#227'o'
      end
      object lblMaxParcelas: TLabel [1]
        Left = 86
        Top = 49
        Width = 72
        Height = 14
        Caption = 'M'#225'x. Parcelas'
      end
      object lblMinParcelas: TLabel [2]
        Left = 0
        Top = 49
        Width = 69
        Height = 14
        Caption = 'M'#237'n. Parcelas'
      end
      object lblPrazo: TLabel [3]
        Left = 172
        Top = 49
        Width = 29
        Height = 14
        Caption = 'Prazo'
      end
      object lblPercTarifa: TLabel [4]
        Left = 258
        Top = 49
        Width = 46
        Height = 14
        Caption = '% Tarifa'
      end
      object lblFlgAtiva: TLabel [5]
        Left = 111
        Top = 93
        Width = 33
        Height = 14
        Caption = 'Ativa?'
      end
      object lblTipoFormaPagamento: TLabel [6]
        Left = 0
        Top = 93
        Width = 24
        Height = 14
        Caption = 'Tipo'
      end
      inherited pnlMsgErro: TPanel
        Top = 135
        Width = 362
        TabOrder = 7
        ExplicitTop = 135
        ExplicitWidth = 362
        inherited lblMensagemErro: TLabel
          Width = 362
        end
        inherited lbMensagemErro: TListBox
          Width = 362
          OnDblClick = lbMensagemErroDblClick
          ExplicitWidth = 362
        end
      end
      object edtDescricao: TDBEdit
        Left = 0
        Top = 21
        Width = 361
        Height = 22
        CharCase = ecUpperCase
        Color = 16114127
        DataField = 'DESCR_FORMAPAGAMENTO'
        DataSource = dsCadastro
        TabOrder = 0
        OnKeyPress = FormKeyPress
      end
      object edtMaxParcelas: TDBEdit
        Left = 86
        Top = 65
        Width = 80
        Height = 22
        Color = 16114127
        DataField = 'MAX_PARCELAS'
        DataSource = dsCadastro
        TabOrder = 2
        OnKeyPress = FormKeyPress
      end
      object edtMinParcelas: TDBEdit
        Left = 0
        Top = 65
        Width = 80
        Height = 22
        Color = 16114127
        DataField = 'MIN_PARCELAS'
        DataSource = dsCadastro
        TabOrder = 1
        OnKeyPress = FormKeyPress
      end
      object edtPrazo: TDBEdit
        Left = 172
        Top = 65
        Width = 80
        Height = 22
        Color = 16114127
        DataField = 'PRAZO'
        DataSource = dsCadastro
        TabOrder = 3
        OnKeyPress = FormKeyPress
      end
      object cedPercTarifa: TJvDBCalcEdit
        Left = 258
        Top = 65
        Width = 103
        Height = 22
        Color = 16114127
        TabOrder = 4
        DecimalPlacesAlwaysShown = False
        OnKeyPress = FormKeyPress
        DataField = 'PERC_TARIFA'
        DataSource = dsCadastro
      end
      object cbFlgAtiva: TDBComboBox
        Left = 111
        Top = 109
        Width = 43
        Height = 22
        Style = csOwnerDrawFixed
        Color = 16114127
        DataField = 'FLG_ATIVA'
        DataSource = dsCadastro
        Items.Strings = (
          'S'
          'N')
        TabOrder = 6
        OnKeyPress = cbFlgAtivaKeyPress
      end
      object lcbTipoFormaPagamento: TJvDBComboBox
        Left = 0
        Top = 109
        Width = 105
        Height = 22
        Style = csOwnerDrawFixed
        Color = 16114127
        DataField = 'TIPO'
        DataSource = dsCadastro
        Items.Strings = (
          'Cheque'
          'Cart'#227'o'
          'Caixa'
          'Dinheiro'
          'Dep'#243'sito'
          'Faturado'
          'Promiss'#243'ria')
        TabOrder = 5
        Values.Strings = (
          'CH'
          'CT'
          'CX'
          'DH'
          'DP'
          'FT'
          'PM')
        ListSettings.OutfilteredValueFont.Charset = DEFAULT_CHARSET
        ListSettings.OutfilteredValueFont.Color = clRed
        ListSettings.OutfilteredValueFont.Height = -11
        ListSettings.OutfilteredValueFont.Name = 'Tahoma'
        ListSettings.OutfilteredValueFont.Style = []
        OnKeyPress = FormKeyPress
      end
    end
    inherited pnlMenu: TPanel
      Height = 196
      ExplicitHeight = 196
      inherited btnDadosPrincipais: TJvTransparentButton
        Visible = False
      end
      inherited btnOk: TJvTransparentButton
        Top = 106
        ExplicitTop = 91
      end
      inherited btnCancelar: TJvTransparentButton
        Top = 151
        ExplicitTop = 136
      end
    end
  end
  inherited dsCadastro: TDataSource
    DataSet = cdsFormaPagamento
    Left = 436
    Top = 150
  end
  object qryFormaPagamento: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM FORMAPAGAMENTO'
      ' WHERE ID_FORMAPAGAMENTO = :ID_FORMAPAGAMENTO')
    Left = 144
    Top = 150
    ParamData = <
      item
        Position = 1
        Name = 'ID_FORMAPAGAMENTO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspFormaPagamento: TDataSetProvider
    DataSet = qryFormaPagamento
    Left = 248
    Top = 150
  end
  object cdsFormaPagamento: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_FORMAPAGAMENTO'
        ParamType = ptInput
      end>
    ProviderName = 'dspFormaPagamento'
    Left = 352
    Top = 150
  end
end
