inherited FAlteracaoNaturezaLancamento: TFAlteracaoNaturezaLancamento
  Caption = 'Alterar a Natureza do Lan'#231'amento'
  ClientHeight = 283
  ClientWidth = 715
  ExplicitWidth = 721
  ExplicitHeight = 312
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 709
    Height = 277
    inherited pnlDados: TPanel
      Width = 595
      Height = 271
      object lblCodigo: TLabel [0]
        Left = 111
        Top = 8
        Width = 37
        Height = 14
        Caption = 'C'#243'digo'
      end
      object Label3: TLabel [1]
        Left = 181
        Top = 28
        Width = 13
        Height = 14
        Caption = ' / '
      end
      object lblAno: TLabel [2]
        Left = 193
        Top = 8
        Width = 22
        Height = 14
        Caption = 'Ano'
      end
      object lblDataLancamento: TLabel [3]
        Left = 5
        Top = 8
        Width = 25
        Height = 14
        Caption = 'Data'
      end
      object lblNumeroRecibo: TLabel [4]
        Left = 454
        Top = 21
        Width = 139
        Height = 23
        Alignment = taRightJustify
        Caption = 'N'#186' do Recibo: '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblValorTotalPrev: TLabel [5]
        Left = 5
        Top = 52
        Width = 82
        Height = 14
        Caption = 'Vlr. Total Prev.'
      end
      object lblJuros: TLabel [6]
        Left = 101
        Top = 52
        Width = 28
        Height = 14
        Caption = 'Juros'
      end
      object lblDesconto: TLabel [7]
        Left = 187
        Top = 52
        Width = 52
        Height = 14
        Caption = 'Desconto'
      end
      object lblValorTotalPago: TLabel [8]
        Left = 282
        Top = 52
        Width = 81
        Height = 14
        Caption = 'Vlr. Total Pago'
      end
      object lblNumParcelas: TLabel [9]
        Left = 369
        Top = 52
        Width = 61
        Height = 14
        Caption = 'Qtd. Parcs.'
      end
      inherited pnlMsgErro: TPanel
        Top = 210
        Width = 595
        TabOrder = 9
        inherited lblMensagemErro: TLabel
          Width = 595
        end
        inherited lbMensagemErro: TListBox
          Width = 595
          OnDblClick = lbMensagemErroDblClick
        end
      end
      object edtCodigo: TDBEdit
        Left = 111
        Top = 24
        Width = 71
        Height = 22
        Color = clSilver
        DataField = 'COD_LANCAMENTO'
        DataSource = dsCadastro
        ReadOnly = True
        TabOrder = 1
        OnKeyPress = FormKeyPress
      end
      object edtAno: TDBEdit
        Left = 193
        Top = 24
        Width = 39
        Height = 22
        Color = clSilver
        DataField = 'ANO_LANCAMENTO'
        DataSource = dsCadastro
        ReadOnly = True
        TabOrder = 2
        OnKeyPress = FormKeyPress
      end
      object dteDataLancamento: TJvDBDateEdit
        Left = 5
        Top = 24
        Width = 100
        Height = 22
        DataField = 'DATA_LANCAMENTO'
        DataSource = dsCadastro
        ReadOnly = True
        Color = clSilver
        ShowNullDate = False
        TabOrder = 0
        OnKeyPress = FormKeyPress
      end
      object cedValorTotalPrev: TJvDBCalcEdit
        Left = 5
        Top = 68
        Width = 90
        Height = 22
        Color = clSilver
        DisplayFormat = ',0.00'
        ReadOnly = True
        TabOrder = 3
        DecimalPlacesAlwaysShown = False
        OnKeyPress = FormKeyPress
        DataField = 'VR_TOTAL_PREV'
        DataSource = dsCadastro
        EmptyIsNull = False
      end
      object cedValorTotalJuros: TJvDBCalcEdit
        Left = 101
        Top = 68
        Width = 80
        Height = 22
        Color = clSilver
        DisplayFormat = ',0.00'
        ReadOnly = True
        TabOrder = 4
        DecimalPlacesAlwaysShown = False
        OnKeyPress = FormKeyPress
        DataField = 'VR_TOTAL_JUROS'
        DataSource = dsCadastro
        EmptyIsNull = False
      end
      object cedValorTotalDesconto: TJvDBCalcEdit
        Left = 187
        Top = 68
        Width = 80
        Height = 22
        Color = clSilver
        DisplayFormat = ',0.00'
        ReadOnly = True
        TabOrder = 5
        DecimalPlacesAlwaysShown = False
        OnKeyPress = FormKeyPress
        DataField = 'VR_TOTAL_DESCONTO'
        DataSource = dsCadastro
        EmptyIsNull = False
      end
      object cedValorTotalPago: TJvDBCalcEdit
        Left = 273
        Top = 68
        Width = 90
        Height = 22
        Color = clSilver
        DisplayFormat = ',0.00'
        ReadOnly = True
        TabOrder = 6
        DecimalPlacesAlwaysShown = False
        OnKeyPress = FormKeyPress
        DataField = 'VR_TOTAL_PAGO'
        DataSource = dsCadastro
        EmptyIsNull = False
      end
      object edtQtdParcelasLanc: TDBEdit
        Left = 369
        Top = 68
        Width = 61
        Height = 22
        Color = clSilver
        DataField = 'QTD_PARCELAS'
        DataSource = dsCadastro
        ReadOnly = True
        TabOrder = 7
        OnKeyPress = FormKeyPress
      end
      object gbNatureza: TGroupBox
        Left = 5
        Top = 96
        Width = 588
        Height = 108
        Caption = 'Natureza'
        TabOrder = 8
        object btnIncluirNatureza: TJvTransparentButton
          Left = 315
          Top = 79
          Width = 22
          Height = 22
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          FrameStyle = fsIndent
          ParentFont = False
          Transparent = False
          OnClick = btnIncluirNaturezaClick
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            1800000000000006000000000000000000000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5F5F5DADADACCCCCCCC
            CCCCCCCCCCCCCCCCDADADAF5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFDDDDDDA3C0B3369D6E008C4B008B4A008B4A008C4B369D6EA3C0B3E1E1
            E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDDB2B2B26A6A6A47474746
            46464646464747476A6A6AB2B2B2E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            E1E1E144A27700905001A16900AA7600AB7700AB7700AA7601A16900905055A8
            82E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFE1E1E17474744A4A4A5656565C5C5C5C
            5C5C5C5C5C5C5C5C5656564A4A4A7F7F7FE1E1E1FFFFFFFFFFFFFFFFFFF5F5F5
            55A88200915202AC7700C38C00D69918DEA818DEA800D69900C38C01AB760092
            5355A882F5F5F5FFFFFFFFFFFFF5F5F57F7F7F4A4A4A5D5D5D6A6A6A74747484
            84848484847474746A6A6A5C5C5C4B4B4B7F7F7FF5F5F5FFFFFFFFFFFFAECABE
            0090510FB48302D29900D69B00D193FFFFFFFFFFFF00D19300D69B00D19801AB
            76009050AECBBEFFFFFFFFFFFFBCBCBC4A4A4A686868737373757575717171FF
            FFFFFFFFFF7171717575757272725C5C5C4A4A4ABDBDBDFFFFFFFFFFFF369D6C
            16AB7811C99700D49A00D29700CD8EFFFFFFFFFFFF00CD8E00D29700D59B00C1
            8C01A169369E6EFFFFFFFFFFFF6A6A6A6565657575757474747272726E6E6EFF
            FFFFFFFFFF6E6E6E7272727474746969695656566B6B6BFFFFFFFFFFFF008A48
            38C49C00D19800CD9200CB8E00C787FFFFFFFFFFFF00C78700CB8E00CE9300D0
            9A00AB76008C4BFFFFFFFFFFFF4646468484847272726F6F6F6E6E6E6B6B6BFF
            FFFFFFFFFF6B6B6B6E6E6E7070707272725C5C5C474747FFFFFFFFFFFF008946
            51D2AF12D4A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CF
            9700AD78008B4AFFFFFFFFFFFF4545459797977D7D7DFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFF7171715D5D5D464646FFFFFFFFFFFF008845
            66DDBE10D0A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CD
            9700AD78008B4AFFFFFFFFFFFF444444A7A7A77A7A7AFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFF7070705D5D5D464646FFFFFFFFFFFF008846
            76E0C500CA9800C59000C48E00C187FFFFFFFFFFFF00C18700C48E00C79300CB
            9900AB76008C4BFFFFFFFFFFFF444444B0B0B06F6F6F6C6C6C6B6B6B686868FF
            FFFFFFFFFF6868686B6B6B6D6D6D7070705C5C5C474747FFFFFFFFFFFF41A675
            59C9A449DEBC00C79400C79400C38EFFFFFFFFFFFF00C38E00C89600CB9A06C1
            9000A16840A878FFFFFFFFFFFF7474749595959C9C9C6D6D6D6D6D6D6A6A6AFF
            FFFFFFFFFF6A6A6A6E6E6E7070706C6C6C555555757575FFFFFFFFFFFFCCE8DB
            0A9458ADF8E918D0A700C49400C290FFFFFFFFFFFF00C39100C79905C89B18B7
            87009050CCE8DBFFFFFFFFFFFFDADADA515151D7D7D77E7E7E6C6C6C6A6A6AFF
            FFFFFFFFFF6B6B6B6E6E6E7171716E6E6E4A4A4ADADADAFFFFFFFFFFFFFFFFFF
            55B185199C63BCFFF75DE4C900C39700BF9000C09100C49822CAA231C2970393
            556ABC96FFFFFFFFFFFFFFFFFFFFFFFF8383835C5C5CE3E3E3A9A9A96C6C6C69
            69696A6A6A6D6D6D7F7F7F7F7F7F4D4D4D949494FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFF6ABB940E965974D5B69FF3E092EFDA79E5CA5DD6B52EB58603915255B2
            88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF939393535353A8A8A8CECECEC6
            C6C6B4B4B49F9F9F7676764C4C4C848484FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFCCE8DB44A87700874400874300874400894644AA7ACCE8DBFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDADADA76767644444443
            4343444444454545787878DADADAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          NumGlyphs = 2
        end
        object lblNaturezaAtual: TLabel
          Left = 5
          Top = 18
          Width = 80
          Height = 14
          Caption = 'Natureza Atual'
        end
        object lblNaturezaNova: TLabel
          Left = 5
          Top = 63
          Width = 79
          Height = 14
          Caption = 'Natureza Nova'
        end
        object lcbNaturezaNova: TDBLookupComboBox
          Left = 5
          Top = 79
          Width = 308
          Height = 22
          Color = 16114127
          DataField = 'ID_NATUREZA_FK'
          DataSource = dsCadastro
          KeyField = 'ID_NATUREZA'
          ListField = 'DESCR_NATUREZA'
          ListSource = dsNatureza
          TabOrder = 2
          OnClick = lcbNaturezaNovaClick
          OnExit = lcbNaturezaNovaExit
          OnKeyPress = FormKeyPress
        end
        object edtNaturezaAtual: TEdit
          Left = 5
          Top = 35
          Width = 332
          Height = 22
          Color = clSilver
          ReadOnly = True
          TabOrder = 0
          OnKeyPress = FormKeyPress
        end
        object gbClassificacaoAtual: TGroupBox
          Left = 343
          Top = 18
          Width = 240
          Height = 39
          Caption = 'Classifica'#231#227'o'
          TabOrder = 1
          object chbImpostoRendaA: TCheckBox
            Left = 8
            Top = 20
            Width = 30
            Height = 17
            Caption = 'IR'
            Enabled = False
            TabOrder = 0
            OnKeyPress = FormKeyPress
          end
          object chbAuxiliarA: TCheckBox
            Left = 44
            Top = 20
            Width = 84
            Height = 17
            Caption = 'Livro Auxiliar'
            Enabled = False
            TabOrder = 1
            OnKeyPress = FormKeyPress
          end
          object chbPessoalA: TCheckBox
            Left = 134
            Top = 20
            Width = 97
            Height = 17
            Caption = 'Contas Pessoais'
            Enabled = False
            TabOrder = 2
            OnKeyPress = FormKeyPress
          end
        end
        object gbClassificacaoNova: TGroupBox
          Left = 343
          Top = 62
          Width = 240
          Height = 39
          Caption = 'Classifica'#231#227'o'
          TabOrder = 3
          object chbImpostoRendaN: TDBCheckBox
            Left = 8
            Top = 20
            Width = 30
            Height = 17
            Caption = 'IR'
            DataField = 'FLG_IMPOSTORENDA'
            DataSource = dsCadastro
            ReadOnly = True
            TabOrder = 0
            ValueChecked = 'S'
            ValueUnchecked = 'N'
            OnKeyPress = FormKeyPress
          end
          object chbAuxiliarN: TDBCheckBox
            Left = 44
            Top = 20
            Width = 84
            Height = 17
            Caption = 'Livro Auxiliar'
            DataField = 'FLG_AUXILIAR'
            DataSource = dsCadastro
            ReadOnly = True
            TabOrder = 1
            ValueChecked = 'S'
            ValueUnchecked = 'N'
            OnKeyPress = FormKeyPress
          end
          object chbPessoalN: TDBCheckBox
            Left = 134
            Top = 20
            Width = 104
            Height = 17
            Caption = 'Contas Pessoais'
            DataField = 'FLG_PESSOAL'
            DataSource = dsCadastro
            ReadOnly = True
            TabOrder = 2
            ValueChecked = 'S'
            ValueUnchecked = 'N'
            OnKeyPress = chbPessoalNKeyPress
          end
        end
      end
    end
    inherited pnlMenu: TPanel
      Height = 271
      ExplicitHeight = 559
      inherited btnOk: TJvTransparentButton
        Top = 181
        OnClick = btnOkClick
      end
      inherited btnCancelar: TJvTransparentButton
        Top = 226
      end
    end
  end
  inherited dsCadastro: TDataSource
    DataSet = cdsLanc
    Left = 654
    Top = 222
  end
  object qryLanc: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT ID_NATUREZA_FK,'
      '       COD_LANCAMENTO,'
      '       ANO_LANCAMENTO,'
      '       DATA_LANCAMENTO,'
      '       TIPO_LANCAMENTO,'
      '       FLG_IMPOSTORENDA,'
      '       FLG_AUXILIAR,'
      '       FLG_PESSOAL,'
      '       QTD_PARCELAS,'
      '       VR_TOTAL_PREV,'
      '       VR_TOTAL_JUROS,'
      '       VR_TOTAL_DESCONTO,'
      '       VR_TOTAL_PAGO,'
      '       NUM_RECIBO'
      '  FROM LANCAMENTO'
      ' WHERE COD_LANCAMENTO = :COD_LANCAMENTO'
      '   AND ANO_LANCAMENTO = :ANO_LANCAMENTO')
    Left = 502
    Top = 222
    ParamData = <
      item
        Position = 1
        Name = 'COD_LANCAMENTO'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'ANO_LANCAMENTO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspLanc: TDataSetProvider
    DataSet = qryLanc
    Left = 550
    Top = 222
  end
  object cdsLanc: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'COD_LANCAMENTO'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ANO_LANCAMENTO'
        ParamType = ptInput
      end>
    ProviderName = 'dspLanc'
    OnCalcFields = cdsLancCalcFields
    Left = 598
    Top = 222
    object cdsLancID_NATUREZA_FK: TIntegerField
      FieldName = 'ID_NATUREZA_FK'
    end
    object cdsLancCOD_LANCAMENTO: TIntegerField
      FieldName = 'COD_LANCAMENTO'
      Required = True
    end
    object cdsLancANO_LANCAMENTO: TIntegerField
      FieldName = 'ANO_LANCAMENTO'
      Required = True
    end
    object cdsLancDATA_LANCAMENTO: TDateField
      FieldName = 'DATA_LANCAMENTO'
    end
    object cdsLancTIPO_LANCAMENTO: TStringField
      FieldName = 'TIPO_LANCAMENTO'
      FixedChar = True
      Size = 1
    end
    object cdsLancFLG_IMPOSTORENDA: TStringField
      FieldName = 'FLG_IMPOSTORENDA'
      FixedChar = True
      Size = 1
    end
    object cdsLancFLG_AUXILIAR: TStringField
      FieldName = 'FLG_AUXILIAR'
      FixedChar = True
      Size = 1
    end
    object cdsLancFLG_PESSOAL: TStringField
      FieldName = 'FLG_PESSOAL'
      FixedChar = True
      Size = 1
    end
    object cdsLancQTD_PARCELAS: TIntegerField
      FieldName = 'QTD_PARCELAS'
    end
    object cdsLancVR_TOTAL_PREV: TBCDField
      FieldName = 'VR_TOTAL_PREV'
      Precision = 18
      Size = 2
    end
    object cdsLancVR_TOTAL_JUROS: TBCDField
      FieldName = 'VR_TOTAL_JUROS'
      Precision = 18
      Size = 2
    end
    object cdsLancVR_TOTAL_DESCONTO: TBCDField
      FieldName = 'VR_TOTAL_DESCONTO'
      Precision = 18
      Size = 2
    end
    object cdsLancVR_TOTAL_PAGO: TBCDField
      FieldName = 'VR_TOTAL_PAGO'
      Precision = 18
      Size = 2
    end
    object cdsLancNUM_RECIBO: TIntegerField
      FieldName = 'NUM_RECIBO'
    end
    object cdsLancDESCR_NATUREZA: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCR_NATUREZA'
      Size = 250
    end
  end
  object qryNatureza: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM NATUREZA'
      ' WHERE TIPO_NATUREZA = :TIPO_NATUREZA'
      'ORDER BY DESCR_NATUREZA')
    Left = 200
    Top = 222
    ParamData = <
      item
        Name = 'TIPO_NATUREZA'
        DataType = ftFixedChar
        ParamType = ptInput
        Size = 1
      end>
    object qryNaturezaID_NATUREZA: TIntegerField
      FieldName = 'ID_NATUREZA'
      Origin = 'ID_NATUREZA'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryNaturezaDESCR_NATUREZA: TStringField
      FieldName = 'DESCR_NATUREZA'
      Origin = 'DESCR_NATUREZA'
      Size = 250
    end
    object qryNaturezaTIPO_NATUREZA: TStringField
      FieldName = 'TIPO_NATUREZA'
      Origin = 'TIPO_NATUREZA'
      FixedChar = True
      Size = 1
    end
    object qryNaturezaFLG_EDITAVEL: TStringField
      FieldName = 'FLG_EDITAVEL'
      Origin = 'FLG_EDITAVEL'
      FixedChar = True
      Size = 1
    end
    object qryNaturezaFLG_IMPOSTORENDA: TStringField
      FieldName = 'FLG_IMPOSTORENDA'
      Origin = 'FLG_IMPOSTORENDA'
      FixedChar = True
      Size = 1
    end
    object qryNaturezaFLG_AUXILIAR: TStringField
      FieldName = 'FLG_AUXILIAR'
      Origin = 'FLG_AUXILIAR'
      FixedChar = True
      Size = 1
    end
    object qryNaturezaFLG_PESSOAL: TStringField
      FieldName = 'FLG_PESSOAL'
      Origin = 'FLG_PESSOAL'
      FixedChar = True
      Size = 1
    end
  end
  object dsNatureza: TDataSource
    DataSet = qryNatureza
    Left = 264
    Top = 222
  end
end
