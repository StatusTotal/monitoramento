{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UPrincipal.pas
  Descricao:   Tela Principal do Sistema
  Author   :   Cristina
  Date:        11-jan-2016
  Last Update: 20-mar-2019  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, JvExControls, JvButton,
  JvTransparentButton, Vcl.StdCtrls, JvBaseDlg, JvProgressDialog,
  System.Actions, Vcl.ActnList, Vcl.ActnMan, dxGDIPlusClasses, System.DateUtils,
  UFRMAtualizacaoSistema, UFRMDespesasPendentes, Vcl.Menus, JvMenus,
  Vcl.PlatformDefaultStyleActnCtrls, FireDAC.Stan.Param, JvAnimatedImage,
  Winapi.ShellApi, System.StrUtils, IdExplicitTLSClientServerBase;

type
  TFPrincipal = class(TForm)
    pnlMenu: TPanel;
    pnlMenuAbas: TPanel;
    pnlMenuBotoes: TPanel;
    btnMenuAbaAtalhos: TJvTransparentButton;
    ntbMenuBotoes: TNotebook;
    JvTransparentButton2: TJvTransparentButton;
    btnMenuAbaConfiguracoes: TJvTransparentButton;
    btnMenuAbaSuporte: TJvTransparentButton;
    btnMenuAbaOficios: TJvTransparentButton;
    btnMenuAbaFinanceiro: TJvTransparentButton;
    JvTransparentButton1: TJvTransparentButton;
    btnAtalhoReceitas: TJvTransparentButton;
    btnAtalhoDespesas: TJvTransparentButton;
    JvTransparentButton6: TJvTransparentButton;
    JvTransparentButton7: TJvTransparentButton;
    pnlSistema: TPanel;
    lblVersaoSistema: TLabel;
    lblUsuarioLogado: TLabel;
    pnVersao: TPanel;
    Panel3: TPanel;
    pnTotalSistemas: TPanel;
    lbCallCenter: TLabel;
    lbMsn: TLabel;
    Label4: TLabel;
    acmMenu: TActionManager;
    actSupCriptografia: TAction;
    actConfigFiltroLog: TAction;
    actConfigSistema: TAction;
    actCadColaboradores: TAction;
    actCadClientes: TAction;
    actCadFornecedores: TAction;
    actCadProdutos: TAction;
    actCadServicos: TAction;
    actCadReceita: TAction;
    actCadDespesa: TAction;
    actConfigCadUsuarios: TAction;
    actConfigCadPermissoes: TAction;
    actFiltroLancamento: TAction;
    actCadCategoriaReceitas: TAction;
    actCadSubcatReceitas: TAction;
    actCadCategoriaDespesas: TAction;
    actCadSubcatDespesas: TAction;
    actRelFechamentoCaixaFP: TAction;
    actRelRecibosSelos: TAction;
    actRelFaturamentoMensal: TAction;
    actRelInadimplencias: TAction;
    actRelExtratoContas: TAction;
    actSupBackup: TAction;
    actSupArquivoIni: TAction;
    actSupSQL: TAction;
    actRelRecibo1Via: TAction;
    actRelFlutuantes: TAction;
    actRelRepasses: TAction;
    actCadFormaPagto: TAction;
    JvTransparentButton3: TJvTransparentButton;
    JvTransparentButton9: TJvTransparentButton;
    btnFinanceiroReceitas: TJvTransparentButton;
    JvTransparentButton11: TJvTransparentButton;
    btnFinanceiroDespesas: TJvTransparentButton;
    JvTransparentButton13: TJvTransparentButton;
    JvTransparentButton14: TJvTransparentButton;
    JvTransparentButton17: TJvTransparentButton;
    JvTransparentButton18: TJvTransparentButton;
    actNiveisLancamentoMenu: TAction;
    ppmMenuNiveisLanc: TJvPopupMenu;
    CategoriasdeReceita1: TMenuItem;
    CategoriasdeDespesas1: TMenuItem;
    SubcategoriasdeReceitas1: TMenuItem;
    SubcategoriasdeDespesas1: TMenuItem;
    actRelFinanceiroMenu: TAction;
    actCadVales: TAction;
    JvTransparentButton20: TJvTransparentButton;
    actCaixaMenu: TAction;
    ppmRelatoriosFinanceiro: TJvPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    ReceitasxDespesas1: TMenuItem;
    Inadimplentes1: TMenuItem;
    N3: TMenuItem;
    Movimentos1: TMenuItem;
    Flutuantes1: TMenuItem;
    Repasses1: TMenuItem;
    JvTransparentButton19: TJvTransparentButton;
    JvTransparentButton21: TJvTransparentButton;
    actOficRecebidos: TAction;
    actOficEnviados: TAction;
    JvTransparentButton22: TJvTransparentButton;
    JvTransparentButton23: TJvTransparentButton;
    JvTransparentButton24: TJvTransparentButton;
    JvTransparentButton25: TJvTransparentButton;
    JvTransparentButton26: TJvTransparentButton;
    JvTransparentButton28: TJvTransparentButton;
    JvTransparentButton29: TJvTransparentButton;
    JvTransparentButton30: TJvTransparentButton;
    JvTransparentButton31: TJvTransparentButton;
    actColaboradoresMenu: TAction;
    ppmColaboradores: TJvPopupMenu;
    Colaboradores1: TMenuItem;
    actCadFechamentoPagtoFuncs: TAction;
    Pagamento1: TMenuItem;
    actCadOutrosLancamentos: TAction;
    O1: TMenuItem;
    Vales2: TMenuItem;
    OutrasDespesas1: TMenuItem;
    ppmProdutos: TJvPopupMenu;
    actEstoque: TAction;
    actProdutosMenu: TAction;
    Estoque1: TMenuItem;
    Produtos1: TMenuItem;
    actCadFechamentoComissoes: TAction;
    FechamentodeComisses1: TMenuItem;
    actRelDespesasColaborador: TAction;
    actCadBaixaUmFlutuante: TAction;
    PorFormaPagamento1: TMenuItem;
    PorTipoAto1: TMenuItem;
    actRelFechamentoCaixaTA: TAction;
    ppmReceita: TJvPopupMenu;
    actCadDepositoFlutuante: TAction;
    actRepasseRecMenu: TAction;
    Depsito1: TMenuItem;
    actCadNatureza: TAction;
    Natureza1: TMenuItem;
    actCadBaixaGpFlutuantes: TAction;
    N5: TMenuItem;
    JvTransparentButton32: TJvTransparentButton;
    actCadCarneLeao: TAction;
    actRelAtos: TAction;
    Atos1: TMenuItem;
    N6: TMenuItem;
    actRelCReceberCPagar: TAction;
    CReceberxCPagar1: TMenuItem;
    ppmCarneLeao: TJvPopupMenu;
    actCarneLeaoMenu: TAction;
    GeraodeCarnLeo1: TMenuItem;
    actCadRelacaoPlanoContas: TAction;
    RelaodePlanodeContas1: TMenuItem;
    btnMenuAbaAjuda: TJvTransparentButton;
    actFiltroOficio: TAction;
    JvTransparentButton33: TJvTransparentButton;
    actRelMovimentos: TAction;
    MovimentodeCaixa1: TMenuItem;
    actRelDiferencaFechCaixa: TAction;
    DiferenadoFechamentodoCaixa1: TMenuItem;
    actRelImpostoRenda: TAction;
    I1: TMenuItem;
    btnMenuAbaFolhas: TJvTransparentButton;
    ppmFiltroLoteFolhas: TJvPopupMenu;
    L1: TMenuItem;
    o2: TMenuItem;
    JvTransparentButton36: TJvTransparentButton;
    JvTransparentButton37: TJvTransparentButton;
    JvTransparentButton27: TJvTransparentButton;
    ppmRelatoriosFolhas: TJvPopupMenu;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    DespesascomColaboradores1: TMenuItem;
    Recibo1: TMenuItem;
    ModelodeRecibo1: TMenuItem;
    actRelHistoricoRecibos: TAction;
    btnAbrirCalculadora: TJvTransparentButton;
    HistricodeRecibos1: TMenuItem;
    L2: TMenuItem;
    L3: TMenuItem;
    N7: TMenuItem;
    RecibosxSelos1: TMenuItem;
    actRelRecibosCancelados: TAction;
    RecibosCancelados1: TMenuItem;
    actCadFeriado: TAction;
    btnMenuAbaEtiquetas: TJvTransparentButton;
    actCadEtiqueta: TAction;
    JvTransparentButton38: TJvTransparentButton;
    JvTransparentButton39: TJvTransparentButton;
    JvTransparentButton40: TJvTransparentButton;
    actFiltroLoteEtiquetas: TAction;
    actCadLoteEtiqueta: TAction;
    actFiltroLoteFolhasMenu: TAction;
    actCadLoteFolhaSeguranca: TAction;
    actCadLoteFolhaRCPN: TAction;
    actRelEtiquetasMenu: TAction;
    actRelFolhasMenu: TAction;
    ppmRelatoriosEtiquetas: TJvPopupMenu;
    MenuItem6: TMenuItem;
    MenuItem13: TMenuItem;
    JvTransparentButton35: TJvTransparentButton;
    JvTransparentButton41: TJvTransparentButton;
    JvTransparentButton42: TJvTransparentButton;
    JvTransparentButton43: TJvTransparentButton;
    JvTransparentButton44: TJvTransparentButton;
    actRelEtqLoteEtiquetas: TAction;
    actRelEtqEtiquetas: TAction;
    actRelFlsSegLoteFolhasSeguranca: TAction;
    actRelFlsSegLoteFolhasSegurancaRCPN: TAction;
    actRelFlsSegFolhasSeguranca: TAction;
    actRelFlsSegFolhasSegurancaRCPN: TAction;
    actFiltroLoteFolhaSeguranca: TAction;
    actFiltroLoteFolhaRCPN: TAction;
    actFiltroFolhaSeguranca: TAction;
    actFiltroFolhaRCPN: TAction;
    JvTransparentButton46: TJvTransparentButton;
    actCadCargo: TAction;
    btnMenuAlerta: TJvTransparentButton;
    btnLancPendentes: TJvTransparentButton;
    JvTransparentButton34: TJvTransparentButton;
    actConfigAtribuicoes: TAction;
    Image1: TImage;
    lblCaixaVigente: TLabel;
    JvTransparentButton15: TJvTransparentButton;
    JvTransparentButton8: TJvTransparentButton;
    btnFormaPagamento: TJvTransparentButton;
    JvTransparentButton47: TJvTransparentButton;
    ppmDespesa: TJvPopupMenu;
    ppmiRepasseReceita: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    L4: TMenuItem;
    L5: TMenuItem;
    actConfigCadEmails: TAction;
    JvTransparentButton16: TJvTransparentButton;
    btnAtualizarBancosDados: TJvTransparentButton;
    actSupAtualizarBancos: TAction;
    JvTransparentButton4: TJvTransparentButton;
    actSupDownloadBancoM: TAction;
    Depsito2: TMenuItem;
    actCadVinculoLancDeposito: TAction;
    actDepositosFlutuantesMenu: TAction;
    VincularReceitaseDepsitos1: TMenuItem;
    actSupAtualizarRelatorios: TAction;
    JvTransparentButton5: TJvTransparentButton;
    JvTransparentButton10: TJvTransparentButton;
    actSupBaixarHomologacao: TAction;
    actSupCorrigirImportacoesDuplicadas: TAction;
    JvTransparentButton12: TJvTransparentButton;
    actSupVoltarVersaoSistema: TAction;
    JvTransparentButton45: TJvTransparentButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure actSupCriptografiaExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure actConfigCadUsuariosExecute(Sender: TObject);
    procedure actCadCategoriaReceitasExecute(Sender: TObject);
    procedure actCadCategoriaDespesasExecute(Sender: TObject);
    procedure actCadFornecedoresExecute(Sender: TObject);
    procedure actCadColaboradoresExecute(Sender: TObject);
    procedure actCadReceitaExecute(Sender: TObject);
    procedure actCadDespesaExecute(Sender: TObject);
    procedure actConfigSistemaExecute(Sender: TObject);
    procedure actConfigCadPermissoesExecute(Sender: TObject);
    procedure actCadSubcatReceitasExecute(Sender: TObject);
    procedure actCadSubcatDespesasExecute(Sender: TObject);
    procedure actCadClientesExecute(Sender: TObject);
    procedure actRelFechamentoCaixaFPExecute(Sender: TObject);
    procedure actRelExtratoContasExecute(Sender: TObject);
    procedure actRelFlutuantesExecute(Sender: TObject);
    procedure actRelRepassesExecute(Sender: TObject);
    procedure actCadFormaPagtoExecute(Sender: TObject);
    procedure actCadProdutosExecute(Sender: TObject);
    procedure actCadServicosExecute(Sender: TObject);
    procedure actFiltroLancamentoExecute(Sender: TObject);
    procedure actNiveisLancamentoMenuExecute(Sender: TObject);
    procedure btnMenuAbaAtalhosClick(Sender: TObject);
    procedure btnMenuAbaFinanceiroClick(Sender: TObject);
    procedure btnMenuAbaOficiosClick(Sender: TObject);
    procedure btnMenuAbaConfiguracoesClick(Sender: TObject);
    procedure btnMenuAbaSuporteClick(Sender: TObject);
    procedure actCaixaMenuExecute(Sender: TObject);
    procedure actRelFinanceiroMenuExecute(Sender: TObject);
    procedure actCadValesExecute(Sender: TObject);
    procedure actSupArquivoIniExecute(Sender: TObject);
    procedure actColaboradoresMenuExecute(Sender: TObject);
    procedure actCadFechamentoPagtoFuncsExecute(Sender: TObject);
    procedure actCadOutrosLancamentosExecute(Sender: TObject);
    procedure actProdutosMenuExecute(Sender: TObject);
    procedure actEstoqueExecute(Sender: TObject);
    procedure btnMenuAlertaClick(Sender: TObject);
    procedure actCadFechamentoComissoesExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure actRelFechamentoCaixaTAExecute(Sender: TObject);
    procedure actCadDepositoFlutuanteExecute(Sender: TObject);
    procedure actCadNaturezaExecute(Sender: TObject);
    procedure actCadBaixaUmFlutuanteExecute(Sender: TObject);
    procedure actCadBaixaGpFlutuantesExecute(Sender: TObject);
    procedure actCadCarneLeaoExecute(Sender: TObject);
    procedure actRelAtosExecute(Sender: TObject);
    procedure actRelCReceberCPagarExecute(Sender: TObject);
    procedure actCadRelacaoPlanoContasExecute(Sender: TObject);
    procedure actCarneLeaoMenuExecute(Sender: TObject);
    procedure btnLancPendentesClick(Sender: TObject);
    procedure actOficEnviadosExecute(Sender: TObject);
    procedure actOficRecebidosExecute(Sender: TObject);
    procedure actFiltroOficioExecute(Sender: TObject);
    procedure actRelMovimentosExecute(Sender: TObject);
    procedure actRelDiferencaFechCaixaExecute(Sender: TObject);
    procedure actRelImpostoRendaExecute(Sender: TObject);
    procedure btnMenuAbaFolhasClick(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure JvTransparentButton29Click(Sender: TObject);
    function ShellExecuteAndWait(Operation, FileName, Parameter, Directory: String; Show: Word; bWait: Boolean): Longint;
    procedure actRelDespesasColaboradorExecute(Sender: TObject);
    procedure actRelRecibo1ViaExecute(Sender: TObject);
    procedure btnAbrirCalculadoraClick(Sender: TObject);
    procedure actRelHistoricoRecibosExecute(Sender: TObject);
    procedure actRelRecibosCanceladosExecute(Sender: TObject);
    procedure actCadFeriadoExecute(Sender: TObject);
    procedure btnMenuAbaEtiquetasClick(Sender: TObject);
    procedure actRelEtiquetasMenuExecute(Sender: TObject);
    procedure actRelFolhasMenuExecute(Sender: TObject);
    procedure actFiltroLoteEtiquetasExecute(Sender: TObject);
    procedure actRelEtqLoteEtiquetasExecute(Sender: TObject);
    procedure actRelEtqEtiquetasExecute(Sender: TObject);
    procedure actRelFlsSegLoteFolhasSegurancaExecute(Sender: TObject);
    procedure actRelFlsSegLoteFolhasSegurancaRCPNExecute(Sender: TObject);
    procedure actRelFlsSegFolhasSegurancaExecute(Sender: TObject);
    procedure actRelFlsSegFolhasSegurancaRCPNExecute(Sender: TObject);
    procedure actFiltroLoteFolhasMenuExecute(Sender: TObject);
    procedure actFiltroLoteFolhaSegurancaExecute(Sender: TObject);
    procedure actFiltroLoteFolhaRCPNExecute(Sender: TObject);
    procedure actCadLoteFolhaSegurancaExecute(Sender: TObject);
    procedure actCadLoteFolhaRCPNExecute(Sender: TObject);
    procedure actCadLoteEtiquetaExecute(Sender: TObject);
    procedure actFiltroFolhaSegurancaExecute(Sender: TObject);
    procedure actFiltroFolhaRCPNExecute(Sender: TObject);
    procedure actCadEtiquetaExecute(Sender: TObject);
    procedure actCadCargoExecute(Sender: TObject);
    procedure actConfigAtribuicoesExecute(Sender: TObject);
    procedure Panel3CanResize(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
    procedure actConfigFiltroLogExecute(Sender: TObject);
    procedure btnFinanceiroReceitasClick(Sender: TObject);
    procedure btnFinanceiroDespesasClick(Sender: TObject);
    procedure btnAtalhoReceitasClick(Sender: TObject);
    procedure btnAtalhoDespesasClick(Sender: TObject);
    procedure actRepasseRecMenuExecute(Sender: TObject);
    procedure actConfigCadEmailsExecute(Sender: TObject);
    procedure actDepositosFlutuantesMenuExecute(Sender: TObject);
    procedure actCadVinculoLancDepositoExecute(Sender: TObject);
    procedure actSupAtualizarBancosExecute(Sender: TObject);
    procedure actSupDownloadBancoMExecute(Sender: TObject);
    procedure actSupAtualizarRelatoriosExecute(Sender: TObject);
    procedure actSupBaixarHomologacaoExecute(Sender: TObject);
    procedure btnMenuAbaAjudaClick(Sender: TObject);
    procedure actSupCorrigirImportacoesDuplicadasExecute(Sender: TObject);
    procedure actSupVoltarVersaoSistemaExecute(Sender: TObject);
  private
    { Private declarations }

    procedure AbrirMenuBotoes(Aba: TJvTransparentButton);
    procedure HabilitarDesabilitarModuloCliente;
    procedure HabilitarDesabilitarAcaoUsuario;
  public
    { Public declarations }
  end;

var
  FPrincipal: TFPrincipal;
  UFRMAtuSis: TFFRMAtualizacaoSistema;
  UFRMLancPend: TFFRMLancamentosPendentes;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UVariaveisGlobais, UCriptografia, UDM, UGDM,
  UFiltroUsuarios, UFiltroPermissoes, UConfiguracaoSistema,
  UDMConfiguracaoSistema, UFiltroCategoriaDespRec, UFiltroSubCategoriaDespRec,
  UFiltroFormaPagamento, UFiltroClienteFornecedor, UFiltroProdutoServico,
  UFiltroLancamento, UDMLancamento, UCadastroLancamento, UFiltroColaboradores,
  UFiltroVales, UArquivoIni, UFiltroFechamentoSalario,
  UFiltroOutrasDespesasColaboradores, UFiltroEstoque,
  UFiltroRelatorioExtratoContas, UDMRelatorios, UFiltroRelatorioFechamentoCaixa,
  UBaixaLancamento, UFiltroRelatorioFechamentoCaixaAtos,
  UFiltroDepositosFlutuantes, UFiltroNatureza, UListaBaixaFlutuantes,
  UFiltroRelatorioFlutuantes, UFiltroCarneLeao, UFiltroRelatorioRepasses,
  UFiltroRelatorioAtos, UFiltroRelatorioCReceberCPagar,
  UCadastroEquivalenciaCarneLeao, UDMCarneLeao, UFiltroComissoes,
  UCadastroOficio, UDMOficio, UFiltroOficio, UFiltroRelatorioMovimentoCaixa,
  UDMBaixa, UFiltroRelatorioDiferencasFechamentoCaixa,
  UFiltroRelatorioRecDespIR, UFiltroRelatorioDespesasColaboradores,
  UFiltroRelatorioRecibo, UListaRecibos, UFiltroEtiqueta, UFiltroFolhaRCPN,
  UFiltroFolhaSeguranca, UFiltroLoteEtiqueta, UFiltroLoteRCPN,
  UFiltroLoteSeguranca, UFiltroRelatorioEtiquetas, UFiltroRelatorioFolha,
  UFiltroRelatorioLote, UFiltroRelatorioLoteEtiqueta,
  UFiltroRelatorioRecibosCancelados, UFiltroFeriado, UCadastroLoteSeguranca,
  UCadastroLoteRCPN, UCadastroLoteEtiqueta, UDMFolhaSeguranca, UDMEtiqueta,
  UFiltroCargo, UCadastroAtribuicoesServentia, UFiltroCaixas, UDMCaixa,
  UFiltroLogUsuario, UConfiguracaoEmails, URepassesVincularDespesa,
  UVersaoSistema, ULancamentosVincularDepositos, UConsultaArquivoPDF,
  USolicitaData;

{ TFPrincipal }

procedure TFPrincipal.actCadDepositoFlutuanteExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroDepositosFlutuantes, FFiltroDepositosFlutuantes);
end;

procedure TFPrincipal.actCadDespesaExecute(Sender: TObject);
var
  lEncerrar: Boolean;
begin
  repeat
    vgOperacao       := I;
    vgIdConsulta     := 0;
    vgOrigemFiltro   := False;
    vgLanc_CriarNovo := False;
    lEncerrar        := False;

    try
      Application.CreateForm(TdmLancamento, dmLancamento);
      Application.CreateForm(TFCadastroLancamento, FCadastroLancamento);

      FCadastroLancamento.TipoLanc := DESP;

      FCadastroLancamento.ShowModal;
    finally
      FreeAndNil(dmLancamento);
      FCadastroLancamento.Free;
    end;

    if vgLanc_CriarNovo then
    begin
      if Application.MessageBox('Deseja criar um novo Lan�amento de Despesa?',
                                'Novo Lan�amento',
                                 MB_YESNO + MB_ICONQUESTION) = ID_NO then
      lEncerrar := True;
    end
    else
      lEncerrar := True;           
  until lEncerrar;
end;

procedure TFPrincipal.actCadEtiquetaExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroEtiqueta,FFiltroEtiqueta);
end;

procedure TFPrincipal.actCadFechamentoComissoesExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroComissoes, FFiltroComissoes);

//  if vgOrigFechPend then
//    HabilitarDesabilitarAcaoUsuario;
end;

procedure TFPrincipal.actCadFechamentoPagtoFuncsExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroFechamentoSalario, FFiltroFechamentoSalario);

//  if vgOrigFechPend then
//    HabilitarDesabilitarAcaoUsuario;
end;

procedure TFPrincipal.actCadFeriadoExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroFeriado, FFiltroFeriado);
end;

procedure TFPrincipal.actCadFormaPagtoExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroFormaPagamento, FFiltroFormaPagamento);
end;

procedure TFPrincipal.actCadFornecedoresExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  try
    Application.CreateForm(TFFiltroClienteFornecedor, FFiltroClienteFornecedor);

    TpCliFor := FORN;  //Fornecedor

    FFiltroClienteFornecedor.ShowModal;
  finally
    FFiltroClienteFornecedor.Free;
    vgOrigemCadastro := False;
  end;
end;

procedure TFPrincipal.actCadLoteEtiquetaExecute(Sender: TObject);
begin
  vgOrigemCadastro := False;
  vgOrigemFiltro   := False;
  vgIdConsulta     := 0;
  vgOperacao       := I;

  try
    Application.CreateForm(TdmEtiqueta, dmEtiqueta);
    Application.CreateForm(TFCadastroLoteEtiqueta, FCadastroLoteEtiqueta);

    FCadastroLoteEtiqueta.ShowModal;
  finally
    FreeAndNil(dmEtiqueta);
    FCadastroLoteEtiqueta.Free;

    vgOrigemCadastro := False;
  end;
end;

procedure TFPrincipal.actCadLoteFolhaSegurancaExecute(Sender: TObject);
begin
  vgOrigemCadastro := False;
  vgOrigemFiltro   := False;
  vgIdConsulta     := 0;
  vgOperacao       := I;

  try
    Application.CreateForm(TdmFolhaSeguranca, dmFolhaSeguranca);
    Application.CreateForm(TFCadastroLoteSeguranca, FCadastroLoteSeguranca);

    FCadastroLoteSeguranca.ShowModal;
  finally
    FreeAndNil(dmFolhaSeguranca);
    FCadastroLoteSeguranca.Free;

    vgOrigemCadastro := False;
  end;
end;

procedure TFPrincipal.actCadLoteFolhaRCPNExecute(Sender: TObject);
begin
  vgOrigemCadastro := False;
  vgOrigemFiltro   := False;
  vgIdConsulta     := 0;
  vgOperacao       := I;

  try
    Application.CreateForm(TdmFolhaSeguranca, dmFolhaSeguranca);
    Application.CreateForm(TFCadastroLoteRCPN, FCadastroLoteRCPN);

    FCadastroLoteRCPN.ShowModal;
  finally
    FreeAndNil(dmFolhaSeguranca);
    FCadastroLoteRCPN.Free;

    vgOrigemCadastro := False;
  end;
end;

procedure TFPrincipal.actCadNaturezaExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroNatureza, FFiltroNatureza);
end;

procedure TFPrincipal.actCadOutrosLancamentosExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroOutrasDespesasColaboradores, FFiltroOutrasDespesasColaboradores);
end;

procedure TFPrincipal.actCadColaboradoresExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroColaboradores, FFiltroColaboradores);
end;

procedure TFPrincipal.actCadProdutosExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  try
    Application.CreateForm(TFFiltroProdutoServico, FFiltroProdutoServico);

    TpProdServ := PROD;  //Produto

    FFiltroProdutoServico.ShowModal;
  finally
    FFiltroProdutoServico.Free;
    vgOrigemCadastro := False;
  end;
end;

procedure TFPrincipal.actCadReceitaExecute(Sender: TObject);
var
  lEncerrar: Boolean;
begin
  repeat
    vgOperacao     := I;
    vgIdConsulta   := 0;
    vgOrigemFiltro := False;
    vgLanc_CriarNovo := False;
    lEncerrar        := False;

    try
      Application.CreateForm(TdmLancamento, dmLancamento);
      Application.CreateForm(TFCadastroLancamento, FCadastroLancamento);

      FCadastroLancamento.TipoLanc := REC;

      FCadastroLancamento.ShowModal;
    finally
      FreeAndNil(dmLancamento);
      FCadastroLancamento.Free;
    end;

    if vgLanc_CriarNovo then
    begin
      if Application.MessageBox('Deseja criar um novo Lan�amento de Receita?',
                                'Novo Lan�amento',
                                 MB_YESNO + MB_ICONQUESTION) = ID_NO then
      lEncerrar := True;
    end
    else
      lEncerrar := True;
  until lEncerrar;
end;

procedure TFPrincipal.actCadRelacaoPlanoContasExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmCarneLeao, dmCarneLeao);
  dmGerencial.CriarForm(TFCadastroEquivalenciaCarneLeao, FCadastroEquivalenciaCarneLeao);
  FreeAndNil(dmCarneLeao);
end;

procedure TFPrincipal.actCadServicosExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  try
    Application.CreateForm(TFFiltroProdutoServico, FFiltroProdutoServico);

    TpProdServ := SERV;  //Servico

    FFiltroProdutoServico.ShowModal;
  finally
    FFiltroProdutoServico.Free;
    vgOrigemCadastro := False;
  end;
end;

procedure TFPrincipal.actCadSubcatDespesasExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  try
    Application.CreateForm(TFFiltroSubCategoriaDespRec, FFiltroSubCategoriaDespRec);

    TipoSubCategoria := 'D';

    FFiltroSubCategoriaDespRec.ShowModal;
  finally
    FFiltroSubCategoriaDespRec.Free;
    vgOrigemCadastro := False;
  end;
end;

procedure TFPrincipal.AbrirMenuBotoes(Aba: TJvTransparentButton);
begin
  case Aba.Tag of
    1001: ntbMenuBotoes.PageIndex := 0;
    1002: ntbMenuBotoes.PageIndex := 1;
    1003: ntbMenuBotoes.PageIndex := 2;
    1004: ntbMenuBotoes.PageIndex := 3;
    1005: ntbMenuBotoes.PageIndex := 4;
    1006: ntbMenuBotoes.PageIndex := 5;
    1007: ntbMenuBotoes.PageIndex := 6;
  end;
end;

procedure TFPrincipal.actCadBaixaGpFlutuantesExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmBaixa, dmBaixa);
  dmGerencial.CriarForm(TFListaBaixaFlutuantes, FListaBaixaFlutuantes);
  FreeAndNil(dmBaixa);
end;

procedure TFPrincipal.actCadBaixaUmFlutuanteExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmBaixa, dmBaixa);

//  dmGerencial.CriarForm(TFBaixaLancamento, FBaixaLancamento);
  dmGerencial.CriarForm(TFRepassesVincularDespesa, FRepassesVincularDespesa);

  FreeAndNil(dmBaixa);
end;

procedure TFPrincipal.actCadCargoExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroCargo, FFiltroCargo);
end;

procedure TFPrincipal.actCadCarneLeaoExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroCarneLeao, FFiltroCarneLeao);
end;

procedure TFPrincipal.actCadCategoriaDespesasExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  try
    Application.CreateForm(TFFiltroCategoriaDespRec, FFiltroCategoriaDespRec);

    TipoCategoria := 'D';

    FFiltroCategoriaDespRec.ShowModal;
  finally
    FFiltroCategoriaDespRec.Free;
    vgOrigemCadastro := False;
  end;
end;

procedure TFPrincipal.actCadCategoriaReceitasExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  try
    Application.CreateForm(TFFiltroCategoriaDespRec, FFiltroCategoriaDespRec);

    TipoCategoria := 'R';  //Receita

    FFiltroCategoriaDespRec.ShowModal;
  finally
    FFiltroCategoriaDespRec.Free;
    vgOrigemCadastro := False;
  end;
end;

procedure TFPrincipal.actCadClientesExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  try
    Application.CreateForm(TFFiltroClienteFornecedor, FFiltroClienteFornecedor);

    TpCliFor := CLIE;  //Cliente

    FFiltroClienteFornecedor.ShowModal;
  finally
    FFiltroClienteFornecedor.Free;
    vgOrigemCadastro := False;
  end;
end;

procedure TFPrincipal.actColaboradoresMenuExecute(Sender: TObject);
begin
  //pop-up
end;

procedure TFPrincipal.actConfigAtribuicoesExecute(Sender: TObject);
begin
  vgOperacao := E;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFCadastroAtribuicoesServentia, FCadastroAtribuicoesServentia);
end;

procedure TFPrincipal.actConfigCadEmailsExecute(Sender: TObject);
begin
  if (not vgPrm_IncRemDest) and
    (not vgPrm_EdRemDest) and
    (not vgPrm_ExcRemDest) then
    vgOperacao := C
  else
    vgOperacao := E;

  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFConfiguracaoEmails, FConfiguracaoEmails);
end;

procedure TFPrincipal.actConfigCadPermissoesExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroPermissoes, FFiltroPermissoes);
end;

procedure TFPrincipal.actConfigCadUsuariosExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroUsuarios, FFiltroUsuarios);
end;

procedure TFPrincipal.actConfigFiltroLogExecute(Sender: TObject);
begin
  vgOperacao := C;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroLogUsuario, FFiltroLogUsuario);
end;

procedure TFPrincipal.actConfigSistemaExecute(Sender: TObject);
begin
  vgOperacao := E;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmConfiguracaoSistema, dmConfiguracaoSistema);
  dmGerencial.CriarForm(TFConfiguracaoSistema, FConfiguracaoSistema);
end;

procedure TFPrincipal.actDepositosFlutuantesMenuExecute(Sender: TObject);
begin
  //pop-up
end;

procedure TFPrincipal.actSupArquivoIniExecute(Sender: TObject);
begin
  vgOperacao := E;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFArquivoIni, FArquivoIni);
end;

procedure TFPrincipal.actSupAtualizarBancosExecute(Sender: TObject);
begin
  //Forca a atualizacao da estrutura dos bancos de dados Monitoramento e Gerencial
  dmGerencial.Processando(True,
                          'Atualizando a estrutura dos Bancos de Dados Monitoramento e Gerencial',
                          True,
                          'Por favor, aguarde...');

  VS.AtualizaEstruturaBancosDados('1.0.0.1', 1);
  VS.AtualizaEstruturaBancosDados('1.0.0.1', 2);
  VS.AtualizaEstruturaBancosDados('1.0.0.6', 1);
  VS.AtualizaEstruturaBancosDados('1.0.0.7', 1);
  VS.AtualizaEstruturaBancosDados('1.0.0.9', 1);
  VS.AtualizaEstruturaBancosDados('1.0.0.10', 1);
  VS.AtualizaEstruturaBancosDados('1.0.0.10', 2);

  dmGerencial.Processando(False);
end;

procedure TFPrincipal.actSupAtualizarRelatoriosExecute(Sender: TObject);
var
  lAtualiza: Boolean;
begin
  lAtualiza := False;

  if vgConf_Ambiente = 'HML' then
    lAtualiza := True
  else
  begin
    if Application.MessageBox('Esse ambiente � de Produ��o. Deseja realmente atualizar os Relat�rios?',
                              'Aviso',
                              MB_YESNO + MB_ICONWARNING) = ID_YES then
      lAtualiza := True;
  end;

  if lAtualiza then
  begin
    //Realiza o download dos Relatorios do Monitoramento
    dmGerencial.Processando(True,
                            'Fazendo download dos Relat�rios do Monitoramento',
                            True,
                            'Por favor, aguarde...');

    if FileExists(vgDirExec + 'Relatorios\Modelos\frRelatorio_Base.fr3') then
      DeleteFile(PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_Base.fr3'));

    if FileExists(vgDirExec + 'Relatorios\Modelos\frRelatorio_Atos.fr3') then
      DeleteFile(PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_Atos.fr3'));

    if FileExists(vgDirExec + 'Relatorios\Modelos\frRelatorio_ContasReceberContasPagar.fr3') then
      DeleteFile(PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_ContasReceberContasPagar.fr3'));

    if FileExists(vgDirExec + 'Relatorios\Modelos\frRelatorio_DespesasColaborador.fr3') then
      DeleteFile(PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_DespesasColaborador.fr3'));

    if FileExists(vgDirExec + 'Relatorios\Modelos\frRelatorio_DiferencaFechamentoCaixa.fr3') then
      DeleteFile(PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_DiferencaFechamentoCaixa.fr3'));

    if FileExists(vgDirExec + 'Relatorios\Modelos\frRelatorio_Etiqueta.fr3') then
      DeleteFile(PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_Etiqueta.fr3'));

    if FileExists(vgDirExec + 'Relatorios\Modelos\frRelatorio_ExtratoConta.fr3') then
      DeleteFile(PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_ExtratoConta.fr3'));

    if FileExists(vgDirExec + 'Relatorios\Modelos\frRelatorio_FechamentoCaixa.fr3') then
      DeleteFile(PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_FechamentoCaixa.fr3'));

    if FileExists(vgDirExec + 'Relatorios\Modelos\frRelatorio_FechamentoCaixaAtos.fr3') then
      DeleteFile(PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_FechamentoCaixaAtos.fr3'));

    if FileExists(vgDirExec + 'Relatorios\Modelos\frRelatorio_Flutuantes.fr3') then
      DeleteFile(PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_Flutuantes.fr3'));

    if FileExists(vgDirExec + 'Relatorios\Modelos\frRelatorio_Folha.fr3') then
      DeleteFile(PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_Folha.fr3'));

    if FileExists(vgDirExec + 'Relatorios\Modelos\frRelatorio_LancamentosFiltro.fr3') then
      DeleteFile(PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_LogUsuarioFiltro.fr3'));

    if FileExists(vgDirExec + 'Relatorios\Modelos\frRelatorio_Lote.fr3') then
      DeleteFile(PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_Lote.fr3'));

    if FileExists(vgDirExec + 'Relatorios\Modelos\frRelatorio_LoteEtiqueta.fr3') then
      DeleteFile(PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_LoteEtiqueta.fr3'));

    if FileExists(vgDirExec + 'Relatorios\Modelos\frRelatorio_ModeloRecibo.fr3') then
      DeleteFile(PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_ModeloRecibo.fr3'));

    if FileExists(vgDirExec + 'Relatorios\Modelos\frRelatorio_MovimentoCaixa.fr3') then
      DeleteFile(PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_MovimentoCaixa.fr3'));

    if FileExists(vgDirExec + 'Relatorios\Modelos\frRelatorio_ReceitasDespesasIR.fr3') then
      DeleteFile(PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_ReceitasDespesasIR.fr3'));

    if FileExists(vgDirExec + 'Relatorios\Modelos\frRelatorio_RecibosCancelados.fr3') then
      DeleteFile(PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_RecibosCancelados.fr3'));

    if FileExists(vgDirExec + 'Relatorios\Modelos\frRelatorio_Repasses.fr3') then
      DeleteFile(PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_Repasses.fr3'));

    dmGerencial.IdFtp.Disconnect;
    dmGerencial.IdFtp.UseTLS := utNoTLSSupport;
    dmGerencial.IdFtp.Connect;
    dmGerencial.IdFtp.ChangeDir('www\cartorios\StatusTotal\Monitoramento');

    dmGerencial.IdFTP.Get(PChar('frRelatorio_Base.fr3'),
                          PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_Base.fr3'),
                          True);

    dmGerencial.IdFTP.Get(PChar('frRelatorio_Atos.fr3'),
                          PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_Atos.fr3'),
                          True);

    dmGerencial.IdFTP.Get(PChar('frRelatorio_ContasReceberContasPagar.fr3'),
                          PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_ContasReceberContasPagar.fr3'),
                          True);

    dmGerencial.IdFTP.Get(PChar('frRelatorio_DespesasColaborador.fr3'),
                          PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_DespesasColaborador.fr3'),
                          True);

    dmGerencial.IdFTP.Get(PChar('frRelatorio_DiferencaFechamentoCaixa.fr3'),
                          PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_DiferencaFechamentoCaixa.fr3'),
                          True);

    dmGerencial.IdFTP.Get(PChar('frRelatorio_Etiqueta.fr3'),
                          PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_Etiqueta.fr3'),
                          True);

    dmGerencial.IdFTP.Get(PChar('frRelatorio_ExtratoConta.fr3'),
                          PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_ExtratoConta.fr3'),
                          True);

    dmGerencial.IdFTP.Get(PChar('frRelatorio_FechamentoCaixa.fr3'),
                          PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_FechamentoCaixa.fr3'),
                          True);

    dmGerencial.IdFTP.Get(PChar('frRelatorio_FechamentoCaixaAtos.fr3'),
                          PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_FechamentoCaixaAtos.fr3'),
                          True);

    dmGerencial.IdFTP.Get(PChar('frRelatorio_Flutuantes.fr3'),
                          PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_Flutuantes.fr3'),
                          True);

    dmGerencial.IdFTP.Get(PChar('frRelatorio_Folha.fr3'),
                          PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_Folha.fr3'),
                          True);

    dmGerencial.IdFTP.Get(PChar('frRelatorio_LancamentosFiltro.fr3'),
                          PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_LancamentosFiltro.fr3'),
                          True);

    dmGerencial.IdFTP.Get(PChar('frRelatorio_LogUsuarioFiltro.fr3'),
                          PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_LogUsuarioFiltro.fr3'),
                          True);

    dmGerencial.IdFTP.Get(PChar('frRelatorio_Lote.fr3'),
                          PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_Lote.fr3'),
                          True);

    dmGerencial.IdFTP.Get(PChar('frRelatorio_LoteEtiqueta.fr3'),
                          PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_LoteEtiqueta.fr3'),
                          True);

    dmGerencial.IdFTP.Get(PChar('frRelatorio_ModeloRecibo.fr3'),
                          PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_ModeloRecibo.fr3'),
                          True);

    dmGerencial.IdFTP.Get(PChar('frRelatorio_MovimentoCaixa.fr3'),
                          PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_MovimentoCaixa.fr3'),
                          True);

    dmGerencial.IdFTP.Get(PChar('frRelatorio_ReceitasDespesasIR.fr3'),
                          PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_ReceitasDespesasIR.fr3'),
                          True);

    dmGerencial.IdFTP.Get(PChar('frRelatorio_RecibosCancelados.fr3'),
                          PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_RecibosCancelados.fr3'),
                          True);

    dmGerencial.IdFTP.Get(PChar('frRelatorio_Repasses.fr3'),
                          PChar(vgDirExec + 'Relatorios\Modelos\frRelatorio_Repasses.fr3'),
                          True);

    dmGerencial.IdFTP.Disconnect;

    dmGerencial.Processando(False);
  end;
end;

procedure TFPrincipal.actSupBaixarHomologacaoExecute(Sender: TObject);
begin
  //Realiza o download do material para Homologacao do Monitoramento e do Sync
  dmGerencial.Processando(True,
                          'Fazendo o download do material para Homologa��o',
                          True,
                          'Por favor, aguarde...');

  if FileExists(vgDirExec + 'Homologacao.rar') then
    DeleteFile(PChar(vgDirExec + 'Homologacao.rar'));

  dmGerencial.IdFtp.Disconnect;
  dmGerencial.IdFtp.UseTLS := utNoTLSSupport;
  dmGerencial.IdFtp.Connect;
  dmGerencial.IdFtp.ChangeDir('www\cartorios\StatusTotal\Monitoramento\Homologacao');

  dmGerencial.IdFTP.Get(PChar('Homologacao.rar'),
                        PChar(vgDirExec + 'Homologacao.rar'),
                        True);

  dmGerencial.IdFTP.Disconnect;

  dmGerencial.Processando(False);
end;

procedure TFPrincipal.actSupCorrigirImportacoesDuplicadasExecute(
  Sender: TObject);
var
  sQry: String;
  dDataImportacao: TDateTime;
begin
  sQry := '';

  dDataImportacao := 0;

  //Solicita a Data de Realizacao dos Pagamentos
  try
    Application.CreateForm(TFSolicitaData, FSolicitaData);

    FSolicitaData.sCaptionLabel      := 'Por favor, informe a Data de in�cio das importa��es duplicadas:';
    FSolicitaData.lVerifCaixaVigente := False;

    FSolicitaData.ShowModal;
  finally
    dDataImportacao := FSolicitaData.dDataSolicitada;
    FSolicitaData.Free;
  end;

  if dDataImportacao = 0 then
    Application.MessageBox('� necess�rio informar a data de in�cio das importa��es duplicadas.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING)
  else
    dmPrincipal.CancelarImportacoesDuplicadas(dDataImportacao);
end;

procedure TFPrincipal.actSupCriptografiaExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFCriptografia, FCriptografia);
end;

procedure TFPrincipal.actSupDownloadBancoMExecute(Sender: TObject);
begin
  //Realiza o download do banco de dados VAZIO do Monitoramento
  dmGerencial.Processando(True,
                          'Fazendo o download do Banco de Dados Monitoramento',
                          True,
                          'Por favor, aguarde...');

  if FileExists(vgDirExec + 'MONITORAMENTO - VAZIO.rar') then
    DeleteFile(PChar(vgDirExec + 'MONITORAMENTO - VAZIO.rar'));

  dmGerencial.IdFtp.Disconnect;
  dmGerencial.IdFtp.UseTLS := utNoTLSSupport;
  dmGerencial.IdFtp.Connect;
  dmGerencial.IdFtp.ChangeDir('www\cartorios\StatusTotal\Monitoramento');

  dmGerencial.IdFTP.Get(PChar('MONITORAMENTO - VAZIO.rar'),
                        PChar(vgDirExec + 'MONITORAMENTO - VAZIO.rar'),
                        True);

  dmGerencial.IdFTP.Disconnect;

  dmGerencial.Processando(False);
end;

procedure TFPrincipal.actSupVoltarVersaoSistemaExecute(Sender: TObject);
begin
  dmGerencial.VoltarVersaoSistema;
end;

procedure TFPrincipal.actEstoqueExecute(Sender: TObject);
begin
  vgOperacao := E;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroEstoque, FFiltroEstoque);
end;

procedure TFPrincipal.actFiltroFolhaSegurancaExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroFolhaSeguranca,FFiltroFolhaSeguranca);
end;

procedure TFPrincipal.actFiltroFolhaRCPNExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroFolhaRcpn,FFiltroFolhaRcpn);
end;

procedure TFPrincipal.actFiltroLancamentoExecute(Sender: TObject);
begin
  vgOperacao     := C;
  vgOrigemFiltro := False;

  vgAto_CodigoAto := 0;
  vgAto_AnoAto    := YearOf(Date);
  vgLanc_Codigo   := 0;
  vgLanc_Ano      := 0;
  vgLanc_TpLanc   := '';

  dmGerencial.CriarForm(TFFiltroLancamento, FFiltroLancamento);
end;

procedure TFPrincipal.actFiltroLoteEtiquetasExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroLoteEtiqueta,FFiltroLoteEtiqueta);
end;

procedure TFPrincipal.actFiltroLoteFolhaSegurancaExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroLoteSeguranca,FFiltroLoteSeguranca);
end;

procedure TFPrincipal.actFiltroLoteFolhaRCPNExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroLoteRCPN,FFiltroLoteRCPN);
end;

procedure TFPrincipal.actFiltroLoteFolhasMenuExecute(Sender: TObject);
begin
  //pop-up
end;

procedure TFPrincipal.actFiltroOficioExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroOficio, FFiltroOficio);
end;

procedure TFPrincipal.actOficEnviadosExecute(Sender: TObject);
begin
  vgOperacao       := VAZIO;
  vgOrigemCadastro := False;
  vgOrigemFiltro   := False;
  vgIdConsulta     := 0;
  vgOperacao       := I;

  try
    Application.CreateForm(TdmOficio, dmOficio);
    Application.CreateForm(TFCadastroOficio, FCadastroOficio);

    TpOfc := ENV;

    FCadastroOficio.ShowModal;
  finally
    FCadastroOficio.Free;
    FreeAndNil(dmOficio);

    vgOrigemCadastro := False;
  end;
end;

procedure TFPrincipal.actOficRecebidosExecute(Sender: TObject);
begin
  vgOperacao       := VAZIO;
  vgOrigemCadastro := False;
  vgOrigemFiltro   := False;
  vgIdConsulta     := 0;
  vgOperacao       := I;

  try
    Application.CreateForm(TdmOficio, dmOficio);
    Application.CreateForm(TFCadastroOficio, FCadastroOficio);

    TpOfc := RCB;

    FCadastroOficio.ShowModal;
  finally
    FCadastroOficio.Free;
    FreeAndNil(dmOficio);

    vgOrigemCadastro := False;
  end;
end;

procedure TFPrincipal.actProdutosMenuExecute(Sender: TObject);
begin
  //pop-up
end;

procedure TFPrincipal.actCadSubcatReceitasExecute(Sender: TObject);
begin
  vgOperacao       := VAZIO;
  vgOrigemCadastro := False;

  try
    Application.CreateForm(TFFiltroSubCategoriaDespRec, FFiltroSubCategoriaDespRec);

    TipoSubCategoria := 'R';  //Receita

    FFiltroSubCategoriaDespRec.ShowModal;
  finally
    FFiltroSubCategoriaDespRec.Free;
    vgOrigemCadastro := False;
  end;
end;

procedure TFPrincipal.actCadValesExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroVales, FFiltroVales);
end;

procedure TFPrincipal.actCadVinculoLancDepositoExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmBaixa, dmBaixa);
  dmGerencial.CriarForm(TFLancamentosVincularDepositos, FLancamentosVincularDepositos);
  FreeAndNil(dmBaixa);
end;

procedure TFPrincipal.actCaixaMenuExecute(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmCaixa, dmCaixa);
  dmGerencial.CriarForm(TFFiltroCaixas, FFiltroCaixas);
  FreeAndNil(dmCaixa);

  HabilitarDesabilitarAcaoUsuario;
end;

procedure TFPrincipal.actCarneLeaoMenuExecute(Sender: TObject);
begin
  //pop-up
end;

procedure TFPrincipal.actNiveisLancamentoMenuExecute(Sender: TObject);
begin
  //pop-up
end;

procedure TFPrincipal.btnAbrirCalculadoraClick(Sender: TObject);
begin
  if WinExec('Calc.exe', SW_SHOWNORMAL) < 31 then
		ShowMessage('Erro ao abrir a calculadora. Por favor, tente novamente.');
end;

procedure TFPrincipal.btnLancPendentesClick(Sender: TObject);
begin
  if vgPrm_ConsDesp and
    (vgSituacaoCaixa = 'A') and
    (not UFRMAtuSis.Visible) then
  begin
    dmPrincipal.AtualizarLancamentosColaboradorRecorrentes;
    dmPrincipal.AtualizarLancamentosRecorrentes;

    UFRMLancPend.dtePeriodoIni.Date := StartOfTheYear(Date);
    UFRMLancPend.dtePeriodoFim.Date := EndOfTheYear(Date);
    UFRMLancPend.rgTipoLanc.ItemIndex := 0;
    UFRMLancPend.AbrirListaPendentes;

    UFRMLancPend.Visible := True;
  end
  else
    UFRMLancPend.Visible := False;
end;

procedure TFPrincipal.btnMenuAbaAjudaClick(Sender: TObject);
begin
  //Abre o arquivo PDF com as Dicas de Uso do Monitoramento
  try
    Application.CreateForm(TFConsultaArquivoPDF, FConsultaArquivoPDF);

    FConsultaArquivoPDF.sPathPDF           := vgDirExec + 'Dicas_Monitoramento.pdf';
    FConsultaArquivoPDF.lGuiaMonitoramento := True;

    FConsultaArquivoPDF.ShowModal;
  finally
    FConsultaArquivoPDF.Free;
  end;
end;

procedure TFPrincipal.btnMenuAbaAtalhosClick(Sender: TObject);
begin
  AbrirMenuBotoes(btnMenuAbaAtalhos);
end;

procedure TFPrincipal.btnMenuAbaConfiguracoesClick(Sender: TObject);
begin
  AbrirMenuBotoes(btnMenuAbaConfiguracoes);
end;

procedure TFPrincipal.btnMenuAbaEtiquetasClick(Sender: TObject);
begin
  AbrirMenuBotoes(btnMenuAbaEtiquetas);
end;

procedure TFPrincipal.btnMenuAbaFinanceiroClick(Sender: TObject);
begin
  AbrirMenuBotoes(btnMenuAbaFinanceiro);
end;

procedure TFPrincipal.btnMenuAbaFolhasClick(Sender: TObject);
begin
  AbrirMenuBotoes(btnMenuAbaFolhas);
end;

procedure TFPrincipal.btnMenuAbaOficiosClick(Sender: TObject);
begin
  AbrirMenuBotoes(btnMenuAbaOficios);
end;

procedure TFPrincipal.btnMenuAbaSuporteClick(Sender: TObject);
begin
  AbrirMenuBotoes(btnMenuAbaSuporte);
end;

procedure TFPrincipal.btnMenuAlertaClick(Sender: TObject);
begin
  if Application.MessageBox(PChar('Faltam ' + IntToStr(vgDiasPagamento) +
                                  ' dias para o Fechamento de Pagamentos.' + #13#10 +
                                  'Deseja realizar o fechamento das Comiss�es agora?'),
                            'Fechamento de Comiss�es',
                            MB_YESNO + MB_ICONQUESTION) = ID_YES then
    actCadFechamentoComissoes.Execute;
end;

procedure TFPrincipal.actRelEtiquetasMenuExecute(Sender: TObject);
begin
  //pop-up
end;

procedure TFPrincipal.actRelEtqEtiquetasExecute(Sender: TObject);
begin
  vgOperacao := P;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmRelatorios, dmRelatorios);
  dmGerencial.CriarForm(TFFiltroRelatorioEtiquetas, FFiltroRelatorioEtiquetas);
  FreeAndNil(dmRelatorios);
end;

procedure TFPrincipal.actRelEtqLoteEtiquetasExecute(Sender: TObject);
begin
  vgOperacao := P;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmRelatorios, dmRelatorios);
  dmGerencial.CriarForm(TFFiltroRelatorioLoteEtiqueta, FFiltroRelatorioLoteEtiqueta);
  FreeAndNil(dmRelatorios);
end;

procedure TFPrincipal.actRelExtratoContasExecute(Sender: TObject);
begin
  vgOperacao := P;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmRelatorios, dmRelatorios);
  dmGerencial.CriarForm(TFFiltroRelatorioExtratoContas, FFiltroRelatorioExtratoContas);
  FreeAndNil(dmRelatorios);
end;

procedure TFPrincipal.actRelAtosExecute(Sender: TObject);
begin
  vgOperacao := P;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmRelatorios, dmRelatorios);
  dmGerencial.CriarForm(TFFiltroRelatorioAtos, FFiltroRelatorioAtos);
  FreeAndNil(dmRelatorios);
end;

procedure TFPrincipal.actRelCReceberCPagarExecute(Sender: TObject);
begin
  vgOperacao := P;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmRelatorios, dmRelatorios);
  dmGerencial.CriarForm(TFFiltroRelatorioCReceberCPagar, FFiltroRelatorioCReceberCPagar);
  FreeAndNil(dmRelatorios);
end;

procedure TFPrincipal.actRelDespesasColaboradorExecute(Sender: TObject);
begin
  vgOperacao := P;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmRelatorios, dmRelatorios);
  dmGerencial.CriarForm(TFFiltroRelatorioDespesasColaboradores, FFiltroRelatorioDespesasColaboradores);
  FreeAndNil(dmRelatorios);
end;

procedure TFPrincipal.actRelDiferencaFechCaixaExecute(Sender: TObject);
begin
  vgOperacao := P;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmRelatorios, dmRelatorios);
  dmGerencial.CriarForm(TFFiltroRelatorioDiferencasFechamentoCaixa, FFiltroRelatorioDiferencasFechamentoCaixa);
  FreeAndNil(dmRelatorios);
end;

procedure TFPrincipal.actRelFinanceiroMenuExecute(Sender: TObject);
begin
  //pop-up
end;

procedure TFPrincipal.actRelFlsSegFolhasSegurancaExecute(Sender: TObject);
begin
  vgOperacao := P;
  vgOrigemCadastro := False;

  try
    Application.CreateForm(TdmRelatorios, dmRelatorios);
    Application.CreateForm(TFFiltroRelatorioFolha, FFiltroRelatorioFolha);
    FFiltroRelatorioFolha.lRCPN := False;
    FFiltroRelatorioFolha.ShowModal;
  finally
    FFiltroRelatorioFolha.Free;
    FreeAndNil(dmRelatorios);
  end;
end;

procedure TFPrincipal.actRelFlsSegFolhasSegurancaRCPNExecute(Sender: TObject);
begin
  vgOperacao := P;
  vgOrigemCadastro := False;

  try
    Application.CreateForm(TdmRelatorios, dmRelatorios);
    Application.CreateForm(TFFiltroRelatorioFolha, FFiltroRelatorioFolha);
    FFiltroRelatorioFolha.lRCPN := True;
    FFiltroRelatorioFolha.ShowModal;
  finally
    FFiltroRelatorioFolha.Free;
    FreeAndNil(dmRelatorios);
  end;
end;

procedure TFPrincipal.actRelFlsSegLoteFolhasSegurancaExecute(Sender: TObject);
begin
  vgOperacao := P;
  vgOrigemCadastro := False;

  try
    Application.CreateForm(TdmRelatorios, dmRelatorios);
    Application.CreateForm(TFFiltroRelatorioLote, FFiltroRelatorioLote);
    FFiltroRelatorioLote.lRCPN := False;
    FFiltroRelatorioLote.ShowModal;
  finally
    FFiltroRelatorioLote.Free;
    FreeAndNil(dmRelatorios);
  end;
end;

procedure TFPrincipal.actRelFlsSegLoteFolhasSegurancaRCPNExecute(
  Sender: TObject);
begin
  vgOperacao := P;
  vgOrigemCadastro := False;

  try
    Application.CreateForm(TdmRelatorios, dmRelatorios);
    Application.CreateForm(TFFiltroRelatorioLote, FFiltroRelatorioLote);
    FFiltroRelatorioLote.lRCPN := True;
    FFiltroRelatorioLote.ShowModal;
  finally
    FFiltroRelatorioLote.Free;
    FreeAndNil(dmRelatorios);
  end;
end;

procedure TFPrincipal.actRelFlutuantesExecute(Sender: TObject);
begin
  vgOperacao := P;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmRelatorios, dmRelatorios);
  dmGerencial.CriarForm(TFFiltroRelatorioFlutuantes, FFiltroRelatorioFlutuantes);
  FreeAndNil(dmRelatorios);
end;

procedure TFPrincipal.actRelFolhasMenuExecute(Sender: TObject);
begin
  //pop-up
end;

procedure TFPrincipal.actRelHistoricoRecibosExecute(Sender: TObject);
begin
  vgOperacao := P;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmRelatorios, dmRelatorios);
  dmGerencial.CriarForm(TFListaRecibos, FListaRecibos);
  FreeAndNil(dmRelatorios);
end;

procedure TFPrincipal.actRelImpostoRendaExecute(Sender: TObject);
begin
  vgOperacao := P;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmRelatorios, dmRelatorios);
  dmGerencial.CriarForm(TFFiltroRelatorioRecDespIR, FFiltroRelatorioRecDespIR);
  FreeAndNil(dmRelatorios);
end;

procedure TFPrincipal.actRelRecibo1ViaExecute(Sender: TObject);
begin
  vgOperacao := P;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmRelatorios, dmRelatorios);
  dmGerencial.CriarForm(TFFiltroRelatorioRecibo, FFiltroRelatorioRecibo);
  FreeAndNil(dmRelatorios);
end;

procedure TFPrincipal.actRelRecibosCanceladosExecute(Sender: TObject);
begin
  vgOperacao := P;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmRelatorios, dmRelatorios);
  dmGerencial.CriarForm(TFFiltroRelatorioRecibosCancelados, FFiltroRelatorioRecibosCancelados);
  FreeAndNil(dmRelatorios);
end;

procedure TFPrincipal.actRelMovimentosExecute(Sender: TObject);
begin
  vgOperacao := P;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmRelatorios, dmRelatorios);
  dmGerencial.CriarForm(TFFiltroRelatorioMovimentoCaixa, FFiltroRelatorioMovimentoCaixa);
  FreeAndNil(dmRelatorios);
end;

procedure TFPrincipal.actRelFechamentoCaixaFPExecute(Sender: TObject);
begin
  vgOperacao := P;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmRelatorios, dmRelatorios);
  dmGerencial.CriarForm(TFFiltroRelatorioFechamentoCaixa, FFiltroRelatorioFechamentoCaixa);
  FreeAndNil(dmRelatorios);
end;

procedure TFPrincipal.actRelFechamentoCaixaTAExecute(Sender: TObject);
begin
  vgOperacao := P;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmRelatorios, dmRelatorios);
  dmGerencial.CriarForm(TFFiltroRelatorioFechamentoCaixaAtos, FFiltroRelatorioFechamentoCaixaAtos);
  FreeAndNil(dmRelatorios);
end;

procedure TFPrincipal.actRelRepassesExecute(Sender: TObject);
begin
  vgOperacao := P;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmRelatorios, dmRelatorios);
  dmGerencial.CriarForm(TFFiltroRelatorioRepasses, FFiltroRelatorioRepasses);
  FreeAndNil(dmRelatorios);
end;

procedure TFPrincipal.actRepasseRecMenuExecute(Sender: TObject);
begin
  //pop-up
end;

procedure TFPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  BS.GravarUsuarioLog(100, '', 'Usu�rio saiu do Sistema');

  Application.Terminate;
end;

procedure TFPrincipal.FormCreate(Sender: TObject);
begin
  ntbMenuBotoes.PageIndex := 0;

  vgDiasPagamento := 0;
  vgDiasPagamento := DaysBetween(dmPrincipal.CalcularDataPagamentoFunc, Date);

  UFRMAtuSis       := TFFRMAtualizacaoSistema.Create(nil);
  UFRMAtuSis.Color := vgCorSistema;

  UFRMLancPend := TFFRMLancamentosPendentes.Create(nil);
  UFRMLancPend.Parent := Self;

  dmGerencial.AtualizarFeriados;
end;

procedure TFPrincipal.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    Self.Close;
    Application.Terminate;
  end;
end;

procedure TFPrincipal.FormShow(Sender: TObject);
begin
  //Exibe nome da Serventia
  FPrincipal.Caption := 'TOTAL MONITORAMENTO - ' + UpperCase(vgConf_NomeServentia) + '     ';

  { Verifica se a mensagem de atualizacao ja foi exibita e marcada como vista,
    caso ainda nao vista ou persistida, exibe a mensagem }
  if Trim(vgConf_FlgExibeAtualizacao) = 'S' then
  begin
    UFRMAtuSis.mmAtualizacoes.Clear;

    if Trim(vgUsu_FlgVisualizouAtu) = 'N' then
    begin
      dmPrincipal.qryModulo.Close;
      dmPrincipal.qryModulo.Open;

      dmPrincipal.qryModulo.First;

      while not dmPrincipal.qryModulo.Eof do
      begin
        with dmPrincipal.qryAtualizacaoDet do
        begin
          Close;
          Params.ParamByName('IdAtualizacao').Value := vgAtS_IdAtualizacaoSist;
          Params.ParamByName('IdModulo').Value      := dmPrincipal.qryModulo.FieldByName('ID_MODULO').AsInteger;
          Open;

          if dmPrincipal.qryAtualizacaoDet.RecordCount > 0 then
          begin
            First;

            UFRMAtuSis.mmAtualizacoes.Lines.Add('=======================================================================');
            UFRMAtuSis.mmAtualizacoes.Lines.Add('  ' + FieldByName('DESCR_TIPO_ATUALIZACAO').AsString);
            UFRMAtuSis.mmAtualizacoes.Lines.Add('=======================================================================' + #13#10);
            UFRMAtuSis.mmAtualizacoes.Lines.Add('>> ' + FieldByName('DESCR_MODULO').AsString + ':');

            while not Eof do
            begin
              UFRMAtuSis.mmAtualizacoes.Lines.Add('- ' + FieldByName('DESCR_ATUALIZACAO').AsString);
              Next;
            end;

            UFRMAtuSis.mmAtualizacoes.Lines.Add(#13#10);
          end;
        end;

        dmPrincipal.qryModulo.Next;
      end;
    end;
  end;

  //Mensagem de Atualizacao
  if (Trim(vgConf_FlgExibeAtualizacao) = 'N') or
    ((Trim(vgConf_FlgExibeAtualizacao) = 'S') and
     (Trim(vgUsu_FlgVisualizouAtu) = 'S')) then
    UFRMAtuSis.Visible := False
  else
    UFRMAtuSis.Visible := True;

  //Lancamentos Pendentes
  if vgPrm_ConsDesp and
    (vgSituacaoCaixa = 'A') and
    (not UFRMAtuSis.Visible) then
  begin
    dmPrincipal.AtualizarLancamentosColaboradorRecorrentes;
    dmPrincipal.AtualizarLancamentosRecorrentes;

    UFRMLancPend.dtePeriodoIni.Date := StartOfTheYear(Date);
    UFRMLancPend.dtePeriodoFim.Date := EndOfTheYear(Date);
    UFRMLancPend.rgTipoLanc.ItemIndex := 0;
    UFRMLancPend.AbrirListaPendentes;

    UFRMLancPend.Visible := True;
  end
  else
    UFRMLancPend.Visible := False;

  HabilitarDesabilitarModuloCliente;

  lblUsuarioLogado.Caption := 'Usu�rio: ' + vgUsu_Nome + '     ';
  lblVersaoSistema.Caption := 'Vers�o Sistema: ' + vgAtS_VersaoSistema + '     ';

  { SINCRONIZACAO }
  //Caso o sistema SYNC_MONITORAMENTO nao esteja sendo executado, iniciar execucao
  if (not dmPrincipal.ProcessoSistemaAtivo('SYNC_MONITORAMENTO.exe')) and
    (not lAtualizandoVersao) then
  begin
    if FileExists(vgDirExec + 'SYNC_MONITORAMENTO.exe') then
      ShellExecute(Handle,
                   'open',
                   PChar(vgDirExec + 'SYNC_MONITORAMENTO.exe'),
                   nil,
                   nil,
                   SW_SHOWNORMAL);
  end;

  HabilitarDesabilitarAcaoUsuario;
end;

procedure TFPrincipal.HabilitarDesabilitarAcaoUsuario;
var
  lPularFiltro: Boolean;
begin
  if vgUsu_FlgSuporte = 'S' then
  begin
    vgMod_Financeiro    := True;
    vgMod_Oficios       := True;
    vgMod_Relatorios    := True;
    vgMod_Configuracoes := True;
  end;

  if vgOrigFechPend then
    lPularFiltro := True
  else
    lPularFiltro := False;

  vgOrigFechPend := False;

(*  if (vgDiasPagamento = 0) and
    (dmPrincipal.FechamentoPagamentosPendente) and
    (vgConf_FlgBloqueiaFechPagto = 'S') then
  begin
     { CADASTROS }
    actFiltroLancamento.Enabled        := False;
    actCadReceita.Enabled              := False;
    actCadDespesa.Enabled              := False;
    actTabelasAuxiliaresMenu.Enabled   := False;
    actCadCategoriaReceitas.Enabled    := False;
    actCadCategoriaDespesas.Enabled    := False;
    actCadSubcatReceitas.Enabled       := False;
    actCadSubcatDespesas.Enabled       := False;
    N2.Visible                         := (Trim(vgUsu_FlgSuporte) = 'S');
    actCadFormaPagto.Enabled           := False;
    FormasdePagamento1.Visible         := (Trim(vgUsu_FlgSuporte) = 'S');
    actCadFornecedores.Enabled         := False;
    actCadSubcatReceitas.Enabled       := False;
    actCadSubcatDespesas.Enabled       := False;
    actCadClientes.Enabled             := False;
    actProdutosMenu.Enabled            := False;
    actCadProdutos.Enabled             := False;
    actCadServicos.Enabled             := False;
    actCaixaMenu.Enabled               := False;
    actCarneLeaoMenu.Enabled           := False;
    actCadRelacaoPlanoContas.Enabled   := False;
    actCadCarneLeao.Enabled            := False;
    actFlutuantesMenu.Enabled          := False;
    actCadDepositoFlutuante.Enabled    := False;
    actBaixaMenu.Enabled               := False;
    actCadBaixaUmFlutuante.Enabled     := False;
    actCadBaixaGpFlutuantes.Enabled    := False;
    actColaboradoresMenu.Enabled       := vgMod_Financeiro and vgPrm_ConsFunc;
    actCadColaboradores.Enabled        := vgMod_Financeiro and vgPrm_ConsFunc;
    actCadVales.Enabled                := vgMod_Financeiro and vgPrm_ConsVale;
    actCadOutrasDespesas.Enabled       := vgMod_Financeiro and vgPrm_ConsODespF;
    actCadFechamentoComissoes.Enabled  := (vgMod_Financeiro and
                                           vgPrm_RealFecCom and
                                           (vgConf_FlgTrabalhaComissao = 'S') and
                                           dmPrincipal.ExistemReceitasPeriodo);
    actCadFechamentoPagtoFuncs.Enabled := (vgMod_Financeiro and
                                           vgPrm_RealFecSal and
                                           ((vgConf_FlgTrabalhaComissao = 'N') or
                                            ((vgConf_FlgTrabalhaComissao = 'S') and
                                            not dmPrincipal.FechamentoComissoesPendente)));

    { OFICIOS }
    actFiltroOficio.Enabled  := False;
    actOficRecebidos.Enabled := False;
    actOficEnviados.Enabled  := False;

    { FOLHAS }
    actFiltroLoteFolhasMenu.Enabled             := False;
    actFiltroLoteFolhaSeguranca.Enabled         := False;
    actFiltroLoteFolhaRCPN.Enabled              := False;
    actCadLoteFolhaSeguranca.Enabled            := False;
    actCadLoteFolhaRCPN.Enabled                 := False;
    actFiltroFolhaSeguranca.Enabled             := False;
    actFiltroFolhaRCPN.Enabled                  := False;
    actRelFolhasMenu.Enabled                    := False;
    actRelFlsSegLoteFolhasSeguranca.Enabled     := False;
    actRelFlsSegLoteFolhasSegurancaRCPN.Enabled := False;
    actRelFlsSegFolhasSeguranca.Enabled         := False;
    actRelFlsSegFolhasSegurancaRCPN.Enabled     := False;

    { ETIQUETAS }
    actFiltroLoteEtiquetas.Enabled := False;
    actCadLoteEtiqueta.Enabled     := False;
    actCadEtiqueta.Enabled         := False;
    actRelEtiquetasMenu.Enabled    := False;
    actRelEtqLoteEtiquetas.Enabled := False;
    actRelEtqEtiquetas.Enabled     := False;

    { RELATORIOS }
    actRelFinanceiroMenu.Enabled      := False;
    actRelCReceberCPagar.Enabled      := False;
    actRelFechamentoCaixaFP.Enabled   := False;
    actRelFechamentoCaixaTA.Enabled   := False;
    actRelFaturamentoMensal.Enabled   := False;
    actRelMovimentos.Enabled          := False;
    actRelDiferencaFechCaixa.Enabled  := False;
    actRelImpostoRenda.Enabled        := False;
    actRelExtratoContas.Enabled       := False;
    actRelAtos.Enabled                := False;
    actRelFlutuantes.Enabled          := False;
    actRelRepasses.Enabled            := False;
    actRelDespesasColaborador.Enabled := False;
    actRelInadimplencias.Enabled      := False;
    actRelRecibo1Via.Enabled          := False;
    actRelHistoricoRecibos.Enabled    := False;
    actRelRecibosCancelados.Enabled   := False;
    actRelRecibosSelos.Enabled        := False;

    { CONFIGURACOES }
    actConfigCadUsuarios.Enabled   := False;
    actConfigCadPermissoes.Enabled := False;
    actConfigFiltroLog.Enabled     := False;
    actConfigSistema.Enabled       := False;
    actConfigAtribuicoes.Enabled   := False;

    { SUPORTE }
    actSupArquivoIni.Enabled   := False;
    actSupBackup.Enabled       := False;
    actSupCriptografia.Enabled := False;
    actSupSQL.Enabled          := False;

    Application.MessageBox(PChar('Hoje � o �ltimo dia para realizar Fechamento de ' +
                                 'Pagamento dos Colaboradores e as demais funcionalidades ' +
                                 ' do sistema somente ser�o liberadas ap�s a realiza��o ' +
                                 'desse Fechamento.'),
                           'AVISO',
                           MB_OK + MB_ICONWARNING);

    if actCadFechamentoComissoes.Enabled then
    begin
      vgOrigFechPend := True;

      if not lPularFiltro then
        actCadFechamentoComissoes.Execute;
    end
    else if actCadFechamentoPagtoFuncs.Enabled then
    begin
      vgOrigFechPend := True;

      if not lPularFiltro then
        actCadFechamentoPagtoFuncs.Execute;
    end;
  end
  else
  begin   *)

  if vgSituacaoCaixa = 'F' then
  begin
    { CADASTROS }
    actFiltroLancamento.Enabled         := False;
    btnAtalhoReceitas.Enabled           := False;
    btnAtalhoDespesas.Enabled           := False;
    btnFinanceiroReceitas.Enabled       := False;
    btnFinanceiroDespesas.Enabled       := False;
    ppmiRepasseReceita.Enabled          := False;
    btnLancPendentes.Enabled            := False;
    actCadReceita.Enabled               := False;
    actCadDespesa.Enabled               := False;
    actCadFornecedores.Enabled          := vgMod_Financeiro and vgPrm_ConsForn;
    actCadSubcatReceitas.Enabled        := vgMod_Financeiro and vgPrm_ConsRec;
    actCadSubcatDespesas.Enabled        := vgMod_Financeiro and vgPrm_ConsDesp;
    actCadClientes.Enabled              := vgMod_Financeiro and vgPrm_ConsCli;
    actProdutosMenu.Enabled             := vgMod_Financeiro and (vgPrm_ConsProd or vgPrm_ConsServ);
    actCadProdutos.Enabled              := vgMod_Financeiro and vgPrm_ConsProd;
    actCadServicos.Enabled              := vgMod_Financeiro and vgPrm_ConsServ;
    actCaixaMenu.Enabled                := vgMod_Financeiro and
                                           (vgPrm_ConsAbCx or
                                            vgPrm_ConsFecCx or
                                            vgPrm_ConsMovCx);
    actCarneLeaoMenu.Enabled            := vgMod_Financeiro and
                                           (vgPrm_ConsCLeao or
                                            vgPrm_ConsRelacCL);
    actCadRelacaoPlanoContas.Enabled    := vgMod_Financeiro and vgPrm_ConsCLeao;
    actCadCarneLeao.Enabled             := False;
    actRepasseRecMenu.Enabled           := False;
    actDepositosFlutuantesMenu.Enabled  := False;
    actCadDepositoFlutuante.Enabled     := False;
    actCadVinculoLancDeposito.Enabled   := False;
    actCadBaixaUmFlutuante.Enabled      := False;
    actCadBaixaGpFlutuantes.Enabled     := False;
    actColaboradoresMenu.Enabled        := False;
    actCadColaboradores.Enabled         := False;
    actCadVales.Enabled                 := False;
    actCadOutrosLancamentos.Enabled     := False;
    actCadFechamentoComissoes.Enabled   := False;
    actCadFechamentoPagtoFuncs.Enabled  := False;

    { OFICIOS }
    actFiltroOficio.Enabled  := vgMod_Oficios and vgPrm_ConsOficRec and vgPrm_ConsOficEnv;
    actOficRecebidos.Enabled := vgMod_Oficios and vgPrm_ConsOficRec;
    actOficEnviados.Enabled  := vgMod_Oficios and vgPrm_ConsOficEnv;

    { FOLHAS }
    actFiltroLoteFolhasMenu.Enabled             := vgMod_Folhas;
    actFiltroLoteFolhaSeguranca.Enabled         := vgMod_Folhas and vgPrm_ConsLtFlsSeg;
    actFiltroLoteFolhaRCPN.Enabled              := vgMod_Folhas and vgPrm_ConsLtFlsRCPN;
    actCadLoteFolhaSeguranca.Enabled            := vgMod_Folhas and vgPrm_IncLtFlsSeg;
    actCadLoteFolhaRCPN.Enabled                 := vgMod_Folhas and vgPrm_IncLtFlsRCPN;
    actFiltroFolhaSeguranca.Enabled             := vgMod_Folhas and vgPrm_ConsFlsSeg;
    actFiltroFolhaRCPN.Enabled                  := vgMod_Folhas and vgPrm_ConsFlsRCPN;
    actRelFolhasMenu.Enabled                    := vgMod_Folhas and (vgPrm_ImpLtFlsSeg or
                                                                     vgPrm_ImpLtFlsRCPN or
                                                                     vgPrm_ImpFlsSeg or
                                                                     vgPrm_ImpFlsRCPN);
    actRelFlsSegLoteFolhasSeguranca.Enabled     := vgMod_Folhas and vgPrm_ImpLtFlsSeg;
    actRelFlsSegLoteFolhasSegurancaRCPN.Enabled := vgMod_Folhas and vgPrm_ImpLtFlsRCPN;
    actRelFlsSegFolhasSeguranca.Enabled         := vgMod_Folhas and vgPrm_ImpFlsSeg;
    actRelFlsSegFolhasSegurancaRCPN.Enabled     := vgMod_Folhas and vgPrm_ImpFlsRCPN;

    { ETIQUETAS }
    actFiltroLoteEtiquetas.Enabled := vgMod_Etiquetas;
    actCadLoteEtiqueta.Enabled     := vgMod_Etiquetas and vgPrm_ConsLtEtq;
    actCadEtiqueta.Enabled         := vgMod_Etiquetas and vgPrm_ConsEtq;
    actRelEtiquetasMenu.Enabled    := vgMod_Etiquetas and (vgPrm_ImpLtEtq or vgPrm_ImpEtq);
    actRelEtqLoteEtiquetas.Enabled := vgMod_Etiquetas and vgPrm_ImpLtEtq;
    actRelEtqEtiquetas.Enabled     := vgMod_Etiquetas and vgPrm_ImpEtq;

    { RELATORIOS }
    actRelFinanceiroMenu.Enabled      := vgMod_Relatorios;
    actRelCReceberCPagar.Enabled      := vgMod_Relatorios and vgPrm_ConsRelRecDesp;
    actRelFechamentoCaixaFP.Enabled   := vgMod_Relatorios and vgPrm_ConsRelFechFP;
    actRelFechamentoCaixaTA.Enabled   := vgMod_Relatorios and vgPrm_ConsRelFechTA;
    actRelFaturamentoMensal.Enabled   := vgMod_Relatorios and vgPrm_ConsRelFatMsl;
    actRelMovimentos.Enabled          := vgMod_Relatorios and vgPrm_ConsRelMovCx;
    actRelDiferencaFechCaixa.Enabled  := vgMod_Relatorios and vgPrm_ConsRelDifFechCx;
    actRelImpostoRenda.Enabled        := vgMod_Relatorios and vgPrm_ConsRelRecDespIR;
    actRelExtratoContas.Enabled       := vgMod_Relatorios and vgPrm_ConsRelExtCnt;
    actRelAtos.Enabled                := vgMod_Relatorios and vgPrm_ConsRelAtos;
    actRelFlutuantes.Enabled          := vgMod_Relatorios and vgPrm_ConsRelDepFlu;
    actRelRepasses.Enabled            := vgMod_Relatorios and vgPrm_ConsRelRep;
    actRelDespesasColaborador.Enabled := vgMod_Relatorios and vgPrm_ConsRelDespCol;
    actRelInadimplencias.Enabled      := vgMod_Relatorios and vgPrm_ConsRelInad;
    actRelRecibo1Via.Enabled          := vgMod_Relatorios and vgPrm_ConsRelRcb;
    actRelHistoricoRecibos.Enabled    := vgMod_Relatorios and vgPrm_ConsRelRcb;
    actRelRecibosCancelados.Enabled   := vgMod_Relatorios and vgPrm_ConsRelCancel;
    actRelRecibosSelos.Enabled        := vgMod_Relatorios and vgPrm_ConsRelRecbSelo;

    { CONFIGURACOES }
    actConfigCadUsuarios.Enabled   := vgMod_Configuracoes and vgPrm_ConsUsu;
    actConfigCadPermissoes.Enabled := vgMod_Configuracoes and vgPrm_ConsPerm;
    actConfigFiltroLog.Enabled     := vgMod_Configuracoes and vgPrm_ConsLogUsu;
    actConfigSistema.Enabled       := vgMod_Configuracoes and vgPrm_ConsConfigSis;
    actConfigAtribuicoes.Enabled   := vgMod_Configuracoes and vgPrm_ConsAtribServ;

    actCadFeriado.Enabled           := vgMod_Financeiro and vgMod_Configuracoes and vgPrm_ConsFer;
    actCadCargo.Enabled             := vgMod_Financeiro and vgMod_Configuracoes and vgPrm_ConsCrg;
    actCadFormaPagto.Enabled        := (Trim(vgUsu_FlgSuporte) = 'S');
    btnFormaPagamento.Visible       := (Trim(vgUsu_FlgSuporte) = 'S');
    actNiveisLancamentoMenu.Enabled := vgMod_Financeiro and vgMod_Configuracoes;
    actCadCategoriaReceitas.Enabled := vgMod_Financeiro and vgMod_Configuracoes and vgPrm_ConsCatRec;
    actCadCategoriaDespesas.Enabled := vgMod_Financeiro and vgMod_Configuracoes and vgPrm_ConsCatDesp;
    actCadSubcatReceitas.Enabled    := vgMod_Financeiro and vgMod_Configuracoes and vgPrm_ConsSubcatRec;
    actCadSubcatDespesas.Enabled    := vgMod_Financeiro and vgMod_Configuracoes and vgPrm_ConsSubcatDesp;

    actConfigCadEmails.Visible := vgMod_Configuracoes and vgPrm_ConsRemDest;

    { SUPORTE }
    actSupArquivoIni.Enabled          := vgUsu_FlgSuporte = 'S';
    actSupBackup.Enabled              := vgUsu_FlgSuporte = 'S';
    actSupCriptografia.Enabled        := vgUsu_FlgSuporte = 'S';
    actSupSQL.Enabled                 := vgUsu_FlgSuporte = 'S';
    JvTransparentButton29.Enabled     := False;
    actSupAtualizarBancos.Enabled     := vgUsu_FlgSuporte = 'S';
    actSupDownloadBancoM.Enabled      := vgUsu_FlgSuporte = 'S';
    actSupAtualizarRelatorios.Enabled := vgUsu_FlgSuporte = 'S';
    actSupBaixarHomologacao.Enabled   := vgUsu_FlgSuporte = 'S';
    actSupVoltarVersaoSistema.Enabled := vgUsu_FlgSuporte = 'S';
  end
  else
  begin
    { CADASTROS }
    actFiltroLancamento.Enabled         := vgMod_Financeiro and vgPrm_ConsRec and vgPrm_ConsDesp;
    btnAtalhoReceitas.Enabled           := vgMod_Financeiro and (vgPrm_ConsRec or vgPrm_ConsDepo);
    btnAtalhoDespesas.Enabled           := vgMod_Financeiro and (vgPrm_ConsDesp or vgPrm_BxLanc);
    btnFinanceiroReceitas.Enabled       := vgMod_Financeiro and (vgPrm_ConsRec or vgPrm_ConsDepo);
    btnFinanceiroDespesas.Enabled       := vgMod_Financeiro and (vgPrm_ConsDesp or vgPrm_BxLanc);
    ppmiRepasseReceita.Enabled          := vgMod_Financeiro and vgPrm_BxLanc;
    btnLancPendentes.Enabled            := vgMod_Financeiro and vgPrm_ConsRec and vgPrm_ConsDesp;
    actCadReceita.Enabled               := vgMod_Financeiro and vgPrm_ConsRec;
    actCadDespesa.Enabled               := vgMod_Financeiro and vgPrm_ConsDesp;
    actCadFornecedores.Enabled          := vgMod_Financeiro and vgPrm_ConsForn;
    actCadSubcatReceitas.Enabled        := vgMod_Financeiro and vgPrm_ConsRec;
    actCadSubcatDespesas.Enabled        := vgMod_Financeiro and vgPrm_ConsDesp;
    actCadClientes.Enabled              := vgMod_Financeiro and vgPrm_ConsCli;
    actProdutosMenu.Enabled             := vgMod_Financeiro and (vgPrm_ConsProd or vgPrm_ConsServ);
    actCadProdutos.Enabled              := vgMod_Financeiro and vgPrm_ConsProd;
    actCadServicos.Enabled              := vgMod_Financeiro and vgPrm_ConsServ;
    actCaixaMenu.Enabled                := vgMod_Financeiro and
                                           (vgPrm_ConsAbCx or
                                            vgPrm_ConsFecCx or
                                            vgPrm_ConsMovCx);
    actCarneLeaoMenu.Enabled            := vgMod_Financeiro and
                                           (vgPrm_ConsCLeao or
                                            vgPrm_ConsRelacCL);
    actCadRelacaoPlanoContas.Enabled    := vgMod_Financeiro and vgPrm_ConsCLeao;
    actCadCarneLeao.Enabled             := vgMod_Financeiro and vgPrm_ConsRelacCL;
    actRepasseRecMenu.Enabled           := vgMod_Financeiro and (vgPrm_ConsDepo or vgPrm_BxLanc);

    actDepositosFlutuantesMenu.Enabled  := vgMod_Financeiro and vgPrm_ConsDepo;
    actCadDepositoFlutuante.Enabled     := vgMod_Financeiro and vgPrm_ConsDepo;
    actCadVinculoLancDeposito.Enabled   := vgMod_Financeiro and vgPrm_ExpVincDepo;
    actCadBaixaUmFlutuante.Enabled      := vgMod_Financeiro and vgPrm_BxLanc;
    actCadBaixaGpFlutuantes.Enabled     := vgMod_Financeiro and vgPrm_BxLanc;
    actColaboradoresMenu.Enabled        := vgMod_Financeiro and vgPrm_ConsFunc;
    actCadColaboradores.Enabled         := vgMod_Financeiro and vgPrm_ConsFunc;
    actCadVales.Enabled                 := vgMod_Financeiro and vgPrm_ConsVale;
    actCadOutrosLancamentos.Enabled     := vgMod_Financeiro and vgPrm_ConsODespF;
    actCadFechamentoComissoes.Enabled   := vgPrm_RealFecCom; {(vgMod_Financeiro and
                                            vgPrm_RealFecCom and
                                            (vgConf_FlgTrabalhaComissao = 'S') and
                                            dmPrincipal.ExistemReceitasPeriodo and
                                            (vgDiasPagamento in [0, 1, 2, 3, 4, 5]) and
                                            dmPrincipal.FechamentoComissoesPendente); }
    actCadFechamentoPagtoFuncs.Enabled  := (vgMod_Financeiro and
                                            vgPrm_RealFecSal and
                                            ((vgConf_FlgTrabalhaComissao = 'N') or
                                             ((vgConf_FlgTrabalhaComissao = 'S') and
                                             not dmPrincipal.FechamentoComissoesPendente)));

    { OFICIOS }
    actFiltroOficio.Enabled  := vgMod_Oficios and vgPrm_ConsOficRec and vgPrm_ConsOficEnv;
    actOficRecebidos.Enabled := vgMod_Oficios and vgPrm_ConsOficRec;
    actOficEnviados.Enabled  := vgMod_Oficios and vgPrm_ConsOficEnv;

    { FOLHAS }
    actFiltroLoteFolhasMenu.Enabled             := vgMod_Folhas;
    actFiltroLoteFolhaSeguranca.Enabled         := vgMod_Folhas and vgPrm_ConsLtFlsSeg;
    actFiltroLoteFolhaRCPN.Enabled              := vgMod_Folhas and vgPrm_ConsLtFlsRCPN;
    actCadLoteFolhaSeguranca.Enabled            := vgMod_Folhas and vgPrm_IncLtFlsSeg;
    actCadLoteFolhaRCPN.Enabled                 := vgMod_Folhas and vgPrm_IncLtFlsRCPN;
    actFiltroFolhaSeguranca.Enabled             := vgMod_Folhas and vgPrm_ConsFlsSeg;
    actFiltroFolhaRCPN.Enabled                  := vgMod_Folhas and vgPrm_ConsFlsRCPN;
    actRelFolhasMenu.Enabled                    := vgMod_Folhas and (vgPrm_ImpLtFlsSeg or
                                                                     vgPrm_ImpLtFlsRCPN or
                                                                     vgPrm_ImpFlsSeg or
                                                                     vgPrm_ImpFlsRCPN);
    actRelFlsSegLoteFolhasSeguranca.Enabled     := vgMod_Folhas and vgPrm_ImpLtFlsSeg;
    actRelFlsSegLoteFolhasSegurancaRCPN.Enabled := vgMod_Folhas and vgPrm_ImpLtFlsRCPN;
    actRelFlsSegFolhasSeguranca.Enabled         := vgMod_Folhas and vgPrm_ImpFlsSeg;
    actRelFlsSegFolhasSegurancaRCPN.Enabled     := vgMod_Folhas and vgPrm_ImpFlsRCPN;

    { ETIQUETAS }
    actFiltroLoteEtiquetas.Enabled := vgMod_Etiquetas;
    actCadLoteEtiqueta.Enabled     := vgMod_Etiquetas and vgPrm_ConsLtEtq;
    actCadEtiqueta.Enabled         := vgMod_Etiquetas and vgPrm_ConsEtq;
    actRelEtiquetasMenu.Enabled    := vgMod_Etiquetas and (vgPrm_ImpLtEtq or vgPrm_ImpEtq);
    actRelEtqLoteEtiquetas.Enabled := vgMod_Etiquetas and vgPrm_ImpLtEtq;
    actRelEtqEtiquetas.Enabled     := vgMod_Etiquetas and vgPrm_ImpEtq;

    { RELATORIOS }
    actRelFinanceiroMenu.Enabled      := vgMod_Relatorios;
    actRelCReceberCPagar.Enabled      := vgMod_Relatorios and vgPrm_ConsRelRecDesp;
    actRelFechamentoCaixaFP.Enabled   := vgMod_Relatorios and vgPrm_ConsRelFechFP;
    actRelFechamentoCaixaTA.Enabled   := vgMod_Relatorios and vgPrm_ConsRelFechTA;
    actRelFaturamentoMensal.Enabled   := vgMod_Relatorios and vgPrm_ConsRelFatMsl;
    actRelMovimentos.Enabled          := vgMod_Relatorios and vgPrm_ConsRelMovCx;
    actRelDiferencaFechCaixa.Enabled  := vgMod_Relatorios and vgPrm_ConsRelDifFechCx;
    actRelImpostoRenda.Enabled        := vgMod_Relatorios and vgPrm_ConsRelRecDespIR;
    actRelExtratoContas.Enabled       := vgMod_Relatorios and vgPrm_ConsRelExtCnt;
    actRelAtos.Enabled                := vgMod_Relatorios and vgPrm_ConsRelAtos;
    actRelFlutuantes.Enabled          := vgMod_Relatorios and vgPrm_ConsRelDepFlu;
    actRelRepasses.Enabled            := vgMod_Relatorios and vgPrm_ConsRelRep;
    actRelDespesasColaborador.Enabled := vgMod_Relatorios and vgPrm_ConsRelDespCol;
    actRelInadimplencias.Enabled      := vgMod_Relatorios and vgPrm_ConsRelInad;
    actRelRecibo1Via.Enabled          := vgMod_Relatorios and vgPrm_ConsRelRcb;
    actRelHistoricoRecibos.Enabled    := vgMod_Relatorios and vgPrm_ConsRelRcb;
    actRelRecibosCancelados.Enabled   := vgMod_Relatorios and vgPrm_ConsRelCancel;
    actRelRecibosSelos.Enabled        := vgMod_Relatorios and vgPrm_ConsRelRecbSelo;

    { CONFIGURACOES }
    actConfigCadUsuarios.Enabled   := vgMod_Configuracoes and vgPrm_ConsUsu;
    actConfigCadPermissoes.Enabled := vgMod_Configuracoes and vgPrm_ConsPerm;
    actConfigFiltroLog.Enabled     := vgMod_Configuracoes and vgPrm_ConsLogUsu;
    actConfigSistema.Enabled       := vgMod_Configuracoes and vgPrm_ConsConfigSis;
    actConfigAtribuicoes.Enabled   := vgMod_Configuracoes and vgPrm_ConsAtribServ;

    actCadFeriado.Enabled           := vgMod_Financeiro and vgMod_Configuracoes and vgPrm_ConsFer;
    actCadCargo.Enabled             := vgMod_Financeiro and vgMod_Configuracoes and vgPrm_ConsCrg;
    actCadFormaPagto.Enabled        := (Trim(vgUsu_FlgSuporte) = 'S');
    btnFormaPagamento.Visible       := (Trim(vgUsu_FlgSuporte) = 'S');
    actNiveisLancamentoMenu.Enabled := vgMod_Financeiro and vgMod_Configuracoes;
    actCadCategoriaReceitas.Enabled := vgMod_Financeiro and vgMod_Configuracoes and vgPrm_ConsCatRec;
    actCadCategoriaDespesas.Enabled := vgMod_Financeiro and vgMod_Configuracoes and vgPrm_ConsCatDesp;
    actCadSubcatReceitas.Enabled    := vgMod_Financeiro and vgMod_Configuracoes and vgPrm_ConsSubcatRec;
    actCadSubcatDespesas.Enabled    := vgMod_Financeiro and vgMod_Configuracoes and vgPrm_ConsSubcatDesp;

    actConfigCadEmails.Visible := vgMod_Configuracoes and vgPrm_ConsRemDest;

    { SUPORTE }
    actSupArquivoIni.Enabled          := vgUsu_FlgSuporte = 'S';
    actSupBackup.Enabled              := vgUsu_FlgSuporte = 'S';
    actSupCriptografia.Enabled        := vgUsu_FlgSuporte = 'S';
    actSupSQL.Enabled                 := vgUsu_FlgSuporte = 'S';
    JvTransparentButton29.Enabled     := False;
    actSupAtualizarBancos.Enabled     := vgUsu_FlgSuporte = 'S';
    actSupDownloadBancoM.Enabled      := vgUsu_FlgSuporte = 'S';
    actSupAtualizarRelatorios.Enabled := vgUsu_FlgSuporte = 'S';
    actSupBaixarHomologacao.Enabled   := vgUsu_FlgSuporte = 'S';
    actSupVoltarVersaoSistema.Enabled := vgUsu_FlgSuporte = 'S';
  end;

  case AnsiIndexStr(UpperCase(vgSituacaoCaixa), ['N', 'A', 'F']) of
    0: lblCaixaVigente.Visible := False;
    1:
    begin
      lblCaixaVigente.Visible := True;
      lblCaixaVigente.Caption := 'Caixa Aberto: ' + FormatDateTime('DD/MM/YYYY', vgDataLancVigente) + '     ';
      lblCaixaVigente.Hint    := 'Qualquer Lan�amento ou Pagamento para o Caixa dever� ser feito APENAS com data MAIOR OU IGUAL que essa (' +
                                  FormatDateTime('DD/MM/YYYY', vgDataLancVigente) + '.)';
    end;
    2:
    begin
      lblCaixaVigente.Visible := True;
      lblCaixaVigente.Caption := '�lt. Caixa Fechado: ' + FormatDateTime('DD/MM/YYYY', vgDataUltFech) + '     ';
      lblCaixaVigente.Hint    := 'Qualquer Lan�amento ou Pagamento para o Caixa dever� ser feito APENAS com data MAIOR que essa (' +
                                 FormatDateTime('DD/MM/YYYY', vgDataUltFech) + ').';
    end;
  end;
end;

procedure TFPrincipal.HabilitarDesabilitarModuloCliente;
begin
  btnMenuAbaAtalhos.Visible       := True;
  btnMenuAbaFinanceiro.Visible    := vgMod_Financeiro or (vgUsu_FlgSuporte = 'S');
  btnMenuAbaFolhas.Visible        := vgMod_Folhas or (vgUsu_FlgSuporte = 'S');
  btnMenuAbaEtiquetas.Visible     := vgMod_Etiquetas or (vgUsu_FlgSuporte = 'S');
  btnMenuAbaOficios.Visible       := vgMod_Oficios or (vgUsu_FlgSuporte = 'S');
  btnMenuAbaConfiguracoes.Visible := vgMod_Configuracoes or (vgUsu_FlgSuporte = 'S');

  { Exibe a aba de SUPORTE somente se for o login administrativo
    da Total Sistemas }
  btnMenuAbaSuporte.Visible := vgUsu_FlgSuporte = 'S';

  //Exibe ou oculta o aviso de fechamento de comissoes
  if vgConf_FlgTrabalhaComissao = 'S' then
  begin
    if (vgDiasPagamento in [0, 1, 2, 3, 4, 5]) and
      (dmPrincipal.ExistemReceitasPeriodo) and
      (dmPrincipal.FechamentoComissoesPendente) then
    begin
      btnMenuAlerta.Visible := True;
      btnMenuAlerta.Click;
    end
    else
      btnMenuAlerta.Visible := False;
  end
  else
    btnMenuAlerta.Visible := False;
end;

procedure TFPrincipal.btnFinanceiroReceitasClick(Sender: TObject);
begin
  //pop-up
end;

procedure TFPrincipal.btnFinanceiroDespesasClick(Sender: TObject);
begin
  //pop-up
end;

procedure TFPrincipal.JvTransparentButton29Click(Sender: TObject);
begin
{  dmGerencial.Processando(True, 'upx');

  if not DirectoryExists(vgDirExec + 'APP') then
    ForceDirectories(vgDirExec + '\APP');

  ShellExecuteAndWait('OPEN',PChar('cmd.exe'),
                      PWideChar('/C "C:\upx391w\upx" ' +
                      vgDirExec + ExtractFileName(Application.ExeName)),
                      '',SW_SHOWNORMAL,True);

  Sleep(3000);

  CopyFile(PChar(Application.ExeName),PChar(vgDirExec + 'APP\' +
                                            ExtractFileName(Application.ExeName)), False);

  dmGerencial.Processando(False);  }
end;

procedure TFPrincipal.btnAtalhoReceitasClick(Sender: TObject);
begin
  //pop-up
end;

procedure TFPrincipal.btnAtalhoDespesasClick(Sender: TObject);
begin
  //pop-up
end;

procedure TFPrincipal.MenuItem1Click(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroFolhaSeguranca,FFiltroFolhaSeguranca);
end;

procedure TFPrincipal.MenuItem2Click(Sender: TObject);
begin
  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  dmGerencial.CriarForm(TFFiltroFolhaRCPN,FFiltroFolhaRCPN);
end;

procedure TFPrincipal.Panel3CanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  //Atualizacaoes do Sistema
  UFRMAtuSis.Left := Round((NewWidth - UFRMAtuSis.Width) / 2);

  if NewHeight < UFRMAtuSis.Height then
    UFRMAtuSis.Height := NewHeight
  else
    UFRMAtuSis.Height := 380;

  UFRMAtuSis.Top := Round((NewHeight - UFRMAtuSis.Height) / 2);

  UFRMAtuSis.Parent := Panel3;

  //Lancamentos Pendentes
  UFRMLancPend.Left := Round((NewWidth - UFRMLancPend.Width) / 2);

  if NewHeight < UFRMLancPend.Height then
    UFRMLancPend.Height := NewHeight
  else
    UFRMLancPend.Height := 380;

  UFRMLancPend.Top := Round((NewHeight - UFRMLancPend.Height) / 2);

  ShowScrollBar(UFRMLancPend.dbgLancPend.Handle, SB_HORZ, False);
end;

function TFPrincipal.ShellExecuteAndWait(Operation, FileName, Parameter,
  Directory: String; Show: Word; bWait: Boolean): Longint;
var
  bOK: Boolean;
  Info: TShellExecuteInfo;
begin
  FillChar(Info, SizeOf(Info), Chr(0));
  Info.cbSize := SizeOf(Info);
  Info.fMask := SEE_MASK_NOCLOSEPROCESS;
  Info.lpVerb := PChar(Operation);
  Info.lpFile := PChar(FileName);
  Info.lpParameters := PChar(Parameter);
  Info.lpDirectory := PChar(Directory);
  Info.nShow := Show;
  bOK := Boolean(ShellExecuteEx(@Info));

  if bOK then
  begin
    if bWait then
    begin
      while WaitForSingleObject(Info.hProcess, 100) = WAIT_TIMEOUT do
        Application.ProcessMessages;
        bOK := GetExitCodeProcess(Info.hProcess, DWORD(Result));
      end
    else
      Result := 0;
    end;
  if not bOK then
  Result := -1;
end;

end.
