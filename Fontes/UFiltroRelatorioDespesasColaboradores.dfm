inherited FFiltroRelatorioDespesasColaboradores: TFFiltroRelatorioDespesasColaboradores
  Caption = 'Relat'#243'rio de Despesas com Colaboradores'
  ClientHeight = 276
  ClientWidth = 566
  ExplicitWidth = 572
  ExplicitHeight = 305
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlFiltro: TPanel
    Width = 560
    Height = 270
    ExplicitWidth = 560
    ExplicitHeight = 270
    object lblPeriodo: TLabel [0]
      Left = 5
      Top = 8
      Width = 125
      Height = 14
      Caption = 'Per'#237'odo do Pagamento'
    end
    object lblPeriodoInicio: TLabel [1]
      Left = 5
      Top = 31
      Width = 32
      Height = 14
      Caption = 'In'#237'cio:'
    end
    object lblPeriodoFim: TLabel [2]
      Left = 149
      Top = 31
      Width = 22
      Height = 14
      Caption = 'Fim:'
    end
    inherited pnlBotoes: TPanel
      Top = 223
      Width = 560
      TabOrder = 9
      ExplicitTop = 223
      ExplicitWidth = 560
      inherited btnVisualizar: TJvTransparentButton
        Left = 228
        ExplicitLeft = 228
      end
      inherited btnCancelar: TJvTransparentButton
        Left = 336
        ExplicitLeft = 336
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 120
        OnClick = btnLimparClick
        ExplicitLeft = 120
      end
    end
    inherited gbTipo: TGroupBox
      Left = 379
      Top = 180
      Width = 176
      Height = 39
      TabOrder = 8
      ExplicitLeft = 379
      ExplicitTop = 180
      ExplicitWidth = 176
      ExplicitHeight = 39
      inherited chbTipoAnalitico: TCheckBox
        Left = 87
        Top = 18
        Width = 72
        ExplicitLeft = 87
        ExplicitTop = 18
        ExplicitWidth = 72
      end
      inherited chbTipoSintetico: TCheckBox
        Left = 9
        Top = 18
        Width = 72
        ExplicitLeft = 9
        ExplicitTop = 18
        ExplicitWidth = 72
      end
    end
    inherited chbExibirGrafico: TCheckBox
      Left = 241
      Top = 180
      TabOrder = 7
      ExplicitLeft = 241
      ExplicitTop = 180
    end
    inherited chbExportarPDF: TCheckBox
      Left = 106
      Top = 180
      TabOrder = 5
      ExplicitLeft = 106
      ExplicitTop = 180
    end
    inherited chbExportarExcel: TCheckBox
      Left = 106
      Top = 203
      TabOrder = 6
      ExplicitLeft = 106
      ExplicitTop = 203
    end
    object dteDataInicio: TJvDateEdit
      Left = 43
      Top = 28
      Width = 100
      Height = 22
      Color = 16114127
      ShowNullDate = False
      TabOrder = 0
      OnKeyPress = FormKeyPress
    end
    object dteDataFim: TJvDateEdit
      Left = 177
      Top = 28
      Width = 100
      Height = 22
      Color = 16114127
      ShowNullDate = False
      TabOrder = 1
      OnKeyPress = FormKeyPress
    end
    object rgFuncionario: TRadioGroup
      Left = 5
      Top = 56
      Width = 550
      Height = 69
      Caption = 'Colaborador'
      ItemIndex = 0
      Items.Strings = (
        'Todos'
        'Espec'#237'fico:')
      TabOrder = 2
      OnClick = rgFuncionarioClick
      OnExit = rgFuncionarioExit
    end
    object lcbFuncionario: TDBLookupComboBox
      Left = 91
      Top = 96
      Width = 458
      Height = 22
      Color = 16114127
      Enabled = False
      KeyField = 'ID_FUNCIONARIO'
      ListField = 'NOME_FUNCIONARIO'
      ListSource = dsFuncionario
      TabOrder = 3
      OnKeyPress = FormKeyPress
    end
    object chbComRecibo: TCheckBox
      Left = 5
      Top = 180
      Width = 95
      Height = 17
      Caption = 'COM Recibo'
      TabOrder = 4
      OnKeyPress = FormKeyPress
    end
    object GroupBox1: TGroupBox
      Left = 5
      Top = 131
      Width = 550
      Height = 43
      Caption = 'Tipo de Lan'#231'amento'
      TabOrder = 10
      object chbComissao: TCheckBox
        Left = 5
        Top = 18
        Width = 97
        Height = 17
        Caption = 'Comiss'#227'o'
        TabOrder = 0
        OnKeyPress = FormKeyPress
      end
      object chbVale: TCheckBox
        Left = 154
        Top = 18
        Width = 97
        Height = 17
        Caption = 'Vale'
        TabOrder = 1
        OnKeyPress = FormKeyPress
      end
      object chbOutrasDespesas: TCheckBox
        Left = 274
        Top = 18
        Width = 143
        Height = 17
        Caption = 'Outros Lan'#231'amentos'
        TabOrder = 2
        OnKeyPress = FormKeyPress
      end
      object chbSalario: TCheckBox
        Left = 489
        Top = 18
        Width = 57
        Height = 17
        Caption = 'Sal'#225'rio'
        TabOrder = 3
        OnKeyPress = FormKeyPress
      end
    end
  end
  object qryFuncionario: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT ID_FUNCIONARIO, NOME_FUNCIONARIO'
      '  FROM FUNCIONARIO'
      ' WHERE (FLG_ATIVO = '#39'S'#39
      '    OR DATA_DESLIGAMENTO > :DATA_FIM)'
      '   AND (NOME_FUNCIONARIO <> '#39'N'#195'O INFORMADO'#39
      '    OR  NOME_FUNCIONARIO <> '#39'FUNCION'#193'RIO'#39')'
      'ORDER BY NOME_FUNCIONARIO')
    Left = 403
    Top = 75
    ParamData = <
      item
        Name = 'DATA_FIM'
        DataType = ftDate
        ParamType = ptInput
      end>
  end
  object dsFuncionario: TDataSource
    DataSet = qryFuncionario
    Left = 475
    Top = 75
  end
end
