inherited FListaRecibos: TFListaRecibos
  Caption = 'Lista de Recibos'
  ClientHeight = 453
  ClientWidth = 915
  ExplicitWidth = 921
  ExplicitHeight = 482
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 909
    Height = 447
    ExplicitWidth = 909
    inherited pnlGrid: TPanel
      Top = 52
      Width = 903
      Height = 392
      ExplicitTop = 68
      ExplicitWidth = 903
      ExplicitHeight = 375
      object btnReimprimir: TJvTransparentButton [0]
        Left = 0
        Top = 369
        Width = 903
        Height = 23
        Align = alBottom
        Caption = '&Reimprimir'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        Spacing = 413
        TextAlign = ttaRight
        Transparent = False
        OnClick = btnReimprimirClick
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          1800000000000006000000000000000000000000000000000000000000000000
          0000000000003333332323232323232323232323232323232323233333330000
          0000000000000000000000000000000000000000000033333323232323232323
          2323232323232323232323333333000000000000000000000000000000000000
          000000000000D8D8D8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD5D5D53A3A
          3A000000000000000000000000000000000000000000D8D8D8FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFD5D5D53A3A3A000000000000000000000000000000
          000000000000C9CACBFCF3EAE7D5C3E9D7C6E9D7C6E7D5C3FCF3EBC6C7C72F2F
          2F000000000000000000000000000000000000000000CACACAF2F2F2D1D1D1D4
          D4D4D4D4D4D1D1D1F1F1F1C7C7C72F2F2F0000000000000000001212123F3F3F
          3C3C3C2C2C2CC3C4C5FCF4EAE8D5C3E9D7C6E9D7C6E7D5C3FCF3EBC2C3C45151
          513838384343430000001616163F3F3F3C3C3C2C2C2CC5C5C5F2F2F2D2D2D2D4
          D4D4D4D4D4D1D1D1F1F1F1C3C3C35151513838384343430000008383837F7F7F
          7F7F7F777777C2C2C2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5C5C57373
          737F7F7F7F7F7F8787878383837F7F7F7F7F7F777777C2C2C2FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFC5C5C57373737F7F7F7F7F7F878787787878787878
          7878787878786F6F6F6767676767676767676767676767676767677070707777
          777878787878787878787878787878787878787878786F6F6F67676767676767
          6767676767676767676767707070777777787878787878787878787878787878
          7777777070706F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F7070
          707777777878787878787878787878787777777070706F6F6F6F6F6F6F6F6F6F
          6F6F6F6F6F6F6F6F6F6F6F6F6F6F707070777777787878787878787878777777
          737373C2C8CEC5CDD4C5CCD3C5CCD3C5CCD3C5CCD3C5CCD3C5CCD3C5CDD4C1C7
          CD727272777777787878787878777777737373C9C9C9CECECECDCDCDCDCDCDCD
          CDCDCDCDCDCDCDCDCDCDCDCECECEC8C8C8727272777777787878787878707070
          C2C8CEC68D53B06C2AB16F2EB16F2EB16F2EB16F2EB16F2EB16F2EB06C29C890
          59C1C7CC707070787878787878707070CACACA8181815F5F5F63636363636363
          63636363636363636363635F5F5F858585C8C8C87070707878787878786F6F6F
          C5CBD0B06C2AB3783EB3783EB3783EB3783EB3783EB3783EB3783EB3783EB06C
          29C5CCD36F6F6F7878787878786F6F6FCCCCCC6060606D6D6D6D6D6D6D6D6D6D
          6D6D6D6D6D6D6D6D6D6D6D6D6D6D5F5F5FCDCDCD6F6F6F7878787878786F6F6F
          C4CAD0B26F2EB3783EB3783EB3783EB3783EB3783EB3783EB3783EB3783EB26F
          2EC4CBD36F6F6F7878787878786F6F6FCBCBCB6363636D6D6D6D6D6D6D6D6D6D
          6D6D6D6D6D6D6D6D6D6D6D6D6D6D636363CDCDCD6F6F6F7878788787877E7E7E
          D5DCE3BD7732B27336AE6F31AE6F31AE6F31AE6F31AE6F31AE6F31B27336B873
          2FDBE3EA7E7E7E8787878787877E7E7EDEDEDE6A6A6A68686863636363636363
          6363636363636363636363686868666666E4E4E47E7E7E878787000000000000
          231C15533213BEA083DDBFA2D9BB9ED9BB9ED9BB9ED9BB9EDDBFA2BB9E82885D
          320000000000000000000000000000001A1A1A2C2C2C9B9B9BBABABAB6B6B6B6
          B6B6B6B6B6B6B6B6BABABA999999545454000000000000000000000000000000
          000000000000CED2D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC9CDD12E33
          37000000000000000000000000000000000000000000D2D2D2FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFCECECE343434000000000000000000000000000000
          000000000000B7B7B7CECECECACACACACACACACACACACACACECECEB4B4B41212
          12000000000000000000000000000000000000000000B7B7B7CECECECACACACA
          CACACACACACACACACECECEB4B4B4121212000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        NumGlyphs = 2
        ExplicitTop = 370
        ExplicitWidth = 526
      end
      inherited dbgGridListaPadrao: TDBGrid
        Width = 903
        Height = 364
        Align = alNone
        OnCellClick = dbgGridListaPadraoCellClick
        OnColEnter = dbgGridListaPadraoColEnter
        OnColExit = dbgGridListaPadraoColExit
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'SELECIONADO'
            Title.Alignment = taCenter
            Title.Caption = '#'
            Width = 26
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ID_RECIBO_IMPRESSAO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_RECIBO'
            Title.Caption = 'Dt. Recibo'
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'NUM_RECIBO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Title.Alignment = taCenter
            Title.Caption = 'N'#186' Recibo'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = [fsBold]
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VR_RECIBO'
            Title.Alignment = taRightJustify
            Title.Caption = 'Vlr. Recibo'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VR_RECIBO_EXTENSO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'COD_SERVENTIA'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'NOME_SERVENTIA'
            Title.Caption = 'Emissor'
            Width = 250
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCR_SERVICOS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'OBS_RECIBO_IMPRESSAO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'COD_MUNICIPIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'NOME_MUNICIPIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'EM_ID_PESSOA'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'NOME_EMITENTE'
            Title.Caption = 'Emitente'
            Width = 250
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CPF_CNPJ_EMITENTE'
            Title.Caption = 'Doc. Emitente'
            Width = 110
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ENDERECO_EMITENTE'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'COD_LANCAMENTO_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ANO_LANCAMENTO_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_IMPRESSAO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_USUARIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_FUNCIONARIO'
            Visible = False
          end>
      end
    end
    inherited pnlFiltro: TPanel
      Width = 909
      Height = 49
      ExplicitWidth = 909
      ExplicitHeight = 49
      inherited btnFiltrar: TJvTransparentButton
        Left = 734
        Top = 20
        OnClick = btnFiltrarClick
        ExplicitLeft = 734
        ExplicitTop = 20
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 823
        Top = 20
        ExplicitLeft = 823
        ExplicitTop = 20
      end
      object lblPeriodo: TLabel
        Left = 3
        Top = 8
        Width = 99
        Height = 14
        Caption = 'Periodo do Recibo'
      end
      object lblA: TLabel
        Left = 106
        Top = 27
        Width = 6
        Height = 14
        Caption = 'a'
      end
      object lblNumeroRecibo: TLabel
        Left = 221
        Top = 8
        Width = 54
        Height = 14
        Caption = 'N'#186' Recibo'
      end
      object dteDataInicio: TJvDateEdit
        Left = 3
        Top = 24
        Width = 100
        Height = 22
        Color = 16114127
        ShowNullDate = False
        TabOrder = 0
        OnKeyPress = dteDataInicioKeyPress
      end
      object dteDataFim: TJvDateEdit
        Left = 115
        Top = 24
        Width = 100
        Height = 22
        Color = 16114127
        ShowNullDate = False
        TabOrder = 1
        OnKeyPress = dteDataFimKeyPress
      end
      object edtNumeroRecibo: TEdit
        Left = 221
        Top = 24
        Width = 100
        Height = 22
        Color = 16114127
        NumbersOnly = True
        TabOrder = 2
        OnKeyPress = edtNumeroReciboKeyPress
      end
    end
  end
  inherited dsGridListaPadrao: TDataSource
    DataSet = cdsGridListaPadrao
    Left = 836
    Top = 379
  end
  inherited qryGridListaPadrao: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT RI.*'
      '  FROM RECIBO_IMPRESSAO RI'
      ' WHERE RI.DATA_RECIBO BETWEEN :DATA_INI AND :DATA_FIM'
      '   AND RI.NUM_RECIBO = (CASE WHEN (:NUM_REC1 = 0)'
      '                             THEN RI.NUM_RECIBO'
      '                             ELSE :NUM_REC2'
      '                        END)'
      'ORDER BY RI.DATA_IMPRESSAO DESC')
    Left = 834
    Top = 237
    ParamData = <
      item
        Position = 1
        Name = 'DATA_INI'
        DataType = ftDateTime
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'DATA_FIM'
        DataType = ftDateTime
        ParamType = ptInput
      end
      item
        Position = 3
        Name = 'NUM_REC1'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 4
        Name = 'NUM_REC2'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspGridListaPadrao: TDataSetProvider
    DataSet = qryGridListaPadrao
    Left = 836
    Top = 285
  end
  object cdsGridListaPadrao: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATA_INI'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATA_FIM'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NUM_REC1'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'NUM_REC2'
        ParamType = ptInput
      end>
    ProviderName = 'dspGridListaPadrao'
    Left = 836
    Top = 333
    object cdsGridListaPadraoSELECIONADO: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'SELECIONADO'
    end
    object cdsGridListaPadraoID_RECIBO_IMPRESSAO: TIntegerField
      FieldName = 'ID_RECIBO_IMPRESSAO'
      Origin = 'ID_RECIBO_IMPRESSAO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsGridListaPadraoNUM_RECIBO: TIntegerField
      FieldName = 'NUM_RECIBO'
      Origin = 'NUM_RECIBO'
    end
    object cdsGridListaPadraoVR_RECIBO: TBCDField
      FieldName = 'VR_RECIBO'
      Origin = 'VR_RECIBO'
      OnGetText = cdsGridListaPadraoVR_RECIBOGetText
      Precision = 18
      Size = 2
    end
    object cdsGridListaPadraoVR_RECIBO_EXTENSO: TStringField
      FieldName = 'VR_RECIBO_EXTENSO'
      Origin = 'VR_RECIBO_EXTENSO'
      Size = 250
    end
    object cdsGridListaPadraoCOD_SERVENTIA: TStringField
      FieldName = 'COD_SERVENTIA'
      Origin = 'COD_SERVENTIA'
      FixedChar = True
      Size = 4
    end
    object cdsGridListaPadraoNOME_SERVENTIA: TStringField
      FieldName = 'NOME_SERVENTIA'
      Origin = 'NOME_SERVENTIA'
      Size = 250
    end
    object cdsGridListaPadraoDESCR_SERVICOS: TStringField
      FieldName = 'DESCR_SERVICOS'
      Size = 1000
    end
    object cdsGridListaPadraoOBS_RECIBO_IMPRESSAO: TStringField
      FieldName = 'OBS_RECIBO_IMPRESSAO'
      Origin = 'OBS_RECIBO_IMPRESSAO'
      Size = 300
    end
    object cdsGridListaPadraoDATA_RECIBO: TDateField
      FieldName = 'DATA_RECIBO'
      Origin = 'DATA_RECIBO'
    end
    object cdsGridListaPadraoCOD_MUNICIPIO: TStringField
      FieldName = 'COD_MUNICIPIO'
      Origin = 'COD_MUNICIPIO'
      Size = 5
    end
    object cdsGridListaPadraoNOME_MUNICIPIO: TStringField
      FieldName = 'NOME_MUNICIPIO'
      Origin = 'NOME_MUNICIPIO'
      Size = 250
    end
    object cdsGridListaPadraoEM_ID_PESSOA: TIntegerField
      FieldName = 'EM_ID_PESSOA'
      Origin = 'EM_ID_PESSOA'
    end
    object cdsGridListaPadraoNOME_EMITENTE: TStringField
      FieldName = 'NOME_EMITENTE'
      Origin = 'NOME_EMITENTE'
      Size = 250
    end
    object cdsGridListaPadraoCPF_CNPJ_EMITENTE: TStringField
      FieldName = 'CPF_CNPJ_EMITENTE'
      Origin = 'CPF_CNPJ_EMITENTE'
      Size = 18
    end
    object cdsGridListaPadraoENDERECO_EMITENTE: TStringField
      FieldName = 'ENDERECO_EMITENTE'
      Origin = 'ENDERECO_EMITENTE'
      Size = 600
    end
    object cdsGridListaPadraoCOD_LANCAMENTO_FK: TIntegerField
      FieldName = 'COD_LANCAMENTO_FK'
      Origin = 'COD_LANCAMENTO_FK'
    end
    object cdsGridListaPadraoANO_LANCAMENTO_FK: TIntegerField
      FieldName = 'ANO_LANCAMENTO_FK'
      Origin = 'ANO_LANCAMENTO_FK'
    end
    object cdsGridListaPadraoDATA_IMPRESSAO: TSQLTimeStampField
      FieldName = 'DATA_IMPRESSAO'
      Origin = 'DATA_IMPRESSAO'
    end
    object cdsGridListaPadraoID_USUARIO: TIntegerField
      FieldName = 'ID_USUARIO'
      Origin = 'ID_USUARIO'
    end
    object cdsGridListaPadraoFLG_FUNCIONARIO: TStringField
      FieldName = 'FLG_FUNCIONARIO'
      Origin = 'FLG_FUNCIONARIO'
      FixedChar = True
      Size = 1
    end
  end
  object dsReimpressao: TDataSource
    DataSet = cdsReimpressao
    Left = 710
    Top = 375
  end
  object cdsReimpressao: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspReimpressao'
    Left = 710
    Top = 327
  end
  object dspReimpressao: TDataSetProvider
    DataSet = qryReimpressao
    Left = 710
    Top = 279
  end
  object qryReimpressao: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM RECIBO_REIMPRESSAO'
      ' WHERE (1 = 0)')
    Left = 710
    Top = 231
  end
end
