inherited FListaBaixaFlutuantes: TFListaBaixaFlutuantes
  Caption = 'Repasse de Receitas - Gerar Nova Despesa'
  ClientHeight = 550
  ClientWidth = 647
  OnClose = FormClose
  ExplicitWidth = 653
  ExplicitHeight = 579
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 641
    Height = 544
    ExplicitWidth = 641
    ExplicitHeight = 544
    inherited pnlGrid: TPanel
      Top = 103
      Width = 635
      Height = 438
      TabOrder = 1
      ExplicitTop = 103
      ExplicitWidth = 635
      ExplicitHeight = 438
      object btnConfirmar: TJvTransparentButton [0]
        Left = 0
        Top = 415
        Width = 635
        Height = 23
        Align = alBottom
        Caption = '&OK'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        Spacing = 296
        TextAlign = ttaRight
        Transparent = False
        OnClick = btnConfirmarClick
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          1800000000000006000000000000000000000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF4F9EFDDE4ECD8E0ECD8E0EFDDE4FFF4F9FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFAE6E6E6E2
          E2E2E2E2E2E6E6E6FAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFF3FAA3B6AA329064008A47008D49008D49008A47329064A3B6AAFFF3
          FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBABABAB6161613A3A3A39
          39393939393A3A3A616161ABABABFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFECF62F8D5F009A5B34CDA539DDB23AE1B33AE1B239DDB234CDA5009A5A2F8D
          5FFFECF6FFFFFFFFFFFFFFFFFFFFFFFFF8F8F85E5E5E47474788888892929295
          95959595959292928888884747475E5E5EF8F8F8FFFFFFFFFFFFFFFFFFFFF5FE
          1A8B550DB5833EE0B600D79700D49600D49700D49700D49600D7973EE0B50DB5
          831A8B55FFF5FEFFFFFFFFFFFFFFFFFF53535367676797979770707070707070
          7070707070707070707070979797676767535353FFFFFFFFFFFFFFFFFF75A78C
          00A97233DFB200D29500D29600CB8700CE8F00D39900D39900D29800D29533DF
          B200A97275A78CFFFFFFFFFFFF8E8E8E5959599191916D6D6D70707061616168
          68687373737373737272726D6D6D9191915959598E8E8EFFFFFFFFF6FE008641
          45DDB800D19400CF9404CE93FFFFFFA1EDD700C78400D29A00D19900D19900D1
          9445DDB6008641FFF6FEFFFFFF3333339999996D6D6D6D6D6D717171FFFFFFCA
          CACA5C5C5C7272727272727272726D6D6D999999333333FFFFFFA7C6B605AA75
          19D8A700CF9605CC93FFFFFFFFFFFFFFFFFF9DECD600C68400D09A00D09900CF
          9819D8A606AB74A5C9B6B9B3B65E5E5E8181816E6E6E707070FFFFFFFFFFFFFF
          FFFFC8C8C85B5B5B7272727272727070708282825E5E5EB6B6B678B7962EC69C
          00D09800C789FFFFFFFFFFFFA0EAD3FFFFFFFFFFFF9DEBD600C48400CF9A00CE
          9900D0982BC49974BC979C93978080806F6F6F5F5F5FFFFFFFFFFFFFC7C7C7FF
          FFFFFFFFFFC7C7C75B5B5B7171717070706F6F6F7F7F7F9898987CB79738D0A8
          00CC9400C99270E2C4B5EFDF00C18004CA92FFFFFFFFFFFF9DEAD600C28400CD
          9900CE972CC49979BC9A9D95998B8B8B6C6C6C696969AFAFAFD4D4D45454546F
          6F6FFFFFFFFFFFFFC8C8C85959597171716F6F6F7F7F7F9A9A9A75B49250DDB9
          00C99300CB9900C89100C78F00CB9900C99404C893FFFFFFFFFFFFA8ECD900C7
          8F00CC982BC49874BC979990949E9E9E6969697070706767676464647070706A
          6A6A6E6E6EFFFFFFFFFFFFCDCDCD6464646E6E6E7F7F7F989898B1D2BF38C69B
          18D3A700C99700CA9900CA9900CA9900CA9900C79404C793FFFFFFEFFBFA00C2
          8A19D3A604AA73B3D8C4C3BEC18585857F7F7F6C6C6C6E6E6E6E6E6E6E6E6E6E
          6E6E6969696F6F6FFFFFFFF6F6F65E5E5E8080805C5C5CC5C5C5FFFFFF07985C
          64F5D700BF8D00C89900C89900C89900C89900C89900C69421CCA116CC9E00C5
          9245DBB800853FFFFFFFFFFFFF525252B5B5B56262626F6F6F6F6F6F6F6F6F6F
          6F6F6F6F6F6969697F7F7F7A7A7A666666989898313131FFFFFFFFFFFF81BB9C
          6FE1C12FE0B800BF8D00C79900C79900C79900C79900C79900C69700C39233D8
          B300A76D90C3A8FFFFFFFFFFFF9E9E9EACACAC9191916363636E6E6E6E6E6E6D
          6D6D6D6D6D6D6D6D6A6A6A6565658E8E8E545454AAAAAAFFFFFFFFFFFFFFFFFF
          319B66A9FFF043E8C300BA8900C09200C29400C29400C29400C2963ED9B708B2
          7E2B9C68FFFFFFFFFFFFFFFFFFFFFFFF666666DADADA9E9E9E5C5C5C66666668
          6868686868686868676767959595636363656565FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF41A2706ED5B49FFFF64DECCA43E2C042E1BF45E1C140D6B00094524AA8
          7AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF717171A5A5A5DADADAA6A6A69B
          9B9B9B9B9B9D9D9D9292923E3E3E797979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFCDE3D568B7913AAD7D33AC7B20A671109D6363B68FDCEBE1FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D7D790909076767672
          72726666665959598D8D8DE3E3E3FFFFFFFFFFFFFFFFFFFFFFFF}
        NumGlyphs = 2
        ExplicitTop = 368
        ExplicitWidth = 472
      end
      object lblDataPagamento: TLabel [1]
        Left = 0
        Top = 329
        Width = 83
        Height = 14
        Caption = 'Dt. Pagamento'
      end
      object lblDescricaoBaixa: TLabel [2]
        Left = 0
        Top = 371
        Width = 99
        Height = 14
        Caption = 'Descri'#231#227'o da Baixa'
      end
      object lblObservacoesBaixa: TLabel [3]
        Left = 312
        Top = 287
        Width = 69
        Height = 14
        Caption = 'Observa'#231#245'es'
      end
      object lblValorTotalFlutuante: TLabel [4]
        Left = 0
        Top = 287
        Width = 98
        Height = 14
        Caption = 'Vlr. Total Repasse'
      end
      object lblValorTotalDespesa: TLabel [5]
        Left = 126
        Top = 287
        Width = 99
        Height = 14
        Caption = 'Vlr. Total Despesa'
      end
      inherited dbgGridListaPadrao: TDBGrid
        Left = 232
        Top = 327
        Width = 74
        Height = 54
        Align = alNone
        TabOrder = 6
        Visible = False
        Columns = <
          item
            Expanded = False
            FieldName = 'SELECIONADO'
            Title.Caption = 'Sel.'
            Width = 25
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'NUM_RECIBO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Title.Alignment = taCenter
            Title.Caption = 'Recibo'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = [fsBold]
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Title.Caption = 'Descri'#231#227'o'
            Width = 298
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATA_LANCAMENTO_PARC'
            Title.Alignment = taCenter
            Title.Caption = 'Dt. Entrada'
            Width = 87
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VR_PARCELA_PAGO'
            Title.Alignment = taRightJustify
            Title.Caption = 'Vlr. Pago'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COD_LANCAMENTO'
            Title.Caption = 'C'#243'digo'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ANO_LANCAMENTO'
            Title.Caption = 'Ano'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_TOTAL_PREV'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'QTD_PARCELAS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_SISTEMA_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_LANCAMENTO_PARC'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'NUM_PARCELA'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_FORMAPAGAMENTO_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_PARCELA_PREV'
            Title.Alignment = taRightJustify
            Title.Caption = 'Vlr. Previsto'
            Visible = False
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATA_PAGAMENTO'
            Title.Alignment = taCenter
            Title.Caption = 'Dt. Pagamento'
            Visible = False
          end>
      end
      object mmObservacoesBaixa: TMemo
        Left = 312
        Top = 303
        Width = 323
        Height = 106
        Color = 16114127
        TabOrder = 5
        OnKeyPress = mmObservacoesBaixaKeyPress
      end
      object edtDescricaoBaixa: TEdit
        Left = 0
        Top = 387
        Width = 306
        Height = 22
        Color = 16114127
        TabOrder = 4
        OnKeyPress = FormKeyPress
      end
      object dteDataPagamento: TJvDateEdit
        Left = 0
        Top = 345
        Width = 100
        Height = 22
        Color = 16114127
        ShowNullDate = False
        TabOrder = 3
        OnExit = dteDataPagamentoExit
        OnKeyPress = dteDataPagamentoKeyPress
      end
      object cedValorTotalFlutuante: TJvCalcEdit
        Left = 0
        Top = 303
        Width = 120
        Height = 22
        Color = 16114127
        DisplayFormat = ',0.00'
        Enabled = False
        TabOrder = 1
        DecimalPlacesAlwaysShown = False
        OnKeyPress = FormKeyPress
      end
      object cedValorTotalDespesa: TJvCalcEdit
        Left = 126
        Top = 303
        Width = 120
        Height = 22
        Color = 16114127
        DisplayFormat = ',0.00'
        Enabled = False
        TabOrder = 2
        DecimalPlacesAlwaysShown = False
        OnKeyPress = FormKeyPress
      end
      object dbgRepasses: TJvDBUltimGrid
        Left = 0
        Top = 1
        Width = 635
        Height = 280
        DataSource = dsGridListaPadrao
        Options = [dgTitles, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnCellClick = dbgRepassesCellClick
        OnDrawColumnCell = dbgRepassesDrawColumnCell
        OnTitleClick = dbgRepassesTitleClick
        MinColumnWidth = 26
        AutoSizeColumnIndex = 0
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 18
        TitleRowHeight = 18
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'SELECIONADO'
            Title.Alignment = taCenter
            Title.Caption = 'Sel.'
            Width = 26
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'NUM_RECIBO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Title.Alignment = taCenter
            Title.Caption = 'Recibo'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = [fsBold]
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Title.Caption = 'Descri'#231#227'o'
            Width = 298
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATA_LANCAMENTO_PARC'
            Title.Alignment = taCenter
            Title.Caption = 'Dt. Entrada'
            Width = 87
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VR_PARCELA_PAGO'
            Title.Alignment = taRightJustify
            Title.Caption = 'Vlr. Pago'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COD_LANCAMENTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ANO_LANCAMENTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_LANCAMENTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'TIPO_LANCAMENTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'TIPO_CADASTRO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_CATEGORIA_DESPREC_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_SUBCATEGORIA_DESPREC_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_NATUREZA_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_CLIENTE_FORNECEDOR_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_IMPOSTORENDA'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_PESSOAL'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_AUXILIAR'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_REAL'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_FLUTUANTE'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'QTD_PARCELAS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_TOTAL_PREV'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_TOTAL_JUROS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_TOTAL_DESCONTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_TOTAL_PAGO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'CAD_ID_USUARIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_CADASTRO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_STATUS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_CANCELADO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_CANCELAMENTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'CANCEL_ID_USUARIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_RECORRENTE'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_REPLICADO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_ORIGEM'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_SISTEMA_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'OBSERVACAO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_CARNELEAO_EQUIVALENCIA_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_LANCAMENTO_PARC'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'COD_LANCAMENTO_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ANO_LANCAMENTO_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'NUM_PARCELA'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_VENCIMENTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_PARCELA_PREV'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_PARCELA_JUROS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_PARCELA_DESCONTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_FORMAPAGAMENTO_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'AGENCIA'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'CONTA'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_BANCO_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'NUM_CHEQUE'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'NUM_COD_DEPOSITO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_STATUS_1'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_CANCELAMENTO_1'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'CANCEL_ID_USUARIO_1'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_PAGAMENTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'PAG_ID_USUARIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'OBS_LANCAMENTO_PARC'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'CAD_ID_USUARIO_1'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_CADASTRO_1'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_STATUS_PARC'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_FORAFECHCAIXA'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_FORMAPAGAMENTO_ORIG_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'BAIXADO'
            Visible = False
          end>
      end
    end
    inherited pnlFiltro: TPanel
      Width = 641
      Height = 100
      TabOrder = 0
      ExplicitWidth = 641
      ExplicitHeight = 100
      inherited btnFiltrar: TJvTransparentButton
        Left = 555
        Top = 72
        OnClick = btnFiltrarClick
        ExplicitLeft = 555
        ExplicitTop = 72
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 555
        Top = 40
        ExplicitLeft = 555
        ExplicitTop = 40
      end
      object lblPeriodo: TLabel
        Left = 3
        Top = 8
        Width = 130
        Height = 14
        Caption = 'Per'#237'odo de Lan'#231'amento'
      end
      object Label2: TLabel
        Left = 107
        Top = 27
        Width = 6
        Height = 14
        Caption = 'a'
      end
      object lblValorFlutuante: TLabel
        Left = 222
        Top = 9
        Width = 27
        Height = 14
        Caption = 'Valor'
      end
      object lblNumReciboFlutuante: TLabel
        Left = 315
        Top = 8
        Width = 54
        Height = 14
        Caption = 'N'#186' Recibo'
      end
      object lblLegendaBaixado: TLabel
        Left = 365
        Top = 83
        Width = 41
        Height = 14
        Caption = 'Baixado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object spLegendaPago: TShape
        Left = 352
        Top = 87
        Width = 10
        Height = 10
        Brush.Color = 14680031
      end
      object lblLegendaSemBaixa: TLabel
        Left = 277
        Top = 83
        Width = 55
        Height = 14
        Caption = 'Sem Baixa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object spLegendaVencido: TShape
        Left = 264
        Top = 87
        Width = 10
        Height = 10
        Brush.Color = 10797567
      end
      object chbSelecionarTodos: TCheckBox
        Left = 3
        Top = 81
        Width = 113
        Height = 17
        Caption = 'Marcar Todos'
        TabOrder = 5
        OnClick = chbSelecionarTodosClick
        OnKeyPress = FormKeyPress
      end
      object dtePeriodoIni: TJvDateEdit
        Left = 3
        Top = 24
        Width = 100
        Height = 22
        Color = 16114127
        ShowNullDate = False
        TabOrder = 0
        OnKeyPress = dtePeriodoIniKeyPress
      end
      object dtePeriodoFim: TJvDateEdit
        Left = 116
        Top = 24
        Width = 100
        Height = 22
        Color = 16114127
        ShowNullDate = False
        TabOrder = 1
        OnKeyPress = dtePeriodoFimKeyPress
      end
      object rgSituacao: TRadioGroup
        Left = 412
        Top = 8
        Width = 91
        Height = 90
        Caption = 'Situa'#231#227'o'
        ItemIndex = 1
        Items.Strings = (
          'Todos'
          'Sem Baixa'
          'Baixados')
        TabOrder = 4
        OnClick = rgSituacaoClick
        OnExit = rgSituacaoExit
      end
      object cedValorFlutuante: TJvCalcEdit
        Left = 222
        Top = 24
        Width = 87
        Height = 22
        Color = 16114127
        DisplayFormat = ',0.00'
        TabOrder = 2
        DecimalPlacesAlwaysShown = False
        OnKeyPress = cedValorFlutuanteKeyPress
      end
      object edtNumReciboFlutuante: TEdit
        Left = 315
        Top = 24
        Width = 91
        Height = 22
        Color = 16114127
        TabOrder = 3
        OnKeyPress = edtNumReciboFlutuanteKeyPress
      end
    end
  end
  inherited dsGridListaPadrao: TDataSource
    DataSet = dmBaixa.cdsBaixaLancG
    Left = 44
    Top = 227
  end
  inherited qryGridListaPadrao: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    Left = 44
    Top = 171
  end
  object qryQtdParcs: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT COUNT(ID_LANCAMENTO_PARC) AS QTD,'
      '       SUM(VR_PARCELA_PREV) AS TOTAL_PREV,'
      '       SUM(VR_PARCELA_PAGO) AS TOTAL_PAGO'
      '  FROM LANCAMENTO_PARC'
      ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO'
      '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO'
      '   AND FLG_STATUS = '#39'G'#39)
    Left = 582
    Top = 174
    ParamData = <
      item
        Name = 'COD_LANCAMENTO'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'ANO_LANCAMENTO'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryQtdParcsQTD: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'QTD'
      Origin = 'QTD'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryQtdParcsTOTAL_PREV: TBCDField
      AutoGenerateValue = arDefault
      FieldName = 'TOTAL_PREV'
      Origin = 'TOTAL_PREV'
      ProviderFlags = []
      ReadOnly = True
      Precision = 18
      Size = 2
    end
    object qryQtdParcsTOTAL_PAGO: TBCDField
      AutoGenerateValue = arDefault
      FieldName = 'TOTAL_PAGO'
      Origin = 'TOTAL_PAGO'
      ProviderFlags = []
      ReadOnly = True
      Precision = 18
      Size = 2
    end
  end
  object dsQtdParcs: TDataSource
    DataSet = qryQtdParcs
    Left = 582
    Top = 222
  end
end
