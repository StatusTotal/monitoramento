{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UGeracaoCarneLeao.pas
  Descricao:   Tela de geracao e conferencia dos dados do Carne Leao
  Author   :   Cristina
  Date:        25-nov-2016
  Last Update: 23-out-2018 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UGeracaoCarneLeao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.DBCtrls, Vcl.Mask, JvExMask, JvToolEdit, JvDBControls, JvMaskEdit,
  JvBaseEdits, JvExStdCtrls, JvCombobox, JvDBCombobox, Vcl.Grids, Vcl.DBGrids,
  System.StrUtils, System.DateUtils, FireDAC.Stan.Param, Vcl.ComCtrls,
  System.Math;

type
  TFGeracaoCarneLeao = class(TFCadastroGeralPadrao)
    lblDataGeracao: TLabel;
    lblSistemaGerador: TLabel;
    lblAnoReferencia: TLabel;
    rgTipoEnvio: TDBRadioGroup;
    edtSistemaGerador: TDBEdit;
    edtAnoReferencia: TDBEdit;
    dteDataGeracao: TJvDBDateEdit;
    lblCPFDeclarante: TLabel;
    lblNomeDeclarante: TLabel;
    edtNomeDeclarante: TDBEdit;
    GroupBox1: TGroupBox;
    lblMesReferencia: TLabel;
    lblVlrTrabNaoAssalariado: TLabel;
    lblVlrAlugueis: TLabel;
    lblVlrOutros: TLabel;
    lblVlrExterior: TLabel;
    lblVlrPrevidenciaOficial: TLabel;
    lblVlrDependentes: TLabel;
    lblVlrPensaoAlimenticia: TLabel;
    lblVlrLivroCaixa: TLabel;
    lblVlrImpExteriorAComp: TLabel;
    lblVlrImpostoAPagar: TLabel;
    lblVlrImpostoPago: TLabel;
    cbMesReferencia: TJvDBComboBox;
    cedVlrTrabNaoAssalariado: TJvDBCalcEdit;
    cedVlrAlugueis: TJvDBCalcEdit;
    cedVlrOutros: TJvDBCalcEdit;
    cedVlrExterior: TJvDBCalcEdit;
    cedVlrDependentes: TJvDBCalcEdit;
    cedVlrPensaoAlimenticia: TJvDBCalcEdit;
    cedVlrLivroCaixa: TJvDBCalcEdit;
    cedVlrImpExteriorAComp: TJvDBCalcEdit;
    dbgRegistro: TDBGrid;
    cedVlrPrevidenciaOficial: TJvDBCalcEdit;
    cedVlrImpostoAPagar: TJvDBCalcEdit;
    cedVlrImpostoPago: TJvDBCalcEdit;
    edtCPFDeclarante: TDBEdit;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure edtNomeDeclaranteKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure dbgRegistroDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure edtCPFDeclaranteExit(Sender: TObject);
    procedure edtAnoReferenciaExit(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }

    iReg: Integer;

    procedure PreencherDemonstrativo;

    function SemPendencias: Boolean;
  public
    { Public declarations }

    TpEnvio: String;  { E = EXPORTACAO / I = IMPORTACAO / S = SEGURANCA }
  end;

var
  FGeracaoCarneLeao: TFGeracaoCarneLeao;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMCarneLeao,
  UListaCarneLeaoPendentes;

{ TFGeracaoCarneLeao }

procedure TFGeracaoCarneLeao.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFGeracaoCarneLeao.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
  begin
    if SemPendencias then
      GravarGenerico
    else
      Exit;
  end
  else
    Self.Close;
end;

procedure TFGeracaoCarneLeao.edtAnoReferenciaExit(Sender: TObject);
begin
  inherited;

  SemPendencias;
end;

procedure TFGeracaoCarneLeao.edtCPFDeclaranteExit(Sender: TObject);
var
  sCPF: String;
begin
  inherited;

  sCPF := Trim(StringReplace(StringReplace(edtCPFDeclarante.EditText,
                                           '.',
                                           '',
                                           [rfReplaceAll, rfIgnoreCase]),
                             '-',
                             '',
                             [rfReplaceAll, rfIgnoreCase]));

  if sCPF <> '' then
  begin
    if not dmGerencial.VerificarCPF(sCPF) then
      Application.MessageBox('CPF inv�lido.', 'Erro', MB_OK + MB_ICONERROR);
  end;
end;

procedure TFGeracaoCarneLeao.dbgRegistroDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;

  AlternarCorGrid(Sender, Rect, DataCol, Column, State);
end;

procedure TFGeracaoCarneLeao.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtSistemaGerador.MaxLength := 14;
  edtAnoReferencia.MaxLength  := 4;
  edtNomeDeclarante.MaxLength := 60;
end;

procedure TFGeracaoCarneLeao.DesabilitarComponentes;
begin
  inherited;

  rgTipoEnvio.Enabled       := False;
  edtSistemaGerador.Enabled := False;

  cedVlrTrabNaoAssalariado.Enabled := False;
  cedVlrAlugueis.Enabled           := False;
  cedVlrOutros.Enabled             := False;
  cedVlrExterior.Enabled           := False;
  cedVlrPrevidenciaOficial.Enabled := False;
  cedVlrDependentes.Enabled        := False;
  cedVlrPensaoAlimenticia.Enabled  := False;
  cedVlrLivroCaixa.Enabled         := False;
  cedVlrImpExteriorAComp.Enabled   := False;
  cedVlrImpostoAPagar.Enabled      := False;
  cedVlrImpostoPago.Enabled        := False;

  cbMesReferencia.Enabled := False;
end;

procedure TFGeracaoCarneLeao.edtNomeDeclaranteKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFGeracaoCarneLeao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  if lPodeCancelar then
  begin
    vgOperacao     := VAZIO;
    vgIdConsulta   := 0;
    vgOrigemFiltro := False;
  end
  else
    Action := caNone;
end;

procedure TFGeracaoCarneLeao.FormShow(Sender: TObject);
begin
  inherited;

  { INDENTIFICACAO }
  dmCarneLeao.cdsIdent_F.Close;
  dmCarneLeao.cdsIdent_F.Params.ParamByName('ID_CARNELEAO_IDENTIFICACAO').Value := vgIdConsulta;
  dmCarneLeao.cdsIdent_F.Open;

  { ESCRITURACAO }
  dmCarneLeao.cdsEscrit_F.Close;
  dmCarneLeao.cdsEscrit_F.Params.ParamByName('ID_CARNELEAO_IDENTIFICACAO').Value := vgIdConsulta;
  dmCarneLeao.cdsEscrit_F.Open;

  { PLANO DE CONTAS }
  dmCarneLeao.cdsPlCont_F.Close;
  dmCarneLeao.cdsPlCont_F.Params.ParamByName('ID_CARNELEAO_IDENTIFICACAO').Value := vgIdConsulta;
  dmCarneLeao.cdsPlCont_F.Open;

  { REGISTRO }
  dmCarneLeao.cdsRegis_F.Close;
  dmCarneLeao.cdsRegis_F.Params.ParamByName('ID_CARNELEAO_IDENTIFICACAO').Value := vgIdConsulta;
  dmCarneLeao.cdsRegis_F.Open;

  { CONTROLE }
  dmCarneLeao.cdsCtrl_F.Close;
  dmCarneLeao.cdsCtrl_F.Params.ParamByName('ID_CARNELEAO_IDENTIFICACAO').Value := vgIdConsulta;
  dmCarneLeao.cdsCtrl_F.Open;

  if vgOperacao = I then
  begin
    { INDENTIFICACAO }
    dmCarneLeao.cdsIdent_F.Append;
    dmCarneLeao.cdsIdent_F.FieldByName('IDENTIFICADOR').AsString   := '01';
    dmCarneLeao.cdsIdent_F.FieldByName('TIPO_ENVIO').AsString      := TpEnvio;
    dmCarneLeao.cdsIdent_F.FieldByName('DATA_GERACAO').AsDateTime  := Date;
    dmCarneLeao.cdsIdent_F.FieldByName('GER_ID_USUARIO').AsInteger := vgUsu_Id;
    dmCarneLeao.cdsIdent_F.FieldByName('SISTEMA_GERADOR').AsString := 'MONITORAMENTO';
    dmCarneLeao.cdsIdent_F.FieldByName('ANO_DECLARACAO').AsInteger := (YearOf(Date) - 1);
    dmCarneLeao.cdsIdent_F.FieldByName('NOME_DECLARANTE').AsString := vgSrvn_NomeTabeliao;
    dmCarneLeao.cdsIdent_F.FieldByName('FLG_CANCELADO').AsString   := 'N';

    { ESCRITURACAO }

    { PLANO DE CONTAS }

    { REGISTRO }
    PreencherDemonstrativo;
  end;

  if vgOperacao = E then
  begin
    { INDENTIFICACAO }
    dmCarneLeao.cdsIdent_F.Edit;

    { ESCRITURACAO }

    { PLANO DE CONTAS }

    { REGISTRO }
    PreencherDemonstrativo;
  end;

  if dteDataGeracao.CanFocus then
    dteDataGeracao.SetFocus;
end;

function TFGeracaoCarneLeao.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFGeracaoCarneLeao.GravarDadosGenerico(var Msg: String): Boolean;
var
  IdIdentif, IdEscr, IdPlCont, IdReg, IdCtrl: Integer;
  lOk: Boolean;
  sMsg, sTpArq: String;
  TpMsg: Integer;
begin
  Result := True;
  Msg    := '';
  lOk    := True;
  sMsg   := '';
  TpMsg  := 0;
  sTpArq := '';

  IdIdentif := 0;
  IdEscr    := 0;
  IdPlCont  := 0;
  IdReg     := 0;
  IdCtrl    := 0;

  if vgOperacao = C then
    Exit;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    { IDENTIFICACAO }
    if dmCarneLeao.cdsIdent_F.State in [dsInsert, dsEdit] then
    begin
      if vgOperacao = I then
      begin
        IdIdentif := BS.ProximoId('ID_CARNELEAO_IDENTIFICACAO', 'CARNELEAO_IDENTIFICACAO');
        dmCarneLeao.cdsIdent_F.FieldByName('ID_CARNELEAO_IDENTIFICACAO').AsInteger := IdIdentif;
      end
      else
        IdIdentif := dmCarneLeao.cdsIdent_F.FieldByName('ID_CARNELEAO_IDENTIFICACAO').AsInteger;

      dmCarneLeao.cdsIdent_F.Post;
      dmCarneLeao.cdsIdent_F.ApplyUpdates(0);
    end;

    { ESCRITURACAO }

    { PLANO DE CONTAS }

    { REGISTRO }
    if dmCarneLeao.cdsRegis_F.RecordCount > 0 then
    begin
      if vgOperacao = I then
        IdReg := BS.ProximoId('ID_CARNELEAO_REGISTRO', 'CARNELEAO_REGISTRO');

      dmCarneLeao.cdsRegis_F.First;

      while not dmCarneLeao.cdsRegis_F.Eof do
      begin
        dmCarneLeao.cdsRegis_F.Edit;

        if vgOperacao = I then
          dmCarneLeao.cdsRegis_F.FieldByName('ID_CARNELEAO_REGISTRO').AsInteger := IdReg;

        dmCarneLeao.cdsRegis_F.FieldByName('ID_CARNELEAO_IDENTIFICACAO_FK').AsInteger := IdIdentif;

        Inc(IdReg);

        dmCarneLeao.cdsRegis_F.Post;

        dmCarneLeao.cdsRegis_F.Next;
      end;

      dmCarneLeao.cdsRegis_F.ApplyUpdates(0);
    end;

    { CONTROLE }
    if vgOperacao = I then
    begin
      IdCtrl := BS.ProximoId('ID_CARNELEAO_CONTROLE', 'CARNELEAO_CONTROLE');

      dmCarneLeao.cdsCtrl_F.Append;

      dmCarneLeao.cdsCtrl_F.FieldByName('ID_CARNELEAO_CONTROLE').AsInteger := IdCtrl;
      dmCarneLeao.cdsCtrl_F.FieldByName('IDENTIFICADOR').AsString          := '99';
    end
    else
      dmCarneLeao.cdsCtrl_F.Edit;

    dmCarneLeao.cdsCtrl_F.FieldByName('ID_CARNELEAO_IDENTIFICACAO_FK').AsInteger := IdIdentif;
{    dmCarneLeao.cdsCtrl_F.FieldByName('CONTROLE').AsString                       := dmGerencial.CompletaString(IntToStr(RandomRange(Hex(), Hex())),
                                                                                                               ' ',
                                                                                                               'D',
                                                                                                               10);  }

    dmCarneLeao.cdsCtrl_F.FieldByName('CONTROLE').AsString                       := dmGerencial.CompletaString(IntToStr(IdCtrl),
                                                                                                               '0',
                                                                                                               'E',
                                                                                                               10);

    dmCarneLeao.cdsCtrl_F.Post;

    if dmCarneLeao.cdsCtrl_F.RecordCount > 0 then
      dmCarneLeao.cdsCtrl_F.ApplyUpdates(0);

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    Msg := 'Carn�-Le�o gravado com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg    := 'Erro na grava��o do Carn�-Le�o.';
    lOk    := False;
  end;

  if lOk then  //Se tudo ok, perguntar ao usuario se ele deseja gerar o arquivo de imediato
  begin
    case AnsiIndexStr(UpperCase(dmCarneLeao.cdsIdent_F.FieldByName('TIPO_ENVIO').AsString), ['E', 'I', 'S']) of
      0: sTpArq := 'Exporta��o';
      1: sTpArq := 'Importa��o';
      2: sTpArq := 'Seguran�a';
    end;

    if Application.MessageBox(PChar('Deseja gerar o arquivo de ' + sTpArq + '?'),
                                    'Confirma��o',
                                    MB_YESNO + MB_ICONQUESTION) = ID_YES then
    begin
      if dmCarneLeao.GerarArquivoExportacaoCarneLeao(IdIdentif,
                                                     sMsg,
                                                     TpMsg,
                                                     False) then
        Application.MessageBox(PChar('Arquivo de Carn� Le�o ' +
                                     dmCarneLeao.cdsIdent_F.FieldByName('ANO_DECLARACAO').AsString +
                                     ' gerado com sucesso!'),
                               'Sucesso!',
                               MB_OK)
      else
      begin
        if TpMsg = 1 then  //Aviso
        begin
          Application.MessageBox(PChar(sMsg),
                                 'Aviso',
                                 MB_OK + MB_ICONWARNING);
        end
        else if TpMsg = 2 then  //Aviso para regerar arquivo
        begin
          if Application.MessageBox(PChar(sMsg),
                                    'Aviso',
                                    MB_OK + MB_ICONERROR) = ID_YES then
          begin
            if dmCarneLeao.GerarArquivoExportacaoCarneLeao(IdIdentif,
                                                           sMsg,
                                                           TpMsg,
                                                           True) then
              Application.MessageBox('Erro ao sobrescrever o arquivo.',
                                     'Erro',
                                      MB_OK + MB_ICONERROR);
          end;
        end
        else if TpMsg = 3 then  //Erro
        begin
          Application.MessageBox(PChar(sMsg),
                                 'Erro',
                                 MB_OK + MB_ICONERROR);
        end;
      end;
    end;
  end;
end;

procedure TFGeracaoCarneLeao.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta de Carn�-Le�o');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclus�o de Carn�-Le�o');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO',
                                  'Por favor, indique o motivo da edi��o dessa Carn�-Le�o:',
                                  '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de Carn�-Le�o';

      BS.GravarUsuarioLog(2, '', sObservacao);
    end;
  end;
end;

procedure TFGeracaoCarneLeao.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFGeracaoCarneLeao.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

procedure TFGeracaoCarneLeao.PreencherDemonstrativo;
var
  j: Integer;
  cImpExtComp, cBaseCalc: Currency;
  sSiglaMes: String;
begin
  { REGISTRO }
  iReg := 900000;

  for j := 1 to 12 do
  begin
    Inc(iReg);

    if (vgOperacao = I) then
    begin
      if dmCarneLeao.cdsRegis_F.RecordCount = 12 then
      begin
        case j of
          1: sSiglaMes := 'A';
          2: sSiglaMes := 'B';
          3: sSiglaMes := 'C';
          4: sSiglaMes := 'D';
          5: sSiglaMes := 'E';
          6: sSiglaMes := 'F';
          7: sSiglaMes := 'G';
          8: sSiglaMes := 'H';
          9: sSiglaMes := 'I';
          10: sSiglaMes := 'J';
          11: sSiglaMes := 'K';
          12: sSiglaMes := 'L';
        end;

        if dmCarneLeao.cdsRegis_F.Locate('MES_APURACAO',
                                         sSiglaMes,
                                         [loCaseInsensitive, loPartialKey]) then
          dmCarneLeao.cdsRegis_F.Edit;
      end
      else
      begin
        dmCarneLeao.cdsRegis_F.Append;
        dmCarneLeao.cdsRegis_F.FieldByName('ID_CARNELEAO_REGISTRO').AsString := IntToStr(iReg);
        dmCarneLeao.cdsRegis_F.FieldByName('IDENTIFICADOR').AsString         := '02';

        case j of
          1:
          begin
            dmCarneLeao.cdsRegis_F.FieldByName('MES_APURACAO').AsString      := 'A';
            dmCarneLeao.cdsRegis_F.FieldByName('NOME_MES_APURACAO').AsString := 'JANEIRO';
          end;
          2:
          begin
            dmCarneLeao.cdsRegis_F.FieldByName('MES_APURACAO').AsString      := 'B';
            dmCarneLeao.cdsRegis_F.FieldByName('NOME_MES_APURACAO').AsString := 'FEVEREIRO';
          end;
          3:
          begin
            dmCarneLeao.cdsRegis_F.FieldByName('MES_APURACAO').AsString      := 'C';
            dmCarneLeao.cdsRegis_F.FieldByName('NOME_MES_APURACAO').AsString := 'MARCO';
          end;
          4:
          begin
            dmCarneLeao.cdsRegis_F.FieldByName('MES_APURACAO').AsString      := 'D';
            dmCarneLeao.cdsRegis_F.FieldByName('NOME_MES_APURACAO').AsString := 'ABRIL';
          end;
          5:
          begin
            dmCarneLeao.cdsRegis_F.FieldByName('MES_APURACAO').AsString      := 'E';
            dmCarneLeao.cdsRegis_F.FieldByName('NOME_MES_APURACAO').AsString := 'MAIO';
          end;
          6:
          begin
            dmCarneLeao.cdsRegis_F.FieldByName('MES_APURACAO').AsString      := 'F';
            dmCarneLeao.cdsRegis_F.FieldByName('NOME_MES_APURACAO').AsString := 'JUNHO';
          end;
          7:
          begin
            dmCarneLeao.cdsRegis_F.FieldByName('MES_APURACAO').AsString      := 'G';
            dmCarneLeao.cdsRegis_F.FieldByName('NOME_MES_APURACAO').AsString := 'JULHO';
          end;
          8:
          begin
            dmCarneLeao.cdsRegis_F.FieldByName('MES_APURACAO').AsString      := 'H';
            dmCarneLeao.cdsRegis_F.FieldByName('NOME_MES_APURACAO').AsString := 'AGOSTO';
          end;
          9:
          begin
            dmCarneLeao.cdsRegis_F.FieldByName('MES_APURACAO').AsString      := 'I';
            dmCarneLeao.cdsRegis_F.FieldByName('NOME_MES_APURACAO').AsString := 'SETEMBRO';
          end;
          10:
          begin
            dmCarneLeao.cdsRegis_F.FieldByName('MES_APURACAO').AsString      := 'J';
            dmCarneLeao.cdsRegis_F.FieldByName('NOME_MES_APURACAO').AsString := 'OUTUBRO';
          end;
          11:
          begin
            dmCarneLeao.cdsRegis_F.FieldByName('MES_APURACAO').AsString      := 'K';
            dmCarneLeao.cdsRegis_F.FieldByName('NOME_MES_APURACAO').AsString := 'NOVEMBRO';
          end;
          12:
          begin
            dmCarneLeao.cdsRegis_F.FieldByName('MES_APURACAO').AsString      := 'L';
            dmCarneLeao.cdsRegis_F.FieldByName('NOME_MES_APURACAO').AsString := 'DEZEMBRO';
          end;
        end;
      end;
    end
    else if vgOperacao = E then
    begin
      case j of
        1: sSiglaMes := 'A';
        2: sSiglaMes := 'B';
        3: sSiglaMes := 'C';
        4: sSiglaMes := 'D';
        5: sSiglaMes := 'E';
        6: sSiglaMes := 'F';
        7: sSiglaMes := 'G';
        8: sSiglaMes := 'H';
        9: sSiglaMes := 'I';
        10: sSiglaMes := 'J';
        11: sSiglaMes := 'K';
        12: sSiglaMes := 'L';
      end;

      if dmCarneLeao.cdsRegis_F.Locate('MES_APURACAO',
                                       sSiglaMes,
                                       [loCaseInsensitive, loPartialKey]) then
        dmCarneLeao.cdsRegis_F.Edit;
    end;

    //Retorna os Valores referentes ao Mes informado
    cImpExtComp := 0;
    cBaseCalc   := 0;

    dmCarneLeao.cdsRegis_F.FieldByName('VR_TRAB_NAO_ASSALARIADO').AsCurrency := 0;
    dmCarneLeao.cdsRegis_F.FieldByName('VR_ALUGUEIS').AsCurrency             := 0;
    dmCarneLeao.cdsRegis_F.FieldByName('VR_OUTROS').AsCurrency               := 0;
    dmCarneLeao.cdsRegis_F.FieldByName('VR_EXTERIOR').AsCurrency             := 0;
    dmCarneLeao.cdsRegis_F.FieldByName('VR_PREV_OFICIAL').AsCurrency         := 0;
    dmCarneLeao.cdsRegis_F.FieldByName('VR_DEPENDENTES').AsCurrency          := 0;
    dmCarneLeao.cdsRegis_F.FieldByName('VR_PENSAO_ALIMENTICIA').AsCurrency   := 0;
    dmCarneLeao.cdsRegis_F.FieldByName('VR_LIVRO_CAIXA').AsCurrency          := 0;
    dmCarneLeao.cdsRegis_F.FieldByName('VR_IMP_EXTERIOR_ACOMP').AsCurrency   := 0;
    dmCarneLeao.cdsRegis_F.FieldByName('VR_IMP_APAGAR').AsCurrency           := 0;
    dmCarneLeao.cdsRegis_F.FieldByName('VR_IMP_PAGO').AsCurrency             := 0;

    dmCarneLeao.qryValores.Close;
    dmCarneLeao.qryValores.Params.ParamByName('MES').Value := j;
    dmCarneLeao.qryValores.Params.ParamByName('ANO').Value := StrToInt(edtAnoReferencia.Text);
    dmCarneLeao.qryValores.Open;

    dmCarneLeao.qryValores.First;

    while not dmCarneLeao.qryValores.Eof do
    begin
      case dmCarneLeao.qryValores.FieldByName('ID_CARNELEAO_CLASSIFICACAO_FK').AsInteger of
        1:
        begin
          dmCarneLeao.cdsRegis_F.FieldByName('VR_TRAB_NAO_ASSALARIADO').AsCurrency := dmCarneLeao.qryValores.FieldByName('VR_PARCELA_PAGO').AsCurrency;

          cBaseCalc := dmCarneLeao.qryValores.FieldByName('VR_PARCELA_PAGO').AsCurrency;
        end;
        2:
        begin
          dmCarneLeao.cdsRegis_F.FieldByName('VR_ALUGUEIS').AsCurrency := dmCarneLeao.qryValores.FieldByName('VR_PARCELA_PAGO').AsCurrency;

          cBaseCalc := (cBaseCalc + dmCarneLeao.qryValores.FieldByName('VR_PARCELA_PAGO').AsCurrency);
        end;
        3:
        begin
          dmCarneLeao.cdsRegis_F.FieldByName('VR_OUTROS').AsCurrency := dmCarneLeao.qryValores.FieldByName('VR_PARCELA_PAGO').AsCurrency;

          cBaseCalc := (cBaseCalc + dmCarneLeao.qryValores.FieldByName('VR_PARCELA_PAGO').AsCurrency);
        end;
        4:
        begin
          dmCarneLeao.cdsRegis_F.FieldByName('VR_EXTERIOR').AsCurrency := dmCarneLeao.qryValores.FieldByName('VR_PARCELA_PAGO').AsCurrency;

          cBaseCalc := (cBaseCalc + dmCarneLeao.qryValores.FieldByName('VR_PARCELA_PAGO').AsCurrency);
        end;
        5:
        begin
          dmCarneLeao.cdsRegis_F.FieldByName('VR_PREV_OFICIAL').AsCurrency := dmCarneLeao.qryValores.FieldByName('VR_PARCELA_PAGO').AsCurrency;

          cBaseCalc := (cBaseCalc - dmCarneLeao.qryValores.FieldByName('VR_PARCELA_PAGO').AsCurrency);
        end;
        6:
        begin
          dmCarneLeao.cdsRegis_F.FieldByName('VR_DEPENDENTES').AsCurrency := dmCarneLeao.qryValores.FieldByName('VR_PARCELA_PAGO').AsCurrency;

          cBaseCalc := dmCarneLeao.qryValores.FieldByName('VR_PARCELA_PAGO').AsCurrency;
        end;
        7:
        begin
          dmCarneLeao.cdsRegis_F.FieldByName('VR_PENSAO_ALIMENTICIA').AsCurrency := dmCarneLeao.qryValores.FieldByName('VR_PARCELA_PAGO').AsCurrency;

          cBaseCalc := dmCarneLeao.qryValores.FieldByName('VR_PARCELA_PAGO').AsCurrency;
        end;
        8:
        begin
          dmCarneLeao.cdsRegis_F.FieldByName('VR_LIVRO_CAIXA').AsCurrency := dmCarneLeao.qryValores.FieldByName('VR_PARCELA_PAGO').AsCurrency;

          cBaseCalc := dmCarneLeao.qryValores.FieldByName('VR_PARCELA_PAGO').AsCurrency;
        end;
        9:
        begin
          dmCarneLeao.cdsRegis_F.FieldByName('VR_IMP_EXTERIOR_ACOMP').AsCurrency := dmCarneLeao.qryValores.FieldByName('VR_PARCELA_PAGO').AsCurrency;

          cBaseCalc   := dmCarneLeao.qryValores.FieldByName('VR_PARCELA_PAGO').AsCurrency;
          cImpExtComp := dmCarneLeao.qryValores.FieldByName('VR_PARCELA_PAGO').AsCurrency;
        end;
        11:
        begin
          dmCarneLeao.cdsRegis_F.FieldByName('VR_IMP_PAGO').AsCurrency := dmCarneLeao.qryValores.FieldByName('VR_PARCELA_PAGO').AsCurrency;

          cBaseCalc := dmCarneLeao.qryValores.FieldByName('VR_PARCELA_PAGO').AsCurrency;
        end;
      end;

      dmCarneLeao.qryValores.Next;
    end;

    if cBaseCalc <= 1903.98 then
      dmCarneLeao.cdsRegis_F.FieldByName('VR_IMP_APAGAR').AsCurrency := 0
    else if (cBaseCalc > 1903.98) and (cBaseCalc <= 2826.65) then
      dmCarneLeao.cdsRegis_F.FieldByName('VR_IMP_APAGAR').AsCurrency := (((cBaseCalc * 0.075) - 142.80) - cImpExtComp)
    else if (cBaseCalc > 2826.65) and (cBaseCalc <= 3751.05) then
      dmCarneLeao.cdsRegis_F.FieldByName('VR_IMP_APAGAR').AsCurrency := (((cBaseCalc * 0.150) - 354.80) - cImpExtComp)
    else if (cBaseCalc > 3751.05) and (cBaseCalc <= 4664.68) then
      dmCarneLeao.cdsRegis_F.FieldByName('VR_IMP_APAGAR').AsCurrency := (((cBaseCalc * 0.225) - 636.13) - cImpExtComp)
    else if cBaseCalc > 4664.68 then
      dmCarneLeao.cdsRegis_F.FieldByName('VR_IMP_APAGAR').AsCurrency := (((cBaseCalc * 0.275) - 869.36) - cImpExtComp);

    dmCarneLeao.cdsRegis_F.Post;
  end;
end;

function TFGeracaoCarneLeao.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
var
  sCPF: String;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if rgTipoEnvio.ItemIndex = -1 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o TIPO DE ARQUIVO.';
    DadosMsgErro.Componente := rgTipoEnvio;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if dteDataGeracao.Date = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a DATA DE GERA��O.';
    DadosMsgErro.Componente := dteDataGeracao;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtAnoReferencia.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar o ANO DE REFER�NCIA DA DECLARA��O.';
    DadosMsgErro.Componente := edtAnoReferencia;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  sCPF := Trim(StringReplace(StringReplace(edtCPFDeclarante.EditText,
                                           '.',
                                           '',
                                           [rfReplaceAll, rfIgnoreCase]),
                             '-',
                             '',
                             [rfReplaceAll, rfIgnoreCase]));

  if Trim(sCPF) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta informar o CPF DO DECLARANTE.';
    DadosMsgErro.Componente := edtCPFDeclarante;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtNomeDeclarante.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta informar o NOME DO DECLARANTE.';
    DadosMsgErro.Componente := edtNomeDeclarante;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFGeracaoCarneLeao.SemPendencias: Boolean;
var
  sLstLancs: String;
begin
  inherited;

  Result := True;

  sLstLancs := '';

  if dmCarneLeao.ExisteLancamentoSemCarneLeao(StrToInt(edtAnoReferencia.Text), sLstLancs) then
  begin
    Application.MessageBox('Existem Lan�amentos sem relacionamento com o Plano de Contas do Carn�-Le�o.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);

    { Exibe lista com os referidos Lancamentos e da a opcao de cadastrar o
      Plano de Contas; caso o cadastro nao seja feito, sair da tela  }
    try
      Application.CreateForm(TFListaCarneLeaoPendentes, FListaCarneLeaoPendentes);

      FListaCarneLeaoPendentes.ListaLancamentos := sLstLancs;
      FListaCarneLeaoPendentes.AnoDeclaracao    := StrToInt(edtAnoReferencia.Text);

      FListaCarneLeaoPendentes.ShowModal;
    finally
      sLstLancs := FListaCarneLeaoPendentes.ListaLancamentos;
      FListaCarneLeaoPendentes.Free;
    end;

    sLstLancs := '';

    if dmCarneLeao.ExisteLancamentoSemCarneLeao(StrToInt(edtAnoReferencia.Text), sLstLancs) then
    begin
      Result := False;

      Application.MessageBox(PChar('Existem Lan�amentos sem relacionamento com o Plano de Contas do Carn�-Le�o. ' +
                                   #13#10 + 'V� em FINANCEIRO > Carn�-Le�o > Rela��o Plano de Contas e inclua as rela��es pendentes.'),
                             'Aviso',
                             MB_OK + MB_ICONWARNING);
      Self.Close;
    end
    else
      PreencherDemonstrativo;
  end;
end;

function TFGeracaoCarneLeao.VerificarLayoutAto(var QtdErros: Integer): Boolean;
begin
  //
end;

end.
