inherited FFiltroRelatorioFechamentoCaixa: TFFiltroRelatorioFechamentoCaixa
  Caption = 'Relat'#243'rio de Fechamento de Caixa por Lan'#231'amento'
  ClientHeight = 318
  ExplicitHeight = 347
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlFiltro: TPanel
    Height = 312
    ExplicitHeight = 341
    object lblPeriodo: TLabel [0]
      Left = 5
      Top = 8
      Width = 125
      Height = 14
      Caption = 'Per'#237'odo de Pagamento'
    end
    object lblPeriodoInicio: TLabel [1]
      Left = 5
      Top = 31
      Width = 32
      Height = 14
      Caption = 'In'#237'cio:'
    end
    object lblPeriodoFim: TLabel [2]
      Left = 149
      Top = 31
      Width = 22
      Height = 14
      Caption = 'Fim:'
    end
    inherited pnlBotoes: TPanel
      Top = 265
      TabOrder = 7
      ExplicitTop = 294
      inherited btnLimpar: TJvTransparentButton
        OnClick = btnLimparClick
      end
    end
    inherited gbTipo: TGroupBox
      Top = 56
      Width = 148
      Height = 42
      TabOrder = 2
      ExplicitTop = 56
      ExplicitWidth = 148
      ExplicitHeight = 42
      inherited chbTipoAnalitico: TCheckBox
        Left = 81
        Top = 19
        Width = 63
        OnClick = chbTipoAnaliticoClick
        OnExit = chbTipoAnaliticoExit
        ExplicitLeft = 81
        ExplicitTop = 19
        ExplicitWidth = 63
      end
    end
    inherited chbExibirGrafico: TCheckBox
      Left = 159
      Top = 81
      TabOrder = 3
      Visible = False
      ExplicitLeft = 159
      ExplicitTop = 81
    end
    inherited chbExportarPDF: TCheckBox
      Left = 5
      Top = 244
      TabOrder = 5
      ExplicitLeft = 5
      ExplicitTop = 244
    end
    inherited chbExportarExcel: TCheckBox
      Left = 133
      Top = 243
      TabOrder = 6
      ExplicitLeft = 133
      ExplicitTop = 243
    end
    object dteDataInicio: TJvDateEdit
      Left = 43
      Top = 28
      Width = 100
      Height = 22
      Color = 16114127
      ShowNullDate = False
      TabOrder = 0
      OnKeyPress = FormKeyPress
    end
    object dteDataFim: TJvDateEdit
      Left = 177
      Top = 28
      Width = 100
      Height = 22
      Color = 16114127
      ShowNullDate = False
      TabOrder = 1
      OnKeyPress = FormKeyPress
    end
    object gbExibirRelatorio: TGroupBox
      Left = 5
      Top = 104
      Width = 318
      Height = 134
      Caption = 'Exibir os seguintes detalhamentos no Relat'#243'rio:'
      TabOrder = 4
      object chbPrevRealPend: TCheckBox
        Left = 7
        Top = 111
        Width = 191
        Height = 17
        Caption = 'Previsto x Realizado x Pendente'
        Checked = True
        Enabled = False
        State = cbChecked
        TabOrder = 7
        OnKeyPress = FormKeyPress
      end
      object chbFirmas: TCheckBox
        Left = 7
        Top = 19
        Width = 51
        Height = 17
        Caption = 'Firmas'
        TabOrder = 0
        OnKeyPress = FormKeyPress
      end
      object chbPagtoDinheiro: TCheckBox
        Left = 167
        Top = 42
        Width = 121
        Height = 17
        Caption = 'Pagto. em Dinheiro'
        TabOrder = 4
        OnKeyPress = FormKeyPress
      end
      object chbPagtoCartao: TCheckBox
        Left = 167
        Top = 19
        Width = 113
        Height = 17
        Caption = 'Pagto. em Cart'#227'o'
        TabOrder = 3
        OnKeyPress = FormKeyPress
      end
      object chbPagtoCheque: TCheckBox
        Left = 7
        Top = 42
        Width = 122
        Height = 17
        Caption = 'Pagto. em Cheque'
        TabOrder = 1
        OnKeyPress = FormKeyPress
      end
      object chbPagtoDeposito: TCheckBox
        Left = 167
        Top = 65
        Width = 129
        Height = 17
        Caption = 'Pagto. por Dep'#243'sito'
        TabOrder = 5
        OnKeyPress = FormKeyPress
      end
      object chbPagtoFaturado: TCheckBox
        Left = 7
        Top = 65
        Width = 105
        Height = 17
        Caption = 'Pagto. Faturado'
        TabOrder = 2
        OnKeyPress = FormKeyPress
      end
      object chbPagtoPromissoria: TCheckBox
        Left = 7
        Top = 88
        Width = 138
        Height = 17
        Caption = 'Pagto. por Promiss'#243'ria'
        TabOrder = 6
        OnKeyPress = FormKeyPress
      end
    end
  end
end
