inherited FCadastroTipoDespesaColaborador: TFCadastroTipoDespesaColaborador
  Caption = 'Cadastro de Tipo de Despesa de Colaborador'
  ClientHeight = 122
  ClientWidth = 429
  ExplicitWidth = 435
  ExplicitHeight = 151
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 423
    Height = 116
    ExplicitWidth = 423
    ExplicitHeight = 116
    inherited pnlDados: TPanel
      Width = 309
      Height = 110
      ExplicitWidth = 309
      ExplicitHeight = 110
      object lblDescricaoTipoDespesa: TLabel [0]
        Left = 5
        Top = 5
        Width = 51
        Height = 14
        Caption = 'Descri'#231#227'o'
      end
      inherited pnlMsgErro: TPanel
        Top = 49
        Width = 309
        TabOrder = 1
        ExplicitTop = 49
        ExplicitWidth = 309
        inherited lblMensagemErro: TLabel
          Width = 309
        end
        inherited lbMensagemErro: TListBox
          Width = 309
          OnDblClick = lbMensagemErroDblClick
          ExplicitWidth = 309
        end
      end
      object edtDescricaoTipoDespesa: TDBEdit
        Left = 5
        Top = 21
        Width = 302
        Height = 22
        CharCase = ecUpperCase
        Color = 16114127
        DataField = 'DESCR_TIPO_OUTRADESP_FUNC'
        DataSource = dsCadastro
        TabOrder = 0
        OnKeyPress = edtDescricaoTipoDespesaKeyPress
      end
    end
    inherited pnlMenu: TPanel
      Height = 110
      ExplicitHeight = 110
      inherited btnDadosPrincipais: TJvTransparentButton
        Height = 15
        Visible = False
        ExplicitWidth = 102
        ExplicitHeight = 15
      end
      inherited btnOk: TJvTransparentButton
        Top = 20
        ExplicitTop = 20
      end
      inherited btnCancelar: TJvTransparentButton
        Top = 65
        ExplicitTop = 69
      end
    end
  end
  inherited dsCadastro: TDataSource
    DataSet = cdsTpDespColab
    Left = 349
    Top = 55
  end
  object qryTpDespColab: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM TIPO_OUTRADESP_FUNC'
      ' WHERE ID_TIPO_OUTRADESP_FUNC = :ID_TIPO_OUTRADESP_FUNC')
    Left = 146
    Top = 71
    ParamData = <
      item
        Position = 1
        Name = 'ID_TIPO_OUTRADESP_FUNC'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspTpDespColab: TDataSetProvider
    DataSet = qryTpDespColab
    Left = 218
    Top = 55
  end
  object cdsTpDespColab: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_TIPO_OUTRADESP_FUNC'
        ParamType = ptInput
      end>
    ProviderName = 'dspTpDespColab'
    Left = 282
    Top = 71
    object cdsTpDespColabID_TIPO_OUTRADESP_FUNC: TIntegerField
      FieldName = 'ID_TIPO_OUTRADESP_FUNC'
      Required = True
    end
    object cdsTpDespColabDESCR_TIPO_OUTRADESP_FUNC: TStringField
      FieldName = 'DESCR_TIPO_OUTRADESP_FUNC'
      Size = 200
    end
  end
end
