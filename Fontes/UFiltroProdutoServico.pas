{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroProdutoServico.pas
  Descricao:   Tela de Filtro de Produtos e Servicos
  Author   :   Cristina
  Date:        11-abr-2016
  Last Update: 08-nov-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroProdutoServico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Mask, Vcl.DBCtrls, UDM, System.StrUtils,
  frxClass, frxDBSet;

type
  TFFiltroProdutoServico = class(TFFiltroSimplesPadrao)
    lblDescricao: TLabel;
    rgSituacao: TRadioGroup;
    qryGridPaiPadraoID: TIntegerField;
    qryGridPaiPadraoTIPO_ITEM: TStringField;
    qryGridPaiPadraoDESCR_ITEM: TStringField;
    qryGridPaiPadraoABREV_ITEM: TStringField;
    qryGridPaiPadraoULT_VALOR_CUSTO: TBCDField;
    qryGridPaiPadraoESTOQUE_ATUAL: TBCDField;
    qryGridPaiPadraoMEDIDA_PADRAO: TBCDField;
    qryGridPaiPadraoDATA_CADASTRO: TDateField;
    qryGridPaiPadraoOBS_ITEM: TStringField;
    qryGridPaiPadraoFLG_ATIVO: TStringField;
    qryGridPaiPadraoDATA_INATIVACAO: TDateField;
    edtDescricao: TEdit;
    qryGridPaiPadraoDESCR_UNIDADEMEDIDA: TStringField;
    btnSubstituir: TJvTransparentButton;
    procedure edtDescricaoKeyPress(Sender: TObject; var Key: Char);
    procedure rgSituacaoExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dbgGridPaiPadraoDrawDataCell(Sender: TObject; const Rect: TRect;
      Field: TField; State: TGridDrawState);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure qryGridPaiPadraoULT_VALOR_CUSTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoMEDIDA_PADRAOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure btnSubstituirClick(Sender: TObject);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroProdutoServico: TFFiltroProdutoServico;

  TpProdServ: TTipoProdutoServico;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UGDM, UVariaveisGlobais, UCadastroProdutoServico,
  UListaProdutoServico;

{ TFFiltroProdutoServico }

procedure TFFiltroProdutoServico.btnEditarClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao   := E;
  vgIdConsulta := qryGridPaiPadrao.FieldByName('ID').AsInteger;

  try
    Application.CreateForm(TFCadastroProdutoServico, FCadastroProdutoServico);

    FCadastroProdutoServico.TipoProdserv := TpProdServ;

    FCadastroProdutoServico.ShowModal;
  finally
    FCadastroProdutoServico.Free;
  end;

  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroProdutoServico.btnExcluirClick(Sender: TObject);
var
  qryExclusao: TFDQuery;
  sTipo: String;
begin
  inherited;

  sTipo := IfThen((qryGridPaiPadrao.FieldByName('TIPO_ITEM').AsString = 'P'),
                  'Produto',
                  'Servi�o');

  if lPodeExcluir then
  begin
    qryExclusao := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      with qryExclusao, SQL do
      begin
        Close;
        Clear;
        Text := 'DELETE FROM ITEM WHERE ID_ITEM = :ID_ITEM';
        Params.ParamByName('ID_ITEM').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      GravarLog;
      Application.MessageBox(PChar(Trim(sTipo) + ' exclu�do com sucesso!'), 'Aviso', MB_OK)
    except
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Application.MessageBox(PChar('Erro na exclus�o do ' + Trim(sTipo) + '.'), 'Erro', MB_OK + MB_ICONERROR)
    end;

    FreeAndNil(qryExclusao);

    btnFiltrar.Click;
  end;
end;

procedure TFFiltroProdutoServico.btnFiltrarClick(Sender: TObject);
begin
  with qryGridPaiPadrao, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_ITEM AS ID, ' +
            '       TIPO_ITEM, ' +
            '       DESCR_ITEM, ' +
            '       ABREV_ITEM, ' +
            '       ULT_VALOR_CUSTO, ' +
            '       ESTOQUE_ATUAL, ' +
            '       MEDIDA_PADRAO, ' +
            '       DESCR_UNIDADEMEDIDA, ' +
            '       DATA_CADASTRO, ' +
            '       OBS_ITEM, ' +
            '       FLG_ATIVO, ' +
            '       DATA_INATIVACAO ' +
            '  FROM ITEM ' +
            ' WHERE (1 = 1)';

    if TpProdServ in [PROD, SERV] then
    begin
      Add(' AND TIPO_ITEM = :TIPO_ITEM');
      Params.ParamByName('TIPO_ITEM').Value := IfThen((TpProdServ = PROD), 'P', 'S');
    end;

    if Trim(edtDescricao.Text) <> '' then
    begin
      Add(' AND CAST(UPPER(DESCR_ITEM) AS VARCHAR(250)) LIKE CAST(:DESCR_ITEM AS VARCHAR(250))');
      Params.ParamByName('DESCR_ITEM').Value := '%' + Trim(UpperCase(edtDescricao.Text)) + '%';
    end;

    if rgSituacao.ItemIndex = 1 then
      Add(' AND FLG_ATIVO = ' + QuotedStr('S'))
    else if rgSituacao.ItemIndex = 2 then
      Add(' AND FLG_ATIVO = ' + QuotedStr('N'));

    Add('ORDER BY DESCR_ITEM');
    Open;
  end;

  inherited;
end;

procedure TFFiltroProdutoServico.btnIncluirClick(Sender: TObject);
begin
  inherited;

  vgOperacao   := I;
  vgIdConsulta := 0;

  try
    Application.CreateForm(TFCadastroProdutoServico, FCadastroProdutoServico);

    FCadastroProdutoServico.TipoProdserv := TpProdServ;

    FCadastroProdutoServico.ShowModal;
  finally
    FCadastroProdutoServico.Free;
  end;

  btnFiltrar.Click;
end;

procedure TFFiltroProdutoServico.btnLimparClick(Sender: TObject);
begin
  edtDescricao.Clear;
  rgSituacao.ItemIndex := 1;

  inherited;

  if edtDescricao.CanFocus then
    edtDescricao.SetFocus;
end;

procedure TFFiltroProdutoServico.btnSubstituirClick(Sender: TObject);
var
  Op: TOperacao;
  OrigF, OrigC: Boolean;
  IdProdServSub: Integer;
  sTpProdServ: String;
  qrySub, QryAuxLog: TFDQuery;
begin
  inherited;

  IdProdServSub := 0;

  sTpProdServ := IfThen((qryGridPaiPadrao.FieldByName('TIPO_ITEM').AsString = 'P'),
                        'Produto',
                        'Servi�o');

  //Abre tela de selecao do Produto/Servico que ira substituir o Produto/Servico selecionado
  Op    := vgOperacao;
  OrigF := vgOrigemFiltro;
  OrigC := vgOrigemCadastro;

  vgOperacao       := C;
  vgOrigemFiltro   := True;
  vgOrigemCadastro := False;

  try
    Application.CreateForm(TFListaProdutoServico, FListaProdutoServico);

    FListaProdutoServico.IdProdServ := qryGridPaiPadrao.FieldByName('ID').AsInteger;
    FListaProdutoServico.TpProdServ := qryGridPaiPadrao.FieldByName('TIPO_ITEM').AsString;

    FListaProdutoServico.ShowModal;
  finally
    IdProdServSub := FListaProdutoServico.IdProdServSub;
    FListaProdutoServico.Free;
  end;

  vgOperacao       := Op;
  vgOrigemFiltro   := OrigF;
  vgOrigemCadastro := OrigC;

  //Caso um Produto/Servico tenha sido selecionado, efetuar a substituicao
  if IdProdServSub > 0 then
  begin
    qrySub := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      with qrySub, SQL do
      begin
        Close;
        Clear;
        Text := 'UPDATE LANCAMENTO_DET ' +
                '   SET ID_ITEM_FK = :ID_ITEM_SUB ' +
                ' WHERE ID_ITEM_FK = :ID_ITEM';
        Params.ParamByName('ID_ITEM_SUB').Value := IdProdServSub;
        Params.ParamByName('ID_ITEM').Value     := qryGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;
      end;

      with qrySub, SQL do
      begin
        Close;
        Clear;
        Text := 'UPDATE ITEM ' +
                '   SET FLG_ATIVO = ' + QuotedStr('N') +
                ' WHERE ID_ITEM = :ID_ITEM';
        Params.ParamByName('ID_ITEM').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      { LOG }
      QryAuxLog := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

      QryAuxLog.Close;
      QryAuxLog.SQL.Clear;
      QryAuxLog.SQL.Text := 'INSERT INTO USUARIO_LOG (ID_USUARIO_LOG, ID_USUARIO, ID_MODULO_FK, ' +
                            '                         TIPO_LANCAMENTO, CODIGO_LAN, ANO_LAN, ID_ORIGEM, ' +
                            '                         TABELA_ORIGEM, TIPO_ACAO, OBS_USUARIO_LOG) ' +
                            '                 VALUES (:ID_USUARIO_LOG, :ID_USUARIO, :ID_MODULO, ' +
                            '                         :TIPO_LANCAMENTO, :CODIGO_LAN, :ANO_LAN, :ID_ORIGEM, ' +
                            '                         :TABELA_ORIGEM, :TIPO_ACAO, :OBS_USUARIO_LOG)';

      QryAuxLog.Params.ParamByName('ID_USUARIO_LOG').Value  := BS.ProximoId('ID_USUARIO_LOG', 'USUARIO_LOG');
      QryAuxLog.Params.ParamByName('ID_USUARIO').Value      := vgUsu_Id;
      QryAuxLog.Params.ParamByName('ID_MODULO').Value       := 2;
      QryAuxLog.Params.ParamByName('TIPO_LANCAMENTO').Value := '';
      QryAuxLog.Params.ParamByName('CODIGO_LAN').Value      := 0;
      QryAuxLog.Params.ParamByName('ANO_LAN').Value         := 0;
      QryAuxLog.Params.ParamByName('ID_ORIGEM').Value       := qryGridPaiPadrao.FieldByName('ID').AsInteger;
      QryAuxLog.Params.ParamByName('TABELA_ORIGEM').Value   := 'ITEM';
      QryAuxLog.Params.ParamByName('TIPO_ACAO').Value       := 'E';
      QryAuxLog.Params.ParamByName('OBS_USUARIO_LOG').Value := 'Substitui��o do ' + sTpProdServ + ' ' +
                                                               qryGridPaiPadrao.FieldByName('ID').AsString +
                                                               ' pelo ' + sTpProdServ + ' ' +
                                                               IntToStr(IdProdServSub) + '.';

      QryAuxLog.ExecSQL;

      FreeAndNil(QryAuxLog);

      Application.MessageBox(PChar(sTpProdServ + ' substitu�do com sucesso!'),
                             'Aviso',
                             MB_OK);
    except
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Application.MessageBox(PChar('Erro na substitui��o do ' + sTpProdServ),
                             'Erro',
                             MB_OK + MB_ICONERROR);
    end;

    FreeAndNil(qrySub);

    btnFiltrar.Click;
  end;
end;

procedure TFFiltroProdutoServico.dbgGridPaiPadraoDblClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao   := C;
  vgIdConsulta := qryGridPaiPadrao.FieldByName('ID').AsInteger;

  try
    Application.CreateForm(TFCadastroProdutoServico, FCadastroProdutoServico);

    FCadastroProdutoServico.TipoProdserv := TpProdServ;

    FCadastroProdutoServico.ShowModal;
  finally
    FCadastroProdutoServico.Free;
  end;

  DefinirPosicaoGrid;
end;

procedure TFFiltroProdutoServico.dbgGridPaiPadraoDrawDataCell(Sender: TObject;
  const Rect: TRect; Field: TField; State: TGridDrawState);
begin
  inherited;

  if qryGridPaiPadrao.FieldByName('FLG_ATIVO').AsString = 'S' then
  begin
    dbgGridPaiPadrao.Canvas.Font.Color := clGray;
    dbgGridPaiPadrao.Canvas.FillRect(Rect);
    dbgGridPaiPadrao.DefaultDrawDataCell(Rect, Field, State);
  end;
end;

procedure TFFiltroProdutoServico.edtDescricaoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroProdutoServico.FormShow(Sender: TObject);
begin
  inherited;

  btnImprimir.Visible := False;

  rgSituacao.ItemIndex := 1;

  with qryGridPaiPadrao, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_ITEM AS ID, ' +
            '       TIPO_ITEM, ' +
            '       DESCR_ITEM, ' +
            '       ABREV_ITEM, ' +
            '       ULT_VALOR_CUSTO, ' +
            '       ESTOQUE_ATUAL, ' +
            '       MEDIDA_PADRAO, ' +
            '       DESCR_UNIDADEMEDIDA, ' +
            '       DATA_CADASTRO, ' +
            '       OBS_ITEM, ' +
            '       FLG_ATIVO, ' +
            '       DATA_INATIVACAO ' +
            '  FROM ITEM ' +
            ' WHERE (1 = 1)';

    if TpProdServ in [PROD, SERV] then
    begin
      Add(' AND TIPO_ITEM = :TIPO_ITEM');
      Params.ParamByName('TIPO_ITEM').Value := IfThen((TpProdServ = PROD), 'P', 'S');
    end;

    if rgSituacao.ItemIndex = 1 then
      Add(' AND FLG_ATIVO = ' + QuotedStr('S'))
    else if rgSituacao.ItemIndex = 2 then
      Add(' AND FLG_ATIVO = ' + QuotedStr('N'));

    Add('ORDER BY DESCR_ITEM');
    Open;
  end;

  inherited;

  ShowScrollBar(dbgGridPaiPadrao.Handle, SB_HORZ, False);

  if edtDescricao.CanFocus then
    edtDescricao.SetFocus;
end;

procedure TFFiltroProdutoServico.GravarLog;
var
  sObservacao, sTipo: String;
begin
  inherited;

  sObservacao := '';

  sTipo := IfThen((TpProdServ = PROD), 'Produto', 'Servi�o');

  case vgOperacao of
    X:  //EXCLUSAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da exclus�o desse ' + sTipo + ':', '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          qryGridPaiPadrao.FieldByName('ID').AsInteger, 'ITEM');
    end;
    P:  //IMPRESSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente impress�o de Lista de ' + sTipo,
                          qryGridPaiPadrao.FieldByName('ID').AsInteger, 'ITEM');
    end;
    T:  //EXPORTACAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente exporta��o de Lista de ' + sTipo,
                          qryGridPaiPadrao.FieldByName('ID').AsInteger, 'ITEM');
    end;
  end;
end;

function TFFiltroProdutoServico.PodeExcluir(var Msg: String): Boolean;
var
  QryVerif: TFDQuery;
begin
  inherited;

  Result := True;

  QryVerif := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryVerif, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT * FROM LANCAMENTO_DET WHERE ID_ITEM_FK = :ID_ITEM';
    Params.ParamByName('ID_ITEM').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
    Open;
  end;

  if QryVerif.RecordCount > 0 then
  begin
    if TpProdServ = PROD then
      msg := 'N�o � poss�vel excluir o Produto informado, pois o mesmo j� possui hist�rico no sistema.' + #13#10 +
             'Ser� preciso substitui-lo primeiro.'
    else
      msg := 'N�o � poss�vel excluir o Servi�o informado, pois o mesmo j� possui hist�rico no sistema.' + #13#10 +
             'Ser� preciso substitui-lo primeiro.';
  end;

  Result := (QryVerif.RecordCount = 0) and (qryGridPaiPadrao.RecordCount > 0);

  FreeAndNil(QryVerif);
end;

procedure TFFiltroProdutoServico.qryGridPaiPadraoMEDIDA_PADRAOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFFiltroProdutoServico.qryGridPaiPadraoULT_VALOR_CUSTOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFFiltroProdutoServico.rgSituacaoExit(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;

  if dbgGridPaiPadrao.CanFocus then
    dbgGridPaiPadrao.SetFocus;
end;

procedure TFFiltroProdutoServico.VerificarPermissoes;
begin
  inherited;

  if TpProdServ = PROD then
  begin
    btnIncluir.Enabled  := vgPrm_IncProd;
    btnEditar.Enabled   := vgPrm_EdProd and (qryGridPaiPadrao.RecordCount > 0);
    btnExcluir.Enabled  := vgPrm_ExcProd and (qryGridPaiPadrao.RecordCount > 0);
    btnImprimir.Enabled := vgPrm_ImpProd and (qryGridPaiPadrao.RecordCount > 0);
  end;

  if TpProdServ = SERV then
  begin
    btnIncluir.Enabled  := vgPrm_IncServ;
    btnEditar.Enabled   := vgPrm_EdServ and (qryGridPaiPadrao.RecordCount > 0);
    btnExcluir.Enabled  := vgPrm_ExcServ and (qryGridPaiPadrao.RecordCount > 0);
    btnImprimir.Enabled := vgPrm_ImpServ and (qryGridPaiPadrao.RecordCount > 0);
  end;

  btnSubstituir.Enabled := ((vgUsu_FlgMaster = 'S') or
                            (vgUsu_FlgSuporte = 'S')) and
                           (qryGridPaiPadrao.RecordCount > 0);
end;

end.
