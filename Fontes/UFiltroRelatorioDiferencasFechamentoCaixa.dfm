inherited FFiltroRelatorioDiferencasFechamentoCaixa: TFFiltroRelatorioDiferencasFechamentoCaixa
  Caption = 'Relat'#243'rio de Diferen'#231'as de Fechamento de Caixa'
  ClientHeight = 155
  ExplicitHeight = 184
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlFiltro: TPanel
    Height = 149
    ExplicitHeight = 179
    object lblPeriodo: TLabel [0]
      Left = 5
      Top = 8
      Width = 131
      Height = 14
      Caption = 'Per'#237'odo do Fechamento'
    end
    object lblPeriodoInicio: TLabel [1]
      Left = 5
      Top = 31
      Width = 32
      Height = 14
      Caption = 'In'#237'cio:'
    end
    object lblPeriodoFim: TLabel [2]
      Left = 149
      Top = 31
      Width = 22
      Height = 14
      Caption = 'Fim:'
    end
    inherited pnlBotoes: TPanel
      Top = 102
      TabOrder = 6
      ExplicitTop = 132
      inherited btnLimpar: TJvTransparentButton
        OnClick = btnLimparClick
      end
    end
    inherited gbTipo: TGroupBox
      Left = 235
      Top = 56
      Height = 42
      TabOrder = 2
      Visible = False
      ExplicitLeft = 235
      ExplicitTop = 56
      ExplicitHeight = 42
      inherited chbTipoAnalitico: TCheckBox
        Top = 23
        Visible = False
        ExplicitTop = 23
      end
      inherited chbTipoSintetico: TCheckBox
        Top = 14
        Visible = False
        ExplicitTop = 14
      end
    end
    inherited chbExibirGrafico: TCheckBox
      Left = 147
      Top = 58
      TabOrder = 3
      Visible = False
      ExplicitLeft = 147
      ExplicitTop = 58
    end
    inherited chbExportarPDF: TCheckBox
      Left = 5
      Top = 58
      TabOrder = 4
      ExplicitLeft = 5
      ExplicitTop = 58
    end
    inherited chbExportarExcel: TCheckBox
      Left = 5
      Top = 81
      TabOrder = 5
      ExplicitLeft = 5
      ExplicitTop = 81
    end
    object dteDataInicio: TJvDateEdit
      Left = 43
      Top = 28
      Width = 100
      Height = 22
      Color = 16114127
      ShowNullDate = False
      TabOrder = 0
      OnKeyPress = FormKeyPress
    end
    object dteDataFim: TJvDateEdit
      Left = 177
      Top = 28
      Width = 100
      Height = 22
      Color = 16114127
      ShowNullDate = False
      TabOrder = 1
      OnKeyPress = FormKeyPress
    end
  end
end
