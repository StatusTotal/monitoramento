object dmOutrasDespesasColaborador: TdmOutrasDespesasColaborador
  OldCreateOrder = False
  Height = 228
  Width = 345
  object qryFuncionarios: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT ID_FUNCIONARIO, NOME_FUNCIONARIO, CPF'
      '  FROM FUNCIONARIO'
      ' WHERE NOME_FUNCIONARIO <> '#39'FUNCION'#193'RIO'#39
      'ORDER BY NOME_FUNCIONARIO')
    Left = 172
    Top = 19
  end
  object dsFuncionarios: TDataSource
    DataSet = qryFuncionarios
    Left = 172
    Top = 67
  end
  object qryOutraDespFunc: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM OUTRADESPESA_FUNC'
      ' WHERE ID_OUTRADESPESA_FUNC = :ID_OUTRADESPESA_FUNC')
    Left = 40
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'ID_OUTRADESPESA_FUNC'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspOutraDespFunc: TDataSetProvider
    DataSet = qryOutraDespFunc
    Left = 40
    Top = 64
  end
  object cdsOutraDespFunc: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_OUTRADESPESA_FUNC'
        ParamType = ptInput
      end>
    ProviderName = 'dspOutraDespFunc'
    Left = 40
    Top = 112
    object cdsOutraDespFuncID_OUTRADESPESA_FUNC: TIntegerField
      FieldName = 'ID_OUTRADESPESA_FUNC'
      Required = True
    end
    object cdsOutraDespFuncID_FUNCIONARIO_FK: TIntegerField
      FieldName = 'ID_FUNCIONARIO_FK'
    end
    object cdsOutraDespFuncID_TIPO_OUTRADESP_FUNC_FK: TIntegerField
      FieldName = 'ID_TIPO_OUTRADESP_FUNC_FK'
    end
    object cdsOutraDespFuncDATA_OUTRADESPESA_FUNC: TDateField
      FieldName = 'DATA_OUTRADESPESA_FUNC'
    end
    object cdsOutraDespFuncVR_OUTRADESPESA_FUNC: TBCDField
      FieldName = 'VR_OUTRADESPESA_FUNC'
      OnGetText = cdsOutraDespFuncVR_OUTRADESPESA_FUNCGetText
      Precision = 18
      Size = 2
    end
    object cdsOutraDespFuncDESCR_OUTRADESPESA_FUNC: TStringField
      FieldName = 'DESCR_OUTRADESPESA_FUNC'
      Size = 1000
    end
    object cdsOutraDespFuncDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsOutraDespFuncCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsOutraDespFuncDATA_QUITACAO: TDateField
      FieldName = 'DATA_QUITACAO'
    end
    object cdsOutraDespFuncQUIT_ID_USUARIO: TIntegerField
      FieldName = 'QUIT_ID_USUARIO'
    end
    object cdsOutraDespFuncFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsOutraDespFuncDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsOutraDespFuncCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsOutraDespFuncID_FECHAMENTO_SALARIO_FK: TIntegerField
      FieldName = 'ID_FECHAMENTO_SALARIO_FK'
    end
    object cdsOutraDespFuncVR_ODESP_ANTERIOR: TCurrencyField
      FieldKind = fkInternalCalc
      FieldName = 'VR_ODESP_ANTERIOR'
    end
    object cdsOutraDespFuncFLG_MODALIDADE: TStringField
      FieldName = 'FLG_MODALIDADE'
      FixedChar = True
      Size = 1
    end
    object cdsOutraDespFuncFLG_RECORRENTE: TStringField
      FieldName = 'FLG_RECORRENTE'
      FixedChar = True
      Size = 1
    end
    object cdsOutraDespFuncFLG_REPLICADO: TStringField
      FieldName = 'FLG_REPLICADO'
      FixedChar = True
      Size = 1
    end
  end
  object dsOutraDespFunc: TDataSource
    DataSet = cdsOutraDespFunc
    Left = 40
    Top = 160
  end
  object dsTipoODespF: TDataSource
    DataSet = qryTipoODespF
    Left = 272
    Top = 64
  end
  object qryTipoODespF: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM TIPO_OUTRADESP_FUNC'
      'ORDER BY DESCR_TIPO_OUTRADESP_FUNC')
    Left = 272
    Top = 16
  end
end
