object FRepassesVincularDespesa: TFRepassesVincularDespesa
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Repasse de Receitas - Vincular '#224's Despesas'
  ClientHeight = 466
  ClientWidth = 1053
  Color = 11578959
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  object pnlRepasse: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 1047
    Height = 460
    Align = alClient
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 0
    object lblObservacoes: TLabel
      Left = 495
      Top = 372
      Width = 69
      Height = 14
      Caption = 'Observa'#231#245'es'
    end
    object btnConfirmarBaixa: TJvTransparentButton
      Left = 923
      Top = 432
      Width = 118
      Height = 22
      Hint = 'Confirmar dados da Baixa'
      Caption = 'Confirmar Baixa'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      FrameStyle = fsIndent
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      Spacing = 8
      TextAlign = ttaRight
      Transparent = False
      OnClick = btnConfirmarBaixaClick
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        1800000000000006000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFF4F9EFDDE4ECD8E0ECD8E0EFDDE4FFF4F9FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFAE6E6E6E2
        E2E2E2E2E2E6E6E6FAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFF3FAA3B6AA329064008A47008D49008D49008A47329064A3B6AAFFF3
        FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBABABAB6161613A3A3A39
        39393939393A3A3A616161ABABABFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFECF62F8D5F009A5B34CDA539DDB23AE1B33AE1B239DDB234CDA5009A5A2F8D
        5FFFECF6FFFFFFFFFFFFFFFFFFFFFFFFF8F8F85E5E5E47474788888892929295
        95959595959292928888884747475E5E5EF8F8F8FFFFFFFFFFFFFFFFFFFFF5FE
        1A8B550DB5833EE0B600D79700D49600D49700D49700D49600D7973EE0B50DB5
        831A8B55FFF5FEFFFFFFFFFFFFFFFFFF53535367676797979770707070707070
        7070707070707070707070979797676767535353FFFFFFFFFFFFFFFFFF75A78C
        00A97233DFB200D29500D29600CB8700CE8F00D39900D39900D29800D29533DF
        B200A97275A78CFFFFFFFFFFFF8E8E8E5959599191916D6D6D70707061616168
        68687373737373737272726D6D6D9191915959598E8E8EFFFFFFFFF6FE008641
        45DDB800D19400CF9404CE93FFFFFFA1EDD700C78400D29A00D19900D19900D1
        9445DDB6008641FFF6FEFFFFFF3333339999996D6D6D6D6D6D717171FFFFFFCA
        CACA5C5C5C7272727272727272726D6D6D999999333333FFFFFFA7C6B605AA75
        19D8A700CF9605CC93FFFFFFFFFFFFFFFFFF9DECD600C68400D09A00D09900CF
        9819D8A606AB74A5C9B6B9B3B65E5E5E8181816E6E6E707070FFFFFFFFFFFFFF
        FFFFC8C8C85B5B5B7272727272727070708282825E5E5EB6B6B678B7962EC69C
        00D09800C789FFFFFFFFFFFFA0EAD3FFFFFFFFFFFF9DEBD600C48400CF9A00CE
        9900D0982BC49974BC979C93978080806F6F6F5F5F5FFFFFFFFFFFFFC7C7C7FF
        FFFFFFFFFFC7C7C75B5B5B7171717070706F6F6F7F7F7F9898987CB79738D0A8
        00CC9400C99270E2C4B5EFDF00C18004CA92FFFFFFFFFFFF9DEAD600C28400CD
        9900CE972CC49979BC9A9D95998B8B8B6C6C6C696969AFAFAFD4D4D45454546F
        6F6FFFFFFFFFFFFFC8C8C85959597171716F6F6F7F7F7F9A9A9A75B49250DDB9
        00C99300CB9900C89100C78F00CB9900C99404C893FFFFFFFFFFFFA8ECD900C7
        8F00CC982BC49874BC979990949E9E9E6969697070706767676464647070706A
        6A6A6E6E6EFFFFFFFFFFFFCDCDCD6464646E6E6E7F7F7F989898B1D2BF38C69B
        18D3A700C99700CA9900CA9900CA9900CA9900C79404C793FFFFFFEFFBFA00C2
        8A19D3A604AA73B3D8C4C3BEC18585857F7F7F6C6C6C6E6E6E6E6E6E6E6E6E6E
        6E6E6969696F6F6FFFFFFFF6F6F65E5E5E8080805C5C5CC5C5C5FFFFFF07985C
        64F5D700BF8D00C89900C89900C89900C89900C89900C69421CCA116CC9E00C5
        9245DBB800853FFFFFFFFFFFFF525252B5B5B56262626F6F6F6F6F6F6F6F6F6F
        6F6F6F6F6F6969697F7F7F7A7A7A666666989898313131FFFFFFFFFFFF81BB9C
        6FE1C12FE0B800BF8D00C79900C79900C79900C79900C79900C69700C39233D8
        B300A76D90C3A8FFFFFFFFFFFF9E9E9EACACAC9191916363636E6E6E6E6E6E6D
        6D6D6D6D6D6D6D6D6A6A6A6565658E8E8E545454AAAAAAFFFFFFFFFFFFFFFFFF
        319B66A9FFF043E8C300BA8900C09200C29400C29400C29400C2963ED9B708B2
        7E2B9C68FFFFFFFFFFFFFFFFFFFFFFFF666666DADADA9E9E9E5C5C5C66666668
        6868686868686868676767959595636363656565FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF41A2706ED5B49FFFF64DECCA43E2C042E1BF45E1C140D6B00094524AA8
        7AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF717171A5A5A5DADADAA6A6A69B
        9B9B9B9B9B9D9D9D9292923E3E3E797979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFCDE3D568B7913AAD7D33AC7B20A671109D6363B68FDCEBE1FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D7D790909076767672
        72726666665959598D8D8DE3E3E3FFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
    end
    object btnCancelarBaixa: TJvTransparentButton
      Left = 923
      Top = 404
      Width = 118
      Height = 22
      Hint = 'Cancelar dados da Baixa'
      Caption = 'Cancelar Baixa'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      FrameStyle = fsIndent
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      Spacing = 11
      TextAlign = ttaRight
      Transparent = False
      OnClick = btnCancelarBaixaClick
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        1800000000000006000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFF7F5EAE4E2D2E6E4D4E6E4D4E6E4D4E7E5D4E5E3D2F7F5EAFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1F1F1DCDCDCDEDEDEDF
        DFDFDFDFDFDFDFDFDDDDDDF1F1F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFDFAE4424EB71426BA1829B91627B71526B71223B70C1EB6424FB7FEFB
        E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F2F27474745A5A5A5C5C5C5A
        5A5A595959565656545454737373F3F3F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FDFAE4414EB44252D57184FF6D80FF6D80FF6D81FF6E81FF7387FF3142D0414D
        B4FEFBE4FFFFFFFFFFFFFFFFFFFFFFFFF2F2F2727272808080B3B3B3AEAEAEAE
        AEAEAFAFAFAFAFAFB4B4B4747474727272F3F3F3FFFFFFFFFFFFFFFFFFFCFAE4
        434FB54B5BD8667AFF5469FA5E72FB5E71FB5E71FB5E72FB5469FA697DFF3041
        D0414DB5FEFBE4FFFFFFFFFFFFF2F2F2737373878787A9A9A99B9B9BA1A1A1A0
        A0A0A0A0A0A1A1A19B9B9BACACAC737373737373F3F3F3FFFFFFFFFCEA434FB5
        5464D95D72FF3C54F8A5AFFC3F55F85B6FF95B6FF93D55F8A5AFFC3C54F86579
        FF2F41CF404DB4FFFFF1F5F5F57373738D8D8DA4A4A48C8C8CC9C9C98D8D8D9E
        9E9E9E9E9E8D8D8DC9C9C98C8C8CA9A9A9737373727272F6F6F68991D35564DA
        556BFF435BF6C2CAFCFFFFFFC8CEFC324BF6324BF6CCD2FCFFFFFFC3CBFC435A
        F65E74FF2639CC9AA2E8ABAAA28E8E8E9E9E9E8F8F8FDADADAFFFFFFDEDEDE85
        8585858585E1E1E1FFFFFFDBDBDB909090A4A4A46D6D6DADADAD8E95D45E70E7
        4E64F83B54F4E1E5FDFFFFFFFFFFFFA6B1FAA6B1FAFFFFFFFFFFFFDDE1FC3C55
        F45368F9384CDC9EA6E8AEADA49898989696968A8A8AEEEEEEFFFFFFFFFFFFCA
        CACACACACAFFFFFFFFFFFFEBEBEB8A8A8A9A9A9A7F7F7FAFAFAF8E95D26373E7
        495FF54C62F32540F1DADFFDFFFFFFFFFFFFFFFFFFFFFFFFDADFFD2641F14C62
        F34C63F73A4DDA9EA6E8ADACA49C9C9C9292929494947C7C7CE9E9E9FFFFFFFF
        FFFFFFFFFFFFFFFFE9E9E97C7C7C9494949595957F7F7FAFAFAF8D94D26878E7
        425BF1475EF04960F00324EBF2F3FEFFFFFFFFFFFFF2F3FE0324EB4960F0475E
        F0475FF43B4FD99EA6E8ACABA39D9D9D8D8D8D8F8F8F919191666666F7F7F7FF
        FFFFFFFFFFF7F7F76666669191918F8F8F919191808080AFAFAF8C93D26C7CE9
        3D57EF435CEE1C3AEBB2BCF7FFFFFFFFFFFFFFFFFFFFFFFFB2BCF71C3AEB435C
        EE425BF23C50DA9FA6E8ACABA3A2A2A28989898C8C8C757575D0D0D0FFFFFFFF
        FFFFFFFFFFFFFFFFD0D0D07575758C8C8C8E8E8E808080AFAFAF8991D16E7FEA
        3852ED2B46EBB9C2F8FFFFFFFFFFFFBFC7F9BFC7F9FFFFFFFFFFFFB9C2F82B46
        EB3D57EE3D51DB9CA4E8ABAAA1A3A3A38686867D7D7DD4D4D4FFFFFFFFFFFFD8
        D8D8D8D8D8FFFFFFFFFFFFD4D4D47D7D7D898989818181AEAEAE8991D78894E5
        2241EB203DE8E3E6FBFFFFFFD5DAFA0F2EE60F2EE6D5DAFAFFFFFFE3E6FB203D
        E83350F03D4DD0959EE6ADACA4B0B0B0787878767676EDEDEDFFFFFFE5E5E56B
        6B6B6B6B6BE5E5E5FFFFFFEDEDED7676768585857C7C7CB1B1B1FFFFFF5C66CB
        97A1E91A3AE90F2EE4ABB6F50E2DE4354FE8354FE80E2DE4ABB6F50E2EE42B49
        ED4D5DD35963C9FFFFFFFFFFFF8A8A8ABABABA7474746A6A6ACBCBCB6A6A6A83
        83838383836A6A6ACBCBCB6A6A6A7F7F7F868686878787FFFFFFFFFFFFFFFFFF
        5E69CBA4AEEC1333E52240E5314CE6304BE6304BE6314CE6223FE42341EA5767
        D65964C9FFFFFFFFFFFFFFFFFFFFFFFF8C8C8CC3C3C36D6D6D7777777F7F7F7E
        7E7E7E7E7E7F7F7F7676767878788D8D8D888888FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF606BCCABB2EE0E30E51C3CE61D3DE61E3DE7203FE71A3BE9606FD95B66
        CAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EC7C7C76A6A6A73737374
        7474757575767676747474949494898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF737CD3727CD56974D26470D2616CD05A67CF5560CC6D78D1FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9B9B9B9C9C9C95959593
        93938F8F8F8B8B8B878787979797FFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
    end
    object lblDataPagamento: TLabel
      Left = 5
      Top = 416
      Width = 83
      Height = 14
      Caption = 'Dt. Pagamento'
    end
    object lblDescricaoBaixa: TLabel
      Left = 111
      Top = 416
      Width = 99
      Height = 14
      Caption = 'Descri'#231#227'o da Baixa'
    end
    object lblValorTotalFlutuante: TLabel
      Left = 5
      Top = 372
      Width = 98
      Height = 14
      Caption = 'Vlr. Total Repasse'
    end
    object lblValorTotalDespesa: TLabel
      Left = 131
      Top = 372
      Width = 99
      Height = 14
      Caption = 'Vlr. Total Despesa'
    end
    object gbRepasses: TGroupBox
      Left = 7
      Top = 8
      Width = 484
      Height = 358
      Caption = 'Repasses'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      object lblPeriodo: TLabel
        Left = 6
        Top = 16
        Width = 130
        Height = 14
        Caption = 'Per'#237'odo de Lan'#231'amento'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 109
        Top = 35
        Width = 6
        Height = 14
        Caption = 'a'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblValorFlutuante: TLabel
        Left = 224
        Top = 16
        Width = 27
        Height = 14
        Caption = 'Valor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblNumReciboFlutuante: TLabel
        Left = 330
        Top = 16
        Width = 54
        Height = 14
        Caption = 'N'#186' Recibo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object btnFiltrarReceitas: TJvTransparentButton
        Left = 264
        Top = 77
        Width = 83
        Height = 26
        Hint = 
          'Clique para Filtrar a pesquisa de acordo com as informa'#231#245'es forn' +
          'ecidas'
        Caption = 'Filtr&ar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        Transparent = False
        OnClick = btnFiltrarReceitasClick
      end
      object spLegendaVencido: TShape
        Left = 11
        Top = 342
        Width = 10
        Height = 10
        Brush.Color = 10797567
      end
      object lblLegendaSemBaixa: TLabel
        Left = 24
        Top = 339
        Width = 55
        Height = 14
        Caption = 'Sem Baixa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object spLegendaPago: TShape
        Left = 99
        Top = 342
        Width = 10
        Height = 10
        Brush.Color = 14680031
      end
      object lblLegendaBaixado: TLabel
        Left = 112
        Top = 339
        Width = 41
        Height = 14
        Caption = 'Baixado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object dtePeriodoIniR: TJvDateEdit
        Left = 5
        Top = 32
        Width = 100
        Height = 22
        Color = 16114127
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ShowNullDate = False
        TabOrder = 0
        OnKeyPress = dtePeriodoIniRKeyPress
      end
      object dtePeriodoFimR: TJvDateEdit
        Left = 118
        Top = 32
        Width = 100
        Height = 22
        Color = 16114127
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ShowNullDate = False
        TabOrder = 1
        OnKeyPress = dtePeriodoFimRKeyPress
      end
      object cedValorFlutuanteR: TJvCalcEdit
        Left = 224
        Top = 32
        Width = 100
        Height = 22
        Color = 16114127
        DisplayFormat = ',0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        DecimalPlacesAlwaysShown = False
        OnKeyPress = cedValorFlutuanteRKeyPress
      end
      object edtNumReciboFlutuante: TEdit
        Left = 330
        Top = 32
        Width = 100
        Height = 22
        Color = 16114127
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        OnKeyPress = edtNumReciboFlutuanteKeyPress
      end
      object rgSituacao: TRadioGroup
        Left = 5
        Top = 60
        Width = 253
        Height = 43
        Caption = 'Situa'#231#227'o'
        Columns = 3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemIndex = 1
        Items.Strings = (
          'Todos'
          'Sem Baixa'
          'Baixados')
        ParentFont = False
        TabOrder = 4
        OnClick = rgSituacaoClick
        OnExit = rgSituacaoExit
      end
      object chbSelecionarTodas: TCheckBox
        Left = 386
        Top = 86
        Width = 93
        Height = 17
        Caption = 'Marcar Todas'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        OnClick = chbSelecionarTodasClick
        OnKeyPress = FormKeyPress
      end
      object dbgRepasses: TJvDBUltimGrid
        Left = 5
        Top = 109
        Width = 474
        Height = 224
        DataSource = dsBaixaLancG
        Options = [dgTitles, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 6
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        OnCellClick = dbgRepassesCellClick
        OnDrawColumnCell = dbgRepassesDrawColumnCell
        OnTitleClick = dbgRepassesTitleClick
        MinColumnWidth = 26
        AutoSizeColumnIndex = 0
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 18
        TitleRowHeight = 18
        Columns = <
          item
            Expanded = False
            FieldName = 'SELECIONADO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Alignment = taCenter
            Title.Caption = 'Sel.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 26
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'NUM_RECIBO'
            Title.Alignment = taCenter
            Title.Caption = 'Recibo'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Caption = 'Descri'#231#227'o'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 176
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA_LANCAMENTO_PARC'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Caption = 'Dt. Entrada'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 84
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VR_PARCELA_PREV'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Caption = 'Vlr. Previsto'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_PARCELA_PAGO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Caption = 'Vlr. Pago'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 86
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COD_LANCAMENTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ANO_LANCAMENTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_LANCAMENTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'TIPO_LANCAMENTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'TIPO_CADASTRO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_CATEGORIA_DESPREC_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_SUBCATEGORIA_DESPREC_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_NATUREZA_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_CLIENTE_FORNECEDOR_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_IMPOSTORENDA'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_PESSOAL'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_AUXILIAR'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_REAL'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_FLUTUANTE'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'QTD_PARCELAS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_TOTAL_PREV'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_TOTAL_JUROS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_TOTAL_DESCONTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_TOTAL_PAGO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'CAD_ID_USUARIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_CADASTRO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_STATUS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_CANCELADO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_CANCELAMENTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'CANCEL_ID_USUARIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_RECORRENTE'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_REPLICADO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_ORIGEM'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_SISTEMA_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'OBSERVACAO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_CARNELEAO_EQUIVALENCIA_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_LANCAMENTO_PARC'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'COD_LANCAMENTO_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ANO_LANCAMENTO_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'NUM_PARCELA'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_VENCIMENTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_PARCELA_JUROS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_PARCELA_DESCONTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_FORMAPAGAMENTO_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'AGENCIA'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'CONTA'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_BANCO_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'NUM_CHEQUE'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'NUM_COD_DEPOSITO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_STATUS_1'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_CANCELAMENTO_1'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'CANCEL_ID_USUARIO_1'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_PAGAMENTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'PAG_ID_USUARIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'OBS_LANCAMENTO_PARC'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'CAD_ID_USUARIO_1'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_CADASTRO_1'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_STATUS_PARC'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_FORAFECHCAIXA'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_FORMAPAGAMENTO_ORIG_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'BAIXADO'
            Visible = False
          end>
      end
    end
    object gbDespesas: TGroupBox
      Left = 495
      Top = 8
      Width = 546
      Height = 358
      Caption = 'Despesas'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      object Label1: TLabel
        Left = 5
        Top = 16
        Width = 130
        Height = 14
        Caption = 'Per'#237'odo de Lan'#231'amento'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 108
        Top = 35
        Width = 6
        Height = 14
        Caption = 'a'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 223
        Top = 16
        Width = 27
        Height = 14
        Caption = 'Valor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object btnFiltrarDespesas: TJvTransparentButton
        Left = 457
        Top = 28
        Width = 83
        Height = 26
        Hint = 
          'Clique para Filtrar a pesquisa de acordo com as informa'#231#245'es forn' +
          'ecidas'
        Caption = 'Filtr&ar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        Transparent = False
        OnClick = btnFiltrarDespesasClick
      end
      object btnConfirmarDespesa: TJvTransparentButton
        Left = 2
        Top = 333
        Width = 542
        Height = 23
        Align = alBottom
        Caption = '&Selecionar Despesa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        ParentShowHint = False
        ShowHint = False
        Spacing = 208
        TextAlign = ttaRight
        Transparent = False
        OnClick = btnConfirmarDespesaClick
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          1800000000000006000000000000000000000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF4F9EFDDE4ECD8E0ECD8E0EFDDE4FFF4F9FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFAE6E6E6E2
          E2E2E2E2E2E6E6E6FAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFF3FAA3B6AA329064008A47008D49008D49008A47329064A3B6AAFFF3
          FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBABABAB6161613A3A3A39
          39393939393A3A3A616161ABABABFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFECF62F8D5F009A5B34CDA539DDB23AE1B33AE1B239DDB234CDA5009A5A2F8D
          5FFFECF6FFFFFFFFFFFFFFFFFFFFFFFFF8F8F85E5E5E47474788888892929295
          95959595959292928888884747475E5E5EF8F8F8FFFFFFFFFFFFFFFFFFFFF5FE
          1A8B550DB5833EE0B600D79700D49600D49700D49700D49600D7973EE0B50DB5
          831A8B55FFF5FEFFFFFFFFFFFFFFFFFF53535367676797979770707070707070
          7070707070707070707070979797676767535353FFFFFFFFFFFFFFFFFF75A78C
          00A97233DFB200D29500D29600CB8700CE8F00D39900D39900D29800D29533DF
          B200A97275A78CFFFFFFFFFFFF8E8E8E5959599191916D6D6D70707061616168
          68687373737373737272726D6D6D9191915959598E8E8EFFFFFFFFF6FE008641
          45DDB800D19400CF9404CE93FFFFFFA1EDD700C78400D29A00D19900D19900D1
          9445DDB6008641FFF6FEFFFFFF3333339999996D6D6D6D6D6D717171FFFFFFCA
          CACA5C5C5C7272727272727272726D6D6D999999333333FFFFFFA7C6B605AA75
          19D8A700CF9605CC93FFFFFFFFFFFFFFFFFF9DECD600C68400D09A00D09900CF
          9819D8A606AB74A5C9B6B9B3B65E5E5E8181816E6E6E707070FFFFFFFFFFFFFF
          FFFFC8C8C85B5B5B7272727272727070708282825E5E5EB6B6B678B7962EC69C
          00D09800C789FFFFFFFFFFFFA0EAD3FFFFFFFFFFFF9DEBD600C48400CF9A00CE
          9900D0982BC49974BC979C93978080806F6F6F5F5F5FFFFFFFFFFFFFC7C7C7FF
          FFFFFFFFFFC7C7C75B5B5B7171717070706F6F6F7F7F7F9898987CB79738D0A8
          00CC9400C99270E2C4B5EFDF00C18004CA92FFFFFFFFFFFF9DEAD600C28400CD
          9900CE972CC49979BC9A9D95998B8B8B6C6C6C696969AFAFAFD4D4D45454546F
          6F6FFFFFFFFFFFFFC8C8C85959597171716F6F6F7F7F7F9A9A9A75B49250DDB9
          00C99300CB9900C89100C78F00CB9900C99404C893FFFFFFFFFFFFA8ECD900C7
          8F00CC982BC49874BC979990949E9E9E6969697070706767676464647070706A
          6A6A6E6E6EFFFFFFFFFFFFCDCDCD6464646E6E6E7F7F7F989898B1D2BF38C69B
          18D3A700C99700CA9900CA9900CA9900CA9900C79404C793FFFFFFEFFBFA00C2
          8A19D3A604AA73B3D8C4C3BEC18585857F7F7F6C6C6C6E6E6E6E6E6E6E6E6E6E
          6E6E6969696F6F6FFFFFFFF6F6F65E5E5E8080805C5C5CC5C5C5FFFFFF07985C
          64F5D700BF8D00C89900C89900C89900C89900C89900C69421CCA116CC9E00C5
          9245DBB800853FFFFFFFFFFFFF525252B5B5B56262626F6F6F6F6F6F6F6F6F6F
          6F6F6F6F6F6969697F7F7F7A7A7A666666989898313131FFFFFFFFFFFF81BB9C
          6FE1C12FE0B800BF8D00C79900C79900C79900C79900C79900C69700C39233D8
          B300A76D90C3A8FFFFFFFFFFFF9E9E9EACACAC9191916363636E6E6E6E6E6E6D
          6D6D6D6D6D6D6D6D6A6A6A6565658E8E8E545454AAAAAAFFFFFFFFFFFFFFFFFF
          319B66A9FFF043E8C300BA8900C09200C29400C29400C29400C2963ED9B708B2
          7E2B9C68FFFFFFFFFFFFFFFFFFFFFFFF666666DADADA9E9E9E5C5C5C66666668
          6868686868686868676767959595636363656565FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF41A2706ED5B49FFFF64DECCA43E2C042E1BF45E1C140D6B00094524AA8
          7AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF717171A5A5A5DADADAA6A6A69B
          9B9B9B9B9B9D9D9D9292923E3E3E797979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFCDE3D568B7913AAD7D33AC7B20A671109D6363B68FDCEBE1FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D7D790909076767672
          72726666665959598D8D8DE3E3E3FFFFFFFFFFFFFFFFFFFFFFFF}
        NumGlyphs = 2
        ExplicitLeft = 0
        ExplicitTop = 368
        ExplicitWidth = 472
      end
      object dtePeriodoIniD: TJvDateEdit
        Left = 5
        Top = 32
        Width = 100
        Height = 22
        Color = 16114127
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ShowNullDate = False
        TabOrder = 0
        OnKeyPress = dtePeriodoIniDKeyPress
      end
      object dtePeriodoFimD: TJvDateEdit
        Left = 117
        Top = 32
        Width = 100
        Height = 22
        Color = 16114127
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ShowNullDate = False
        TabOrder = 1
        OnKeyPress = dtePeriodoFimDKeyPress
      end
      object cedValorFlutuanteD: TJvCalcEdit
        Left = 223
        Top = 32
        Width = 100
        Height = 22
        Color = 16114127
        DisplayFormat = ',0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        DecimalPlacesAlwaysShown = False
        OnKeyPress = cedValorFlutuanteDKeyPress
      end
      object dbgDespesasPagas: TDBGrid
        Left = 5
        Top = 60
        Width = 535
        Height = 267
        Color = clWhite
        DataSource = dsDespPagas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        ParentFont = False
        TabOrder = 3
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        OnDrawColumnCell = dbgDespesasPagasDrawColumnCell
        OnKeyPress = FormKeyPress
        OnTitleClick = dbgDespesasPagasTitleClick
        Columns = <
          item
            Expanded = False
            FieldName = 'DESCR_NATUREZA'
            Title.Caption = 'Natureza'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCR_CATEGORIA_DESPREC'
            Title.Caption = 'Categoria'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCR_SUBCATEGORIA_DESPREC'
            Title.Caption = 'Subcategoria'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA_LANCAMENTO_PARC'
            Title.Caption = 'Dt. Lanc.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 84
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VR_PARCELA_PAGO'
            Title.Caption = 'Vlr. Pago'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 86
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'BAIXADO'
            Title.Alignment = taCenter
            Title.Caption = 'Vinc.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COD_LANCAMENTO_FK'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ANO_LANCAMENTO_FK'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_LANCAMENTO_PARC'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_PARCELA_PREV'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_PAGAMENTO'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_NATUREZA_FK'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_CATEGORIA_DESPREC_FK'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_SUBCATEGORIA_DESPREC_FK'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'OBSERVACAO'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end>
      end
    end
    object edtDescricaoBaixa: TEdit
      Left = 111
      Top = 432
      Width = 378
      Height = 22
      Color = 16114127
      TabOrder = 5
      OnKeyPress = FormKeyPress
    end
    object dteDataPagamento: TJvDateEdit
      Left = 5
      Top = 432
      Width = 100
      Height = 22
      Color = 16114127
      ShowNullDate = False
      TabOrder = 4
      OnExit = dteDataPagamentoExit
      OnKeyPress = dteDataPagamentoKeyPress
    end
    object cedValorTotalRepasse: TJvCalcEdit
      Left = 5
      Top = 388
      Width = 120
      Height = 22
      Color = 16114127
      DisplayFormat = ',0.00'
      Enabled = False
      TabOrder = 2
      DecimalPlacesAlwaysShown = False
      OnKeyPress = FormKeyPress
    end
    object cedValorTotalDespesa: TJvCalcEdit
      Left = 131
      Top = 388
      Width = 120
      Height = 22
      Color = 16114127
      DisplayFormat = ',0.00'
      Enabled = False
      TabOrder = 3
      DecimalPlacesAlwaysShown = False
      OnKeyPress = FormKeyPress
    end
    object mmObservacoes: TMemo
      Left = 495
      Top = 388
      Width = 422
      Height = 66
      Color = 16114127
      TabOrder = 6
    end
  end
  object qryQtdParcs: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT COUNT(ID_LANCAMENTO_PARC) AS QTD,'
      '       SUM(VR_PARCELA_PREV) AS TOTAL_PREV,'
      '       SUM(VR_PARCELA_PAGO) AS TOTAL_PAGO'
      '  FROM LANCAMENTO_PARC'
      ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO'
      '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO'
      '   AND FLG_STATUS = '#39'G'#39)
    Left = 342
    Top = 384
    ParamData = <
      item
        Name = 'COD_LANCAMENTO'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'ANO_LANCAMENTO'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryQtdParcsQTD: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'QTD'
      Origin = 'QTD'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryQtdParcsTOTAL_PREV: TBCDField
      AutoGenerateValue = arDefault
      FieldName = 'TOTAL_PREV'
      Origin = 'TOTAL_PREV'
      ProviderFlags = []
      ReadOnly = True
      Precision = 18
      Size = 2
    end
    object qryQtdParcsTOTAL_PAGO: TBCDField
      AutoGenerateValue = arDefault
      FieldName = 'TOTAL_PAGO'
      Origin = 'TOTAL_PAGO'
      ProviderFlags = []
      ReadOnly = True
      Precision = 18
      Size = 2
    end
  end
  object dsQtdParcs: TDataSource
    DataSet = qryQtdParcs
    Left = 406
    Top = 384
  end
  object dsDespPagas: TDataSource
    DataSet = dmBaixa.cdsDespPagas
    Left = 979
    Top = 246
  end
  object dsBaixaLancG: TDataSource
    DataSet = dmBaixa.cdsBaixaLancG
    Left = 424
    Top = 274
  end
end
