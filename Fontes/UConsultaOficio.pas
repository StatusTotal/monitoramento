{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UConsultaOficio.pas
  Descricao:   Formulario de Visualizacao de Oficio
  Author   :   Cristina
  Date:        02-fev-2017
  Last Update: 25-jan-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UConsultaOficio;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UConsultaGeralPadrao, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, JvExControls, JvButton,
  JvTransparentButton, Vcl.ExtCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, dxSkinMetropolis,
  Vcl.Grids, Vcl.DBGrids, dxGDIPlusClasses, cxImage, Vcl.StdCtrls, Vcl.Mask,
  Vcl.DBCtrls, JvToolEdit, JvDBControls, JvExMask, JvMaskEdit,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, gtPDFClasses,
  gtCstPDFDoc, gtExPDFDoc, gtExProPDFDoc, gtPDFDoc, gtScrollingPanel,
  gtPDFViewer;

type
  TFConsultaOficio = class(TFConsultaGeralPadrao)
    lblTipoOficio: TLabel;
    lblNumControle: TLabel;
    edtNumControle: TDBEdit;
    dbgImagens: TDBGrid;
    dsImagem: TDataSource;
    qryImagem: TFDQuery;
    lblCodOficio: TLabel;
    edtCodOficio: TDBEdit;
    Label2: TLabel;
    edtAnoOficio: TDBEdit;
    lblAnoOficio: TLabel;
    chbReservado: TDBCheckBox;
    medHoraOficio: TJvDBMaskEdit;
    lblHoraOficio: TLabel;
    lblDataOficio: TLabel;
    dteDataOficio: TJvDBDateEdit;
    lblAssunto: TLabel;
    lblPessoa: TLabel;
    lblFormaEnvio: TLabel;
    lblNumProcesso: TLabel;
    lblOrgao: TLabel;
    lblObservacao: TLabel;
    lblNumRastreio: TLabel;
    lblNumProtocolo: TLabel;
    lblDataSituacaoAR: TLabel;
    lblFuncionario: TLabel;
    edtAssunto: TDBEdit;
    edtNumProcesso: TDBEdit;
    edtNumRastreio: TDBEdit;
    dteDataSituacaoAR: TJvDBDateEdit;
    mmObservacao: TDBMemo;
    edtNumProtocolo: TDBEdit;
    rgSituacaoAR: TDBRadioGroup;
    qryConsultaID: TIntegerField;
    qryConsultaCOD_OFICIO: TIntegerField;
    qryConsultaANO_OFICIO: TIntegerField;
    qryConsultaTIPO_OFICIO: TStringField;
    qryConsultaDATA_OFICIO: TDateField;
    qryConsultaHORA_OFICIO: TTimeField;
    qryConsultaID_FUNCIONARIO_FK: TIntegerField;
    qryConsultaNOME_FUNCIONARIO: TStringField;
    qryConsultaCAD_ID_USUARIO: TIntegerField;
    qryConsultaDATA_CADASTRO: TSQLTimeStampField;
    qryConsultaFLG_STATUS: TStringField;
    qryConsultaSTA_ID_USUARIO: TIntegerField;
    qryConsultaDATA_STATUS: TSQLTimeStampField;
    qryConsultaASSUNTO_OFICIO: TStringField;
    qryConsultaID_OFICIO_PESSOA_FK: TIntegerField;
    qryConsultaNOME_OFICIO_PESSOA: TStringField;
    qryConsultaNUM_PROCESSO: TStringField;
    qryConsultaID_OFICIO_ORGAO_FK: TIntegerField;
    qryConsultaNOME_OFICIO_ORGAO: TStringField;
    qryConsultaID_OFICIO_FORMAENVIO_FK: TIntegerField;
    qryConsultaDESCR_OFICIO_FORMAENVIO: TStringField;
    qryConsultaNUM_RASTREIO: TStringField;
    qryConsultaNUM_PROTOCOLO: TStringField;
    qryConsultaDATA_SITUACAO_AR: TDateField;
    qryConsultaFLG_SITUACAO_AR: TStringField;
    qryConsultaOBS_OFICIO: TStringField;
    qryConsultaID_OFICIO_FK: TIntegerField;
    qryConsultaSEQ_OFICIO: TIntegerField;
    qryImagemID_OFICIO_IMG: TIntegerField;
    qryImagemID_OFICIO_FK: TIntegerField;
    qryImagemNOME_FIXO: TStringField;
    qryImagemNOME_VARIAVEL: TStringField;
    qryImagemEXTENSAO: TStringField;
    qryImagemCAMINHO_COMPLETO: TStringField;
    edtPessoa: TDBEdit;
    edtOrgao: TDBEdit;
    edtFormaEnvio: TDBEdit;
    edtFuncionario: TDBEdit;
    ntbArquivo: TNotebook;
    pnlImagem: TPanel;
    imgImagemOficio: TcxImage;
    vwPDF: TgtPDFViewer;
    docPDF: TgtPDFDocument;
    procedure dbgImagensColEnter(Sender: TObject);
    procedure dbgImagensDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure qryImagemCalcFields(DataSet: TDataSet);
    procedure btnFecharClick(Sender: TObject);
    procedure FormShow(Sender: TObject);   
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;
    procedure GravarLog; override;
  private
    { Private declarations }
  public
    { Public declarations }
    IdOficio: Integer;
  end;

var
  FConsultaOficio: TFConsultaOficio;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

procedure TFConsultaOficio.btnFecharClick(Sender: TObject);
begin
  qryConsulta.Close;
  qryImagem.Close;

  inherited;
end;

procedure TFConsultaOficio.dbgImagensColEnter(Sender: TObject);
begin
  inherited;

  if qryImagem.FieldByName('CAMINHO_COMPLETO').AsString <> '' then
  begin
    if qryImagem.FieldByName('EXTENSAO').AsString = 'jpg' then
    begin
      ntbArquivo.PageIndex := 0;
      imgImagemOficio.Picture.LoadFromFile(qryImagem.FieldByName('CAMINHO_COMPLETO').AsString);
    end
    else if qryImagem.FieldByName('EXTENSAO').AsString = 'pdf' then
    begin
      ntbArquivo.PageIndex := 1;
      docPDF.Reset;
      docPDF.LoadFromFile(qryImagem.FieldByName('CAMINHO_COMPLETO').AsString);
      vwPDF.Align := alClient;
      vwPDF.PDFDocument := docPDF;
      vwPDF.Active := True;
    end;
  end;
end;

procedure TFConsultaOficio.dbgImagensDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;

  AlternarCorGrid(Sender, Rect, DataCol, Column, State);
end;

procedure TFConsultaOficio.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtNumControle.MaxLength  := 8;
  edtAssunto.MaxLength      := 250;
  edtPessoa.MaxLength       := 250;
  edtNumProcesso.MaxLength  := 25;
  edtOrgao.MaxLength        := 250;
  edtNumProtocolo.MaxLength := 25;
  edtFormaEnvio.MaxLength   := 150;
  edtNumRastreio.MaxLength  := 25;
  mmObservacao.MaxLength    := 1000;
  edtFuncionario.MaxLength  := 250;
end;

procedure TFConsultaOficio.DesabilitarComponentes;
begin
  inherited;

  { OFICIO }
  dteDataOficio.ReadOnly     := True;
  medHoraOficio.ReadOnly     := True;
  edtCodOficio.ReadOnly      := True;
  edtAnoOficio.ReadOnly      := True;
  edtAssunto.ReadOnly        := True;
  edtPessoa.ReadOnly         := True;
  edtNumProcesso.ReadOnly    := True;
  edtOrgao.ReadOnly          := True;
  edtNumProtocolo.ReadOnly   := True;
  edtFormaEnvio.ReadOnly     := True;
  edtNumRastreio.ReadOnly    := True;
  rgSituacaoAR.ReadOnly      := True;
  dteDataSituacaoAR.ReadOnly := True;
  mmObservacao.ReadOnly      := True;
  edtFuncionario.ReadOnly    := True;

  { IMAGEM }
  dbgImagens.ReadOnly := True;
end;

procedure TFConsultaOficio.FormShow(Sender: TObject);
begin       
  { OFICIO }
  qryConsulta.Close;
  qryConsulta.Params.ParamByName('ID_OFICIO').Value := idOficio;
  qryConsulta.Open;

  vgOfc_Codigo := qryConsulta.FieldByName('COD_OFICIO').AsInteger;
  vgOfc_Ano    := qryConsulta.FieldByName('ANO_OFICIO').AsInteger;
  vgOfc_TpOfc  := qryConsulta.FieldByName('TIPO_OFICIO').AsString;
  vgOfc_Status := qryConsulta.FieldByName('FLG_STATUS').AsString;

  if qryConsulta.FieldByName('TIPO_OFICIO').AsString = 'E' then
    lblTipoOficio.Caption := 'OF�CIO ENVIADO'
  else if qryConsulta.FieldByName('TIPO_OFICIO').AsString = 'R' then
    lblTipoOficio.Caption := 'OF�CIO RECEBIDO';

  { IMAGEM }
  qryImagem.Close;
  qryImagem.Params.ParamByName('ID_OFICIO').Value := idOficio;
  qryImagem.Open;

  if qryImagem.FieldByName('CAMINHO_COMPLETO').AsString <> '' then
    imgImagemOficio.Picture.LoadFromFile(qryImagem.FieldByName('CAMINHO_COMPLETO').AsString);

  inherited;
end;

procedure TFConsultaOficio.GravarLog;
begin
  inherited;

  //CONSULTA
  BS.GravarUsuarioLog(3, '', 'Somente consulta de ' + lblTipoOficio.Caption);
end;

procedure TFConsultaOficio.qryImagemCalcFields(DataSet: TDataSet);
begin
  inherited;

  qryImagem.FieldByName('CAMINHO_COMPLETO').AsString := vgConf_DiretorioImagens +
                                                        IntToStr(vgOfc_Ano) +
                                                        '\' +
                                                        qryImagem.FieldByName('NOME_FIXO').AsString +
                                                        qryImagem.FieldByName('NOME_VARIAVEL').AsString + '.' +
                                                        qryImagem.FieldByName('EXTENSAO').AsString;
end;

end.
