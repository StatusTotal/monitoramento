inherited FCadastroEtiqueta: TFCadastroEtiqueta
  Caption = 'Cadastro de Etiqueta de Seguran'#231'a'
  ClientHeight = 329
  ClientWidth = 583
  ExplicitWidth = 589
  ExplicitHeight = 358
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 577
    Height = 323
    ExplicitWidth = 577
    ExplicitHeight = 323
    inherited pnlDados: TPanel
      Width = 463
      Height = 317
      ExplicitWidth = 463
      ExplicitHeight = 317
      object lblNomeFuncionario: TLabel [0]
        Left = 7
        Top = 52
        Width = 100
        Height = 14
        Caption = 'Nome Colaborador'
      end
      object lblNatureza: TLabel [1]
        Left = 7
        Top = 96
        Width = 48
        Height = 14
        Caption = 'Natureza'
      end
      object btnIncluirNatureza: TJvTransparentButton [2]
        Left = 439
        Top = 113
        Width = 22
        Height = 22
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        Transparent = False
        OnClick = btnIncluirNaturezaClick
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          1800000000000006000000000000000000000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5F5F5DADADACCCCCCCC
          CCCCCCCCCCCCCCCCDADADAF5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFDDDDDDA3C0B3369D6E008C4B008B4A008B4A008C4B369D6EA3C0B3E1E1
          E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDDB2B2B26A6A6A47474746
          46464646464747476A6A6AB2B2B2E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          E1E1E144A27700905001A16900AA7600AB7700AB7700AA7601A16900905055A8
          82E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFE1E1E17474744A4A4A5656565C5C5C5C
          5C5C5C5C5C5C5C5C5656564A4A4A7F7F7FE1E1E1FFFFFFFFFFFFFFFFFFF5F5F5
          55A88200915202AC7700C38C00D69918DEA818DEA800D69900C38C01AB760092
          5355A882F5F5F5FFFFFFFFFFFFF5F5F57F7F7F4A4A4A5D5D5D6A6A6A74747484
          84848484847474746A6A6A5C5C5C4B4B4B7F7F7FF5F5F5FFFFFFFFFFFFAECABE
          0090510FB48302D29900D69B00D193FFFFFFFFFFFF00D19300D69B00D19801AB
          76009050AECBBEFFFFFFFFFFFFBCBCBC4A4A4A686868737373757575717171FF
          FFFFFFFFFF7171717575757272725C5C5C4A4A4ABDBDBDFFFFFFFFFFFF369D6C
          16AB7811C99700D49A00D29700CD8EFFFFFFFFFFFF00CD8E00D29700D59B00C1
          8C01A169369E6EFFFFFFFFFFFF6A6A6A6565657575757474747272726E6E6EFF
          FFFFFFFFFF6E6E6E7272727474746969695656566B6B6BFFFFFFFFFFFF008A48
          38C49C00D19800CD9200CB8E00C787FFFFFFFFFFFF00C78700CB8E00CE9300D0
          9A00AB76008C4BFFFFFFFFFFFF4646468484847272726F6F6F6E6E6E6B6B6BFF
          FFFFFFFFFF6B6B6B6E6E6E7070707272725C5C5C474747FFFFFFFFFFFF008946
          51D2AF12D4A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CF
          9700AD78008B4AFFFFFFFFFFFF4545459797977D7D7DFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF7171715D5D5D464646FFFFFFFFFFFF008845
          66DDBE10D0A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CD
          9700AD78008B4AFFFFFFFFFFFF444444A7A7A77A7A7AFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF7070705D5D5D464646FFFFFFFFFFFF008846
          76E0C500CA9800C59000C48E00C187FFFFFFFFFFFF00C18700C48E00C79300CB
          9900AB76008C4BFFFFFFFFFFFF444444B0B0B06F6F6F6C6C6C6B6B6B686868FF
          FFFFFFFFFF6868686B6B6B6D6D6D7070705C5C5C474747FFFFFFFFFFFF41A675
          59C9A449DEBC00C79400C79400C38EFFFFFFFFFFFF00C38E00C89600CB9A06C1
          9000A16840A878FFFFFFFFFFFF7474749595959C9C9C6D6D6D6D6D6D6A6A6AFF
          FFFFFFFFFF6A6A6A6E6E6E7070706C6C6C555555757575FFFFFFFFFFFFCCE8DB
          0A9458ADF8E918D0A700C49400C290FFFFFFFFFFFF00C39100C79905C89B18B7
          87009050CCE8DBFFFFFFFFFFFFDADADA515151D7D7D77E7E7E6C6C6C6A6A6AFF
          FFFFFFFFFF6B6B6B6E6E6E7171716E6E6E4A4A4ADADADAFFFFFFFFFFFFFFFFFF
          55B185199C63BCFFF75DE4C900C39700BF9000C09100C49822CAA231C2970393
          556ABC96FFFFFFFFFFFFFFFFFFFFFFFF8383835C5C5CE3E3E3A9A9A96C6C6C69
          69696A6A6A6D6D6D7F7F7F7F7F7F4D4D4D949494FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF6ABB940E965974D5B69FF3E092EFDA79E5CA5DD6B52EB58603915255B2
          88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF939393535353A8A8A8CECECEC6
          C6C6B4B4B49F9F9F7676764C4C4C848484FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFCCE8DB44A87700874400874300874400894644AA7ACCE8DBFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDADADA76767644444443
          4343444444454545787878DADADAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        NumGlyphs = 2
      end
      object lblSistema: TLabel [3]
        Left = 269
        Top = 52
        Width = 42
        Height = 14
        Caption = 'Sistema'
      end
      inherited pnlMsgErro: TPanel
        Top = 256
        Width = 463
        TabOrder = 9
        ExplicitTop = 256
        ExplicitWidth = 463
        inherited lblMensagemErro: TLabel
          Width = 463
        end
        inherited lbMensagemErro: TListBox
          Width = 463
          OnDblClick = lbMensagemErroDblClick
          ExplicitWidth = 463
        end
      end
      object edDataPratica: TsDBDateEdit
        Left = 142
        Top = 20
        Width = 121
        Height = 22
        AutoSize = False
        EditMask = '!99/99/9999;1; '
        MaxLength = 10
        TabOrder = 1
        Text = '  /  /    '
        OnKeyPress = FormKeyPress
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Data Pr'#225'tica'
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        DataField = 'DATA_UTILIZACAO'
        DataSource = dsCadastro
      end
      object edFolha: TsDBEdit
        Left = 7
        Top = 20
        Width = 129
        Height = 22
        CharCase = ecUpperCase
        DataField = 'NUM_ETIQUETA'
        DataSource = dsCadastro
        ReadOnly = True
        TabOrder = 0
        OnKeyPress = FormKeyPress
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Numera'#231#227'o Etiqueta'
        BoundLabel.Layout = sclTopLeft
      end
      object edLetra: TsDBEdit
        Left = 269
        Top = 20
        Width = 56
        Height = 22
        CharCase = ecUpperCase
        DataField = 'LETRA'
        DataSource = dsCadastro
        TabOrder = 2
        OnKeyPress = FormKeyPress
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Letra'
        BoundLabel.Layout = sclTopLeft
      end
      object edNumero: TsDBEdit
        Left = 331
        Top = 20
        Width = 65
        Height = 22
        CharCase = ecUpperCase
        DataField = 'NUMERO'
        DataSource = dsCadastro
        MaxLength = 5
        TabOrder = 3
        OnKeyPress = FormKeyPress
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'N'#250'mero'
        BoundLabel.Layout = sclTopLeft
      end
      object edAleatorio: TsDBEdit
        Left = 404
        Top = 20
        Width = 57
        Height = 22
        CharCase = ecUpperCase
        DataField = 'ALEATORIO'
        DataSource = dsCadastro
        TabOrder = 4
        OnKeyPress = FormKeyPress
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Aleat'#243'rio'
        BoundLabel.Layout = sclTopLeft
      end
      object MMObs: TsDBMemo
        Left = 7
        Top = 155
        Width = 454
        Height = 95
        DataField = 'OBS_ETIQUETA'
        DataSource = dsCadastro
        TabOrder = 8
        OnKeyPress = MMObsKeyPress
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Observa'#231#227'o do Ato'
        BoundLabel.Layout = sclTopLeft
        CharCase = ecUpperCase
      end
      object lcbNomeFuncionario: TDBLookupComboBox
        Left = 7
        Top = 68
        Width = 256
        Height = 22
        Color = 16114127
        DataField = 'ID_FUNCIONARIO_FK'
        DataSource = dsCadastro
        KeyField = 'ID_FUNCIONARIO'
        ListField = 'NOME_FUNCIONARIO'
        ListSource = dmEtiqueta.dsFuncionarios
        NullValueKey = 16460
        TabOrder = 5
        OnKeyPress = FormKeyPress
      end
      object lcbNatureza: TDBLookupComboBox
        Left = 7
        Top = 113
        Width = 430
        Height = 22
        Color = 16114127
        DataField = 'ID_NATUREZAATO_FK'
        DataSource = dsCadastro
        KeyField = 'ID_NATUREZAATO'
        ListField = 'DESCR_NATUREZAATO'
        ListSource = dmEtiqueta.dsNatAto
        TabOrder = 7
        OnKeyPress = FormKeyPress
      end
      object lcbSistema: TDBLookupComboBox
        Left = 269
        Top = 68
        Width = 192
        Height = 22
        Color = 16114127
        DataField = 'ID_SISTEMA_FK'
        DataSource = dsCadastro
        KeyField = 'ID_SISTEMA'
        ListField = 'NOME_SISTEMA'
        ListSource = dmEtiqueta.dsSistema
        TabOrder = 6
        OnClick = lcbSistemaClick
        OnExit = lcbSistemaExit
        OnKeyPress = FormKeyPress
      end
    end
    inherited pnlMenu: TPanel
      Height = 317
      ExplicitHeight = 317
      inherited btnOk: TJvTransparentButton
        Top = 227
        ExplicitTop = 74
      end
      inherited btnCancelar: TJvTransparentButton
        Top = 272
        ExplicitTop = 119
      end
    end
  end
  inherited dsCadastro: TDataSource
    DataSet = dmEtiqueta.cdsEtiqueta
    Left = 526
    Top = 275
  end
end
