inherited FFiltroRelatorioAtos: TFFiltroRelatorioAtos
  Caption = 'Relat'#243'rio de Atos'
  ClientHeight = 318
  ClientWidth = 565
  ExplicitWidth = 571
  ExplicitHeight = 347
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlFiltro: TPanel
    Width = 559
    Height = 312
    ExplicitWidth = 559
    ExplicitHeight = 312
    inherited pnlBotoes: TPanel
      Top = 265
      Width = 559
      TabOrder = 8
      ExplicitTop = 265
      ExplicitWidth = 559
      inherited btnVisualizar: TJvTransparentButton
        Left = 225
        ExplicitLeft = 225
      end
      inherited btnCancelar: TJvTransparentButton
        Left = 333
        ExplicitLeft = 333
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 117
        OnClick = btnLimparClick
        ExplicitLeft = 117
      end
    end
    inherited gbTipo: TGroupBox
      Top = 217
      Width = 149
      Height = 43
      TabOrder = 4
      ExplicitTop = 217
      ExplicitWidth = 149
      ExplicitHeight = 43
      inherited chbTipoAnalitico: TCheckBox
        Left = 81
        Top = 19
        ExplicitLeft = 81
        ExplicitTop = 19
      end
    end
    inherited chbExibirGrafico: TCheckBox
      Left = 168
      Top = 217
      TabOrder = 5
      ExplicitLeft = 168
      ExplicitTop = 217
    end
    inherited chbExportarPDF: TCheckBox
      Left = 168
      Top = 236
      TabOrder = 6
      ExplicitLeft = 168
      ExplicitTop = 236
    end
    inherited chbExportarExcel: TCheckBox
      Left = 299
      Top = 236
      TabOrder = 7
      ExplicitLeft = 299
      ExplicitTop = 236
    end
    object rgTipoAto: TRadioGroup
      Left = 5
      Top = 142
      Width = 550
      Height = 69
      Caption = 'Tipo de Ato'
      ItemIndex = 0
      Items.Strings = (
        'Todos'
        'Espec'#237'fico:')
      TabOrder = 2
      OnClick = rgTipoAtoClick
      OnExit = rgTipoAtoExit
    end
    object lcbTipoAto: TDBLookupComboBox
      Left = 86
      Top = 184
      Width = 458
      Height = 22
      Color = 16114127
      Enabled = False
      KeyField = 'ID_ITEM'
      ListField = 'DESCR_ITEM'
      ListSource = dsTipoAto
      TabOrder = 3
      OnKeyPress = FormKeyPress
    end
    object gbPeriodo: TGroupBox
      Left = 5
      Top = 8
      Width = 284
      Height = 49
      Caption = 'Per'#237'odo de Lan'#231'amento'
      TabOrder = 0
      object lblPeriodoInicio: TLabel
        Left = 5
        Top = 21
        Width = 32
        Height = 14
        Caption = 'In'#237'cio:'
      end
      object lblPeriodoFim: TLabel
        Left = 149
        Top = 21
        Width = 22
        Height = 14
        Caption = 'Fim:'
      end
      object dteDataInicio: TJvDateEdit
        Left = 43
        Top = 18
        Width = 100
        Height = 22
        Color = 16114127
        ShowNullDate = False
        TabOrder = 0
        OnKeyPress = dteDataInicioKeyPress
      end
      object dteDataFim: TJvDateEdit
        Left = 177
        Top = 18
        Width = 100
        Height = 22
        Color = 16114127
        ShowNullDate = False
        TabOrder = 1
        OnKeyPress = dteDataFimKeyPress
      end
    end
    object gbAtribuicao: TGroupBox
      Left = 5
      Top = 63
      Width = 550
      Height = 73
      Caption = 'Atribui'#231#227'o'
      TabOrder = 1
      object chbFirmas: TCheckBox
        Left = 7
        Top = 19
        Width = 97
        Height = 17
        Caption = 'FIRMAS'
        TabOrder = 0
      end
      object chbNotas: TCheckBox
        Left = 156
        Top = 19
        Width = 97
        Height = 17
        Caption = 'NOTAS'
        TabOrder = 1
      end
      object chbProtesto: TCheckBox
        Left = 311
        Top = 19
        Width = 97
        Height = 17
        Caption = 'PROTESTO'
        TabOrder = 2
      end
      object chbRCPJ: TCheckBox
        Left = 486
        Top = 19
        Width = 50
        Height = 17
        Caption = 'RCPJ'
        TabOrder = 3
      end
      object chbRCPN: TCheckBox
        Left = 7
        Top = 48
        Width = 97
        Height = 17
        Caption = 'RCPN'
        TabOrder = 4
      end
      object chbRGI: TCheckBox
        Left = 156
        Top = 48
        Width = 97
        Height = 17
        Caption = 'RGI'
        TabOrder = 5
      end
      object chbRIT: TCheckBox
        Left = 311
        Top = 48
        Width = 97
        Height = 17
        Caption = 'RIT'
        TabOrder = 6
      end
      object chbRTD: TCheckBox
        Left = 486
        Top = 48
        Width = 50
        Height = 17
        Caption = 'RTD'
        TabOrder = 7
      end
    end
  end
  object qryTipoAto: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT I.ID_ITEM, I.DESCR_ITEM'
      '  FROM ITEM I'
      ' WHERE EXISTS(SELECT LD.ID_ITEM_FK'
      '                FROM LANCAMENTO_DET LD'
      '               INNER JOIN LANCAMENTO L'
      '                  ON LD.COD_LANCAMENTO_FK = L.COD_LANCAMENTO'
      '                 AND LD.ANO_LANCAMENTO_FK = L.ANO_LANCAMENTO'
      '               WHERE L.TIPO_LANCAMENTO = '#39'R'#39
      '                 AND L.FLG_CANCELADO = '#39'N'#39
      '                 AND L.FLG_FLUTUANTE = '#39'N'#39
      '                 AND LD.COD_ADICIONAL IS NOT NULL'
      '                 AND LD.FLG_CANCELADO = '#39'N'#39
      '                 AND LD.ID_ITEM_FK = I.ID_ITEM'
      
        '                 AND L.DATA_LANCAMENTO BETWEEN :DATA_INI AND :DA' +
        'TA_FIM)'
      'ORDER BY I.DESCR_ITEM')
    Left = 419
    Top = 171
    ParamData = <
      item
        Name = 'DATA_INI'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'DATA_FIM'
        DataType = ftDate
        ParamType = ptInput
      end>
  end
  object dsTipoAto: TDataSource
    DataSet = qryTipoAto
    Left = 475
    Top = 171
  end
  object qrySistema: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM SISTEMA'
      'ORDER BY NOME_SISTEMA')
    Left = 454
    Top = 14
  end
  object dsSistema: TDataSource
    DataSet = qrySistema
    Left = 510
    Top = 14
  end
end
