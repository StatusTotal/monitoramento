{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroRelatorioFlutuantes.pas
  Descricao:   Filtro Relatorios de Flutuantes (R04 - R04DET)
  Author   :   Cristina
  Date:        22-nov-2016
  Last Update: 10-nov-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroRelatorioFlutuantes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroRelatorioPadrao, Vcl.StdCtrls,
  JvExControls, JvButton, JvTransparentButton, Vcl.ExtCtrls, Vcl.Mask, JvExMask,
  JvToolEdit, FireDAC.Stan.Param, System.StrUtils;

type
  TFFiltroRelatorioFlutuantes = class(TFFiltroRelatorioPadrao)
    lblPeriodo: TLabel;
    lblPeriodoInicio: TLabel;
    dteDataInicio: TJvDateEdit;
    lblPeriodoFim: TLabel;
    dteDataFim: TJvDateEdit;
    gbTipoFlutuante: TGroupBox;
    chbDepositos: TCheckBox;
    chbLancamentos: TCheckBox;
    chbDepositosACompensar: TCheckBox;
    chbDepositosFlutuantes: TCheckBox;
    chbDepositosBaixados: TCheckBox;
    chbDepositosCancelados: TCheckBox;
    chbLancamentosNaoBaixados: TCheckBox;
    chbLancamentosBaixados: TCheckBox;
    chbLancamentosCancelados: TCheckBox;
    procedure btnLimparClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chbDepositosClick(Sender: TObject);
    procedure chbLancamentosClick(Sender: TObject);
  protected
    procedure GravarLog; override;
    procedure GerarRelatorio(ImpDet: Boolean); override;
    function VerificarFiltro: Boolean; override;
  private
    { Private declarations }

    procedure SelecaoDepositosLancamentos;
  public
    { Public declarations }
  end;

var
  FFiltroRelatorioFlutuantes: TFFiltroRelatorioFlutuantes;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UDMRelatorios, UGDM, UVariaveisGlobais;

{ TFFiltroRelatorioFlutuantes }

procedure TFFiltroRelatorioFlutuantes.btnLimparClick(Sender: TObject);
begin
  inherited;

  dteDataInicio.Date := Date;
  dteDataFim.Date    := Date;

  chbDepositos.Checked   := True;
  chbLancamentos.Checked := True;

  SelecaoDepositosLancamentos;

  chbTipoSintetico.Checked := True;
  chbTipoAnalitico.Checked := False;

  chbExportarPDF.Checked := False;

  chbExportarExcel.Enabled := vgPrm_ExpDepo or vgPrm_ExpRec or vgPrm_ExpDesp or vgPrm_ExpRelDepFlu;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioFlutuantes.btnVisualizarClick(Sender: TObject);
begin
  inherited;

  if not VerificarFiltro then
    Exit;

  //Totais
  dmRelatorios.qryRel04_Totais.Close;

  dmRelatorios.qryRel04_Totais.Params.ParamByName('DATA_INI11').Value := dteDataInicio.Date;
  dmRelatorios.qryRel04_Totais.Params.ParamByName('DATA_FIM11').Value := dteDataFim.Date;
  dmRelatorios.qryRel04_Totais.Params.ParamByName('DATA_INI12').Value := dteDataInicio.Date;
  dmRelatorios.qryRel04_Totais.Params.ParamByName('DATA_FIM12').Value := dteDataFim.Date;
  dmRelatorios.qryRel04_Totais.Params.ParamByName('DATA_INI13').Value := dteDataInicio.Date;
  dmRelatorios.qryRel04_Totais.Params.ParamByName('DATA_FIM13').Value := dteDataFim.Date;
  dmRelatorios.qryRel04_Totais.Params.ParamByName('DATA_INI14').Value := dteDataInicio.Date;
  dmRelatorios.qryRel04_Totais.Params.ParamByName('DATA_FIM14').Value := dteDataFim.Date;
  dmRelatorios.qryRel04_Totais.Params.ParamByName('DATA_INI21').Value := dteDataInicio.Date;
  dmRelatorios.qryRel04_Totais.Params.ParamByName('DATA_FIM21').Value := dteDataFim.Date;
  dmRelatorios.qryRel04_Totais.Params.ParamByName('DATA_INI22').Value := dteDataInicio.Date;
  dmRelatorios.qryRel04_Totais.Params.ParamByName('DATA_FIM22').Value := dteDataFim.Date;
  dmRelatorios.qryRel04_Totais.Params.ParamByName('DATA_INI23').Value := dteDataInicio.Date;
  dmRelatorios.qryRel04_Totais.Params.ParamByName('DATA_FIM23').Value := dteDataFim.Date;

  if chbDepositosACompensar.Checked then
    dmRelatorios.qryRel04_Totais.Params.ParamByName('EXIBIR11').Value := 1
  else
    dmRelatorios.qryRel04_Totais.Params.ParamByName('EXIBIR11').Value := 0;

  if chbDepositosFlutuantes.Checked then
    dmRelatorios.qryRel04_Totais.Params.ParamByName('EXIBIR12').Value := 1
  else
    dmRelatorios.qryRel04_Totais.Params.ParamByName('EXIBIR12').Value := 0;

  if chbDepositosBaixados.Checked then
    dmRelatorios.qryRel04_Totais.Params.ParamByName('EXIBIR13').Value := 1
  else
    dmRelatorios.qryRel04_Totais.Params.ParamByName('EXIBIR13').Value := 0;

  if chbDepositosCancelados.Checked then
    dmRelatorios.qryRel04_Totais.Params.ParamByName('EXIBIR14').Value := 1
  else
    dmRelatorios.qryRel04_Totais.Params.ParamByName('EXIBIR14').Value := 0;

  if chbLancamentosNaoBaixados.Checked then
    dmRelatorios.qryRel04_Totais.Params.ParamByName('EXIBIR21').Value := 1
  else
    dmRelatorios.qryRel04_Totais.Params.ParamByName('EXIBIR21').Value := 0;

  if chbLancamentosBaixados.Checked then
    dmRelatorios.qryRel04_Totais.Params.ParamByName('EXIBIR22').Value := 1
  else
    dmRelatorios.qryRel04_Totais.Params.ParamByName('EXIBIR22').Value := 0;

  if chbLancamentosCancelados.Checked then
    dmRelatorios.qryRel04_Totais.Params.ParamByName('EXIBIR23').Value := 1
  else
    dmRelatorios.qryRel04_Totais.Params.ParamByName('EXIBIR23').Value := 0;

  dmRelatorios.qryRel04_Totais.Open;

  if dmRelatorios.qryRel04_Totais.RecordCount = 0 then
  begin
    Application.MessageBox('N�o h� Repasses registrados.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
  end
  else
  begin
    if chbTipoSintetico.Checked then
      GerarRelatorio(False);

    if chbTipoAnalitico.Checked then
      GerarRelatorio(True);
  end;
end;

procedure TFFiltroRelatorioFlutuantes.chbDepositosClick(Sender: TObject);
begin
  inherited;

  SelecaoDepositosLancamentos;
end;

procedure TFFiltroRelatorioFlutuantes.chbLancamentosClick(Sender: TObject);
begin
  inherited;

  SelecaoDepositosLancamentos;
end;

procedure TFFiltroRelatorioFlutuantes.FormShow(Sender: TObject);
begin
  inherited;

  dteDataInicio.Date := Date;
  dteDataFim.Date    := Date;

  chbDepositos.Checked   := True;
  chbLancamentos.Checked := True;

  SelecaoDepositosLancamentos;

  chbTipoSintetico.Checked := True;
  chbTipoAnalitico.Checked := False;

  chbExportarPDF.Checked := False;

  chbExportarExcel.Enabled := vgPrm_ExpDepo or vgPrm_ExpRec or vgPrm_ExpDesp or vgPrm_ExpRelDepFlu;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;

  chbExibirGrafico.Visible := False;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioFlutuantes.GerarRelatorio(ImpDet: Boolean);
begin
  inherited;

  try
    sTituloRelatorio    := 'REPASSE DE RECEITAS';
    sSubtituloRelatorio := 'Per�odo de ' + DateToStr(dteDataInicio.Date) + ' a ' + DateToStr(dteDataFim.Date);

    if ImpDet then
    begin
      lExibeDetalhe := True;

      dmRelatorios.DefinirRelatorio(R04DET);

      //Depositos
      if chbDepositos.Checked then
      begin
        dmRelatorios.qryRel04_Depos.Close;
        dmRelatorios.qryRel04_Depos.Params.ParamByName('DATA_INI').Value     := dteDataInicio.Date;
        dmRelatorios.qryRel04_Depos.Params.ParamByName('DATA_FIM').Value     := dteDataFim.Date;

        dmRelatorios.qryRel04_Depos.Params.ParamByName('FLG_STATUS01').Value := IfThen(chbDepositosACompensar.Checked,
                                                                                       'P', '');
        dmRelatorios.qryRel04_Depos.Params.ParamByName('FLG_STATUS02').Value := IfThen(chbDepositosFlutuantes.Checked,
                                                                                       'F', '');
        dmRelatorios.qryRel04_Depos.Params.ParamByName('FLG_STATUS03').Value := IfThen(chbDepositosBaixados.Checked,
                                                                                       'B', '');
        dmRelatorios.qryRel04_Depos.Params.ParamByName('FLG_STATUS04').Value := IfThen(chbDepositosCancelados.Checked,
                                                                                       'C', '');
        dmRelatorios.qryRel04_Depos.Open;
      end;

      //Lancamentos
      if chbLancamentos.Checked then
      begin
        dmRelatorios.qryRel04_Lanc.Close;
        dmRelatorios.qryRel04_Lanc.Params.ParamByName('DATA_INI').Value     := dteDataInicio.Date;
        dmRelatorios.qryRel04_Lanc.Params.ParamByName('DATA_FIM').Value     := dteDataFim.Date;

        dmRelatorios.qryRel04_Lanc.Params.ParamByName('FLG_STATUS01').Value := IfThen(chbLancamentosCancelados.Checked,
                                                                                       'C', '');
        dmRelatorios.qryRel04_Lanc.Params.ParamByName('FLG_STATUS02').Value := IfThen(chbLancamentosNaoBaixados.Checked,
                                                                                       'P', '');
        dmRelatorios.qryRel04_Lanc.Params.ParamByName('FLG_STATUS03').Value := IfThen(chbLancamentosBaixados.Checked,
                                                                                       'G', '');
        dmRelatorios.qryRel04_Lanc.Open;
      end;
    end
    else
    begin
      lExibeDetalhe := False;
      dmRelatorios.DefinirRelatorio(R04);
    end;

    dmRelatorios.ImprimirRelatorio(chbExportarPDF.Checked, chbExportarExcel.Checked);

    GravarLog;
  except
    Application.MessageBox('Erro ao gerar relat�rio de Repasse de Receitas.',
                           'Erro',
                           MB_OK + MB_ICONERROR);
  end;
end;

procedure TFFiltroRelatorioFlutuantes.GravarLog;
begin
  inherited;

  BS.GravarUsuarioLog(50,
                      '',
                      'Impress�o de Relatorio de Repasse de Receitas (' + DateToStr(dDataRelatorio) +
                                                                      ' ' +
                                                                      TimeToStr(tHoraRelatorio) + ')',
                      0,
                      '');
end;

procedure TFFiltroRelatorioFlutuantes.SelecaoDepositosLancamentos;
begin
  if chbDepositos.Checked then
  begin
    chbDepositosACompensar.Enabled := True;
    chbDepositosFlutuantes.Enabled := True;
    chbDepositosBaixados.Enabled   := True;
    chbDepositosCancelados.Enabled := True;

    chbDepositosACompensar.Checked := True;
    chbDepositosFlutuantes.Checked := True;
    chbDepositosBaixados.Checked   := True;
    chbDepositosCancelados.Checked := True;

    lExibirDepositos := True;
  end
  else
  begin
    chbDepositosACompensar.Checked := False;
    chbDepositosFlutuantes.Checked := False;
    chbDepositosBaixados.Checked   := False;
    chbDepositosCancelados.Checked := False;

    chbDepositosACompensar.Enabled := False;
    chbDepositosFlutuantes.Enabled := False;
    chbDepositosBaixados.Enabled   := False;
    chbDepositosCancelados.Enabled := False;

    lExibirDepositos := False;
  end;

  if chbLancamentos.Checked then
  begin
    chbLancamentosNaoBaixados.Enabled := True;
    chbLancamentosBaixados.Enabled    := True;
    chbLancamentosCancelados.Enabled  := True;

    chbLancamentosNaoBaixados.Checked := True;
    chbLancamentosBaixados.Checked    := True;
    chbLancamentosCancelados.Checked  := True;

    lExibirLancamentos := True;
  end
  else
  begin
    chbLancamentosNaoBaixados.Checked := False;
    chbLancamentosBaixados.Checked    := False;
    chbLancamentosCancelados.Checked  := False;

    chbLancamentosNaoBaixados.Enabled := False;
    chbLancamentosBaixados.Enabled    := False;
    chbLancamentosCancelados.Enabled  := False;

    lExibirLancamentos := False;
  end;
end;

function TFFiltroRelatorioFlutuantes.VerificarFiltro: Boolean;
var
  Msg: String;
begin
  Result := True;

  if dteDataInicio.Date = 0 then
  begin
    Msg := '- Informe a DATA DE IN�CIO do Per�odo.';
    Result := False;
  end;

  if dteDataFim.Date = 0 then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe a DATA DE FIM do Per�odo.'
    else
      Msg := #13#10 + '- Informe a DATA DE FIM do Per�odo.';

    Result := False;
  end;

  if dteDataFim.Date < dteDataInicio.Date then
  begin
    if Trim(Msg) = '' then
      Msg := '- A DATA DE FIM do Per�odo n�o pode inferior � DATA DE IN�CIO do mesmo.'
    else
      Msg := #13#10 + '- A DATA DE FIM do Per�odo n�o pode ser inferior � DATA DE IN�CIO do mesmo.';

    Result := False;
  end;

  if (not chbDepositos.Checked) and
    (not chbLancamentos.Checked) then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe o TIPO DE REPASSE.'
    else
      Msg := #13#10 + '- Informe o TIPO DE REPASSE.';

    Result := False;
  end;

  if (not chbTipoAnalitico.Checked) and
    (not chbTipoSintetico.Checked) then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe o TIPO do Relat�rio de Repasse de Receitas.'
    else
      Msg := #13#10 + '- Informe o TIPO do Relat�rio de Repasse de Receitas.';

    Result := False;
  end;

  if not Result then
    Application.MessageBox(PChar(':: ATEN��O ::' + #13#10 + #13#10 + Msg),
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
end;

end.
