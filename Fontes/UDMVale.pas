{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UDMVale.pas
  Descricao:   Data Module de Vale
  Author   :   Cristina
  Date:        27-mai-2016
  Last Update: 27-out-201  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UDMVale;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TdmVale = class(TDataModule)
    qryVale: TFDQuery;
    dspVale: TDataSetProvider;
    cdsVale: TClientDataSet;
    cdsValeID_VALE: TIntegerField;
    cdsValeID_FUNCIONARIO_FK: TIntegerField;
    cdsValeVR_VALE: TBCDField;
    cdsValeDESCR_VALE: TStringField;
    cdsValeCAD_ID_USUARIO: TIntegerField;
    cdsValeDATA_QUITACAO: TDateField;
    cdsValeQUIT_ID_USUARIO: TIntegerField;
    dsVale: TDataSource;
    qryFuncionarios: TFDQuery;
    dsFuncionarios: TDataSource;
    cdsValeFLG_CANCELADO: TStringField;
    cdsValeDATA_CANCELAMENTO: TDateField;
    cdsValeCANCEL_ID_USUARIO: TIntegerField;
    cdsValeID_FECHAMENTO_SALARIO_FK: TIntegerField;
    cdsValeVR_VALE_ANTERIOR: TCurrencyField;
    cdsValeDATA_VALE: TDateField;
    cdsValeDATA_CADASTRO: TSQLTimeStampField;
    procedure cdsValeVR_VALEGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsValeVR_VALE_ANTERIORGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmVale: TdmVale;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{$R *.dfm}

procedure TdmVale.cdsValeVR_VALEGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmVale.cdsValeVR_VALE_ANTERIORGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

end.
