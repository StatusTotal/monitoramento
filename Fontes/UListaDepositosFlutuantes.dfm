inherited FListaDepositosFlutuantes: TFListaDepositosFlutuantes
  Caption = 'Lista de Dep'#243'sitos Flutuantes'
  ClientHeight = 449
  ClientWidth = 519
  OnCreate = FormCreate
  ExplicitWidth = 525
  ExplicitHeight = 478
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 513
    Height = 443
    ExplicitWidth = 513
    inherited pnlGrid: TPanel
      Top = 52
      Width = 507
      Height = 388
      ExplicitTop = 70
      ExplicitWidth = 507
      ExplicitHeight = 373
      object btnConfirmar: TJvTransparentButton [0]
        Left = 0
        Top = 365
        Width = 507
        Height = 23
        Align = alBottom
        Caption = '&OK'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        Spacing = 236
        TextAlign = ttaRight
        Transparent = False
        OnClick = btnConfirmarClick
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          1800000000000006000000000000000000000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF4F9EFDDE4ECD8E0ECD8E0EFDDE4FFF4F9FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFAE6E6E6E2
          E2E2E2E2E2E6E6E6FAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFF3FAA3B6AA329064008A47008D49008D49008A47329064A3B6AAFFF3
          FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBABABAB6161613A3A3A39
          39393939393A3A3A616161ABABABFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFECF62F8D5F009A5B34CDA539DDB23AE1B33AE1B239DDB234CDA5009A5A2F8D
          5FFFECF6FFFFFFFFFFFFFFFFFFFFFFFFF8F8F85E5E5E47474788888892929295
          95959595959292928888884747475E5E5EF8F8F8FFFFFFFFFFFFFFFFFFFFF5FE
          1A8B550DB5833EE0B600D79700D49600D49700D49700D49600D7973EE0B50DB5
          831A8B55FFF5FEFFFFFFFFFFFFFFFFFF53535367676797979770707070707070
          7070707070707070707070979797676767535353FFFFFFFFFFFFFFFFFF75A78C
          00A97233DFB200D29500D29600CB8700CE8F00D39900D39900D29800D29533DF
          B200A97275A78CFFFFFFFFFFFF8E8E8E5959599191916D6D6D70707061616168
          68687373737373737272726D6D6D9191915959598E8E8EFFFFFFFFF6FE008641
          45DDB800D19400CF9404CE93FFFFFFA1EDD700C78400D29A00D19900D19900D1
          9445DDB6008641FFF6FEFFFFFF3333339999996D6D6D6D6D6D717171FFFFFFCA
          CACA5C5C5C7272727272727272726D6D6D999999333333FFFFFFA7C6B605AA75
          19D8A700CF9605CC93FFFFFFFFFFFFFFFFFF9DECD600C68400D09A00D09900CF
          9819D8A606AB74A5C9B6B9B3B65E5E5E8181816E6E6E707070FFFFFFFFFFFFFF
          FFFFC8C8C85B5B5B7272727272727070708282825E5E5EB6B6B678B7962EC69C
          00D09800C789FFFFFFFFFFFFA0EAD3FFFFFFFFFFFF9DEBD600C48400CF9A00CE
          9900D0982BC49974BC979C93978080806F6F6F5F5F5FFFFFFFFFFFFFC7C7C7FF
          FFFFFFFFFFC7C7C75B5B5B7171717070706F6F6F7F7F7F9898987CB79738D0A8
          00CC9400C99270E2C4B5EFDF00C18004CA92FFFFFFFFFFFF9DEAD600C28400CD
          9900CE972CC49979BC9A9D95998B8B8B6C6C6C696969AFAFAFD4D4D45454546F
          6F6FFFFFFFFFFFFFC8C8C85959597171716F6F6F7F7F7F9A9A9A75B49250DDB9
          00C99300CB9900C89100C78F00CB9900C99404C893FFFFFFFFFFFFA8ECD900C7
          8F00CC982BC49874BC979990949E9E9E6969697070706767676464647070706A
          6A6A6E6E6EFFFFFFFFFFFFCDCDCD6464646E6E6E7F7F7F989898B1D2BF38C69B
          18D3A700C99700CA9900CA9900CA9900CA9900C79404C793FFFFFFEFFBFA00C2
          8A19D3A604AA73B3D8C4C3BEC18585857F7F7F6C6C6C6E6E6E6E6E6E6E6E6E6E
          6E6E6969696F6F6FFFFFFFF6F6F65E5E5E8080805C5C5CC5C5C5FFFFFF07985C
          64F5D700BF8D00C89900C89900C89900C89900C89900C69421CCA116CC9E00C5
          9245DBB800853FFFFFFFFFFFFF525252B5B5B56262626F6F6F6F6F6F6F6F6F6F
          6F6F6F6F6F6969697F7F7F7A7A7A666666989898313131FFFFFFFFFFFF81BB9C
          6FE1C12FE0B800BF8D00C79900C79900C79900C79900C79900C69700C39233D8
          B300A76D90C3A8FFFFFFFFFFFF9E9E9EACACAC9191916363636E6E6E6E6E6E6D
          6D6D6D6D6D6D6D6D6A6A6A6565658E8E8E545454AAAAAAFFFFFFFFFFFFFFFFFF
          319B66A9FFF043E8C300BA8900C09200C29400C29400C29400C2963ED9B708B2
          7E2B9C68FFFFFFFFFFFFFFFFFFFFFFFF666666DADADA9E9E9E5C5C5C66666668
          6868686868686868676767959595636363656565FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF41A2706ED5B49FFFF64DECCA43E2C042E1BF45E1C140D6B00094524AA8
          7AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF717171A5A5A5DADADAA6A6A69B
          9B9B9B9B9B9D9D9D9292923E3E3E797979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFCDE3D568B7913AAD7D33AC7B20A671109D6363B68FDCEBE1FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D7D790909076767672
          72726666665959598D8D8DE3E3E3FFFFFFFFFFFFFFFFFFFFFFFF}
        NumGlyphs = 2
        ExplicitTop = 368
        ExplicitWidth = 472
      end
      inherited dbgGridListaPadrao: TDBGrid
        Width = 507
        Height = 360
        Align = alNone
        OnDblClick = dbgGridListaPadraoDblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'ID_DEPOSITO_FLUTUANTE'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_DEPOSITO'
            Title.Caption = 'Dt. Dep'#243'sito'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NUM_COD_DEPOSITO'
            Title.Caption = 'Identificador'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VR_DEPOSITO_FLUTUANTE'
            Title.Caption = 'Valor'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCR_DEPOSITO_FLUTUANTE'
            Title.Caption = 'Descri'#231#227'o'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FLG_STATUS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_STATUS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ST_ID_USUARIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'CAD_ID_USUARIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_CADASTRO'
            Visible = False
          end>
      end
    end
    inherited pnlFiltro: TPanel
      Width = 513
      Height = 49
      ExplicitWidth = 513
      ExplicitHeight = 49
      inherited btnFiltrar: TJvTransparentButton
        Left = 338
        Top = 20
        OnClick = btnFiltrarClick
        ExplicitLeft = 338
        ExplicitTop = 20
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 427
        Top = 20
        ExplicitLeft = 427
        ExplicitTop = 20
      end
      object lblPeriodo: TLabel
        Left = 3
        Top = 8
        Width = 111
        Height = 14
        Caption = 'Per'#237'odo do Dep'#243'sito'
      end
      object Label2: TLabel
        Left = 106
        Top = 27
        Width = 6
        Height = 14
        Caption = 'a'
      end
      object dtePeriodoIni: TJvDateEdit
        Left = 3
        Top = 24
        Width = 100
        Height = 22
        Color = 16114127
        ShowNullDate = False
        TabOrder = 0
        OnKeyPress = dtePeriodoIniKeyPress
      end
      object dtePeriodoFim: TJvDateEdit
        Left = 115
        Top = 24
        Width = 100
        Height = 22
        Color = 16114127
        ShowNullDate = False
        TabOrder = 1
        OnKeyPress = dtePeriodoFimKeyPress
      end
    end
  end
  inherited dsGridListaPadrao: TDataSource
    Left = 444
    Top = 331
  end
  inherited qryGridListaPadrao: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT ID_DEPOSITO_FLUTUANTE AS ID,'
      '       DESCR_DEPOSITO_FLUTUANTE,'
      '       DATA_DEPOSITO,'
      '       NUM_COD_DEPOSITO,'
      '       VR_DEPOSITO_FLUTUANTE,'
      '       FLG_STATUS,'
      '       DATA_STATUS,'
      '       ST_ID_USUARIO,'
      '       CAD_ID_USUARIO,'
      '       DATA_CADASTRO'
      '  FROM DEPOSITO_FLUTUANTE'
      ' WHERE FLG_STATUS = '#39'F'#39
      '   AND DATA_DEPOSITO BETWEEN :DATA_INI AND :DATA_FIM'
      'ORDER BY DATA_DEPOSITO')
    Left = 444
    Top = 279
    ParamData = <
      item
        Name = 'DATA_INI'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'DATA_FIM'
        DataType = ftDate
        ParamType = ptInput
      end>
    object qryGridListaPadraoID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID_DEPOSITO_FLUTUANTE'
      Required = True
    end
    object qryGridListaPadraoDESCR_DEPOSITO_FLUTUANTE: TStringField
      FieldName = 'DESCR_DEPOSITO_FLUTUANTE'
      Origin = 'DESCR_DEPOSITO_FLUTUANTE'
      Size = 250
    end
    object qryGridListaPadraoDATA_DEPOSITO: TDateField
      FieldName = 'DATA_DEPOSITO'
      Origin = 'DATA_DEPOSITO'
    end
    object qryGridListaPadraoNUM_COD_DEPOSITO: TStringField
      FieldName = 'NUM_COD_DEPOSITO'
      Origin = 'NUM_COD_DEPOSITO'
      Size = 25
    end
    object qryGridListaPadraoVR_DEPOSITO_FLUTUANTE: TBCDField
      FieldName = 'VR_DEPOSITO_FLUTUANTE'
      Origin = 'VR_DEPOSITO_FLUTUANTE'
      OnGetText = qryGridListaPadraoVR_DEPOSITO_FLUTUANTEGetText
      Precision = 18
      Size = 2
    end
    object qryGridListaPadraoFLG_STATUS: TStringField
      FieldName = 'FLG_STATUS'
      Origin = 'FLG_STATUS'
      FixedChar = True
      Size = 1
    end
    object qryGridListaPadraoDATA_STATUS: TDateField
      FieldName = 'DATA_STATUS'
      Origin = 'DATA_STATUS'
    end
    object qryGridListaPadraoST_ID_USUARIO: TIntegerField
      FieldName = 'ST_ID_USUARIO'
      Origin = 'ST_ID_USUARIO'
    end
    object qryGridListaPadraoCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
      Origin = 'CAD_ID_USUARIO'
    end
    object qryGridListaPadraoDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
      Origin = 'DATA_CADASTRO'
    end
  end
end
