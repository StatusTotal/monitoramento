inherited FCadastroOficioPessoa: TFCadastroOficioPessoa
  Caption = 'Cadastro de Pessoa do Of'#237'cio'
  ClientHeight = 127
  ClientWidth = 488
  ExplicitWidth = 494
  ExplicitHeight = 156
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 482
    Height = 121
    ExplicitWidth = 482
    ExplicitHeight = 121
    inherited pnlDados: TPanel
      Width = 368
      Height = 115
      ExplicitWidth = 368
      ExplicitHeight = 115
      object lblNome: TLabel [0]
        Left = 5
        Top = 8
        Width = 32
        Height = 14
        Caption = 'Nome'
      end
      inherited pnlMsgErro: TPanel
        Top = 54
        Width = 368
        ExplicitTop = 54
        ExplicitWidth = 368
        inherited lblMensagemErro: TLabel
          Width = 368
        end
        inherited lbMensagemErro: TListBox
          Width = 368
          OnDblClick = lbMensagemErroDblClick
          ExplicitWidth = 368
        end
      end
      object edtNome: TDBEdit
        Left = 5
        Top = 24
        Width = 361
        Height = 22
        CharCase = ecUpperCase
        Color = 16114127
        DataField = 'NOME_OFICIO_PESSOA'
        DataSource = dsCadastro
        TabOrder = 1
        OnKeyPress = edtNomeKeyPress
      end
    end
    inherited pnlMenu: TPanel
      Height = 115
      ExplicitHeight = 115
      inherited btnOk: TJvTransparentButton
        Top = 25
        ExplicitTop = 42
      end
      inherited btnCancelar: TJvTransparentButton
        Top = 70
        ExplicitTop = 87
      end
    end
  end
  inherited dsCadastro: TDataSource
    DataSet = cdsOfcPes
    Left = 430
    Top = 70
  end
  object qryOfcPes: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM OFICIO_PESSOA'
      ' WHERE ID_OFICIO_PESSOA = :ID_OFICIO_PESSOA')
    Left = 258
    Top = 69
    ParamData = <
      item
        Position = 1
        Name = 'ID_OFICIO_PESSOA'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspOfcPes: TDataSetProvider
    DataSet = qryOfcPes
    Left = 314
    Top = 69
  end
  object cdsOfcPes: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_OFICIO_PESSOA'
        ParamType = ptInput
      end>
    ProviderName = 'dspOfcPes'
    Left = 370
    Top = 69
    object cdsOfcPesID_OFICIO_PESSOA: TIntegerField
      FieldName = 'ID_OFICIO_PESSOA'
      Required = True
    end
    object cdsOfcPesNOME_OFICIO_PESSOA: TStringField
      FieldName = 'NOME_OFICIO_PESSOA'
      Size = 250
    end
  end
end
