{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UDMUsuarioCarneLeao.pas
  Descricao:   Data Module do cadastro de Carne-Leao
  Author   :   Cristina
  Date:        25-nov-2016
  Last Update: 28-fev-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UDMCarneLeao;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  Datasnap.Provider, System.StrUtils, System.Variants, System.DateUtils;

type
  TdmCarneLeao = class(TDataModule)
    qryIdent_F: TFDQuery;
    qryPlCont_F: TFDQuery;
    qryEscrit_F: TFDQuery;
    qryRegis_F: TFDQuery;
    qryCtrl_F: TFDQuery;
    dspIdent_F: TDataSetProvider;
    dspPlCont_F: TDataSetProvider;
    dspEscrit_F: TDataSetProvider;
    dspRegis_F: TDataSetProvider;
    dspCtrl_F: TDataSetProvider;
    cdsIdent_F: TClientDataSet;
    cdsPlCont_F: TClientDataSet;
    cdsEscrit_F: TClientDataSet;
    cdsRegis_F: TClientDataSet;
    cdsCtrl_F: TClientDataSet;
    dsIdent_F: TDataSource;
    dsPlCont_F: TDataSource;
    dsEscrit_F: TDataSource;
    dsRegis_F: TDataSource;
    dsCtrl_F: TDataSource;
    cdsIdent_FTIPO_ENVIO: TStringField;
    cdsIdent_FIDENTIFICADOR: TStringField;
    cdsIdent_FSISTEMA_GERADOR: TStringField;
    cdsIdent_FANO_DECLARACAO: TIntegerField;
    cdsIdent_FCPF_DECLARANTE: TStringField;
    cdsIdent_FNOME_DECLARANTE: TStringField;
    cdsIdent_FATIVACAO_LIVROCAIXA: TStringField;
    cdsIdent_FCOD_ORIGEM: TIntegerField;
    cdsIdent_FLOGRADOURO: TStringField;
    cdsIdent_FNUM_LOGRADOURO: TStringField;
    cdsIdent_FCOMPL_LOGRADOURO: TStringField;
    cdsIdent_FBAIRRO_LOGRADOURO: TStringField;
    cdsIdent_FMUNIC_LOGRADOURO: TStringField;
    cdsIdent_FTELEFONE: TStringField;
    cdsIdent_FSIGLA_UF: TStringField;
    cdsIdent_FCEP: TStringField;
    cdsPlCont_FID_CARNELEAO_PLANOCONTAS: TIntegerField;
    cdsPlCont_FIDENTIFICADOR: TStringField;
    cdsPlCont_FCPF_DECLARANTE: TStringField;
    cdsPlCont_FCOD_CONTA: TStringField;
    cdsPlCont_FDESCR_CONTA: TStringField;
    cdsPlCont_FFLG_SELECIONAVEL: TStringField;
    cdsEscrit_FID_CARNELEAO_ESCRITURACAO: TIntegerField;
    cdsEscrit_FIDENTIFICADOR: TStringField;
    cdsEscrit_FMES_ESCRITURACAO: TStringField;
    cdsEscrit_FDATA_ESCRITURACAO: TStringField;
    cdsEscrit_FCOD_TIPO_CONTA: TStringField;
    cdsEscrit_FHISTORICO_CONTA: TStringField;
    cdsEscrit_FVR_CONTA: TBCDField;
    cdsRegis_FID_CARNELEAO_REGISTRO: TIntegerField;
    cdsRegis_FIDENTIFICADOR: TStringField;
    cdsRegis_FMES_APURACAO: TStringField;
    cdsRegis_FNOME_MES_APURACAO: TStringField;
    cdsRegis_FVR_TRAB_NAO_ASSALARIADO: TBCDField;
    cdsRegis_FVR_ALUGUEIS: TBCDField;
    cdsRegis_FVR_OUTROS: TBCDField;
    cdsRegis_FVR_EXTERIOR: TBCDField;
    cdsRegis_FVR_PREV_OFICIAL: TBCDField;
    cdsRegis_FVR_DEPENDENTES: TBCDField;
    cdsRegis_FVR_PENSAO_ALIMENTICIA: TBCDField;
    cdsRegis_FVR_LIVRO_CAIXA: TBCDField;
    cdsRegis_FVR_IMP_EXTERIOR_ACOMP: TBCDField;
    cdsRegis_FVR_IMP_APAGAR: TBCDField;
    cdsRegis_FVR_IMP_PAGO: TBCDField;
    cdsRegis_FVR_BASE_CALCULO: TBCDField;
    cdsRegis_FVR_DEDUCOES: TBCDField;
    cdsRegis_FVR_REND_EXT_N_ASSAL: TBCDField;
    cdsRegis_FVR_REND_EXT_OUTROS: TBCDField;
    cdsRegis_FVR_DEDUC_LIVRO_CAIXA_MES: TBCDField;
    cdsRegis_FVR_EXCS_DEDUC_MES_ANT: TBCDField;
    cdsRegis_FVR_TOTAL_DEDUC_MES: TBCDField;
    cdsRegis_FVR_REND_PJ_TRAB_NAO_ASS: TBCDField;
    cdsRegis_FVR_LIM_LEGAL_LIVRO_CX: TBCDField;
    cdsRegis_FVR_DEDUC_LIVRO_CAIXA: TBCDField;
    cdsRegis_FVR_EXCS_DEDUC_MES_SEG: TBCDField;
    cdsRegis_FVR_CL_DEV_CREND_EXT: TBCDField;
    cdsRegis_FVR_CL_DEV_SREND_EXT: TBCDField;
    cdsRegis_FVR_LIM_COMP_MENSAL_EXT: TBCDField;
    cdsRegis_FVR_IMP_PG_EXT_MESATUAL: TBCDField;
    cdsRegis_FVR_IMP_PG_EXT_EXCS_MESANT: TBCDField;
    cdsRegis_FVR_IMP_PG_EXT_MES_EXCS: TBCDField;
    cdsRegis_FVR_IMP_PG_EXT_ACOMP: TBCDField;
    cdsRegis_FVR_EXCS_PROX_MES: TBCDField;
    cdsCtrl_FID_CARNELEAO_CONTROLE: TIntegerField;
    cdsCtrl_FIDENTIFICADOR: TStringField;
    cdsCtrl_FCONTROLE: TStringField;
    cdsIdent_FDATA_GERACAO: TSQLTimeStampField;
    cdsIdent_FGER_ID_USUARIO: TIntegerField;
    cdsIdent_FFLG_CANCELADO: TStringField;
    cdsIdent_FDATA_CANCELAMENTO: TDateField;
    cdsIdent_FCANCEL_ID_USUARIO: TIntegerField;
    cdsIdent_FID_CARNELEAO_IDENTIFICACAO: TIntegerField;
    cdsIdent_FNOME_ARQUIVO: TStringField;
    cdsIdent_FEXTENSAO_ARQUIVO: TStringField;
    cdsPlCont_FID_CARNELEAO_IDENTIFICACAO_FK: TIntegerField;
    cdsEscrit_FID_CARNELEAO_IDENTIFICACAO_FK: TIntegerField;
    cdsRegis_FID_CARNELEAO_IDENTIFICACAO_FK: TIntegerField;
    cdsCtrl_FID_CARNELEAO_IDENTIFICACAO_FK: TIntegerField;
    qryClassif: TFDQuery;
    dsClassif: TDataSource;
    dsEquiv_F: TDataSource;
    dspEquiv_F: TDataSetProvider;
    cdsEquiv_F: TClientDataSet;
    qryEquiv_F: TFDQuery;
    cdsEquiv_FM_TIPO_LANCAMENTO: TStringField;
    cdsEquiv_FM_ID_CATEGORIA_DESPREC_FK: TIntegerField;
    cdsEquiv_FM_ID_SUBCATEGORIA_DESPREC_FK: TIntegerField;
    cdsEquiv_FM_ID_NATUREZA_FK: TIntegerField;
    cdsEquiv_FID_CARNELEAO_CLASSIFICACAO_FK: TIntegerField;
    qryCategoria: TFDQuery;
    qrySubCategoria: TFDQuery;
    qryNatureza: TFDQuery;
    dsCategoria: TDataSource;
    dsSubCategoria: TDataSource;
    dsNatureza: TDataSource;
    cdsEquiv_FDATA_CADASTRO: TSQLTimeStampField;
    cdsEquiv_FCAD_ID_USUARIO: TIntegerField;
    cdsEquiv_FFLG_CANCELADO: TStringField;
    cdsEquiv_FDATA_CANCELAMENTO: TDateField;
    cdsEquiv_FCANCEL_ID_USUARIO: TIntegerField;
    cdsEquiv_FNOVO: TBooleanField;
    cdsEquiv_FDESCR_CATEGORIA_DESPREC: TStringField;
    cdsEquiv_FDESCR_SUBCATEGORIA_DESPREC: TStringField;
    cdsEquiv_FDESCR_NATUREZA: TStringField;
    cdsEquiv_FDESCR_CARNELEAO_CLASSIFICACAO: TStringField;
    cdsEquiv_FID_CARNELEAO_EQUIVALENCIA: TIntegerField;
    cdsEquiv_FOBS_CARNELEAO_EQUIVALENCIA: TStringField;
    qryValores: TFDQuery;
    dsValores: TDataSource;
    qryValoresID_CARNELEAO_CLASSIFICACAO_FK: TIntegerField;
    qryValoresVR_PARCELA_PAGO: TBCDField;
    procedure cdsEscrit_FVR_CONTAGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsRegis_FVR_TRAB_NAO_ASSALARIADOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsRegis_FVR_ALUGUEISGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsRegis_FVR_OUTROSGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsRegis_FVR_EXTERIORGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsRegis_FVR_PREV_OFICIALGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsRegis_FVR_DEPENDENTESGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsRegis_FVR_PENSAO_ALIMENTICIAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsRegis_FVR_LIVRO_CAIXAGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsRegis_FVR_IMP_EXTERIOR_ACOMPGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsRegis_FVR_IMP_APAGARGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsRegis_FVR_IMP_PAGOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsRegis_FVR_BASE_CALCULOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsRegis_FVR_DEDUCOESGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsRegis_FVR_REND_EXT_N_ASSALGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsRegis_FVR_REND_EXT_OUTROSGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsRegis_FVR_DEDUC_LIVRO_CAIXA_MESGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsRegis_FVR_EXCS_DEDUC_MES_ANTGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsRegis_FVR_TOTAL_DEDUC_MESGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsRegis_FVR_REND_PJ_TRAB_NAO_ASSGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsRegis_FVR_LIM_LEGAL_LIVRO_CXGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsRegis_FVR_DEDUC_LIVRO_CAIXAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsRegis_FVR_EXCS_DEDUC_MES_SEGGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsRegis_FVR_CL_DEV_CREND_EXTGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsRegis_FVR_CL_DEV_SREND_EXTGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsRegis_FVR_LIM_COMP_MENSAL_EXTGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsRegis_FVR_IMP_PG_EXT_MESATUALGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsRegis_FVR_IMP_PG_EXT_EXCS_MESANTGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsRegis_FVR_IMP_PG_EXT_MES_EXCSGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsRegis_FVR_IMP_PG_EXT_ACOMPGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsRegis_FVR_EXCS_PROX_MESGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsEquiv_FCalcFields(DataSet: TDataSet);
    procedure cdsIdent_FCPF_DECLARANTEGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    function GerarArquivoExportacaoCarneLeao(IdCarneLeao: Integer; var Msg: String;
      var TipoMsg: Integer; DelArq: Boolean): Boolean;

    function ExisteLancamentoSemCarneLeao(Ano: Integer; var LstLancs: String): Boolean;
  end;

var
  dmCarneLeao: TdmCarneLeao;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{$R *.dfm}

procedure TdmCarneLeao.cdsEquiv_FCalcFields(DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with qryAux, SQL do
  begin
    //Carne-Leao
    if cdsEquiv_F.FieldByName('ID_CARNELEAO_CLASSIFICACAO_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT UPPER(DESCR_CARNELEAO_CLASSIFICACAO) AS DESCR_CARNELEAO_CLASSIFICACAO ' +
              '  FROM CARNELEAO_CLASSIFICACAO ' +
              ' WHERE ID_CARNELEAO_CLASSIFICACAO = :ID_CARNELEAO_CLASSIFICACAO';
      Params.ParamByName('ID_CARNELEAO_CLASSIFICACAO').AsInteger := cdsEquiv_F.FieldByName('ID_CARNELEAO_CLASSIFICACAO_FK').AsInteger;
      Open;

      cdsEquiv_F.FieldByName('DESCR_CARNELEAO_CLASSIFICACAO').AsString := qryAux.FieldByName('DESCR_CARNELEAO_CLASSIFICACAO').AsString;
    end;

    //Categoria
    if cdsEquiv_F.FieldByName('M_ID_CATEGORIA_DESPREC_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT UPPER(DESCR_CATEGORIA_DESPREC) AS DESCR_CATEGORIA_DESPREC ' +
              '  FROM CATEGORIA_DESPREC ' +
              ' WHERE ID_CATEGORIA_DESPREC = :ID_CATEGORIA_DESPREC';
      Params.ParamByName('ID_CATEGORIA_DESPREC').AsInteger := cdsEquiv_F.FieldByName('M_ID_CATEGORIA_DESPREC_FK').AsInteger;
      Open;

      cdsEquiv_F.FieldByName('DESCR_CATEGORIA_DESPREC').AsString := qryAux.FieldByName('DESCR_CATEGORIA_DESPREC').AsString;
    end;

    //Subcategoria
    if cdsEquiv_F.FieldByName('M_ID_SUBCATEGORIA_DESPREC_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT UPPER(DESCR_SUBCATEGORIA_DESPREC) AS DESCR_SUBCATEGORIA_DESPREC ' +
              '  FROM SUBCATEGORIA_DESPREC ' +
              ' WHERE ID_SUBCATEGORIA_DESPREC = :ID_SUBCATEGORIA_DESPREC';
      Params.ParamByName('ID_SUBCATEGORIA_DESPREC').AsInteger := cdsEquiv_F.FieldByName('M_ID_SUBCATEGORIA_DESPREC_FK').AsInteger;
      Open;

      cdsEquiv_F.FieldByName('DESCR_SUBCATEGORIA_DESPREC').AsString := qryAux.FieldByName('DESCR_SUBCATEGORIA_DESPREC').AsString;
    end;

    //Natureza
    if cdsEquiv_F.FieldByName('M_ID_NATUREZA_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT UPPER(DESCR_NATUREZA) AS DESCR_NATUREZA ' +
              '  FROM NATUREZA ' +
              ' WHERE ID_NATUREZA = :ID_NATUREZA';
      Params.ParamByName('ID_NATUREZA').AsInteger := cdsEquiv_F.FieldByName('M_ID_NATUREZA_FK').AsInteger;
      Open;

      cdsEquiv_F.FieldByName('DESCR_NATUREZA').AsString := qryAux.FieldByName('DESCR_NATUREZA').AsString;
    end;
  end;

  FreeAndNil(qryAux);
end;

procedure TdmCarneLeao.cdsEscrit_FVR_CONTAGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsIdent_FCPF_DECLARANTEGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := Copy(Sender.AsString, 1, 3) + '.' +
            Copy(Sender.AsString, 4, 3) + '.' +
            Copy(Sender.AsString, 7, 3) + '-' +
            Copy(Sender.AsString, 10, 2);
end;

procedure TdmCarneLeao.cdsRegis_FVR_ALUGUEISGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_BASE_CALCULOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_CL_DEV_CREND_EXTGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_CL_DEV_SREND_EXTGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_DEDUCOESGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_DEDUC_LIVRO_CAIXAGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_DEDUC_LIVRO_CAIXA_MESGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_DEPENDENTESGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_EXCS_DEDUC_MES_ANTGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_EXCS_DEDUC_MES_SEGGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_EXCS_PROX_MESGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_EXTERIORGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_IMP_APAGARGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_IMP_EXTERIOR_ACOMPGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_IMP_PAGOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_IMP_PG_EXT_ACOMPGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_IMP_PG_EXT_EXCS_MESANTGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_IMP_PG_EXT_MESATUALGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_IMP_PG_EXT_MES_EXCSGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_LIM_COMP_MENSAL_EXTGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_LIM_LEGAL_LIVRO_CXGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_LIVRO_CAIXAGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_OUTROSGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_PENSAO_ALIMENTICIAGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_PREV_OFICIALGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_REND_EXT_N_ASSALGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_REND_EXT_OUTROSGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_REND_PJ_TRAB_NAO_ASSGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_TOTAL_DEDUC_MESGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TdmCarneLeao.cdsRegis_FVR_TRAB_NAO_ASSALARIADOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

function TdmCarneLeao.GerarArquivoExportacaoCarneLeao(IdCarneLeao: Integer;
  var Msg: String; var TipoMsg: Integer; DelArq: Boolean): Boolean;
var
  sStr, sArq,
  sNomeArq, sExt, sMes: String;
  tLog: TextFile;
begin
  Result := True;

  cdsIdent_F.Close;
  cdsIdent_F.Params.ParamByName('ID_CARNELEAO_IDENTIFICACAO').Value := IdCarneLeao;
  cdsIdent_F.Open;

  if cdsIdent_F.RecordCount = 0 then
  begin
    Msg     := 'N�o h� Carn�-Le�o gerado.';
    TipoMsg := 1;  //Aviso
    Result  := False;
    Exit;
  end
  else
  begin
    if cdsIdent_F.FieldByName('FLG_CANCELADO').AsString = 'S' then
    begin
      Msg     := 'O Carn�-Le�o gerado est� cancelado.';
      TipoMsg := 1;  //Aviso
      Result  := False;
      Exit;
    end
    else if not cdsIdent_F.FieldByName('NOME_ARQUIVO').IsNull then
    begin
      //No caso em que ha o comando para sobrescrever arquivo
      if DelArq then
      begin
        try
          if vgConSISTEMA.Connected then
            vgConSISTEMA.StartTransaction;

          //Apaga o arquivo
          DeleteFile(vgConf_DiretorioCarneLeao + '\' + IntToStr(YearOf(Date)) + '\' +
                     cdsIdent_F.FieldByName('NOME_ARQUIVO').AsString + '.' +
                     cdsIdent_F.FieldByName('EXTENSAO_ARQUIVO').AsString);

          //Apaga o nome no banco de dados
          cdsIdent_F.Edit;
          cdsIdent_F.FieldByName('NOME_ARQUIVO').Value     := Null;
          cdsIdent_F.FieldByName('EXTENSAO_ARQUIVO').Value := Null;
          cdsIdent_F.Post;
          cdsIdent_F.ApplyUpdates(0);

          if vgConSISTEMA.InTransaction then
            vgConSISTEMA.Commit;
        except
          if vgConSISTEMA.InTransaction then
            vgConSISTEMA.Rollback;

          Result  := False;
          Exit;
        end;
      end
      else
      begin
        if Trim(cdsIdent_F.FieldByName('TIPO_ENVIO').AsString) = 'E' then
          Msg := 'O arquivo de exporta��o desse Carn�-Le�o j� foi gerado. Deseja deletar esse e gerar um novo?'
        else if Trim(cdsIdent_F.FieldByName('TIPO_ENVIO').AsString) = 'I' then
          Msg := 'O arquivo de importa��o desse Carn�-Le�o j� foi gerado. Deseja deletar esse e gerar um novo?'
        else if Trim(cdsIdent_F.FieldByName('TIPO_ENVIO').AsString) = 'S' then
          Msg := 'O arquivo de seguran�a desse Carn�-Le�o j� foi gerado. Deseja deletar esse e gerar um novo?';

        TipoMsg := 2;  //Aviso para regerar arquivo
        Result  := False;
        Exit;
      end;
    end;
  end;

  sNomeArq := '';
  sExt     := '';

  { Nome do Arquivo (CPF-LEAO-(ANO DE REFERENCIA)-(ANO DE REFERENCIA)-EXPORTA-IRPF(ANO DE ENVIO DA DECLARACAO).DEC)
   Exemplo: 52957950715-LEAO-2017-2017-EXPORTA-IRPF2018.DEC }
  sNomeArq := cdsIdent_F.FieldByName('CPF_DECLARANTE').AsString +
              '-LEAO-' +
              cdsIdent_F.FieldByName('ANO_DECLARACAO').AsString +
              '-' +
              cdsIdent_F.FieldByName('ANO_DECLARACAO').AsString +
              '-EXPORTA-IRPF' +
              IntToStr(cdsIdent_F.FieldByName('ANO_DECLARACAO').AsInteger + 1);
  sExt := 'DEC';

  sArq := vgConf_DiretorioCarneLeao + '\' + IntToStr(YearOf(Date)) + '\' + sNomeArq + '.' + sExt;

  //Associa o arquivo a variavel do tipo TextFile
  AssignFile(tLog, sArq);

  if not(FileExists(sArq)) then
    ReWrite(tLog) //Cria arquivo
  else
    Append(tLog); //Coloca o cursor no final do arquivo

  { TITULO }
  sStr := 'IRCLEAO   ' +
           IntToStr(cdsIdent_F.FieldByName('ANO_DECLARACAO').AsInteger + 1) +
           cdsIdent_F.FieldByName('ANO_DECLARACAO').AsString +
           cdsIdent_F.FieldByName('CPF_DECLARANTE').AsString +
           '100';

  Writeln(tLog, sStr);

  { IDENTIFICACAO }
  //Escreve no arquivo e pula linha (Identificador, Sistema, Ano, CPF, Nome)
  sStr := cdsIdent_F.FieldByName('IDENTIFICADOR').AsString +
          dmGerencial.CompletaString('CLEAO' + cdsIdent_F.FieldByName('ANO_DECLARACAO').AsString,
                                     ' ',
                                     'D',
                                     14) +
          cdsIdent_F.FieldByName('ANO_DECLARACAO').AsString +
          cdsIdent_F.FieldByName('CPF_DECLARANTE').AsString +
          dmGerencial.CompletaString(cdsIdent_F.FieldByName('NOME_DECLARANTE').AsString,
                                     ' ',
                                     'D',
                                     60) +
          '117' +  //Esse e o campo ocupacaoPrincipal que, no caso, e Titular de Cartorio
          dmGerencial.CompletaString(' ',
                                     ' ',
                                     'D',
                                     31);
  Writeln(tLog, sStr);

  { DETALHE }
  case AnsiIndexStr(UpperCase(cdsIdent_F.FieldByName('TIPO_ENVIO').AsString), ['E', 'I', 'S']) of
    0:  //EXPORTACAO
    begin
      cdsRegis_F.Close;
      cdsRegis_F.Params.ParamByName('ID_CARNELEAO_IDENTIFICACAO').Value := IdCarneLeao;
      cdsRegis_F.Open;

      cdsRegis_F.First;

      while not cdsRegis_F.Eof do
      begin
        //Escreve no arquivo e pula linha
        sStr := cdsRegis_F.FieldByName('IDENTIFICADOR').AsString +
                dmGerencial.CompletaString(cdsRegis_F.FieldByName('NOME_MES_APURACAO').AsString,
                                           ' ',
                                           'D',
                                           15) +
                dmGerencial.CompletaString(dmGerencial.PegarNumeroTexto(cdsRegis_F.FieldByName('VR_TRAB_NAO_ASSALARIADO').AsString),
                                           '0',
                                           'E',
                                           13) +
                dmGerencial.CompletaString(dmGerencial.PegarNumeroTexto(cdsRegis_F.FieldByName('VR_ALUGUEIS').AsString),
                                           '0',
                                           'E',
                                           13) +
                dmGerencial.CompletaString(dmGerencial.PegarNumeroTexto(cdsRegis_F.FieldByName('VR_OUTROS').AsString),
                                           '0',
                                           'E',
                                           13) +
                dmGerencial.CompletaString(dmGerencial.PegarNumeroTexto(cdsRegis_F.FieldByName('VR_EXTERIOR').AsString),
                                           '0',
                                           'E',
                                           13) +
                dmGerencial.CompletaString(dmGerencial.PegarNumeroTexto(cdsRegis_F.FieldByName('VR_PREV_OFICIAL').AsString),
                                           '0',
                                           'E',
                                           13) +
                dmGerencial.CompletaString(dmGerencial.PegarNumeroTexto(cdsRegis_F.FieldByName('VR_DEPENDENTES').AsString),
                                           '0',
                                           'E',
                                           13) +
                dmGerencial.CompletaString(dmGerencial.PegarNumeroTexto(cdsRegis_F.FieldByName('VR_PENSAO_ALIMENTICIA').AsString),
                                           '0',
                                           'E',
                                           13) +
                dmGerencial.CompletaString(dmGerencial.PegarNumeroTexto(cdsRegis_F.FieldByName('VR_LIVRO_CAIXA').AsString),
                                           '0',
                                           'E',
                                           13) +
                dmGerencial.CompletaString(dmGerencial.PegarNumeroTexto(cdsRegis_F.FieldByName('VR_IMP_EXTERIOR_ACOMP').AsString),
                                           '0',
                                           'E',
                                           13) +
                dmGerencial.CompletaString(dmGerencial.PegarNumeroTexto(cdsRegis_F.FieldByName('VR_IMP_APAGAR').AsString),
                                           '0',
                                           'E',
                                           13) +
                dmGerencial.CompletaString(dmGerencial.PegarNumeroTexto(cdsRegis_F.FieldByName('VR_IMP_PAGO').AsString),
                                           '0',
                                           'E',
                                           13);

        Writeln(tLog, sStr);

        cdsRegis_F.Next;
      end;
    end;
    1:  //IMPORTACAO
    begin
      //Escreve no arquivo e pula linha
      { sStr := '';
      Writeln(tLog, sStr); }
    end;
    2:  //SEGURANCA
    begin
      //Escreve no arquivo e pula linha
      { sStr := '';
      Writeln(tLog, sStr); }
    end;
  end;

  { ESCRITURACAO }
  if Trim(cdsIdent_F.FieldByName('TIPO_ENVIO').AsString) = 'E' then
  begin
    sMes := '';

    cdsRegis_F.Close;
    cdsRegis_F.Params.ParamByName('ID_CARNELEAO_IDENTIFICACAO').Value := IdCarneLeao;
    cdsRegis_F.Open;

    cdsRegis_F.First;

    while not cdsRegis_F.Eof do
    begin
      case AnsiIndexStr(UpperCase(cdsRegis_F.FieldByName('NOME_MES_APURACAO').AsString), ['JANEIRO',
                                                                                          'FEVEREIRO',
                                                                                          'MARCO',
                                                                                          'ABRIL',
                                                                                          'MAIO',
                                                                                          'JUNHO',
                                                                                          'JULHO',
                                                                                          'AGOSTO',
                                                                                          'SETEMBRO',
                                                                                          'OUTUBRO',
                                                                                          'NOVEMBRO',
                                                                                          'DEZEMBRO']) of
        0: sMes := '01';
        1: sMes := '02';
        2: sMes := '03';
        3: sMes := '04';
        4: sMes := '05';
        5: sMes := '06';
        6: sMes := '07';
        7: sMes := '08';
        8: sMes := '09';
        9: sMes := '10';
        10: sMes := '11';
        11: sMes := '12';
      end;

      //Escreve no arquivo e pula linha
      if cdsRegis_F.FieldByName('VR_TRAB_NAO_ASSALARIADO').AsCurrency > 0 then
      begin
        sStr := '03' +
                sMes +
                cdsIdent_F.FieldByName('ANO_DECLARACAO').AsString +
                dmGerencial.CompletaString('1000',
                                           ' ',
                                           'D',
                                           26) +  //1000 e o Codigo para Trabalho Nao Assalariado
                dmGerencial.CompletaString(dmGerencial.PegarNumeroTexto(cdsRegis_F.FieldByName('VR_TRAB_NAO_ASSALARIADO').AsString),
                                           '0',
                                           'E',
                                           13);

        Writeln(tLog, sStr);
      end;

      cdsRegis_F.Next;
    end;
  end;

  { CONTROLE }
  cdsCtrl_F.Close;
  cdsCtrl_F.Params.ParamByName('ID_CARNELEAO_IDENTIFICACAO').Value := IdCarneLeao;
  cdsCtrl_F.Open;

  //Escreve no arquivo e pula linha
  sStr := cdsCtrl_F.FieldByName('IDENTIFICADOR').AsString +
          cdsCtrl_F.FieldByName('CONTROLE').AsString;

  Writeln(tLog, sStr);

  //Fecha o Arquivo
  CloseFile(tLog);

  //Grava o nome do Arquivo no banco de dados
  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    //Grava nome do arquivo
    cdsIdent_F.Edit;
    cdsIdent_F.FieldByName('NOME_ARQUIVO').Value     := sNomeArq;
    cdsIdent_F.FieldByName('EXTENSAO_ARQUIVO').Value := sExt;
    cdsIdent_F.Post;
    cdsIdent_F.ApplyUpdates(0);

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
  end;
end;

function TdmCarneLeao.ExisteLancamentoSemCarneLeao(Ano: Integer; var LstLancs: String): Boolean;
var
  QryVerif: TFDQuery;
begin
  { Verifica se ha Parcelas pagas cujos Lancamentos nao apresentem a
    classificacao do Carne-Leao }
  Result   := False;
  LstLancs := '';

  QryVerif := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  QryVerif.Close;
  QryVerif.SQL.Clear;
  QryVerif.SQL.Text := 'SELECT L.COD_LANCAMENTO, ' +
                       '       L.ANO_LANCAMENTO ' +
                       '  FROM LANCAMENTO_PARC LP ' +
                        'INNER JOIN LANCAMENTO L ' +
                       '    ON LP.COD_LANCAMENTO_FK = L.COD_LANCAMENTO ' +
                       '   AND LP.ANO_LANCAMENTO_FK = L.ANO_LANCAMENTO ' +
                       ' WHERE LP.FLG_STATUS = ' + QuotedStr('G') +
                       '   AND L.FLG_CANCELADO = ' + QuotedStr('N') +
                       '   AND L.FLG_IMPOSTORENDA = ' + QuotedStr('S') +
                       '   AND EXTRACT(YEAR FROM LP.DATA_PAGAMENTO)  = :ANO ' +
                       '   AND L.ID_CARNELEAO_EQUIVALENCIA_FK IS NULL';
  QryVerif.Params.ParamByName('ANO').Value := Ano;
  QryVerif.Open;

  QryVerif.First;

  while not QryVerif.Eof do
  begin
    if Trim(LstLancs) = '' then
      LstLancs := QryVerif.FieldByName('ANO_LANCAMENTO').AsString + QryVerif.FieldByName('COD_LANCAMENTO').AsString
    else
      LstLancs := LstLancs + ', ' + QryVerif.FieldByName('ANO_LANCAMENTO').AsString + QryVerif.FieldByName('COD_LANCAMENTO').AsString;

    QryVerif.Next;
  end;

  Result := (QryVerif.RecordCount > 0);

  FreeAndNil(QryVerif);
end;

end.
