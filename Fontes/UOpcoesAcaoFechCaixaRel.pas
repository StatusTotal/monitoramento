{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UOpcoesAcaoFechCaixaRel.pas
  Descricao:   Tela de Opcoes de Acao para impressao de relatorios atrelados ao
               Fechamento de Caixa
  Author   :   Cristina
  Date:        25-ago-2017
  Last Update: 16-out-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UOpcoesAcaoFechCaixaRel;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UOpcoesAcaoPadrao, System.Actions,
  Vcl.ActnList, Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan, JvExControls,
  JvButton, JvTransparentButton, Vcl.ExtCtrls;

type
  TFOpcoesAcaoFechCaixaRel = class(TFOpcoesAcaoPadrao)
    acmBotoes: TActionManager;
    actRelatorioDiferencas: TAction;
    actRelatorioMovimentoCaixa: TAction;
    procedure actRelatorioDiferencasExecute(Sender: TObject);
    procedure actRelatorioMovimentoCaixaExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    DataMovimento: TDatetime;
  end;

var
  FOpcoesAcaoFechCaixaRel: TFOpcoesAcaoFechCaixaRel;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UVariaveisGlobais, UDM, UGDM;

procedure TFOpcoesAcaoFechCaixaRel.actRelatorioMovimentoCaixaExecute(Sender: TObject);
begin
  inherited;

  dmPrincipal.AbrirRelatorio(9, False, DataMovimento);
  Self.Close;
end;

procedure TFOpcoesAcaoFechCaixaRel.FormShow(Sender: TObject);
begin
  inherited;

  btnOpcao1.Enabled  := vgPrm_ImpFecCx or vgPrm_ImpRelMovCx;   //Movimento de Caixa
  btnOpcao2.Enabled  := vgPrm_ImpRelDifFechCx;  //Diferencas
end;

procedure TFOpcoesAcaoFechCaixaRel.actRelatorioDiferencasExecute(Sender: TObject);
begin
  inherited;

  dmPrincipal.AbrirRelatorio(10);
  Self.Close;
end;

end.
