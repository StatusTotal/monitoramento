object dmLancamento: TdmLancamento
  OldCreateOrder = False
  Height = 393
  Width = 596
  object qryLancamento_F: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM LANCAMENTO'
      ' WHERE COD_LANCAMENTO = :COD_LANCAMENTO'
      '   AND ANO_LANCAMENTO = :ANO_LANCAMENTO')
    Left = 40
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'COD_LANCAMENTO'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'ANO_LANCAMENTO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object qryParcela_F: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM LANCAMENTO_PARC'
      ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO'
      '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO'
      'ORDER BY NUM_PARCELA, DATA_VENCIMENTO')
    Left = 128
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'COD_LANCAMENTO'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'ANO_LANCAMENTO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspLancamento_F: TDataSetProvider
    DataSet = qryLancamento_F
    Left = 40
    Top = 64
  end
  object dspParcela_F: TDataSetProvider
    DataSet = qryParcela_F
    Left = 128
    Top = 64
  end
  object cdsLancamento_F: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'COD_LANCAMENTO'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ANO_LANCAMENTO'
        ParamType = ptInput
      end>
    ProviderName = 'dspLancamento_F'
    Left = 40
    Top = 112
    object cdsLancamento_FCOD_LANCAMENTO: TIntegerField
      FieldName = 'COD_LANCAMENTO'
      Required = True
    end
    object cdsLancamento_FANO_LANCAMENTO: TIntegerField
      FieldName = 'ANO_LANCAMENTO'
      Required = True
    end
    object cdsLancamento_FDATA_LANCAMENTO: TDateField
      FieldName = 'DATA_LANCAMENTO'
    end
    object cdsLancamento_FTIPO_LANCAMENTO: TStringField
      FieldName = 'TIPO_LANCAMENTO'
      FixedChar = True
      Size = 1
    end
    object cdsLancamento_FTIPO_CADASTRO: TStringField
      FieldName = 'TIPO_CADASTRO'
      FixedChar = True
      Size = 1
    end
    object cdsLancamento_FID_CATEGORIA_DESPREC_FK: TIntegerField
      FieldName = 'ID_CATEGORIA_DESPREC_FK'
    end
    object cdsLancamento_FID_SUBCATEGORIA_DESPREC_FK: TIntegerField
      FieldName = 'ID_SUBCATEGORIA_DESPREC_FK'
    end
    object cdsLancamento_FID_NATUREZA_FK: TIntegerField
      FieldName = 'ID_NATUREZA_FK'
    end
    object cdsLancamento_FFLG_IMPOSTORENDA: TStringField
      FieldName = 'FLG_IMPOSTORENDA'
      FixedChar = True
      Size = 1
    end
    object cdsLancamento_FFLG_PESSOAL: TStringField
      FieldName = 'FLG_PESSOAL'
      FixedChar = True
      Size = 1
    end
    object cdsLancamento_FFLG_AUXILIAR: TStringField
      FieldName = 'FLG_AUXILIAR'
      FixedChar = True
      Size = 1
    end
    object cdsLancamento_FFLG_REAL: TStringField
      FieldName = 'FLG_REAL'
      FixedChar = True
      Size = 1
    end
    object cdsLancamento_FFLG_FLUTUANTE: TStringField
      FieldName = 'FLG_FLUTUANTE'
      FixedChar = True
      Size = 1
    end
    object cdsLancamento_FID_CLIENTE_FORNECEDOR_FK: TIntegerField
      FieldName = 'ID_CLIENTE_FORNECEDOR_FK'
    end
    object cdsLancamento_FQTD_PARCELAS: TIntegerField
      FieldName = 'QTD_PARCELAS'
    end
    object cdsLancamento_FVR_TOTAL_PREV: TBCDField
      FieldName = 'VR_TOTAL_PREV'
      OnGetText = cdsLancamento_FVR_TOTAL_PREVGetText
      Precision = 18
      Size = 2
    end
    object cdsLancamento_FVR_TOTAL_JUROS: TBCDField
      FieldName = 'VR_TOTAL_JUROS'
      OnGetText = cdsLancamento_FVR_TOTAL_JUROSGetText
      Precision = 18
      Size = 2
    end
    object cdsLancamento_FVR_TOTAL_DESCONTO: TBCDField
      FieldName = 'VR_TOTAL_DESCONTO'
      OnGetText = cdsLancamento_FVR_TOTAL_DESCONTOGetText
      Precision = 18
      Size = 2
    end
    object cdsLancamento_FVR_TOTAL_PAGO: TBCDField
      FieldName = 'VR_TOTAL_PAGO'
      OnGetText = cdsLancamento_FVR_TOTAL_PAGOGetText
      Precision = 18
      Size = 2
    end
    object cdsLancamento_FDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsLancamento_FCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsLancamento_FFLG_STATUS: TStringField
      FieldName = 'FLG_STATUS'
      FixedChar = True
      Size = 1
    end
    object cdsLancamento_FDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsLancamento_FFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsLancamento_FCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsLancamento_FFLG_RECORRENTE: TStringField
      FieldName = 'FLG_RECORRENTE'
      FixedChar = True
      Size = 1
    end
    object cdsLancamento_FFLG_REPLICADO: TStringField
      FieldName = 'FLG_REPLICADO'
      FixedChar = True
      Size = 1
    end
    object cdsLancamento_FNUM_RECIBO: TIntegerField
      FieldName = 'NUM_RECIBO'
    end
    object cdsLancamento_FOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 1000
    end
    object cdsLancamento_FID_ORIGEM: TIntegerField
      FieldName = 'ID_ORIGEM'
    end
    object cdsLancamento_FID_SISTEMA_FK: TIntegerField
      FieldName = 'ID_SISTEMA_FK'
    end
    object cdsLancamento_FID_CARNELEAO_EQUIVALENCIA_FK: TIntegerField
      FieldName = 'ID_CARNELEAO_EQUIVALENCIA_FK'
    end
    object cdsLancamento_FFLG_FORAFECHCAIXA: TStringField
      FieldName = 'FLG_FORAFECHCAIXA'
      FixedChar = True
      Size = 1
    end
  end
  object cdsParcela_F: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'COD_LANCAMENTO'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ANO_LANCAMENTO'
        ParamType = ptInput
      end>
    ProviderName = 'dspParcela_F'
    Left = 128
    Top = 112
    object cdsParcela_FID_LANCAMENTO_PARC: TIntegerField
      FieldName = 'ID_LANCAMENTO_PARC'
      Required = True
    end
    object cdsParcela_FCOD_LANCAMENTO_FK: TIntegerField
      FieldName = 'COD_LANCAMENTO_FK'
    end
    object cdsParcela_FANO_LANCAMENTO_FK: TIntegerField
      FieldName = 'ANO_LANCAMENTO_FK'
    end
    object cdsParcela_FDATA_LANCAMENTO_PARC: TDateField
      FieldName = 'DATA_LANCAMENTO_PARC'
    end
    object cdsParcela_FNUM_PARCELA: TIntegerField
      FieldName = 'NUM_PARCELA'
    end
    object cdsParcela_FDATA_VENCIMENTO: TDateField
      FieldName = 'DATA_VENCIMENTO'
    end
    object cdsParcela_FVR_PARCELA_PREV: TBCDField
      FieldName = 'VR_PARCELA_PREV'
      OnGetText = cdsParcela_FVR_PARCELA_PREVGetText
      Precision = 18
      Size = 2
    end
    object cdsParcela_FVR_PARCELA_JUROS: TBCDField
      FieldName = 'VR_PARCELA_JUROS'
      OnGetText = cdsParcela_FVR_PARCELA_JUROSGetText
      Precision = 18
      Size = 2
    end
    object cdsParcela_FVR_PARCELA_DESCONTO: TBCDField
      FieldName = 'VR_PARCELA_DESCONTO'
      OnGetText = cdsParcela_FVR_PARCELA_DESCONTOGetText
      Precision = 18
      Size = 2
    end
    object cdsParcela_FVR_PARCELA_PAGO: TBCDField
      FieldName = 'VR_PARCELA_PAGO'
      OnGetText = cdsParcela_FVR_PARCELA_PAGOGetText
      Precision = 18
      Size = 2
    end
    object cdsParcela_FAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Size = 10
    end
    object cdsParcela_FCONTA: TStringField
      FieldName = 'CONTA'
      Size = 10
    end
    object cdsParcela_FID_BANCO_FK: TIntegerField
      FieldName = 'ID_BANCO_FK'
    end
    object cdsParcela_FNUM_COD_DEPOSITO: TStringField
      FieldName = 'NUM_COD_DEPOSITO'
      Size = 600
    end
    object cdsParcela_FNUM_CHEQUE: TStringField
      FieldName = 'NUM_CHEQUE'
      Size = 10
    end
    object cdsParcela_FFLG_STATUS: TStringField
      FieldName = 'FLG_STATUS'
      FixedChar = True
      Size = 1
    end
    object cdsParcela_FDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsParcela_FCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsParcela_FDATA_PAGAMENTO: TDateField
      FieldName = 'DATA_PAGAMENTO'
    end
    object cdsParcela_FPAG_ID_USUARIO: TIntegerField
      FieldName = 'PAG_ID_USUARIO'
    end
    object cdsParcela_FOBS_LANCAMENTO_PARC: TStringField
      FieldName = 'OBS_LANCAMENTO_PARC'
      Size = 1000
    end
    object cdsParcela_FDATA_CADASTRO: TDateField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsParcela_FCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsParcela_FNOVO: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'NOVO'
    end
    object cdsParcela_FPRE_CADASTRADO: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'PRE_CADASTRADO'
    end
    object cdsParcela_FID_FORMAPAGAMENTO_FK: TIntegerField
      FieldName = 'ID_FORMAPAGAMENTO_FK'
    end
    object cdsParcela_FVR_DIFERENCA: TCurrencyField
      FieldKind = fkInternalCalc
      FieldName = 'VR_DIFERENCA'
    end
    object cdsParcela_FTP_DIFERENCA: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'TP_DIFERENCA'
    end
    object cdsParcela_FDATA_PAGAMENTO_ANT: TDateField
      FieldKind = fkInternalCalc
      FieldName = 'DATA_PAGAMENTO_ANT'
    end
    object cdsParcela_FID_FORMAPAGAMENTO_ORIG_FK: TIntegerField
      FieldName = 'ID_FORMAPAGAMENTO_ORIG_FK'
    end
  end
  object dsLancamento_F: TDataSource
    DataSet = cdsLancamento_F
    Left = 40
    Top = 160
  end
  object dsParcela_F: TDataSource
    DataSet = cdsParcela_F
    Left = 128
    Top = 160
  end
  object qryCliFor: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT ID_CLIENTE_FORNECEDOR, RAZAO_SOCIAL'
      '  FROM CLIENTE_FORNECEDOR'
      ' WHERE TIPO_CLIENTE_FORNECEDOR = :TIPO_CLIFOR'
      
        '   AND FLG_ATIVO = (CASE WHEN (CAST(:FLG_ATIVO_CF1 AS CHAR(1)) =' +
        ' '#39#39') THEN FLG_ATIVO ELSE CAST(:FLG_ATIVO_CF2 AS CHAR(1)) END)'
      'ORDER BY RAZAO_SOCIAL')
    Left = 248
    Top = 280
    ParamData = <
      item
        Name = 'TIPO_CLIFOR'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'FLG_ATIVO_CF1'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'FLG_ATIVO_CF2'
        DataType = ftString
        ParamType = ptInput
      end>
  end
  object qryCategoria: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT ID_CATEGORIA_DESPREC, DESCR_CATEGORIA_DESPREC'
      '  FROM CATEGORIA_DESPREC'
      ' WHERE ID_NATUREZA_FK = :ID_NAT'
      '   AND TIPO = :TIPO_CAT'
      'ORDER BY DESCR_CATEGORIA_DESPREC')
    Left = 96
    Top = 280
    ParamData = <
      item
        Name = 'ID_NAT'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'TIPO_CAT'
        DataType = ftString
        ParamType = ptInput
      end>
  end
  object qrySubCategoria: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM SUBCATEGORIA_DESPREC'
      ' WHERE ID_CATEGORIA_DESPREC_FK = :ID_CAT'
      '   AND TIPO = :TIPO_SUBCAT'
      'ORDER BY DESCR_SUBCATEGORIA_DESPREC')
    Left = 176
    Top = 280
    ParamData = <
      item
        Name = 'ID_CAT'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'TIPO_SUBCAT'
        DataType = ftString
        ParamType = ptInput
        Size = 1
      end>
  end
  object qryFormaPagto: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM FORMAPAGAMENTO'
      
        ' WHERE FLG_ATIVA = (CASE WHEN (CAST(:FLG_ATIVA_FP1 AS CHAR(1)) =' +
        ' '#39#39') THEN FLG_ATIVA ELSE CAST(:FLG_ATIVA_FP2 AS CHAR(1)) END)'
      'ORDER BY DESCR_FORMAPAGAMENTO')
    Left = 440
    Top = 280
    ParamData = <
      item
        Name = 'FLG_ATIVA_FP1'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'FLG_ATIVA_FP2'
        DataType = ftString
        ParamType = ptInput
      end>
  end
  object qryItem: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT ID_ITEM,'
      '       TIPO_ITEM,'
      '       DESCR_ITEM,'
      '       ULT_VALOR_CUSTO,'
      '       ESTOQUE_ATUAL,'
      '       DESCR_UNIDADEMEDIDA,'
      '       FLG_FUNCIONARIO'
      '  FROM ITEM'
      
        ' WHERE FLG_ATIVO = (CASE WHEN (CAST(:FLG_ATIVO_I1 AS CHAR(1)) = ' +
        #39#39') THEN FLG_ATIVO ELSE CAST(:FLG_ATIVO_I2 AS CHAR(1)) END)'
      
        '   AND TIPO_ITEM = (CASE WHEN (CAST(:TIPO_ITEM1 AS CHAR(1)) = '#39#39 +
        ') THEN TIPO_ITEM ELSE CAST(:TIPO_ITEM2 AS CHAR(1)) END)'
      'ORDER BY DESCR_ITEM')
    Left = 376
    Top = 280
    ParamData = <
      item
        Name = 'FLG_ATIVO_I1'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'FLG_ATIVO_I2'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'TIPO_ITEM1'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'TIPO_ITEM2'
        DataType = ftString
        ParamType = ptInput
      end>
  end
  object dsCliFor: TDataSource
    DataSet = qryCliFor
    Left = 248
    Top = 328
  end
  object dsCategoria: TDataSource
    DataSet = qryCategoria
    Left = 96
    Top = 328
  end
  object dsSubCategoria: TDataSource
    DataSet = qrySubCategoria
    Left = 176
    Top = 328
  end
  object dsFormaPagto: TDataSource
    DataSet = qryFormaPagto
    Left = 440
    Top = 328
  end
  object dsItem: TDataSource
    DataSet = qryItem
    Left = 376
    Top = 328
  end
  object dsImagem_F: TDataSource
    DataSet = cdsImagem_F
    Left = 304
    Top = 160
  end
  object cdsImagem_F: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'COD_LANCAMENTO'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ANO_LANCAMENTO'
        ParamType = ptInput
      end>
    ProviderName = 'dspImagem_F'
    OnCalcFields = cdsImagem_FCalcFields
    Left = 304
    Top = 112
    object cdsImagem_FID_LANCAMENTO_IMG: TIntegerField
      FieldName = 'ID_LANCAMENTO_IMG'
      Required = True
    end
    object cdsImagem_FCOD_LANCAMENTO_FK: TIntegerField
      FieldName = 'COD_LANCAMENTO_FK'
    end
    object cdsImagem_FANO_LANCAMENTO_FK: TIntegerField
      FieldName = 'ANO_LANCAMENTO_FK'
    end
    object cdsImagem_FNOME_FIXO: TStringField
      FieldName = 'NOME_FIXO'
      Size = 18
    end
    object cdsImagem_FNOME_VARIAVEL: TStringField
      FieldName = 'NOME_VARIAVEL'
      Size = 100
    end
    object cdsImagem_FEXTENSAO: TStringField
      FieldName = 'EXTENSAO'
      Size = 4
    end
    object cdsImagem_FNOVO: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'NOVO'
    end
    object cdsImagem_FORIGEM_ANTIGA: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'ORIGEM_ANTIGA'
      Size = 2000
    end
    object cdsImagem_FNOME_V_ANTIGO: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOME_V_ANTIGO'
      Size = 1000
    end
    object cdsImagem_FCAMINHO_COMPLETO: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAMINHO_COMPLETO'
      Size = 2000
    end
  end
  object dspImagem_F: TDataSetProvider
    DataSet = qryImagem_F
    Left = 304
    Top = 64
  end
  object qryImagem_F: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM LANCAMENTO_IMG'
      ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO'
      '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO'
      'ORDER BY NOME_FIXO, NOME_VARIAVEL')
    Left = 304
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'COD_LANCAMENTO'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'ANO_LANCAMENTO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object qryBanco: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM BANCO'
      'ORDER BY DESCR_BANCO')
    Left = 512
    Top = 280
  end
  object dsBanco: TDataSource
    DataSet = qryBanco
    Left = 512
    Top = 328
  end
  object qryDetalhe_F: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM LANCAMENTO_DET'
      ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO'
      '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO'
      'ORDER BY ID_LANCAMENTO_DET')
    Left = 216
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'COD_LANCAMENTO'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'ANO_LANCAMENTO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspDetalhe_F: TDataSetProvider
    DataSet = qryDetalhe_F
    Left = 216
    Top = 64
  end
  object cdsDetalhe_F: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'COD_LANCAMENTO'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ANO_LANCAMENTO'
        ParamType = ptInput
      end>
    ProviderName = 'dspDetalhe_F'
    OnCalcFields = cdsDetalhe_FCalcFields
    Left = 216
    Top = 112
    object cdsDetalhe_FID_LANCAMENTO_DET: TIntegerField
      FieldName = 'ID_LANCAMENTO_DET'
      Required = True
    end
    object cdsDetalhe_FCOD_LANCAMENTO_FK: TIntegerField
      FieldName = 'COD_LANCAMENTO_FK'
    end
    object cdsDetalhe_FANO_LANCAMENTO_FK: TIntegerField
      FieldName = 'ANO_LANCAMENTO_FK'
    end
    object cdsDetalhe_FID_ITEM_FK: TIntegerField
      FieldName = 'ID_ITEM_FK'
    end
    object cdsDetalhe_FDESCR_ITEM: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCR_ITEM'
      Size = 250
    end
    object cdsDetalhe_FVR_UNITARIO: TBCDField
      FieldName = 'VR_UNITARIO'
      OnGetText = cdsDetalhe_FVR_UNITARIOGetText
      Precision = 18
      Size = 2
    end
    object cdsDetalhe_FQUANTIDADE: TIntegerField
      FieldName = 'QUANTIDADE'
      OnGetText = cdsDetalhe_FQUANTIDADEGetText
    end
    object cdsDetalhe_FVR_TOTAL: TBCDField
      FieldName = 'VR_TOTAL'
      OnGetText = cdsDetalhe_FVR_TOTALGetText
      Precision = 18
      Size = 2
    end
    object cdsDetalhe_FSELO_ORIGEM: TStringField
      FieldName = 'SELO_ORIGEM'
      Size = 9
    end
    object cdsDetalhe_FALEATORIO_ORIGEM: TStringField
      FieldName = 'ALEATORIO_ORIGEM'
      FixedChar = True
      Size = 3
    end
    object cdsDetalhe_FTIPO_COBRANCA: TStringField
      FieldName = 'TIPO_COBRANCA'
      FixedChar = True
      Size = 2
    end
    object cdsDetalhe_FCOD_ADICIONAL: TIntegerField
      FieldName = 'COD_ADICIONAL'
    end
    object cdsDetalhe_FEMOLUMENTOS: TBCDField
      FieldName = 'EMOLUMENTOS'
      OnGetText = cdsDetalhe_FEMOLUMENTOSGetText
      Precision = 18
      Size = 2
    end
    object cdsDetalhe_FFETJ: TBCDField
      FieldName = 'FETJ'
      OnGetText = cdsDetalhe_FFETJGetText
      Precision = 18
      Size = 2
    end
    object cdsDetalhe_FFUNDPERJ: TBCDField
      FieldName = 'FUNDPERJ'
      OnGetText = cdsDetalhe_FFUNDPERJGetText
      Precision = 18
      Size = 2
    end
    object cdsDetalhe_FFUNPERJ: TBCDField
      FieldName = 'FUNPERJ'
      OnGetText = cdsDetalhe_FFUNPERJGetText
      Precision = 18
      Size = 2
    end
    object cdsDetalhe_FFUNARPEN: TBCDField
      FieldName = 'FUNARPEN'
      OnGetText = cdsDetalhe_FFUNARPENGetText
      Precision = 18
      Size = 2
    end
    object cdsDetalhe_FPMCMV: TBCDField
      FieldName = 'PMCMV'
      OnGetText = cdsDetalhe_FPMCMVGetText
      Precision = 18
      Size = 2
    end
    object cdsDetalhe_FISS: TBCDField
      FieldName = 'ISS'
      OnGetText = cdsDetalhe_FISSGetText
      Precision = 18
      Size = 2
    end
    object cdsDetalhe_FFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsDetalhe_FNOVO: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'NOVO'
    end
    object cdsDetalhe_FPRE_CADASTRADO: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'PRE_CADASTRADO'
    end
    object cdsDetalhe_FQTD_ANTERIOR: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'QTD_ANTERIOR'
    end
    object cdsDetalhe_FNUM_PROTOCOLO: TIntegerField
      FieldName = 'NUM_PROTOCOLO'
    end
    object cdsDetalhe_FMUTUA: TBCDField
      FieldName = 'MUTUA'
      OnGetText = cdsDetalhe_FMUTUAGetText
      Precision = 18
      Size = 2
    end
    object cdsDetalhe_FACOTERJ: TBCDField
      FieldName = 'ACOTERJ'
      OnGetText = cdsDetalhe_FACOTERJGetText
      Precision = 18
      Size = 2
    end
  end
  object dsDetalhe_F: TDataSource
    DataSet = cdsDetalhe_F
    Left = 216
    Top = 160
  end
  object dsNatureza: TDataSource
    DataSet = qryNatureza
    Left = 24
    Top = 328
  end
  object qryNatureza: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM NATUREZA'
      ' WHERE TIPO_NATUREZA = :TIPO_NATUREZA'
      'ORDER BY DESCR_NATUREZA')
    Left = 24
    Top = 280
    ParamData = <
      item
        Name = 'TIPO_NATUREZA'
        DataType = ftFixedChar
        ParamType = ptInput
        Size = 1
      end>
    object qryNaturezaID_NATUREZA: TIntegerField
      FieldName = 'ID_NATUREZA'
      Origin = 'ID_NATUREZA'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryNaturezaDESCR_NATUREZA: TStringField
      FieldName = 'DESCR_NATUREZA'
      Origin = 'DESCR_NATUREZA'
      Size = 250
    end
    object qryNaturezaTIPO_NATUREZA: TStringField
      FieldName = 'TIPO_NATUREZA'
      Origin = 'TIPO_NATUREZA'
      FixedChar = True
      Size = 1
    end
    object qryNaturezaFLG_EDITAVEL: TStringField
      FieldName = 'FLG_EDITAVEL'
      Origin = 'FLG_EDITAVEL'
      FixedChar = True
      Size = 1
    end
    object qryNaturezaFLG_IMPOSTORENDA: TStringField
      FieldName = 'FLG_IMPOSTORENDA'
      Origin = 'FLG_IMPOSTORENDA'
      FixedChar = True
      Size = 1
    end
    object qryNaturezaFLG_AUXILIAR: TStringField
      FieldName = 'FLG_AUXILIAR'
      Origin = 'FLG_AUXILIAR'
      FixedChar = True
      Size = 1
    end
    object qryNaturezaFLG_PESSOAL: TStringField
      FieldName = 'FLG_PESSOAL'
      Origin = 'FLG_PESSOAL'
      FixedChar = True
      Size = 1
    end
  end
  object qryCarneLeao: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT CLE.ID_CARNELEAO_EQUIVALENCIA,'
      '       CLC.DESCR_CARNELEAO_CLASSIFICACAO'
      '  FROM CARNELEAO_CLASSIFICACAO CLC'
      ' INNER JOIN CARNELEAO_EQUIVALENCIA CLE'
      
        '    ON CLC.ID_CARNELEAO_CLASSIFICACAO = CLE.ID_CARNELEAO_CLASSIF' +
        'ICACAO_FK')
    Left = 312
    Top = 280
  end
  object dsCarneLeao: TDataSource
    DataSet = qryCarneLeao
    Left = 312
    Top = 328
  end
end
