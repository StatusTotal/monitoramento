{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UListaOficios.pas
  Descricao:   Lista Simples de Oficios
  Author   :   Cristina
  Date:        23-jan-2017
  Last Update:
------------------------------------------------------------------------------------------------------------------------}

unit UListaOficios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UListaSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Mask, JvExMask, JvToolEdit, System.DateUtils;

type
  TFListaOficios = class(TFListaSimplesPadrao)
    lblPeriodo: TLabel;
    Label2: TLabel;
    dtePeriodoIni: TJvDateEdit;
    dtePeriodoFim: TJvDateEdit;
    qryGridListaPadraoID: TIntegerField;
    qryGridListaPadraoCOD_OFICIO: TIntegerField;
    qryGridListaPadraoANO_OFICIO: TIntegerField;
    qryGridListaPadraoTIPO_OFICIO: TStringField;
    qryGridListaPadraoDATA_OFICIO: TDateField;
    qryGridListaPadraoHORA_OFICIO: TTimeField;
    qryGridListaPadraoID_FUNCIONARIO_FK: TIntegerField;
    qryGridListaPadraoNOME_FUNCIONARIO: TStringField;
    qryGridListaPadraoCAD_ID_USUARIO: TIntegerField;
    qryGridListaPadraoDATA_CADASTRO: TSQLTimeStampField;
    qryGridListaPadraoFLG_STATUS: TStringField;
    qryGridListaPadraoSTA_ID_USUARIO: TIntegerField;
    qryGridListaPadraoDATA_STATUS: TSQLTimeStampField;
    qryGridListaPadraoASSUNTO_OFICIO: TStringField;
    qryGridListaPadraoID_OFICIO_PESSOA_FK: TIntegerField;
    qryGridListaPadraoNOME_OFICIO_PESSOA: TStringField;
    qryGridListaPadraoNUM_PROCESSO: TStringField;
    qryGridListaPadraoID_OFICIO_ORGAO_FK: TIntegerField;
    qryGridListaPadraoNOME_OFICIO_ORGAO: TStringField;
    qryGridListaPadraoID_OFICIO_FORMAENVIO_FK: TIntegerField;
    qryGridListaPadraoDESCR_OFICIO_FORMAENVIO: TStringField;
    qryGridListaPadraoNUM_RASTREIO: TStringField;
    qryGridListaPadraoNUM_PROTOCOLO: TStringField;
    qryGridListaPadraoDATA_SITUACAO_AR: TDateField;
    qryGridListaPadraoFLG_SITUACAO_AR: TStringField;
    qryGridListaPadraoOBS_OFICIO: TStringField;
    qryGridListaPadraoID_OFICIO_FK: TIntegerField;
    qryGridListaPadraoSEQ_OFICIO: TIntegerField;
    qryGridListaPadraoNUM_OFICIO: TStringField;
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure dtePeriodoFimKeyPress(Sender: TObject; var Key: Char);
    procedure dtePeriodoIniKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dbgGridListaPadraoDblClick(Sender: TObject);
    procedure dbgGridListaPadraoTitleClick(Column: TColumn);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FListaOficios: TFListaOficios;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

procedure TFListaOficios.btnFiltrarClick(Sender: TObject);
begin
  inherited;

  qryGridListaPadrao.Close;
  qryGridListaPadrao.Params.ParamByName('DATA_INI').Value    := dtePeriodoIni.Date;
  qryGridListaPadrao.Params.ParamByName('DATA_FIM').Value    := dtePeriodoFim.Date;
  qryGridListaPadrao.Params.ParamByName('TIPO_OFICIO').Value := Trim(vgOfc_TpOfc);
  qryGridListaPadrao.Open;
end;

procedure TFListaOficios.btnLimparClick(Sender: TObject);
begin
  dtePeriodoIni.Date := StartOfTheMonth(Date);
  dtePeriodoFim.Date := EndOfTheMonth(Date);

  inherited;

  if dtePeriodoIni.CanFocus then
    dtePeriodoFim.SetFocus;
end;

procedure TFListaOficios.dbgGridListaPadraoDblClick(Sender: TObject);
var
  sTexto: String;
begin
  inherited;

  if qryGridListaPadrao.RecordCount = 0 then
    vgOfc_IdAssoc := 0
  else
  begin
    if qryGridListaPadrao.FieldByName('ID_OFICIO_FK').IsNull then
      vgOfc_IdAssoc := qryGridListaPadrao.FieldByName('ID').AsInteger
    else
      vgOfc_IdAssoc := qryGridListaPadrao.FieldByName('ID_OFICIO_FK').AsInteger;

    sTexto := 'Confirma a sele��o do Of�cio ' + qryGridListaPadrao.FieldByName('ID').AsString + '?';

    if Application.MessageBox(PChar(sTexto), 'Confirma��o', MB_YESNO + MB_ICONQUESTION) = ID_YES then
      Self.Close
    else
      vgOfc_IdAssoc := 0;
  end;
end;

procedure TFListaOficios.dbgGridListaPadraoTitleClick(Column: TColumn);
begin
  inherited;

  qryGridListaPadrao.IndexFieldNames := Column.FieldName;
end;

procedure TFListaOficios.dtePeriodoFimKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
     if dtePeriodoFim.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso', MB_OK + MB_ICONWARNING);

      if dtePeriodoFim.CanFocus then
        dtePeriodoFim.SetFocus;
    end
    else if dtePeriodoIni.Date > dtePeriodoFim.Date then
    begin
      Application.MessageBox('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso', MB_OK + MB_ICONWARNING);

      dtePeriodoFim.Clear;

      if dtePeriodoFim.CanFocus then
        dtePeriodoFim.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dtePeriodoFim.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso', MB_OK + MB_ICONWARNING);

      if dtePeriodoFim.CanFocus then
        dtePeriodoFim.SetFocus;
    end
    else if dtePeriodoIni.Date > dtePeriodoFim.Date then
    begin
      Application.MessageBox('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso', MB_OK + MB_ICONWARNING);

      dtePeriodoFim.Clear;

      if dtePeriodoFim.CanFocus then
        dtePeriodoFim.SetFocus;
    end
    else
      btnFiltrar.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFListaOficios.dtePeriodoIniKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
     if dtePeriodoIni.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso', MB_OK + MB_ICONWARNING);

      if dtePeriodoIni.CanFocus then
        dtePeriodoIni.SetFocus;
    end
    else if dtePeriodoIni.Date > dtePeriodoFim.Date then
    begin
      Application.MessageBox('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso', MB_OK + MB_ICONWARNING);

      dtePeriodoIni.Clear;

      if dtePeriodoIni.CanFocus then
        dtePeriodoIni.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dtePeriodoIni.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso', MB_OK + MB_ICONWARNING);

      if dtePeriodoIni.CanFocus then
        dtePeriodoIni.SetFocus;
    end
    else if dtePeriodoIni.Date > dtePeriodoFim.Date then
    begin
      Application.MessageBox('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso', MB_OK + MB_ICONWARNING);

      dtePeriodoIni.Clear;

      if dtePeriodoIni.CanFocus then
        dtePeriodoIni.SetFocus;
    end
    else
      btnFiltrar.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFListaOficios.FormCreate(Sender: TObject);
begin
  inherited;

  vgOfc_IdAssoc := 0;
end;

procedure TFListaOficios.FormShow(Sender: TObject);
begin
  inherited;

  dtePeriodoIni.Date := StartOfTheMonth(Date);
  dtePeriodoFim.Date := EndOfTheMonth(Date);

  btnFiltrar.Click;

  if dtePeriodoIni.CanFocus then
    dtePeriodoFim.SetFocus;
end;

end.
