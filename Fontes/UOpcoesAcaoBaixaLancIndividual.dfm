inherited FOpcoesAcaoBaixaLancIndividual: TFOpcoesAcaoBaixaLancIndividual
  Caption = 'FOpcoesAcaoBaixaLancIndividual'
  ClientHeight = 125
  ClientWidth = 443
  ExplicitWidth = 443
  ExplicitHeight = 125
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlOpcoes: TPanel
    Width = 437
    Height = 119
    ExplicitWidth = 437
    ExplicitHeight = 119
    inherited pnlBotoes: TPanel
      Width = 431
      Height = 113
      ExplicitWidth = 431
      ExplicitHeight = 113
      inherited btnOpcao2: TJvTransparentButton
        Left = 288
        Top = 41
        Caption = 'Observa'#231#227'o de Falta'
        Font.Color = clMaroon
        Spacing = 4
        WordWrap = True
        OnClick = btnOpcao2Click
        Images.ActiveImage = dmGerencial.Im32
        Images.ActiveIndex = 13
        Images.DisabledImage = dmGerencial.Im32Dis
        Images.DisabledIndex = 13
        ExplicitLeft = 144
        ExplicitTop = 41
      end
      inherited btnOpcao1: TJvTransparentButton
        Top = 41
        Caption = 'Observa'#231#227'o de Sobra'
        Font.Color = clGreen
        Spacing = 4
        WordWrap = True
        OnClick = btnOpcao1Click
        Images.ActiveImage = dmGerencial.Im32
        Images.ActiveIndex = 13
        Images.DisabledImage = dmGerencial.Im32Dis
        Images.DisabledIndex = 13
        ExplicitTop = 41
      end
      object btnOpcao3: TJvTransparentButton
        Left = 216
        Top = 41
        Width = 72
        Height = 72
        Hint = 'Selecione a op'#231#227'o desejada'
        Align = alLeft
        BorderWidth = 0
        Caption = 'Nova Parcela no Flutuante'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        Spacing = 4
        TextAlign = ttaBottom
        Transparent = False
        WordWrap = True
        OnClick = btnOpcao3Click
        NumGlyphs = 2
        Images.ActiveImage = dmGerencial.Im32
        Images.ActiveIndex = 45
        Images.DisabledImage = dmGerencial.Im32Dis
        Images.DisabledIndex = 45
        ExplicitLeft = 138
        ExplicitTop = 47
      end
      object btnOpcao4: TJvTransparentButton
        Left = 144
        Top = 41
        Width = 72
        Height = 72
        Hint = 'Selecione a op'#231#227'o desejada'
        Align = alLeft
        BorderWidth = 0
        Caption = 'Lan'#231'ar Sobra de Caixa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        Spacing = 4
        TextAlign = ttaBottom
        Transparent = False
        WordWrap = True
        OnClick = btnOpcao4Click
        NumGlyphs = 2
        Images.ActiveImage = dmGerencial.Im32
        Images.ActiveIndex = 30
        Images.DisabledImage = dmGerencial.Im32Dis
        Images.DisabledIndex = 30
        ExplicitLeft = 96
        ExplicitTop = 30
      end
      object btnOpcao5: TJvTransparentButton
        Left = 360
        Top = 41
        Width = 72
        Height = 72
        Hint = 'Selecione a op'#231#227'o desejada'
        Align = alLeft
        BorderWidth = 0
        Caption = 'Lan'#231'ar Falta de Caixa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        Spacing = 4
        TextAlign = ttaBottom
        Transparent = False
        WordWrap = True
        OnClick = btnOpcao5Click
        NumGlyphs = 2
        Images.ActiveImage = dmGerencial.Im32
        Images.ActiveIndex = 6
        Images.DisabledImage = dmGerencial.Im32Dis
        Images.DisabledIndex = 6
        ExplicitLeft = 264
        ExplicitTop = 47
      end
      object btnOpcao6: TJvTransparentButton
        Left = 72
        Top = 41
        Width = 72
        Height = 72
        Hint = 'Selecione a op'#231#227'o desejada'
        Align = alLeft
        BorderWidth = 0
        Caption = 'Desmembrar Flutuante'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        Spacing = 4
        TextAlign = ttaBottom
        Transparent = False
        WordWrap = True
        OnClick = btnOpcao6Click
        NumGlyphs = 2
        Images.ActiveImage = dmGerencial.Im32
        Images.ActiveIndex = 59
        Images.DisabledImage = dmGerencial.Im32Dis
        Images.DisabledIndex = 59
        ExplicitLeft = 0
        ExplicitHeight = 451
      end
      object pnlValores: TPanel
        Left = 0
        Top = 0
        Width = 431
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        Color = clWhite
        ParentBackground = False
        TabOrder = 0
        object lblSobra: TLabel
          Left = 0
          Top = 8
          Width = 92
          Height = 16
          Caption = 'Sobra: R$0,00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblFalta: TLabel
          Left = 347
          Top = 8
          Width = 85
          Height = 16
          Alignment = taRightJustify
          Caption = 'Falta: R$0,00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
    end
  end
end
