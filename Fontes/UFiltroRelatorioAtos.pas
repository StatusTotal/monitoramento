{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroRelatorioAtos.pas
  Descricao:   Filtro Relatorio de Atos (R07 - R07DET)
  Author   :   Cristina
  Date:        28-nov-2016
  Last Update: 31-ago-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroRelatorioAtos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroRelatorioPadrao, Vcl.StdCtrls,
  JvExControls, JvButton, JvTransparentButton, Vcl.ExtCtrls, Vcl.DBCtrls,
  Vcl.Mask, JvExMask, JvToolEdit, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFFiltroRelatorioAtos = class(TFFiltroRelatorioPadrao)
    rgTipoAto: TRadioGroup;
    lcbTipoAto: TDBLookupComboBox;
    qryTipoAto: TFDQuery;
    dsTipoAto: TDataSource;
    gbPeriodo: TGroupBox;
    lblPeriodoInicio: TLabel;
    dteDataInicio: TJvDateEdit;
    lblPeriodoFim: TLabel;
    dteDataFim: TJvDateEdit;
    gbAtribuicao: TGroupBox;
    chbFirmas: TCheckBox;
    chbNotas: TCheckBox;
    chbProtesto: TCheckBox;
    chbRCPJ: TCheckBox;
    chbRCPN: TCheckBox;
    chbRGI: TCheckBox;
    chbRIT: TCheckBox;
    chbRTD: TCheckBox;
    qrySistema: TFDQuery;
    dsSistema: TDataSource;
    procedure btnLimparClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure rgTipoAtoClick(Sender: TObject);
    procedure rgTipoAtoExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dteDataInicioKeyPress(Sender: TObject; var Key: Char);
    procedure dteDataFimKeyPress(Sender: TObject; var Key: Char);
  protected
    procedure GravarLog; override;
    procedure GerarRelatorio(ImpDet: Boolean); override;
    function VerificarFiltro: Boolean; override;
  private
    { Private declarations }

    procedure DefinirAtribuicoes;
    procedure AbrirListaAtos;
    procedure AbrirListaAtosDetalhe;
  public
    { Public declarations }
  end;

var
  FFiltroRelatorioAtos: TFFiltroRelatorioAtos;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMRelatorios;

{ TFFiltroRelatorioAtos }

procedure TFFiltroRelatorioAtos.AbrirListaAtos;
var
  sSistemas: String;
begin
  sSistemas := '';

  dmRelatorios.qryRel07_Atos.Close;
  dmRelatorios.qryRel07_Atos.SQL.Clear;

  dmRelatorios.qryRel07_Atos.SQL.Text := 'SELECT LANCS.ID_SISTEMA, ' +
                                         '       LANCS.NOME_SISTEMA, ' +
                                         '       LANCS.ID_ITEM_FK, ' +
                                         '       LANCS.DESCR_ITEM, ' +
                                         '       SUM(CASE WHEN (LANCS.VR_DH > 0) ' +
                                         '                THEN CAST((((((CAST(LANCS.VR_UNITARIO AS NUMERIC(10,2)) * CAST(LANCS.QUANTIDADE AS NUMERIC(10,2))) * 100) / CAST(LANCS.VR_TOTAL_PREV AS NUMERIC(10,2))) * CAST(LANCS.VR_DH AS NUMERIC(10,2))) / 100) AS NUMERIC(10,2)) ' +
                                         '                ELSE 0 ' +
                                         '           END) AS VR_TOTAL_DH, ' +
                                         '       SUM(CASE WHEN (LANCS.VR_DP > 0) ' +
                                         '                THEN CAST((((((CAST(LANCS.VR_UNITARIO AS NUMERIC(10,2)) * CAST(LANCS.QUANTIDADE AS NUMERIC(10,2))) * 100) / CAST(LANCS.VR_TOTAL_PREV AS NUMERIC(10,2))) * CAST(LANCS.VR_DP AS NUMERIC(10,2))) / 100) AS NUMERIC(10,2)) ' +
                                         '                ELSE 0 ' +
                                         '           END) AS VR_TOTAL_DP, ' +
                                         '       SUM(CASE WHEN (LANCS.VR_CT > 0) ' +
                                         '                THEN CAST((((((CAST(LANCS.VR_UNITARIO AS NUMERIC(10,2)) * CAST(LANCS.QUANTIDADE AS NUMERIC(10,2))) * 100) / CAST(LANCS.VR_TOTAL_PREV AS NUMERIC(10,2))) * CAST(LANCS.VR_CT AS NUMERIC(10,2))) / 100) AS NUMERIC(10,2)) ' +
                                         '                ELSE 0 ' +
                                         '           END) AS VR_TOTAL_CT, ' +
                                         '       SUM(CASE WHEN (LANCS.VR_CH > 0) ' +
                                         '                THEN CAST((((((CAST(LANCS.VR_UNITARIO AS NUMERIC(10,2)) * CAST(LANCS.QUANTIDADE AS NUMERIC(10,2))) * 100) / CAST(LANCS.VR_TOTAL_PREV AS NUMERIC(10,2))) * CAST(LANCS.VR_CH AS NUMERIC(10,2))) / 100) AS NUMERIC(10,2)) ' +
                                         '                ELSE 0 ' +
                                         '           END) AS VR_TOTAL_CH, ' +
                                         '       SUM(CASE WHEN (LANCS.VR_FT > 0) ' +
                                         '                THEN CAST((((((CAST(LANCS.VR_UNITARIO AS NUMERIC(10,2)) * CAST(LANCS.QUANTIDADE AS NUMERIC(10,2))) * 100) / CAST(LANCS.VR_TOTAL_PREV AS NUMERIC(10,2))) * CAST(LANCS.VR_FT AS NUMERIC(10,2))) / 100) AS NUMERIC(10,2)) ' +
                                         '                ELSE 0 ' +
                                         '           END) AS VR_TOTAL_FT, ' +
                                         '       SUM(CASE WHEN (LANCS.VR_PM > 0) ' +
                                         '                THEN CAST((((((CAST(LANCS.VR_UNITARIO AS NUMERIC(10,2)) * CAST(LANCS.QUANTIDADE AS NUMERIC(10,2))) * 100) / CAST(LANCS.VR_TOTAL_PREV AS NUMERIC(10,2))) * CAST(LANCS.VR_PM AS NUMERIC(10,2))) / 100) AS NUMERIC(10,2)) ' +
                                         '                ELSE 0 ' +
                                         '           END) AS VR_TOTAL_PM, ' +
                                         '       SUM(LANCS.QUANTIDADE) AS QTD_TOTAL, ' +
                                         '       SUM(LANCS.QTD_TOTAL_CC) AS QTD_TOTAL_CC, ' +
                                         '       SUM(LANCS.QTD_TOTAL_JG) AS QTD_TOTAL_JG, ' +
                                         '       SUM(LANCS.QTD_TOTAL_SC) AS QTD_TOTAL_SC, ' +
                                         '       SUM(LANCS.QTD_TOTAL_NH) AS QTD_TOTAL_NH, ' +
                                         '       SUM(LANCS.QTD_TOTAL_FL) AS QTD_TOTAL_FL, ' +
                                         '       SUM(LANCS.QTD_TOTAL_IS) AS QTD_TOTAL_IS, ' +
                                         '       SUM(LANCS.QTD_TOTAL_PA) AS QTD_TOTAL_PA, ' +
                                         '       SUM(LANCS.QTD_TOTAL_PP) AS QTD_TOTAL_PP, ' +
                                         '       SUM(CASE WHEN (LANCS.VR_ATO > 0) ' +
                                         '                THEN CAST((((((CAST(LANCS.VR_UNITARIO AS NUMERIC(10,2)) * CAST(LANCS.QUANTIDADE AS NUMERIC(10,2))) * 100) / CAST(LANCS.VR_TOTAL_PREV AS NUMERIC(10,2))) * CAST(LANCS.VR_ATO AS NUMERIC(10,2))) / 100) AS NUMERIC(10,2)) ' +
                                         '                ELSE 0 ' +
                                         '           END) AS VR_TOTAL_ATO ' +
                                         '  FROM (SELECT S.ID_SISTEMA, ' +
                                         '               S.NOME_SISTEMA, ' +
                                         '               LD.ID_ITEM_FK, ' +
                                         '               I.DESCR_ITEM, ' +
                                         '               L.COD_LANCAMENTO, ' +
                                         '               L.ANO_LANCAMENTO, ' +
                                         '               L.VR_TOTAL_PREV, ' +
                                         '               L.QTD_PARCELAS, ' +
                                         '               (SELECT SUM(LP.VR_PARCELA_PREV) AS TOTAL_PARC ' +
                                         '                  FROM LANCAMENTO_PARC LP ' +
                                         '                 WHERE LP.COD_LANCAMENTO_FK = LD.COD_LANCAMENTO_FK ' +
                                         '                   AND LP.ANO_LANCAMENTO_FK = LD.ANO_LANCAMENTO_FK ' +
                                         '                   AND LP.FLG_STATUS <> ' + QuotedStr('C') +
                                         '                   AND LP.ID_FORMAPAGAMENTO_FK = 6) AS VR_DH, ' +
                                         '               (SELECT SUM(LP.VR_PARCELA_PREV) AS TOTAL_PARC ' +
                                         '                  FROM LANCAMENTO_PARC LP ' +
                                         '                 WHERE LP.COD_LANCAMENTO_FK = LD.COD_LANCAMENTO_FK ' +
                                         '                   AND LP.ANO_LANCAMENTO_FK = LD.ANO_LANCAMENTO_FK ' +
                                         '                   AND LP.FLG_STATUS <> ' + QuotedStr('C') +
                                         '                   AND LP.ID_FORMAPAGAMENTO_FK = 5) AS VR_DP, ' +
                                         '               (SELECT SUM(LP.VR_PARCELA_PREV) AS TOTAL_PARC ' +
                                         '                  FROM LANCAMENTO_PARC LP ' +
                                         '                 WHERE LP.COD_LANCAMENTO_FK = LD.COD_LANCAMENTO_FK ' +
                                         '                   AND LP.ANO_LANCAMENTO_FK = LD.ANO_LANCAMENTO_FK ' +
                                         '                   AND LP.FLG_STATUS <> ' + QuotedStr('C') +
                                         '                   AND LP.ID_FORMAPAGAMENTO_FK = 2) AS VR_CT, ' +
                                         '               (SELECT SUM(LP.VR_PARCELA_PREV) AS TOTAL_PARC ' +
                                         '                  FROM LANCAMENTO_PARC LP ' +
                                         '                 WHERE LP.COD_LANCAMENTO_FK = LD.COD_LANCAMENTO_FK ' +
                                         '                   AND LP.ANO_LANCAMENTO_FK = LD.ANO_LANCAMENTO_FK ' +
                                         '                   AND LP.FLG_STATUS <> ' + QuotedStr('C') +
                                         '                   AND LP.ID_FORMAPAGAMENTO_FK = 3) AS VR_CH, ' +
                                         '               (SELECT SUM(LP.VR_PARCELA_PREV) AS TOTAL_PARC ' +
                                         '                  FROM LANCAMENTO_PARC LP ' +
                                         '                 WHERE LP.COD_LANCAMENTO_FK = LD.COD_LANCAMENTO_FK ' +
                                         '                   AND LP.ANO_LANCAMENTO_FK = LD.ANO_LANCAMENTO_FK ' +
                                         '                   AND LP.FLG_STATUS <> ' + QuotedStr('C') +
                                         '                   AND LP.ID_FORMAPAGAMENTO_FK = 4) AS VR_FT, ' +
                                         '               (SELECT SUM(LP.VR_PARCELA_PREV) AS TOTAL_PARC ' +
                                         '                  FROM LANCAMENTO_PARC LP ' +
                                         '                 WHERE LP.COD_LANCAMENTO_FK = LD.COD_LANCAMENTO_FK ' +
                                         '                   AND LP.ANO_LANCAMENTO_FK = LD.ANO_LANCAMENTO_FK ' +
                                         '                   AND LP.FLG_STATUS <> ' + QuotedStr('C') +
                                         '                   AND LP.ID_FORMAPAGAMENTO_FK = 7) AS VR_PM, ' +
                                         '               LD.VR_UNITARIO, ' +
                                         '               LD.QUANTIDADE, ' +
                                         '               (CASE WHEN (LD.TIPO_COBRANCA = ' + QuotedStr('CC') +') ' +
                                         '                     THEN LD.QUANTIDADE ' +
                                         '                     ELSE 0 ' +
                                         '                END) AS QTD_TOTAL_CC, ' +
                                         '               (CASE WHEN (LD.TIPO_COBRANCA = ' + QuotedStr('JG') +') ' +
                                         '                     THEN LD.QUANTIDADE ' +
                                         '                     ELSE 0 ' +
                                         '                END) AS QTD_TOTAL_JG, ' +
                                         '               (CASE WHEN (LD.TIPO_COBRANCA = ' + QuotedStr('SC') +') ' +
                                         '                     THEN LD.QUANTIDADE ' +
                                         '                     ELSE 0 ' +
                                         '                END) AS QTD_TOTAL_SC, ' +
                                         '               (CASE WHEN (LD.TIPO_COBRANCA = ' + QuotedStr('NH') +') ' +
                                         '                     THEN LD.QUANTIDADE ' +
                                         '                     ELSE 0 ' +
                                         '                END) AS QTD_TOTAL_NH, ' +
                                         '               (CASE WHEN (LD.TIPO_COBRANCA = ' + QuotedStr('FL') +') ' +
                                         '                     THEN LD.QUANTIDADE ' +
                                         '                     ELSE 0 ' +
                                         '                END) AS QTD_TOTAL_FL, ' +
                                         '               (CASE WHEN (LD.TIPO_COBRANCA = ' + QuotedStr('IS') +') ' +
                                         '                     THEN LD.QUANTIDADE ' +
                                         '                     ELSE 0 ' +
                                         '                END) AS QTD_TOTAL_IS, ' +
                                         '               (CASE WHEN (LD.TIPO_COBRANCA = ' + QuotedStr('PA') +') ' +
                                         '                     THEN LD.QUANTIDADE ' +
                                         '                     ELSE 0 ' +
                                         '                END) AS QTD_TOTAL_PA, ' +
                                         '               (CASE WHEN (LD.TIPO_COBRANCA = ' + QuotedStr('PP') +') ' +
                                         '                     THEN LD.QUANTIDADE ' +
                                         '                     ELSE 0 ' +
                                         '                END) AS QTD_TOTAL_PP, ' +
                                         '                (SELECT SUM(LP.VR_PARCELA_PREV) AS TOTAL_PARC ' +
                                         '                  FROM LANCAMENTO_PARC LP ' +
                                         '                 WHERE LP.COD_LANCAMENTO_FK = LD.COD_LANCAMENTO_FK ' +
                                         '                   AND LP.ANO_LANCAMENTO_FK = LD.ANO_LANCAMENTO_FK ' +
                                         '                   AND LP.FLG_STATUS <> ' + QuotedStr('C') +') AS VR_ATO ' +
                                         '          FROM LANCAMENTO_DET LD ' +
                                         '         INNER JOIN ITEM I ' +
                                         '            ON LD.ID_ITEM_FK = I.ID_ITEM ' +
                                         '         INNER JOIN LANCAMENTO L ' +
                                         '            ON LD.COD_LANCAMENTO_FK = L.COD_LANCAMENTO ' +
                                         '           AND LD.ANO_LANCAMENTO_FK = L.ANO_LANCAMENTO ' +
                                         '         INNER JOIN SISTEMA S ' +
                                         '            ON L.ID_SISTEMA_FK = S.ID_SISTEMA ' +
                                         '         WHERE LD.FLG_CANCELADO = ' + QuotedStr('N');

  //Atribuicao
  if chbFirmas.Checked then  //FIRMAS
    sSistemas := '1';

  if chbNotas.Checked then  //NOTAS
  begin
    if Trim(sSistemas) = '' then
      sSistemas := '2'
    else
      sSistemas := sSistemas + ', 2';
  end;

  if chbProtesto.Checked then  //PROTESTO
  begin
    if Trim(sSistemas) = '' then
      sSistemas := '3'
    else
      sSistemas := sSistemas + ', 3';
  end;

  if chbRCPJ.Checked then  //RCPJ
  begin
    if Trim(sSistemas) = '' then
      sSistemas := '4'
    else
      sSistemas := sSistemas + ', 4';
  end;

  if chbRCPN.Checked then  //RCPN
  begin
    if Trim(sSistemas) = '' then
      sSistemas := '5'
    else
      sSistemas := sSistemas + ', 5';
  end;

  if chbRGI.Checked then  //RGI
  begin
    if Trim(sSistemas) = '' then
      sSistemas := '6'
    else
      sSistemas := sSistemas + ', 6';
  end;

  if chbRTD.Checked then  //RTD
  begin
    if Trim(sSistemas) = '' then
      sSistemas := '7'
    else
      sSistemas := sSistemas + ', 7';
  end;

  if chbRIT.Checked then  //RIT
  begin
    if Trim(sSistemas) = '' then
      sSistemas := '8'
    else
      sSistemas := sSistemas + ', 8';
  end;

  if Trim(sSistemas) <> '' then
    dmRelatorios.qryRel07_Atos.SQL.Add(' AND L.ID_SISTEMA_FK IN (' + sSistemas + ') ');

  dmRelatorios.qryRel07_Atos.SQL.Add('           AND LD.ID_ITEM_FK = (CASE WHEN (:ID_ITEM1 = 0) ' +
                                     '                                     THEN LD.ID_ITEM_FK ' +
                                     '                                     ELSE :ID_ITEM2 ' +
                                     '                                END) ' +
                                     '           AND EXISTS (SELECT LP.ID_LANCAMENTO_PARC ' +
                                     '                         FROM LANCAMENTO_PARC LP ' +
                                     '                        WHERE LP.COD_LANCAMENTO_FK = LD.COD_LANCAMENTO_FK ' +
                                     '                          AND LP.ANO_LANCAMENTO_FK = LD.ANO_LANCAMENTO_FK ' +
                                     '                          AND LP.FLG_STATUS <> ' + QuotedStr('C') +
                                     '                          AND LP.DATA_LANCAMENTO_PARC BETWEEN :DATA_INI AND :DATA_FIM) ' +
                                     '               AND L.TIPO_LANCAMENTO = ' + QuotedStr('R') +
                                     '               AND L.NUM_RECIBO IS NOT NULL ' +
                                     '               AND L.FLG_FORAFECHCAIXA = ' + QuotedStr('N') +
                                     '               AND L.FLG_FLUTUANTE = ' + QuotedStr('N') + ') AS LANCS ' +
                                     'GROUP BY LANCS.ID_SISTEMA, ' +
                                     '         LANCS.NOME_SISTEMA, ' +
                                     '         LANCS.ID_ITEM_FK, ' +
                                     '         LANCS.DESCR_ITEM ' +
                                     'ORDER BY LANCS.NOME_SISTEMA, ' +
                                     '         LANCS.DESCR_ITEM');

  //Periodo
  dmRelatorios.qryRel07_Atos.Params.ParamByName('DATA_INI').Value := dteDataInicio.Date;
  dmRelatorios.qryRel07_Atos.Params.ParamByName('DATA_FIM').Value := dteDataFim.Date;

  //Ato
  if rgTipoAto.ItemIndex = 0 then
  begin
    dmRelatorios.qryRel07_Atos.Params.ParamByName('ID_ITEM1').Value := 0;
    dmRelatorios.qryRel07_Atos.Params.ParamByName('ID_ITEM2').Value := 0;
  end
  else
  begin
    dmRelatorios.qryRel07_Atos.Params.ParamByName('ID_ITEM1').Value := lcbTipoAto.KeyValue;
    dmRelatorios.qryRel07_Atos.Params.ParamByName('ID_ITEM2').Value := lcbTipoAto.KeyValue;
  end;

  dmRelatorios.qryRel07_Atos.Open;
end;

procedure TFFiltroRelatorioAtos.AbrirListaAtosDetalhe;
var
  sSistemas: String;
begin
  sSistemas := '';

  dmRelatorios.qryRel07_AtosDet.Close;
  dmRelatorios.qryRel07_AtosDet.SQL.Clear;

  dmRelatorios.qryRel07_AtosDet.SQL.Text := 'SELECT L.ID_SISTEMA_FK AS ID_SISTEMA, ' +
                                            '       S.NOME_SISTEMA, ' +
                                            '       L.COD_LANCAMENTO, ' +
                                            '       L.ANO_LANCAMENTO, ' +
                                            '       L.DATA_LANCAMENTO, ' +
                                            '       L.NUM_RECIBO, ' +
                                            '       LD.ID_ITEM_FK, ' +
                                            '       UPPER(I.DESCR_ITEM) AS DESCR_ITEM, ' +
                                            '       SUM(LD.QUANTIDADE) AS QUANTIDADE, ' +
                                            '       SUM(LD.EMOLUMENTOS) AS EMOLUMENTOS, ' +
                                            '       SUM(LD.FETJ) AS FETJ, ' +
                                            '       SUM(LD.FUNDPERJ) AS FUNDPERJ, ' +
                                            '       SUM(LD.FUNPERJ) AS FUNPERJ, ' +
                                            '       SUM(LD.FUNARPEN) AS FUNARPEN, ' +
                                            '       SUM(LD.PMCMV) AS PMCMV, ' +
                                            '       SUM(LD.ISS) AS ISS, ' +
                                            '       SUM(LD.MUTUA) AS MUTUA, ' +
                                            '       SUM(LD.ACOTERJ) AS ACOTERJ, ' +
                                            '       SUM(LD.VR_TOTAL) AS VR_TOTAL, ' +
                                            '       LD.TIPO_COBRANCA, ' +
                                            '       (CASE LD.TIPO_COBRANCA ' +
                                            '          WHEN ' + QuotedStr('CC') + ' THEN 1 ' +
                                            '          WHEN ' + QuotedStr('JG') + ' THEN 2 ' +
                                            '          WHEN ' + QuotedStr('SC') + ' THEN 3 ' +
                                            '          WHEN ' + QuotedStr('NH') + ' THEN 4 ' +
                                            '          WHEN ' + QuotedStr('FL') + ' THEN 5 ' +
                                            '          WHEN ' + QuotedStr('IS') + ' THEN 6 ' +
                                            '          WHEN ' + QuotedStr('PA') + ' THEN 7 ' +
                                            '          WHEN ' + QuotedStr('PP') + ' THEN 8 ' +
                                            '          ELSE 9 ' +
                                            '        END) AS TP_COBRANCA ' +
                                            '  FROM LANCAMENTO_DET LD ' +
                                            '  LEFT JOIN ITEM I ' +
                                            '    ON LD.ID_ITEM_FK = I.ID_ITEM ' +
                                            ' INNER JOIN LANCAMENTO L ' +
                                            '    ON LD.COD_LANCAMENTO_FK = L.COD_LANCAMENTO ' +
                                            '   AND LD.ANO_LANCAMENTO_FK = L.ANO_LANCAMENTO ' +
                                            ' INNER JOIN SISTEMA S ' +
                                            '    ON L.ID_SISTEMA_FK = S.ID_SISTEMA ' +
                                            ' WHERE L.TIPO_LANCAMENTO = ' + QuotedStr('R') +
                                            '   AND L.FLG_FLUTUANTE = ' + QuotedStr('N') +
                                            '   AND L.FLG_CANCELADO = ' + QuotedStr('N') +
                                            '   AND L.FLG_FORAFECHCAIXA = ' + QuotedStr('N') +
                                            '   AND LD.FLG_CANCELADO = ' + QuotedStr('N') +
                                            '   AND L.NUM_RECIBO IS NOT NULL ' +
                                            '   AND EXISTS (SELECT LP.ID_LANCAMENTO_PARC ' +
                                            '                 FROM LANCAMENTO_PARC LP ' +
                                            '                WHERE LP.COD_LANCAMENTO_FK = L.COD_LANCAMENTO ' +
                                            '                  AND LP.ANO_LANCAMENTO_FK = L.ANO_LANCAMENTO ' +
                                            '                  AND LP.DATA_LANCAMENTO_PARC BETWEEN :DATA_INI AND :DATA_FIM) ' +
                                            '   AND LD.ID_ITEM_FK = (CASE WHEN (:ID_ITEM1 = 0) ' +
                                            '                             THEN LD.ID_ITEM_FK ' +
                                            '                             ELSE :ID_ITEM2 ' +
                                            '                        END) ';

  //Atribuicao
  if chbFirmas.Checked then  //FIRMAS
    sSistemas := '1';

  if chbNotas.Checked then  //NOTAS
  begin
    if Trim(sSistemas) = '' then
      sSistemas := '2'
    else
      sSistemas := sSistemas + ', 2';
  end;

  if chbProtesto.Checked then  //PROTESTO
  begin
    if Trim(sSistemas) = '' then
      sSistemas := '3'
    else
      sSistemas := sSistemas + ', 3';
  end;

  if chbRCPJ.Checked then  //RCPJ
  begin
    if Trim(sSistemas) = '' then
      sSistemas := '4'
    else
      sSistemas := sSistemas + ', 4';
  end;

  if chbRCPN.Checked then  //RCPN
  begin
    if Trim(sSistemas) = '' then
      sSistemas := '5'
    else
      sSistemas := sSistemas + ', 5';
  end;

  if chbRGI.Checked then  //RGI
  begin
    if Trim(sSistemas) = '' then
      sSistemas := '6'
    else
      sSistemas := sSistemas + ', 6';
  end;

  if chbRTD.Checked then  //RTD
  begin
    if Trim(sSistemas) = '' then
      sSistemas := '7'
    else
      sSistemas := sSistemas + ', 7';
  end;

  if chbRIT.Checked then  //RIT
  begin
    if Trim(sSistemas) = '' then
      sSistemas := '8'
    else
      sSistemas := sSistemas + ', 8';
  end;

  if Trim(sSistemas) <> '' then
    dmRelatorios.qryRel07_AtosDet.SQL.Add(' AND L.ID_SISTEMA_FK IN (' + sSistemas + ') ');

  dmRelatorios.qryRel07_AtosDet.SQL.Add('GROUP BY L.ID_SISTEMA_FK, ' +
                                        '         S.NOME_SISTEMA, ' +
                                        '         L.COD_LANCAMENTO, ' +
                                        '         L.ANO_LANCAMENTO, ' +
                                        '         L.DATA_LANCAMENTO, ' +
                                        '         L.NUM_RECIBO, ' +
                                        '         LD.ID_ITEM_FK, ' +
                                        '         I.DESCR_ITEM, ' +
                                        '         LD.TIPO_COBRANCA ' +
                                        'ORDER BY S.NOME_SISTEMA, ' +
                                        '         I.DESCR_ITEM, ' +
                                        '         L.NUM_RECIBO, ' +
                                        '         TP_COBRANCA, ' +
                                        '         L.DATA_LANCAMENTO');

  //Periodo
  dmRelatorios.qryRel07_AtosDet.Params.ParamByName('DATA_INI').Value := dteDataInicio.Date;
  dmRelatorios.qryRel07_AtosDet.Params.ParamByName('DATA_FIM').Value := dteDataFim.Date;

  //Ato
  if rgTipoAto.ItemIndex = 0 then
  begin
    dmRelatorios.qryRel07_AtosDet.Params.ParamByName('ID_ITEM1').Value := 0;
    dmRelatorios.qryRel07_AtosDet.Params.ParamByName('ID_ITEM2').Value := 0;
  end
  else
  begin
    dmRelatorios.qryRel07_AtosDet.Params.ParamByName('ID_ITEM1').Value := lcbTipoAto.KeyValue;
    dmRelatorios.qryRel07_AtosDet.Params.ParamByName('ID_ITEM2').Value := lcbTipoAto.KeyValue;
  end;

  dmRelatorios.qryRel07_AtosDet.Open;
end;

procedure TFFiltroRelatorioAtos.btnLimparClick(Sender: TObject);
begin
  inherited;

  DefinirAtribuicoes;

  dteDataInicio.Date  := Date;
  dteDataFim.Date     := Date;
  rgTipoAto.ItemIndex := 0;
  lcbTipoAto.Enabled  := False;

  chbTipoSintetico.Checked := True;
  chbTipoAnalitico.Checked := False;

  chbExportarPDF.Checked := False;

  chbExportarExcel.Enabled := vgPrm_ExpRelAtos;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioAtos.btnVisualizarClick(Sender: TObject);
begin
  inherited;

  if not VerificarFiltro then
    Exit;

  AbrirListaAtos;

  if dmRelatorios.qryRel07_Atos.RecordCount = 0 then
  begin
    Application.MessageBox('N�o h� Atos registrados no per�odo informado.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
  end
  else
  begin
    dDataPerInicio := dteDataInicio.Date;
    dDataPerFim    := dteDataFim.Date;

    if chbTipoSintetico.Checked then
      GerarRelatorio(False);

    if chbTipoAnalitico.Checked then
    begin
      AbrirListaAtosDetalhe;
      GerarRelatorio(True);
    end;
  end;
end;

procedure TFFiltroRelatorioAtos.DefinirAtribuicoes;
begin
  qrySistema.First;

  while not qrySistema.Eof do
  begin
    case qrySistema.FieldByName('ID_SISTEMA').AsInteger of
      1: //FIRMAS
      begin
        chbFirmas.Enabled := (Trim(qrySistema.FieldByName('FLG_TRABALHA').AsString) = 'S');
        chbFirmas.Checked := (Trim(qrySistema.FieldByName('FLG_TRABALHA').AsString) = 'S');
      end;
      2: //NOTAS
      begin
        chbNotas.Enabled := (Trim(qrySistema.FieldByName('FLG_TRABALHA').AsString) = 'S');
        chbNotas.Checked := (Trim(qrySistema.FieldByName('FLG_TRABALHA').AsString) = 'S');
      end;
      3: //PROTESTO
      begin
        chbProtesto.Enabled := (Trim(qrySistema.FieldByName('FLG_TRABALHA').AsString) = 'S');
        chbProtesto.Checked := (Trim(qrySistema.FieldByName('FLG_TRABALHA').AsString) = 'S');
      end;
      4: //RCPJ
      begin
        chbRCPJ.Enabled := (Trim(qrySistema.FieldByName('FLG_TRABALHA').AsString) = 'S');
        chbRCPJ.Checked := (Trim(qrySistema.FieldByName('FLG_TRABALHA').AsString) = 'S');
      end;
      5: //RCPN
      begin
        chbRCPN.Enabled := (Trim(qrySistema.FieldByName('FLG_TRABALHA').AsString) = 'S');
        chbRCPN.Checked := (Trim(qrySistema.FieldByName('FLG_TRABALHA').AsString) = 'S');
      end;
      6: //RGI
      begin
        chbRGI.Enabled := (Trim(qrySistema.FieldByName('FLG_TRABALHA').AsString) = 'S');
        chbRGI.Checked := (Trim(qrySistema.FieldByName('FLG_TRABALHA').AsString) = 'S');
      end;
      7: //RTD
      begin
        chbRTD.Enabled := (Trim(qrySistema.FieldByName('FLG_TRABALHA').AsString) = 'S');
        chbRTD.Checked := (Trim(qrySistema.FieldByName('FLG_TRABALHA').AsString) = 'S');
      end;
      8:  //RIT
      begin
        chbRIT.Enabled := (Trim(qrySistema.FieldByName('FLG_TRABALHA').AsString) = 'S');
        chbRIT.Checked := (Trim(qrySistema.FieldByName('FLG_TRABALHA').AsString) = 'S');
      end;
    end;

    qrySistema.Next;
  end;
end;

procedure TFFiltroRelatorioAtos.dteDataFimKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if rgTipoAto.ItemIndex = 1 then  //Especifico
    begin
      qryTipoAto.Close;
      qryTipoAto.Params.ParamByName('DATA_INI').Value := dteDataInicio.Date;
      qryTipoAto.Params.ParamByName('DATA_FIM').Value := dteDataFim.Date;
      qryTipoAto.Open;
    end;

    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroRelatorioAtos.dteDataInicioKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if rgTipoAto.ItemIndex = 1 then  //Especifico
    begin
      qryTipoAto.Close;
      qryTipoAto.Params.ParamByName('DATA_INI').Value := dteDataInicio.Date;
      qryTipoAto.Params.ParamByName('DATA_FIM').Value := dteDataFim.Date;
      qryTipoAto.Open;
    end;

    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroRelatorioAtos.FormCreate(Sender: TObject);
begin
  inherited;

  dDataPerInicio := 0;
  dDataPerFim    := 0;
end;

procedure TFFiltroRelatorioAtos.FormShow(Sender: TObject);
begin
  inherited;

  qrySistema.Close;
  qrySistema.Open;

  DefinirAtribuicoes;

  dteDataInicio.Date  := Date;
  dteDataFim.Date     := Date;
  rgTipoAto.ItemIndex := 0;
  lcbTipoAto.Enabled  := False;

  chbTipoSintetico.Checked := True;
  chbTipoAnalitico.Checked := False;

  chbExportarPDF.Checked := False;

  chbExportarExcel.Enabled := vgPrm_ExpRelAtos;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;

  chbExibirGrafico.Visible := False;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioAtos.GerarRelatorio(ImpDet: Boolean);
begin
  inherited;

  try
    sTituloRelatorio    := 'ATOS';
    sSubtituloRelatorio := 'Per�odo de ' + DateToStr(dteDataInicio.Date) + ' a ' + DateToStr(dteDataFim.Date);

    if ImpDet then
    begin
      lExibeDetalhe := True;
      dmRelatorios.DefinirRelatorio(R07DET);
    end
    else
    begin
      lExibeDetalhe := False;
      dmRelatorios.DefinirRelatorio(R07);
    end;

    dmRelatorios.ImprimirRelatorio(chbExportarPDF.Checked, chbExportarExcel.Checked);

    GravarLog;
  except
    Application.MessageBox('Erro ao gerar relat�rio de Atos.',
                           'Erro',
                           MB_OK + MB_ICONERROR);
  end;
end;

procedure TFFiltroRelatorioAtos.GravarLog;
begin
  inherited;

  BS.GravarUsuarioLog(50,
                      '',
                      'Impress�o de Relatorio de Atos (' + DateToStr(dDataRelatorio) +
                                                                      ' ' +
                                                                      TimeToStr(tHoraRelatorio) + ')',
                      0,
                      '');
end;

procedure TFFiltroRelatorioAtos.rgTipoAtoClick(Sender: TObject);
begin
  inherited;

  if rgTipoAto.ItemIndex = 0 then  //Todos
  begin
    lcbTipoAto.KeyValue := Null;
    lcbTipoAto.Enabled := False;
  end
  else  //Especifico
  begin
    qryTipoAto.Close;
    qryTipoAto.Params.ParamByName('DATA_INI').Value := dteDataInicio.Date;
    qryTipoAto.Params.ParamByName('DATA_FIM').Value := dteDataFim.Date;
    qryTipoAto.Open;

    lcbTipoAto.Enabled := True;
  end;
end;

procedure TFFiltroRelatorioAtos.rgTipoAtoExit(Sender: TObject);
begin
  inherited;

  if rgTipoAto.ItemIndex = 0 then  //Todos
  begin
    lcbTipoAto.KeyValue := Null;
    lcbTipoAto.Enabled := False;
  end
  else  //Especifico
  begin
    qryTipoAto.Close;
    qryTipoAto.Params.ParamByName('DATA_INI').Value := dteDataInicio.Date;
    qryTipoAto.Params.ParamByName('DATA_FIM').Value := dteDataFim.Date;
    qryTipoAto.Open;

    lcbTipoAto.Enabled := True;
  end;
end;

function TFFiltroRelatorioAtos.VerificarFiltro: Boolean;
var
  Msg: String;
begin
  Result := True;

  if dteDataInicio.Date = 0 then
  begin
    Msg := '- Informe a DATA DE IN�CIO do Per�odo.';
    Result := False;
  end;

  if dteDataFim.Date = 0 then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe a DATA DE FIM do Per�odo.'
    else
      Msg := #13#10 + '- Informe a DATA DE FIM do Per�odo.';

    Result := False;
  end;

  if dteDataFim.Date < dteDataInicio.Date then
  begin
    if Trim(Msg) = '' then
      Msg := '- A DATA DE FIM do Per�odo n�o pode inferior � DATA DE IN�CIO do mesmo.'
    else
      Msg := #13#10 + '- A DATA DE FIM do Per�odo n�o pode ser inferior � DATA DE IN�CIO do mesmo.';

    Result := False;
  end;

  if (not chbTipoAnalitico.Checked) and
    (not chbTipoSintetico.Checked) then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe o TIPO do Extrato de Contas.'
    else
      Msg := #13#10 + '- Informe o TIPO do Relat�rio de Fechamento de Caixa.';

    Result := False;
  end;

  if rgTipoAto.ItemIndex > 0 then
  begin
    if Trim(lcbTipoAto.Text) = '' then
    begin
      if Trim(Msg) = '' then
        Msg := '- Informe o tipo de Ato.'
      else
        Msg := #13#10 + '- Informe o tipo de Ato.';

      Result := False;
    end;
  end;

  if not Result then
    Application.MessageBox(PChar(':: ATEN��O ::' + #13#10 + #13#10 + Msg),
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
end;

end.
