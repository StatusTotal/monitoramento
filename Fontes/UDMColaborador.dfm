object dmColaborador: TdmColaborador
  OldCreateOrder = False
  Height = 518
  Width = 900
  object qryCargo: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT *'
      '  FROM CARGO'
      'ORDER BY DESCR_CARGO')
    Left = 486
    Top = 30
  end
  object dsCargo: TDataSource
    DataSet = qryCargo
    Left = 486
    Top = 78
  end
  object qryFuncionario_F: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT *'
      '  FROM FUNCIONARIO'
      ' WHERE ID_FUNCIONARIO = :ID_FUNCIONARIO')
    Left = 34
    Top = 15
    ParamData = <
      item
        Position = 1
        Name = 'ID_FUNCIONARIO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspFuncionario_F: TDataSetProvider
    DataSet = qryFuncionario_F
    Left = 34
    Top = 63
  end
  object cdsFuncionario_F: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_FUNCIONARIO'
        ParamType = ptInput
      end>
    ProviderName = 'dspFuncionario_F'
    Left = 34
    Top = 111
    object cdsFuncionario_FID_FUNCIONARIO: TIntegerField
      FieldName = 'ID_FUNCIONARIO'
      Required = True
    end
    object cdsFuncionario_FID_PESSOA: TIntegerField
      FieldName = 'ID_PESSOA'
    end
    object cdsFuncionario_FNOME_FUNCIONARIO: TStringField
      FieldName = 'NOME_FUNCIONARIO'
      Size = 250
    end
    object cdsFuncionario_FDATA_NASCIMENTO: TDateField
      FieldName = 'DATA_NASCIMENTO'
    end
    object cdsFuncionario_FCPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
    object cdsFuncionario_FNUM_IDENTIDADE: TStringField
      FieldName = 'NUM_IDENTIDADE'
      Size = 25
    end
    object cdsFuncionario_FORGAO_EMISSOR: TStringField
      FieldName = 'ORGAO_EMISSOR'
      Size = 70
    end
    object cdsFuncionario_FDATA_EMISSAO_IDENTIDADE: TDateField
      FieldName = 'DATA_EMISSAO_IDENTIDADE'
    end
    object cdsFuncionario_FNUM_CTPS: TStringField
      FieldName = 'NUM_CTPS'
      Size = 7
    end
    object cdsFuncionario_FSERIE_CTPS: TStringField
      FieldName = 'SERIE_CTPS'
      Size = 5
    end
    object cdsFuncionario_FDATA_CTPS: TDateField
      FieldName = 'DATA_CTPS'
    end
    object cdsFuncionario_FNUM_PIS: TStringField
      FieldName = 'NUM_PIS'
      Size = 14
    end
    object cdsFuncionario_FSIGLA_ESTADO_CTPS: TStringField
      FieldName = 'SIGLA_ESTADO_CTPS'
      FixedChar = True
      Size = 2
    end
    object cdsFuncionario_FNUM_TITULO_ELEITOR: TStringField
      FieldName = 'NUM_TITULO_ELEITOR'
      Size = 14
    end
    object cdsFuncionario_FZONA_TE: TStringField
      FieldName = 'ZONA_TE'
      Size = 3
    end
    object cdsFuncionario_FSECAO_TE: TStringField
      FieldName = 'SECAO_TE'
      Size = 4
    end
    object cdsFuncionario_FCOD_MUNICIPIO_TE: TStringField
      FieldName = 'COD_MUNICIPIO_TE'
      Size = 7
    end
    object cdsFuncionario_FSIGLA_ESTADO_TE: TStringField
      FieldName = 'SIGLA_ESTADO_TE'
      FixedChar = True
      Size = 2
    end
    object cdsFuncionario_FDATA_EMISSAO_TE: TDateField
      FieldName = 'DATA_EMISSAO_TE'
    end
    object cdsFuncionario_FTIPO_SANGUINEO: TStringField
      FieldName = 'TIPO_SANGUINEO'
      Size = 2
    end
    object cdsFuncionario_FFATOR_RH: TStringField
      FieldName = 'FATOR_RH'
      Size = 8
    end
    object cdsFuncionario_FLOGRADOURO: TStringField
      FieldName = 'LOGRADOURO'
      Size = 250
    end
    object cdsFuncionario_FNUM_LOGRADOURO: TStringField
      FieldName = 'NUM_LOGRADOURO'
      Size = 25
    end
    object cdsFuncionario_FCOMPLEMENTO_LOGRADOURO: TStringField
      FieldName = 'COMPLEMENTO_LOGRADOURO'
      Size = 100
    end
    object cdsFuncionario_FBAIRRO_LOGRADOURO: TStringField
      FieldName = 'BAIRRO_LOGRADOURO'
      Size = 250
    end
    object cdsFuncionario_FCEP_LOGR: TStringField
      FieldName = 'CEP_LOGR'
      Size = 9
    end
    object cdsFuncionario_FCOD_MUNICIPIO_LOGR: TStringField
      FieldName = 'COD_MUNICIPIO_LOGR'
      Size = 7
    end
    object cdsFuncionario_FSIGLA_ESTADO_LOGR: TStringField
      FieldName = 'SIGLA_ESTADO_LOGR'
      FixedChar = True
      Size = 2
    end
    object cdsFuncionario_FTELEFONE: TStringField
      FieldName = 'TELEFONE'
      Size = 13
    end
    object cdsFuncionario_FCELULAR: TStringField
      FieldName = 'CELULAR'
      Size = 14
    end
    object cdsFuncionario_FEMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 250
    end
    object cdsFuncionario_FID_CARGO_FK: TIntegerField
      FieldName = 'ID_CARGO_FK'
    end
    object cdsFuncionario_FCOD_ESCOLARIDADE: TIntegerField
      FieldName = 'COD_ESCOLARIDADE'
    end
    object cdsFuncionario_FCOD_ESTADOCIVIL: TIntegerField
      FieldName = 'COD_ESTADOCIVIL'
    end
    object cdsFuncionario_FQTD_FILHOS: TIntegerField
      FieldName = 'QTD_FILHOS'
    end
    object cdsFuncionario_FQTD_DEPENDENTES: TIntegerField
      FieldName = 'QTD_DEPENDENTES'
    end
    object cdsFuncionario_FHORA_ENT_TRAB: TTimeField
      FieldName = 'HORA_ENT_TRAB'
    end
    object cdsFuncionario_FHORA_SAI_ALMOCO: TTimeField
      FieldName = 'HORA_SAI_ALMOCO'
    end
    object cdsFuncionario_FHORA_ENT_ALMOCO: TTimeField
      FieldName = 'HORA_ENT_ALMOCO'
    end
    object cdsFuncionario_FHORA_SAI_TRAB: TTimeField
      FieldName = 'HORA_SAI_TRAB'
    end
    object cdsFuncionario_FVR_SALARIO: TBCDField
      FieldName = 'VR_SALARIO'
      Precision = 18
      Size = 2
    end
    object cdsFuncionario_FVR_MAXIMO_VALE: TBCDField
      FieldName = 'VR_MAXIMO_VALE'
      Precision = 18
      Size = 2
    end
    object cdsFuncionario_FSEXO: TStringField
      FieldName = 'SEXO'
      FixedChar = True
      Size = 1
    end
    object cdsFuncionario_FDATA_ADMISSAO: TDateField
      FieldName = 'DATA_ADMISSAO'
    end
    object cdsFuncionario_FDATA_CADASTRO: TDateField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsFuncionario_FDATA_DESLIGAMENTO: TDateField
      FieldName = 'DATA_DESLIGAMENTO'
    end
    object cdsFuncionario_FFLG_ATIVO: TStringField
      FieldName = 'FLG_ATIVO'
      FixedChar = True
      Size = 1
    end
  end
  object dsFuncionario_F: TDataSource
    DataSet = cdsFuncionario_F
    Left = 34
    Top = 159
  end
  object qryFuncComissao_F: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT *'
      '  FROM FUNCIONARIO_COMISSAO'
      ' WHERE ID_FUNCIONARIO_FK = :ID_FUNCIONARIO')
    Left = 130
    Top = 15
    ParamData = <
      item
        Position = 1
        Name = 'ID_FUNCIONARIO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspFuncComissao_F: TDataSetProvider
    DataSet = qryFuncComissao_F
    Left = 132
    Top = 63
  end
  object cdsFuncComissao_F: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_FUNCIONARIO'
        ParamType = ptInput
      end>
    ProviderName = 'dspFuncComissao_F'
    OnCalcFields = cdsFuncComissao_FCalcFields
    Left = 132
    Top = 111
    object cdsFuncComissao_FID_FUNCIONARIO_COMISSAO: TIntegerField
      FieldName = 'ID_FUNCIONARIO_COMISSAO'
      Required = True
    end
    object cdsFuncComissao_FID_FUNCIONARIO_FK: TIntegerField
      FieldName = 'ID_FUNCIONARIO_FK'
    end
    object cdsFuncComissao_FPERCENT_COMISSAO: TBCDField
      FieldName = 'PERCENT_COMISSAO'
      OnGetText = cdsFuncComissao_FPERCENT_COMISSAOGetText
      Precision = 18
      Size = 2
    end
    object cdsFuncComissao_FID_SISTEMA_FK: TIntegerField
      FieldName = 'ID_SISTEMA_FK'
    end
    object cdsFuncComissao_FCOD_ADICIONAL: TIntegerField
      FieldName = 'COD_ADICIONAL'
    end
    object cdsFuncComissao_FNOVO: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'NOVO'
    end
    object cdsFuncComissao_FNOMESISTEMA: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMESISTEMA'
      Size = 150
    end
    object cdsFuncComissao_FID_ITEM_FK: TIntegerField
      FieldName = 'ID_ITEM_FK'
    end
    object cdsFuncComissao_FDESCR_ITEM: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCR_ITEM'
      Size = 250
    end
    object cdsFuncComissao_FVR_COMISSAO: TBCDField
      FieldName = 'VR_COMISSAO'
      Precision = 18
      Size = 2
    end
    object cdsFuncComissao_FTIPO_COBRANCA: TStringField
      FieldName = 'TIPO_COBRANCA'
      FixedChar = True
      Size = 2
    end
  end
  object dsFuncComissao_F: TDataSource
    DataSet = cdsFuncComissao_F
    Left = 128
    Top = 160
  end
  object qryVale_F: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM VALE')
    Left = 224
    Top = 16
  end
  object dsVale_F: TDataSource
    DataSet = cdsVale_F
    Left = 224
    Top = 160
  end
  object qrySistema: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT *'
      '  FROM SISTEMA'
      'ORDER BY NOME_SISTEMA')
    Left = 560
    Top = 32
  end
  object qryTipoAto: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT DISTINCT I.ID_ITEM, I.DESCR_ITEM'
      '  FROM ITEM I'
      ' INNER JOIN LANCAMENTO_DET LD'
      '    ON I.ID_ITEM = LD.ID_ITEM_FK'
      ' INNER JOIN LANCAMENTO L'
      '    ON LD.COD_LANCAMENTO_FK = L.COD_LANCAMENTO'
      '   AND LD.ANO_LANCAMENTO_FK = L.ANO_LANCAMENTO'
      ' WHERE LD.COD_ADICIONAL IS NOT NULL'
      '   AND L.ID_SISTEMA_FK = :ID_SISTEMA'
      'ORDER BY I.DESCR_ITEM')
    Left = 640
    Top = 32
    ParamData = <
      item
        Name = 'ID_SISTEMA'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dsSistema: TDataSource
    DataSet = qrySistema
    Left = 560
    Top = 80
  end
  object dsTipoAto: TDataSource
    DataSet = qryTipoAto
    Left = 640
    Top = 80
  end
  object dsEscolaridade: TDataSource
    DataSet = qryEscolaridade
    Left = 720
    Top = 80
  end
  object dsUFCTPS: TDataSource
    DataSet = qryUFCTPS
    Left = 792
    Top = 80
  end
  object dsUFEnd: TDataSource
    DataSet = qryUFEnd
    Left = 624
    Top = 200
  end
  object dsEstadoCivil: TDataSource
    DataSet = qryEstadoCivil
    Left = 768
    Top = 200
  end
  object qryEscolaridade: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT *'
      '  FROM ESCOLARIDADE'
      'ORDER BY DESCR_ESCOLARIDADE')
    Left = 720
    Top = 32
  end
  object qryUFCTPS: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT *'
      '  FROM ESTADO'
      'ORDER BY SIGLA_ESTADO')
    Left = 792
    Top = 32
  end
  object qryUFEnd: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT *'
      '  FROM ESTADO'
      'ORDER BY SIGLA_ESTADO')
    Left = 624
    Top = 152
  end
  object qryEstadoCivil: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT *'
      '  FROM ESTADOCIVIL'
      'ORDER BY DESCR_ESTADOCIVIL')
    Left = 768
    Top = 152
  end
  object qryCidade: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT COD_MUNICIPIOC, MUNICIPIO'
      '  FROM MUNICIPIO'
      ' WHERE UF_ESTADO = :sUF'
      'ORDER BY MUNICIPIO')
    Left = 692
    Top = 152
    ParamData = <
      item
        Name = 'SUF'
        DataType = ftString
        ParamType = ptInput
        Size = 2
      end>
  end
  object dsCidade: TDataSource
    DataSet = qryCidade
    Left = 692
    Top = 200
  end
  object dsUFTE: TDataSource
    DataSet = qryUFTE
    Left = 488
    Top = 200
  end
  object qryUFTE: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT *'
      '  FROM ESTADO'
      'ORDER BY SIGLA_ESTADO')
    Left = 488
    Top = 152
  end
  object qryCidadeTE: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT COD_MUNICIPIOC, MUNICIPIO'
      '  FROM MUNICIPIO'
      ' WHERE UF_ESTADO = :sUF'
      'ORDER BY MUNICIPIO')
    Left = 556
    Top = 152
    ParamData = <
      item
        Name = 'SUF'
        DataType = ftString
        ParamType = ptInput
        Size = 2
      end>
  end
  object dsCidadeTE: TDataSource
    DataSet = qryCidadeTE
    Left = 556
    Top = 200
  end
  object qryComisRec_F: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM COMISSAO')
    Left = 296
    Top = 16
  end
  object dsComisRec_F: TDataSource
    DataSet = cdsComisRec_F
    Left = 296
    Top = 160
  end
  object qrySistemaCom: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM SISTEMA'
      'ORDER BY NOME_SISTEMA')
    Left = 32
    Top = 288
  end
  object qryTipoAtoCom: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT DISTINCT I.ID_ITEM, I.DESCR_ITEM'
      '  FROM ITEM I'
      ' INNER JOIN LANCAMENTO_DET LD'
      '    ON I.ID_ITEM = LD.ID_ITEM_FK'
      ' INNER JOIN LANCAMENTO L'
      '    ON LD.COD_LANCAMENTO_FK = L.COD_LANCAMENTO'
      '   AND LD.ANO_LANCAMENTO_FK = L.ANO_LANCAMENTO'
      ' WHERE LD.COD_ADICIONAL IS NOT NULL'
      '   AND L.ID_SISTEMA_FK = :ID_SISTEMA'
      'ORDER BY I.DESCR_ITEM')
    Left = 120
    Top = 288
    ParamData = <
      item
        Name = 'ID_SISTEMA'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dsSistemaCom: TDataSource
    DataSet = qrySistemaCom
    Left = 32
    Top = 336
  end
  object dsTipoAtoCom: TDataSource
    DataSet = qryTipoAtoCom
    Left = 120
    Top = 336
  end
  object cdsVale_F: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspVale_F'
    Left = 224
    Top = 112
    object cdsVale_FID_VALE: TIntegerField
      FieldName = 'ID_VALE'
      Required = True
    end
    object cdsVale_FID_FUNCIONARIO_FK: TIntegerField
      FieldName = 'ID_FUNCIONARIO_FK'
    end
    object cdsVale_FDATA_VALE: TDateField
      FieldName = 'DATA_VALE'
    end
    object cdsVale_FVR_VALE: TBCDField
      FieldName = 'VR_VALE'
      OnGetText = cdsVale_FVR_VALEGetText
      Precision = 18
      Size = 2
    end
    object cdsVale_FDESCR_VALE: TStringField
      FieldName = 'DESCR_VALE'
      Size = 1000
    end
    object cdsVale_FDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsVale_FCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsVale_FDATA_QUITACAO: TDateField
      FieldName = 'DATA_QUITACAO'
    end
    object cdsVale_FQUIT_ID_USUARIO: TIntegerField
      FieldName = 'QUIT_ID_USUARIO'
    end
    object cdsVale_FFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsVale_FDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsVale_FCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsVale_FID_FECHAMENTO_SALARIO_FK: TIntegerField
      FieldName = 'ID_FECHAMENTO_SALARIO_FK'
    end
  end
  object cdsComisRec_F: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspComisRec_F'
    OnCalcFields = cdsComisRec_FCalcFields
    Left = 296
    Top = 112
    object cdsComisRec_FID_COMISSAO: TIntegerField
      FieldName = 'ID_COMISSAO'
      Required = True
    end
    object cdsComisRec_FID_FUNCIONARIO_FK: TIntegerField
      FieldName = 'ID_FUNCIONARIO_FK'
    end
    object cdsComisRec_FID_SISTEMA_FK: TIntegerField
      FieldName = 'ID_SISTEMA_FK'
    end
    object cdsComisRec_FDATA_COMISSAO: TDateField
      FieldName = 'DATA_COMISSAO'
    end
    object cdsComisRec_FID_ITEM_FK: TIntegerField
      FieldName = 'ID_ITEM_FK'
    end
    object cdsComisRec_FCOD_ADICIONAL: TIntegerField
      FieldName = 'COD_ADICIONAL'
    end
    object cdsComisRec_FID_ORIGEM: TIntegerField
      FieldName = 'ID_ORIGEM'
    end
    object cdsComisRec_FTIPO_ORIGEM: TStringField
      FieldName = 'TIPO_ORIGEM'
      FixedChar = True
      Size = 1
    end
    object cdsComisRec_FPERCENT_COMISSAO: TBCDField
      FieldName = 'PERCENT_COMISSAO'
      Precision = 18
      Size = 2
    end
    object cdsComisRec_FVR_COMISSAO: TBCDField
      FieldName = 'VR_COMISSAO'
      Precision = 18
      Size = 2
    end
    object cdsComisRec_FID_FECHAMENTO_SALARIO_FK: TIntegerField
      FieldName = 'ID_FECHAMENTO_SALARIO_FK'
    end
    object cdsComisRec_FDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsComisRec_FCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsComisRec_FDATA_PAGAMENTO: TDateField
      FieldName = 'DATA_PAGAMENTO'
    end
    object cdsComisRec_FPAG_ID_USUARIO: TIntegerField
      FieldName = 'PAG_ID_USUARIO'
    end
    object cdsComisRec_FFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsComisRec_FDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsComisRec_FCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsComisRec_FDESCR_ITEM: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCR_ITEM'
      Size = 250
    end
    object cdsComisRec_FTIPO_COBRANCA: TStringField
      FieldName = 'TIPO_COBRANCA'
      FixedChar = True
      Size = 2
    end
  end
  object dspVale_F: TDataSetProvider
    DataSet = qryVale_F
    Left = 224
    Top = 64
  end
  object dspComisRec_F: TDataSetProvider
    DataSet = qryComisRec_F
    Left = 296
    Top = 64
  end
  object qryODespF_F: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM OUTRADESPESA_FUNC')
    Left = 376
    Top = 16
  end
  object dspODespF_F: TDataSetProvider
    DataSet = qryODespF_F
    Left = 376
    Top = 64
  end
  object cdsODespF_F: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspODespF_F'
    OnCalcFields = cdsODespF_FCalcFields
    Left = 376
    Top = 112
    object cdsODespF_FID_OUTRADESPESA_FUNC: TIntegerField
      FieldName = 'ID_OUTRADESPESA_FUNC'
      Required = True
    end
    object cdsODespF_FID_FUNCIONARIO_FK: TIntegerField
      FieldName = 'ID_FUNCIONARIO_FK'
    end
    object cdsODespF_FID_TIPO_OUTRADESP_FUNC_FK: TIntegerField
      FieldName = 'ID_TIPO_OUTRADESP_FUNC_FK'
    end
    object cdsODespF_FDATA_OUTRADESPESA_FUNC: TDateField
      FieldName = 'DATA_OUTRADESPESA_FUNC'
    end
    object cdsODespF_FVR_OUTRADESPESA_FUNC: TBCDField
      FieldName = 'VR_OUTRADESPESA_FUNC'
      OnGetText = cdsODespF_FVR_OUTRADESPESA_FUNCGetText
      Precision = 18
      Size = 2
    end
    object cdsODespF_FDESCR_OUTRADESPESA_FUNC: TStringField
      FieldName = 'DESCR_OUTRADESPESA_FUNC'
      Size = 1000
    end
    object cdsODespF_FDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsODespF_FCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsODespF_FDATA_QUITACAO: TDateField
      FieldName = 'DATA_QUITACAO'
    end
    object cdsODespF_FQUIT_ID_USUARIO: TIntegerField
      FieldName = 'QUIT_ID_USUARIO'
    end
    object cdsODespF_FFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsODespF_FDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsODespF_FCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsODespF_FTIPO_ODESPF: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'TIPO_ODESPF'
      Size = 200
    end
    object cdsODespF_FID_FECHAMENTO_SALARIO_FK: TIntegerField
      FieldName = 'ID_FECHAMENTO_SALARIO_FK'
    end
    object cdsODespF_FFLG_MODALIDADE: TStringField
      FieldName = 'FLG_MODALIDADE'
      FixedChar = True
      Size = 1
    end
    object cdsODespF_FFLG_RECORRENTE: TStringField
      FieldName = 'FLG_RECORRENTE'
      FixedChar = True
      Size = 1
    end
  end
  object dsODespF_F: TDataSource
    DataSet = cdsODespF_F
    Left = 376
    Top = 160
  end
  object qryFunc2: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM FUNCIONARIO'
      'WHERE ID_FUNCIONARIO = :ID_FUNCIONARIO')
    Left = 288
    Top = 288
    ParamData = <
      item
        Position = 1
        Name = 'ID_FUNCIONARIO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspFunc2: TDataSetProvider
    DataSet = qryFunc2
    Left = 288
    Top = 336
  end
  object cdsFunc2: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_FUNCIONARIO'
        ParamType = ptInput
      end>
    ProviderName = 'dspFunc2'
    Left = 288
    Top = 384
    object cdsFunc2ID_FUNCIONARIO: TIntegerField
      FieldName = 'ID_FUNCIONARIO'
      Required = True
    end
    object cdsFunc2NOME_FUNCIONARIO: TStringField
      FieldName = 'NOME_FUNCIONARIO'
      Size = 250
    end
    object cdsFunc2DATA_NASCIMENTO: TDateField
      FieldName = 'DATA_NASCIMENTO'
    end
    object cdsFunc2NUM_IDENTIDADE: TStringField
      FieldName = 'NUM_IDENTIDADE'
      Size = 25
    end
    object cdsFunc2ORGAO_EMISSOR: TStringField
      FieldName = 'ORGAO_EMISSOR'
      Size = 70
    end
    object cdsFunc2DATA_EMISSAO_IDENTIDADE: TDateField
      FieldName = 'DATA_EMISSAO_IDENTIDADE'
    end
    object cdsFunc2CPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
    object cdsFunc2NUM_CTPS: TStringField
      FieldName = 'NUM_CTPS'
      Size = 7
    end
    object cdsFunc2SERIE_CTPS: TStringField
      FieldName = 'SERIE_CTPS'
      Size = 5
    end
    object cdsFunc2DATA_CTPS: TDateField
      FieldName = 'DATA_CTPS'
    end
    object cdsFunc2NUM_PIS: TStringField
      FieldName = 'NUM_PIS'
      Size = 14
    end
    object cdsFunc2SIGLA_ESTADO_CTPS: TStringField
      FieldName = 'SIGLA_ESTADO_CTPS'
      FixedChar = True
      Size = 2
    end
    object cdsFunc2NUM_TITULO_ELEITOR: TStringField
      FieldName = 'NUM_TITULO_ELEITOR'
      Size = 14
    end
    object cdsFunc2ZONA_TE: TStringField
      FieldName = 'ZONA_TE'
      Size = 3
    end
    object cdsFunc2SECAO_TE: TStringField
      FieldName = 'SECAO_TE'
      Size = 4
    end
    object cdsFunc2COD_MUNICIPIO_TE: TStringField
      FieldName = 'COD_MUNICIPIO_TE'
      Size = 7
    end
    object cdsFunc2SIGLA_ESTADO_TE: TStringField
      FieldName = 'SIGLA_ESTADO_TE'
      FixedChar = True
      Size = 2
    end
    object cdsFunc2DATA_EMISSAO_TE: TDateField
      FieldName = 'DATA_EMISSAO_TE'
    end
    object cdsFunc2TIPO_SANGUINEO: TStringField
      FieldName = 'TIPO_SANGUINEO'
      Size = 2
    end
    object cdsFunc2FATOR_RH: TStringField
      FieldName = 'FATOR_RH'
      Size = 8
    end
    object cdsFunc2LOGRADOURO: TStringField
      FieldName = 'LOGRADOURO'
      Size = 250
    end
    object cdsFunc2NUM_LOGRADOURO: TStringField
      FieldName = 'NUM_LOGRADOURO'
      Size = 25
    end
    object cdsFunc2COMPLEMENTO_LOGRADOURO: TStringField
      FieldName = 'COMPLEMENTO_LOGRADOURO'
      Size = 100
    end
    object cdsFunc2BAIRRO_LOGRADOURO: TStringField
      FieldName = 'BAIRRO_LOGRADOURO'
      Size = 250
    end
    object cdsFunc2CEP_LOGR: TStringField
      FieldName = 'CEP_LOGR'
      Size = 9
    end
    object cdsFunc2COD_MUNICIPIO_LOGR: TStringField
      FieldName = 'COD_MUNICIPIO_LOGR'
      Size = 7
    end
    object cdsFunc2SIGLA_ESTADO_LOGR: TStringField
      FieldName = 'SIGLA_ESTADO_LOGR'
      FixedChar = True
      Size = 2
    end
    object cdsFunc2TELEFONE: TStringField
      FieldName = 'TELEFONE'
      Size = 13
    end
    object cdsFunc2CELULAR: TStringField
      FieldName = 'CELULAR'
      Size = 14
    end
    object cdsFunc2EMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 250
    end
    object cdsFunc2ID_CARGO_FK: TIntegerField
      FieldName = 'ID_CARGO_FK'
    end
    object cdsFunc2COD_ESCOLARIDADE: TIntegerField
      FieldName = 'COD_ESCOLARIDADE'
    end
    object cdsFunc2COD_ESTADOCIVIL: TIntegerField
      FieldName = 'COD_ESTADOCIVIL'
    end
    object cdsFunc2QTD_FILHOS: TIntegerField
      FieldName = 'QTD_FILHOS'
    end
    object cdsFunc2QTD_DEPENDENTES: TIntegerField
      FieldName = 'QTD_DEPENDENTES'
    end
    object cdsFunc2HORA_ENT_TRAB: TTimeField
      FieldName = 'HORA_ENT_TRAB'
    end
    object cdsFunc2HORA_SAI_ALMOCO: TTimeField
      FieldName = 'HORA_SAI_ALMOCO'
    end
    object cdsFunc2HORA_ENT_ALMOCO: TTimeField
      FieldName = 'HORA_ENT_ALMOCO'
    end
    object cdsFunc2HORA_SAI_TRAB: TTimeField
      FieldName = 'HORA_SAI_TRAB'
    end
    object cdsFunc2VR_SALARIO: TBCDField
      FieldName = 'VR_SALARIO'
      Precision = 18
      Size = 2
    end
    object cdsFunc2VR_MAXIMO_VALE: TBCDField
      FieldName = 'VR_MAXIMO_VALE'
      Precision = 18
      Size = 2
    end
    object cdsFunc2SEXO: TStringField
      FieldName = 'SEXO'
      FixedChar = True
      Size = 1
    end
    object cdsFunc2DATA_ADMISSAO: TDateField
      FieldName = 'DATA_ADMISSAO'
    end
    object cdsFunc2DATA_CADASTRO: TDateField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsFunc2DATA_DESLIGAMENTO: TDateField
      FieldName = 'DATA_DESLIGAMENTO'
    end
    object cdsFunc2FLG_ATIVO: TStringField
      FieldName = 'FLG_ATIVO'
      FixedChar = True
      Size = 1
    end
    object cdsFunc2ID_PESSOA: TIntegerField
      FieldName = 'ID_PESSOA'
    end
  end
  object dsFunc2: TDataSource
    DataSet = cdsFunc2
    Left = 288
    Top = 432
  end
  object qryFuncCom2: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM FUNCIONARIO_COMISSAO'
      ' WHERE ID_FUNCIONARIO_COMISSAO = :ID_FUNCIONARIO_COMISSAO')
    Left = 368
    Top = 288
    ParamData = <
      item
        Position = 1
        Name = 'ID_FUNCIONARIO_COMISSAO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspFuncCom2: TDataSetProvider
    DataSet = qryFuncCom2
    Left = 368
    Top = 336
  end
  object cdsFuncCom2: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_FUNCIONARIO_COMISSAO'
        ParamType = ptInput
      end>
    ProviderName = 'dspFuncCom2'
    Left = 368
    Top = 384
    object cdsFuncCom2ID_FUNCIONARIO_COMISSAO: TIntegerField
      FieldName = 'ID_FUNCIONARIO_COMISSAO'
      Required = True
    end
    object cdsFuncCom2ID_FUNCIONARIO_FK: TIntegerField
      FieldName = 'ID_FUNCIONARIO_FK'
    end
    object cdsFuncCom2PERCENT_COMISSAO: TBCDField
      FieldName = 'PERCENT_COMISSAO'
      Precision = 18
      Size = 2
    end
    object cdsFuncCom2ID_SISTEMA_FK: TIntegerField
      FieldName = 'ID_SISTEMA_FK'
    end
    object cdsFuncCom2COD_ADICIONAL: TIntegerField
      FieldName = 'COD_ADICIONAL'
    end
    object cdsFuncCom2ID_ITEM_FK: TIntegerField
      FieldName = 'ID_ITEM_FK'
    end
    object cdsFuncCom2VR_COMISSAO: TBCDField
      FieldName = 'VR_COMISSAO'
      Precision = 18
      Size = 2
    end
    object cdsFuncCom2TIPO_COBRANCA: TStringField
      FieldName = 'TIPO_COBRANCA'
      FixedChar = True
      Size = 2
    end
  end
  object dsFuncCom2: TDataSource
    DataSet = cdsFuncCom2
    Left = 368
    Top = 432
  end
end
