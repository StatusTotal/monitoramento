{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroRelatorioFechamentoCaixaAtos.pas
  Descricao:   Filtro Relatorios de Fechamento de Caixa por Tipo de Ato (R03 - R03DET)
  Author:      Cristina
  Date:        03-nov-2016
  Last Update: 11-out-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroRelatorioFechamentoCaixaAtos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroRelatorioPadrao, Vcl.StdCtrls,
  JvExControls, JvButton, JvTransparentButton, Vcl.ExtCtrls, Vcl.Mask, JvExMask,
  JvToolEdit, Vcl.DBCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFFiltroRelatorioFechamentoCaixaAtos = class(TFFiltroRelatorioPadrao)
    lblPeriodo: TLabel;
    lblPeriodoInicio: TLabel;
    dteDataInicio: TJvDateEdit;
    lblPeriodoFim: TLabel;
    dteDataFim: TJvDateEdit;
    rgTipoAto: TRadioGroup;
    lcbTipoAto: TDBLookupComboBox;
    qryTipoAto: TFDQuery;
    dsTipoAto: TDataSource;
    chbExibirDemaisLanc: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure rgTipoAtoClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
    procedure rgTipoAtoExit(Sender: TObject);
    procedure chbExibirDemaisLancClick(Sender: TObject);
    procedure dteDataInicioKeyPress(Sender: TObject; var Key: Char);
    procedure dteDataFimKeyPress(Sender: TObject; var Key: Char);
  protected
    procedure GravarLog; override;
    procedure GerarRelatorio(ImpDet: Boolean); override;
    function VerificarFiltro: Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroRelatorioFechamentoCaixaAtos: TFFiltroRelatorioFechamentoCaixaAtos;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMRelatorios;

{ TFFiltroRelatorioFechamentoCaixaAtos }

procedure TFFiltroRelatorioFechamentoCaixaAtos.btnLimparClick(Sender: TObject);
begin
  inherited;

  dteDataInicio.Date  := Date;
  dteDataFim.Date     := Date;
  rgTipoAto.ItemIndex := 0;
  lcbTipoAto.Enabled  := False;

  chbExibirDemaisLanc.Checked := True;

  chbTipoSintetico.Checked := True;
  chbTipoAnalitico.Checked := False;

  chbExportarPDF.Checked := False;

  chbExportarExcel.Enabled := vgPrm_ExpFecCx;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioFechamentoCaixaAtos.btnVisualizarClick(
  Sender: TObject);
begin
  inherited;

  if not VerificarFiltro then
    Exit;

  //Totais
  dmRelatorios.qryRel03_Totais.Close;

  dmRelatorios.qryRel03_Totais.Params.ParamByName('DATA_INI1').Value := dteDataInicio.Date;
  dmRelatorios.qryRel03_Totais.Params.ParamByName('DATA_FIM1').Value := dteDataFim.Date;
  dmRelatorios.qryRel03_Totais.Params.ParamByName('DATA_INI2').Value := dteDataInicio.Date;
  dmRelatorios.qryRel03_Totais.Params.ParamByName('DATA_FIM2').Value := dteDataFim.Date;
  dmRelatorios.qryRel03_Totais.Params.ParamByName('DATA_INI3').Value := dteDataInicio.Date;
  dmRelatorios.qryRel03_Totais.Params.ParamByName('DATA_FIM3').Value := dteDataFim.Date;
  dmRelatorios.qryRel03_Totais.Params.ParamByName('DATA_INI4').Value := dteDataInicio.Date;
  dmRelatorios.qryRel03_Totais.Params.ParamByName('DATA_FIM4').Value := dteDataFim.Date;

  if rgTipoAto.ItemIndex = 0 then
  begin
    dmRelatorios.qryRel03_Totais.Params.ParamByName('ID_ITEM11').Value := 0;
    dmRelatorios.qryRel03_Totais.Params.ParamByName('ID_ITEM12').Value := 0;
    dmRelatorios.qryRel03_Totais.Params.ParamByName('ID_ITEM13').Value := 0;
    dmRelatorios.qryRel03_Totais.Params.ParamByName('ID_ITEM14').Value := 0;
    dmRelatorios.qryRel03_Totais.Params.ParamByName('ID_ITEM31').Value := 0;
    dmRelatorios.qryRel03_Totais.Params.ParamByName('ID_ITEM32').Value := 0;
    dmRelatorios.qryRel03_Totais.Params.ParamByName('ID_ITEM33').Value := 0;
    dmRelatorios.qryRel03_Totais.Params.ParamByName('ID_ITEM34').Value := 0;
  end
  else
  begin
    dmRelatorios.qryRel03_Totais.Params.ParamByName('ID_ITEM11').Value := lcbTipoAto.KeyValue;
    dmRelatorios.qryRel03_Totais.Params.ParamByName('ID_ITEM12').Value := lcbTipoAto.KeyValue;
    dmRelatorios.qryRel03_Totais.Params.ParamByName('ID_ITEM13').Value := lcbTipoAto.KeyValue;
    dmRelatorios.qryRel03_Totais.Params.ParamByName('ID_ITEM14').Value := lcbTipoAto.KeyValue;
    dmRelatorios.qryRel03_Totais.Params.ParamByName('ID_ITEM31').Value := lcbTipoAto.KeyValue;
    dmRelatorios.qryRel03_Totais.Params.ParamByName('ID_ITEM32').Value := lcbTipoAto.KeyValue;
    dmRelatorios.qryRel03_Totais.Params.ParamByName('ID_ITEM33').Value := lcbTipoAto.KeyValue;
    dmRelatorios.qryRel03_Totais.Params.ParamByName('ID_ITEM34').Value := lcbTipoAto.KeyValue;
  end;

  if chbExibirDemaisLanc.Checked then
  begin
    dmRelatorios.qryRel03_Totais.Params.ParamByName('EXIBIR2').Value := 1;
    dmRelatorios.qryRel03_Totais.Params.ParamByName('EXIBIR4').Value := 1;
  end
  else
  begin
    dmRelatorios.qryRel03_Totais.Params.ParamByName('EXIBIR2').Value := 0;
    dmRelatorios.qryRel03_Totais.Params.ParamByName('EXIBIR4').Value := 0;
  end;

  dmRelatorios.qryRel03_Totais.Open;

  if dmRelatorios.qryRel03_Totais.RecordCount = 0 then
  begin
    Application.MessageBox('N�o h� Lan�amentos registrados no per�odo informado.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
  end
  else
  begin
    dmRelatorios.qryRel03_Lancs.Close;

    dmRelatorios.qryRel03_Lancs.Params.ParamByName('DATA_INI1').Value := dteDataInicio.Date;
    dmRelatorios.qryRel03_Lancs.Params.ParamByName('DATA_FIM1').Value := dteDataFim.Date;

    dmRelatorios.qryRel03_Lancs.Params.ParamByName('DATA_INI2').Value := dteDataInicio.Date;
    dmRelatorios.qryRel03_Lancs.Params.ParamByName('DATA_FIM2').Value := dteDataFim.Date;

    if rgTipoAto.ItemIndex = 0 then
    begin
      dmRelatorios.qryRel03_Lancs.Params.ParamByName('ID_ITEM11').Value := 0;
      dmRelatorios.qryRel03_Lancs.Params.ParamByName('ID_ITEM12').Value := 0;
      dmRelatorios.qryRel03_Lancs.Params.ParamByName('ID_ITEM21').Value := 0;
      dmRelatorios.qryRel03_Lancs.Params.ParamByName('ID_ITEM22').Value := 0;
    end
    else
    begin
      dmRelatorios.qryRel03_Lancs.Params.ParamByName('ID_ITEM11').Value := lcbTipoAto.KeyValue;
      dmRelatorios.qryRel03_Lancs.Params.ParamByName('ID_ITEM12').Value := lcbTipoAto.KeyValue;
      dmRelatorios.qryRel03_Lancs.Params.ParamByName('ID_ITEM21').Value := lcbTipoAto.KeyValue;
      dmRelatorios.qryRel03_Lancs.Params.ParamByName('ID_ITEM22').Value := lcbTipoAto.KeyValue;
    end;

    if chbExibirDemaisLanc.Checked then
      dmRelatorios.qryRel03_Lancs.Params.ParamByName('EXIBIR').Value := 1
    else
      dmRelatorios.qryRel03_Lancs.Params.ParamByName('EXIBIR').Value := 0;

    dmRelatorios.qryRel03_Lancs.Open;

    if chbTipoSintetico.Checked then
      GerarRelatorio(False);

    if chbTipoAnalitico.Checked then
      GerarRelatorio(True);
  end;
end;

procedure TFFiltroRelatorioFechamentoCaixaAtos.chbExibirDemaisLancClick(
  Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(chbExibirDemaisLanc, 1, 'Demais Lan�amentos',
                                      'Marque essa op��o para incluir no relat�rio os Lan�amentos n�o associados ' +
                                      'a Atos ou que possuem Atos que n�o est�o cadastrados na tabela Adicional.',
                                      clBlue, clNavy);
end;

procedure TFFiltroRelatorioFechamentoCaixaAtos.dteDataFimKeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if rgTipoAto.ItemIndex = 1 then  //Especifico
    begin
      qryTipoAto.Close;
      qryTipoAto.Params.ParamByName('DATA_INI').Value := dteDataInicio.Date;
      qryTipoAto.Params.ParamByName('DATA_FIM').Value := dteDataFim.Date;
      qryTipoAto.Open;
    end;

    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroRelatorioFechamentoCaixaAtos.dteDataInicioKeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if rgTipoAto.ItemIndex = 1 then  //Especifico
    begin
      qryTipoAto.Close;
      qryTipoAto.Params.ParamByName('DATA_INI').Value := dteDataInicio.Date;
      qryTipoAto.Params.ParamByName('DATA_FIM').Value := dteDataFim.Date;
      qryTipoAto.Open;
    end;

    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroRelatorioFechamentoCaixaAtos.FormShow(Sender: TObject);
begin
  inherited;

  dteDataInicio.Date := Date;
  dteDataFim.Date    := Date;
  rgTipoAto.ItemIndex := 0;
  lcbTipoAto.Enabled := False;

  chbExibirDemaisLanc.Checked := True;

  chbTipoSintetico.Checked := True;
  chbTipoAnalitico.Checked := False;

  chbExportarPDF.Checked := False;

  chbExportarExcel.Enabled := vgPrm_ExpFecCx;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;

  chbExibirGrafico.Visible := False;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioFechamentoCaixaAtos.GerarRelatorio(ImpDet: Boolean);
begin
  inherited;

  try
    sTituloRelatorio    := 'FECHAMENTO DE CAIXA (por Forma de Pagamento)';
    sSubtituloRelatorio := 'Per�odo de ' + DateToStr(dteDataInicio.Date) + ' a ' + DateToStr(dteDataFim.Date);

    if ImpDet then
    begin
      lExibeDetalhe := True;

      dmRelatorios.DefinirRelatorio(R03DET);

      dmRelatorios.qryRel03_Det.Close;
      dmRelatorios.qryRel03_Det.Params.ParamByName('COD_LANCAMENTO').Value := dmRelatorios.qryRel03_Lancs.FieldByName('COD_LANCAMENTO_FK').AsInteger;
      dmRelatorios.qryRel03_Det.Params.ParamByName('ANO_LANCAMENTO').Value := dmRelatorios.qryRel03_Lancs.FieldByName('ANO_LANCAMENTO_FK').AsInteger;
      dmRelatorios.qryRel03_Det.Open;
    end
    else
    begin
      lExibeDetalhe := False;
      dmRelatorios.DefinirRelatorio(R03);
    end;

    dmRelatorios.ImprimirRelatorio(chbExportarPDF.Checked, chbExportarExcel.Checked);

    GravarLog;
  except
    Application.MessageBox('Erro ao gerar Fechamento de Caixa.',
                           'Erro',
                           MB_OK + MB_ICONERROR);
  end;
end;

procedure TFFiltroRelatorioFechamentoCaixaAtos.GravarLog;
begin
  inherited;

  BS.GravarUsuarioLog(50,
                      '',
                      'Impress�o de Relatorio de Fechamento de Caixa (' + DateToStr(dDataRelatorio) +
                                                                      ' ' +
                                                                      TimeToStr(tHoraRelatorio) + ')',
                      0,
                      '');
end;

procedure TFFiltroRelatorioFechamentoCaixaAtos.rgTipoAtoClick(Sender: TObject);
begin
  inherited;

  if rgTipoAto.ItemIndex = 0 then  //Todos
  begin
    lcbTipoAto.KeyValue := Null;
    lcbTipoAto.Enabled := False;

    chbExibirDemaisLanc.Enabled := True;
    chbExibirDemaisLanc.Checked := True;
  end
  else  //Especifico
  begin
    qryTipoAto.Close;
    qryTipoAto.Params.ParamByName('DATA_INI').Value := dteDataInicio.Date;
    qryTipoAto.Params.ParamByName('DATA_FIM').Value := dteDataFim.Date;
    qryTipoAto.Open;

    lcbTipoAto.Enabled := True;

    chbExibirDemaisLanc.Checked := False;
    chbExibirDemaisLanc.Enabled := False;
  end;
end;

procedure TFFiltroRelatorioFechamentoCaixaAtos.rgTipoAtoExit(Sender: TObject);
begin
  inherited;

  if rgTipoAto.ItemIndex = 0 then  //Todos
  begin
    lcbTipoAto.KeyValue := Null;
    lcbTipoAto.Enabled := False;

    chbExibirDemaisLanc.Enabled := True;
    chbExibirDemaisLanc.Checked := True;
  end
  else  //Especifico
  begin
    qryTipoAto.Close;
    qryTipoAto.Params.ParamByName('DATA_INI').Value := dteDataInicio.Date;
    qryTipoAto.Params.ParamByName('DATA_FIM').Value := dteDataFim.Date;
    qryTipoAto.Open;

    lcbTipoAto.Enabled := True;

    chbExibirDemaisLanc.Checked := False;
    chbExibirDemaisLanc.Enabled := False;
  end;
end;

function TFFiltroRelatorioFechamentoCaixaAtos.VerificarFiltro: Boolean;
var
  Msg: String;
begin
  Result := True;

  if dteDataInicio.Date = 0 then
  begin
    Msg := '- Informe a DATA DE IN�CIO do Per�odo.';
    Result := False;
  end;

  if dteDataFim.Date = 0 then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe a DATA DE FIM do Per�odo.'
    else
      Msg := #13#10 + '- Informe a DATA DE FIM do Per�odo.';

    Result := False;
  end;

  if dteDataFim.Date < dteDataInicio.Date then
  begin
    if Trim(Msg) = '' then
      Msg := '- A DATA DE FIM do Per�odo n�o pode inferior � DATA DE IN�CIO do mesmo.'
    else
      Msg := #13#10 + '- A DATA DE FIM do Per�odo n�o pode ser inferior � DATA DE IN�CIO do mesmo.';

    Result := False;
  end;

  if (not chbTipoAnalitico.Checked) and
    (not chbTipoSintetico.Checked) then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe o TIPO do Extrato de Contas.'
    else
      Msg := #13#10 + '- Informe o TIPO do Relat�rio de Fechamento de Caixa.';

    Result := False;
  end;

  if rgTipoAto.ItemIndex > 0 then
  begin
    if Trim(lcbTipoAto.Text) = '' then
    begin
      if Trim(Msg) = '' then
        Msg := '- Informe o tipo de Ato.'
      else
        Msg := #13#10 + '- Informe o tipo de Ato.';

      Result := False;
    end;
  end;

  if not Result then
    Application.MessageBox(PChar(':: ATEN��O ::' + #13#10 + #13#10 + Msg),
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
end;

end.
