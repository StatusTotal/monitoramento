{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroLogUsuario.pas
  Descricao:   Filtro de Log de Usuarios
  Author   :   Cristina
  Date:        01-nov-2017
  Last Update:
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroLogUsuario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.Menus, Vcl.DBCtrls, Vcl.StdCtrls, Vcl.Mask, JvExMask,
  JvToolEdit, System.DateUtils, System.Math, System.StrUtils;

type
  TFFiltroLogUsuario = class(TFFiltroSimplesPadrao)
    lblUsuario: TLabel;
    lcbUsuario: TDBLookupComboBox;
    dbgGridAudLanc: TDBGrid;
    lblLogGeral: TLabel;
    lblAuditoriaLancamento: TLabel;
    lblAuditoriaParcLancamento: TLabel;
    dbgGridAudParc: TDBGrid;
    lblAuditoriaDeposito: TLabel;
    dbgGridAudDepo: TDBGrid;
    ppmImpressao: TPopupMenu;
    ppmImpLogGeral: TMenuItem;
    N1: TMenuItem;
    ppmImpAudLancamento: TMenuItem;
    ppmImpAudParcLancamento: TMenuItem;
    ppmImpAudDeposito: TMenuItem;
    qryLogGeral: TFDQuery;
    qryAudLanc: TFDQuery;
    qryAudParc: TFDQuery;
    qryAudDepo: TFDQuery;
    dsLogGeral: TDataSource;
    dsAudLanc: TDataSource;
    dsAudParc: TDataSource;
    dsAudDepo: TDataSource;
    dsUsuario: TDataSource;
    qryUsuario: TFDQuery;
    qryLogGeralID_USUARIO_LOG: TIntegerField;
    qryLogGeralID_USUARIO: TIntegerField;
    qryLogGeralID_MODULO_FK: TIntegerField;
    qryLogGeralDATA_USUARIO_LOG: TDateField;
    qryLogGeralTIPO_LANCAMENTO: TStringField;
    qryLogGeralCODIGO_LAN: TIntegerField;
    qryLogGeralANO_LAN: TIntegerField;
    qryLogGeralID_ORIGEM: TIntegerField;
    qryLogGeralTABELA_ORIGEM: TStringField;
    qryLogGeralTIPO_ACAO: TStringField;
    qryLogGeralOBS_USUARIO_LOG: TStringField;
    qryLogGeralTP_LANCAMENTO: TStringField;
    qryLogGeralTP_ACAO: TStringField;
    qryLogGeralNOME_USUARIO: TStringField;
    qryLogGeralNOME_FUNCIONARIO: TStringField;
    qryLogGeralDESCR_MODULO: TStringField;
    gbTipo: TGroupBox;
    chbLogGeral: TCheckBox;
    chbAudLancamento: TCheckBox;
    chbAudParcela: TCheckBox;
    chbAudDeposito: TCheckBox;
    qryAudLancID_USUARIO: TIntegerField;
    qryAudLancCODIGO_LAN: TIntegerField;
    qryAudLancANO_LAN: TIntegerField;
    qryAudLancTIPO_ACAO: TStringField;
    qryAudLancTP_LANCAMENTO: TStringField;
    qryAudLancTP_ACAO: TStringField;
    qryAudLancNOME_USUARIO: TStringField;
    qryAudLancNOME_FUNCIONARIO: TStringField;
    qryAudLancID_CATEGORIA_DESPREC_FK: TIntegerField;
    qryAudLancDESCR_CATEGORIA_DESPREC: TStringField;
    qryAudLancID_SUBCATEGORIA_DESPREC_FK: TIntegerField;
    qryAudLancDESCR_SUBCATEGORIA_DESPREC: TStringField;
    qryAudLancID_NATUREZA_FK: TIntegerField;
    qryAudLancDESCR_NATUREZA: TStringField;
    qryAudLancID_CLIENTE_FORNECEDOR_FK: TIntegerField;
    qryAudLancNOME_FANTASIA: TStringField;
    qryAudLancFLG_IMPOSTORENDA: TStringField;
    qryAudLancFLG_PESSOAL: TStringField;
    qryAudLancFLG_AUXILIAR: TStringField;
    qryAudLancFLG_FLUTUANTE: TStringField;
    qryAudLancQTD_PARCELAS: TIntegerField;
    qryAudLancVR_TOTAL_PREV: TBCDField;
    qryAudLancVR_TOTAL_JUROS: TBCDField;
    qryAudLancVR_TOTAL_DESCONTO: TBCDField;
    qryAudLancVR_TOTAL_PAGO: TBCDField;
    qryAudLancDATA_EDICAO: TSQLTimeStampField;
    qryAudLancFLG_STATUS: TStringField;
    qryAudLancFLG_CANCELADO: TStringField;
    qryAudLancFLG_RECORRENTE: TStringField;
    qryAudLancNUM_RECIBO: TIntegerField;
    qryAudLancID_CARNELEAO_EQUIVALENCIA_FK: TIntegerField;
    qryAudLancDESCR_CARNELEAO_CLASSIFICACAO: TStringField;
    qryAudParcID_USUARIO: TIntegerField;
    qryAudParcCODIGO_LAN: TIntegerField;
    qryAudParcANO_LAN: TIntegerField;
    qryAudParcTIPO_ACAO: TStringField;
    qryAudParcTP_LANCAMENTO: TStringField;
    qryAudParcTP_ACAO: TStringField;
    qryAudParcNOME_USUARIO: TStringField;
    qryAudParcNOME_FUNCIONARIO: TStringField;
    qryAudParcID_LANCAMENTO_PARC_FK: TIntegerField;
    qryAudParcDATA_EDICAO: TDateField;
    qryAudParcNUM_PARCELA: TIntegerField;
    qryAudParcDATA_VENCIMENTO: TDateField;
    qryAudParcVR_PARCELA_PREV: TBCDField;
    qryAudParcVR_PARCELA_JUROS: TBCDField;
    qryAudParcVR_PARCELA_DESCONTO: TBCDField;
    qryAudParcVR_PARCELA_PAGO: TBCDField;
    qryAudParcID_FORMAPAGAMENTO_FK: TIntegerField;
    qryAudParcDESCR_FORMAPAGAMENTO: TStringField;
    qryAudParcAGENCIA: TStringField;
    qryAudParcCONTA: TStringField;
    qryAudParcID_BANCO_FK: TIntegerField;
    qryAudParcDESCR_BANCO: TStringField;
    qryAudParcNUM_CHEQUE: TStringField;
    qryAudParcNUM_COD_DEPOSITO: TStringField;
    qryAudParcFLG_STATUS: TStringField;
    qryAudDepoID_USUARIO: TIntegerField;
    qryAudDepoTIPO_ACAO: TStringField;
    qryAudDepoTP_ACAO: TStringField;
    qryAudDepoNOME_USUARIO: TStringField;
    qryAudDepoNOME_FUNCIONARIO: TStringField;
    qryAudDepoDATA_EDICAO: TSQLTimeStampField;
    qryAudDepoID_DEPOSITO_FLUTUANTE_FK: TIntegerField;
    qryAudDepoDESCR_DEPOSITO_FLUTUANTE: TStringField;
    qryAudDepoDATA_DEPOSITO: TDateField;
    qryAudDepoNUM_COD_DEPOSITO: TStringField;
    qryAudDepoVR_DEPOSITO_FLUTUANTE: TBCDField;
    qryAudDepoFLG_STATUS: TStringField;
    lblPeriodo: TLabel;
    dteIniPeriodo: TJvDateEdit;
    Label3: TLabel;
    dteFimPeriodo: TJvDateEdit;
    procedure btnLimparClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure lcbUsuarioKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure dbgGridAudLancDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgGridAudParcDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgGridAudDepoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure chbLogGeralKeyPress(Sender: TObject; var Key: Char);
    procedure chbAudLancamentoKeyPress(Sender: TObject; var Key: Char);
    procedure chbAudParcelaKeyPress(Sender: TObject; var Key: Char);
    procedure chbAudDepositoKeyPress(Sender: TObject; var Key: Char);
    procedure chbAudDepositoClick(Sender: TObject);
    procedure chbAudParcelaClick(Sender: TObject);
    procedure chbAudLancamentoClick(Sender: TObject);
    procedure chbLogGeralClick(Sender: TObject);
    procedure dbgGridPaiPadraoTitleClick(Column: TColumn);
    procedure dbgGridAudLancTitleClick(Column: TColumn);
    procedure dbgGridAudParcTitleClick(Column: TColumn);
    procedure dbgGridAudDepoTitleClick(Column: TColumn);
    procedure ppmImpLogGeralClick(Sender: TObject);
    procedure ppmImpAudLancamentoClick(Sender: TObject);
    procedure ppmImpAudParcLancamentoClick(Sender: TObject);
    procedure ppmImpAudDepositoClick(Sender: TObject);
    procedure dteIniPeriodoKeyPress(Sender: TObject; var Key: Char);
    procedure dteFimPeriodoKeyPress(Sender: TObject; var Key: Char);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }

    procedure GerarRelatorio(Tipo: String);
  public
    { Public declarations }
  end;

var
  FFiltroLogUsuario: TFFiltroLogUsuario;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMRelatorios;

{ TFFiltroLogUsuario }

procedure TFFiltroLogUsuario.btnFiltrarClick(Sender: TObject);
begin
  if (dteIniPeriodo.Date = 0) and
    (dteFimPeriodo.Date = 0) then
  begin
    Application.MessageBox('Por favor, preencha o Per�odo.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
    Exit;
  end
  else if dteFimPeriodo.Date < dteIniPeriodo.Date then
  begin
    Application.MessageBox('A Data Final do Per�odo n�o pode ser inferior � Data Inicial do mesmo.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
    Exit;
  end;

  //LOG GERAL
  qryLogGeral.Close;

  if chbLogGeral.Checked then
  begin
    if Trim(lcbUsuario.Text) = '' then
    begin
      qryLogGeral.Params.ParamByName('ID_USU01').Value := 0;
      qryLogGeral.Params.ParamByName('ID_USU02').Value := 0;
    end
    else
    begin
      qryLogGeral.Params.ParamByName('ID_USU01').Value := lcbUsuario.KeyValue;
      qryLogGeral.Params.ParamByName('ID_USU02').Value := lcbUsuario.KeyValue;
    end;

    qryLogGeral.Params.ParamByName('DT_INI').Value := FormatDateTime('YYYY-MM-DD', dteIniPeriodo.Date) + ' 00:00:00';
    qryLogGeral.Params.ParamByName('DT_FIM').Value := FormatDateTime('YYYY-MM-DD', dteFimPeriodo.Date) + ' 23:59:59';

    qryLogGeral.Open;
  end;

  //AUDITORIA DE LANCAMENTO
  qryAudLanc.Close;

  if chbAudLancamento.Checked then
  begin
    if Trim(lcbUsuario.Text) = '' then
    begin
      qryAudLanc.Params.ParamByName('ID_USU01').Value := 0;
      qryAudLanc.Params.ParamByName('ID_USU02').Value := 0;
    end
    else
    begin
      qryAudLanc.Params.ParamByName('ID_USU01').Value := lcbUsuario.KeyValue;
      qryAudLanc.Params.ParamByName('ID_USU02').Value := lcbUsuario.KeyValue;
    end;

    qryAudLanc.Params.ParamByName('DT_INI').Value := FormatDateTime('YYYY-MM-DD', dteIniPeriodo.Date) + ' 00:00:00';
    qryAudLanc.Params.ParamByName('DT_FIM').Value := FormatDateTime('YYYY-MM-DD', dteFimPeriodo.Date) + ' 23:59:59';

    qryAudLanc.Open;
  end;

  //AUDITORIA DE PARCELA
  qryAudParc.Close;

  if chbAudParcela.Checked then
  begin
    if Trim(lcbUsuario.Text) = '' then
    begin
      qryAudParc.Params.ParamByName('ID_USU01').Value := 0;
      qryAudParc.Params.ParamByName('ID_USU02').Value := 0;
    end
    else
    begin
      qryAudParc.Params.ParamByName('ID_USU01').Value := lcbUsuario.KeyValue;
      qryAudParc.Params.ParamByName('ID_USU02').Value := lcbUsuario.KeyValue;
    end;

    qryAudParc.Params.ParamByName('DT_INI').Value := FormatDateTime('YYYY-MM-DD', dteIniPeriodo.Date) + ' 00:00:00';
    qryAudParc.Params.ParamByName('DT_FIM').Value := FormatDateTime('YYYY-MM-DD', dteFimPeriodo.Date) + ' 23:59:59';

    qryAudParc.Open;
  end;

  //AUDITORIA DE DEPOSITO
  qryAudDepo.Close;

  if chbAudDeposito.Checked then
  begin
    if Trim(lcbUsuario.Text) = '' then
    begin
      qryAudDepo.Params.ParamByName('ID_USU01').Value := 0;
      qryAudDepo.Params.ParamByName('ID_USU02').Value := 0;
    end
    else
    begin
      qryAudDepo.Params.ParamByName('ID_USU01').Value := lcbUsuario.KeyValue;
      qryAudDepo.Params.ParamByName('ID_USU02').Value := lcbUsuario.KeyValue;
    end;

    qryAudDepo.Params.ParamByName('DT_INI').Value := FormatDateTime('YYYY-MM-DD', dteIniPeriodo.Date) + ' 00:00:00';
    qryAudDepo.Params.ParamByName('DT_FIM').Value := FormatDateTime('YYYY-MM-DD', dteFimPeriodo.Date) + ' 23:59:59';

    qryAudDepo.Open;
  end;

  inherited;
end;

procedure TFFiltroLogUsuario.btnLimparClick(Sender: TObject);
begin
  inherited;

  dteIniPeriodo.Date := StartOfTheMonth(Date);
  dteFimPeriodo.Date := EndOfTheMonth(Date);

  lcbUsuario.KeyValue := -1;

  chbLogGeral.Checked      := True;
  chbAudLancamento.Checked := True;
  chbAudParcela.Checked    := True;
  chbAudDeposito.Checked   := True;

  btnFiltrar.Click;

  if lcbUsuario.CanFocus then
    lcbUsuario.SetFocus;
end;

procedure TFFiltroLogUsuario.chbAudDepositoClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroLogUsuario.chbAudDepositoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLogUsuario.chbAudLancamentoClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroLogUsuario.chbAudLancamentoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLogUsuario.chbAudParcelaClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroLogUsuario.chbAudParcelaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLogUsuario.chbLogGeralClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroLogUsuario.chbLogGeralKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLogUsuario.dbgGridAudDepoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;

  AlternarCorGrid(Sender, Rect, DataCol, Column, State);
end;

procedure TFFiltroLogUsuario.dbgGridAudDepoTitleClick(Column: TColumn);
begin
  inherited;

  qryAudDepo.IndexFieldNames := Column.FieldName;
end;

procedure TFFiltroLogUsuario.dbgGridAudLancDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;

  AlternarCorGrid(Sender, Rect, DataCol, Column, State);
end;

procedure TFFiltroLogUsuario.dbgGridAudLancTitleClick(Column: TColumn);
begin
  inherited;

  qryAudLanc.IndexFieldNames := Column.FieldName;
end;

procedure TFFiltroLogUsuario.dbgGridAudParcDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;

  AlternarCorGrid(Sender, Rect, DataCol, Column, State);
end;

procedure TFFiltroLogUsuario.dbgGridAudParcTitleClick(Column: TColumn);
begin
  inherited;

  qryAudParc.IndexFieldNames := Column.FieldName;
end;

procedure TFFiltroLogUsuario.dbgGridPaiPadraoDblClick(Sender: TObject);
begin
  //inherited;
end;

procedure TFFiltroLogUsuario.dbgGridPaiPadraoTitleClick(Column: TColumn);
begin
  inherited;

  qryLogGeral.IndexFieldNames := Column.FieldName;
end;

procedure TFFiltroLogUsuario.dteFimPeriodoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLogUsuario.dteIniPeriodoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLogUsuario.FormShow(Sender: TObject);
begin
  btnIncluir.Visible := False;
  btnEditar.Visible  := False;
  btnExcluir.Visible := False;

  qryUsuario.Close;
  qryUsuario.Open;

  dteIniPeriodo.Date := StartOfTheMonth(Date);
  dteFimPeriodo.Date := EndOfTheMonth(Date);

  lcbUsuario.KeyValue := -1;

  chbLogGeral.Checked      := True;
  chbAudLancamento.Checked := True;
  chbAudParcela.Checked    := True;
  chbAudDeposito.Checked   := True;

  btnFiltrar.Click;

  ShowScrollBar(dbgGridPaiPadrao.Handle, SB_HORZ, False);
  ShowScrollBar(dbgGridAudLanc.Handle, SB_HORZ, False);
  ShowScrollBar(dbgGridAudParc.Handle, SB_HORZ, False);
  ShowScrollBar(dbgGridAudDepo.Handle, SB_HORZ, False);

  inherited;

  if lcbUsuario.CanFocus then
    lcbUsuario.SetFocus;
end;

procedure TFFiltroLogUsuario.GerarRelatorio(Tipo: String);
var
  sDetalhe: String;
begin
  vgOperacao := P;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmRelatorios, dmRelatorios);

  dmRelatorios.qryRel15_LogG.Close;

  if Trim(lcbUsuario.Text) = '' then
  begin
    dmRelatorios.qryRel15_LogG.Params.ParamByName('ID_USU11').Value := 0;
    dmRelatorios.qryRel15_LogG.Params.ParamByName('ID_USU12').Value := 0;
    dmRelatorios.qryRel15_LogG.Params.ParamByName('ID_USU21').Value := 0;
    dmRelatorios.qryRel15_LogG.Params.ParamByName('ID_USU22').Value := 0;
    dmRelatorios.qryRel15_LogG.Params.ParamByName('ID_USU31').Value := 0;
    dmRelatorios.qryRel15_LogG.Params.ParamByName('ID_USU32').Value := 0;
    dmRelatorios.qryRel15_LogG.Params.ParamByName('ID_USU41').Value := 0;
    dmRelatorios.qryRel15_LogG.Params.ParamByName('ID_USU42').Value := 0;
  end
  else
  begin
    dmRelatorios.qryRel15_LogG.Params.ParamByName('ID_USU11').Value := lcbUsuario.KeyValue;
    dmRelatorios.qryRel15_LogG.Params.ParamByName('ID_USU12').Value := lcbUsuario.KeyValue;
    dmRelatorios.qryRel15_LogG.Params.ParamByName('ID_USU21').Value := lcbUsuario.KeyValue;
    dmRelatorios.qryRel15_LogG.Params.ParamByName('ID_USU22').Value := lcbUsuario.KeyValue;
    dmRelatorios.qryRel15_LogG.Params.ParamByName('ID_USU31').Value := lcbUsuario.KeyValue;
    dmRelatorios.qryRel15_LogG.Params.ParamByName('ID_USU32').Value := lcbUsuario.KeyValue;
    dmRelatorios.qryRel15_LogG.Params.ParamByName('ID_USU41').Value := lcbUsuario.KeyValue;
    dmRelatorios.qryRel15_LogG.Params.ParamByName('ID_USU42').Value := lcbUsuario.KeyValue;
  end;

  dmRelatorios.qryRel15_LogG.Params.ParamByName('DT_INI1').Value := FormatDateTime('YYYY-MM-DD', dteIniPeriodo.Date) + ' 00:00:00';
  dmRelatorios.qryRel15_LogG.Params.ParamByName('DT_FIM1').Value := FormatDateTime('YYYY-MM-DD', dteFimPeriodo.Date) + ' 23:59:59';
  dmRelatorios.qryRel15_LogG.Params.ParamByName('DT_INI2').Value := FormatDateTime('YYYY-MM-DD', dteIniPeriodo.Date) + ' 00:00:00';
  dmRelatorios.qryRel15_LogG.Params.ParamByName('DT_FIM2').Value := FormatDateTime('YYYY-MM-DD', dteFimPeriodo.Date) + ' 23:59:59';
  dmRelatorios.qryRel15_LogG.Params.ParamByName('DT_INI3').Value := FormatDateTime('YYYY-MM-DD', dteIniPeriodo.Date) + ' 00:00:00';
  dmRelatorios.qryRel15_LogG.Params.ParamByName('DT_FIM3').Value := FormatDateTime('YYYY-MM-DD', dteFimPeriodo.Date) + ' 23:59:59';
  dmRelatorios.qryRel15_LogG.Params.ParamByName('DT_INI4').Value := FormatDateTime('YYYY-MM-DD', dteIniPeriodo.Date) + ' 00:00:00';
  dmRelatorios.qryRel15_LogG.Params.ParamByName('DT_FIM4').Value := FormatDateTime('YYYY-MM-DD', dteFimPeriodo.Date) + ' 23:59:59';

  dmRelatorios.qryRel15_LogG.Params.ParamByName('EXIBIR1').Value := IfThen(Trim(Tipo) = 'G', 1, 0);
  dmRelatorios.qryRel15_LogG.Params.ParamByName('EXIBIR2').Value := IfThen(Trim(Tipo) = 'L', 1, 0);
  dmRelatorios.qryRel15_LogG.Params.ParamByName('EXIBIR3').Value := IfThen(Trim(Tipo) = 'P', 1, 0);
  dmRelatorios.qryRel15_LogG.Params.ParamByName('EXIBIR4').Value := IfThen(Trim(Tipo) = 'D', 1, 0);
  dmRelatorios.qryRel15_LogG.Open;

  if dmRelatorios.qryRel15_LogG.RecordCount = 0 then
    Application.MessageBox('N�o h� registros para o filtro realizado.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING)
  else
  begin
    dmRelatorios.qryRel15_LogG.First;

    while not dmRelatorios.qryRel15_LogG.Eof do
    begin
      sDetalhe := '';

      dmRelatorios.qryRel15_LogG.Edit;

      case dmRelatorios.qryRel15_LogG.FieldByName('TIPO_LOGAUD').AsInteger of
        1:  //LOG
        begin
          if Trim(dmRelatorios.qryRel15_LogG.FieldByName('TABELA_ORIGEM').AsString) <> '' then
            sDetalhe := 'TABELA: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('TABELA_ORIGEM').AsString);

          if Trim(dmRelatorios.qryRel15_LogG.FieldByName('CODIGO_LAN').AsString) <> '' then
          begin
            if Trim(sDetalhe) = '' then
              sDetalhe := 'LAN�AMENTO: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('CODIGO_LAN').AsString) + '/' +
                                           Trim(dmRelatorios.qryRel15_LogG.FieldByName('ANO_LAN').AsString)
            else
              sDetalhe := sDetalhe + #13#10 +
                          'LAN�AMENTO: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('CODIGO_LAN').AsString) + '/' +
                                           Trim(dmRelatorios.qryRel15_LogG.FieldByName('ANO_LAN').AsString);

            if Trim(dmRelatorios.qryRel15_LogG.FieldByName('ID_ORIGEM').AsString) <> '' then
              sDetalhe := sDetalhe + '(ID DO REGISTRO: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('ID_ORIGEM').AsString) + ')';
          end
          else if Trim(dmRelatorios.qryRel15_LogG.FieldByName('ID_ORIGEM').AsString) <> '' then
          begin
            if Trim(sDetalhe) = '' then
              sDetalhe := 'ID DO REGISTRO: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('ID_ORIGEM').AsString)
            else
              sDetalhe := sDetalhe + #13#10 +
                          'ID DO REGISTRO: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('ID_ORIGEM').AsString);
          end;

          if Trim(dmRelatorios.qryRel15_LogG.FieldByName('OBS_USUARIO_LOG').AsString) <> '' then
          begin
            if Trim(sDetalhe) = '' then
              sDetalhe := 'OBS.: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('OBS_USUARIO_LOG').AsString)
            else
              sDetalhe := sDetalhe + #13#10 +
                          'OBS.: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('OBS_USUARIO_LOG').AsString);
          end;

          dmRelatorios.qryRel15_LogG.FieldByName('DETALHE_LOGAUD').AsString := Trim(sDetalhe);
        end;
        2:  //AUDITORIA DE LANCAMENTO
        begin
          sDetalhe := 'LAN�AMENTO: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('CODIGO_LAN').AsString) + '/' +
                                       Trim(dmRelatorios.qryRel15_LogG.FieldByName('ANO_LAN').AsString);

          if Trim(dmRelatorios.qryRel15_LogG.FieldByName('NUM_RECIBO').AsString) = 'S' then
            sDetalhe := sDetalhe + ' (RECIBO: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('NUM_RECIBO').AsString) + ')';

          if Trim(dmRelatorios.qryRel15_LogG.FieldByName('FLG_CANCELADO').AsString) = 'S' then
            sDetalhe := sDetalhe + ' - Cancelado'
          else
          begin
            if Trim(dmRelatorios.qryRel15_LogG.FieldByName('FLG_STATUS_LANC').AsString) = 'P' then
              sDetalhe := sDetalhe + ' - Pendente'
            else if Trim(dmRelatorios.qryRel15_LogG.FieldByName('FLG_STATUS_LANC').AsString) = 'G' then
              sDetalhe := sDetalhe + ' - Pago';
          end;

          sDetalhe := sDetalhe + #13#10 +
                      'NATUREZA: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('DESCR_NATUREZA').AsString);

          if Trim(dmRelatorios.qryRel15_LogG.FieldByName('DESCR_CATEGORIA_DESPREC').AsString) <> '' then
            sDetalhe := sDetalhe + #13#10 +
                        'CATEGORIA: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('DESCR_CATEGORIA_DESPREC').AsString);

          if Trim(dmRelatorios.qryRel15_LogG.FieldByName('DESCR_SUBCATEGORIA_DESPREC').AsString) <> '' then
            sDetalhe := sDetalhe + #13#10 +
                        'SUBCATEGORIA: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('DESCR_SUBCATEGORIA_DESPREC').AsString);

          if Trim(dmRelatorios.qryRel15_LogG.FieldByName('NOME_FANTASIA').AsString) <> '' then
            sDetalhe := sDetalhe + #13#10 +
                        'CLIENTE/FORNECEDOR: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('NOME_FANTASIA').AsString);

          sDetalhe := sDetalhe + #13#10 +
                      'IMPOSTO DE RENDA: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('FLG_IMPOSTORENDA').AsString) +
                      ' CONTA PESSOAL: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('FLG_PESSOAL').AsString) +
                      ' LIVRO AUXILIAR: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('FLG_AUXILIAR').AsString) +
                      ' FLUTUANTE: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('FLG_FLUTUANTE').AsString) +
                      ' RECORRENTE: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('FLG_RECORRENTE').AsString);

          sDetalhe := sDetalhe + #13#10 +
                      'QTD. PARCELAS: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('QTD_PARCELAS').AsString) +
                      ' TOT. PREVISTO: ' + FloatToStr(dmRelatorios.qryRel15_LogG.FieldByName('VR_TOTAL_PREV').AsFloat) +
                      ' TOT. JUROS: ' + FloatToStr(dmRelatorios.qryRel15_LogG.FieldByName('VR_TOTAL_JUROS').AsFloat) +
                      ' TOT. DESCONTO: ' + FloatToStr(dmRelatorios.qryRel15_LogG.FieldByName('VR_TOTAL_DESCONTO').AsFloat) +
                      ' TOT. PAGO: ' + FloatToStr(dmRelatorios.qryRel15_LogG.FieldByName('VR_TOTAL_PAGO').AsFloat);
                                             
          if Trim(dmRelatorios.qryRel15_LogG.FieldByName('OBS_USUARIO_LOG').AsString) <> '' then
            sDetalhe := sDetalhe + #13#10 +
                        'CARN�-LE�O: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('DESCR_CARNELEAO_CLASSIFICACAO').AsString);

          if Trim(dmRelatorios.qryRel15_LogG.FieldByName('OBS_USUARIO_LOG').AsString) <> '' then
            sDetalhe := sDetalhe + #13#10 +
                        'OBS.: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('OBS_USUARIO_LOG').AsString);

          dmRelatorios.qryRel15_LogG.FieldByName('DETALHE_LOGAUD').AsString := Trim(sDetalhe);
        end;
        3:  //AUDITORIA DE PARCELA
        begin
          sDetalhe := 'LAN�AMENTO: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('CODIGO_LAN').AsString) + '/' +
                                       Trim(dmRelatorios.qryRel15_LogG.FieldByName('ANO_LAN').AsString);

          if Trim(dmRelatorios.qryRel15_LogG.FieldByName('NUM_RECIBO').AsString) = 'S' then
            sDetalhe := sDetalhe + ' (RECIBO: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('NUM_RECIBO').AsString) + ')';

          case AnsiIndexStr(UpperCase(dmRelatorios.qryRel15_LogG.FieldByName('FLG_STATUS_PARC').AsString),
                            ['P', 'G', 'C']) of
            0: sDetalhe := sDetalhe + ' - Pendente';
            1: sDetalhe := sDetalhe + ' - Pago';
            2: sDetalhe := sDetalhe + ' - Cancelado';
          end;

          sDetalhe := sDetalhe + #13#10 +
                      'N� PARCELA: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('NUM_PARCELA').AsString) +
                      ' DT. VENCIMENTO: ' + DateToStr(dmRelatorios.qryRel15_LogG.FieldByName('DATA_VENCIMENTO').AsDateTime) + 
                      ' TOT. PREVISTO: ' + FloatToStr(dmRelatorios.qryRel15_LogG.FieldByName('VR_PARCELA_PREV').AsFloat) +
                      ' TOT. JUROS: ' + FloatToStr(dmRelatorios.qryRel15_LogG.FieldByName('VR_PARCELA_JUROS').AsFloat) +
                      ' TOT. DESCONTO: ' + FloatToStr(dmRelatorios.qryRel15_LogG.FieldByName('VR_PARCELA_DESCONTO').AsFloat) +
                      ' TOT. PAGO: ' + FloatToStr(dmRelatorios.qryRel15_LogG.FieldByName('VR_PARCELA_PAGO').AsFloat) +
                      ' F. PAGAMENTO: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('DESCR_FORMAPAGAMENTO').AsString);
                                             
          if Trim(dmRelatorios.qryRel15_LogG.FieldByName('NUM_COD_DEPOSITO_PARC').AsString) <> '' then
            sDetalhe := sDetalhe + #13#10 +
                        'IDENTIFICA��O DE DEP�SITO: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('NUM_COD_DEPOSITO_PARC').AsString)
          else if Trim(dmRelatorios.qryRel15_LogG.FieldByName('NUM_CHEQUE').AsString) <> '' then
          begin
            sDetalhe := sDetalhe + #13#10 +
                        'AG�NCIA: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('AGENCIA').AsString) +
                        ' CONTA: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('CONTA').AsString) +
                        ' BANCO: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('DESCR_BANCO').AsString) +
                        ' N� CHEQUE: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('NUM_CHEQUE').AsString);
          end;

          if Trim(dmRelatorios.qryRel15_LogG.FieldByName('OBS_USUARIO_LOG').AsString) <> '' then
            sDetalhe := sDetalhe + #13#10 +
                        'OBS.: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('OBS_USUARIO_LOG').AsString);
                                
          dmRelatorios.qryRel15_LogG.FieldByName('DETALHE_LOGAUD').AsString := Trim(sDetalhe);
        end;
        4:  //AUDITORIA DE DEPOSITO
        begin                            
          sDetalhe := 'DATA: ' + DateToStr(dmRelatorios.qryRel15_LogG.FieldByName('DATA_DEPOSITO').AsDateTime) + #13#10 +
                      'IDENTIFICA��O DE DEP�SITO: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('NUM_COD_DEPOSITO').AsString) + #13#10 +
                      'VALOR: ' + FloatToStr(dmRelatorios.qryRel15_LogG.FieldByName('VR_DEPOSITO_FLUTUANTE').AsFloat) + #13#10 +
                      'SITUA��O: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('FLG_STATUS_DEPO').AsString);        

          if Trim(dmRelatorios.qryRel15_LogG.FieldByName('DESCR_DEPOSITO_FLUTUANTE').AsString) <> '' then
            sDetalhe := sDetalhe + #13#10 +
                        'DESCRI��O: ' + Trim(dmRelatorios.qryRel15_LogG.FieldByName('DESCR_DEPOSITO_FLUTUANTE').AsString);
        
          dmRelatorios.qryRel15_LogG.FieldByName('DETALHE_LOGAUD').AsString := Trim(sDetalhe);
        end;
      end;

      dmRelatorios.qryRel15_LogG.Post;

      dmRelatorios.qryRel15_LogG.Next;
    end;

    try
      sTituloRelatorio    := 'LOG DE USU�RIOS E AUTITORIA DE TABELAS';
      sSubtituloRelatorio := 'Per�odo de ' + DateToStr(dteIniPeriodo.Date) + ' a ' + DateToStr(dteFimPeriodo.Date);

      lExibeDetalhe := False;
      dmRelatorios.DefinirRelatorio(R15);

      dmRelatorios.ImprimirRelatorio(False, False);

      GravarLog;
    except
      Application.MessageBox('Erro ao gerar relat�rio de Log/Auditoria.',
                             'Erro',
                             MB_OK + MB_ICONERROR);
    end;
  end;

  FreeAndNil(dmRelatorios);
end;

procedure TFFiltroLogUsuario.GravarLog;
begin
  //inherited;
end;

procedure TFFiltroLogUsuario.lcbUsuarioKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

function TFFiltroLogUsuario.PodeExcluir(var Msg: String): Boolean;
begin
  //
end;

procedure TFFiltroLogUsuario.ppmImpAudDepositoClick(Sender: TObject);
begin
  inherited;

  GerarRelatorio('D');
end;

procedure TFFiltroLogUsuario.ppmImpAudLancamentoClick(Sender: TObject);
begin
  inherited;

  GerarRelatorio('L');
end;

procedure TFFiltroLogUsuario.ppmImpAudParcLancamentoClick(Sender: TObject);
begin
  inherited;

  GerarRelatorio('P');
end;

procedure TFFiltroLogUsuario.ppmImpLogGeralClick(Sender: TObject);
begin
  inherited;

  GerarRelatorio('G');
end;

procedure TFFiltroLogUsuario.VerificarPermissoes;
begin
  inherited;

  btnImprimir.Enabled := vgPrm_ImpLogUsu and
                         ((qryLogGeral.RecordCount > 0) or
                          (qryAudLanc.RecordCount > 0) or
                          (qryAudParc.RecordCount > 0) or
                          (qryAudDepo.RecordCount > 0));
end;

end.
