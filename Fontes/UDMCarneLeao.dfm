object dmCarneLeao: TdmCarneLeao
  OldCreateOrder = False
  Height = 440
  Width = 430
  object qryIdent_F: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM CARNELEAO_IDENTIFICACAO'
      ' WHERE ID_CARNELEAO_IDENTIFICACAO = :ID_CARNELEAO_IDENTIFICACAO')
    Left = 24
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'ID_CARNELEAO_IDENTIFICACAO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object qryPlCont_F: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '   FROM CARNELEAO_PLANOCONTAS'
      
        ' WHERE ID_CARNELEAO_IDENTIFICACAO_FK = :ID_CARNELEAO_IDENTIFICAC' +
        'AO'
      'ORDER BY IDENTIFICADOR, ID_CARNELEAO_PLANOCONTAS')
    Left = 168
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'ID_CARNELEAO_IDENTIFICACAO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object qryEscrit_F: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM CARNELEAO_ESCRITURACAO'
      
        ' WHERE ID_CARNELEAO_IDENTIFICACAO_FK = :ID_CARNELEAO_IDENTIFICAC' +
        'AO'
      'ORDER BY IDENTIFICADOR, ID_CARNELEAO_ESCRITURACAO')
    Left = 96
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'ID_CARNELEAO_IDENTIFICACAO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object qryRegis_F: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM CARNELEAO_REGISTRO'
      
        ' WHERE ID_CARNELEAO_IDENTIFICACAO_FK = :ID_CARNELEAO_IDENTIFICAC' +
        'AO'
      'ORDER BY IDENTIFICADOR, ID_CARNELEAO_REGISTRO')
    Left = 240
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'ID_CARNELEAO_IDENTIFICACAO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object qryCtrl_F: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM CARNELEAO_CONTROLE'
      
        ' WHERE ID_CARNELEAO_IDENTIFICACAO_FK = :ID_CARNELEAO_IDENTIFICAC' +
        'AO'
      'ORDER BY IDENTIFICADOR, ID_CARNELEAO_CONTROLE')
    Left = 312
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'ID_CARNELEAO_IDENTIFICACAO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspIdent_F: TDataSetProvider
    DataSet = qryIdent_F
    Left = 24
    Top = 64
  end
  object dspPlCont_F: TDataSetProvider
    DataSet = qryPlCont_F
    Left = 168
    Top = 64
  end
  object dspEscrit_F: TDataSetProvider
    DataSet = qryEscrit_F
    Left = 96
    Top = 64
  end
  object dspRegis_F: TDataSetProvider
    DataSet = qryRegis_F
    Left = 240
    Top = 64
  end
  object dspCtrl_F: TDataSetProvider
    DataSet = qryCtrl_F
    Left = 312
    Top = 64
  end
  object cdsIdent_F: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_CARNELEAO_IDENTIFICACAO'
        ParamType = ptInput
      end>
    ProviderName = 'dspIdent_F'
    Left = 24
    Top = 112
    object cdsIdent_FID_CARNELEAO_IDENTIFICACAO: TIntegerField
      FieldName = 'ID_CARNELEAO_IDENTIFICACAO'
      Required = True
    end
    object cdsIdent_FTIPO_ENVIO: TStringField
      FieldName = 'TIPO_ENVIO'
      FixedChar = True
      Size = 1
    end
    object cdsIdent_FIDENTIFICADOR: TStringField
      FieldName = 'IDENTIFICADOR'
      FixedChar = True
      Size = 2
    end
    object cdsIdent_FSISTEMA_GERADOR: TStringField
      FieldName = 'SISTEMA_GERADOR'
      Size = 14
    end
    object cdsIdent_FANO_DECLARACAO: TIntegerField
      FieldName = 'ANO_DECLARACAO'
    end
    object cdsIdent_FCPF_DECLARANTE: TStringField
      FieldName = 'CPF_DECLARANTE'
      OnGetText = cdsIdent_FCPF_DECLARANTEGetText
      FixedChar = True
      Size = 11
    end
    object cdsIdent_FNOME_DECLARANTE: TStringField
      FieldName = 'NOME_DECLARANTE'
      Size = 60
    end
    object cdsIdent_FATIVACAO_LIVROCAIXA: TStringField
      FieldName = 'ATIVACAO_LIVROCAIXA'
      FixedChar = True
      Size = 1
    end
    object cdsIdent_FCOD_ORIGEM: TIntegerField
      FieldName = 'COD_ORIGEM'
    end
    object cdsIdent_FLOGRADOURO: TStringField
      FieldName = 'LOGRADOURO'
      Size = 40
    end
    object cdsIdent_FNUM_LOGRADOURO: TStringField
      FieldName = 'NUM_LOGRADOURO'
      Size = 6
    end
    object cdsIdent_FCOMPL_LOGRADOURO: TStringField
      FieldName = 'COMPL_LOGRADOURO'
    end
    object cdsIdent_FBAIRRO_LOGRADOURO: TStringField
      FieldName = 'BAIRRO_LOGRADOURO'
    end
    object cdsIdent_FMUNIC_LOGRADOURO: TStringField
      FieldName = 'MUNIC_LOGRADOURO'
    end
    object cdsIdent_FTELEFONE: TStringField
      FieldName = 'TELEFONE'
      Size = 12
    end
    object cdsIdent_FSIGLA_UF: TStringField
      FieldName = 'SIGLA_UF'
      FixedChar = True
      Size = 2
    end
    object cdsIdent_FCEP: TStringField
      FieldName = 'CEP'
      FixedChar = True
      Size = 8
    end
    object cdsIdent_FDATA_GERACAO: TSQLTimeStampField
      FieldName = 'DATA_GERACAO'
    end
    object cdsIdent_FGER_ID_USUARIO: TIntegerField
      FieldName = 'GER_ID_USUARIO'
    end
    object cdsIdent_FFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsIdent_FDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsIdent_FCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsIdent_FNOME_ARQUIVO: TStringField
      FieldName = 'NOME_ARQUIVO'
      Size = 100
    end
    object cdsIdent_FEXTENSAO_ARQUIVO: TStringField
      FieldName = 'EXTENSAO_ARQUIVO'
      Size = 4
    end
  end
  object cdsPlCont_F: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_CARNELEAO_IDENTIFICACAO'
        ParamType = ptInput
      end>
    ProviderName = 'dspPlCont_F'
    Left = 168
    Top = 112
    object cdsPlCont_FID_CARNELEAO_PLANOCONTAS: TIntegerField
      FieldName = 'ID_CARNELEAO_PLANOCONTAS'
      Required = True
    end
    object cdsPlCont_FID_CARNELEAO_IDENTIFICACAO_FK: TIntegerField
      FieldName = 'ID_CARNELEAO_IDENTIFICACAO_FK'
    end
    object cdsPlCont_FIDENTIFICADOR: TStringField
      FieldName = 'IDENTIFICADOR'
      FixedChar = True
      Size = 2
    end
    object cdsPlCont_FCPF_DECLARANTE: TStringField
      FieldName = 'CPF_DECLARANTE'
      FixedChar = True
      Size = 11
    end
    object cdsPlCont_FCOD_CONTA: TStringField
      FieldName = 'COD_CONTA'
      Size = 5
    end
    object cdsPlCont_FDESCR_CONTA: TStringField
      FieldName = 'DESCR_CONTA'
      Size = 80
    end
    object cdsPlCont_FFLG_SELECIONAVEL: TStringField
      FieldName = 'FLG_SELECIONAVEL'
      FixedChar = True
      Size = 1
    end
  end
  object cdsEscrit_F: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_CARNELEAO_IDENTIFICACAO'
        ParamType = ptInput
      end>
    ProviderName = 'dspEscrit_F'
    Left = 96
    Top = 112
    object cdsEscrit_FID_CARNELEAO_ESCRITURACAO: TIntegerField
      FieldName = 'ID_CARNELEAO_ESCRITURACAO'
      Required = True
    end
    object cdsEscrit_FID_CARNELEAO_IDENTIFICACAO_FK: TIntegerField
      FieldName = 'ID_CARNELEAO_IDENTIFICACAO_FK'
    end
    object cdsEscrit_FIDENTIFICADOR: TStringField
      FieldName = 'IDENTIFICADOR'
      FixedChar = True
      Size = 2
    end
    object cdsEscrit_FMES_ESCRITURACAO: TStringField
      FieldName = 'MES_ESCRITURACAO'
      FixedChar = True
      Size = 1
    end
    object cdsEscrit_FDATA_ESCRITURACAO: TStringField
      FieldName = 'DATA_ESCRITURACAO'
      FixedChar = True
      Size = 8
    end
    object cdsEscrit_FCOD_TIPO_CONTA: TStringField
      FieldName = 'COD_TIPO_CONTA'
      Size = 5
    end
    object cdsEscrit_FHISTORICO_CONTA: TStringField
      FieldName = 'HISTORICO_CONTA'
      Size = 255
    end
    object cdsEscrit_FVR_CONTA: TBCDField
      FieldName = 'VR_CONTA'
      OnGetText = cdsEscrit_FVR_CONTAGetText
      Precision = 18
      Size = 2
    end
  end
  object cdsRegis_F: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_CARNELEAO_IDENTIFICACAO'
        ParamType = ptInput
      end>
    ProviderName = 'dspRegis_F'
    Left = 240
    Top = 112
    object cdsRegis_FID_CARNELEAO_REGISTRO: TIntegerField
      FieldName = 'ID_CARNELEAO_REGISTRO'
      Required = True
    end
    object cdsRegis_FID_CARNELEAO_IDENTIFICACAO_FK: TIntegerField
      FieldName = 'ID_CARNELEAO_IDENTIFICACAO_FK'
    end
    object cdsRegis_FIDENTIFICADOR: TStringField
      FieldName = 'IDENTIFICADOR'
      FixedChar = True
      Size = 2
    end
    object cdsRegis_FMES_APURACAO: TStringField
      FieldName = 'MES_APURACAO'
      FixedChar = True
      Size = 1
    end
    object cdsRegis_FNOME_MES_APURACAO: TStringField
      FieldName = 'NOME_MES_APURACAO'
      Size = 15
    end
    object cdsRegis_FVR_TRAB_NAO_ASSALARIADO: TBCDField
      FieldName = 'VR_TRAB_NAO_ASSALARIADO'
      OnGetText = cdsRegis_FVR_TRAB_NAO_ASSALARIADOGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_ALUGUEIS: TBCDField
      FieldName = 'VR_ALUGUEIS'
      OnGetText = cdsRegis_FVR_ALUGUEISGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_OUTROS: TBCDField
      FieldName = 'VR_OUTROS'
      OnGetText = cdsRegis_FVR_OUTROSGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_EXTERIOR: TBCDField
      FieldName = 'VR_EXTERIOR'
      OnGetText = cdsRegis_FVR_EXTERIORGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_PREV_OFICIAL: TBCDField
      FieldName = 'VR_PREV_OFICIAL'
      OnGetText = cdsRegis_FVR_PREV_OFICIALGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_DEPENDENTES: TBCDField
      FieldName = 'VR_DEPENDENTES'
      OnGetText = cdsRegis_FVR_DEPENDENTESGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_PENSAO_ALIMENTICIA: TBCDField
      FieldName = 'VR_PENSAO_ALIMENTICIA'
      OnGetText = cdsRegis_FVR_PENSAO_ALIMENTICIAGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_LIVRO_CAIXA: TBCDField
      FieldName = 'VR_LIVRO_CAIXA'
      OnGetText = cdsRegis_FVR_LIVRO_CAIXAGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_IMP_EXTERIOR_ACOMP: TBCDField
      FieldName = 'VR_IMP_EXTERIOR_ACOMP'
      OnGetText = cdsRegis_FVR_IMP_EXTERIOR_ACOMPGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_IMP_APAGAR: TBCDField
      FieldName = 'VR_IMP_APAGAR'
      OnGetText = cdsRegis_FVR_IMP_APAGARGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_IMP_PAGO: TBCDField
      FieldName = 'VR_IMP_PAGO'
      OnGetText = cdsRegis_FVR_IMP_PAGOGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_BASE_CALCULO: TBCDField
      FieldName = 'VR_BASE_CALCULO'
      OnGetText = cdsRegis_FVR_BASE_CALCULOGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_DEDUCOES: TBCDField
      FieldName = 'VR_DEDUCOES'
      OnGetText = cdsRegis_FVR_DEDUCOESGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_REND_EXT_N_ASSAL: TBCDField
      FieldName = 'VR_REND_EXT_N_ASSAL'
      OnGetText = cdsRegis_FVR_REND_EXT_N_ASSALGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_REND_EXT_OUTROS: TBCDField
      FieldName = 'VR_REND_EXT_OUTROS'
      OnGetText = cdsRegis_FVR_REND_EXT_OUTROSGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_DEDUC_LIVRO_CAIXA_MES: TBCDField
      FieldName = 'VR_DEDUC_LIVRO_CAIXA_MES'
      OnGetText = cdsRegis_FVR_DEDUC_LIVRO_CAIXA_MESGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_EXCS_DEDUC_MES_ANT: TBCDField
      FieldName = 'VR_EXCS_DEDUC_MES_ANT'
      OnGetText = cdsRegis_FVR_EXCS_DEDUC_MES_ANTGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_TOTAL_DEDUC_MES: TBCDField
      FieldName = 'VR_TOTAL_DEDUC_MES'
      OnGetText = cdsRegis_FVR_TOTAL_DEDUC_MESGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_REND_PJ_TRAB_NAO_ASS: TBCDField
      FieldName = 'VR_REND_PJ_TRAB_NAO_ASS'
      OnGetText = cdsRegis_FVR_REND_PJ_TRAB_NAO_ASSGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_LIM_LEGAL_LIVRO_CX: TBCDField
      FieldName = 'VR_LIM_LEGAL_LIVRO_CX'
      OnGetText = cdsRegis_FVR_LIM_LEGAL_LIVRO_CXGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_DEDUC_LIVRO_CAIXA: TBCDField
      FieldName = 'VR_DEDUC_LIVRO_CAIXA'
      OnGetText = cdsRegis_FVR_DEDUC_LIVRO_CAIXAGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_EXCS_DEDUC_MES_SEG: TBCDField
      FieldName = 'VR_EXCS_DEDUC_MES_SEG'
      OnGetText = cdsRegis_FVR_EXCS_DEDUC_MES_SEGGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_CL_DEV_CREND_EXT: TBCDField
      FieldName = 'VR_CL_DEV_CREND_EXT'
      OnGetText = cdsRegis_FVR_CL_DEV_CREND_EXTGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_CL_DEV_SREND_EXT: TBCDField
      FieldName = 'VR_CL_DEV_SREND_EXT'
      OnGetText = cdsRegis_FVR_CL_DEV_SREND_EXTGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_LIM_COMP_MENSAL_EXT: TBCDField
      FieldName = 'VR_LIM_COMP_MENSAL_EXT'
      OnGetText = cdsRegis_FVR_LIM_COMP_MENSAL_EXTGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_IMP_PG_EXT_MESATUAL: TBCDField
      FieldName = 'VR_IMP_PG_EXT_MESATUAL'
      OnGetText = cdsRegis_FVR_IMP_PG_EXT_MESATUALGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_IMP_PG_EXT_EXCS_MESANT: TBCDField
      FieldName = 'VR_IMP_PG_EXT_EXCS_MESANT'
      OnGetText = cdsRegis_FVR_IMP_PG_EXT_EXCS_MESANTGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_IMP_PG_EXT_MES_EXCS: TBCDField
      FieldName = 'VR_IMP_PG_EXT_MES_EXCS'
      OnGetText = cdsRegis_FVR_IMP_PG_EXT_MES_EXCSGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_IMP_PG_EXT_ACOMP: TBCDField
      FieldName = 'VR_IMP_PG_EXT_ACOMP'
      OnGetText = cdsRegis_FVR_IMP_PG_EXT_ACOMPGetText
      Precision = 18
      Size = 2
    end
    object cdsRegis_FVR_EXCS_PROX_MES: TBCDField
      FieldName = 'VR_EXCS_PROX_MES'
      OnGetText = cdsRegis_FVR_EXCS_PROX_MESGetText
      Precision = 18
      Size = 2
    end
  end
  object cdsCtrl_F: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_CARNELEAO_IDENTIFICACAO'
        ParamType = ptInput
      end>
    ProviderName = 'dspCtrl_F'
    Left = 312
    Top = 112
    object cdsCtrl_FID_CARNELEAO_CONTROLE: TIntegerField
      FieldName = 'ID_CARNELEAO_CONTROLE'
      Required = True
    end
    object cdsCtrl_FID_CARNELEAO_IDENTIFICACAO_FK: TIntegerField
      FieldName = 'ID_CARNELEAO_IDENTIFICACAO_FK'
    end
    object cdsCtrl_FIDENTIFICADOR: TStringField
      FieldName = 'IDENTIFICADOR'
      FixedChar = True
      Size = 2
    end
    object cdsCtrl_FCONTROLE: TStringField
      FieldName = 'CONTROLE'
      Size = 10
    end
  end
  object dsIdent_F: TDataSource
    DataSet = cdsIdent_F
    Left = 24
    Top = 160
  end
  object dsPlCont_F: TDataSource
    DataSet = cdsPlCont_F
    Left = 168
    Top = 160
  end
  object dsEscrit_F: TDataSource
    DataSet = cdsEscrit_F
    Left = 96
    Top = 160
  end
  object dsRegis_F: TDataSource
    DataSet = cdsRegis_F
    Left = 240
    Top = 160
  end
  object dsCtrl_F: TDataSource
    DataSet = cdsCtrl_F
    Left = 312
    Top = 160
  end
  object qryClassif: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT CL.ID_CARNELEAO_CLASSIFICACAO AS ID,'
      '       CL.DESCR_CARNELEAO_CLASSIFICACAO AS DESCR'
      '  FROM CARNELEAO_CLASSIFICACAO CL'
      ' WHERE CL.TIPO_ORIGEM = :TIPO'
      '   AND CL.ID_CARNELEAO_CLASSIFICACAO <> 10'
      'ORDER BY CL.DESCR_CARNELEAO_CLASSIFICACAO')
    Left = 248
    Top = 232
    ParamData = <
      item
        Position = 1
        Name = 'TIPO'
        DataType = ftString
        ParamType = ptInput
      end>
  end
  object dsClassif: TDataSource
    DataSet = qryClassif
    Left = 248
    Top = 280
  end
  object dsEquiv_F: TDataSource
    DataSet = cdsEquiv_F
    Left = 312
    Top = 376
  end
  object dspEquiv_F: TDataSetProvider
    DataSet = qryEquiv_F
    Left = 312
    Top = 280
  end
  object cdsEquiv_F: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'M_TIPO_LANCAMENTO'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'M_ID_CATEGORIA_DESPREC'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'M_ID_SUBCATEGORIA_DESPREC'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'M_ID_NATUREZA'
        ParamType = ptInput
      end>
    ProviderName = 'dspEquiv_F'
    OnCalcFields = cdsEquiv_FCalcFields
    Left = 312
    Top = 328
    object cdsEquiv_FID_CARNELEAO_EQUIVALENCIA: TIntegerField
      FieldName = 'ID_CARNELEAO_EQUIVALENCIA'
      Required = True
    end
    object cdsEquiv_FM_TIPO_LANCAMENTO: TStringField
      FieldName = 'M_TIPO_LANCAMENTO'
      FixedChar = True
      Size = 1
    end
    object cdsEquiv_FM_ID_CATEGORIA_DESPREC_FK: TIntegerField
      FieldName = 'M_ID_CATEGORIA_DESPREC_FK'
    end
    object cdsEquiv_FM_ID_SUBCATEGORIA_DESPREC_FK: TIntegerField
      FieldName = 'M_ID_SUBCATEGORIA_DESPREC_FK'
    end
    object cdsEquiv_FM_ID_NATUREZA_FK: TIntegerField
      FieldName = 'M_ID_NATUREZA_FK'
    end
    object cdsEquiv_FID_CARNELEAO_CLASSIFICACAO_FK: TIntegerField
      FieldName = 'ID_CARNELEAO_CLASSIFICACAO_FK'
    end
    object cdsEquiv_FOBS_CARNELEAO_EQUIVALENCIA: TStringField
      FieldName = 'OBS_CARNELEAO_EQUIVALENCIA'
      Size = 300
    end
    object cdsEquiv_FDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsEquiv_FCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsEquiv_FFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsEquiv_FDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsEquiv_FCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsEquiv_FNOVO: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'NOVO'
    end
    object cdsEquiv_FDESCR_CATEGORIA_DESPREC: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCR_CATEGORIA_DESPREC'
      Size = 250
    end
    object cdsEquiv_FDESCR_SUBCATEGORIA_DESPREC: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCR_SUBCATEGORIA_DESPREC'
      Size = 250
    end
    object cdsEquiv_FDESCR_NATUREZA: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCR_NATUREZA'
      Size = 250
    end
    object cdsEquiv_FDESCR_CARNELEAO_CLASSIFICACAO: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCR_CARNELEAO_CLASSIFICACAO'
      Size = 250
    end
  end
  object qryEquiv_F: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT PLE.*'
      '  FROM CARNELEAO_EQUIVALENCIA PLE')
    Left = 312
    Top = 232
    ParamData = <
      item
        Position = 1
        Name = 'M_TIPO_LANCAMENTO'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'M_ID_CATEGORIA_DESPREC'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 3
        Name = 'M_ID_SUBCATEGORIA_DESPREC'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 4
        Name = 'M_ID_NATUREZA'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object qryCategoria: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT CAT.ID_CATEGORIA_DESPREC AS ID,'
      '       CAT.DESCR_CATEGORIA_DESPREC AS DESCR'
      '  FROM CATEGORIA_DESPREC CAT'
      ' WHERE CAT.TIPO = :TIPO'
      '   AND CAT.ID_NATUREZA_FK = :ID_NATUREZA'
      'ORDER BY CAT.DESCR_CATEGORIA_DESPREC')
    Left = 96
    Top = 232
    ParamData = <
      item
        Position = 1
        Name = 'TIPO'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'ID_NATUREZA'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object qrySubCategoria: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT SCAT.ID_SUBCATEGORIA_DESPREC AS ID,'
      '       SCAT.DESCR_SUBCATEGORIA_DESPREC AS DESCR'
      '  FROM SUBCATEGORIA_DESPREC SCAT'
      ' WHERE SCAT.TIPO = :TIPO'
      '   AND SCAT.ID_CATEGORIA_DESPREC_FK = :ID_CATEGORIA_DESPREC'
      'ORDER BY SCAT.DESCR_SUBCATEGORIA_DESPREC')
    Left = 176
    Top = 232
    ParamData = <
      item
        Name = 'TIPO'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'ID_CATEGORIA_DESPREC'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object qryNatureza: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT NAT.ID_NATUREZA AS ID,'
      '       NAT.DESCR_NATUREZA AS DESCR'
      '  FROM NATUREZA NAT'
      ' WHERE NAT.TIPO_NATUREZA = :TIPO'
      '   AND NAT.FLG_IMPOSTORENDA = '#39'S'#39
      'ORDER BY NAT.DESCR_NATUREZA')
    Left = 24
    Top = 232
    ParamData = <
      item
        Position = 1
        Name = 'TIPO'
        DataType = ftString
        ParamType = ptInput
      end>
  end
  object dsCategoria: TDataSource
    DataSet = qryCategoria
    Left = 96
    Top = 280
  end
  object dsSubCategoria: TDataSource
    DataSet = qrySubCategoria
    Left = 176
    Top = 280
  end
  object dsNatureza: TDataSource
    DataSet = qryNatureza
    Left = 24
    Top = 280
  end
  object qryValores: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT CLE.ID_CARNELEAO_CLASSIFICACAO_FK,'
      '       SUM(LP.VR_PARCELA_PAGO) VR_PARCELA_PAGO'
      '  FROM LANCAMENTO_PARC LP'
      ' INNER JOIN LANCAMENTO L'
      '    ON LP.COD_LANCAMENTO_FK = L.COD_LANCAMENTO'
      '   AND LP.ANO_LANCAMENTO_FK = L.ANO_LANCAMENTO'
      ' INNER JOIN CARNELEAO_EQUIVALENCIA CLE'
      
        '    ON L.ID_CARNELEAO_EQUIVALENCIA_FK = CLE.ID_CARNELEAO_EQUIVAL' +
        'ENCIA'
      ' WHERE LP.FLG_STATUS = '#39'G'#39
      '   AND L.FLG_CANCELADO = '#39'N'#39
      '   AND L.FLG_IMPOSTORENDA = '#39'S'#39
      '   AND EXTRACT(MONTH FROM LP.DATA_PAGAMENTO) = :MES'
      '   AND EXTRACT(YEAR FROM LP.DATA_PAGAMENTO)  = :ANO'
      'GROUP BY CLE.ID_CARNELEAO_CLASSIFICACAO_FK'
      'ORDER BY CLE.ID_CARNELEAO_CLASSIFICACAO_FK')
    Left = 376
    Top = 16
    ParamData = <
      item
        Name = 'MES'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'ANO'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryValoresID_CARNELEAO_CLASSIFICACAO_FK: TIntegerField
      FieldName = 'ID_CARNELEAO_CLASSIFICACAO_FK'
      Origin = 'ID_CARNELEAO_CLASSIFICACAO_FK'
    end
    object qryValoresVR_PARCELA_PAGO: TBCDField
      FieldName = 'VR_PARCELA_PAGO'
      Origin = 'VR_PARCELA_PAGO'
      Precision = 18
      Size = 2
    end
  end
  object dsValores: TDataSource
    DataSet = qryValores
    Left = 376
    Top = 64
  end
end
