object dmFechamentoSalario: TdmFechamentoSalario
  OldCreateOrder = False
  Height = 237
  Width = 338
  object cdsSalario: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_FECHAMENTO_SALARIO'
        ParamType = ptInput
      end>
    ProviderName = 'dspSalario'
    BeforePost = cdsSalarioBeforePost
    AfterScroll = cdsSalarioAfterScroll
    OnCalcFields = cdsSalarioCalcFields
    Left = 112
    Top = 112
    object cdsSalarioID_SALARIO: TIntegerField
      FieldName = 'ID_SALARIO'
      Required = True
    end
    object cdsSalarioID_FECHAMENTO_SALARIO_FK: TIntegerField
      FieldName = 'ID_FECHAMENTO_SALARIO_FK'
    end
    object cdsSalarioID_FUNCIONARIO_FK: TIntegerField
      FieldName = 'ID_FUNCIONARIO_FK'
    end
    object cdsSalarioVR_SALARIO_BASE: TBCDField
      FieldName = 'VR_SALARIO_BASE'
      OnGetText = cdsSalarioVR_SALARIO_BASEGetText
      Precision = 18
      Size = 2
    end
    object cdsSalarioVR_TOTAL_COMISSAO: TBCDField
      FieldName = 'VR_TOTAL_COMISSAO'
      OnGetText = cdsSalarioVR_TOTAL_COMISSAOGetText
      Precision = 18
      Size = 2
    end
    object cdsSalarioVR_TOTAL_OUTRADESPESA: TBCDField
      FieldName = 'VR_TOTAL_OUTRADESPESA'
      OnGetText = cdsSalarioVR_TOTAL_OUTRADESPESAGetText
      Precision = 18
      Size = 2
    end
    object cdsSalarioVR_TOTAL_VALE: TBCDField
      FieldName = 'VR_TOTAL_VALE'
      OnGetText = cdsSalarioVR_TOTAL_VALEGetText
      Precision = 18
      Size = 2
    end
    object cdsSalarioVR_SALARIO_APAGAR: TBCDField
      FieldName = 'VR_SALARIO_APAGAR'
      OnGetText = cdsSalarioVR_SALARIO_APAGARGetText
      Precision = 18
      Size = 2
    end
    object cdsSalarioFUNCIONARIO: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'FUNCIONARIO'
      Size = 250
    end
    object cdsSalarioCPF: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'CPF'
      Size = 14
    end
    object cdsSalarioPRE_CADASTRADO: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'PRE_CADASTRADO'
    end
    object cdsSalarioNOVO: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'NOVO'
    end
    object cdsSalarioEXCLUIDO: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'EXCLUIDO'
    end
    object cdsSalarioVR_TOTAL_OD: TCurrencyField
      FieldKind = fkInternalCalc
      FieldName = 'VR_TOTAL_OD'
      OnGetText = cdsSalarioVR_TOTAL_ODGetText
    end
    object cdsSalarioVR_TOTAL_OD_A: TCurrencyField
      FieldKind = fkInternalCalc
      FieldName = 'VR_TOTAL_OD_A'
      OnGetText = cdsSalarioVR_TOTAL_OD_AGetText
    end
    object cdsSalarioVR_TOTAL_OD_D: TCurrencyField
      FieldKind = fkInternalCalc
      FieldName = 'VR_TOTAL_OD_D'
      OnGetText = cdsSalarioVR_TOTAL_OD_DGetText
    end
  end
  object dspSalario: TDataSetProvider
    DataSet = qrySalario
    Left = 112
    Top = 64
  end
  object qrySalario: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM SALARIO'
      ' WHERE ID_FECHAMENTO_SALARIO_FK = :ID_FECHAMENTO_SALARIO')
    Left = 112
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'ID_FECHAMENTO_SALARIO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dsSalario: TDataSource
    DataSet = cdsSalario
    Left = 112
    Top = 160
  end
  object dsFechamento: TDataSource
    DataSet = cdsFechamento
    Left = 32
    Top = 160
  end
  object dspFechamento: TDataSetProvider
    DataSet = qryFechamento
    Left = 32
    Top = 64
  end
  object cdsFechamento: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_FECHAMENTO_SALARIO'
        ParamType = ptInput
      end>
    ProviderName = 'dspFechamento'
    AfterScroll = cdsFechamentoAfterScroll
    Left = 32
    Top = 112
    object cdsFechamentoID_FECHAMENTO_SALARIO: TIntegerField
      FieldName = 'ID_FECHAMENTO_SALARIO'
      Required = True
    end
    object cdsFechamentoMES_REFERENCIA: TIntegerField
      FieldName = 'MES_REFERENCIA'
    end
    object cdsFechamentoANO_REFERENCIA: TIntegerField
      FieldName = 'ANO_REFERENCIA'
    end
    object cdsFechamentoMES_PAGAMENTO: TIntegerField
      FieldName = 'MES_PAGAMENTO'
    end
    object cdsFechamentoANO_PAGAMENTO: TIntegerField
      FieldName = 'ANO_PAGAMENTO'
    end
    object cdsFechamentoDATA_PREV_PAGAMENTO: TDateField
      FieldName = 'DATA_PREV_PAGAMENTO'
    end
    object cdsFechamentoPREV_ID_USUARIO: TIntegerField
      FieldName = 'PREV_ID_USUARIO'
    end
    object cdsFechamentoVR_TOTAL_BASE: TBCDField
      FieldName = 'VR_TOTAL_BASE'
      Precision = 18
      Size = 2
    end
    object cdsFechamentoVR_TOTAL_COMISSAO: TBCDField
      FieldName = 'VR_TOTAL_COMISSAO'
      Precision = 18
      Size = 2
    end
    object cdsFechamentoVR_TOTAL_OUTRADESPESA: TBCDField
      FieldName = 'VR_TOTAL_OUTRADESPESA'
      Precision = 18
      Size = 2
    end
    object cdsFechamentoVR_TOTAL_VALE: TBCDField
      FieldName = 'VR_TOTAL_VALE'
      Precision = 18
      Size = 2
    end
    object cdsFechamentoVR_TOTAL_APAGAR: TBCDField
      FieldName = 'VR_TOTAL_APAGAR'
      Precision = 18
      Size = 2
    end
    object cdsFechamentoPAG_ID_USUARIO: TIntegerField
      FieldName = 'PAG_ID_USUARIO'
    end
    object cdsFechamentoDATA_REAL_PAGAMENTO: TDateField
      FieldName = 'DATA_REAL_PAGAMENTO'
    end
    object cdsFechamentoFLG_STATUS: TStringField
      FieldName = 'FLG_STATUS'
      FixedChar = True
      Size = 1
    end
    object cdsFechamentoDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsFechamentoCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsFechamentoDATA_REAL_PAGAMENTO_ANT: TDateField
      FieldKind = fkInternalCalc
      FieldName = 'DATA_REAL_PAGAMENTO_ANT'
    end
  end
  object qryFechamento: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM FECHAMENTO_SALARIO'
      ' WHERE ID_FECHAMENTO_SALARIO = :ID_FECHAMENTO_SALARIO')
    Left = 32
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'ID_FECHAMENTO_SALARIO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object qryDetalheLanc: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM LANCAMENTO_DET'
      ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO'
      '    AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO')
    Left = 264
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'COD_LANCAMENTO'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'ANO_LANCAMENTO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspDetalheLanc: TDataSetProvider
    DataSet = qryDetalheLanc
    Left = 264
    Top = 64
  end
  object cdsDetalheLanc: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'COD_LANCAMENTO'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ANO_LANCAMENTO'
        ParamType = ptInput
      end>
    ProviderName = 'dspDetalheLanc'
    OnCalcFields = cdsDetalheLancCalcFields
    Left = 264
    Top = 112
    object cdsDetalheLancID_LANCAMENTO_DET: TIntegerField
      FieldName = 'ID_LANCAMENTO_DET'
      Required = True
    end
    object cdsDetalheLancCOD_LANCAMENTO_FK: TIntegerField
      FieldName = 'COD_LANCAMENTO_FK'
    end
    object cdsDetalheLancANO_LANCAMENTO_FK: TIntegerField
      FieldName = 'ANO_LANCAMENTO_FK'
    end
    object cdsDetalheLancID_ITEM_FK: TIntegerField
      FieldName = 'ID_ITEM_FK'
    end
    object cdsDetalheLancDESCR_ITEM: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCR_ITEM'
      Size = 250
    end
    object cdsDetalheLancVR_UNITARIO: TBCDField
      FieldName = 'VR_UNITARIO'
      OnGetText = cdsDetalheLancVR_UNITARIOGetText
      Precision = 18
      Size = 2
    end
    object cdsDetalheLancQUANTIDADE: TIntegerField
      FieldName = 'QUANTIDADE'
    end
    object cdsDetalheLancVR_TOTAL: TBCDField
      FieldName = 'VR_TOTAL'
      OnGetText = cdsDetalheLancVR_TOTALGetText
      Precision = 18
      Size = 2
    end
    object cdsDetalheLancSELO_ORIGEM: TStringField
      FieldName = 'SELO_ORIGEM'
      Size = 9
    end
    object cdsDetalheLancALEATORIO_ORIGEM: TStringField
      FieldName = 'ALEATORIO_ORIGEM'
      FixedChar = True
      Size = 3
    end
    object cdsDetalheLancTIPO_COBRANCA: TStringField
      FieldName = 'TIPO_COBRANCA'
      FixedChar = True
      Size = 2
    end
    object cdsDetalheLancCOD_ADICIONAL: TIntegerField
      FieldName = 'COD_ADICIONAL'
    end
    object cdsDetalheLancEMOLUMENTOS: TBCDField
      FieldName = 'EMOLUMENTOS'
      Precision = 18
      Size = 2
    end
    object cdsDetalheLancFETJ: TBCDField
      FieldName = 'FETJ'
      Precision = 18
      Size = 2
    end
    object cdsDetalheLancFUNDPERJ: TBCDField
      FieldName = 'FUNDPERJ'
      Precision = 18
      Size = 2
    end
    object cdsDetalheLancFUNPERJ: TBCDField
      FieldName = 'FUNPERJ'
      Precision = 18
      Size = 2
    end
    object cdsDetalheLancFUNARPEN: TBCDField
      FieldName = 'FUNARPEN'
      Precision = 18
      Size = 2
    end
    object cdsDetalheLancPMCMV: TBCDField
      FieldName = 'PMCMV'
      Precision = 18
      Size = 2
    end
    object cdsDetalheLancISS: TBCDField
      FieldName = 'ISS'
      Precision = 18
      Size = 2
    end
    object cdsDetalheLancMUTUA: TBCDField
      FieldName = 'MUTUA'
      Precision = 18
      Size = 2
    end
    object cdsDetalheLancACOTERJ: TBCDField
      FieldName = 'ACOTERJ'
      Precision = 18
      Size = 2
    end
    object cdsDetalheLancFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsDetalheLancNUM_PROTOCOLO: TIntegerField
      FieldName = 'NUM_PROTOCOLO'
    end
  end
  object dsDetalheLanc: TDataSource
    DataSet = cdsDetalheLanc
    Left = 264
    Top = 160
  end
end
