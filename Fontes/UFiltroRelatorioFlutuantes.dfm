inherited FFiltroRelatorioFlutuantes: TFFiltroRelatorioFlutuantes
  Caption = 'Relat'#243'rio de Repasse de Receitas'
  ClientHeight = 337
  ExplicitHeight = 366
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlFiltro: TPanel
    Height = 331
    ExplicitWidth = 328
    ExplicitHeight = 331
    object lblPeriodo: TLabel [0]
      Left = 5
      Top = 8
      Width = 158
      Height = 14
      Caption = 'Per'#237'odo de Entrada/Dep'#243'sito'
    end
    object lblPeriodoInicio: TLabel [1]
      Left = 5
      Top = 31
      Width = 32
      Height = 14
      Caption = 'In'#237'cio:'
    end
    object lblPeriodoFim: TLabel [2]
      Left = 149
      Top = 31
      Width = 22
      Height = 14
      Caption = 'Fim:'
    end
    inherited pnlBotoes: TPanel
      Top = 284
      TabOrder = 7
      ExplicitTop = 284
      ExplicitWidth = 328
      inherited btnLimpar: TJvTransparentButton
        OnClick = btnLimparClick
      end
    end
    inherited gbTipo: TGroupBox
      Left = 3
      Top = 216
      TabOrder = 3
      ExplicitLeft = 3
      ExplicitTop = 216
    end
    inherited chbExibirGrafico: TCheckBox
      Left = 97
      Top = 216
      TabOrder = 4
      ExplicitLeft = 97
      ExplicitTop = 216
    end
    inherited chbExportarPDF: TCheckBox
      Left = 97
      Top = 239
      TabOrder = 5
      ExplicitLeft = 97
      ExplicitTop = 239
    end
    inherited chbExportarExcel: TCheckBox
      Left = 97
      Top = 262
      TabOrder = 6
      ExplicitLeft = 97
      ExplicitTop = 262
    end
    object dteDataInicio: TJvDateEdit
      Left = 43
      Top = 28
      Width = 100
      Height = 22
      Color = 16114127
      ShowNullDate = False
      TabOrder = 0
      OnKeyPress = FormKeyPress
    end
    object dteDataFim: TJvDateEdit
      Left = 177
      Top = 28
      Width = 100
      Height = 22
      Color = 16114127
      ShowNullDate = False
      TabOrder = 1
      OnKeyPress = FormKeyPress
    end
    object gbTipoFlutuante: TGroupBox
      Left = 3
      Top = 56
      Width = 318
      Height = 154
      Caption = 'Tipo de Repasse'
      TabOrder = 2
      object chbDepositos: TCheckBox
        Left = 7
        Top = 24
        Width = 97
        Height = 17
        Caption = 'Dep'#243'sitos:'
        TabOrder = 0
        OnClick = chbDepositosClick
        OnKeyPress = FormKeyPress
      end
      object chbLancamentos: TCheckBox
        Left = 7
        Top = 104
        Width = 97
        Height = 17
        Caption = 'Lan'#231'amentos:'
        TabOrder = 5
        OnClick = chbLancamentosClick
        OnKeyPress = FormKeyPress
      end
      object chbDepositosACompensar: TCheckBox
        Left = 24
        Top = 47
        Width = 97
        Height = 17
        Caption = 'A Compensar'
        TabOrder = 1
        OnKeyPress = FormKeyPress
      end
      object chbDepositosFlutuantes: TCheckBox
        Left = 24
        Top = 70
        Width = 97
        Height = 17
        Caption = 'Flutuante'
        TabOrder = 2
        OnKeyPress = FormKeyPress
      end
      object chbDepositosBaixados: TCheckBox
        Left = 144
        Top = 47
        Width = 97
        Height = 17
        Caption = 'Baixados'
        TabOrder = 3
        OnKeyPress = FormKeyPress
      end
      object chbDepositosCancelados: TCheckBox
        Left = 144
        Top = 70
        Width = 97
        Height = 17
        Caption = 'Cancelados'
        TabOrder = 4
        OnKeyPress = FormKeyPress
      end
      object chbLancamentosNaoBaixados: TCheckBox
        Left = 24
        Top = 127
        Width = 97
        Height = 17
        Caption = 'N'#227'o Baixados'
        TabOrder = 6
        OnKeyPress = FormKeyPress
      end
      object chbLancamentosBaixados: TCheckBox
        Left = 127
        Top = 127
        Width = 74
        Height = 17
        Caption = 'Baixados'
        TabOrder = 7
        OnKeyPress = FormKeyPress
      end
      object chbLancamentosCancelados: TCheckBox
        Left = 207
        Top = 127
        Width = 97
        Height = 17
        Caption = 'Cancelados'
        TabOrder = 8
        OnKeyPress = FormKeyPress
      end
    end
  end
end
