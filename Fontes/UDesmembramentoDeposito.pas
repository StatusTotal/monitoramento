{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UDesmembramentoDeposito.pas
  Descricao:   Tela para Desmembramento dos Valores de um Deposito
  Author   :   Cristina
  Date:        10-jan-2017
  Last Update: 23-out-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UDesmembramentoDeposito;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Datasnap.DBClient, Datasnap.Provider, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, JvExControls, JvButton, JvTransparentButton,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, JvExStdCtrls, JvCombobox,
  JvDBCombobox, JvBaseEdits, JvDBControls, Vcl.DBCtrls, Vcl.Mask, JvExMask,
  JvToolEdit;

type
  TFDesmembramentoDeposito = class(TFCadastroGeralPadrao)
    qryDepoD: TFDQuery;
    gbDepositoPrincipal: TGroupBox;
    gbDepositosDerivados: TGroupBox;
    dbgDepositosDerivados: TDBGrid;
    lblDescricaoDepositoD: TLabel;
    lblNumCodDepositoD: TLabel;
    lblDataDepositoD: TLabel;
    lblValorDepositoD: TLabel;
    lblSituacaoD: TLabel;
    lblDataSituacaoD: TLabel;
    dteDataDepositoD: TJvDBDateEdit;
    edtNumCodDepositoD: TDBEdit;
    edtDescricaoDepositoD: TDBEdit;
    cedValorDepositoD: TJvDBCalcEdit;
    dteDataSituacaoD: TJvDBDateEdit;
    btnIncluirParcelaDepo: TJvTransparentButton;
    btnEditarParcelaDepo: TJvTransparentButton;
    btnExcluirParcelaDepo: TJvTransparentButton;
    btnConfirmarParcelaDepo: TJvTransparentButton;
    btnCancelarParcelaDepo: TJvTransparentButton;
    lblDataDepositoP: TLabel;
    dteDataDepositoP: TJvDBDateEdit;
    lblNumCodDepositoP: TLabel;
    edtNumCodDepositoP: TDBEdit;
    lblDescricaoDepositoP: TLabel;
    edtDescricaoDepositoP: TDBEdit;
    lblValorDepositoPUtilizado: TLabel;
    cedValorDepositoPUtilizado: TJvDBCalcEdit;
    lblSituacaoP: TLabel;
    cbSituacaoP: TJvDBComboBox;
    lblDataSituacaoP: TLabel;
    dteDataSituacaoP: TJvDBDateEdit;
    lblValorDiferenca: TLabel;
    cedValorDiferenca: TJvDBCalcEdit;
    qryDepoP: TFDQuery;
    dspDepoD: TDataSetProvider;
    cdsDepoD: TClientDataSet;
    dsDepoP: TDataSource;
    cdsDepoDID_DEPOSITO_FLUTUANTE: TIntegerField;
    cdsDepoDDESCR_DEPOSITO_FLUTUANTE: TStringField;
    cdsDepoDDATA_DEPOSITO: TDateField;
    cdsDepoDNUM_COD_DEPOSITO: TStringField;
    cdsDepoDVR_DEPOSITO_FLUTUANTE: TBCDField;
    cdsDepoDFLG_STATUS: TStringField;
    cdsDepoDDATA_STATUS: TDateField;
    cdsDepoDCAD_ID_USUARIO: TIntegerField;
    cdsDepoDDATA_CADASTRO: TSQLTimeStampField;
    dspDepoP: TDataSetProvider;
    cdsDepoP: TClientDataSet;
    cdsDepoPID_DEPOSITO_FLUTUANTE: TIntegerField;
    cdsDepoPDESCR_DEPOSITO_FLUTUANTE: TStringField;
    cdsDepoPDATA_DEPOSITO: TDateField;
    cdsDepoPNUM_COD_DEPOSITO: TStringField;
    cdsDepoPVR_DEPOSITO_FLUTUANTE: TBCDField;
    cdsDepoPFLG_STATUS: TStringField;
    cdsDepoPDATA_STATUS: TDateField;
    cdsDepoPCAD_ID_USUARIO: TIntegerField;
    cdsDepoPDATA_CADASTRO: TSQLTimeStampField;
    cdsDepoPVR_ANTIGO: TCurrencyField;
    cdsDepoPVR_DIFERENCA: TCurrencyField;
    lblValorDepositoPAntigo: TLabel;
    cedValorDepositoPAntigo: TJvDBCalcEdit;
    lblTotalDesmembrado: TLabel;
    cedTotalDesmembrado: TJvCalcEdit;
    cdsDepoDFLUT_ID_USUARIO: TIntegerField;
    cdsDepoDBAIXA_ID_USUARIO: TIntegerField;
    cdsDepoDCANCEL_ID_USUARIO: TIntegerField;
    cdsDepoDDATA_FLUTUANTE: TDateField;
    cdsDepoDDATA_BAIXADO: TDateField;
    cdsDepoDDATA_CANCELAMENTO: TDateField;
    cdsDepoPFLUT_ID_USUARIO: TIntegerField;
    cdsDepoPBAIXA_ID_USUARIO: TIntegerField;
    cdsDepoPCANCEL_ID_USUARIO: TIntegerField;
    cdsDepoPDATA_FLUTUANTE: TDateField;
    cdsDepoPDATA_BAIXADO: TDateField;
    cdsDepoPDATA_CANCELAMENTO: TDateField;
    edtSituacaoD: TEdit;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure btnIncluirParcelaDepoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnEditarParcelaDepoClick(Sender: TObject);
    procedure btnExcluirParcelaDepoClick(Sender: TObject);
    procedure btnCancelarParcelaDepoClick(Sender: TObject);
    procedure btnConfirmarParcelaDepoClick(Sender: TObject);
    procedure cedValorDepositoDKeyPress(Sender: TObject; var Key: Char);
    procedure cdsDepoPVR_DEPOSITO_FLUTUANTEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsDepoDVR_DEPOSITO_FLUTUANTEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure dbgDepositosDerivadosDblClick(Sender: TObject);
    procedure dbgDepositosDerivadosDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }

    iIdIncParcDepo: Integer;

    procedure HabDesabBotoesCRUD(Inc, Ed, Exc: Boolean);
    procedure DesabilitaHabilitaCampos(Hab: Boolean);
    procedure CalculaRTotalDesmembrado;

    function VerificarDadosParcelaDeposito(var Msg: String; var QtdErros: Integer): Boolean;
  public
    { Public declarations }

    iIdDeposFlutP: Integer;

    cValorUtilizado: Currency;

    lGravarVinculos: Boolean;
  end;

var
  FDesmembramentoDeposito: TFDesmembramentoDeposito;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{ TFDesmembramentoDeposito }

procedure TFDesmembramentoDeposito.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFDesmembramentoDeposito.btnCancelarParcelaDepoClick(Sender: TObject);
begin
  inherited;

  cdsDepoD.Cancel;

  if cdsDepoD.RecordCount = 0 then
    HabDesabBotoesCRUD(True, False, False)
  else
    HabDesabBotoesCRUD(True, True, True);

  DesabilitaHabilitaCampos(False);
end;

procedure TFDesmembramentoDeposito.btnConfirmarParcelaDepoClick(
  Sender: TObject);
var
  Msg: String;
  iCount, Qtd: Integer;
  lInserindo:  Boolean;
begin
  inherited;

  lblMensagemErro.Caption := '  Mensagem:';
  lbMensagemErro.Color    := clWindow;
  lbMensagemErro.Clear;

  Msg    := '';
  iCount := 0;
  Qtd    := 0;
  lInserindo := False;

  if not VerificarDadosParcelaDeposito(Msg, Qtd) then
  begin
    case Qtd of
      0:
      begin
        lblMensagemErro.Caption := '  Mensagem:';
        lbMensagemErro.Color    := clWindow;
      end;
      1:
      begin
        lblMensagemErro.Caption := '  Durante a verifica��o dos dados, o seguinte erro foi encontrado:';
        lbMensagemErro.Color    := clInfoBk;
      end
      else
      begin
        lblMensagemErro.Caption := '  Durante a verifica��o dos dados, os seguintes erros foram encontrados:';
        lbMensagemErro.Color    := clInfoBk;
      end;
    end;

    for iCount := 0 to Pred(ListaMsgErro.Count) do
      lbMensagemErro.Items.Add(ListaMsgErro[iCount].Mensagem);
  end
  else
  begin
    if cdsDepoD.State = dsInsert then
    begin
      Inc(iIdIncParcDepo);
      cdsDepoD.FieldByName('ID_DEPOSITO_FLUTUANTE').AsInteger := (iIdIncParcDepo + 900000);
      lInserindo := True;
    end;

    cdsDepoD.Post;

    CalculaRTotalDesmembrado;

    HabDesabBotoesCRUD(True, True, True);

    if lInserindo then
    begin
      if vgOperacao = E then
        DesabilitaHabilitaCampos(False)
      else
        btnOk.Click;
    end
    else
      DesabilitaHabilitaCampos(False);
  end;
end;

procedure TFDesmembramentoDeposito.btnEditarParcelaDepoClick(Sender: TObject);
begin
  inherited;

  if cdsDepoD.RecordCount > 0 then
  begin
    cdsDepoD.Edit;
    HabDesabBotoesCRUD(False, False, False);
    DesabilitaHabilitaCampos(True);

    if cedValorDepositoD.CanFocus then
      cedValorDepositoD.SetFocus;
  end;
end;

procedure TFDesmembramentoDeposito.btnExcluirParcelaDepoClick(Sender: TObject);
begin
  inherited;

  if cdsDepoD.RecordCount > 0 then
    cdsDepoD.Delete;
end;

procedure TFDesmembramentoDeposito.btnIncluirParcelaDepoClick(Sender: TObject);
begin
  inherited;

  cdsDepoD.Append;
  cdsDepoD.FieldByName('DATA_DEPOSITO').AsDateTime  := cdsDepoP.FieldByName('DATA_DEPOSITO').AsDateTime;
  cdsDepoD.FieldByName('DATA_DEPOSITO').AsDateTime  := cdsDepoP.FieldByName('DATA_DEPOSITO').AsDateTime;
  cdsDepoD.FieldByName('NUM_COD_DEPOSITO').AsString := cdsDepoP.FieldByName('NUM_COD_DEPOSITO').AsString;
  cdsDepoD.FieldByName('DATA_CADASTRO').AsDateTime  := cdsDepoP.FieldByName('DATA_CADASTRO').AsDateTime;
  cdsDepoD.FieldByName('CAD_ID_USUARIO').AsInteger  := cdsDepoP.FieldByName('CAD_ID_USUARIO').AsInteger;
  cdsDepoD.FieldByName('FLG_STATUS').AsString       := 'F';
  cdsDepoD.FieldByName('DATA_STATUS').AsDateTime    := Date;
  cdsDepoD.FieldByName('DATA_FLUTUANTE').AsDateTime := Date;
  cdsDepoD.FieldByName('FLUT_ID_USUARIO').AsInteger := vgUsu_Id;

  HabDesabBotoesCRUD(False, False, False);
  DesabilitaHabilitaCampos(True);

  if cedValorDepositoD.CanFocus then
    cedValorDepositoD.SetFocus;
end;

procedure TFDesmembramentoDeposito.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
  begin
    if cedTotalDesmembrado.Value = cedValorDiferenca.Value then
      GravarGenerico
    else
      Application.MessageBox('O Valor Desmembrado tem que ser equivalente ao Valor da Diferen�a.',
                             'Aviso',
                             MB_OK + MB_ICONWARNING);
  end
  else
    Self.Close;
end;

procedure TFDesmembramentoDeposito.CalculaRTotalDesmembrado;
var
  cVlrTotal: Currency;
begin
  cVlrTotal := 0;

//  cdsDepoD.DisableControls;

  cdsDepoD.First;

  while not cdsDepoD.Eof do
  begin
    cVlrTotal := (cVlrTotal + cdsDepoD.FieldByName('VR_DEPOSITO_FLUTUANTE').AsCurrency);

    cdsDepoD.Next;
  end;

//  cdsDepoD.EnableControls;

  cedTotalDesmembrado.Value := cVlrTotal;
end;

procedure TFDesmembramentoDeposito.cdsDepoDVR_DEPOSITO_FLUTUANTEGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFDesmembramentoDeposito.cdsDepoPVR_DEPOSITO_FLUTUANTEGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFDesmembramentoDeposito.cedValorDepositoDKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnConfirmarParcelaDepo.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFDesmembramentoDeposito.dbgDepositosDerivadosDblClick(
  Sender: TObject);
begin
  inherited;

  if vgOperacao in [I, E] then
    btnEditarParcelaDepo.Click;
end;

procedure TFDesmembramentoDeposito.dbgDepositosDerivadosDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;

  AlternarCorGrid(Sender, Rect, DataCol, Column, State);
end;

procedure TFDesmembramentoDeposito.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtNumCodDepositoD.MaxLength    := 25;
  edtDescricaoDepositoD.MaxLength := 250;
end;

procedure TFDesmembramentoDeposito.DesabilitaHabilitaCampos(Hab: Boolean);
begin
  edtDescricaoDepositoD.Enabled := Hab;
  cedValorDepositoD.Enabled     := Hab;
end;

procedure TFDesmembramentoDeposito.DesabilitarComponentes;
begin
  inherited;

  { DEPOSITO PRINCIPAL }
  dteDataDepositoP.ReadOnly           := True;
  edtNumCodDepositoP.ReadOnly         := True;
  edtDescricaoDepositoP.ReadOnly      := True;
  cedValorDepositoPUtilizado.ReadOnly := True;
  cbSituacaoP.ReadOnly                := True;
  dteDataSituacaoP.ReadOnly           := True;
  cedValorDepositoPAntigo.ReadOnly    := True;
  cedValorDiferenca.ReadOnly          := True;

  { DEPOSITOS DERIVADOS }
  dteDataDepositoD.ReadOnly      := True;
  edtNumCodDepositoD.ReadOnly    := True;
  edtSituacaoD.ReadOnly          := True;
  dteDataSituacaoD.ReadOnly      := True;
  dbgDepositosDerivados.ReadOnly := True;

  cedTotalDesmembrado.ReadOnly := True;
end;

procedure TFDesmembramentoDeposito.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  if not lPodeCancelar then
    Action := caNone;
end;

procedure TFDesmembramentoDeposito.FormCreate(Sender: TObject);
var
  QryDepo: TFDQuery;
begin
  inherited;

  iIdIncParcDepo := 0;

  QryDepo := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  QryDepo.Close;
  QryDepo.SQL.Clear;
  QryDepo.SQL.Text := 'SELECT COUNT(ID_DEPOSITO_FLUTUANTE) AS QTD ' +
                      '  FROM DEPOSITO_FLUTUANTE';
  QryDepo.Open;

  iIdIncParcDepo := QryDepo.FieldByName('QTD').AsInteger;
end;

procedure TFDesmembramentoDeposito.FormShow(Sender: TObject);
begin
  inherited;

  { DEPOSITO PRINCIPAL }
  cdsDepoP.Close;
  cdsDepoP.Params.ParamByName('ID_DEPOSITO_FLUTUANTE').Value := iIdDeposFlutP;
  cdsDepoP.Open;

  cdsDepoP.Edit;

  cdsDepoP.FieldByName('VR_ANTIGO').AsCurrency             := cdsDepoP.FieldByName('VR_DEPOSITO_FLUTUANTE').AsCurrency;
  cdsDepoP.FieldByName('VR_DIFERENCA').AsCurrency          := (cdsDepoP.FieldByName('VR_DEPOSITO_FLUTUANTE').AsCurrency -
                                                               cValorUtilizado);
  cdsDepoP.FieldByName('VR_DEPOSITO_FLUTUANTE').AsCurrency := cValorUtilizado;

  cdsDepoP.Post;

  { DEPOSITOS DERIVADOS }
  cdsDepoD.Close;
  cdsDepoD.Open;

  cdsDepoD.Append;

  Inc(iIdIncParcDepo);

  cdsDepoD.FieldByName('ID_DEPOSITO_FLUTUANTE').AsInteger  := (iIdIncParcDepo + 900000);
  cdsDepoD.FieldByName('DATA_DEPOSITO').AsDateTime         := cdsDepoP.FieldByName('DATA_DEPOSITO').AsDateTime;
  cdsDepoD.FieldByName('NUM_COD_DEPOSITO').AsString        := cdsDepoP.FieldByName('NUM_COD_DEPOSITO').AsString;
  cdsDepoD.FieldByName('VR_DEPOSITO_FLUTUANTE').AsCurrency := cdsDepoP.FieldByName('VR_DIFERENCA').AsCurrency;
  cdsDepoD.FieldByName('DATA_CADASTRO').AsDateTime         := cdsDepoP.FieldByName('DATA_CADASTRO').AsDateTime;
  cdsDepoD.FieldByName('CAD_ID_USUARIO').AsInteger         := cdsDepoP.FieldByName('CAD_ID_USUARIO').AsInteger;
  cdsDepoD.FieldByName('FLG_STATUS').AsString              := 'F';
  cdsDepoD.FieldByName('DATA_STATUS').AsDateTime           := Date;
  cdsDepoD.FieldByName('DATA_FLUTUANTE').AsDateTime        := Date;
  cdsDepoD.FieldByName('FLUT_ID_USUARIO').AsInteger        := vgUsu_Id;
  cdsDepoD.Post;

  CalcularTotalDesmembrado;

  if vgOperacao <> C then
  begin
    if cdsDepoD.RecordCount = 0 then
      HabDesabBotoesCRUD(True, False, False)
    else
      HabDesabBotoesCRUD(True, True, True);

    DesabilitaHabilitaCampos(False);
  end;

  ShowScrollBar(dbgDepositosDerivados.Handle, SB_HORZ, False);
end;

function TFDesmembramentoDeposito.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFDesmembramentoDeposito.GravarDadosGenerico(var Msg: String): Boolean;
var
  IdDepo: Integer;
begin
  Result := True;
  Msg := '';

  IdDepo := 0;

  if vgOperacao = C then
    Exit;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    cdsDepoP.ApplyUpdates(0);

    if cdsDepoD.RecordCount > 0 then
    begin
      cdsDepoD.First;

      IdDepo := BS.ProximoId('ID_DEPOSITO_FLUTUANTE', 'DEPOSITO_FLUTUANTE');

      while not cdsDepoD.Eof do
      begin
        cdsDepoD.Edit;
        cdsDepoD.FieldByName('ID_DEPOSITO_FLUTUANTE').AsInteger := IdDepo;
        cdsDepoD.Post;

        Inc(IdDepo);
        cdsDepoD.Next;
      end;

      cdsDepoD.ApplyUpdates(0);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    Msg := 'Desmembramento de Dep�sito Flutuante gravado com sucesso!';

    lGravarVinculos := True;
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na grava��o do Desmembramento de Dep�sito Flutuante.';
  end;
end;

procedure TFDesmembramentoDeposito.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta de Dep�sito Flutuante',
                          vgIdConsulta, 'DEPOSITO_FLUTUANTE');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclus�o de Dep�sito Flutuante',
                          vgIdConsulta, 'DEPOSITO_FLUTUANTE');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO',
                                  'Por favor, indique o motivo da edi��o desse Dep�sito Flutuante:',
                                  '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de dados de Dep�sito Flutuante';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          vgIdConsulta, 'DEPOSITO_FLUTUANTE');
    end;
  end;
end;

procedure TFDesmembramentoDeposito.HabDesabBotoesCRUD(Inc, Ed, Exc: Boolean);
begin
  btnIncluirParcelaDepo.Enabled   := Inc;
  btnEditarParcelaDepo.Enabled    := Ed;
  btnExcluirParcelaDepo.Enabled   := Exc;
  btnConfirmarParcelaDepo.Enabled := (not (Inc or Ed or Exc));
  btnCancelarParcelaDepo.Enabled  := (not (Inc or Ed or Exc));
end;

procedure TFDesmembramentoDeposito.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFDesmembramentoDeposito.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

function TFDesmembramentoDeposito.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;
end;

function TFDesmembramentoDeposito.VerificarDadosParcelaDeposito(var Msg: String;
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if cedValorDepositoD.Value <= 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta informar o VALOR DO DEP�SITO.';
    DadosMsgErro.Componente := cedValorDepositoD;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFDesmembramentoDeposito.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

end.

