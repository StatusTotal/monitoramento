{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroAtribuicoesServentia.pas
  Descricao:   Formulario de cadastro das Atribuicoes trabalhadas na Serventia
  Author   :   Cristina
  Date:        28-set-2017
  Last Update: 23-out-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroAtribuicoesServentia;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Datasnap.DBClient, Datasnap.Provider, System.StrUtils;

type
  TFCadastroAtribuicoesServentia = class(TFCadastroGeralPadrao)
    dspSistema: TDataSetProvider;
    cdsSistema: TClientDataSet;
    qrySistema: TFDQuery;
    GroupBox1: TGroupBox;
    chbNotas: TCheckBox;
    chbProtesto: TCheckBox;
    chbRCPJ: TCheckBox;
    chbRCPN: TCheckBox;
    chbRGI: TCheckBox;
    chbRIT: TCheckBox;
    chbRTD: TCheckBox;
    chbFirmas: TCheckBox;
    cdsSistemaID_SISTEMA: TIntegerField;
    cdsSistemaNOME_SISTEMA: TStringField;
    cdsSistemaATRIBUICAO: TIntegerField;
    cdsSistemaFLG_TRABALHA: TStringField;
    qrySisG: TFDQuery;
    dspSisG: TDataSetProvider;
    cdsSisG: TClientDataSet;
    dsSisG: TDataSource;
    cdsSisGID_SISTEMA: TIntegerField;
    cdsSisGNOME_SISTEMA: TStringField;
    cdsSisGATRIBUICAO: TIntegerField;
    cdsSisGFLG_TRABALHA: TStringField;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure chbFirmasClick(Sender: TObject);
    procedure chbNotasClick(Sender: TObject);
    procedure chbProtestoClick(Sender: TObject);
    procedure chbRCPJClick(Sender: TObject);
    procedure chbRCPNClick(Sender: TObject);
    procedure chbRGIClick(Sender: TObject);
    procedure chbRITClick(Sender: TObject);
    procedure chbRTDClick(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadastroAtribuicoesServentia: TFCadastroAtribuicoesServentia;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{ TFCadastroAtribuicoesServentia }

procedure TFCadastroAtribuicoesServentia.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroAtribuicoesServentia.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroAtribuicoesServentia.chbFirmasClick(Sender: TObject);
begin
  inherited;

  if cdsSistema.Locate('ID_SISTEMA', 1, []) then
  begin
    cdsSistema.Edit;
    cdsSistema.FieldByName('FLG_TRABALHA').AsString := IfThen(chbFirmas.Checked, 'S', 'N');
    cdsSistema.Post;
  end;
end;

procedure TFCadastroAtribuicoesServentia.chbNotasClick(Sender: TObject);
begin
  inherited;

  if cdsSistema.Locate('ID_SISTEMA', 2, []) then
  begin
    cdsSistema.Edit;
    cdsSistema.FieldByName('FLG_TRABALHA').AsString := IfThen(chbNotas.Checked, 'S', 'N');
    cdsSistema.Post;
  end;
end;

procedure TFCadastroAtribuicoesServentia.chbProtestoClick(Sender: TObject);
begin
  inherited;

  if cdsSistema.Locate('ID_SISTEMA', 3, []) then
  begin
    cdsSistema.Edit;
    cdsSistema.FieldByName('FLG_TRABALHA').AsString := IfThen(chbProtesto.Checked, 'S', 'N');
    cdsSistema.Post;
  end;
end;

procedure TFCadastroAtribuicoesServentia.chbRCPJClick(Sender: TObject);
begin
  inherited;

  if cdsSistema.Locate('ID_SISTEMA', 4, []) then
  begin
    cdsSistema.Edit;
    cdsSistema.FieldByName('FLG_TRABALHA').AsString := IfThen(chbRCPJ.Checked, 'S', 'N');
    cdsSistema.Post;
  end;
end;

procedure TFCadastroAtribuicoesServentia.chbRCPNClick(Sender: TObject);
begin
  inherited;

  if cdsSistema.Locate('ID_SISTEMA', 5, []) then
  begin
    cdsSistema.Edit;
    cdsSistema.FieldByName('FLG_TRABALHA').AsString := IfThen(chbRCPN.Checked, 'S', 'N');
    cdsSistema.Post;
  end;
end;

procedure TFCadastroAtribuicoesServentia.chbRGIClick(Sender: TObject);
begin
  inherited;

  if cdsSistema.Locate('ID_SISTEMA', 6, []) then
  begin
    cdsSistema.Edit;
    cdsSistema.FieldByName('FLG_TRABALHA').AsString := IfThen(chbRGI.Checked, 'S', 'N');
    cdsSistema.Post;
  end;
end;

procedure TFCadastroAtribuicoesServentia.chbRITClick(Sender: TObject);
begin
  inherited;

  if cdsSistema.Locate('ID_SISTEMA', 8, []) then
  begin
    cdsSistema.Edit;
    cdsSistema.FieldByName('FLG_TRABALHA').AsString := IfThen(chbRIT.Checked, 'S', 'N');
    cdsSistema.Post;
  end;
end;

procedure TFCadastroAtribuicoesServentia.chbRTDClick(Sender: TObject);
begin
  inherited;

  if cdsSistema.Locate('ID_SISTEMA', 7, []) then
  begin
    cdsSistema.Edit;
    cdsSistema.FieldByName('FLG_TRABALHA').AsString := IfThen(chbRTD.Checked, 'S', 'N');
    cdsSistema.Post;
  end;
end;

procedure TFCadastroAtribuicoesServentia.DefinirTamanhoMaxCampos;
begin
  inherited;
  //
end;

procedure TFCadastroAtribuicoesServentia.DesabilitarComponentes;
begin
  inherited;

  if vgOperacao = C then
    pnlDados.Enabled := False;
end;

procedure TFCadastroAtribuicoesServentia.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  if lPodeCancelar then
  begin
    cdsSistema.Cancel;

    vgOperacao     := VAZIO;
    vgIdConsulta   := 0;
    vgOrigemFiltro := False;
  end
  else
    Action := caNone;
end;

procedure TFCadastroAtribuicoesServentia.FormCreate(Sender: TObject);
begin
  inherited;

  cdsSistema.Close;
  cdsSistema.Open;
end;

procedure TFCadastroAtribuicoesServentia.FormShow(Sender: TObject);
begin
  inherited;

  cdsSistema.First;

  while not cdsSistema.Eof do
  begin
    case cdsSistema.FieldByName('ID_SISTEMA').AsInteger of
      1: chbFirmas.Checked   := cdsSistema.FieldByName('FLG_TRABALHA').AsString = 'S';
      2: chbNotas.Checked    := cdsSistema.FieldByName('FLG_TRABALHA').AsString = 'S';
      3: chbProtesto.Checked := cdsSistema.FieldByName('FLG_TRABALHA').AsString = 'S';
      4: chbRCPJ.Checked     := cdsSistema.FieldByName('FLG_TRABALHA').AsString = 'S';
      5: chbRCPJ.Checked     := cdsSistema.FieldByName('FLG_TRABALHA').AsString = 'S';
      6: chbRGI.Checked      := cdsSistema.FieldByName('FLG_TRABALHA').AsString = 'S';
      7: chbRTD.Checked      := cdsSistema.FieldByName('FLG_TRABALHA').AsString = 'S';
      8: chbRIT.Checked      := cdsSistema.FieldByName('FLG_TRABALHA').AsString = 'S';
    end;

    cdsSistema.Next;
  end;

  if chbFirmas.CanFocus then
    chbFirmas.SetFocus;
end;

function TFCadastroAtribuicoesServentia.GravarDadosAto(
  var Msg: String): Boolean;
begin
  //
end;

function TFCadastroAtribuicoesServentia.GravarDadosGenerico(
  var Msg: String): Boolean;
begin
  Result := True;
  Msg := '';

  if vgOperacao = C then
    Exit;

  //MONITORAMENTO
  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    if vgConGER.Connected then
      vgConGER.StartTransaction;

    cdsSistema.First;

    while not cdsSistema.Eof do
    begin
      cdsSisG.Close;
      cdsSisG.Params.ParamByName('ID_SISTEMA').Value := cdsSistema.FieldByName('ID_SISTEMA').AsInteger;
      cdsSisG.Open;

      if cdsSisG.RecordCount > 0 then
      begin
        cdsSisG.Edit;
        cdsSisG.FieldByName('FLG_TRABALHA').AsString := cdsSistema.FieldByName('FLG_TRABALHA').AsString;
        cdsSisG.Post;
        cdsSisG.ApplyUpdates(0);
      end;

      cdsSistema.Next;
    end;

    cdsSistema.ApplyUpdates(0);

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    if vgConGER.InTransaction then
      vgConGER.Commit;

    Msg := 'Atribui��es da Serventia gravadas com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    if vgConGER.InTransaction then
      vgConGER.Rollback;

    Result := False;
    Msg := 'Erro na grava��o das Atribui��es da Serventia.';
  end;
end;

procedure TFCadastroAtribuicoesServentia.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(80, '', 'Somente consulta de Atribui��es da Serventia.',
                          vgIdConsulta, 'SISTEMA');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(80, '', 'Somente inclus�o de Atribui��es da Serventia.',
                          vgIdConsulta, 'SISTEMA');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO',
                                  'Por favor, indique o motivo da edi��o das Atribui��es da Serventia:',
                                  '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o das Atribui��es da Serventia.';

      BS.GravarUsuarioLog(80, '', sObservacao,
                          vgIdConsulta, 'SISTEMA');
    end;
  end;
end;

procedure TFCadastroAtribuicoesServentia.lbMensagemErroDblClick(
  Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroAtribuicoesServentia.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

function TFCadastroAtribuicoesServentia.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if (not chbFirmas.Checked) and
    (not chbNotas.Checked) and
    (not chbProtesto.Checked) and
    (not chbRCPJ.Checked) and
    (not chbRCPN.Checked) and
    (not chbRGI.Checked) and
    (not chbRIT.Checked) and
    (not chbRTD.Checked) then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') � preciso informar todas as Atribui��es da Serventia.';
    DadosMsgErro.Componente := chbFirmas;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroAtribuicoesServentia.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

end.
