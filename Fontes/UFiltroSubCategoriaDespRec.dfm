inherited FFiltroSubCategoriaDespRec: TFFiltroSubCategoriaDespRec
  Caption = 'Filtro de Subcategoria de Lan'#231'amento'
  ClientHeight = 277
  ClientWidth = 809
  OnCreate = FormCreate
  ExplicitWidth = 815
  ExplicitHeight = 306
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 803
    Height = 271
    ExplicitWidth = 803
    ExplicitHeight = 271
    inherited pnlGrid: TPanel
      Top = 52
      Width = 719
      Height = 216
      ExplicitTop = 52
      ExplicitWidth = 719
      ExplicitHeight = 216
      inherited pnlGridPai: TPanel
        Width = 719
        Height = 216
        ExplicitWidth = 719
        ExplicitHeight = 216
        inherited dbgGridPaiPadrao: TDBGrid
          Width = 719
          Height = 216
          Columns = <
            item
              Expanded = False
              FieldName = 'ID'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'ID_CATEGORIA_DESPREC_FK'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'DESCR_NATUREZA'
              Title.Caption = 'Natureza'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DESCR_CATEGORIA_DESPREC'
              Title.Caption = 'Categoria'
              Width = 230
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TIPO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'DESCR_SUBCATEGORIA_DESPREC'
              Title.Caption = 'Subcategoria'
              Width = 255
              Visible = True
            end>
        end
      end
    end
    inherited pnlFiltro: TPanel
      Width = 803
      Height = 49
      ExplicitWidth = 803
      ExplicitHeight = 49
      inherited btnFiltrar: TJvTransparentButton
        Left = 628
        Top = 20
        ExplicitLeft = 628
        ExplicitTop = 20
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 717
        Top = 20
        ExplicitLeft = 717
        ExplicitTop = 20
      end
      object lblSubCategoria: TLabel
        Left = 334
        Top = 8
        Width = 71
        Height = 14
        Caption = 'Subcategoria'
      end
      object lblCategoria: TLabel
        Left = 81
        Top = 8
        Width = 51
        Height = 14
        Caption = 'Categoria'
      end
      object edtSubCategoria: TEdit
        Left = 334
        Top = 24
        Width = 288
        Height = 22
        Color = 16114127
        TabOrder = 1
        OnKeyPress = edtSubCategoriaKeyPress
      end
      object cbCategoria: TComboBox
        Left = 81
        Top = 24
        Width = 247
        Height = 22
        Style = csOwnerDrawFixed
        Color = 16114127
        TabOrder = 0
        OnKeyPress = cbCategoriaKeyPress
      end
    end
    inherited pnlMenu: TPanel
      Top = 52
      Height = 216
      ExplicitTop = 52
      ExplicitHeight = 216
      inherited btnImprimir: TJvTransparentButton
        Visible = False
      end
    end
  end
  inherited dsGridPaiPadrao: TDataSource
    Left = 740
    Top = 195
  end
  inherited qryGridPaiPadrao: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT S.ID_SUBCATEGORIA_DESPREC AS ID,'
      '       S.ID_CATEGORIA_DESPREC_FK,'
      '       C.DESCR_CATEGORIA_DESPREC,'
      '       S.TIPO,'
      '       S.DESCR_SUBCATEGORIA_DESPREC,'
      '       N.DESCR_NATUREZA'
      '  FROM SUBCATEGORIA_DESPREC S'
      ' INNER JOIN CATEGORIA_DESPREC C'
      '    ON S.ID_CATEGORIA_DESPREC_FK = C.ID_CATEGORIA_DESPREC'
      ' INNER JOIN NATUREZA N'
      '    ON C.ID_NATUREZA_FK = N.ID_NATUREZA'
      ' WHERE S.TIPO = :TIPO'
      'ORDER BY C.DESCR_CATEGORIA_DESPREC, S.DESCR_SUBCATEGORIA_DESPREC')
    Left = 740
    Top = 143
    ParamData = <
      item
        Name = 'TIPO'
        DataType = ftString
        ParamType = ptInput
      end>
    object qryGridPaiPadraoID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID_SUBCATEGORIA_DESPREC'
      Required = True
    end
    object qryGridPaiPadraoID_CATEGORIA_DESPREC_FK: TIntegerField
      FieldName = 'ID_CATEGORIA_DESPREC_FK'
      Origin = 'ID_CATEGORIA_DESPREC_FK'
    end
    object qryGridPaiPadraoDESCR_CATEGORIA_DESPREC: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DESCR_CATEGORIA_DESPREC'
      Origin = 'DESCR_CATEGORIA_DESPREC'
      ProviderFlags = []
      ReadOnly = True
      Size = 250
    end
    object qryGridPaiPadraoTIPO: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      FixedChar = True
      Size = 1
    end
    object qryGridPaiPadraoDESCR_SUBCATEGORIA_DESPREC: TStringField
      FieldName = 'DESCR_SUBCATEGORIA_DESPREC'
      Origin = 'DESCR_SUBCATEGORIA_DESPREC'
      Size = 250
    end
    object qryGridPaiPadraoDESCR_NATUREZA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DESCR_NATUREZA'
      Origin = 'DESCR_NATUREZA'
      ProviderFlags = []
      ReadOnly = True
      Size = 250
    end
  end
  object qryCategoria: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT ID_CATEGORIA_DESPREC'
      '  FROM CATEGORIA_DESPREC'
      ' WHERE DESCR_CATEGORIA_DESPREC = :DESCR_CATEGORIA_DESPREC'
      '   AND TIPO = :TIPO')
    Left = 116
    Top = 110
    ParamData = <
      item
        Name = 'DESCR_CATEGORIA_DESPREC'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'TIPO'
        DataType = ftString
        ParamType = ptInput
      end>
  end
end
