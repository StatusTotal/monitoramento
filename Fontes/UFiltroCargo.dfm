inherited FFiltroCargo: TFFiltroCargo
  Caption = 'Filtro de Cargo'
  ClientHeight = 277
  ClientWidth = 589
  OnCreate = FormCreate
  ExplicitWidth = 595
  ExplicitHeight = 306
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 583
    Height = 271
    ExplicitWidth = 583
    ExplicitHeight = 271
    inherited pnlGrid: TPanel
      Top = 52
      Width = 499
      Height = 216
      ExplicitTop = 52
      ExplicitWidth = 499
      ExplicitHeight = 216
      inherited pnlGridPai: TPanel
        Width = 499
        Height = 216
        ExplicitWidth = 499
        ExplicitHeight = 216
        inherited dbgGridPaiPadrao: TDBGrid
          Width = 499
          Height = 216
          Columns = <
            item
              Expanded = False
              FieldName = 'ID'
              Visible = False
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'COD_CARGO'
              Title.Alignment = taCenter
              Title.Caption = 'C'#243'd.'
              Width = 34
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DESCR_CARGO'
              Title.Caption = 'Descri'#231#227'o'
              Width = 432
              Visible = True
            end>
        end
      end
    end
    inherited pnlFiltro: TPanel
      Width = 583
      Height = 49
      ExplicitWidth = 583
      ExplicitHeight = 49
      inherited btnFiltrar: TJvTransparentButton
        Left = 408
        Top = 20
        ExplicitLeft = 408
        ExplicitTop = 20
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 497
        Top = 20
        ExplicitLeft = 497
        ExplicitTop = 20
      end
      object lblDescrCargo: TLabel
        Left = 81
        Top = 8
        Width = 31
        Height = 14
        Caption = 'Cargo'
      end
      object edtDescrCargo: TEdit
        Left = 81
        Top = 24
        Width = 321
        Height = 22
        Color = 16114127
        TabOrder = 0
        OnKeyPress = edtDescrCargoKeyPress
      end
    end
    inherited pnlMenu: TPanel
      Top = 52
      Height = 216
      ExplicitTop = 52
      ExplicitHeight = 216
      inherited btnImprimir: TJvTransparentButton
        Visible = False
      end
    end
  end
  inherited dsGridPaiPadrao: TDataSource
    Left = 516
    Top = 219
  end
  inherited qryGridPaiPadrao: TFDQuery
    SQL.Strings = (
      'SELECT ID_CARGO AS ID,'
      '       DESCR_CARGO,'
      '       COD_CARGO'
      '  FROM CARGO'
      'ORDER BY DESCR_CARGO')
    Left = 516
    Top = 167
    object qryGridPaiPadraoID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID_CARGO'
      Required = True
    end
    object qryGridPaiPadraoDESCR_CARGO: TStringField
      FieldName = 'DESCR_CARGO'
      Origin = 'DESCR_CARGO'
      Size = 250
    end
    object qryGridPaiPadraoCOD_CARGO: TIntegerField
      FieldName = 'COD_CARGO'
      Origin = 'COD_CARGO'
    end
  end
end
