inherited FFiltroCarneLeao: TFFiltroCarneLeao
  Caption = 'Filtro de Carn'#234'-Le'#227'o'
  ClientHeight = 367
  ClientWidth = 908
  OnCreate = FormCreate
  ExplicitWidth = 914
  ExplicitHeight = 396
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 902
    Height = 361
    ExplicitWidth = 902
    ExplicitHeight = 361
    inherited pnlGrid: TPanel
      Top = 70
      Width = 818
      ExplicitTop = 70
      ExplicitWidth = 818
      inherited pnlGridPai: TPanel
        Width = 818
        ExplicitWidth = 818
        inherited dbgGridPaiPadrao: TDBGrid
          Width = 818
          Columns = <
            item
              Expanded = False
              FieldName = 'ID'
              Visible = False
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'ANO_DECLARACAO'
              Title.Alignment = taCenter
              Title.Caption = 'Ano Declara'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TIPO_ENVIO'
              Title.Caption = 'Tipo Envio'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'SISTEMA_GERADOR'
              Title.Caption = 'Sistema Gerador'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CPF_DECLARANTE'
              Title.Caption = 'CPF Declarante'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_DECLARANTE'
              Title.Caption = 'Nome Declarante'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ARQUIVO'
              Title.Caption = 'Arquivo'
              Width = 118
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATA_GERACAO'
              Title.Caption = 'Dt. Gera'#231#227'o'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GER_ID_USUARIO'
              Visible = False
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'FLG_CANCELADO'
              Title.Alignment = taCenter
              Title.Caption = 'Canc.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATA_CANCELAMENTO'
              Title.Caption = 'Dt. Cancel.'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'CANCEL_ID_USUARIO'
              Visible = False
            end>
        end
      end
    end
    inherited pnlFiltro: TPanel
      Width = 902
      Height = 67
      ExplicitWidth = 902
      ExplicitHeight = 67
      inherited btnFiltrar: TJvTransparentButton
        Left = 727
        Top = 38
        ExplicitLeft = 727
        ExplicitTop = 38
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 816
        ExplicitLeft = 816
      end
      object lblAno: TLabel
        Left = 81
        Top = 26
        Width = 22
        Height = 14
        Caption = 'Ano'
      end
      object edtAno: TEdit
        Left = 81
        Top = 42
        Width = 65
        Height = 22
        Color = 16114127
        TabOrder = 0
        OnKeyPress = edtAnoKeyPress
      end
    end
    inherited pnlMenu: TPanel
      Top = 70
      ExplicitTop = 70
      inherited btnExcluir: TJvTransparentButton
        Caption = '&Cancelar'
      end
    end
  end
  inherited dsGridPaiPadrao: TDataSource
    Left = 836
    Top = 259
  end
  inherited qryGridPaiPadrao: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT ID_CARNELEAO_IDENTIFICACAO AS ID,'
      '       ANO_DECLARACAO,'
      '       TIPO_ENVIO,'
      '       SISTEMA_GERADOR,'
      '       CPF_DECLARANTE,'
      '       NOME_DECLARANTE,'
      '       DATA_GERACAO,'
      '       GER_ID_USUARIO,'
      '       FLG_CANCELADO,'
      '       DATA_CANCELAMENTO,'
      '       CANCEL_ID_USUARIO,'
      
        '       CAST((NOME_ARQUIVO || '#39'.'#39' || EXTENSAO_ARQUIVO) AS VARCHAR' +
        '(123)) AS ARQUIVO'
      '  FROM CARNELEAO_IDENTIFICACAO'
      ' WHERE ANO_DECLARACAO = (CASE WHEN (:ANO1 = 0)'
      '                              THEN ANO_DECLARACAO'
      '                              ELSE :ANO2'
      '                         END)'
      'ORDER BY ANO_DECLARACAO DESC,'
      '         TIPO_ENVIO ASC')
    Left = 836
    Top = 207
    ParamData = <
      item
        Name = 'ANO1'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'ANO2'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryGridPaiPadraoID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID_CARNELEAO_IDENTIF'
      Required = True
    end
    object qryGridPaiPadraoANO_DECLARACAO: TIntegerField
      FieldName = 'ANO_DECLARACAO'
      Origin = 'ANO_DECLARACAO'
    end
    object qryGridPaiPadraoTIPO_ENVIO: TStringField
      FieldName = 'TIPO_ENVIO'
      Origin = 'TIPO_ENVIO'
      FixedChar = True
      Size = 1
    end
    object qryGridPaiPadraoSISTEMA_GERADOR: TStringField
      FieldName = 'SISTEMA_GERADOR'
      Origin = 'SISTEMA_GERADOR'
      Size = 14
    end
    object qryGridPaiPadraoCPF_DECLARANTE: TStringField
      FieldName = 'CPF_DECLARANTE'
      Origin = 'CPF_DECLARANTE'
      FixedChar = True
      Size = 11
    end
    object qryGridPaiPadraoNOME_DECLARANTE: TStringField
      FieldName = 'NOME_DECLARANTE'
      Origin = 'NOME_DECLARANTE'
      Size = 60
    end
    object qryGridPaiPadraoDATA_GERACAO: TSQLTimeStampField
      FieldName = 'DATA_GERACAO'
      Origin = 'DATA_GERACAO'
    end
    object qryGridPaiPadraoGER_ID_USUARIO: TIntegerField
      FieldName = 'GER_ID_USUARIO'
      Origin = 'GER_ID_USUARIO'
    end
    object qryGridPaiPadraoFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      Origin = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object qryGridPaiPadraoDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
      Origin = 'DATA_CANCELAMENTO'
    end
    object qryGridPaiPadraoCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
      Origin = 'CANCEL_ID_USUARIO'
    end
    object qryGridPaiPadraoARQUIVO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ARQUIVO'
      Origin = 'ARQUIVO'
      ProviderFlags = []
      ReadOnly = True
      Size = 123
    end
  end
end
