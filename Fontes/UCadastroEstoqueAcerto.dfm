inherited FCadastroEstoqueAcerto: TFCadastroEstoqueAcerto
  Caption = 'Acerto de Estoque'
  ClientHeight = 479
  ClientWidth = 675
  ExplicitWidth = 681
  ExplicitHeight = 508
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 669
    Height = 473
    ExplicitWidth = 669
    ExplicitHeight = 473
    inherited pnlDados: TPanel
      Width = 555
      Height = 467
      ExplicitWidth = 555
      ExplicitHeight = 467
      object lblItem: TLabel [0]
        Left = 5
        Top = 46
        Width = 26
        Height = 14
        Caption = 'Item'
      end
      object lblDataHistorico: TLabel [1]
        Left = 5
        Top = 90
        Width = 25
        Height = 14
        Caption = 'Data'
      end
      object lblTipoHistorico: TLabel [2]
        Left = 151
        Top = 90
        Width = 83
        Height = 14
        Caption = 'Tipo de Acerto'
      end
      object lblQuantidade: TLabel [3]
        Left = 5
        Top = 134
        Width = 63
        Height = 14
        Caption = 'Quantidade'
      end
      object lblValor: TLabel [4]
        Left = 111
        Top = 134
        Width = 27
        Height = 14
        Caption = 'Valor'
      end
      object lblQtdAtual: TLabel [5]
        Left = 443
        Top = 17
        Width = 110
        Height = 23
        Alignment = taRightJustify
        Caption = 'Qtd. Atual: '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cedQuantidade: TJvDBCalcEdit [6]
        Left = 5
        Top = 150
        Width = 100
        Height = 22
        Color = 16114127
        DisplayFormat = ',0.00'
        TabOrder = 2
        DecimalPlacesAlwaysShown = False
        OnKeyPress = FormKeyPress
        DataField = 'QUANTIDADE'
        DataSource = dsCadastro
      end
      object cedValor: TJvDBCalcEdit [7]
        Left = 111
        Top = 150
        Width = 100
        Height = 22
        Color = 16114127
        DisplayFormat = ',0.00'
        TabOrder = 3
        DecimalPlacesAlwaysShown = False
        OnKeyPress = cedValorKeyPress
        DataField = 'VR_UNITARIO'
        DataSource = dsCadastro
      end
      object medDataHistorico: TJvDBMaskEdit [8]
        Left = 5
        Top = 106
        Width = 140
        Height = 22
        Color = 16114127
        DataField = 'DATA_HISTORICO'
        DataSource = dsCadastro
        Enabled = False
        MaxLength = 19
        TabOrder = 6
        EditMask = '99/99/9999 99:99:99'
        OnKeyPress = FormKeyPress
      end
      inherited pnlMsgErro: TPanel
        Top = 406
        Width = 555
        TabOrder = 5
        ExplicitTop = 406
        ExplicitWidth = 555
        inherited lblMensagemErro: TLabel
          Width = 555
        end
        inherited lbMensagemErro: TListBox
          Width = 555
          OnDblClick = lbMensagemErroDblClick
          ExplicitWidth = 555
        end
      end
      object lcbTipoHistorico: TDBLookupComboBox
        Left = 151
        Top = 106
        Width = 402
        Height = 22
        Color = 16114127
        DataField = 'ID_TIPO_HISTORICO_FK'
        DataSource = dsCadastro
        KeyField = 'ID_TIPO_HISTORICO'
        ListField = 'DESCR_TIPO_HISTORICO'
        ListSource = dsTipoHistorico
        TabOrder = 1
        OnExit = lcbTipoHistoricoExit
        OnKeyPress = FormKeyPress
      end
      object dbgHistoricoItem: TDBGrid
        Left = 5
        Top = 178
        Width = 548
        Height = 222
        Color = clWhite
        DataSource = dsCadastro
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 4
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnKeyPress = FormKeyPress
        Columns = <
          item
            Expanded = False
            FieldName = 'ID_HISTORICO_ITEM'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_ITEM_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_HISTORICO'
            Title.Caption = 'Data'
            Width = 136
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ID_USUARIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'QUANTIDADE'
            Title.Caption = 'Qtd.'
            Width = 37
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VR_UNITARIO'
            Title.Caption = 'Vlr. Unit'#225'rio'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ID_TIPO_HISTORICO_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DESCR_TIPO_HISTORICO'
            Title.Caption = 'Tp. Hist'#243'rico'
            Width = 260
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FLG_TIPO_HISTORICO'
            Visible = False
          end>
      end
      object edtItem: TEdit
        Left = 5
        Top = 62
        Width = 548
        Height = 22
        CharCase = ecUpperCase
        Color = 16114127
        Enabled = False
        TabOrder = 0
        OnKeyPress = FormKeyPress
      end
    end
    inherited pnlMenu: TPanel
      Height = 467
      ExplicitHeight = 467
      inherited btnOk: TJvTransparentButton
        Top = 377
        ExplicitTop = 336
      end
      inherited btnCancelar: TJvTransparentButton
        Top = 422
        ExplicitTop = 381
      end
    end
  end
  inherited dsCadastro: TDataSource
    DataSet = cdsHistoricoItem
    Left = 621
    Top = 427
  end
  object qryHistoricoItem: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM HISTORICO_ITEM'
      ' WHERE ID_ITEM_FK = :ID_ITEM'
      'ORDER BY DATA_HISTORICO DESC')
    Left = 371
    Top = 427
    ParamData = <
      item
        Position = 1
        Name = 'ID_ITEM'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object qryTipoHistorico: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM TIPO_HISTORICO'
      'ORDER BY DESCR_TIPO_HISTORICO')
    Left = 38
    Top = 198
  end
  object dsTipoHistorico: TDataSource
    DataSet = qryTipoHistorico
    Left = 38
    Top = 246
  end
  object dspHistoricoItem: TDataSetProvider
    DataSet = qryHistoricoItem
    Left = 458
    Top = 427
  end
  object cdsHistoricoItem: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_ITEM'
        ParamType = ptInput
      end>
    ProviderName = 'dspHistoricoItem'
    OnCalcFields = cdsHistoricoItemCalcFields
    Left = 546
    Top = 427
    object cdsHistoricoItemID_HISTORICO_ITEM: TIntegerField
      FieldName = 'ID_HISTORICO_ITEM'
      Required = True
    end
    object cdsHistoricoItemID_ITEM_FK: TIntegerField
      FieldName = 'ID_ITEM_FK'
    end
    object cdsHistoricoItemDATA_HISTORICO: TSQLTimeStampField
      FieldName = 'DATA_HISTORICO'
    end
    object cdsHistoricoItemID_USUARIO: TIntegerField
      FieldName = 'ID_USUARIO'
    end
    object cdsHistoricoItemQUANTIDADE: TIntegerField
      FieldName = 'QUANTIDADE'
      OnGetText = cdsHistoricoItemQUANTIDADEGetText
    end
    object cdsHistoricoItemVR_UNITARIO: TBCDField
      FieldName = 'VR_UNITARIO'
      OnGetText = cdsHistoricoItemVR_UNITARIOGetText
      Precision = 18
      Size = 2
    end
    object cdsHistoricoItemID_TIPO_HISTORICO_FK: TIntegerField
      FieldName = 'ID_TIPO_HISTORICO_FK'
    end
    object cdsHistoricoItemFLG_TIPO_HISTORICO: TStringField
      FieldName = 'FLG_TIPO_HISTORICO'
      FixedChar = True
      Size = 1
    end
    object cdsHistoricoItemDESCR_TIPO_HISTORICO: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCR_TIPO_HISTORICO'
      Size = 200
    end
  end
end
