inherited FCadastroComissao: TFCadastroComissao
  Caption = 'Cadastro de Comiss'#227'o'
  ClientHeight = 210
  ClientWidth = 583
  ExplicitWidth = 589
  ExplicitHeight = 239
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 577
    Height = 204
    ExplicitWidth = 577
    ExplicitHeight = 204
    inherited pnlDados: TPanel
      Width = 463
      Height = 198
      ExplicitWidth = 463
      ExplicitHeight = 198
      object lblNomeColaborador: TLabel [0]
        Left = 5
        Top = 5
        Width = 64
        Height = 14
        Caption = 'Colaborador'
      end
      object lblDataComissao: TLabel [1]
        Left = 361
        Top = 5
        Width = 70
        Height = 14
        Caption = 'Dt. Comiss'#227'o'
      end
      object lblItem: TLabel [2]
        Left = 5
        Top = 49
        Width = 26
        Height = 14
        Caption = 'Item'
      end
      object lblPercentagem: TLabel [3]
        Left = 5
        Top = 93
        Width = 12
        Height = 14
        Caption = '%'
      end
      object lblValor: TLabel [4]
        Left = 81
        Top = 93
        Width = 71
        Height = 14
        Caption = 'Vlr. Comiss'#227'o'
      end
      object lblDataPagamento: TLabel [5]
        Left = 361
        Top = 93
        Width = 83
        Height = 14
        Caption = 'Dt. Pagamento'
      end
      object lblValorItem: TLabel [6]
        Left = 361
        Top = 49
        Width = 48
        Height = 14
        Caption = 'Vlr. Item'
      end
      inherited pnlMsgErro: TPanel
        Top = 137
        Width = 463
        TabOrder = 7
        ExplicitTop = 137
        ExplicitWidth = 463
        inherited lblMensagemErro: TLabel
          Width = 463
        end
        inherited lbMensagemErro: TListBox
          Width = 463
          OnDblClick = lbMensagemErroDblClick
          ExplicitWidth = 463
        end
      end
      object lcbNomeColaborador: TDBLookupComboBox
        Left = 5
        Top = 21
        Width = 350
        Height = 22
        Color = 16114127
        DataField = 'ID_FUNCIONARIO_FK'
        DataSource = dsCadastro
        KeyField = 'ID_FUNCIONARIO'
        ListField = 'NOME_FUNCIONARIO'
        ListSource = dsColab
        TabOrder = 0
        OnKeyPress = FormKeyPress
      end
      object lcbItem: TDBLookupComboBox
        Left = 5
        Top = 65
        Width = 350
        Height = 22
        Color = 16114127
        DataField = 'ID_ITEM_FK'
        DataSource = dsCadastro
        KeyField = 'ID_ITEM'
        ListField = 'DESCR_ITEM'
        ListSource = dsItem
        TabOrder = 2
        OnExit = lcbItemExit
        OnKeyPress = FormKeyPress
      end
      object dteDataComissao: TJvDBDateEdit
        Left = 361
        Top = 21
        Width = 100
        Height = 22
        DataField = 'DATA_COMISSAO'
        DataSource = dsCadastro
        Color = 16114127
        ShowNullDate = False
        TabOrder = 1
        OnExit = dteDataComissaoExit
        OnKeyPress = dteDataComissaoKeyPress
      end
      object dteDataPagamento: TJvDBDateEdit
        Left = 361
        Top = 109
        Width = 100
        Height = 22
        DataField = 'DATA_PAGAMENTO'
        DataSource = dsCadastro
        Color = 16114127
        Enabled = False
        ShowNullDate = False
        TabOrder = 6
        OnKeyPress = FormKeyPress
      end
      object cedPercentagem: TJvDBCalcEdit
        Left = 5
        Top = 109
        Width = 70
        Height = 22
        Color = 16114127
        DisplayFormat = ',0.00'
        TabOrder = 4
        DecimalPlacesAlwaysShown = False
        OnExit = cedPercentagemExit
        OnKeyPress = FormKeyPress
        DataField = 'PERCENT_COMISSAO'
        DataSource = dsCadastro
      end
      object cedValor: TJvDBCalcEdit
        Left = 81
        Top = 109
        Width = 100
        Height = 22
        Color = 16114127
        DisplayFormat = ',0.00'
        TabOrder = 5
        DecimalPlacesAlwaysShown = False
        OnExit = cedValorExit
        OnKeyPress = cedValorKeyPress
        DataField = 'VR_COMISSAO'
        DataSource = dsCadastro
      end
      object cedValorItem: TJvDBCalcEdit
        Left = 361
        Top = 65
        Width = 100
        Height = 22
        Color = 16114127
        DisplayFormat = ',0.00'
        TabOrder = 3
        DecimalPlacesAlwaysShown = False
        OnKeyPress = FormKeyPress
        DataField = 'VR_ITEM'
        DataSource = dsCadastro
      end
    end
    inherited pnlMenu: TPanel
      Height = 198
      ExplicitHeight = 198
      inherited btnDadosPrincipais: TJvTransparentButton
        Caption = 'Comiss'#227'o'
      end
      inherited btnOk: TJvTransparentButton
        Top = 108
        ExplicitTop = 108
      end
      inherited btnCancelar: TJvTransparentButton
        Top = 153
        ExplicitTop = 153
      end
    end
  end
  inherited dsCadastro: TDataSource
    DataSet = cdsComissao
    Left = 526
    Top = 150
  end
  object cdsComissao: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_COMISSAO'
        ParamType = ptInput
      end>
    ProviderName = 'dspComissao'
    OnCalcFields = cdsComissaoCalcFields
    Left = 466
    Top = 150
    object cdsComissaoID_COMISSAO: TIntegerField
      FieldName = 'ID_COMISSAO'
      Required = True
    end
    object cdsComissaoID_FUNCIONARIO_FK: TIntegerField
      FieldName = 'ID_FUNCIONARIO_FK'
    end
    object cdsComissaoID_SISTEMA_FK: TIntegerField
      FieldName = 'ID_SISTEMA_FK'
    end
    object cdsComissaoCOD_ADICIONAL: TIntegerField
      FieldName = 'COD_ADICIONAL'
    end
    object cdsComissaoID_ORIGEM: TIntegerField
      FieldName = 'ID_ORIGEM'
    end
    object cdsComissaoTIPO_ORIGEM: TStringField
      FieldName = 'TIPO_ORIGEM'
      FixedChar = True
      Size = 1
    end
    object cdsComissaoPERCENT_COMISSAO: TBCDField
      FieldName = 'PERCENT_COMISSAO'
      Precision = 18
      Size = 2
    end
    object cdsComissaoVR_COMISSAO: TBCDField
      FieldName = 'VR_COMISSAO'
      Precision = 18
      Size = 2
    end
    object cdsComissaoID_FECHAMENTO_SALARIO_FK: TIntegerField
      FieldName = 'ID_FECHAMENTO_SALARIO_FK'
    end
    object cdsComissaoDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsComissaoCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsComissaoDATA_PAGAMENTO: TDateField
      FieldName = 'DATA_PAGAMENTO'
    end
    object cdsComissaoPAG_ID_USUARIO: TIntegerField
      FieldName = 'PAG_ID_USUARIO'
    end
    object cdsComissaoFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsComissaoDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsComissaoCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsComissaoDATA_COMISSAO: TDateField
      FieldName = 'DATA_COMISSAO'
    end
    object cdsComissaoID_ITEM_FK: TIntegerField
      FieldName = 'ID_ITEM_FK'
    end
    object cdsComissaoVR_COMISSAO_ANT: TCurrencyField
      FieldKind = fkInternalCalc
      FieldName = 'VR_COMISSAO_ANT'
    end
    object cdsComissaoVR_ITEM: TCurrencyField
      FieldKind = fkInternalCalc
      FieldName = 'VR_ITEM'
    end
  end
  object dspComissao: TDataSetProvider
    DataSet = qryComissao
    Left = 402
    Top = 150
  end
  object qryComissao: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM COMISSAO'
      ' WHERE ID_COMISSAO = :ID_COMISSAO')
    Left = 338
    Top = 150
    ParamData = <
      item
        Position = 1
        Name = 'ID_COMISSAO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dsItem: TDataSource
    DataSet = qryItem
    Left = 249
    Top = 159
  end
  object dsColab: TDataSource
    DataSet = qryColab
    Left = 66
    Top = 62
  end
  object qryItem: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      
        'SELECT I.ID_ITEM, I.DESCR_ITEM, I.ULT_VALOR_CUSTO, LD.COD_ADICIO' +
        'NAL'
      '  FROM ITEM I'
      ' INNER JOIN LANCAMENTO_DET LD'
      '    ON I.ID_ITEM = LD.ID_ITEM_FK'
      ' WHERE LD.COD_ADICIONAL IS NOT NULL'
      'ORDER BY I.DESCR_ITEM')
    Left = 210
    Top = 159
  end
  object qryColab: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT ID_FUNCIONARIO, NOME_FUNCIONARIO, CPF'
      '  FROM FUNCIONARIO'
      'ORDER BY NOME_FUNCIONARIO')
    Left = 22
    Top = 62
  end
end
