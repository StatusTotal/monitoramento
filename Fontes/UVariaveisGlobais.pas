{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UVariaveisGlobais.pas
  Descricao:   Variaveis globais do Sistema
  Author   :   Cristina
  Date:        30-nov-2015
  Last Update: 18-mar-2019 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UVariaveisGlobais;

interface

uses System.SysUtils, System.Classes, System.Variants, System.DateUtils,
  System.VarUtils, FireDAC.Comp.Client;

var
  { O preenchimento dessas variaveis ficam nas seguintes units:
    - UDM (procedure DataModuleCreate)
    - ULogin (procedure btnEntrarClick)
    - UBibliotecaSistema (procedure PreencherVariaveisSistema)

    Sempre que forem efetuadas alteracoes ou acrescimos nessas variaveis,
    as mesmas devem ser atualizadas nessas units }

  { VARIAVEIS DO SISTEMA }
  //USUARIO
  vgUsu_Id                : Integer;
  vgUsu_Nome              : String;
  vgUsu_CPF               : String;
  vgUsu_Qualificacao      : String;
  vgUsu_FlgMaster         : String;
  vgUsu_FlgOficial        : String;
  vgUsu_FlgJustifEdicao   : String;
  vgUsu_Matricula         : String;
  vgUsu_FlgSuporte        : String;
  vgUsu_FlgVisualizouAtu  : String;
  vgUsu_FlgSexo           : String;
  vgUsu_IdFuncionario     : Integer;
  vgUsu_FlgAtivo          : String;
  vgUsu_FlgAssinaNegativa : String;
  vgUsu_DataCadastro      : TDatetime;
  vgUsu_DataInativacao    : TDatetime;


  //CONFIGURACOES
  vgConf_Atribuicao           : Integer;
  vgConf_Ambiente             : String;
  vgConf_CodServentia         : String;
  vgConf_NomeServentia        : String;
  vgConf_DiretorioDatabase    : String;
  vgConf_DiretorioImagens     : String;
  vgConf_DiretorioRelatorios  : String;
  vgConf_DiretorioCarneLeao   : String;
  vgConf_FlgExibeAtualizacao  : String;
  vgConf_FlgTrabalhaComissao  : String;
  vgConf_FlgBloqueiaFechPagto : String;
  vgConf_FlgExpedienteSabado  : String;
  vgConf_FlgExpedienteDomingo : String;
  vgConf_FlgExpedienteFeriado : String;
  vgConf_FlgZerarNumOficio    : String;
  vgConf_VersaoSync           : String;
  vgConf_IntervaloSync        : Integer;
  vgConf_DataIniImportacao    : TDateTime;
  vgConf_HoraFechamentoPadrao : String;
  vgConf_DiaFechamentoCaixa   : String;


  //SERVENTIA
  vgSrvn_IdServentia       : Integer;
  vgSrvn_CodServentia      : String;
  vgSrvn_NomeServentia     : String;
  vgSrvn_EndServentia      : String;
  vgSrvn_CidadeServentia   : String;
  vgSrvn_EstadoServentia   : String;
  vgSrvn_CEPServentia      : String;
  vgSrvn_TelServentia      : String;
  vgSrvn_EmailServentia    : String;
  vgSrvn_NomeTabeliao      : String;
  vgSrvn_MatriculaTabeliao : String;
  vgSrvn_NomeTabeliaoSubst : String;
  vgSrvn_CNPJServentia     : String;
  vgSrvn_CodMunicipioServ  : Integer;
  vgSrvn_TituloServentia   : String;


  //MODULO
  vgMod_Financeiro    : Boolean;
  vgMod_Oficios       : Boolean;
  vgMod_Folhas        : Boolean;
  vgMod_Etiquetas     : Boolean;
  vgMod_Relatorios    : Boolean;
  vgMod_Configuracoes : Boolean;


  //ATUALIZACAO DO SISTEMA
  vgAtS_IdAtualizacaoSist   : Integer;
  vgAtS_DataAtualizacaoSist : TDateTime;
  vgAtS_VersaoSistema       : String;


  //LANCAMENTOS (GERAL)
  vgLanc_Codigo     : Integer;
  vgLanc_Ano        : Integer;
  vgLanc_TpLanc     : String;
  vgLanc_CriarNovo  : Boolean;
  vgLanc_VlrTotPrev : Currency;
  vgLanc_VlrTotPago : Currency;


  //OFICIOS (GERAL)
  vgOfc_Codigo  : Integer;
  vgOfc_Ano     : Integer;
  vgOfc_TpOfc   : String;
  vgOfc_Status  : String;
  vgOfc_IdAssoc : Integer;


  //PERMISSAO
  { Cadastro }
  //Flutuantes - Baixa
  vgPrm_BxLanc   : Boolean;

  //Flutuantes - Depositos
  vgPrm_ConsDepo : Boolean;
  vgPrm_IncDepo  : Boolean;
  vgPrm_EdDepo   : Boolean;
  vgPrm_ExcDepo  : Boolean;
  vgPrm_ImpDepo  : Boolean;
  vgPrm_ExpDepo  : Boolean;
  vgPrm_ExpVincDepo : Boolean;

  //Categoria de Receitas
  vgPrm_ConsCatRec : Boolean;
  vgPrm_IncCatRec  : Boolean;
  vgPrm_EdCatRec   : Boolean;
  vgPrm_ExcCatRec  : Boolean;

  //Subcategoria de Receitas
  vgPrm_ConsSubcatRec : Boolean;
  vgPrm_IncSubcatRec  : Boolean;
  vgPrm_EdSubcatRec   : Boolean;
  vgPrm_ExcSubcatRec  : Boolean;

  //Receitas
  vgPrm_ConsRec : Boolean;
  vgPrm_IncRec  : Boolean;
  vgPrm_EdRec   : Boolean;
  vgPrm_ExcRec  : Boolean;
  vgPrm_ImpRec  : Boolean;
  vgPrm_ExpRec  : Boolean;
  vgPrm_RepRec  : Boolean;

  //Categoria de Despesas
  vgPrm_ConsCatDesp : Boolean;
  vgPrm_IncCatDesp  : Boolean;
  vgPrm_EdCatDesp   : Boolean;
  vgPrm_ExcCatDesp  : Boolean;

  //Subcategoria de Despesas
  vgPrm_ConsSubcatDesp : Boolean;
  vgPrm_IncSubcatDesp  : Boolean;
  vgPrm_EdSubcatDesp   : Boolean;
  vgPrm_ExcSubcatDesp  : Boolean;

  //Despesas
  vgPrm_ConsDesp : Boolean;
  vgPrm_IncDesp  : Boolean;
  vgPrm_EdDesp   : Boolean;
  vgPrm_ExcDesp  : Boolean;
  vgPrm_ImpDesp  : Boolean;
  vgPrm_ExpDesp  : Boolean;
  vgPrm_RepDesp  : Boolean;

  //Autorizacao de Lancamento de Despesa
  vgPrm_AutDesp : Boolean;

  //Formas de Pagamento
  vgPrm_ConsFPagto : Boolean;
  vgPrm_IncFPagto  : Boolean;
  vgPrm_EdFPagto   : Boolean;
  vgPrm_ExcFPagto  : Boolean;

  //Funcionarios
  vgPrm_ConsFunc : Boolean;
  vgPrm_IncFunc  : Boolean;
  vgPrm_EdFunc   : Boolean;
  vgPrm_ExcFunc  : Boolean;
  vgPrm_ImpFunc  : Boolean;
  vgPrm_ExpFunc  : Boolean;

  //Clientes
  vgPrm_ConsCli : Boolean;
  vgPrm_IncCli  : Boolean;
  vgPrm_EdCli   : Boolean;
  vgPrm_ExcCli  : Boolean;
  vgPrm_ImpCli  : Boolean;
  vgPrm_ExpCli  : Boolean;

  //Fornecedores
  vgPrm_ConsForn : Boolean;
  vgPrm_IncForn  : Boolean;
  vgPrm_EdForn   : Boolean;
  vgPrm_ExcForn  : Boolean;
  vgPrm_ImpForn  : Boolean;
  vgPrm_ExpForn  : Boolean;

  //Produtos
  vgPrm_ConsProd : Boolean;
  vgPrm_IncProd  : Boolean;
  vgPrm_EdProd   : Boolean;
  vgPrm_ExcProd  : Boolean;
  vgPrm_ImpProd  : Boolean;
  vgPrm_ExpProd  : Boolean;

  //Servicos
  vgPrm_ConsServ : Boolean;
  vgPrm_IncServ  : Boolean;
  vgPrm_EdServ   : Boolean;
  vgPrm_ExcServ  : Boolean;
  vgPrm_ImpServ  : Boolean;
  vgPrm_ExpServ  : Boolean;

  //Vales
  vgPrm_ConsVale : Boolean;
  vgPrm_IncVale  : Boolean;
  vgPrm_EdVale   : Boolean;
  vgPrm_ExcVale  : Boolean;
  vgPrm_ImpVale  : Boolean;
  vgPrm_ExpVale  : Boolean;

  //Outras Despesas de Funcionario
  vgPrm_ConsODespF : Boolean;
  vgPrm_IncODespF  : Boolean;
  vgPrm_EdODespF   : Boolean;
  vgPrm_ExcODespF  : Boolean;
  vgPrm_ImpODespF  : Boolean;
  vgPrm_ExpODespF  : Boolean;

  //Fechamento de Salarios
  vgPrm_ConsFecSal   : Boolean;
  vgPrm_RealFecSal   : Boolean;
  vgPrm_ImpFecSal    : Boolean;
  vgPrm_ExpFecSal    : Boolean;
  vgPrm_EdRealFecSal : Boolean;
  vgPrm_EdFecSal     : Boolean;
  vgPrm_ExcFecSal    : Boolean;

  //Fechamento de Comissoes
  vgPrm_ConsFecCom : Boolean;
  vgPrm_RealFecCom : Boolean;
  vgPrm_ImpFecCom  : Boolean;
  vgPrm_ExpFecCom  : Boolean;

  //Estoque
  vgPrm_ConsEst : Boolean;
  vgPrm_AceEst  : Boolean;
  vgPrm_ImpEst  : Boolean;
  vgPrm_ExpEst  : Boolean;

  //Naturezas
  vgPrm_ConsNat : Boolean;
  vgPrm_IncNat  : Boolean;
  vgPrm_EdNat   : Boolean;
  vgPrm_ExcNat  : Boolean;

  //Feriados
  vgPrm_ConsFer : Boolean;
  vgPrm_IncFer  : Boolean;
  vgPrm_EdFer   : Boolean;
  vgPrm_ExcFer  : Boolean;
  vgPrm_ImpFer  : Boolean;
  vgPrm_ExpFer  : Boolean;

  //Edicao de Lancamentos Pagos
  vgPrm_EdLancPago : Boolean;

  //Geracao Carne Leao
  vgPrm_ConsCLeao : Boolean;
  vgPrm_IncCLeao  : Boolean;
  vgPrm_EdCLeao   : Boolean;
  vgPrm_ExcCLeao  : Boolean;
  vgPrm_ImpCLeao  : Boolean;
  vgPrm_ExpCLeao  : Boolean;

  //Relacao de Carne Leao
  vgPrm_ConsRelacCL : Boolean;
  vgPrm_ConfRelacCL : Boolean;

  //Cargos
  vgPrm_ConsCrg : Boolean;
  vgPrm_IncCrg  : Boolean;
  vgPrm_EdCrg   : Boolean;
  vgPrm_ExcCrg  : Boolean;

  { Caixa }
  //Abertura
  vgPrm_ConsAbCx : Boolean;
  vgPrm_IncAbCx  : Boolean;
  vgPrm_EdAbCx   : Boolean;
  vgPrm_ExcAbCx  : Boolean;
  vgPrm_ImpAbCx  : Boolean;
  vgPrm_ExpAbCx  : Boolean;

  //Fechamento
  vgPrm_ConsFecCx : Boolean;
  vgPrm_IncFecCx  : Boolean;
  vgPrm_EdFecCx   : Boolean;
  vgPrm_ExcFecCx  : Boolean;
  vgPrm_ImpFecCx  : Boolean;
  vgPrm_ExpFecCx  : Boolean;

  //Movimento de Caixa
  vgPrm_ConsMovCx : Boolean;
  vgPrm_IncMovCx  : Boolean;
  vgPrm_EdMovCx   : Boolean;
  vgPrm_ExcMovCx  : Boolean;
  vgPrm_ImpMovCx  : Boolean;
  vgPrm_ExpMovCx  : Boolean;

  { Folhas }
  //Lote de Folhas de Seguranca
  vgPrm_ConsLtFlsSeg : Boolean;
  vgPrm_IncLtFlsSeg  : Boolean;
  vgPrm_EdLtFlsSeg   : Boolean;
  vgPrm_ExcLtFlsSeg  : Boolean;
  vgPrm_ImpLtFlsSeg  : Boolean;
  vgPrm_ExpLtFlsSeg  : Boolean;

  //Lote de Folhas RCPN
  vgPrm_ConsLtFlsRCPN : Boolean;
  vgPrm_IncLtFlsRCPN  : Boolean;
  vgPrm_EdLtFlsRCPN   : Boolean;
  vgPrm_ExcLtFlsRCPN  : Boolean;
  vgPrm_ImpLtFlsRCPN  : Boolean;
  vgPrm_ExpLtFlsRCPN  : Boolean;

  //Folhas de Seguranca
  vgPrm_ConsFlsSeg : Boolean;
  vgPrm_EdFlsSeg   : Boolean;
  vgPrm_ExcFlsSeg  : Boolean;
  vgPrm_ImpFlsSeg  : Boolean;
  vgPrm_ExpFlsSeg  : Boolean;

  //Folhas RCPN
  vgPrm_ConsFlsRCPN : Boolean;
  vgPrm_EdFlsRCPN   : Boolean;
  vgPrm_ExcFlsRCPN  : Boolean;
  vgPrm_ImpFlsRCPN  : Boolean;
  vgPrm_ExpFlsRCPN  : Boolean;

  { Etiquetas }
  //Lote de Etiquetas
  vgPrm_ConsLtEtq : Boolean;
  vgPrm_IncLtEtq  : Boolean;
  vgPrm_EdLtEtq   : Boolean;
  vgPrm_ExcLtEtq  : Boolean;
  vgPrm_ImpLtEtq  : Boolean;
  vgPrm_ExpLtEtq  : Boolean;

  //Etiquetas
  vgPrm_ConsEtq : Boolean;
  vgPrm_EdEtq   : Boolean;
  vgPrm_ExcEtq  : Boolean;
  vgPrm_ImpEtq  : Boolean;
  vgPrm_ExpEtq  : Boolean;

  { Oficios }
  vgPrm_ConsOficRec : Boolean;
  vgPrm_IncOficRec  : Boolean;
  vgPrm_EdOficRec   : Boolean;
  vgPrm_ExcOficRec  : Boolean;
  vgPrm_ImpOficRec  : Boolean;
  vgPrm_ExpOficRec  : Boolean;

  vgPrm_ConsOficEnv : Boolean;
  vgPrm_IncOficEnv  : Boolean;
  vgPrm_EdOficEnv   : Boolean;
  vgPrm_ExcOficEnv  : Boolean;
  vgPrm_ImpOficEnv  : Boolean;
  vgPrm_ExpOficEnv  : Boolean;

  { Relatorios }
  //Receitas x Despesas
  vgPrm_ConsRelRecDesp : Boolean;
  vgPrm_ImpRelRecDesp  : Boolean;
  vgPrm_ExpRelRecDesp  : Boolean;

  //Recibos x Selos
  vgPrm_ConsRelRecbSelo : Boolean;
  vgPrm_ImpRelRecbSelo  : Boolean;
  vgPrm_ExpRelRecbSelo  : Boolean;

  //Faturamento Mensal
  vgPrm_ConsRelFatMsl : Boolean;
  vgPrm_ImpRelFatMsl  : Boolean;
  vgPrm_ExpRelFatMsl  : Boolean;

  //Inadimplentes
  vgPrm_ConsRelInad : Boolean;
  vgPrm_ImpRelInad  : Boolean;
  vgPrm_ExpRelInad  : Boolean;

  //Extrato de Conta
  vgPrm_ConsRelExtCnt : Boolean;
  vgPrm_ImpRelExtCnt  : Boolean;
  vgPrm_ExpRelExtCnt  : Boolean;

  //Movimento de Caixa
  vgPrm_ConsRelMovCx : Boolean;
  vgPrm_ImpRelMovCx  : Boolean;
  vgPrm_ExpRelMovCx  : Boolean;

  //Recibos
  vgPrm_ConsRelRcb : Boolean;
  vgPrm_ImpRelRcb  : Boolean;
  vgPrm_ExpRelRcb  : Boolean;

  //Depositos Flutuantes
  vgPrm_ConsRelDepFlu : Boolean;
  vgPrm_ImpRelDepFlu  : Boolean;
  vgPrm_ExpRelDepFlu  : Boolean;

  //Depositos de Protestos para Repasse
  vgPrm_ConsRelRep : Boolean;
  vgPrm_ImpRelRep  : Boolean;
  vgPrm_ExpRelRep  : Boolean;

  //Despesas com Colaboradores
  vgPrm_ConsRelDespCol : Boolean;
  vgPrm_ImpRelDespCol  : Boolean;
  vgPrm_ExpRelDespCol  : Boolean;

  //Atos
  vgPrm_ConsRelAtos : Boolean;
  vgPrm_ImpRelAtos  : Boolean;
  vgPrm_ExpRelAtos  : Boolean;

  //Fechamento de Caixa por FP
  vgPrm_ConsRelFechFP : Boolean;
  vgPrm_ImpRelFechFP  : Boolean;
  vgPrm_ExpRelFechFP  : Boolean;

  //Fechamento de Caixa por TA
  vgPrm_ConsRelFechTA : Boolean;
  vgPrm_ImpRelFechTA  : Boolean;
  vgPrm_ExpRelFechTA  : Boolean;

  //Diferencas do Fechamento de Caixa
  vgPrm_ConsRelDifFechCx : Boolean;
  vgPrm_ImpRelDifFechCx  : Boolean;
  vgPrm_ExpRelDifFechCx  : Boolean;

  //Receitas e Despesas para o Imposto de Renda
  vgPrm_ConsRelRecDespIR : Boolean;
  vgPrm_ImpRelRecDespIR  : Boolean;
  vgPrm_ExpRelRecDespIR  : Boolean;

  //Recibos Cancelados
  vgPrm_ConsRelCancel : Boolean;
  vgPrm_ImpRelCancel  : Boolean;
  vgPrm_ExpRelCancel  : Boolean;

  { Configuracoes }
  //Usuarios
  vgPrm_ConsUsu   : Boolean;
  vgPrm_IncUsu    : Boolean;
  vgPrm_EdUsu     : Boolean;
  vgPrm_ExcUsu    : Boolean;
  vgPrm_ImpUsu    : Boolean;
  vgPrm_ExpUsu    : Boolean;

  //Log de Usuario
  vgPrm_ConsLogUsu : Boolean;
  vgPrm_ImpLogUsu  : Boolean;
  vgPrm_ExpLogUsu  : Boolean;

  //Sistema
  vgPrm_ConsConfigSis : Boolean;
  vgPrm_RealConfigSis : Boolean;

  //Permissoes
  vgPrm_ConsPerm : Boolean;
  vgPrm_ConfPerm : Boolean;

  //Atribuicoes da Serventia
  vgPrm_ConsAtribServ : Boolean;
  vgPrm_RealAtribServ : Boolean;

  //Pessoas
  vgPrm_ConsPes : Boolean;
  vgPrm_IncPes  : Boolean;
  vgPrm_EdPes   : Boolean;
  vgPrm_ExcPes  : Boolean;

  //Remetente e Destinatario de Email
  vgPrm_ConsRemDest : Boolean;
  vgPrm_IncRemDest  : Boolean;
  vgPrm_EdRemDest   : Boolean;
  vgPrm_ExcRemDest  : Boolean;

  //ATO
  vgAto_CodigoAto : Integer;
  vgAto_AnoAto    : Integer;
  vgAto_TipoAto   : Integer;

  //FECHAMENTOS DE MES
  vgDiasPagamento : Integer;
  vgOrigFechPend  : Boolean;

  //CAIXA
  vgSituacaoCaixa : String;

  vgDataUltAb   : TDatetime;
  vgHoraUltAb   : TTime;
  vgDataProxAb  : TDatetime;
  vgVlrNotasAb  : Currency;
  vgVlrMoedasAb : Currency;
  vgVlrContaAb  : Currency;
  vgVlrTotalAb  : Currency;
  vgIdMovAb     : Integer;

  vgDataUltFech   : TDatetime;
  vgHoraUltFech   : TTime;
  vgDataProxFech  : TDatetime;
  vgVlrNotasFech  : Currency;
  vgVlrMoedasFech : Currency;
  vgVlrContaFech  : Currency;
  vgVlrTotalFech  : Currency;
  vgIdMovFech     : Integer;

  vgDataLancVigente : TDateTime;

  //SISTEMAS TOTAL
  vgUsaTotalFirmas : Boolean;
  vgUsaTotalRecibo : Boolean;

implementation

uses UGDM;

end.
 