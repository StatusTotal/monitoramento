inherited FListaOficios: TFListaOficios
  Caption = 'Lista de Of'#237'cios'
  ClientWidth = 906
  OnCreate = FormCreate
  ExplicitWidth = 912
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 900
    ExplicitWidth = 900
    inherited pnlGrid: TPanel
      Top = 52
      Width = 894
      Height = 391
      ExplicitTop = 70
      ExplicitWidth = 894
      ExplicitHeight = 373
      inherited dbgGridListaPadrao: TDBGrid
        Width = 894
        Height = 391
        OnDblClick = dbgGridListaPadraoDblClick
        OnTitleClick = dbgGridListaPadraoTitleClick
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'ID'
            Title.Alignment = taCenter
            Title.Caption = 'N'#186' Controle'
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COD_OFICIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ANO_OFICIO'
            Visible = False
          end
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'NUM_OFICIO'
            Title.Alignment = taRightJustify
            Title.Caption = 'N'#186' Of'#237'cio'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TIPO_OFICIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_OFICIO'
            Title.Caption = 'Data'
            Width = 75
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'HORA_OFICIO'
            Title.Caption = 'Hora'
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CAD_ID_USUARIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_CADASTRO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_STATUS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'STA_ID_USUARIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_STATUS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ASSUNTO_OFICIO'
            Title.Caption = 'Assunto'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ID_OFICIO_PESSOA_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'NOME_OFICIO_PESSOA'
            Title.Caption = 'Destinat'#225'rio/Remetente'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NUM_PROCESSO'
            Title.Caption = 'N'#186' Processo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ID_OFICIO_ORGAO_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'NOME_OFICIO_ORGAO'
            Title.Caption = #211'rg'#227'o'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NUM_PROTOCOLO'
            Title.Caption = 'N'#186' Protocolo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ID_OFICIO_FORMAENVIO_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DESCR_OFICIO_FORMAENVIO'
            Title.Caption = 'Forma de Envio/Recebimento'
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NUM_RASTREIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_SITUACAO_AR'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_SITUACAO_AR'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_FUNCIONARIO_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'NOME_FUNCIONARIO'
            Title.Caption = 'Funcion'#225'rio'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OBS_OFICIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_OFICIO_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'SEQ_OFICIO'
            Visible = False
          end>
      end
    end
    inherited pnlFiltro: TPanel
      Width = 900
      Height = 49
      ExplicitWidth = 900
      ExplicitHeight = 49
      inherited btnFiltrar: TJvTransparentButton
        Left = 725
        Top = 20
        OnClick = btnFiltrarClick
        ExplicitLeft = 725
        ExplicitTop = 20
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 814
        Top = 20
        ExplicitLeft = 814
        ExplicitTop = 20
      end
      object lblPeriodo: TLabel
        Left = 3
        Top = 8
        Width = 151
        Height = 14
        Caption = 'Per'#237'odo Envio/Recebimento'
      end
      object Label2: TLabel
        Left = 106
        Top = 27
        Width = 6
        Height = 14
        Caption = 'a'
      end
      object dtePeriodoIni: TJvDateEdit
        Left = 3
        Top = 24
        Width = 100
        Height = 22
        Color = 16114127
        ShowNullDate = False
        TabOrder = 0
        OnKeyPress = dtePeriodoIniKeyPress
      end
      object dtePeriodoFim: TJvDateEdit
        Left = 115
        Top = 24
        Width = 100
        Height = 22
        Color = 16114127
        ShowNullDate = False
        TabOrder = 1
        OnKeyPress = dtePeriodoFimKeyPress
      end
    end
  end
  inherited dsGridListaPadrao: TDataSource
    Left = 828
  end
  inherited qryGridListaPadrao: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT O.ID_OFICIO AS ID,'
      '       O.COD_OFICIO,'
      '       O.ANO_OFICIO,'
      '       (O.COD_OFICIO || '#39'/'#39' || O.ANO_OFICIO) AS NUM_OFICIO,'
      '       O.TIPO_OFICIO,'
      '       O.DATA_OFICIO,'
      '       O.HORA_OFICIO,'
      '       O.ID_FUNCIONARIO_FK,'
      '       F.NOME_FUNCIONARIO,'
      '       O.CAD_ID_USUARIO,'
      '       O.DATA_CADASTRO,'
      '       O.FLG_STATUS,'
      '       O.STA_ID_USUARIO,'
      '       O.DATA_STATUS,'
      '       O.ASSUNTO_OFICIO,'
      '       O.ID_OFICIO_PESSOA_FK,'
      '       OP.NOME_OFICIO_PESSOA,'
      '       O.NUM_PROCESSO,'
      '       O.ID_OFICIO_ORGAO_FK,'
      '       OO.NOME_OFICIO_ORGAO,'
      '       O.ID_OFICIO_FORMAENVIO_FK,'
      '       OFE.DESCR_OFICIO_FORMAENVIO,'
      '       O.NUM_RASTREIO,'
      '       O.NUM_PROTOCOLO,'
      '       O.DATA_SITUACAO_AR,'
      '       O.FLG_SITUACAO_AR,'
      '       O.OBS_OFICIO,'
      '       O.ID_OFICIO_FK,'
      '       O.SEQ_OFICIO'
      '  FROM OFICIO O'
      '  LEFT JOIN FUNCIONARIO F'
      '    ON O.ID_FUNCIONARIO_FK = F.ID_FUNCIONARIO'
      '  LEFT JOIN OFICIO_PESSOA OP'
      '    ON O.ID_OFICIO_PESSOA_FK = OP.ID_OFICIO_PESSOA'
      '  LEFT JOIN OFICIO_ORGAO OO'
      '    ON O.ID_OFICIO_ORGAO_FK = OO.ID_OFICIO_ORGAO'
      '  LEFT JOIN OFICIO_FORMAENVIO OFE'
      '    ON O.ID_OFICIO_FORMAENVIO_FK = OFE.ID_OFICIO_FORMAENVIO'
      ' WHERE O.DATA_OFICIO BETWEEN :DATA_INI AND :DATA_FIM'
      '   AND O.TIPO_OFICIO = :TIPO_OFICIO'
      'ORDER BY O.ID_OFICIO,'
      '         O.DATA_OFICIO,'
      '         O.HORA_OFICIO')
    Left = 828
    ParamData = <
      item
        Name = 'DATA_INI'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'DATA_FIM'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'TIPO_OFICIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object qryGridListaPadraoID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID_OFICIO'
      Required = True
    end
    object qryGridListaPadraoCOD_OFICIO: TIntegerField
      FieldName = 'COD_OFICIO'
      Origin = 'COD_OFICIO'
    end
    object qryGridListaPadraoANO_OFICIO: TIntegerField
      FieldName = 'ANO_OFICIO'
      Origin = 'ANO_OFICIO'
    end
    object qryGridListaPadraoNUM_OFICIO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NUM_OFICIO'
      Origin = 'NUM_OFICIO'
      ProviderFlags = []
      ReadOnly = True
      Size = 23
    end
    object qryGridListaPadraoTIPO_OFICIO: TStringField
      FieldName = 'TIPO_OFICIO'
      Origin = 'TIPO_OFICIO'
      FixedChar = True
      Size = 1
    end
    object qryGridListaPadraoDATA_OFICIO: TDateField
      FieldName = 'DATA_OFICIO'
      Origin = 'DATA_OFICIO'
    end
    object qryGridListaPadraoHORA_OFICIO: TTimeField
      FieldName = 'HORA_OFICIO'
      Origin = 'HORA_OFICIO'
    end
    object qryGridListaPadraoID_FUNCIONARIO_FK: TIntegerField
      FieldName = 'ID_FUNCIONARIO_FK'
      Origin = 'ID_FUNCIONARIO_FK'
    end
    object qryGridListaPadraoNOME_FUNCIONARIO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOME_FUNCIONARIO'
      Origin = 'NOME_FUNCIONARIO'
      ProviderFlags = []
      ReadOnly = True
      Size = 250
    end
    object qryGridListaPadraoCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
      Origin = 'CAD_ID_USUARIO'
    end
    object qryGridListaPadraoDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
      Origin = 'DATA_CADASTRO'
    end
    object qryGridListaPadraoFLG_STATUS: TStringField
      FieldName = 'FLG_STATUS'
      Origin = 'FLG_STATUS'
      FixedChar = True
      Size = 1
    end
    object qryGridListaPadraoSTA_ID_USUARIO: TIntegerField
      FieldName = 'STA_ID_USUARIO'
      Origin = 'STA_ID_USUARIO'
    end
    object qryGridListaPadraoDATA_STATUS: TSQLTimeStampField
      FieldName = 'DATA_STATUS'
      Origin = 'DATA_STATUS'
    end
    object qryGridListaPadraoASSUNTO_OFICIO: TStringField
      FieldName = 'ASSUNTO_OFICIO'
      Origin = 'ASSUNTO_OFICIO'
      Size = 250
    end
    object qryGridListaPadraoID_OFICIO_PESSOA_FK: TIntegerField
      FieldName = 'ID_OFICIO_PESSOA_FK'
      Origin = 'ID_OFICIO_PESSOA_FK'
    end
    object qryGridListaPadraoNOME_OFICIO_PESSOA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOME_OFICIO_PESSOA'
      Origin = 'NOME_OFICIO_PESSOA'
      ProviderFlags = []
      ReadOnly = True
      Size = 250
    end
    object qryGridListaPadraoNUM_PROCESSO: TStringField
      FieldName = 'NUM_PROCESSO'
      Origin = 'NUM_PROCESSO'
      Size = 25
    end
    object qryGridListaPadraoID_OFICIO_ORGAO_FK: TIntegerField
      FieldName = 'ID_OFICIO_ORGAO_FK'
      Origin = 'ID_OFICIO_ORGAO_FK'
    end
    object qryGridListaPadraoNOME_OFICIO_ORGAO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOME_OFICIO_ORGAO'
      Origin = 'NOME_OFICIO_ORGAO'
      ProviderFlags = []
      ReadOnly = True
      Size = 250
    end
    object qryGridListaPadraoID_OFICIO_FORMAENVIO_FK: TIntegerField
      FieldName = 'ID_OFICIO_FORMAENVIO_FK'
      Origin = 'ID_OFICIO_FORMAENVIO_FK'
    end
    object qryGridListaPadraoDESCR_OFICIO_FORMAENVIO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DESCR_OFICIO_FORMAENVIO'
      Origin = 'DESCR_OFICIO_FORMAENVIO'
      ProviderFlags = []
      ReadOnly = True
      Size = 150
    end
    object qryGridListaPadraoNUM_RASTREIO: TStringField
      FieldName = 'NUM_RASTREIO'
      Origin = 'NUM_RASTREIO'
      Size = 25
    end
    object qryGridListaPadraoNUM_PROTOCOLO: TStringField
      FieldName = 'NUM_PROTOCOLO'
      Origin = 'NUM_PROTOCOLO'
      Size = 25
    end
    object qryGridListaPadraoDATA_SITUACAO_AR: TDateField
      FieldName = 'DATA_SITUACAO_AR'
      Origin = 'DATA_SITUACAO_AR'
    end
    object qryGridListaPadraoFLG_SITUACAO_AR: TStringField
      FieldName = 'FLG_SITUACAO_AR'
      Origin = 'FLG_SITUACAO_AR'
      FixedChar = True
      Size = 1
    end
    object qryGridListaPadraoOBS_OFICIO: TStringField
      FieldName = 'OBS_OFICIO'
      Origin = 'OBS_OFICIO'
      Size = 1000
    end
    object qryGridListaPadraoID_OFICIO_FK: TIntegerField
      FieldName = 'ID_OFICIO_FK'
      Origin = 'ID_OFICIO_FK'
    end
    object qryGridListaPadraoSEQ_OFICIO: TIntegerField
      FieldName = 'SEQ_OFICIO'
      Origin = 'SEQ_OFICIO'
    end
  end
end
