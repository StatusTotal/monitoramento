{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroMovimentacaoCaixa.pas
  Descricao:   Tela de filtro de Movimentacao de Caixa
  Author   :   Cristina
  Date:        29-dez-2016
  Last Update: 16-out-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroMovimentacaoCaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Mask, JvExMask, JvToolEdit, System.DateUtils,
  System.StrUtils, Vcl.ComCtrls, JvExComCtrls, JvMonthCalendar;

type
  TFFiltroMovimentacaoCaixa = class(TFFiltroSimplesPadrao)
    lblData: TLabel;
    qryGridPaiPadraoID: TIntegerField;
    qryGridPaiPadraoDATA_MOVIMENTO: TDateField;
    qryGridPaiPadraoDESCR_MOVIMENTO: TStringField;
    qryGridPaiPadraoTIPO_ORIGEM: TStringField;
    qryGridPaiPadraoID_ORIGEM: TIntegerField;
    qryGridPaiPadraoNUM_RECIBO: TIntegerField;
    qryGridPaiPadraoTIPO_LANCAMENTO: TStringField;
    qryGridPaiPadraoFLG_CANCELADO: TStringField;
    dteData: TJvDateEdit;
    procedure btnLimparClick(Sender: TObject);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure dbgGridPaiPadraoTitleClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryGridPaiPadraoCalcFields(DataSet: TDataSet);
    procedure dteDataKeyPress(Sender: TObject; var Key: Char);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }

    procedure AbrirListaMovimentos;
  public
    { Public declarations }
  end;

var
  FFiltroMovimentacaoCaixa: TFFiltroMovimentacaoCaixa;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMCaixa, UDMLancamento,
  UCadastroAberturaCaixa, UCadastroFechamentoCaixa, UCadastroLancamento;

{ TFFiltroMovimentacaoCaixa }

procedure TFFiltroMovimentacaoCaixa.AbrirListaMovimentos;
begin
  qryGridPaiPadrao.Close;
  qryGridPaiPadrao.Params.ParamByName('DATA_INI').Value := dteData.Date;
  qryGridPaiPadrao.Params.ParamByName('DATA_FIM').Value := dteData.Date;
  qryGridPaiPadrao.Open;

  VerificarPermissoes;
end;

procedure TFFiltroMovimentacaoCaixa.btnFiltrarClick(Sender: TObject);
begin
  inherited;

  AbrirListaMovimentos;
end;

procedure TFFiltroMovimentacaoCaixa.btnImprimirClick(Sender: TObject);
begin
  inherited;

  dmPrincipal.AbrirRelatorio(9, False, dteData.Date);
end;

procedure TFFiltroMovimentacaoCaixa.btnLimparClick(Sender: TObject);
begin
  dteData.Date := Date;

  inherited;

  if dteData.CanFocus then
    dteData.SetFocus;
end;

procedure TFFiltroMovimentacaoCaixa.dbgGridPaiPadraoDblClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao   := C;
  vgIdConsulta := qryGridPaiPadrao.FieldByName('ID').AsInteger;

  case AnsiIndexStr(UpperCase(qryGridPaiPadrao.FieldByName('TIPO_ORIGEM').AsString), ['P', 'A', 'F']) of
    0:  //Parcela
    begin
      try
        Application.CreateForm(TdmLancamento, dmLancamento);
        Application.CreateForm(TFCadastroLancamento, FCadastroLancamento);

        if qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString = 'D' then
          FCadastroLancamento.TipoLanc := DESP
        else
          FCadastroLancamento.TipoLanc := REC;

        FCadastroLancamento.ShowModal;
      finally
        FreeAndNil(dmLancamento);
        FCadastroLancamento.Free;
      end;
    end;
    1:  //Abertura
    begin
      Application.CreateForm(TdmCaixa, dmCaixa);
      dmGerencial.CriarForm(TFCadastroAberturaCaixa, FCadastroAberturaCaixa);
    end;
    2:  //Fechamento
    begin
      Application.CreateForm(TdmCaixa, dmCaixa);
      dmGerencial.CriarForm(TFCadastroAberturaCaixa, FCadastroAberturaCaixa);
    end;
  end;

  DefinirPosicaoGrid;
end;

procedure TFFiltroMovimentacaoCaixa.dbgGridPaiPadraoTitleClick(Column: TColumn);
begin
  inherited;

  qryGridPaiPadrao.IndexFieldNames := Column.FieldName;
end;

procedure TFFiltroMovimentacaoCaixa.dteDataKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
     if dteData.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data do Movimento', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteData.CanFocus then
        dteData.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dteData.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data do Movimento', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteData.CanFocus then
        dteData.SetFocus;
    end
    else
      btnFiltrar.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroMovimentacaoCaixa.FormCreate(Sender: TObject);
begin
  inherited;

  dteData.Date := Date;

  AbrirListaMovimentos;
end;

procedure TFFiltroMovimentacaoCaixa.FormShow(Sender: TObject);
begin
  inherited;

  btnIncluir.Visible := False;
  btnEditar.Visible  := False;
  btnExcluir.Visible := False;

  if dteData.CanFocus then
    dteData.SetFocus;
end;

procedure TFFiltroMovimentacaoCaixa.GravarLog;
begin
  inherited;

  case vgOperacao of
    P:  //IMPRESSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente impress�o de Lista de Aberturas de Caixa',
                          0,
                          '');
    end;
    T:  //EXPORTACAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente exporta��o de Lista de Aberturas de Caixa',
                          0,
                          '');
    end;
  end;
end;

function TFFiltroMovimentacaoCaixa.PodeExcluir(var Msg: String): Boolean;
begin
  Result := False;
end;

procedure TFFiltroMovimentacaoCaixa.qryGridPaiPadraoCalcFields(
  DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with qryAux, SQL do
  begin
    //Tipo de Lancamento
    if (qryGridPaiPadrao.FieldByName('ID_ORIGEM').AsInteger > 0) and
      (qryGridPaiPadrao.FieldByName('TIPO_ORIGEM').AsString = 'P') then
    begin
      Close;
      Clear;
      Text := 'SELECT L.TIPO_LANCAMENTO ' +
              '  FROM LANCAMENTO L ' +
              ' INNER JOIN LANCAMENTO_PARC LP' +
              '    ON L.COD_LANCAMENTO = LP.COD_LANCAMENTO_FK ' +
              '   AND L.ANO_LANCAMENTO = LP.ANO_LANCAMENTO_FK' +
              ' WHERE LP.ID_LANCAMENTO_PARC = :ID_LANCAMENTO_PARC';
      Params.ParamByName('ID_LANCAMENTO_PARC').AsInteger := qryGridPaiPadrao.FieldByName('ID_ORIGEM').AsInteger;
      Open;

      if qryAux.RecordCount > 0 then
        qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString := qryAux.FieldByName('TIPO_LANCAMENTO').AsString;
    end
    else
      qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString := '-';
  end;

  FreeAndNil(qryAux);
end;

procedure TFFiltroMovimentacaoCaixa.VerificarPermissoes;
begin
  inherited;

  btnIncluir.Enabled  := False;  //vgPrm_IncMovCx;
  btnEditar.Enabled   := False;  //vgPrm_EdMovCx and (qryGridPaiPadrao.RecordCount > 0);
  btnExcluir.Enabled  := False;  //vgPrm_ExcMovCx and (qryGridPaiPadrao.RecordCount > 0);
  btnImprimir.Enabled := vgPrm_ImpMovCx and (qryGridPaiPadrao.RecordCount > 0);
end;

end.
