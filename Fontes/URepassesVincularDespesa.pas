{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   URepassesVincularDespesa.pas
  Descricao:   Formulario de Vinculo de Repasse de Receitas com Despesa Preexistente
  Author   :   Cristina
  Date:        24-nov-2017
  Last Update: 08-mar-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit URepassesVincularDespesa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Data.DB, Vcl.Grids,
  Vcl.DBGrids, JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls,
  JvBaseEdits, Vcl.Mask, JvExMask, JvToolEdit, Vcl.DBCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, System.DateUtils, System.StrUtils, JvExDBGrids, JvDBGrid,
  JvDBUltimGrid;

type
  TFRepassesVincularDespesa = class(TForm)
    pnlRepasse: TPanel;
    gbRepasses: TGroupBox;
    gbDespesas: TGroupBox;
    lblPeriodo: TLabel;
    dtePeriodoIniR: TJvDateEdit;
    Label2: TLabel;
    dtePeriodoFimR: TJvDateEdit;
    cedValorFlutuanteR: TJvCalcEdit;
    lblValorFlutuante: TLabel;
    edtNumReciboFlutuante: TEdit;
    lblNumReciboFlutuante: TLabel;
    rgSituacao: TRadioGroup;
    btnFiltrarReceitas: TJvTransparentButton;
    Label1: TLabel;
    dtePeriodoIniD: TJvDateEdit;
    Label3: TLabel;
    dtePeriodoFimD: TJvDateEdit;
    cedValorFlutuanteD: TJvCalcEdit;
    Label4: TLabel;
    btnFiltrarDespesas: TJvTransparentButton;
    dbgDespesasPagas: TDBGrid;
    lblObservacoes: TLabel;
    btnConfirmarBaixa: TJvTransparentButton;
    btnCancelarBaixa: TJvTransparentButton;
    lblDataPagamento: TLabel;
    lblDescricaoBaixa: TLabel;
    lblValorTotalFlutuante: TLabel;
    lblValorTotalDespesa: TLabel;
    edtDescricaoBaixa: TEdit;
    dteDataPagamento: TJvDateEdit;
    cedValorTotalRepasse: TJvCalcEdit;
    cedValorTotalDespesa: TJvCalcEdit;
    qryQtdParcs: TFDQuery;
    qryQtdParcsQTD: TIntegerField;
    qryQtdParcsTOTAL_PREV: TBCDField;
    qryQtdParcsTOTAL_PAGO: TBCDField;
    dsQtdParcs: TDataSource;
    dsDespPagas: TDataSource;
    btnConfirmarDespesa: TJvTransparentButton;
    dsBaixaLancG: TDataSource;
    chbSelecionarTodas: TCheckBox;
    mmObservacoes: TMemo;
    spLegendaVencido: TShape;
    lblLegendaSemBaixa: TLabel;
    spLegendaPago: TShape;
    lblLegendaBaixado: TLabel;
    dbgRepasses: TJvDBUltimGrid;
    procedure FormShow(Sender: TObject);
    procedure btnFiltrarReceitasClick(Sender: TObject);
    procedure btnFiltrarDespesasClick(Sender: TObject);
    procedure cedValorFlutuanteRKeyPress(Sender: TObject; var Key: Char);
    procedure cedValorFlutuanteDKeyPress(Sender: TObject; var Key: Char);
    procedure chbSelecionarTodasClick(Sender: TObject);
    procedure dteDataPagamentoExit(Sender: TObject);
    procedure dteDataPagamentoKeyPress(Sender: TObject; var Key: Char);
    procedure dtePeriodoFimRKeyPress(Sender: TObject; var Key: Char);
    procedure dtePeriodoFimDKeyPress(Sender: TObject; var Key: Char);
    procedure dtePeriodoIniRKeyPress(Sender: TObject; var Key: Char);
    procedure dtePeriodoIniDKeyPress(Sender: TObject; var Key: Char);
    procedure edtNumReciboFlutuanteKeyPress(Sender: TObject; var Key: Char);
    procedure mmObservacoesKeyPress(Sender: TObject; var Key: Char);
    procedure rgSituacaoClick(Sender: TObject);
    procedure rgSituacaoExit(Sender: TObject);
    procedure dbgDespesasPagasDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgDespesasPagasTitleClick(Column: TColumn);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btnConfirmarDespesaClick(Sender: TObject);
    procedure btnConfirmarBaixaClick(Sender: TObject);
    procedure btnCancelarBaixaClick(Sender: TObject);
    procedure dbgRepassesCellClick(Column: TColumn);
    procedure dbgRepassesDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgRepassesTitleClick(Column: TColumn);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }

    sNovaObservacao: String;

    procedure MarcarDesmarcarTodas;
    procedure SalvarBoleano;
  public
    { Public declarations }
  end;

var
  FRepassesVincularDespesa: TFRepassesVincularDespesa;

implementation

{$R *.dfm}

uses UVariaveisGlobais, UBibliotecaSistema, UDM, UGDM, UDMBaixa,
  UOpcoesAcaoBaixaLancGrupo, UDMLancamento;

procedure TFRepassesVincularDespesa.btnCancelarBaixaClick(Sender: TObject);
begin
  chbSelecionarTodas.Checked := False;

  MarcarDesmarcarTodas;

  cedValorTotalDespesa.Value := 0;
  dteDataPagamento.Date      := 0;
  edtNumReciboFlutuante.Clear;
  mmObservacoes.Clear;
end;

procedure TFRepassesVincularDespesa.btnConfirmarBaixaClick(Sender: TObject);
var
  sMsg: String;
  iCont, iQtdTotParcs, iQtdParcsPagas, iPos, IdDetalhe, m: Integer;
  sLstFlut: String;
  cVlrTotPago, cVlrTotPrev, cTotPrev: Currency;
  lFinalizar, lIncParcelas, lOk: Boolean;
  QryLancDeriv, QryAux, QryVinc: TFDQuery;
  IdNat, Cod, IdCons, Ano: Integer;
  Descr, Obs, sIR, sCP, sLA: String;
  Op: TOperacao;
  OrigF, OrigC: Boolean;
begin
  inherited;


  if dteDataPagamento.Date = 0 then
  begin
    if Application.MessageBox('Por favor, informe a Data de Pagamento.',
                              'Aviso',
                              MB_OK + MB_ICONWARNING) = ID_NO then
      Exit;
  end;

  iCont := 0;
  cTotPrev := 0;
  iPos := 0;

  sNovaObservacao := '';

  sMsg := '';
  dmBaixa.sTipoBaixa := '';
  { '' = Nenhuma / S = Sobra de Caixa / F = Falta / P = Nova Parcela }

  dmBaixa.cDifTot := 0;

  lFinalizar := False;
  lIncParcelas := False;
  lOk := False;

  QryLancDeriv := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
  QryAux       := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
  QryVinc      := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  if cedValorTotalDespesa.Value <= 0 then
  begin
    if Application.MessageBox('A Despesa equivalente n�o foi selecionada.',
                              'Aviso',
                              MB_OK + MB_ICONWARNING) = ID_NO then
      Exit;
  end;

  iPos := dbgRepasses.DataSource.DataSet.RecNo;

  dmBaixa.cdsBaixaLancG.First;

  while not dmBaixa.cdsBaixaLancG.Eof do
  begin

    if dmBaixa.cdsBaixaLancG.FieldByName('SELECIONADO').AsBoolean then
    begin
      if Trim(sLstFlut) = '' then
      begin
        sLstFlut := dmBaixa.cdsBaixaLancG.FieldByName('COD_LANCAMENTO').AsString + '/' +
                    dmBaixa.cdsBaixaLancG.FieldByName('ANO_LANCAMENTO').AsString +
                    ' (Recibo: ' + dmBaixa.cdsBaixaLancG.FieldByName('NUM_RECIBO').AsString + ')';
      end
      else
      begin
        sLstFlut := (sLstFlut + #13#10 + dmBaixa.cdsBaixaLancG.FieldByName('COD_LANCAMENTO').AsString + '/' +
                     dmBaixa.cdsBaixaLancG.FieldByName('ANO_LANCAMENTO').AsString + ' (Recibo: ' +
                     dmBaixa.cdsBaixaLancG.FieldByName('NUM_RECIBO').AsString + ')');
      end;

      Inc(iCont);

      cTotPrev := (cTotPrev + dmBaixa.cdsBaixaLancG.FieldByName('VR_PARCELA_PREV').AsCurrency);
    end;

    dmBaixa.cdsBaixaLancG.Next;
  end;

  dbgRepasses.DataSource.DataSet.RecNo := iPos;

  if iCont = 0 then
    Application.MessageBox('Nenhum Repasse selecionado.', 'Aviso', MB_OK + MB_ICONWARNING)
  else
  begin
    if iCont = 1 then
      sMsg := 'Confirma a sele��o do Repasse abaixo? ' + #13#10 + sLstFlut
    else
      sMsg := 'Confirma a sele��o dos Repasses abaixo? ' + #13#10 + sLstFlut;

    if Application.MessageBox(PChar(sMsg), 'Aviso', MB_YESNO + MB_ICONQUESTION) = ID_YES then
    begin
      lFinalizar := True;

      cVlrTotPrev := cedValorTotalRepasse.Value;
      cVlrTotPago := cedValorTotalDespesa.Value;

      if cVlrTotPago < cVlrTotPrev then
      begin
        // Sobra de Caixa
        Application.MessageBox
          ('O valor total da Despesa est� MENOR que o Valor previsto para a Parcela Flutuante. Isso configura uma SOBRA de Valor em Caixa.',
          'Aviso', MB_OK);

        dmBaixa.cDifTot := (cVlrTotPrev - cVlrTotPago);
        dmBaixa.sTipoBaixa := 'S';
      end
      else if cVlrTotPago > cVlrTotPrev then
      begin
        // Nova Parcela
        Application.MessageBox
          ('O valor total da Despesa est� MAIOR que o Valor previsto para a Parcela Flutuante. Isso configura uma FALTA de Valor em Caixa.',
          'Aviso', MB_OK);

        dmBaixa.cDifTot := (cVlrTotPago - cVlrTotPrev);
        dmBaixa.sTipoBaixa := 'F';
      end;

      if cVlrTotPago <> cVlrTotPrev then
      begin
        // Solicitar ao Usuario a forma como ele deseja tratar a diferenca de valor
        Op := vgOperacao;
        IdCons := vgIdConsulta;
        OrigF := vgOrigemFiltro;
        OrigC := vgOrigemCadastro;

        vgOperacao := I;
        vgIdConsulta := 0;
        vgOrigemFiltro := False;
        vgOrigemCadastro := True;

        try
          Application.CreateForm(TFOpcoesAcaoBaixaLancGrupo, FOpcoesAcaoBaixaLancGrupo);
          FOpcoesAcaoBaixaLancGrupo.ShowModal;
        finally
          sNovaObservacao := FOpcoesAcaoBaixaLancGrupo.sObservacao;
          FOpcoesAcaoBaixaLancGrupo.Free;
        end;

        vgOperacao := Op;
        vgIdConsulta := IdCons;
        vgOrigemFiltro := OrigF;
        vgOrigemCadastro := OrigC;
      end;

      // Finalizar Baixa e gravar tabela BAIXA_LANC_DERIVADO
      if lFinalizar then
      begin
        dmBaixa.cdsBaixaLancG.First;

        while not dmBaixa.cdsBaixaLancG.Eof do
        begin
          if dmBaixa.cdsBaixaLancG.FieldByName('SELECIONADO').AsBoolean then
          begin
            lIncParcelas := False;

            try
              if vgConSISTEMA.Connected then
                vgConSISTEMA.StartTransaction;

              QryLancDeriv.Close;
              QryLancDeriv.SQL.Clear;
              QryLancDeriv.SQL.Text := 'INSERT INTO BAIXA_LANC_DERIVADO (ID_BAIXA_LANC_DERIVADO, ' +
                                       '                                 DATA_BAIXA_LANC_DERIVADO, ' +
                                       '                                 DESCR_BAIXA_LANC_DERIVADO, ' +
                                       '                                 ORIG_COD_LANCAMENTO_FK, ' +
                                       '                                 ORIG_ANO_LANCAMENTO_FK, ' +
                                       '                                 DERIV_COD_LANCAMENTO_FK, ' +
                                       '                                 DERIV_ANO_LANCAMENTO_FK, ' +
                                       '                                 OBS_BAIXA_LANC_DERIVADO) ' +
                                       '                         VALUES (:ID_BAIXA_LANC_DERIVADO, ' +
                                       '                                 :DATA_BAIXA_LANC_DERIVADO, ' +
                                       '                                 :DESCR_BAIXA_LANC_DERIVADO, ' +
                                       '                                 :ORIG_COD_LANCAMENTO_FK, ' +
                                       '                                 :ORIG_ANO_LANCAMENTO_FK, ' +
                                       '                                 :DERIV_COD_LANCAMENTO_FK, ' +
                                       '                                 :DERIV_ANO_LANCAMENTO_FK, ' +
                                       '                                 :OBS_BAIXA_LANC_DERIVADO)';

              QryLancDeriv.Params.ParamByName('ID_BAIXA_LANC_DERIVADO').Value    := BS.ProximoId('ID_BAIXA_LANC_DERIVADO', 'BAIXA_LANC_DERIVADO');
              QryLancDeriv.Params.ParamByName('DESCR_BAIXA_LANC_DERIVADO').Value := Trim(edtDescricaoBaixa.Text);
              QryLancDeriv.Params.ParamByName('ORIG_COD_LANCAMENTO_FK').Value    := dmBaixa.cdsBaixaLancG.FieldByName('COD_LANCAMENTO').AsInteger;
              QryLancDeriv.Params.ParamByName('ORIG_ANO_LANCAMENTO_FK').Value    := dmBaixa.cdsBaixaLancG.FieldByName('ANO_LANCAMENTO').AsInteger;
              QryLancDeriv.Params.ParamByName('DERIV_COD_LANCAMENTO_FK').Value   := dmBaixa.cdsDespPagas.FieldByName('COD_LANCAMENTO_FK').AsInteger;
              QryLancDeriv.Params.ParamByName('DERIV_ANO_LANCAMENTO_FK').Value   := dmBaixa.cdsDespPagas.FieldByName('ANO_LANCAMENTO_FK').AsInteger;
              QryLancDeriv.Params.ParamByName('OBS_BAIXA_LANC_DERIVADO').Value   := Trim(mmObservacoes.Text);

              if Date < vgDataLancVigente then
                QryLancDeriv.Params.ParamByName('DATA_BAIXA_LANC_DERIVADO').Value := vgDataLancVigente
              else
                QryLancDeriv.Params.ParamByName('DATA_BAIXA_LANC_DERIVADO').Value := Date;

              QryLancDeriv.ExecSQL;

              if vgConSISTEMA.InTransaction then
                vgConSISTEMA.Commit;

              lOk := True;
            except
              if vgConSISTEMA.InTransaction then
                vgConSISTEMA.Rollback;
            end;

            try
              if vgConSISTEMA.Connected then
                vgConSISTEMA.StartTransaction;

              //Sobra de Caixa
              if ((Trim(dmBaixa.sTipoBaixa) = 'S') or
                (Trim(dmBaixa.sTipoBaixa) = 'F')) and (dmBaixa.cDifTot > 0) then
              begin
                //Natureza
                IdNat := 0;
                Cod := 0;
                Descr := '';
                Obs := '';
                sIR := '';
                sCP := '';
                sLA := '';

                IdNat := 0;
                Obs := 'Inclus�o AUTOM�TICA de Natureza';

                if Trim(dmBaixa.sTipoBaixa) = 'S' then
                begin
                  Cod := 2; // Receita
                  Descr := 'SOBRA DE CAIXA';
                end
                else if Trim(dmBaixa.sTipoBaixa) = 'F' then
                begin
                  Cod := 1; // Receita
                  Descr := 'FALTA DE CAIXA';
                end;

                dmPrincipal.RetornaNatureza(IdNat, Cod, Descr, Obs);
                dmPrincipal.RetornarClassificacaoNatureza(IdNat, sIR, sCP, sLA);

                dmPrincipal.InicializarComponenteLancamento;

                //Lancamento
                DadosLancamento := Lancamento.Create;

{                if dmPrincipal.qryFechCx.RecordCount = 0 then
                  DadosLancamento.DataLancamento := Now
                else
                  DadosLancamento.DataLancamento := IncDay(Now, 1);  }

                DadosLancamento.DataLancamento := dteDataPagamento.Date;

                if Trim(dmBaixa.sTipoBaixa) = 'S' then
                  DadosLancamento.TipoLancamento := 'R'
                else if Trim(dmBaixa.sTipoBaixa) = 'F' then
                  DadosLancamento.TipoLancamento := 'D';

                DadosLancamento.TipoCadastro     := 'A';
                DadosLancamento.FlgReal          := 'S';
                DadosLancamento.FlgIR            := sIR;
                DadosLancamento.FlgPessoal       := sCP;
                DadosLancamento.FlgFlutuante     := 'N';
                DadosLancamento.FlgAuxiliar      := sLA;
                DadosLancamento.FlgForaFechCaixa := 'N';
                DadosLancamento.IdNatureza       := IdNat;
                DadosLancamento.VlrTotalPrev     := dmBaixa.cDifTot;
                DadosLancamento.QtdParcelas      := 1;
                DadosLancamento.FlgRecorrente    := 'N';
                DadosLancamento.FlgReplicado     := 'N';
                DadosLancamento.DataCadastro     := Now;
                DadosLancamento.CadIdUsuario     := vgUsu_Id;
                DadosLancamento.IdSistema        := dmBaixa.cdsBaixaLancG.FieldByName('ID_SISTEMA_FK').AsInteger;
                DadosLancamento.FlgStatus        := 'G';
                DadosLancamento.VlrTotalPago     := dmBaixa.cDifTot;

                if Trim(dmBaixa.sTipoBaixa) = 'S' then
                begin
                  DadosLancamento.Observacao := 'Lan�amento gerado por pagamento de Parcela (' +
                                                dmBaixa.cdsBaixaLancG.FieldByName('COD_LANCAMENTO').AsString
                                                + '/' + dmBaixa.cdsBaixaLancG.FieldByName('ANO_LANCAMENTO').AsString +
                                                ' - Parc. N�: ' + dmBaixa.cdsBaixaLancG.FieldByName('NUM_PARCELA').AsString +
                                                ' - Recibo: ' + dmBaixa.cdsBaixaLancG.FieldByName('NUM_RECIBO').AsString +
                                                ') com valor acima do Previsto.';
                end
                else if Trim(dmBaixa.sTipoBaixa) = 'F' then
                begin
                  DadosLancamento.Observacao := 'Lan�amento gerado por pagamento de Parcela (' +
                                                dmBaixa.cdsBaixaLancG.FieldByName('COD_LANCAMENTO').AsString
                                                + '/' + dmBaixa.cdsBaixaLancG.FieldByName('ANO_LANCAMENTO').AsString +
                                                ' - Parc. N�: ' + dmBaixa.cdsBaixaLancG.FieldByName('NUM_PARCELA').AsString +
                                                ' - Recibo: ' + dmBaixa.cdsBaixaLancG.FieldByName('NUM_RECIBO').AsString +
                                                ') com valor abaixo do Previsto.';
                end;

                ListaLancamentos.Add(DadosLancamento);

                dmPrincipal.InsertLancamento(0);

                //Parcela
                DadosLancamentoParc := LancamentoParc.Create;

{                if dmPrincipal.qryFechCx.RecordCount = 0 then
                  DadosLancamentoParc.DataLancParc := Date
                else
                  DadosLancamentoParc.DataLancParc := IncDay(Date, 1);  }

                DadosLancamentoParc.DataLancParc   := dteDataPagamento.Date;
                DadosLancamentoParc.NumParcela     := 1;
                DadosLancamentoParc.DataVencimento := dteDataPagamento.Date;
                DadosLancamentoParc.VlrPrevisto    := dmBaixa.cDifTot;
                DadosLancamentoParc.IdFormaPagto   := dmBaixa.cdsBaixaLancG.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger;
                DadosLancamentoParc.CadIdUsuario   := vgUsu_Id;
                DadosLancamentoParc.DataCadastro   := Now;
                DadosLancamentoParc.VlrPago        := dmBaixa.cDifTot;
                DadosLancamentoParc.FlgStatus      := 'G';
                DadosLancamentoParc.PagIdUsuario   := vgUsu_Id;
                DadosLancamentoParc.DataPagamento  := dteDataPagamento.Date;

                if (dmBaixa.cdsBaixaLancG.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger = 5) and
                  (Trim(dmBaixa.sTipoBaixa) = 'S') then
                  DadosLancamentoParc.NumCodDeposito := dmBaixa.cdsBaixaLancG.FieldByName('NUM_COD_DEPOSITO').AsString;

                ListaLancamentoParcs.Add(DadosLancamentoParc);

                dmPrincipal.InsertLancamentoParc(0, 0);

                //Manter Vinculo de Deposito, quando for o caso
                if (dmBaixa.cdsBaixaLancG.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger = 5) and
                  (Trim(dmBaixa.sTipoBaixa) = 'S') then
                begin
                  QryAux.Close;
                  QryAux.SQL.Clear;

                  QryAux.SQL.Text := 'SELECT * ' +
                                     '  FROM VINCULO_LANCDEPO ' +
                                     ' WHERE ID_LANCAMENTO_PARC_FK = : ID_LANCAMENTO_PARC ' +
                                     '   AND DATA_CANCELAMENTO IS NULL';

                  QryAux.Params.ParamByName('ID_LANCAMENTO_PARC').Value := dmBaixa.cdsBaixaLancG.FieldByName('ID_LANCAMENTO_PARC').AsInteger;
                  QryAux.Open;

                  QryAux.First;

                  while not QryAux.Eof do
                  begin
                    QryVinc.Close;
                    QryVinc.SQL.Clear;
                    QryVinc.SQL.Text := 'INSERT INTO VINCULO_LANCDEPO (DATA_VINCULO_LANCDEPO, ' +
                                        '                              DESCR_VINCULO_LANCDEPO, ' +
                                        '                              COD_LANCAMENTO_FK, ' +
                                        '                              ANO_LANCAMENTO_FK, ' +
                                        '                              ID_LANCAMENTO_PARC_FK, ' +
                                        '                              ID_DEPOSITO_FLUTUANTE_FK, ' +
                                        '                              OBS_VINCULO_LANCDEPO) ' +
                                        '                      VALUES (:DATA_VINCULO_LANCDEPO, ' +
                                        '                              :DESCR_VINCULO_LANCDEPO, ' +
                                        '                              :COD_LANCAMENTO, ' +
                                        '                              :ANO_LANCAMENTO, ' +
                                        '                              :ID_LANCAMENTO_PARC, ' +
                                        '                              :ID_DEPOSITO_FLUTUANTE, ' +
                                        '                              :OBS_VINCULO_LANCDEPO)';
                    QryVinc.Params.ParamByName('DATA_VINCULO_LANCDEPO').Value  := QryAux.FieldByName('DATA_VINCULO_LANCDEPO').AsDateTime;
                    QryVinc.Params.ParamByName('DESCR_VINCULO_LANCDEPO').Value := QryAux.FieldByName('DESCR_VINCULO_LANCDEPO').AsString;
                    QryVinc.Params.ParamByName('COD_LANCAMENTO').Value         := QryAux.FieldByName('COD_LANCAMENTO_FK').AsInteger;
                    QryVinc.Params.ParamByName('ANO_LANCAMENTO').Value         := QryAux.FieldByName('ANO_LANCAMENTO_FK').AsInteger;
                    QryVinc.Params.ParamByName('ID_LANCAMENTO_PARC').Value     := ListaLancamentoParcs[0].IdLancamentoParc;
                    QryVinc.Params.ParamByName('ID_DEPOSITO_FLUTUANTE').Value  := QryAux.FieldByName('ID_DEPOSITO_FLUTUANTE_FK').AsInteger;
                    QryVinc.Params.ParamByName('OBS_VINCULO_LANCDEPO').Value   := QryAux.FieldByName('OBS_VINCULO_LANCDEPO').AsString;
                    QryVinc.ExecSQL;

                    QryAux.Next;
                  end;
                end;

                dmPrincipal.FinalizarComponenteLancamento;
              end;

              // Observacao de Sobra ou Falta de Caixa
              if (dmBaixa.sTipoBaixa = 'O') and (dmBaixa.cDifTot > 0) then
              begin
                dmPrincipal.InicializarComponenteLancamento;

                // FLUTUANTES
                DadosLancamento := Lancamento.Create;

                DadosLancamento.CodLancamento := dmBaixa.cdsBaixaLancG.FieldByName('COD_LANCAMENTO_FK').AsInteger;
                DadosLancamento.AnoLancamento := dmBaixa.cdsBaixaLancG.FieldByName('ANO_LANCAMENTO_FK').AsInteger;
                DadosLancamento.VlrTotalPago  := dmBaixa.cdsBaixaLancG.FieldByName('VR_PARCELA_PREV').AsCurrency;

                ListaLancamentos.Add(DadosLancamento);

                dmPrincipal.UpdateLancamento(0);

                dmPrincipal.FinalizarComponenteLancamento;
              end;

              // Desmembramento de Flutuante
              if (dmBaixa.sTipoBaixa = 'D') and (dmBaixa.cDifTot > 0) and
                (dmBaixa.cdsBaixaLancG.FieldByName('VR_TOTAL_PREV').AsCurrency >= dmBaixa.cDifTot) then
              begin
                IdDetalhe := BS.ProximoId('ID_LANCAMENTO_DET', 'LANCAMENTO_DET');

                dmPrincipal.InicializarComponenteLancamento;

                // Lancamento
                DadosLancamento := Lancamento.Create;

                DadosLancamento.DataLancamento := dmBaixa.cdsBaixaLancG.FieldByName('DATA_LANCAMENTO').AsDateTime;
                DadosLancamento.TipoLancamento := dmBaixa.cdsBaixaLancG.FieldByName('TIPO_LANCAMENTO').AsString;
                DadosLancamento.TipoCadastro := 'A';

                if dmBaixa.cdsBaixaLancG.FieldByName('ID_CLIENTE_FORNECEDOR_FK').IsNull then
                  DadosLancamento.IdCliFor := 0
                else
                  DadosLancamento.IdCliFor := dmBaixa.cdsBaixaLancG.FieldByName('ID_CLIENTE_FORNECEDOR_FK').AsInteger;

                if dmBaixa.cdsBaixaLancG.FieldByName('ID_CATEGORIA_DESPREC_FK').IsNull
                then
                  DadosLancamento.IdCategoria := 0
                else
                  DadosLancamento.IdCategoria := dmBaixa.cdsBaixaLancG.FieldByName('ID_CATEGORIA_DESPREC_FK').AsInteger;

                if dmBaixa.cdsBaixaLancG.FieldByName('ID_SUBCATEGORIA_DESPREC_FK').IsNull then
                  DadosLancamento.IdSubCategoria := 0
                else
                  DadosLancamento.IdSubCategoria := dmBaixa.cdsBaixaLancG.FieldByName('ID_SUBCATEGORIA_DESPREC_FK').AsInteger;

                DadosLancamento.FlgReal          := dmBaixa.cdsBaixaLancG.FieldByName('FLG_REAL').AsString;
                DadosLancamento.FlgIR            := dmBaixa.cdsBaixaLancG.FieldByName('FLG_IMPOSTORENDA').AsString;
                DadosLancamento.FlgPessoal       := dmBaixa.cdsBaixaLancG.FieldByName('FLG_PESSOAL').AsString;
                DadosLancamento.FlgFlutuante     := dmBaixa.cdsBaixaLancG.FieldByName('FLG_FLUTUANTE').AsString;
                DadosLancamento.FlgAuxiliar      := dmBaixa.cdsBaixaLancG.FieldByName('FLG_AUXILIAR').AsString;
                DadosLancamento.FlgForaFechCaixa := dmBaixa.cdsBaixaLancG.FieldByName('FLG_FORAFECHCAIXA').AsString;
                DadosLancamento.IdNatureza       := dmBaixa.cdsBaixaLancG.FieldByName('ID_NATUREZA_FK').AsInteger;
                DadosLancamento.VlrTotalPrev     := dmBaixa.cDifTot;

                if Trim(dmBaixa.cdsBaixaLancG.FieldByName('FLG_STATUS').AsString) = 'G' then
                begin
                  DadosLancamento.VlrTotalJuros    := dmBaixa.cdsBaixaLancG.FieldByName('VR_TOTAL_JUROS').AsCurrency;
                  DadosLancamento.VlrTotalDesconto := dmBaixa.cdsBaixaLancG.FieldByName('VR_TOTAL_DESCONTO').AsCurrency;
                  DadosLancamento.VlrTotalPago     := dmBaixa.cDifTot;
                  DadosLancamento.FlgStatus        := 'G';
                end
                else
                begin
                  DadosLancamento.VlrTotalJuros    := 0;
                  DadosLancamento.VlrTotalDesconto := 0;
                  DadosLancamento.VlrTotalPago     := 0;
                  DadosLancamento.FlgStatus        := 'P';
                end;

                DadosLancamento.NumRecibo      := dmBaixa.cdsBaixaLancG.FieldByName('NUM_RECIBO').AsInteger;
                DadosLancamento.IdOrigem       := dmBaixa.cdsBaixaLancG.FieldByName('ID_ORIGEM').AsInteger;
                DadosLancamento.IdEquivalencia := dmBaixa.cdsBaixaLancG.FieldByName('ID_CARNELEAO_EQUIVALENCIA_FK').AsInteger;
                DadosLancamento.Observacao := 'Lan�amento gerado por desmembramento de Parcela (' +
                                              dmBaixa.cdsBaixaLancG.FieldByName('COD_LANCAMENTO').AsString +
                                              '/' + dmBaixa.cdsBaixaLancG.FieldByName('ANO_LANCAMENTO').AsString +
                                              ' - Parc. N�: ' + dmBaixa.cdsBaixaLancG.FieldByName('NUM_PARCELA').AsString +
                                              ' - Recibo: ' + dmBaixa.cdsBaixaLancG.FieldByName('NUM_RECIBO').AsString +
                                              ') com valor acima do Previsto.';
                DadosLancamento.QtdParcelas   := 1;
                DadosLancamento.FlgRecorrente := 'N';
                DadosLancamento.FlgReplicado  := 'N';
                DadosLancamento.DataCadastro  := Now;
                DadosLancamento.CadIdUsuario  := vgUsu_Id;
                DadosLancamento.IdSistema     := dmBaixa.cdsBaixaLancG.FieldByName('ID_SISTEMA_FK').AsInteger;

                ListaLancamentos.Add(DadosLancamento);

                dmPrincipal.InsertLancamento(0);

                // Detalhe
                QryAux.Close;
                QryAux.SQL.Clear;

                QryAux.SQL.Text := 'SELECT * FROM LANCAMENTO_DET ' +
                                    '  WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                                    '    AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ' +
                                    '    AND FLG_CANCELADO = ' + QuotedStr('N') +
                                    'ORDER BY ID_LANCAMENTO_DET';

                QryAux.Params.ParamByName('COD_LANCAMENTO').Value := dmBaixa.cdsBaixaLancG.FieldByName('COD_LANCAMENTO').AsString;
                QryAux.Params.ParamByName('ANO_LANCAMENTO').Value := dmBaixa.cdsBaixaLancG.FieldByName('ANO_LANCAMENTO').AsString;
                QryAux.Open;

                QryAux.First;
                m := 0;

                while not QryAux.Eof do
                begin
                  DadosLancamentoDet := LancamentoDet.Create;

                  DadosLancamentoDet.IdLancamentoDet := IdDetalhe;
                  DadosLancamentoDet.IdItem          := QryAux.FieldByName('ID_ITEM_FK').AsInteger;
                  DadosLancamentoDet.Quantidade      := QryAux.FieldByName('QUANTIDADE').AsInteger;
                  DadosLancamentoDet.VlrUnitario     := QryAux.FieldByName('VR_UNITARIO').AsCurrency;
                  DadosLancamentoDet.VlrTotal        := QryAux.FieldByName('VR_TOTAL').AsCurrency;

                  if Trim(QryAux.FieldByName('SELO_ORIGEM').AsString) <> '' then
                    DadosLancamentoDet.SeloOrigem := QryAux.FieldByName('SELO_ORIGEM').AsString;

                  if Trim(QryAux.FieldByName('ALEATORIO_ORIGEM').AsString) <> ''
                  then
                    DadosLancamentoDet.AleatorioOrigem := QryAux.FieldByName('ALEATORIO_ORIGEM').AsString;

                  if Trim(QryAux.FieldByName('TIPO_COBRANCA').AsString) <> ''
                  then
                    DadosLancamentoDet.TipoCobranca := QryAux.FieldByName('TIPO_COBRANCA').AsString;

                  if not QryAux.FieldByName('COD_ADICIONAL').IsNull then
                    DadosLancamentoDet.CodAdicional := QryAux.FieldByName('COD_ADICIONAL').AsInteger;

                  if not QryAux.FieldByName('EMOLUMENTOS').IsNull then
                    DadosLancamentoDet.Emolumentos := QryAux.FieldByName('EMOLUMENTOS').AsCurrency;

                  if not QryAux.FieldByName('FETJ').IsNull then
                    DadosLancamentoDet.FETJ := QryAux.FieldByName('FETJ').AsCurrency;

                  if not QryAux.FieldByName('FUNDPERJ').IsNull then
                    DadosLancamentoDet.FUNDPERJ := QryAux.FieldByName('FUNDPERJ').AsCurrency;

                  if not QryAux.FieldByName('FUNPERJ').IsNull then
                    DadosLancamentoDet.FUNPERJ := QryAux.FieldByName('FUNPERJ').AsCurrency;

                  if not QryAux.FieldByName('FUNARPEN').IsNull then
                    DadosLancamentoDet.FUNARPEN := QryAux.FieldByName('FUNARPEN').AsCurrency;

                  if not QryAux.FieldByName('PMCMV').IsNull then
                    DadosLancamentoDet.PMCMV := QryAux.FieldByName('PMCMV').AsCurrency;

                  if not QryAux.FieldByName('ISS').IsNull then
                    DadosLancamentoDet.ISS := QryAux.FieldByName('ISS').AsCurrency;

                  if not QryAux.FieldByName('MUTUA').IsNull then
                    DadosLancamentoDet.Mutua := QryAux.FieldByName('MUTUA').AsCurrency;

                  if not QryAux.FieldByName('ACOTERJ').IsNull then
                    DadosLancamentoDet.Acoterj := QryAux.FieldByName('ACOTERJ').AsCurrency;

                  if not QryAux.FieldByName('NUM_PROTOCOLO').IsNull then
                    DadosLancamentoDet.NumProtocolo := QryAux.FieldByName('NUM_PROTOCOLO').AsInteger;

                  ListaLancamentoDets.Add(DadosLancamentoDet);

                  dmPrincipal.InsertLancamentoDet(m, 0);

                  IdDetalhe := ListaLancamentoDets[m].IdLancamentoDet;

                  Inc(IdDetalhe);
                  Inc(m);

                  QryAux.Next;
                end;

                // Parcela
                DadosLancamentoParc := LancamentoParc.Create;

                DadosLancamentoParc.DataLancParc   := dmBaixa.cdsBaixaLancG.FieldByName('DATA_LANCAMENTO_PARC').AsDateTime;
                DadosLancamentoParc.NumParcela     := 1;
                DadosLancamentoParc.DataVencimento := dmBaixa.cdsBaixaLancG.FieldByName('DATA_VENCIMENTO').AsDateTime;
                DadosLancamentoParc.IdFormaPagto   := dmBaixa.cdsBaixaLancG.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger;
                DadosLancamentoParc.CadIdUsuario   := vgUsu_Id;
                DadosLancamentoParc.DataCadastro   := Now;
                DadosLancamentoParc.VlrPrevisto    := dmBaixa.cDifTot;

                if Trim(dmBaixa.cdsBaixaLancG.FieldByName('FLG_STATUS_PARC').AsString) = 'G' then
                begin
                  DadosLancamentoParc.VlrJuros      := dmBaixa.cdsBaixaLancG.FieldByName('VR_PARCELA_JUROS').AsCurrency;
                  DadosLancamentoParc.VlrDesconto   := dmBaixa.cdsBaixaLancG.FieldByName('VR_PARCELA_DESCONTO').AsCurrency;
                  DadosLancamentoParc.FlgStatus     := dmBaixa.cdsBaixaLancG.FieldByName('FLG_STATUS_PARC').AsString;
                  DadosLancamentoParc.PagIdUsuario  := vgUsu_Id;
                  DadosLancamentoParc.DataPagamento := dmBaixa.cdsBaixaLancG.FieldByName('DATA_PAGAMENTO').AsDateTime;
                  DadosLancamentoParc.VlrPago       := dmBaixa.cDifTot;
                end;

                DadosLancamentoParc.Observacao := dmBaixa.cdsBaixaLancG.FieldByName('OBS_LANCAMENTO_PARC').AsString;

                if dmBaixa.cdsBaixaLancG.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger = 5 then
                  DadosLancamentoParc.NumCodDeposito := dmBaixa.cdsBaixaLancG.FieldByName('NUM_COD_DEPOSITO').AsString;

                ListaLancamentoParcs.Add(DadosLancamentoParc);

                dmPrincipal.InsertLancamentoParc(0, 0);

                //Manter Vinculo de Deposito, quando for o caso
                if dmBaixa.cdsBaixaLancG.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger = 5 then
                begin
                  QryAux.Close;
                  QryAux.SQL.Clear;

                  QryAux.SQL.Text := 'SELECT * ' +
                                     '  FROM VINCULO_LANCDEPO ' +
                                     ' WHERE ID_LANCAMENTO_PARC_FK = : ID_LANCAMENTO_PARC ' +
                                     '   AND DATA_CANCELAMENTO IS NULL';

                  QryAux.Params.ParamByName('ID_LANCAMENTO_PARC').Value := dmBaixa.cdsBaixaLancG.FieldByName('ID_LANCAMENTO_PARC').AsInteger;
                  QryAux.Open;

                  QryAux.First;

                  while not QryAux.Eof do
                  begin
                    QryVinc.Close;
                    QryVinc.SQL.Clear;
                    QryVinc.SQL.Text := 'INSERT INTO VINCULO_LANCDEPO (DATA_VINCULO_LANCDEPO, ' +
                                        '                              DESCR_VINCULO_LANCDEPO, ' +
                                        '                              COD_LANCAMENTO_FK, ' +
                                        '                              ANO_LANCAMENTO_FK, ' +
                                        '                              ID_LANCAMENTO_PARC_FK, ' +
                                        '                              ID_DEPOSITO_FLUTUANTE_FK, ' +
                                        '                              OBS_VINCULO_LANCDEPO) ' +
                                        '                      VALUES (:DATA_VINCULO_LANCDEPO, ' +
                                        '                              :DESCR_VINCULO_LANCDEPO, ' +
                                        '                              :COD_LANCAMENTO, ' +
                                        '                              :ANO_LANCAMENTO, ' +
                                        '                              :ID_LANCAMENTO_PARC, ' +
                                        '                              :ID_DEPOSITO_FLUTUANTE, ' +
                                        '                              :OBS_VINCULO_LANCDEPO)';
                    QryVinc.Params.ParamByName('DATA_VINCULO_LANCDEPO').Value  := QryAux.FieldByName('DATA_VINCULO_LANCDEPO').AsDateTime;
                    QryVinc.Params.ParamByName('DESCR_VINCULO_LANCDEPO').Value := QryAux.FieldByName('DESCR_VINCULO_LANCDEPO').AsString;
                    QryVinc.Params.ParamByName('COD_LANCAMENTO').Value         := QryAux.FieldByName('COD_LANCAMENTO_FK').AsInteger;
                    QryVinc.Params.ParamByName('ANO_LANCAMENTO').Value         := QryAux.FieldByName('ANO_LANCAMENTO_FK').AsInteger;
                    QryVinc.Params.ParamByName('ID_LANCAMENTO_PARC').Value     := ListaLancamentoParcs[0].IdLancamentoParc;
                    QryVinc.Params.ParamByName('ID_DEPOSITO_FLUTUANTE').Value  := QryAux.FieldByName('ID_DEPOSITO_FLUTUANTE_FK').AsInteger;
                    QryVinc.Params.ParamByName('OBS_VINCULO_LANCDEPO').Value   := QryAux.FieldByName('OBS_VINCULO_LANCDEPO').AsString;
                    QryVinc.ExecSQL;

                    QryAux.Next;
                  end;
                end;

                dmPrincipal.FinalizarComponenteLancamento;
              end;

              // Nova Parcela
              if (Trim(dmBaixa.sTipoBaixa) = 'P') and (dmBaixa.cDifTot > 0) then
              begin
                lIncParcelas := True;

                dmPrincipal.InicializarComponenteLancamento;

                // Lancamento
                DadosLancamento := Lancamento.Create;

                DadosLancamento.CodLancamento := dmBaixa.cdsBaixaLancG.FieldByName('COD_LANCAMENTO').AsInteger;
                DadosLancamento.AnoLancamento := dmBaixa.cdsBaixaLancG.FieldByName('ANO_LANCAMENTO').AsInteger;
//                DadosLancamento.VlrTotalPrev  := dmBaixa.cdsBaixaLancG.FieldByName('VR_TOTAL_PREV').AsCurrency;
//                DadosLancamento.VlrTotalPago  := dmBaixa.cdsBaixaLancG.FieldByName('VR_PARCELA_PAGO').AsCurrency;
                DadosLancamento.QtdParcelas   := (dmBaixa.cdsBaixaLancG.FieldByName('QTD_PARCELAS').AsInteger + 1);

                ListaLancamentos.Add(DadosLancamento);

                dmPrincipal.UpdateLancamento(0);

                //Parcela Nova
                DadosLancamentoParc := LancamentoParc.Create;

{                if dmPrincipal.qryFechCx.RecordCount = 0 then
                  DadosLancamentoParc.DataLancParc := Date
                else
                  DadosLancamentoParc.DataLancParc := IncDay(Date, 1);  }

                DadosLancamentoParc.DataLancParc   := dmBaixa.cdsBaixaLancG.FieldByName('DATA_LANCAMENTO_PARC').AsDateTime;
                DadosLancamentoParc.CodLancamento  := dmBaixa.cdsBaixaLancG.FieldByName('COD_LANCAMENTO').AsInteger;
                DadosLancamentoParc.AnoLancamento  := dmBaixa.cdsBaixaLancG.FieldByName('ANO_LANCAMENTO').AsInteger;
                DadosLancamentoParc.NumParcela     := (dmBaixa.cdsBaixaLancG.FieldByName('QTD_PARCELAS').AsInteger + 1);
                DadosLancamentoParc.DataVencimento := IncMonth(Date, 1);
                DadosLancamentoParc.VlrPrevisto    := dmBaixa.cDifTot;
                DadosLancamentoParc.IdFormaPagto   := dmBaixa.cdsBaixaLancG.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger;

                if Trim(dmBaixa.cdsBaixaLancG.FieldByName('FLG_STATUS_PARC').AsString) = 'G' then
                begin
                  DadosLancamentoParc.FlgStatus      := dmBaixa.cdsBaixaLancG.FieldByName('FLG_STATUS_PARC').AsString;
                  DadosLancamentoParc.PagIdUsuario   := dmBaixa.cdsBaixaLancG.FieldByName('PAG_ID_USUARIO').AsInteger;
                end;

                DadosLancamentoParc.CadIdUsuario   := vgUsu_Id;
                DadosLancamentoParc.DataCadastro   := Now;
                DadosLancamentoParc.Observacao     := 'Parcela gerada por valor insuficiente no pagamento da parcela N� ' +
                                                      dmBaixa.cdsBaixaLancG.FieldByName('NUM_PARCELA').AsString +
                                                      ' - Recibo: ' + dmBaixa.cdsBaixaLancG.FieldByName('NUM_RECIBO').AsString;

                if dmBaixa.cdsBaixaLancG.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger = 5 then
                  DadosLancamentoParc.NumCodDeposito := dmBaixa.cdsBaixaLancG.FieldByName('NUM_COD_DEPOSITO').AsString;

                ListaLancamentoParcs.Add(DadosLancamentoParc);

                dmPrincipal.InsertLancamentoParc(0, 0);

                //Manter Vinculo de Deposito, quando for o caso
                if dmBaixa.cdsBaixaLancG.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger = 5 then
                begin
                  QryAux.Close;
                  QryAux.SQL.Clear;

                  QryAux.SQL.Text := 'SELECT * ' +
                                     '  FROM VINCULO_LANCDEPO ' +
                                     ' WHERE ID_LANCAMENTO_PARC_FK = : ID_LANCAMENTO_PARC ' +
                                     '   AND DATA_CANCELAMENTO IS NULL';

                  QryAux.Params.ParamByName('ID_LANCAMENTO_PARC').Value := dmBaixa.cdsBaixaLancG.FieldByName('ID_LANCAMENTO_PARC').AsInteger;
                  QryAux.Open;

                  QryAux.First;

                  while not QryAux.Eof do
                  begin
                    QryVinc.Close;
                    QryVinc.SQL.Clear;
                    QryVinc.SQL.Text := 'INSERT INTO VINCULO_LANCDEPO (DATA_VINCULO_LANCDEPO, ' +
                                        '                              DESCR_VINCULO_LANCDEPO, ' +
                                        '                              COD_LANCAMENTO_FK, ' +
                                        '                              ANO_LANCAMENTO_FK, ' +
                                        '                              ID_LANCAMENTO_PARC_FK, ' +
                                        '                              ID_DEPOSITO_FLUTUANTE_FK, ' +
                                        '                              OBS_VINCULO_LANCDEPO) ' +
                                        '                      VALUES (:DATA_VINCULO_LANCDEPO, ' +
                                        '                              :DESCR_VINCULO_LANCDEPO, ' +
                                        '                              :COD_LANCAMENTO, ' +
                                        '                              :ANO_LANCAMENTO, ' +
                                        '                              :ID_LANCAMENTO_PARC, ' +
                                        '                              :ID_DEPOSITO_FLUTUANTE, ' +
                                        '                              :OBS_VINCULO_LANCDEPO)';
                    QryVinc.Params.ParamByName('DATA_VINCULO_LANCDEPO').Value  := QryAux.FieldByName('DATA_VINCULO_LANCDEPO').AsDateTime;
                    QryVinc.Params.ParamByName('DESCR_VINCULO_LANCDEPO').Value := QryAux.FieldByName('DESCR_VINCULO_LANCDEPO').AsString;
                    QryVinc.Params.ParamByName('COD_LANCAMENTO').Value         := QryAux.FieldByName('COD_LANCAMENTO_FK').AsInteger;
                    QryVinc.Params.ParamByName('ANO_LANCAMENTO').Value         := QryAux.FieldByName('ANO_LANCAMENTO_FK').AsInteger;
                    QryVinc.Params.ParamByName('ID_LANCAMENTO_PARC').Value     := ListaLancamentoParcs[0].IdLancamentoParc;
                    QryVinc.Params.ParamByName('ID_DEPOSITO_FLUTUANTE').Value  := QryAux.FieldByName('ID_DEPOSITO_FLUTUANTE_FK').AsInteger;
                    QryVinc.Params.ParamByName('OBS_VINCULO_LANCDEPO').Value   := QryAux.FieldByName('OBS_VINCULO_LANCDEPO').AsString;
                    QryVinc.ExecSQL;

                    QryAux.Next;
                  end;
                end;

                dmPrincipal.FinalizarComponenteLancamento;
              end;

              if Trim(dmBaixa.sTipoBaixa) <> '' then
              begin
                //Lancamento do Flutuante
                QryAux.Close;
                QryAux.SQL.Clear;

                QryAux.SQL.Text := 'UPDATE LANCAMENTO SET ';

                case AnsiIndexStr(UpperCase(dmBaixa.sTipoBaixa), ['S', 'F', 'P', 'O', 'D']) of
                  0:
                    begin
                      QryAux.SQL.Add(' FLG_STATUS    = :FLG_STATUS, ' +
                                     ' VR_TOTAL_PAGO = :VR_TOTAL_PAGO ');

                      QryAux.Params.ParamByName('FLG_STATUS').Value    := 'G';
                      QryAux.Params.ParamByName('VR_TOTAL_PAGO').Value := dmBaixa.cdsBaixaLancG.FieldByName('VR_PARCELA_PREV').AsCurrency;
                    end;
                  1:
                    begin
                      QryAux.SQL.Add(' FLG_STATUS    = :FLG_STATUS, ' +
                                     ' VR_TOTAL_PREV = :VR_TOTAL_PREV ');

                      QryAux.Params.ParamByName('FLG_STATUS').Value    := 'G';
                      QryAux.Params.ParamByName('VR_TOTAL_PREV').Value := dmBaixa.cdsBaixaLancG.FieldByName('VR_PARCELA_PAGO').AsCurrency;
                    end;
                  2:
                    begin
                      QryAux.SQL.Add(//' VR_TOTAL_PREV = :VR_TOTAL_PREV, ' +
                                     ' FLG_STATUS    = :FLG_STATUS, ' +
                                     ' VR_TOTAL_PAGO = :VR_TOTAL_PAGO ');
//                      QryAux.Params.ParamByName('VR_TOTAL_PREV').AsCurrency := (dmBaixa.cdsBaixaLancG.FieldByName('VR_TOTAL_PREV').AsCurrency + dmBaixa.cDifTot);

                      if dmBaixa.cdsBaixaLancG.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger in [5, 6] then
                        QryAux.Params.ParamByName('FLG_STATUS').Value := 'G'
                      else
                        QryAux.Params.ParamByName('FLG_STATUS').Value := dmBaixa.cdsBaixaLancG.FieldByName('FLG_STATUS').AsString;

                      QryAux.Params.ParamByName('VR_TOTAL_PAGO').AsCurrency := dmBaixa.cdsBaixaLancG.FieldByName('VR_TOTAL_PAGO').AsCurrency;
                    end;
                  3:
                    begin
                      QryAux.SQL.Add(' FLG_STATUS    = :FLG_STATUS, ' +
                                     ' VR_TOTAL_PAGO = :VR_TOTAL_PAGO ');

                      if dmBaixa.cdsBaixaLancG.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger in [5, 6] then
                        QryAux.Params.ParamByName('FLG_STATUS').Value := 'G'
                      else
                        QryAux.Params.ParamByName('FLG_STATUS').Value := dmBaixa.cdsBaixaLancG.FieldByName('FLG_STATUS').AsString;

                      QryAux.Params.ParamByName('VR_TOTAL_PAGO').AsCurrency := dmBaixa.cdsBaixaLancG.FieldByName('VR_TOTAL_PREV').AsCurrency;
                    end;
                  4:
                    begin
                      QryAux.SQL.Add(' FLG_STATUS    = :FLG_STATUS, ' +
                                     ' VR_TOTAL_PREV = :VR_TOTAL_PREV, ' +
                                     ' VR_TOTAL_PAGO = :VR_TOTAL_PAGO ');

                      if dmBaixa.cdsBaixaLancG.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger in [5, 6] then
                        QryAux.Params.ParamByName('FLG_STATUS').Value := 'G'
                      else
                        QryAux.Params.ParamByName('FLG_STATUS').Value := dmBaixa.cdsBaixaLancG.FieldByName('FLG_STATUS').AsString;

                      if dmBaixa.cdsBaixaLancG.FieldByName('VR_TOTAL_PREV').AsCurrency >= dmBaixa.cDifTot then
                      begin
                        QryAux.Params.ParamByName('VR_TOTAL_PREV').AsCurrency := (dmBaixa.cdsBaixaLancG.FieldByName('VR_TOTAL_PREV').AsCurrency - dmBaixa.cDifTot);
                        QryAux.Params.ParamByName('VR_TOTAL_PAGO').AsCurrency := (dmBaixa.cdsBaixaLancG.FieldByName('VR_TOTAL_PREV').AsCurrency - dmBaixa.cDifTot);
                      end
                      else
                      begin
                        QryAux.Params.ParamByName('VR_TOTAL_PREV').AsCurrency := dmBaixa.cdsBaixaLancG.FieldByName('VR_TOTAL_PREV').AsCurrency;
                        QryAux.Params.ParamByName('VR_TOTAL_PAGO').AsCurrency := dmBaixa.cdsBaixaLancG.FieldByName('VR_TOTAL_PREV').AsCurrency;
                      end;
                    end;
                end;

                QryAux.SQL.Add(' WHERE COD_LANCAMENTO = :COD_LANCAMENTO ' +
                               '   AND ANO_LANCAMENTO = :ANO_LANCAMENTO');

                QryAux.Params.ParamByName('COD_LANCAMENTO').Value := dmBaixa.cdsBaixaLancG.FieldByName('COD_LANCAMENTO').AsInteger;
                QryAux.Params.ParamByName('ANO_LANCAMENTO').Value := dmBaixa.cdsBaixaLancG.FieldByName('ANO_LANCAMENTO').AsInteger;
                QryAux.ExecSQL;

                //Parcela do Flutuante
                QryAux.Close;
                QryAux.SQL.Clear;

                QryAux.SQL.Text := 'UPDATE LANCAMENTO_PARC ' +
                                   '   SET ';
//                                   '   SET FLG_STATUS = :FLG_STATUS, ' +
//                                   '       DATA_PAGAMENTO = :DATA_PAGAMENTO, ' +
//                                   '       PAG_ID_USUARIO = :PAG_ID_USUARIO, ';

                case AnsiIndexStr(UpperCase(dmBaixa.sTipoBaixa), ['S', 'F', 'P', 'O', 'D']) of
                  0, 1, 3:
                    begin
                      QryAux.SQL.Add(' VR_PARCELA_PAGO = :VR_PARCELA_PAGO ');
                      QryAux.Params.ParamByName('VR_PARCELA_PAGO').Value := dmBaixa.cdsBaixaLancG.FieldByName('VR_PARCELA_PREV').AsCurrency;
                    end;
                  2:
                    begin
                      QryAux.SQL.Add(' VR_PARCELA_PREV = :VR_PARCELA_PREV, ' +
                                     ' VR_PARCELA_PAGO = :VR_PARCELA_PAGO ');

                      QryAux.Params.ParamByName('VR_PARCELA_PREV').Value := dmBaixa.cdsBaixaLancG.FieldByName('VR_PARCELA_PAGO').AsCurrency;
                      QryAux.Params.ParamByName('VR_PARCELA_PAGO').Value := dmBaixa.cdsBaixaLancG.FieldByName('VR_PARCELA_PAGO').AsCurrency;
                    end;
                  4:
                    begin
                      QryAux.SQL.Add(' VR_PARCELA_PREV = :VR_PARCELA_PREV, ' +
                                     ' VR_PARCELA_PAGO = :VR_PARCELA_PAGO ');

                      if dmBaixa.cdsBaixaLancG.FieldByName('VR_TOTAL_PREV').AsCurrency >= dmBaixa.cDifTot then
                      begin
                        QryAux.Params.ParamByName('VR_PARCELA_PREV').AsCurrency := (dmBaixa.cdsBaixaLancG.FieldByName('VR_PARCELA_PREV').AsCurrency - dmBaixa.cDifTot);
                        QryAux.Params.ParamByName('VR_PARCELA_PAGO').AsCurrency := (dmBaixa.cdsBaixaLancG.FieldByName('VR_PARCELA_PREV').AsCurrency - dmBaixa.cDifTot);
                      end
                      else
                      begin
                        QryAux.Params.ParamByName('VR_PARCELA_PREV').AsCurrency := dmBaixa.cdsBaixaLancG.FieldByName('VR_PARCELA_PREV').AsCurrency;
                        QryAux.Params.ParamByName('VR_PARCELA_PAGO').AsCurrency := dmBaixa.cdsBaixaLancG.FieldByName('VR_PARCELA_PREV').AsCurrency;
                      end;
                    end;
                end;

//                QryAux.Params.ParamByName('FLG_STATUS').Value     := 'G';
//                QryAux.Params.ParamByName('DATA_PAGAMENTO').Value := dteDataPagamento.Date;
//                QryAux.Params.ParamByName('PAG_ID_USUARIO').Value := vgUsu_Id;

                QryAux.SQL.Add(' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                               '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ' +
                               '   AND ID_LANCAMENTO_PARC = :ID_LANCAMENTO_PARC');

                QryAux.Params.ParamByName('COD_LANCAMENTO').Value     := dmBaixa.cdsBaixaLancG.FieldByName('COD_LANCAMENTO').AsInteger;
                QryAux.Params.ParamByName('ANO_LANCAMENTO').Value     := dmBaixa.cdsBaixaLancG.FieldByName('ANO_LANCAMENTO').AsInteger;
                QryAux.Params.ParamByName('ID_LANCAMENTO_PARC').Value := dmBaixa.cdsBaixaLancG.FieldByName('ID_LANCAMENTO_PARC').AsInteger;
                QryAux.ExecSQL;
              end;

              if lIncParcelas then
                iQtdTotParcs := (dmBaixa.cdsBaixaLancG.FieldByName('QTD_PARCELAS').AsInteger + 1)
              else
                iQtdTotParcs := dmBaixa.cdsBaixaLancG.FieldByName('QTD_PARCELAS').AsInteger;

              if vgConSISTEMA.InTransaction then
                vgConSISTEMA.Commit;

              if Trim(dmBaixa.sTipoBaixa) = 'D' then
              begin
                if dmBaixa.cdsBaixaLancG.FieldByName('VR_TOTAL_PREV').AsCurrency >= dmBaixa.cDifTot then
                  dmBaixa.cDifTot := 0;
              end
              else
                dmBaixa.cDifTot := 0;

              lOk := True;
            except
              if vgConSISTEMA.InTransaction then
                vgConSISTEMA.Rollback;

              lOk := False;
            end;

            //Quitacao do Lancamento
            vgLanc_Codigo := dmBaixa.cdsBaixaLancG.FieldByName('COD_LANCAMENTO').AsInteger;
            vgLanc_Ano    := dmBaixa.cdsBaixaLancG.FieldByName('ANO_LANCAMENTO').AsInteger;
            vgLanc_TpLanc := dmBaixa.cdsBaixaLancG.FieldByName('TIPO_LANCAMENTO').AsString;

            Application.CreateForm(TdmLancamento, dmLancamento);
            dmLancamento.QuitarLancamento;
            FreeAndNil(dmLancamento);

            vgLanc_Codigo := 0;
            vgLanc_Ano    := 0;
            vgLanc_TpLanc := '';

{            //Quitacao do Lancamento
            qryQtdParcs.Close;
            qryQtdParcs.Params.ParamByName('COD_LANCAMENTO').Value := dmBaixa.cdsBaixaLancG.FieldByName('COD_LANCAMENTO').AsInteger;
            qryQtdParcs.Params.ParamByName('ANO_LANCAMENTO').Value := dmBaixa.cdsBaixaLancG.FieldByName('ANO_LANCAMENTO').AsInteger;
            qryQtdParcs.Open;

            iQtdParcsPagas := qryQtdParcs.FieldByName('QTD').AsInteger;

            if iQtdTotParcs = iQtdParcsPagas then
            begin
              try
                if vgConSISTEMA.Connected then
                  vgConSISTEMA.StartTransaction;

                vgLanc_Codigo := dmBaixa.cdsBaixaLancG.FieldByName('COD_LANCAMENTO').AsInteger;
                vgLanc_Ano    := dmBaixa.cdsBaixaLancG.FieldByName('ANO_LANCAMENTO').AsInteger;

                Application.CreateForm(TdmLancamento, dmLancamento);

                dmLancamento.QuitarLancamento(qryQtdParcs.FieldByName('TOTAL_PAGO').AsCurrency);

                FreeAndNil(dmLancamento);

                vgLanc_Codigo := 0;
                vgLanc_Ano := 0;

                if vgConSISTEMA.InTransaction then
                  vgConSISTEMA.Commit;

                lOk := True;
              except
                if vgConSISTEMA.InTransaction then
                  vgConSISTEMA.Rollback;

                lOk := False;
              end;
            end;  }
          end;

          dmBaixa.cdsBaixaLancG.Next;
        end;

        // Observacao de Sobra ou Falta de Caixa
        if dmBaixa.sTipoBaixa = 'O' then
        begin
          // DESPESA
          QryAux.Close;
          QryAux.SQL.Clear;
          QryAux.SQL.Text := 'SELECT OBSERVACAO ' +
                             '  FROM LANCAMENTO ' +
                             ' WHERE COD_LANCAMENTO = :COD_LANCAMENTO ' +
                             '   AND ANO_LANCAMENTO = :ANO_LANCAMENTO';
          QryAux.Params.ParamByName('COD_LANCAMENTO').Value := dmBaixa.cdsDespPagas.FieldByName('COD_LANCAMENTO_FK').AsInteger;
          QryAux.Params.ParamByName('ANO_LANCAMENTO').Value := dmBaixa.cdsDespPagas.FieldByName('ANO_LANCAMENTO_FK').AsInteger;
          QryAux.Open;

          if Trim(QryAux.FieldByName('OBSERVACAO').AsString) <> '' then
            sNovaObservacao := Trim(QryAux.FieldByName('OBSERVACAO').AsString) + #13#10 + sNovaObservacao;

          dmPrincipal.InicializarComponenteLancamento;

          DadosLancamento := Lancamento.Create;

          DadosLancamento.CodLancamento := dmBaixa.cdsDespPagas.FieldByName('COD_LANCAMENTO_FK').AsInteger;
          DadosLancamento.AnoLancamento := dmBaixa.cdsDespPagas.FieldByName('ANO_LANCAMENTO_FK').AsInteger;
          DadosLancamento.Observacao    := sNovaObservacao;

          ListaLancamentos.Add(DadosLancamento);

          dmPrincipal.UpdateLancamento(0);

          dmPrincipal.FinalizarComponenteLancamento;

          //Quitacao do Lancamento
          vgLanc_Codigo := dmBaixa.cdsDespPagas.FieldByName('COD_LANCAMENTO_FK').AsInteger;
          vgLanc_Ano    := dmBaixa.cdsDespPagas.FieldByName('ANO_LANCAMENTO_FK').AsInteger;
          vgLanc_TpLanc := dmBaixa.cdsDespPagas.FieldByName('TIPO_LANCAMENTO').AsString;

          Application.CreateForm(TdmLancamento, dmLancamento);
          dmLancamento.QuitarLancamento;
          FreeAndNil(dmLancamento);

          vgLanc_Codigo := 0;
          vgLanc_Ano    := 0;
          vgLanc_TpLanc := '';
        end;
      end;
    end;
  end;

  FreeAndNil(QryLancDeriv);
  FreeAndNil(QryAux);
  FreeAndNil(QryVinc);

  if lOk then
  begin
    Application.MessageBox('Baixa realizada com sucesso!', 'Sucesso!', MB_OK);

    cedValorTotalRepasse.Value := 0;
    cedValorTotalDespesa.Value := 0;
    edtDescricaoBaixa.Clear;
    mmObservacoes.Clear;

    btnFiltrarReceitas.Click;
    btnFiltrarDespesas.Click;
  end
  else if cVlrTotPago > 0 then
    Application.MessageBox('Erro durante a realiza��o da Baixa', 'Erro', MB_OK + MB_ICONERROR);
end;

procedure TFRepassesVincularDespesa.btnConfirmarDespesaClick(Sender: TObject);
begin
  cedValorTotalDespesa.Value := dmBaixa.cdsDespPagas.FieldByName('VR_PARCELA_PAGO').AsCurrency;

  if dteDataPagamento.CanFocus then
    dteDataPagamento.SetFocus;
end;

procedure TFRepassesVincularDespesa.btnFiltrarDespesasClick(Sender: TObject);
begin
  dmBaixa.cdsDespPagas.Close;
  dmBaixa.qryDespPagas.Close;

  dmBaixa.qryDespPagas.SQL.Clear;
  dmBaixa.qryDespPagas.SQL.Text := 'SELECT DISTINCT LP.COD_LANCAMENTO_FK, ' +
                                   '       LP.ANO_LANCAMENTO_FK, ' +
                                   '       LP.ID_LANCAMENTO_PARC, ' +
                                   '       LP.DATA_LANCAMENTO_PARC, ' +
                                   '       LP.VR_PARCELA_PREV, ' +
                                   '       LP.VR_PARCELA_PAGO, ' +
                                   '       LP.DATA_PAGAMENTO, ' +
                                   '       L.TIPO_LANCAMENTO, ' +
                                   '       L.ID_NATUREZA_FK, ' +
                                   '       N.DESCR_NATUREZA, ' +
                                   '       L.ID_CATEGORIA_DESPREC_FK, ' +
                                   '       C.DESCR_CATEGORIA_DESPREC, ' +
                                   '       L.ID_SUBCATEGORIA_DESPREC_FK, ' +
                                   '       S.DESCR_SUBCATEGORIA_DESPREC, ' +
                                   '       L.OBSERVACAO, ' +
                                   '       (CASE WHEN (BLD.DERIV_COD_LANCAMENTO_FK IS NULL) ' +
                                   '             THEN ' + QuotedStr('N') +
                                   '             ELSE ' + QuotedStr('S') +
                                   '        END) AS BAIXADO ' +
                                   '  FROM LANCAMENTO L ' +
                                   '  LEFT JOIN NATUREZA N ' +
                                   '    ON L.ID_NATUREZA_FK = N.ID_NATUREZA ' +
                                   '  LEFT JOIN CATEGORIA_DESPREC C ' +
                                   '    ON L.ID_CATEGORIA_DESPREC_FK = C.ID_CATEGORIA_DESPREC ' +
                                   '  LEFT JOIN SUBCATEGORIA_DESPREC S ' +
                                   '    ON L.ID_SUBCATEGORIA_DESPREC_FK = S.ID_SUBCATEGORIA_DESPREC ' +
                                   ' INNER JOIN LANCAMENTO_PARC LP ' +
                                   '    ON L.COD_LANCAMENTO = LP.COD_LANCAMENTO_FK ' +
                                   '   AND L.ANO_LANCAMENTO = LP.ANO_LANCAMENTO_FK ' +
                                   '  LEFT JOIN BAIXA_LANC_DERIVADO BLD ' +
                                   '    ON L.COD_LANCAMENTO = BLD.DERIV_COD_LANCAMENTO_FK ' +
                                   '   AND L.ANO_LANCAMENTO = BLD.DERIV_ANO_LANCAMENTO_FK ' +
                                   ' WHERE L.TIPO_LANCAMENTO = ' + QuotedStr('D') +
                                   '   AND LP.FLG_STATUS = ' + QuotedStr('G') +
                                   '   AND L.NUM_RECIBO IS NULL ' +
                                   '   AND LP.DATA_LANCAMENTO_PARC BETWEEN :DATA_INI AND :DATA_FIM ';

  //Valor
  if cedValorFlutuanteD.Value > 0 then
    dmBaixa.qryDespPagas.SQL.Add(' AND LP.VR_PARCELA_PAGO = ' + ReplaceStr(cedValorFlutuanteD.EditText, ',', '.'));

  dmBaixa.qryDespPagas.SQL.Add(' ORDER BY LP.DATA_LANCAMENTO_PARC DESC, ' +
                               '          L.COD_LANCAMENTO DESC, ' +
                               '          L.ANO_LANCAMENTO DESC');

  dmBaixa.cdsDespPagas.Params.ParamByName('DATA_INI').Value := dtePeriodoIniD.Date;
  dmBaixa.cdsDespPagas.Params.ParamByName('DATA_FIM').Value := dtePeriodoFimD.Date;
  dmBaixa.cdsDespPagas.Open;
end;

procedure TFRepassesVincularDespesa.btnFiltrarReceitasClick(Sender: TObject);
var
  QryAux: TFDQuery;
  sDescricao: String;
begin
  { Mesma query da tela UListaBaixaFlutuantes (qryBaixaLancG) }
  dmBaixa.cdsBaixaLancG.Close;
  dmBaixa.qryBaixaLancG.Close;

  dmBaixa.qryBaixaLancG.SQL.Clear;
  dmBaixa.qryBaixaLancG.SQL.Text := 'SELECT DISTINCT L.*, ' +
                                    '       LP.*, ' +
                                    '       LP.FLG_STATUS AS FLG_STATUS_PARC, ' +
                                    '       (CASE WHEN (BLD.DERIV_COD_LANCAMENTO_FK IS NOT NULL) ' +
                                    '             THEN ' + QuotedStr('S') +
                                    '             ELSE ' + QuotedStr('N') +
                                    '        END) AS BAIXADO ' +
                                    '  FROM LANCAMENTO L ' +
                                    ' INNER JOIN LANCAMENTO_PARC LP ' +
                                    '    ON L.COD_LANCAMENTO = LP.COD_LANCAMENTO_FK ' +
                                    '   AND L.ANO_LANCAMENTO = LP.ANO_LANCAMENTO_FK ' +
                                    '  LEFT JOIN BAIXA_LANC_DERIVADO BLD ' +
                                    '    ON L.COD_LANCAMENTO = BLD.ORIG_COD_LANCAMENTO_FK ' +
                                    '   AND L.ANO_LANCAMENTO = BLD.ORIG_ANO_LANCAMENTO_FK ' +
                                    ' WHERE L.FLG_FLUTUANTE = ' + QuotedStr('S') +
                                    '   AND L.TIPO_LANCAMENTO = ' + QuotedStr('R') +
                                    '   AND L.FLG_CANCELADO = ' + QuotedStr('N') +
                                    '   AND LP.FLG_STATUS <> ' + QuotedStr('C') +
                                    '   AND LP.DATA_LANCAMENTO_PARC BETWEEN :DATA_INI AND :DATA_FIM ';

  //Valor
  if cedValorFlutuanteR.Value > 0 then
    dmBaixa.qryBaixaLancG.SQL.Add(' AND LP.VR_PARCELA_PAGO = ' + ReplaceStr(cedValorFlutuanteR.EditText, ',', '.'));

  //Recibo
  if Trim(edtNumReciboFlutuante.Text) <> '' then
    dmBaixa.qryBaixaLancG.SQL.Add(' AND L.NUM_RECIBO = ' + Trim(edtNumReciboFlutuante.Text));

  //Situacao
  case rgSituacao.ItemIndex of
    1: //Sem baixa
      dmBaixa.qryBaixaLancG.SQL.Add(' AND BLD.DERIV_COD_LANCAMENTO_FK IS NULL');
    2: //Baixados
      dmBaixa.qryBaixaLancG.SQL.Add(' AND BLD.DERIV_COD_LANCAMENTO_FK IS NOT NULL');
  end;

  dmBaixa.qryBaixaLancG.SQL.Add(' ORDER BY LP.DATA_LANCAMENTO_PARC DESC, ' +
                                '          L.COD_LANCAMENTO DESC, ' +
                                '          L.ANO_LANCAMENTO DESC');

  dmBaixa.cdsBaixaLancG.Params.ParamByName('DATA_INI').Value := dtePeriodoIniR.Date;
  dmBaixa.cdsBaixaLancG.Params.ParamByName('DATA_FIM').Value := dtePeriodoFimR.Date;
  dmBaixa.cdsBaixaLancG.Open;

  dmBaixa.cdsBaixaLancG.DisableControls;

  dmBaixa.cdsBaixaLancG.First;

  while not dmBaixa.cdsBaixaLancG.Eof do
  begin
    QryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

    QryAux.Close;
    QryAux.SQL.Clear;

    QryAux.SQL.Text := 'SELECT UPPER(I.DESCR_ITEM) AS DESCR_ITEM ' +
                       '  FROM LANCAMENTO_DET LD ' +
                       ' INNER JOIN ITEM I ' +
                       '    ON LD.ID_ITEM_FK = I.ID_ITEM ' +
                       ' WHERE LD.COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                       '   AND LD.ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ' +
                       '   AND LD.FLG_CANCELADO = ' + QuotedStr('N');

    QryAux.Params.ParamByName('COD_LANCAMENTO').Value := dmBaixa.cdsBaixaLancG.FieldByName('COD_LANCAMENTO').AsInteger;
    QryAux.Params.ParamByName('ANO_LANCAMENTO').Value := dmBaixa.cdsBaixaLancG.FieldByName('ANO_LANCAMENTO').AsInteger;
    QryAux.Open;

    if QryAux.RecordCount > 0 then
    begin
      sDescricao := '';

      QryAux.First;

      while not QryAux.Eof do
      begin
        if Trim(sDescricao) = '' then
          sDescricao := QryAux.FieldByName('DESCR_ITEM').AsString
        else
          sDescricao := sDescricao + ' | ' + QryAux.FieldByName('DESCR_ITEM').AsString;

        QryAux.Next;
      end;

      if Trim(sDescricao) = '' then
        sDescricao := 'INDEFINIDO';

      dmBaixa.cdsBaixaLancG.Edit;
      dmBaixa.cdsBaixaLancG.FieldByName('DESCRICAO').AsString := Trim(sDescricao);
      dmBaixa.cdsBaixaLancG.Post;
    end;

    dmBaixa.cdsBaixaLancG.Next;
  end;

  dmBaixa.cdsBaixaLancG.EnableControls;

  MarcarDesmarcarTodas;

  dmBaixa.cdsBaixaLancG.First;
end;

procedure TFRepassesVincularDespesa.cedValorFlutuanteDKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then // TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    btnFiltrarDespesas.Click;
    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFRepassesVincularDespesa.cedValorFlutuanteRKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then // TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    btnFiltrarReceitas.Click;
    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFRepassesVincularDespesa.chbSelecionarTodasClick(Sender: TObject);
begin
  MarcarDesmarcarTodas;
end;

procedure TFRepassesVincularDespesa.dbgDespesasPagasDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if gdSelected in State then
    dbgDespesasPagas.Canvas.Brush.Color := clHighlight;

  dbgDespesasPagas.DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure TFRepassesVincularDespesa.dbgDespesasPagasTitleClick(Column: TColumn);
begin
  dmBaixa.cdsDespPagas.IndexFieldNames := Column.FieldName;
end;

procedure TFRepassesVincularDespesa.dbgRepassesCellClick(Column: TColumn);
begin
  if dbgRepasses.SelectedField.DataType = ftBoolean then
    SalvarBoleano;
end;

procedure TFRepassesVincularDespesa.dbgRepassesDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if gdSelected in State then
    dbgRepasses.Canvas.Brush.Color := clHighlight
  else
  begin
    if dmBaixa.cdsBaixaLancG.FieldByName('BAIXADO').AsString = 'S' then
      dbgRepasses.Canvas.Brush.Color := $00DFFFDF
    else
      dbgRepasses.Canvas.Brush.Color := $00A4C1FF;
  end;

  dbgRepasses.DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure TFRepassesVincularDespesa.dbgRepassesTitleClick(Column: TColumn);
begin
  dmBaixa.cdsBaixaLancG.IndexFieldNames := Column.FieldName;
end;

procedure TFRepassesVincularDespesa.dteDataPagamentoExit(Sender: TObject);
begin
  if dteDataPagamento.Date < vgDataLancVigente then
    dteDataPagamento.Date := vgDataLancVigente;
end;

procedure TFRepassesVincularDespesa.dteDataPagamentoKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dteDataPagamento.Date < vgDataLancVigente then
      dteDataPagamento.Date := vgDataLancVigente;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFRepassesVincularDespesa.dtePeriodoFimDKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then // TAB
  begin
    if dtePeriodoFimD.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso',
        MB_OK + MB_ICONWARNING);

      if dtePeriodoFimD.CanFocus then
        dtePeriodoFimD.SetFocus;
    end
    else if dtePeriodoIniD.Date > dtePeriodoFimD.Date then
    begin
      Application.MessageBox
        ('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso',
        MB_OK + MB_ICONWARNING);

      dtePeriodoFimD.Clear;

      if dtePeriodoFimD.CanFocus then
        dtePeriodoFimD.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    if dtePeriodoFimD.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso',
        MB_OK + MB_ICONWARNING);

      if dtePeriodoFimD.CanFocus then
        dtePeriodoFimD.SetFocus;
    end
    else if dtePeriodoIniD.Date > dtePeriodoFimD.Date then
    begin
      Application.MessageBox
        ('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso',
        MB_OK + MB_ICONWARNING);

      dtePeriodoFimD.Clear;

      if dtePeriodoFimD.CanFocus then
        dtePeriodoFimD.SetFocus;
    end
    else
      btnFiltrarDespesas.Click;

    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFRepassesVincularDespesa.dtePeriodoFimRKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then // TAB
  begin
    if dtePeriodoFimR.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso',
        MB_OK + MB_ICONWARNING);

      if dtePeriodoFimR.CanFocus then
        dtePeriodoFimR.SetFocus;
    end
    else if dtePeriodoIniR.Date > dtePeriodoFimR.Date then
    begin
      Application.MessageBox
        ('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso',
        MB_OK + MB_ICONWARNING);

      dtePeriodoFimR.Clear;

      if dtePeriodoFimR.CanFocus then
        dtePeriodoFimR.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    if dtePeriodoFimR.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso',
        MB_OK + MB_ICONWARNING);

      if dtePeriodoFimR.CanFocus then
        dtePeriodoFimR.SetFocus;
    end
    else if dtePeriodoIniR.Date > dtePeriodoFimR.Date then
    begin
      Application.MessageBox
        ('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso',
        MB_OK + MB_ICONWARNING);

      dtePeriodoFimR.Clear;

      if dtePeriodoFimR.CanFocus then
        dtePeriodoFimR.SetFocus;
    end
    else
      btnFiltrarReceitas.Click;

    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFRepassesVincularDespesa.dtePeriodoIniDKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then // TAB
  begin
    if dtePeriodoIniD.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso',
        MB_OK + MB_ICONWARNING);

      if dtePeriodoIniD.CanFocus then
        dtePeriodoIniD.SetFocus;
    end
    else if dtePeriodoIniD.Date > dtePeriodoFimD.Date then
    begin
      Application.MessageBox
        ('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso',
        MB_OK + MB_ICONWARNING);

      dtePeriodoIniD.Clear;

      if dtePeriodoIniD.CanFocus then
        dtePeriodoIniD.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    if dtePeriodoIniD.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso',
        MB_OK + MB_ICONWARNING);

      if dtePeriodoIniD.CanFocus then
        dtePeriodoIniD.SetFocus;
    end
    else if dtePeriodoIniD.Date > dtePeriodoFimD.Date then
    begin
      Application.MessageBox
        ('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso',
        MB_OK + MB_ICONWARNING);

      dtePeriodoIniD.Clear;

      if dtePeriodoIniD.CanFocus then
        dtePeriodoIniD.SetFocus;
    end
    else
      btnFiltrarDespesas.Click;

    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFRepassesVincularDespesa.dtePeriodoIniRKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then // TAB
  begin
    if dtePeriodoIniR.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso',
        MB_OK + MB_ICONWARNING);

      if dtePeriodoIniR.CanFocus then
        dtePeriodoIniR.SetFocus;
    end
    else if dtePeriodoIniR.Date > dtePeriodoFimR.Date then
    begin
      Application.MessageBox
        ('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso',
        MB_OK + MB_ICONWARNING);

      dtePeriodoIniR.Clear;

      if dtePeriodoIniR.CanFocus then
        dtePeriodoIniR.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    if dtePeriodoIniR.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso',
        MB_OK + MB_ICONWARNING);

      if dtePeriodoIniR.CanFocus then
        dtePeriodoIniR.SetFocus;
    end
    else if dtePeriodoIniR.Date > dtePeriodoFimR.Date then
    begin
      Application.MessageBox
        ('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso',
        MB_OK + MB_ICONWARNING);

      dtePeriodoIniR.Clear;

      if dtePeriodoIniR.CanFocus then
        dtePeriodoIniR.SetFocus;
    end
    else
      btnFiltrarReceitas.Click;

    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFRepassesVincularDespesa.edtNumReciboFlutuanteKeyPress(
  Sender: TObject; var Key: Char);
begin
  if Key = #9 then // TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    btnFiltrarReceitas.Click;
    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFRepassesVincularDespesa.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  vgOrigemCadastro := False;
end;

procedure TFRepassesVincularDespesa.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFRepassesVincularDespesa.FormShow(Sender: TObject);
begin
  Self.Color := vgCorSistema;

  dtePeriodoIniR.Date := StartOfTheMonth(Date);
  dtePeriodoFimR.Date := Date;

  cedValorFlutuanteR.Value := 0;

  edtNumReciboFlutuante.Clear;

  rgSituacao.ItemIndex := 1;

  btnFiltrarReceitas.Click;

  dtePeriodoIniD.Date := StartOfTheMonth(Date);
  dtePeriodoFimD.Date := Date;

  cedValorFlutuanteD.Value := 0;

  btnFiltrarDespesas.Click;

  dteDataPagamento.Date := vgDataLancVigente;

//  ShowScrollBar(dbgRepasses.Handle, SB_HORZ, False);
  ShowScrollBar(dbgDespesasPagas.Handle, SB_HORZ, False);

  if dtePeriodoIniR.CanFocus then
    dtePeriodoIniR.SetFocus;
end;

procedure TFRepassesVincularDespesa.MarcarDesmarcarTodas;
begin
  dmBaixa.cdsBaixaLancG.DisableControls;

  dmBaixa.cdsBaixaLancG.First;

  while not dmBaixa.cdsBaixaLancG.Eof do
  begin
    dmBaixa.cdsBaixaLancG.Edit;

    if dmBaixa.cdsBaixaLancG.FieldByName('BAIXADO').AsString = 'N' then
    begin
      if chbSelecionarTodas.Checked then
        dmBaixa.cdsBaixaLancG.FieldByName('SELECIONADO').AsBoolean := True
      else
        dmBaixa.cdsBaixaLancG.FieldByName('SELECIONADO').AsBoolean := False;
    end
    else
      dmBaixa.cdsBaixaLancG.FieldByName('SELECIONADO').AsBoolean := False;

    dmBaixa.cdsBaixaLancG.Post;

    dmBaixa.cdsBaixaLancG.Next;
  end;

  if dmBaixa.cdsBaixaLancG.FieldByName('SELECIONADO').AsBoolean then
    chbSelecionarTodas.Caption := 'Desmarcar Todas'
  else
    chbSelecionarTodas.Caption := 'Desmarcar Todas';

  dmBaixa.cdsBaixaLancG.EnableControls;
end;

procedure TFRepassesVincularDespesa.mmObservacoesKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then // TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    if dteDataPagamento.Date = 0 then
    begin
      Application.MessageBox('Falta informar a DATA DE PAGAMENTO.', 'Aviso', MB_OK);

      if dteDataPagamento.CanFocus then
        dteDataPagamento.SetFocus;
    end
    else
      btnConfirmarBaixa.Click;

    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFRepassesVincularDespesa.rgSituacaoClick(Sender: TObject);
begin
  btnFiltrarReceitas.Click;
end;

procedure TFRepassesVincularDespesa.rgSituacaoExit(Sender: TObject);
begin
  btnFiltrarReceitas.Click;
end;

procedure TFRepassesVincularDespesa.SalvarBoleano;
begin
  dbgRepasses.SelectedField.DataSet.Edit;

  if dmBaixa.cdsBaixaLancG.FieldByName('BAIXADO').AsString = 'N' then
    dbgRepasses.SelectedField.AsBoolean := not dbgRepasses.SelectedField.AsBoolean
  else
    dbgRepasses.SelectedField.AsBoolean := False;

//  dbgRepasses.SelectedField.AsBoolean := not dbgRepasses.SelectedField.AsBoolean;

  dbgRepasses.SelectedField.DataSet.Post;

  cedValorTotalRepasse.Value := dmBaixa.cValorTotalFlutuante;
end;

end.
