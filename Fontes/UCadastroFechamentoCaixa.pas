{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroFechamentoCaixa.pas
  Descricao:   Tela de cadastro de Fechamento de Caixa
  Author   :   Cristina
  Date:        23-dez-2016
  Last Update: 23-out-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroFechamentoCaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.DBClient, Datasnap.Provider,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.DBCtrls, JvBaseEdits,
  JvDBControls, JvMaskEdit, Vcl.Mask, JvExMask, JvToolEdit, JvExExtCtrls,
  JvExtComponent, JvPanel, System.DateUtils, Vcl.ExtDlgs,
  Vcl.Grids, Vcl.DBGrids, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinMetropolis, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxGDIPlusClasses, cxImage, Vcl.Clipbrd,
  JvImage, Vcl.Imaging.jpeg, Vcl.Imaging.pngimage, Vcl.Menus, cxTextEdit,
  cxMaskEdit, cxSpinEdit, cxTimeEdit, cxDBEdit, System.StrUtils, gtPDFClasses,
  gtCstPDFDoc, gtExPDFDoc, gtExProPDFDoc, gtPDFDoc, gtScrollingPanel,
  gtPDFViewer;

type
  TFCadastroFechamentoCaixa = class(TFCadastroGeralPadrao)
    ntbCadastro: TNotebook;
    lblNomeUsuarioResponsavel: TLabel;
    lblDataFechamento: TLabel;
    lblHoraFechamento: TLabel;
    lblVlrTotalUltAberta: TLabel;
    lblDataCancelamento: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    lblFechCxFechAnterior: TLabel;
    lblFechCxTotalRecebido: TLabel;
    lblFechCxTotalDespesas: TLabel;
    lblFechCxSaldoDia: TLabel;
    lblVlrRealNotas: TLabel;
    lblVlrRealMoedas: TLabel;
    lblVlrRealSaldoConta: TLabel;
    Shape1: TShape;
    lblDiferenca: TLabel;
    lblObservacoes: TLabel;
    lblFechamentoDia: TLabel;
    lblVisualizacao: TLabel;
    lblRecPendTotal: TLabel;
    lblSaldoGeral: TLabel;
    dteDataFechamento: TJvDBDateEdit;
    cedVlrTotalUltAberta: TJvDBCalcEdit;
    edtNomeUsuarioResponsavel: TDBEdit;
    dteDataCancelamento: TJvDBDateEdit;
    cedFechCxFechAnterior: TJvDBCalcEdit;
    cedFechCxTotalRecebido: TJvDBCalcEdit;
    cedFechCxTotalDespesas: TJvDBCalcEdit;
    cedFechCxSaldoDia: TJvDBCalcEdit;
    cedVlrRealNotas: TJvDBCalcEdit;
    cedVlrRealMoedas: TJvDBCalcEdit;
    cedVlrRealSaldoConta: TJvDBCalcEdit;
    cedDiferenca: TJvDBCalcEdit;
    mmObservacoes: TDBMemo;
    cedFechamentoDia: TJvDBCalcEdit;
    cedRecPendTotal: TJvDBCalcEdit;
    cedSaldoGeral: TJvDBCalcEdit;
    cedVlrRealTContFinal: TJvDBCalcEdit;
    lblVlrRealTContFinal: TLabel;
    btnAbaDigitalizacao: TJvTransparentButton;
    btnCancelarAcao: TJvTransparentButton;
    btnConfirmarAcao: TJvTransparentButton;
    btnColarImagem: TJvTransparentButton;
    btnAbrirPastaImagem: TJvTransparentButton;
    btnAcionarScanner: TJvTransparentButton;
    edtNomeArquivo: TEdit;
    btnEditarImagem: TJvTransparentButton;
    btnExcluirImagem: TJvTransparentButton;
    dbgImagens: TDBGrid;
    odAbrirPastaImagem: TOpenPictureDialog;
    ppmImagem: TPopupMenu;
    btnExportar: TMenuItem;
    medHoraFechamento: TJvDBMaskEdit;
    btnAbaAnalitico: TJvTransparentButton;
    ntbSinteticoAnalitico: TNotebook;
    Label5: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    lblImportRecibos: TLabel;
    lblImportDinheiro: TLabel;
    lblImportDeposito: TLabel;
    lblImportCartao: TLabel;
    lblImportCheque: TLabel;
    lblImportPromissoria: TLabel;
    lblImportFaturado: TLabel;
    lblImportRepasse: TLabel;
    lblOutrasRecTotal: TLabel;
    lblRecAntCartoes: TLabel;
    lblRecAntCheques: TLabel;
    lblRecAntPromissorias: TLabel;
    lblRecAntFaturados: TLabel;
    lblOutrasRecDinheiro: TLabel;
    lblOutrasRecDeposito: TLabel;
    lblOutrasRecCartao: TLabel;
    lblOutrasRecCheque: TLabel;
    lblOutrasRecPromissoria: TLabel;
    lblOutrasRecFaturado: TLabel;
    lblOutrasRecRepasse: TLabel;
    lblRecPendCartoes: TLabel;
    lblRecPendCheques: TLabel;
    lblRecPendPromissorias: TLabel;
    lblRecPendFaturados: TLabel;
    lblFlutDeposSBaixa: TLabel;
    lblFlutRecRepasses: TLabel;
    lblTotalDespesas: TLabel;
    lblDespDinheiro: TLabel;
    lblDespDescontos: TLabel;
    lblDespRepFlutuante: TLabel;
    cedImportRecibos: TJvDBCalcEdit;
    cedImportDinheiro: TJvDBCalcEdit;
    cedImportDeposito: TJvDBCalcEdit;
    cedImportCartao: TJvDBCalcEdit;
    cedImportCheque: TJvDBCalcEdit;
    cedImportPromissoria: TJvDBCalcEdit;
    cedImportFaturado: TJvDBCalcEdit;
    cedImportRepasse: TJvDBCalcEdit;
    cedOutrasRecTotal: TJvDBCalcEdit;
    cedRecAntCartoes: TJvDBCalcEdit;
    cedRecAntCheques: TJvDBCalcEdit;
    cedRecAntPromissorias: TJvDBCalcEdit;
    cedRecAntFaturados: TJvDBCalcEdit;
    cedRecPendCartoes: TJvDBCalcEdit;
    cedRecPendCheques: TJvDBCalcEdit;
    cedRecPendPromissorias: TJvDBCalcEdit;
    cedRecPendFaturados: TJvDBCalcEdit;
    cedOutrasRecDinheiro: TJvDBCalcEdit;
    cedOutrasRecDeposito: TJvDBCalcEdit;
    cedOutrasRecCartao: TJvDBCalcEdit;
    cedOutrasRecCheque: TJvDBCalcEdit;
    cedOutrasRecPromissoria: TJvDBCalcEdit;
    cedOutrasRecFaturado: TJvDBCalcEdit;
    cedOutrasRecRepasse: TJvDBCalcEdit;
    cedFlutDeposSBaixa: TJvDBCalcEdit;
    cedFlutRecRepasses: TJvDBCalcEdit;
    cedTotalDespesas: TJvDBCalcEdit;
    cedDespDinheiro: TJvDBCalcEdit;
    cedDespDescontos: TJvDBCalcEdit;
    cedDespRepFlutuante: TJvDBCalcEdit;
    lblImportRecibosS: TLabel;
    cedImportRecibosS: TJvDBCalcEdit;
    lblOutrasRecTotalS: TLabel;
    cedOutrasRecTotalS: TJvDBCalcEdit;
    lblRecAntTotalS: TLabel;
    cedRecAntTotalS: TJvDBCalcEdit;
    lblRecPendTotS: TLabel;
    cedRecPendTotS: TJvDBCalcEdit;
    Label36: TLabel;
    lblFlutDeposSBaixaS: TLabel;
    lblFlutRecRepassesS: TLabel;
    cedFlutRecRepassesS: TJvDBCalcEdit;
    cedFlutDeposSBaixaS: TJvDBCalcEdit;
    btnRelatorioMovimentoCaixa: TJvTransparentButton;
    docPDF: TgtPDFDocument;
    odAbrirPDF: TOpenDialog;
    odSalvarPDF: TOpenDialog;
    ntbArquivo: TNotebook;
    pnlImagem: TPanel;
    imgImagemFechamento: TcxImage;
    vwPDF: TgtPDFViewer;
    btnAssociarPDF: TJvTransparentButton;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cedVlrRealNotasExit(Sender: TObject);
    procedure cedVlrRealSaldoContaExit(Sender: TObject);
    procedure cedVlrRealMoedasExit(Sender: TObject);
    procedure mmObservacoesKeyPress(Sender: TObject; var Key: Char);
    procedure cedImportRecibosClick(Sender: TObject);
    procedure cedImportDinheiroClick(Sender: TObject);
    procedure cedImportDepositoClick(Sender: TObject);
    procedure cedImportCartaoClick(Sender: TObject);
    procedure cedImportChequeClick(Sender: TObject);
    procedure cedImportPromissoriaClick(Sender: TObject);
    procedure cedImportFaturadoClick(Sender: TObject);
    procedure cedImportRepasseClick(Sender: TObject);
    procedure cedOutrasRecTotalClick(Sender: TObject);
    procedure cedOutrasRecDinheiroClick(Sender: TObject);
    procedure cedOutrasRecDepositoClick(Sender: TObject);
    procedure cedOutrasRecCartaoClick(Sender: TObject);
    procedure cedOutrasRecChequeClick(Sender: TObject);
    procedure cedOutrasRecPromissoriaClick(Sender: TObject);
    procedure cedOutrasRecFaturadoClick(Sender: TObject);
    procedure cedOutrasRecRepasseClick(Sender: TObject);
    procedure cedFechamentoDiaClick(Sender: TObject);
    procedure cedDiferencaClick(Sender: TObject);
    procedure cedRecAntCartoesClick(Sender: TObject);
    procedure cedRecAntChequesClick(Sender: TObject);
    procedure cedRecAntPromissoriasClick(Sender: TObject);
    procedure cedRecAntFaturadosClick(Sender: TObject);
    procedure cedRecPendCartoesClick(Sender: TObject);
    procedure cedRecPendChequesClick(Sender: TObject);
    procedure cedRecPendPromissoriasClick(Sender: TObject);
    procedure cedRecPendFaturadosClick(Sender: TObject);
    procedure cedFlutDeposSBaixaClick(Sender: TObject);
    procedure cedFlutRecRepassesClick(Sender: TObject);
    procedure cedFechCxFechAnteriorClick(Sender: TObject);
    procedure cedFechCxTotalRecebidoClick(Sender: TObject);
    procedure cedFechCxTotalDespesasClick(Sender: TObject);
    procedure cedFechCxSaldoDiaClick(Sender: TObject);
    procedure cedTotalDespesasClick(Sender: TObject);
    procedure cedDespDinheiroClick(Sender: TObject);
    procedure cedDespDescontosClick(Sender: TObject);
    procedure cedDespRepFlutuanteClick(Sender: TObject);
    procedure cedVlrRealNotasClick(Sender: TObject);
    procedure cedVlrRealMoedasClick(Sender: TObject);
    procedure cedVlrRealSaldoContaClick(Sender: TObject);
    procedure cedVlrRealTContFinalClick(Sender: TObject);
    procedure cedRecPendTotalClick(Sender: TObject);
    procedure cedSaldoGeralClick(Sender: TObject);
    procedure btnAbaDigitalizacaoClick(Sender: TObject);
    procedure btnDadosPrincipaisClick(Sender: TObject);
    procedure btnAcionarScannerClick(Sender: TObject);
    procedure btnAbrirPastaImagemClick(Sender: TObject);
    procedure btnColarImagemClick(Sender: TObject);
    procedure btnConfirmarAcaoClick(Sender: TObject);
    procedure btnCancelarAcaoClick(Sender: TObject);
    procedure edtNomeArquivoKeyPress(Sender: TObject; var Key: Char);
    procedure dbgImagensColEnter(Sender: TObject);
    procedure dbgImagensDblClick(Sender: TObject);
    procedure dbgImagensDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnEditarImagemClick(Sender: TObject);
    procedure btnExcluirImagemClick(Sender: TObject);
    procedure btnExportarClick(Sender: TObject);
    procedure dteDataFechamentoKeyPress(Sender: TObject; var Key: Char);
    procedure dteDataFechamentoExit(Sender: TObject);
    procedure btnAbaAnaliticoClick(Sender: TObject);
    procedure btnRelatorioMovimentoCaixaClick(Sender: TObject);
    procedure btnAssociarPDFClick(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }

    sObsAtraso,
    sDataUltFechamento: String;

    lEmAtraso,
    lTabAuxDig: Boolean;

    iIdIncImg: Integer;

    sNomeOriginal: String;

    procedure GerarRelatorioMovimentoCaixa(GerarPDF: Boolean);
    procedure CalcularTotaisFechamento(DataUltAb, DataProxFech: TDatetime);
    procedure CalcularFechamentoDia;
    procedure HabDesabBotoesCRUD(Inc, Ed, Exc: Boolean);
    procedure DesabilitaHabilitaCampos(Hab: Boolean);
    procedure VerificarDepositosPendentes;
  public
    { Public declarations }
  end;

var
  FCadastroFechamentoCaixa: TFCadastroFechamentoCaixa;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMCaixa, UCapturaImagem,
  UDMRelatorios, UFiltroRelatorioMovimentoCaixa, UDMBaixa,
  ULancamentosVincularDepositos;

{ TFCadastroFechamentoCaixa }

procedure TFCadastroFechamentoCaixa.btnAbaAnaliticoClick(Sender: TObject);
begin
  inherited;

  MarcarDesmarcarAbas(PRINC);

  btnDadosPrincipais.Transparent := True;
  btnAbaAnalitico.Transparent    := False;

  ntbSinteticoAnalitico.PageIndex := 1;
end;

procedure TFCadastroFechamentoCaixa.btnAbaDigitalizacaoClick(Sender: TObject);
begin
  inherited;

  //DIGITALIZACAO
  if not lTabAuxDig then
  begin
    with dmCaixa do
    begin
      if not (vgOperacao in [C, I]) then
      begin
        if cdsFechCXImg_F.RecordCount > 0 then
          HabDesabBotoesCRUD(True, True, True)
        else
          HabDesabBotoesCRUD(True, False, False);
      end;

      if (vgOperacao <> I) and
        (dmCaixa.cdsFechCXImg_F.FieldByName('CAMINHO_COMPLETO').AsString <> '') then
      begin
        if dmCaixa.cdsFechCXImg_F.FieldByName('EXTENSAO').AsString = 'jpg' then
          imgImagemFechamento.Picture.LoadFromFile(dmCaixa.cdsFechCXImg_F.FieldByName('CAMINHO_COMPLETO').AsString)
        else if dmCaixa.cdsFechCXImg_F.FieldByName('EXTENSAO').AsString = 'pdf' then
        begin
          docPDF.Reset;
          docPDF.LoadFromFile(dmCaixa.cdsFechCXImg_F.FieldByName('CAMINHO_COMPLETO').AsString);
          vwPDF.Align := alClient;
          vwPDF.PDFDocument := docPDF;
          vwPDF.Active := True;
        end;
      end;

      DesabilitaHabilitaCampos(False);
    end;

    lTabAuxDig := True;

    if dmCaixa.cdsFechCXImg_F.RecordCount > 0 then
    begin
      imgImagemFechamento.PopupMenu := ppmImagem;
      vwPDF.PopupMenu               := ppmImagem;
    end
    else
    begin
      imgImagemFechamento.PopupMenu := nil;
      vwPDF.PopupMenu               := nil;
    end;

    ntbArquivo.PageIndex := 0;
  end;

  MarcarDesmarcarAbas(DIG);
end;

procedure TFCadastroFechamentoCaixa.btnAbrirPastaImagemClick(Sender: TObject);
var
  sCEscolhido, sCaminho: String;
  j, iPos: Integer;
begin
  inherited;

  imgImagemFechamento.PopupMenu := nil;

  ntbArquivo.PageIndex := 0;

  odAbrirPastaImagem.InitialDir := vgConf_DiretorioImagens +
                                   IntToStr(YearOf(dmCaixa.cdsFechCX_F.FieldByName('DATA_FECHAMENTO').AsDateTime)) +
                                   '\';
  iPos := 0;

  if odAbrirPastaImagem.Execute then
  begin
    if odAbrirPastaImagem.FileName <> '' then
    begin
      sCEscolhido := odAbrirPastaImagem.FileName;

      for j := 0 to Length(sCEscolhido) - 1 do
      begin
        if sCEscolhido[j] = '\' then
          iPos := j;
      end;

      sCaminho  := Copy(sCEscolhido, 1, iPos);

      try
        //Tela
        imgImagemFechamento.Picture.LoadFromFile(odAbrirPastaImagem.FileName);

        //Banco
        Inc(iIdIncImg);
        dmCaixa.cdsFechCXImg_F.Append;
        dmCaixa.cdsFechCXImg_F.FieldByName('NOVO').AsBoolean                    := True;
        dmCaixa.cdsFechCXImg_F.FieldByName('ID_CAIXA_FECHAMENTO_IMG').AsInteger := iIdIncImg;

        if UpperCase(sCaminho) <> UpperCase(vgConf_DiretorioImagens +
                                            IntToStr(YearOf(dmCaixa.cdsFechCX_F.FieldByName('DATA_FECHAMENTO').AsDateTime))
                                            + '\') then
          dmCaixa.cdsFechCXImg_F.FieldByName('ORIGEM_ANTIGA').AsString := sCEscolhido;

        HabDesabBotoesCRUD(False, False, False);
        DesabilitaHabilitaCampos(True);

        if edtNomeArquivo.CanFocus then
          edtNomeArquivo.SetFocus;
      except
        //Nao exibir erro caso nao consiga carregar a Imagem
        if dmCaixa.cdsFechCXImg_F.State = dsInsert then
          dmCaixa.cdsFechCXImg_F.Cancel;

        HabDesabBotoesCRUD(True, True, True);
        DesabilitaHabilitaCampos(False);

        if dmCaixa.cdsFechCXImg_F.RecordCount > 0 then
        begin
          imgImagemFechamento.PopupMenu := ppmImagem;
          vwPDF.PopupMenu               := ppmImagem
        end;
      end;
    end;
  end;
end;

procedure TFCadastroFechamentoCaixa.btnAcionarScannerClick(Sender: TObject);
var
  Imagem: TBitmap;
begin
  inherited;

  imgImagemFechamento.PopupMenu := nil;

  Imagem := nil;

  try
    Application.CreateForm(TFCapturaImagem, FCapturaImagem);
    FCapturaImagem.ShowModal;
  finally
    Imagem := FCapturaImagem.ImagemCapturada;
    FreeAndNil(FCapturaImagem);
  end;

  if Imagem <> nil then
  begin
    try
      //Tela
      imgImagemFechamento.Picture.Assign(Imagem);

      //Banco
      Inc(iIdIncImg);
      dmCaixa.cdsFechCXImg_F.Append;
      dmCaixa.cdsFechCXImg_F.FieldByName('NOVO').AsBoolean                    := True;
      dmCaixa.cdsFechCXImg_F.FieldByName('ID_CAIXA_FECHAMENTO_IMG').AsInteger := iIdIncImg;

      HabDesabBotoesCRUD(False, False, False);
      DesabilitaHabilitaCampos(True);

      if edtNomeArquivo.CanUndo then
        edtNomeArquivo.SetFocus;
    except
      HabDesabBotoesCRUD(True, True, True);
      DesabilitaHabilitaCampos(False);

      if dmCaixa.cdsFechCXImg_F.RecordCount > 0 then
        imgImagemFechamento.PopupMenu := ppmImagem;
    end;
  end;
end;

procedure TFCadastroFechamentoCaixa.btnAssociarPDFClick(Sender: TObject);
var
  sCEscolhido, sCaminho: String;
  j, iPos: Integer;
begin
  inherited;

  sNomeOriginal := '';

  vwPDF.PopupMenu := nil;

  ntbArquivo.PageIndex := 1;

  odAbrirPDF.InitialDir := vgConf_DiretorioImagens +
                           IntToStr(YearOf(dmCaixa.cdsFechCX_F.FieldByName('DATA_FECHAMENTO').AsDateTime)) + '\';
  iPos := 0;

  if odAbrirPDF.Execute then
  begin
    if odAbrirPDF.FileName <> '' then
    begin
      sNomeOriginal := odAbrirPDF.FileName;
      sCEscolhido   := odAbrirPDF.FileName;

      for j := 0 to Length(sCEscolhido) - 1 do
      begin
        if sCEscolhido[j] = '\' then
          iPos := j;
      end;

      sCaminho := Copy(sCEscolhido, 1, iPos);

      try
        //Tela
        docPDF.Reset;
        docPDF.LoadFromFile(odAbrirPDF.FileName);
        vwPDF.Align := alClient;
        vwPDF.PDFDocument := docPDF;
        vwPDF.Active := True;

        //Banco
        Inc(iIdIncImg);
        dmCaixa.cdsFechCXImg_F.Append;
        dmCaixa.cdsFechCXImg_F.FieldByName('NOVO').AsBoolean          := True;
        dmCaixa.cdsFechCXImg_F.FieldByName('ID_CAIXA_FECHAMENTO_IMG').AsInteger := iIdIncImg;

        if UpperCase(sCaminho) <> UpperCase(vgConf_DiretorioImagens +
                                            IntToStr(YearOf(dmCaixa.cdsFechCX_F.FieldByName('DATA_FECHAMENTO').AsDateTime)) + '\') then
          dmCaixa.cdsFechCXImg_F.FieldByName('ORIGEM_ANTIGA').AsString := sCEscolhido;

        HabDesabBotoesCRUD(False, False, False);
        DesabilitaHabilitaCampos(True);

        if edtNomeArquivo.CanFocus then
          edtNomeArquivo.SetFocus;
      except
        //Nao exibir erro caso nao consiga carregar o Arquivo
        if dmCaixa.cdsFechCXImg_F.State = dsInsert then
          dmCaixa.cdsFechCXImg_F.Cancel;

        HabDesabBotoesCRUD(True, True, True);
        DesabilitaHabilitaCampos(False);

        if dmCaixa.cdsFechCXImg_F.RecordCount > 0 then
        begin
          imgImagemFechamento.PopupMenu := ppmImagem;
          vwPDF.PopupMenu               := ppmImagem;
        end;
      end;
    end;
  end;
end;

procedure TFCadastroFechamentoCaixa.btnCancelarAcaoClick(Sender: TObject);
begin
  inherited;

  dmCaixa.cdsFechCXImg_F.Cancel;

  edtNomeArquivo.Clear;

  if dmCaixa.cdsFechCXImg_F.RecordCount = 0 then
    HabDesabBotoesCRUD(True, False, False)
  else
    HabDesabBotoesCRUD(True, True, True);

  DesabilitaHabilitaCampos(False);

  if dmCaixa.cdsFechCXImg_F.RecordCount > 0 then
  begin
    imgImagemFechamento.PopupMenu := ppmImagem;
    vwPDF.PopupMenu               := ppmImagem;
  end;
end;

procedure TFCadastroFechamentoCaixa.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if (vgOperacao = C) and
    (vgIdConsulta > 0) then
  begin
    if lPodeCancelar then
      Self.Close;
  end
  else
  begin
    if lPodeCancelar then
    begin
      { IMAGENS }
      if dmCaixa.cdsFechCXImg_F.RecordCount > 0 then
      begin
        dmCaixa.cdsFechCXImg_F.First;

        while not dmCaixa.cdsFechCXImg_F.Eof do
        begin
          if not dmCaixa.cdsFechCXImg_F.FieldByName('NOME_V_ANTIGO').IsNull then
          begin
            if FileExists(vgConf_DiretorioImagens +
                          IntToStr(YearOf(dmCaixa.cdsFechCX_F.FieldByName('DATA_FECHAMENTO').AsDateTime)) +
                          '\' +
                          dmCaixa.cdsFechCXImg_F.FieldByName('NOME_FIXO').AsString +
                          dmCaixa.cdsFechCXImg_F.FieldByName('NOME_V_ANTIGO').AsString + '.' +
                          dmCaixa.cdsFechCXImg_F.FieldByName('EXTENSAO').AsString) then
            begin
              DeleteFile(vgConf_DiretorioImagens +
                         IntToStr(YearOf(dmCaixa.cdsFechCX_F.FieldByName('DATA_FECHAMENTO').AsDateTime)) +
                         '\' +
                         dmCaixa.cdsFechCXImg_F.FieldByName('NOME_FIXO').AsString +
                         dmCaixa.cdsFechCXImg_F.FieldByName('NOME_V_ANTIGO').AsString + '.' +
                         dmCaixa.cdsFechCXImg_F.FieldByName('EXTENSAO').AsString);
            end;
          end;

          if dmCaixa.cdsFechCXImg_F.FieldByName('NOVO').AsBoolean and
            dmCaixa.cdsFechCXImg_F.FieldByName('ID_CAIXA_FECHAMENTO_FK').IsNull then
          begin
            if FileExists(vgConf_DiretorioImagens +
                          IntToStr(YearOf(dmCaixa.cdsFechCX_F.FieldByName('DATA_FECHAMENTO').AsDateTime)) +
                          '\' +
                          dmCaixa.cdsFechCXImg_F.FieldByName('NOME_FIXO').AsString +
                          dmCaixa.cdsFechCXImg_F.FieldByName('NOME_VARIAVEL').AsString + '.' +
                          dmCaixa.cdsFechCXImg_F.FieldByName('EXTENSAO').AsString) then
            begin
              DeleteFile(vgConf_DiretorioImagens +
                         IntToStr(YearOf(dmCaixa.cdsFechCX_F.FieldByName('DATA_FECHAMENTO').AsDateTime)) +
                         '\' +
                         dmCaixa.cdsFechCXImg_F.FieldByName('NOME_FIXO').AsString +
                         dmCaixa.cdsFechCXImg_F.FieldByName('NOME_VARIAVEL').AsString + '.' +
                         dmCaixa.cdsFechCXImg_F.FieldByName('EXTENSAO').AsString);
            end;
          end;

          dmCaixa.cdsFechCXImg_F.Next;
        end;
      end;

      Self.Close;
    end;
  end;
end;

procedure TFCadastroFechamentoCaixa.btnColarImagemClick(Sender: TObject);
var
  imgBitmap: TBitmap;
begin
  inherited;

  imgImagemFechamento.PopupMenu := nil;

  ntbArquivo.PageIndex := 0;

  try
    Clipboard.Open;

    if Clipboard.HasFormat(CF_PICTURE) or
      Clipboard.HasFormat(CF_BITMAP) then
    begin
      //Tela
      imgBitmap := TBitmap.Create;
      imgBitmap.Assign(Clipboard);
      imgImagemFechamento.Picture.Bitmap.Assign(imgBitmap);

      //Banco
      Inc(iIdIncImg);
      dmCaixa.cdsFechCXImg_F.Append;
      dmCaixa.cdsFechCXImg_F.FieldByName('NOVO').AsBoolean                    := True;
      dmCaixa.cdsFechCXImg_F.FieldByName('ID_CAIXA_FECHAMENTO_IMG').AsInteger := iIdIncImg;

      HabDesabBotoesCRUD(False, False, False);
      DesabilitaHabilitaCampos(True);

      if edtNomeArquivo.CanFocus then
        edtNomeArquivo.SetFocus;
    end;

    Clipboard.Close;

    FreeAndNil(imgBitmap);
  except
    FreeAndNil(imgBitmap);

    if dmCaixa.cdsFechCXImg_F.RecordCount > 0 then
    begin
      imgImagemFechamento.PopupMenu := ppmImagem;
      vwPDF.PopupMenu               := ppmImagem;
    end;
  end;
end;

procedure TFCadastroFechamentoCaixa.btnConfirmarAcaoClick(Sender: TObject);
var
  sNomeFoto, MsgErro: String;
  imgJpg: TJPEGImage;
  arqPDF: TgtPDFDocument;
begin
  inherited;

  MsgErro := '';

  if Trim(edtNomeArquivo.Text) = '' then
  begin
    MsgErro := 'Nenhum nome foi escolhido para o arquivo.' + #13 +
               'Por favor, digite um nome para o mesmo.';

    Application.MessageBox(PChar(MsgErro), 'Erro', MB_OK + MB_ICONERROR);

    if edtNomeArquivo.CanFocus then
      edtNomeArquivo.SetFocus;
  end
  else
  begin
    sNomeFoto := 'CXF_' +
                 dmGerencial.PegarNumeroTexto(dmCaixa.cdsFechCX_F.FieldByName('DATA_FECHAMENTO').AsString) +
                 '_' +
                 Trim(edtNomeArquivo.Text) +
                 IfThen(ntbArquivo.PageIndex = 0, '.jpg', '.pdf');

    if FileExists(vgConf_DiretorioImagens +
                  IntToStr(YearOf(dmCaixa.cdsFechCX_F.FieldByName('DATA_FECHAMENTO').AsDateTime)) +
                  '\' + sNomeFoto) then
    begin
      MsgErro := 'J� existe um arquivo com o nome escolhido.' + #13 +
                 'Por favor, digite outro nome para o arquivo.';

      Application.MessageBox(PChar(MsgErro), 'Aviso', MB_OK + MB_ICONEXCLAMATION);

      if edtNomeArquivo.CanFocus then
        edtNomeArquivo.SetFocus;
    end
    else
    begin
      if dmCaixa.cdsFechCXImg_F.State = dsInsert then
      begin
        //Arquivo
        if ntbArquivo.PageIndex = 0 then
        begin
          dmGerencial.Processando(True, 'Salvando Imagem', True, 'Por favor, aguarde...');
          imgJpg := TJPEGImage.Create;
          imgJpg.Assign(imgImagemFechamento.Picture.Graphic);
          imgJpg.SaveToFile(vgConf_DiretorioImagens +
                            IntToStr(YearOf(dmCaixa.cdsFechCX_F.FieldByName('DATA_FECHAMENTO').AsDateTime)) +
                            '\' + sNomeFoto);
          dmGerencial.Processando(False);
        end
        else if ntbArquivo.PageIndex = 1 then
        begin
          if Trim(sNomeOriginal) <> '' then
          begin
            dmGerencial.Processando(True, 'Salvando arquivo PDF', True, 'Por favor, aguarde...');
            arqPDF := TgtPDFDocument.Create(nil);
            arqPDF.LoadFromFile(sNomeOriginal);
            arqPDF.SaveToFile(vgConf_DiretorioImagens +
                              IntToStr(YearOf(dmCaixa.cdsFechCX_F.FieldByName('DATA_FECHAMENTO').AsDateTime)) +
                              '\' +
                              sNomeFoto);
            arqPDF.OpenAfterSave := True;
            dmGerencial.Processando(False);
          end;
        end;
      end;

      if not dmCaixa.cdsFechCXImg_F.FieldByName('NOME_V_ANTIGO').IsNull then
      begin
        //Arquivo
        if ntbArquivo.PageIndex = 0 then
        begin
          dmGerencial.Processando(True, 'Salvando Imagem', True, 'Por favor, aguarde...');
          imgJpg := TJPEGImage.Create;
          imgJpg.Assign(imgImagemFechamento.Picture.Graphic);
          imgJpg.SaveToFile(vgConf_DiretorioImagens +
                            IntToStr(YearOf(dmCaixa.cdsFechCX_F.FieldByName('DATA_FECHAMENTO').AsDateTime)) +
                            '\' + sNomeFoto);
          dmGerencial.Processando(False);
        end
        else if ntbArquivo.PageIndex = 1 then
        begin
          if Trim(sNomeOriginal) <> '' then
          begin
            dmGerencial.Processando(True, 'Salvando arquivo PDF', True, 'Por favor, aguarde...');
            arqPDF := TgtPDFDocument.Create(nil);
            arqPDF.LoadFromFile(sNomeOriginal);
            arqPDF.SaveToFile(vgConf_DiretorioImagens +
                              IntToStr(YearOf(dmCaixa.cdsFechCX_F.FieldByName('DATA_FECHAMENTO').AsDateTime)) +
                              '\' +
                              sNomeFoto);
            arqPDF.OpenAfterSave := True;
            dmGerencial.Processando(False);
          end;
        end;
      end;

      dmCaixa.cdsFechCXImg_F.FieldByName('NOME_VARIAVEL').AsString := Trim(edtNomeArquivo.Text);
      dmCaixa.cdsFechCXImg_F.FieldByName('NOME_FIXO').AsString     := 'CXF_' +
                                                                      dmGerencial.PegarNumeroTexto(dmCaixa.cdsFechCX_F.FieldByName('DATA_FECHAMENTO').AsString)
                                                                      + '_';
      dmCaixa.cdsFechCXImg_F.FieldByName('EXTENSAO').AsString      := IfThen(ntbArquivo.PageIndex = 0, 'jpg', 'pdf');

      dmCaixa.cdsFechCXImg_F.Post;

      HabDesabBotoesCRUD(True, True, True);
      DesabilitaHabilitaCampos(False);

      FreeAndNil(imgJpg);
      FreeAndNil(arqPDF);
    end;
  end;

  imgImagemFechamento.PopupMenu := ppmImagem;

  if (ntbArquivo.PageIndex = 1) and
    (Trim(sNomeOriginal) <> '') then
  begin
    docPDF.Reset;
    docPDF.LoadFromFile(vgConf_DiretorioImagens +
                        IntToStr(YearOf(dmCaixa.cdsFechCX_F.FieldByName('DATA_FECHAMENTO').AsDateTime)) +
                        '\' +
                        sNomeFoto);
    vwPDF.Align := alClient;
    vwPDF.PDFDocument := docPDF;
    vwPDF.Active := True;
  end;

  vwPDF.PopupMenu := ppmImagem;

  edtNomeArquivo.Clear;
end;

procedure TFCadastroFechamentoCaixa.btnDadosPrincipaisClick(Sender: TObject);
begin
  inherited;

  MarcarDesmarcarAbas(PRINC);

  btnDadosPrincipais.Transparent := False;
  btnAbaAnalitico.Transparent    := True;

  ntbSinteticoAnalitico.PageIndex := 0;
end;

procedure TFCadastroFechamentoCaixa.btnEditarImagemClick(Sender: TObject);
begin
  inherited;

  if dmCaixa.cdsFechCXImg_F.RecordCount > 0 then
  begin
    dmCaixa.cdsFechCXImg_F.Edit;
    dmCaixa.cdsFechCXImg_F.FieldByName('NOME_V_ANTIGO').AsString := dmCaixa.cdsFechCXImg_F.FieldByName('NOME_VARIAVEL').AsString;
    edtNomeArquivo.Text := dmCaixa.cdsFechCXImg_F.FieldByName('NOME_VARIAVEL').AsString;
    HabDesabBotoesCRUD(False, False, False);
    DesabilitaHabilitaCampos(True);
  end;
end;

procedure TFCadastroFechamentoCaixa.btnExcluirImagemClick(Sender: TObject);
begin
  inherited;

  if dmCaixa.cdsFechCXImg_F.RecordCount > 0 then
    dmCaixa.cdsFechCXImg_F.Delete;
end;

procedure TFCadastroFechamentoCaixa.btnExportarClick(Sender: TObject);
var
  sCEscolhido: String;
  j, iPos: Integer;
  imgJpg: TJPEGImage;
  arqPDF: TgtPDFDocument;
begin
  inherited;

  //Exporta para outro diretorio, a escolha do usuario
  if Trim(dmCaixa.cdsFechCXImg_F.FieldByName('EXTENSAO').AsString) = 'jpg' then
  begin
    odAbrirPastaImagem.InitialDir := 'C:\';
    iPos := 0;

    if odAbrirPastaImagem.Execute then
    begin
      if odAbrirPastaImagem.FileName <> '' then
      begin
        sCEscolhido := odAbrirPastaImagem.FileName;

        for j := 0 to Length(sCEscolhido) - 1 do
        begin
          if sCEscolhido[j] = '\' then
            iPos := j;
        end;

        imgJpg := TJPEGImage.Create;
        imgJpg.Assign(imgImagemFechamento.Picture.Graphic);
        imgJpg.SaveToFile(sCEscolhido);
      end;
    end;
  end
  else if Trim(dmCaixa.cdsFechCXImg_F.FieldByName('EXTENSAO').AsString) = 'pdf' then
  begin
    odSalvarPDF.InitialDir := 'C:\';
    iPos := 0;

    if odSalvarPDF.Execute then
    begin
      if odSalvarPDF.FileName <> '' then
      begin
        sCEscolhido := odSalvarPDF.FileName;

        for j := 0 to Length(sCEscolhido) - 1 do
        begin
          if sCEscolhido[j] = '\' then
            iPos := j;
        end;

        arqPDF := TgtPDFDocument.Create(nil);
        arqPDF.LoadFromFile(dmCaixa.cdsFechCXImg_F.FieldByName('CAMINHO_COMPLETO').AsString);
        arqPDF.SaveToFile(sCEscolhido);
        arqPDF.OpenAfterSave := True;
      end;
    end;
  end;
end;

procedure TFCadastroFechamentoCaixa.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
  begin
    VerificarDepositosPendentes;
    GravarGenerico;
  end
  else
    Self.Close;
end;

procedure TFCadastroFechamentoCaixa.btnRelatorioMovimentoCaixaClick(
  Sender: TObject);
begin
  inherited;

  GerarRelatorioMovimentoCaixa(False);
end;

procedure TFCadastroFechamentoCaixa.CalcularFechamentoDia;
var
  cVlrNotas: Currency;
begin
  { DESPESAS }
  //Total de Despesas
  cedTotalDespesas.Value := (cedDespDinheiro.Value +
                             cedDespDescontos.Value +
                             cedDespRepFlutuante.Value);

  { FECHAMENTO }
  //Total Recebido
  cedFechCxTotalRecebido.Value := (cedImportRecibos.Value +
                                   cedOutrasRecTotal.Value -
                                   (cedImportCartao.Value +
                                    cedImportCheque.Value +
                                    cedImportPromissoria.Value +
                                    cedImportFaturado.Value) -
                                   (cedOutrasRecCartao.Value +
                                    cedOutrasRecCheque.Value +
                                    cedOutrasRecPromissoria.Value +
                                    cedOutrasRecFaturado.Value) +
                                   (cedRecAntCartoes.Value +
                                    cedRecAntCheques.Value +
                                    cedRecAntPromissorias.Value +
                                    cedRecAntFaturados.Value));

  //Saldo do Dia
  cedFechCxTotalDespesas.Value := cedTotalDespesas.Value;

  cedFechCxSaldoDia.Value := (cedFechCxFechAnterior.Value +
             (cedFechCxTotalRecebido.Value -
              cedFechCxTotalDespesas.Value));

  { VALORES REAIS }
  //Total Contagem Final
  cVlrNotas := Trunc(cedVlrRealNotas.Value);

  cedVlrRealTContFinal.Value := (cVlrNotas +
                                 cedVlrRealMoedas.Value +
                                 cedVlrRealSaldoConta.Value);

  { FECHAMENTO DO DIA }
  cedFechamentoDia.Value := (cedVlrRealTContFinal.Value - cedFlutDeposSBaixa.Value);

  { DIFERENCA }
  cedDiferenca.Value := (cedFechamentoDia.Value -
                         cedFechCxSaldoDia.Value);

  { SALDO GERAL }
  cedSaldoGeral.Value := (cedFechamentoDia.Value + cedRecPendTotal.Value);

  if cedDiferenca.Value = 0 then
  begin
    cedDiferenca.Color      := clMenu;
    lblDiferenca.Font.Color := clWindowText;
  end
  else if cedDiferenca.Value > 0 then
  begin
    cedDiferenca.Color      := $00DFFFDF;
    lblDiferenca.Font.Color := clGreen;
  end
  else if cedDiferenca.Value < 0 then
  begin
    cedDiferenca.Color      := $00DCD9FF;
    lblDiferenca.Font.Color := clMaroon;
  end;
end;

procedure TFCadastroFechamentoCaixa.CalcularTotaisFechamento(DataUltAb, DataProxFech: TDatetime);
begin
  lEmAtraso := False;

  dmCaixa.cdsFechCX_F.Close;
  dmCaixa.cdsFechCX_F.Params.ParamByName('ID_CAIXA_FECHAMENTO').Value := vgIdConsulta;
  dmCaixa.cdsFechCX_F.Open;

  if (vgOperacao in [I, E]) or
    ((vgOperacao = C) and
     (vgIdConsulta = 0)) then  //Inclusao, Edicao ou Parcial
  begin
    {dmCaixa.AbrirQrys((FormatDateTime('YYYY-MM-DD', DataUltAb) + ' 00:00:00'),
                      (FormatDateTime('YYYY-MM-DD', DataProxFech) + ' 23:59:59')); }

    dmCaixa.AbrirQrys((FormatDateTime('YYYY-MM-DD', DataUltAb)),
                      (FormatDateTime('YYYY-MM-DD', DataProxFech)));

    if vgOperacao = I then
      dmCaixa.cdsFechCX_F.Append
    else
      dmCaixa.cdsFechCX_F.Edit;

    dmCaixa.cdsFechCX_F.FieldByName('VR_TOTAL_ULT_AB').AsCurrency     := 0;
    dmCaixa.cdsFechCX_F.FieldByName('VR_FECH_ANTERIOR').AsCurrency    := 0;
    dmCaixa.cdsFechCX_F.FieldByName('VR_NOTAS').AsCurrency            := 0;
    dmCaixa.cdsFechCX_F.FieldByName('VR_MOEDAS').AsCurrency           := 0;
    dmCaixa.cdsFechCX_F.FieldByName('VR_CONTACORRENTE').AsCurrency    := 0;
    dmCaixa.cdsFechCX_F.FieldByName('VR_CONTAGEM_FINAL').AsCurrency   := 0;
    dmCaixa.cdsFechCX_F.FieldByName('VR_TOTAL_RECEITA').AsCurrency    := 0;
    dmCaixa.cdsFechCX_F.FieldByName('VR_TOTAL_DESPESA').AsCurrency    := 0;
    dmCaixa.cdsFechCX_F.FieldByName('VR_TOTAL_FECHAMENTO').AsCurrency := 0;
    dmCaixa.cdsFechCX_F.FieldByName('VR_SALDO_DIA').AsCurrency        := 0;
    dmCaixa.cdsFechCX_F.FieldByName('VR_DIFERENCA').AsCurrency        := 0;
    dmCaixa.cdsFechCX_F.FieldByName('VR_SRVANT_TOTAL').AsCurrency     := 0;
    dmCaixa.cdsFechCX_F.FieldByName('VR_RECPEND_TOTAL').AsCurrency    := 0;
    dmCaixa.cdsFechCX_F.FieldByName('VR_SALDO_GERAL').AsCurrency      := 0;

    { IMPORTACAO }
    //Total
    if dmCaixa.qryImportTotal.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_RECIBO_IMPORTADO').AsCurrency := dmCaixa.qryImportTotal.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_RECIBO_IMPORTADO').AsCurrency := 0;

    //Pgto. Dinheiro
    if dmCaixa.qryImportDH.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_IMPORT_DINHEIRO').AsCurrency := dmCaixa.qryImportDH.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_IMPORT_DINHEIRO').AsCurrency := 0;

    //Pgto. Deposito
    if dmCaixa.qryImportDP.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_IMPORT_DEPOSITO').AsCurrency := dmCaixa.qryImportDP.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_IMPORT_DEPOSITO').AsCurrency := 0;

    //Pgto. Cartao
    if dmCaixa.qryImportCT.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_IMPORT_CARTAO').AsCurrency := dmCaixa.qryImportCT.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_IMPORT_CARTAO').AsCurrency := 0;

    //Pgto. Cheque
    if dmCaixa.qryImportCH.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_IMPORT_CHEQUE').AsCurrency := dmCaixa.qryImportCH.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_IMPORT_CHEQUE').AsCurrency := 0;

    //Pgto. Promissoria
    if dmCaixa.qryImportPR.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_IMPORT_PROMISSORIA').AsCurrency := dmCaixa.qryImportPR.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_IMPORT_PROMISSORIA').AsCurrency := 0;

    //Pgto. Faturado
    if dmCaixa.qryImportFT.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_IMPORT_FATURADO').AsCurrency := dmCaixa.qryImportFT.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_IMPORT_FATURADO').AsCurrency := 0;

    //Receitas para Repasses
    if dmCaixa.qryImportRep.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_IMPORT_REPASSES').AsCurrency := dmCaixa.qryImportRep.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_IMPORT_REPASSES').AsCurrency := 0;

    { OUTRAS RECEITAS }
    //Total
    if dmCaixa.qryORecTotal.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_OUTRASREC_TOTAL').AsCurrency := dmCaixa.qryORecTotal.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_OUTRASREC_TOTAL').AsCurrency := 0;

    //Pgto. Dinheiro
    if dmCaixa.qryORecDH.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_OUTRASREC_DINHEIRO').AsCurrency := dmCaixa.qryORecDH.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_OUTRASREC_DINHEIRO').AsCurrency := 0;

    //Pgto. Deposito
    if dmCaixa.qryORecDP.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_OUTRASREC_DEPOSITO').AsCurrency := dmCaixa.qryORecDP.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_OUTRASREC_DEPOSITO').AsCurrency := 0;

    //Pgto. Cartao
    if dmCaixa.qryORecCT.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_OUTRASREC_CARTAO').AsCurrency := dmCaixa.qryORecCT.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_OUTRASREC_CARTAO').AsCurrency := 0;

    //Pgto. Cheque
    if dmCaixa.qryORecCH.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_OUTRASREC_CHEQUE').AsCurrency := dmCaixa.qryORecCH.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_OUTRASREC_CHEQUE').AsCurrency := 0;

    //Pgto. Promissoria
    if dmCaixa.qryORecPR.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_OUTRASREC_PROMISSORIA').AsCurrency := dmCaixa.qryORecPR.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_OUTRASREC_PROMISSORIA').AsCurrency := 0;

    //Pgto. Faturado
    if dmCaixa.qryORecFT.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_OUTRASREC_FATURADO').AsCurrency := dmCaixa.qryORecFT.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_OUTRASREC_FATURADO').AsCurrency := 0;

    //Receitas para Repasses
    if dmCaixa.qryORecRep.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_OUTRASREC_REPASSES').AsCurrency := dmCaixa.qryORecRep.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_OUTRASREC_REPASSES').AsCurrency := 0;

    { RECEITAS ORIUNDAS DE RECIBOS ANTERIORES }
    //Cartao
    if dmCaixa.qryRecAntCT.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_SRVANT_CARTAO').AsCurrency := dmCaixa.qryRecAntCT.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_SRVANT_CARTAO').AsCurrency := 0;

    //Cheque
    if dmCaixa.qryRecAntCH.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_SRVANT_CHEQUE').AsCurrency := dmCaixa.qryRecAntCH.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_SRVANT_CHEQUE').AsCurrency := 0;

    //Promissoria
    if dmCaixa.qryRecAntPR.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_SRVANT_PROMISSORIA').AsCurrency := dmCaixa.qryRecAntPR.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_SRVANT_PROMISSORIA').AsCurrency := 0;

    //Faturado
    if dmCaixa.qryRecAntFT.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_SRVANT_FATURADO').AsCurrency := dmCaixa.qryRecAntFT.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_SRVANT_FATURADO').AsCurrency := 0;

    { RECEITAS PENDENTES }
    //Cartao
    if dmCaixa.qryRecPendCT.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_RECPEND_CARTAO').AsCurrency := dmCaixa.qryRecPendCT.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_RECPEND_CARTAO').AsCurrency := 0;

    //Cheque
    if dmCaixa.qryRecPendCH.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_RECPEND_CHEQUE').AsCurrency := dmCaixa.qryRecPendCH.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_RECPEND_CHEQUE').AsCurrency := 0;

    //Promissoria
    if dmCaixa.qryRecPendPR.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_RECPEND_PROMISSORIA').AsCurrency := dmCaixa.qryRecPendPR.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_RECPEND_PROMISSORIA').AsCurrency := 0;

    //Faturado
    if dmCaixa.qryRecPendFT.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_RECPEND_FATURADO').AsCurrency := dmCaixa.qryRecPendFT.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_RECPEND_FATURADO').AsCurrency := 0;

    { RELACAO DE FLUTUANTES }
    //Depositos Sem Baixa
    if dmCaixa.qryDeposSBx.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_FLUT_DEPOSITO').AsCurrency := dmCaixa.qryDeposSBx.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_FLUT_DEPOSITO').AsCurrency := 0;

    //Receitas para Repasse
    if dmCaixa.qryFlutSBx.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_FLUT_REPASSE').AsCurrency := dmCaixa.qryFlutSBx.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_FLUT_REPASSE').AsCurrency := 0;

    { DESPESAS }
    //Dinheiro
    if dmCaixa.qryDespDH.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_DESP_DINHEIRO').AsCurrency := dmCaixa.qryDespDH.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_DESP_DINHEIRO').AsCurrency := 0;

    //Descontos
    if dmCaixa.qryDespDesc.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_DESP_DESCONTO').AsCurrency := dmCaixa.qryDespDesc.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_DESP_DESCONTO').AsCurrency := 0;

    //Repasses Flutuantes
    if dmCaixa.qryDespRepFlut.RecordCount > 0 then
      dmCaixa.cdsFechCX_F.FieldByName('VR_DESP_REPASSE').AsCurrency := dmCaixa.qryDespRepFlut.FieldByName('VALOR').AsCurrency
    else
      dmCaixa.cdsFechCX_F.FieldByName('VR_DESP_REPASSE').AsCurrency := 0;

    if (vgOperacao = I) or
      ((vgOperacao = C) and
       (vgIdConsulta = 0)) then  //Inclusao ou Parcial
    begin
      dmCaixa.cdsFechCX_F.FieldByName('DATA_FECHAMENTO').AsDateTime  := DataProxFech;
      dmCaixa.cdsFechCX_F.FieldByName('VR_TOTAL_ULT_AB').AsCurrency  := vgVlrTotalAb;
      dmCaixa.cdsFechCX_F.FieldByName('VR_FECH_ANTERIOR').AsCurrency := vgVlrTotalFech;
      dmCaixa.cdsFechCX_F.FieldByName('VR_NOTAS').AsCurrency         := vgVlrNotasAb;
      dmCaixa.cdsFechCX_F.FieldByName('VR_MOEDAS').AsCurrency        := vgVlrMoedasAb;
      dmCaixa.cdsFechCX_F.FieldByName('VR_CONTACORRENTE').AsCurrency := vgVlrContaAb;
    end;

    if vgOperacao = I then
    begin
      dmCaixa.cdsFechCX_F.FieldByName('CAD_ID_USUARIO').AsInteger := vgUsu_Id;
      dmCaixa.cdsFechCX_F.FieldByName('FLG_CANCELADO').AsString   := 'N';

      if vgDataProxFech < Date then
      begin
        lEmAtraso := True;

        dmCaixa.cdsFechCX_F.FieldByName('HORA_FECHAMENTO').AsString := '18:00:00';

        if Trim(sObsAtraso) = '' then
        begin
          repeat
            sObsAtraso := InputBox('MOTIVO', 'Fechamento de Caixa Atrasado. Por favor, indique o motivo do atraso:', '');
          until sObsAtraso <> '';
        end;
      end
      else
        dmCaixa.cdsFechCX_F.FieldByName('HORA_FECHAMENTO').AsString := TimeToStr(Now);
    end;
  end;
end;

procedure TFCadastroFechamentoCaixa.cedDespDescontosClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedDespDescontos, 1, 'DESPESAS: Descontos',
  'Descontos cedidos nos Recibos ou em outras Receitas.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedDespDinheiroClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedDespDinheiro, 1, 'DESPESAS: Dinheiro',
  'Soma das Despesas do Cart�rio.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedDespRepFlutuanteClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedDespRepFlutuante, 1, 'DESPESAS: Repasses Flutuantes',
  PChar('Despesa oriundas servi�o pendente de realiza��o, pagos pelos clientes anteriormente, ' +
  'tais como, Envios, Pagamento de Servi�os realizados por outros Cart�rios, etc.'), clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedDiferencaClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedDiferenca, 1, 'DIFEREN�A',
  PChar('A diferen�a correponde ao saldo do dia que eu deveria ter menos o ' +
  'Total da Contagem Final. ' + #13#10 +
  'Caso este campo esteja verde, trata-se de uma Sobra de caixa e caso o campo ' +
  'esteja vermelho, trata-se de uma Falta de caixa.'), clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedFechamentoDiaClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedFechamentoDia, 1, 'FECHAMENTO DO DIA',
  PChar('Saldo Final deste dia correspondente do Valor Total da Contagem menos o ' +
  'valor dos Dep�sitos Flutuantes, ou seja, aqueles que cuja baixa ainda n�o foi realizada.'), clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedFechCxFechAnteriorClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedFechCxFechAnterior, 1, 'FECHAMENTO: Fechamento Anterior',
  'Saldo Total do dia anterior.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedFechCxSaldoDiaClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedFechCxSaldoDia, 1, 'FECHAMENTO: Saldo do Dia',
  PChar('Saldo deste dia, correspondente ao c�lculo do Saldo do Dia (Receitas - Despesas), ' +
  'somando o resultado ao Saldo do Dia Anterior.'), clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedFechCxTotalDespesasClick(
  Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedFechCxTotalDespesas, 1, 'FECHAMENTO: Total Despesas',
  'Valor total das Despesas geradas nesta data, inclusive as oriundas de Repasses.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedFechCxTotalRecebidoClick(
  Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedFechCxTotalRecebido, 1, 'FECHAMENTO: Total Recebido',
  'Valor Total das Receitas recebidas nesta data.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedFlutDeposSBaixaClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedFlutDeposSBaixa, 1, 'RELA��O DE FLUTUANTES: Dep�sitos sem Baixa',
  PChar('Valor Total de Dep�sitos efetuados na conta do Cart�rio, cujos servi�os ' +
  'ainda n�o foram realizados. ' + #13#10 +
  'Obs: Este valor ser� diminu�do quando vinculado a algum cadastro de Receita.'), clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedFlutRecRepassesClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedFlutRecRepasses, 1, 'RELA��O DE FLUTUANTES: Receitas para Repasses',
  PChar('Valor Total de Receitas geradas para realiza��o de outros servi�os, cujos ' +
  'servi�os ainda n�o foram realizados. Inclusive, somando as entradas realizadas neste dia, ' +
  'caso existam.'), clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedImportCartaoClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedImportCartao, 1, 'IMPORTA��O: Pagto. Cart�o',
  'Recibos pagos por Cart�o de D�bito ou Cr�dito.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedImportChequeClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedImportCheque, 1, 'IMPORTA��O: Pagto. Cheque',
  'Recibos pagos em Cheque � Vista ou Pr�.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedImportDepositoClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedImportDeposito, 1, 'IMPORTA��O: Pagto. Dep�sito',
  'Recibos pagos por Dep�sito.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedImportDinheiroClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedImportDinheiro, 1, 'IMPORTA��O: Pagto. Dinheiro',
  'Recibos pagos em Dinheiro.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedImportFaturadoClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedImportFaturado, 1, 'IMPORTA��O: Pagto. Faturado',
  'Recibos Faturados para pagamento posterior.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedImportPromissoriaClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedImportPromissoria, 1, 'IMPORTA��O: Pagto. Promiss�ria',
  'Recibos Pagos por Nota Promiss�ria.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedImportRecibosClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedImportRecibos, 1, 'IMPORTA��O: Total Recibos Importados',
  'Valor Total importado de Recibos emitidos pelo Cart�rio neste dia.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedImportRepasseClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedImportRepasse, 1, 'IMPORTA��O: Receitas para Repasses',
  PChar('Valores pagos para realiza��o de outros servi�os, tais como Dilig�ncias, ' +
  'Envios, Servi�os de Outras Serventias. ' + #13#10 +
  'Obs: Valor j� calculado nas formas de pagamentos acima.'), clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedOutrasRecCartaoClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedOutrasRecCartao, 1, 'OUTRAS RECEITAS: Pagto. Cart�o',
  'Outras Receitas pagas com Cart�o de D�bito ou Cr�dito.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedOutrasRecChequeClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedOutrasRecCheque, 1, 'OUTRAS RECEITAS: Pagto. Cheque',
  'Outras Receitas pagas com Cheque � Vista ou Pr�.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedOutrasRecDepositoClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedOutrasRecDeposito, 1, 'OUTRAS RECEITAS: Pagto. Dep�sito',
  'Outras Receitas pagas em Dep�sito.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedOutrasRecDinheiroClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedOutrasRecDinheiro, 1, 'OUTRAS RECEITAS: Pagto. Dinheiro',
  'Outras Receitas pagas em Dinheiro.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedOutrasRecFaturadoClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedOutrasRecFaturado, 1, 'OUTRAS RECEITAS: Pagto. Faturado',
  'Outras Receitas Faturadas para pagamento posterior.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedOutrasRecPromissoriaClick(
  Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedOutrasRecPromissoria, 1, 'OUTRAS RECEITAS: Pagto. Promiss�ria',
  'Outras Receitas pagas com Nota Promiss�ria.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedOutrasRecRepasseClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedOutrasRecRepasse, 1, 'OUTRAS RECEITAS: Receitas para Repasses',
  PChar('Valores pagos para realiza��o de outros servi�os, tais como Dilig�ncias, ' +
  'Envios, Servi�os de Outras Serventias. ' + #13#10 +
  'Obs: Valor j� calculado nas formas de pagamentos acima.'), clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedOutrasRecTotalClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedOutrasRecTotal, 1, 'OUTRAS RECEITAS: Total Outras Receitas',
  PChar('Valor Total de outras Receitas n�o oriundas de emiss�o de Recibos, ou seja, ' +
  'aquelas cadastradas direto no sistema Monitoramento neste dia.'), clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedRecAntCartoesClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedRecAntCartoes, 1, 'RECEITAS ORIUNDAS DE SERVI�OS ANTERIORES: Cart�es',
  'Pagamentos anteriores por Cart�o de D�bito ou Cr�dito compesados na data de hoje.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedRecAntChequesClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedRecAntCheques, 1, 'RECEITAS ORIUNDAS DE SERVI�OS ANTERIORES: Cheques',
  'Pagamentos anteriores por Cheque � Vista ou Pr� compensados na data de hoje.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedRecAntFaturadosClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedRecAntFaturados, 1, 'RECEITAS ORIUNDAS DE SERVI�OS ANTERIORES: Faturados',
  'Pagamentos anteriormente Faturados, pagos na data de hoje.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedRecAntPromissoriasClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedRecAntPromissorias, 1, 'RECEITAS ORIUNDAS DE SERVI�OS ANTERIORES: Promiss�rias',
  'Pagamentos anteriores por Nota Promiss�ria resgatada na data de hoje.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedRecPendCartoesClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedRecPendCartoes, 1, 'RECEITAS PENDENTES: Cart�es',
  PChar('Pagamentos por Cart�o de D�bito ou Cr�dito, ' +
  'realizados nesta ou em outras datas anteriores, cujos quais ainda n�o foram compensados.'), clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedRecPendChequesClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedRecPendCheques, 1, 'RECEITAS PENDENTES: Cheques',
  PChar('Pagamentos por Cheque � Vista ou Pr� realizado nesta ou ' +
  'em outras datas anteriores, cujos quais ainda n�o foram compensados.'), clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedRecPendFaturadosClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedRecPendFaturados, 1, 'RECEITAS PENDENTES: Faturados',
  PChar('Pagamentos Faturados, realizados nesta ou em outras datas ' +
  'anteriores, cujos quais ainda n�o foram quitados.'), clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedRecPendPromissoriasClick(
  Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedRecPendPromissorias, 1, 'RECEITAS PENDENTES: Promiss�rias',
  PChar('Pagamentos por Nota Promiss�ria, realizados nesta ou em outras ' +
  'datas anteriores, cujos quais ainda n�o foram compensados.'), clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedRecPendTotalClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedFechamentoDia, 1, 'TOTAL DAS RECEITAS PENDENTES',
  PChar('Valor Total das Receitas Pendentes (Cart�es, Cheques, Promiss�rias e Faturados).'), clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedSaldoGeralClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedTotalDespesas, 1, 'SALDO GERAL',
  'Soma do Fechamento do Dia + Total das Receitas Pendentes.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedTotalDespesasClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedTotalDespesas, 1, 'DESPESAS: Total de Despesas',
  'Soma de todas as Despesas geradas para a data de hoje.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedVlrRealMoedasClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedVlrRealMoedas, 1, 'VALORES REAIS: Moedas',
  'Valor em Moedas contado no fim do dia.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedVlrRealMoedasExit(Sender: TObject);
begin
  inherited;

  CalcularFechamentoDia;
end;

procedure TFCadastroFechamentoCaixa.cedVlrRealNotasClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedVlrRealNotas, 1, 'VALORES REAIS: Notas',
  'Valor em Notas contado no fim do dia.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedVlrRealNotasExit(Sender: TObject);
begin
  inherited;

  cedVlrRealNotas.Value := Trunc(cedVlrRealNotas.Value);

  CalcularFechamentoDia;
end;

procedure TFCadastroFechamentoCaixa.cedVlrRealSaldoContaClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedVlrRealSaldoConta, 1, 'VALORES REAIS: Saldo da Conta',
  'Saldo da Conta Corrente no momento do Fechamento.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.cedVlrRealSaldoContaExit(Sender: TObject);
begin
  inherited;

  CalcularFechamentoDia;
end;

procedure TFCadastroFechamentoCaixa.cedVlrRealTContFinalClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(cedVlrRealTContFinal, 1, 'VALORES REAIS: Total Contagem Final',
  'Soma dos valores acima, gerando o valor Total em poder do Servi�o no momento deste Fechamento.', clBlue, clNavy);
end;

procedure TFCadastroFechamentoCaixa.dbgImagensColEnter(Sender: TObject);
begin
  inherited;

  if dmCaixa.cdsFechCXImg_F.FieldByName('CAMINHO_COMPLETO').AsString <> '' then
  begin
    if dmCaixa.cdsFechCXImg_F.FieldByName('EXTENSAO').AsString = 'jpg' then
    begin
      ntbArquivo.PageIndex := 0;
      imgImagemFechamento.Picture.LoadFromFile(dmCaixa.cdsFechCXImg_F.FieldByName('CAMINHO_COMPLETO').AsString);
    end
    else if dmCaixa.cdsFechCXImg_F.FieldByName('EXTENSAO').AsString = 'pdf' then
    begin
      ntbArquivo.PageIndex := 1;
      docPDF.Reset;
      docPDF.LoadFromFile(dmCaixa.cdsFechCXImg_F.FieldByName('CAMINHO_COMPLETO').AsString);
      vwPDF.Align := alClient;
      vwPDF.PDFDocument := docPDF;
      vwPDF.Active := True;
    end;
  end;
end;

procedure TFCadastroFechamentoCaixa.dbgImagensDblClick(Sender: TObject);
begin
  inherited;

  if vgOperacao in [I, E] then
    btnEditarImagem.Click;
end;

procedure TFCadastroFechamentoCaixa.dbgImagensDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;

  AlternarCorGrid(Sender, Rect, DataCol, Column, State);
end;

procedure TFCadastroFechamentoCaixa.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtNomeUsuarioResponsavel.MaxLength  := 30;
end;

procedure TFCadastroFechamentoCaixa.DesabilitaHabilitaCampos(Hab: Boolean);
begin
  edtNomeArquivo.Enabled := Hab;
  dbgImagens.Enabled := not Hab;
end;

procedure TFCadastroFechamentoCaixa.DesabilitarComponentes;
begin
  inherited;

  edtNomeUsuarioResponsavel.ReadOnly  := True;
  cedVlrTotalUltAberta.ReadOnly       := True;
  dteDataFechamento.ReadOnly          := (vgOperacao in [C, E]);
  medHoraFechamento.ReadOnly          := (vgOperacao in [C, E]);

  { IMPORTACAO }
  cedImportRecibos.ReadOnly     := True;
  cedImportDinheiro.ReadOnly    := True;
  cedImportDeposito.ReadOnly    := True;
  cedImportCartao.ReadOnly      := True;
  cedImportCheque.ReadOnly      := True;
  cedImportPromissoria.ReadOnly := True;
  cedImportFaturado.ReadOnly    := True;
  cedImportRepasse.ReadOnly     := True;

  { OUTRAS RECEITAS }
  cedOutrasRecTotal.ReadOnly       := True;
  cedOutrasRecDinheiro.ReadOnly    := True;
  cedOutrasRecDeposito.ReadOnly    := True;
  cedOutrasRecCartao.ReadOnly      := True;
  cedOutrasRecCheque.ReadOnly      := True;
  cedOutrasRecPromissoria.ReadOnly := True;
  cedOutrasRecFaturado.ReadOnly    := True;
  cedOutrasRecRepasse.ReadOnly     := True;

  { RECEITAS ORIUNDAS DE SERVICOS ANTERIORES }
  cedRecAntCartoes.ReadOnly      := True;
  cedRecAntCheques.ReadOnly      := True;
  cedRecAntPromissorias.ReadOnly := True;
  cedRecAntFaturados.ReadOnly    := True;

  { RECEITAS PENDENTES }
  cedRecPendCartoes.ReadOnly      := True;
  cedRecPendCheques.ReadOnly      := True;
  cedRecPendPromissorias.ReadOnly := True;
  cedRecPendFaturados.ReadOnly    := True;

  { RELACOES DE FLUTUANTES }
  cedFlutDeposSBaixaS.ReadOnly := True;
  cedFlutRecRepassesS.ReadOnly := True;
  cedFlutDeposSBaixa.ReadOnly  := True;
  cedFlutRecRepasses.ReadOnly  := True;

  { FECHAMENTO }
  cedFechCxFechAnterior.ReadOnly   := True;
  cedFechCxTotalRecebido.ReadOnly  := True;
  cedFechCxTotalDespesas.ReadOnly  := True;
  cedFechCxSaldoDia.ReadOnly       := True;

  { DESPESAS }
  cedTotalDespesas.ReadOnly    := True;
  cedDespDinheiro.ReadOnly     := True;
  cedDespDescontos.ReadOnly    := True;
  cedDespRepFlutuante.ReadOnly := True;

  { VALORES REAIS }
  cedVlrRealTContFinal.ReadOnly  := True;

  cedFechamentoDia.ReadOnly  := True;
  cedDiferenca.ReadOnly      := True;
  cedRecPendTotal.ReadOnly   := True;
  cedSaldoGeral.ReadOnly     := True;

  lblNomeUsuarioResponsavel.Visible := (vgOperacao in [I, E]) or
                                       ((vgOperacao = C) and
                                        (vgIdConsulta > 0));

  edtNomeUsuarioResponsavel.Visible := (vgOperacao in [I, E]) or
                                       ((vgOperacao = C) and
                                        (vgIdConsulta > 0));
  lblHoraFechamento.Visible := (vgOperacao in [I, E]) or
                               ((vgOperacao = C) and
                                (vgIdConsulta > 0));

  medHoraFechamento.Visible := (vgOperacao in [I, E]) or
                               ((vgOperacao = C) and
                                (vgIdConsulta > 0));

  lblDataCancelamento.Visible := (vgOperacao <> I) and
                                 (dmCaixa.cdsFechCX_F.FieldByName('FLG_CANCELADO').AsString = 'S');
  dteDataCancelamento.Visible := (vgOperacao <> I) and
                                 (dmCaixa.cdsFechCX_F.FieldByName('FLG_CANCELADO').AsString = 'S');

  if vgOperacao = C then
  begin
    edtNomeUsuarioResponsavel.Enabled  := False;
    dteDataFechamento.Enabled          := False;
    medHoraFechamento.Enabled          := False;
    cedVlrTotalUltAberta.Enabled       := False;
    dteDataCancelamento.Enabled        := False;

    { VALORES REAIS }
    cedVlrRealNotas.ReadOnly       := True;
    cedVlrRealMoedas.ReadOnly      := True;
    cedVlrRealSaldoConta.ReadOnly  := True;
    cedVlrRealTContFinal.ReadOnly  := True;

    mmObservacoes.ReadOnly  := True;

    { IMAGEM }
    btnAcionarScanner.Enabled   := False;
    btnAbrirPastaImagem.Enabled := False;
    btnColarImagem.Enabled      := False;
    btnAssociarPDF.Enabled      := False;
    btnConfirmarAcao.Enabled    := False;
    btnCancelarAcao.Enabled     := False;
    edtNomeArquivo.Enabled      := False;
    btnEditarImagem.Enabled     := False;
    btnExcluirImagem.Enabled    := False;

    dbgImagens.ReadOnly := True;
  end;
end;

procedure TFCadastroFechamentoCaixa.dteDataFechamentoExit(Sender: TObject);
begin
  inherited;

  if (vgOperacao = I) and
    (dmCaixa.cdsFechCX_F.State in [dsInsert, dsEdit]) then
  begin
    if (dteDataFechamento.Date < vgDataProxFech) or
      (dteDataFechamento.Date > Date) then
      dteDataFechamento.Date := vgDataProxFech;

    CalcularTotaisFechamento(dteDataFechamento.Date, dteDataFechamento.Date);

    CalcularFechamentoDia;
  end;
end;

procedure TFCadastroFechamentoCaixa.dteDataFechamentoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if (vgOperacao = I) and
      (dmCaixa.cdsFechCX_F.State in [dsInsert, dsEdit]) then
    begin
      if (dteDataFechamento.Date < vgDataProxFech) or
        (dteDataFechamento.Date > Date) then
        dteDataFechamento.Date := vgDataProxFech;

      CalcularTotaisFechamento(dteDataFechamento.Date, dteDataFechamento.Date);
      CalcularFechamentoDia;
    end;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroFechamentoCaixa.edtNomeArquivoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
     SelectNext(ActiveControl, True, True);
     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnConfirmarAcao.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroFechamentoCaixa.VerificarDepositosPendentes;
var
  QryDepPend: TFDQuery;
  lCancelado: Boolean;
  sTexto: String;
begin
  lCancelado := False;

  sTexto := '';

  QryDepPend := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  repeat
    QryDepPend.Close;
    QryDepPend.SQL.Clear;
    QryDepPend.SQL.Text := 'SELECT LP.ID_LANCAMENTO_PARC ' +
                           '  FROM LANCAMENTO L ' +
                           ' INNER JOIN LANCAMENTO_PARC LP ' +
                           '    ON L.COD_LANCAMENTO = LP.COD_LANCAMENTO_FK ' +
                           '   AND L.ANO_LANCAMENTO = LP.ANO_LANCAMENTO_FK ' +
                           '  LEFT JOIN VINCULO_LANCDEPO V ' +
                           '    ON LP.ID_LANCAMENTO_PARC = V.ID_LANCAMENTO_PARC_FK ' +
                           ' WHERE L.TIPO_LANCAMENTO = '+ QuotedStr('R') +
                           '   AND L.FLG_CANCELADO = '+ QuotedStr('N') +
                           '   AND L.FLG_FORAFECHCAIXA = '+ QuotedStr('N') +
                           '   AND L.FLG_FLUTUANTE = '+ QuotedStr('S') +
                           '   AND V.ID_LANCAMENTO_PARC_FK IS NULL ' +
                           '   AND LP.ID_FORMAPAGAMENTO_FK = 5 ' +
                           '   AND LP.FLG_STATUS <> '+ QuotedStr('C') +
                           '   AND LP.DATA_LANCAMENTO_PARC BETWEEN :DATA_INI AND :DATA_FIM';
    QryDepPend.Params.ParamByName('DATA_INI').Value := dteDataFechamento.Date;
    QryDepPend.Params.ParamByName('DATA_FIM').Value := dteDataFechamento.Date;
    QryDepPend.Open;

    if QryDepPend.RecordCount > 0 then
    begin
      if QryDepPend.RecordCount = 1 then
        sTexto := 'H� um Lan�amento de Dep�sito pendente de V�nculo!' + #13#10 +
                  'Deseja realizar o V�nculo agora?'
      else
        sTexto := 'H� Lan�amentos de Dep�sitos pendentes de V�nculo!' + #13#10 +
                  'Deseja realizar os V�nculos agora?';

      if Application.MessageBox(PChar(sTexto),
                                'ATEN��O',
                                MB_YESNO) = ID_YES then
      begin
        Application.CreateForm(TdmBaixa, dmBaixa);
        dmGerencial.CriarForm(TFLancamentosVincularDepositos, FLancamentosVincularDepositos);
        FreeAndNil(dmBaixa);
      end
      else
        lCancelado := True;
    end;
  until lCancelado or (QryDepPend.RecordCount = 0);

  FreeAndNil(QryDepPend);
end;

procedure TFCadastroFechamentoCaixa.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  if lPodeCancelar then
  begin
    dmCaixa.cdsFechCXImg_F.Cancel;
    dmCaixa.cdsFechCX_F.Cancel;

    dmCaixa.cdsFechCXImg_F.Close;
    dmCaixa.cdsFechCX_F.Close;

    vgOperacao     := VAZIO;
    vgIdConsulta   := 0;
    vgOrigemFiltro := False;
  end
  else
    Action := caNone;
end;

procedure TFCadastroFechamentoCaixa.FormCreate(Sender: TObject);
begin
  inherited;

  sObsAtraso := '';
  lTabAuxDig := False;

  CalcularTotaisFechamento(vgDataUltAb, vgDataProxFech);
end;

procedure TFCadastroFechamentoCaixa.FormShow(Sender: TObject);
begin
  inherited;

  //IMAGENS
  dmCaixa.cdsFechCXImg_F.Close;
  dmCaixa.cdsFechCXImg_F.Params.ParamByName('ID_CAIXA_FECHAMENTO').Value := vgIdConsulta;
  dmCaixa.cdsFechCXImg_F.Open;

  iIdIncImg := dmCaixa.cdsFechCXImg_F.RecordCount;

  if vgOperacao = C then
    lblVisualizacao.Visible  := True
  else
    lblVisualizacao.Visible  := False;

  btnAbaDigitalizacao.Visible := (vgOperacao in [I, E]) or
                                 ((vgOperacao = C) and
                                  (vgIdConsulta > 0));

  btnRelatorioMovimentoCaixa.Visible := vgOperacao in [E, C];

  btnOk.Visible := (vgOperacao in [I, E]) or
                   ((vgOperacao = C) and
                    (vgIdConsulta > 0));

  if not btnOk.Visible then
  begin
    btnCancelar.Caption := '&Sair';
    btnCancelar.Spacing := 23;
  end;

  CalcularFechamentoDia;

  btnDadosPrincipais.Click;
end;

procedure TFCadastroFechamentoCaixa.GerarRelatorioMovimentoCaixa(
  GerarPDF: Boolean);
var
  Op: TOperacao;
  OrigC: Boolean;
begin
  Op    := vgOperacao;
  OrigC := vgOrigemCadastro;

  vgOperacao       := P;
  vgOrigemCadastro := False;

  try
    Application.CreateForm(TdmRelatorios, dmRelatorios);
    Application.CreateForm(TFFiltroRelatorioMovimentoCaixa, FFiltroRelatorioMovimentoCaixa);

    FFiltroRelatorioMovimentoCaixa.dteData.Date := dteDataFechamento.Date;
    FFiltroRelatorioMovimentoCaixa.RecuperarValorContaCorrente;
    FFiltroRelatorioMovimentoCaixa.chbExportarPDF.Checked   := GerarPDF;
    FFiltroRelatorioMovimentoCaixa.chbExportarExcel.Checked := False;
    FFiltroRelatorioMovimentoCaixa.chbExibirGrafico.Checked := False;
    FFiltroRelatorioMovimentoCaixa.btnVisualizar.Click;

    FFiltroRelatorioMovimentoCaixa.Hide;
  finally
    sDataUltFechamento := FormatDateTime('DD/MM/YYYY', dDataUltimoFechamento);
    FreeAndNil(FFiltroRelatorioMovimentoCaixa);
    FreeAndNil(dmRelatorios);
  end;

  vgOperacao       := Op;
  vgOrigemCadastro := OrigC;
end;

function TFCadastroFechamentoCaixa.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroFechamentoCaixa.GravarDadosGenerico(
  var Msg: String): Boolean;
var
  QryDest: TFDQuery;
  IdFech, IdImagem: Integer;
  lGerarRelatorio: Boolean;
  sDiferenca, sContagem, sCorpoEmail,
  sMsgEmail: String;
begin
  Result := True;
  Msg := '';

  IdFech   := 0;
  IdImagem := 0;

  lGerarRelatorio := False;

  sNomeRelatorio := '';
  sDiferenca     := '';
  sContagem      := '';
  sCorpoEmail    := '';
  sMsgEmail      := '';

  if vgOperacao = C then
    Exit;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    if dmCaixa.cdsFechCX_F.State in [dsInsert, dsEdit] then
    begin
      if vgOperacao = I then
      begin
        lGerarRelatorio := True;

        IdFech := BS.ProximoId('ID_CAIXA_FECHAMENTO', 'CAIXA_FECHAMENTO');
        dmCaixa.cdsFechCX_F.FieldByName('ID_CAIXA_FECHAMENTO').AsInteger := IdFech;

        if Trim(dmCaixa.cdsFechCX_F.FieldByName('HORA_FECHAMENTO').AsString) = '' then
          dmCaixa.cdsFechCX_F.FieldByName('HORA_FECHAMENTO').AsString    := TimeToStr(Now);

        if lEmAtraso then
        begin
          if Trim(mmObservacoes.Text) = '' then
            dmCaixa.cdsFechCX_F.FieldByName('OBS_CAIXA_FECHAMENTO').AsString := 'MOTIVO ATRASO: ' + sObsAtraso
          else
            dmCaixa.cdsFechCX_F.FieldByName('OBS_CAIXA_FECHAMENTO').AsString := dmCaixa.cdsFechCX_F.FieldByName('OBS_CAIXA_FECHAMENTO').AsString +
                                                                                #13#10 + 'MOTIVO ATRASO: ' + sObsAtraso;
        end
        else
          dmCaixa.cdsFechCX_F.FieldByName('OBS_CAIXA_FECHAMENTO').AsString := mmObservacoes.Text;
      end
      else
        IdFech := dmCaixa.cdsFechCX_F.FieldByName('ID_CAIXA_FECHAMENTO').AsInteger;

      dmCaixa.cdsFechCX_F.Post;
      dmCaixa.cdsFechCX_F.ApplyUpdates(0);
    end;

    { IMAGENS }
    if dmCaixa.cdsFechCXImg_F.RecordCount > 0 then
    begin
      IdImagem := BS.ProximoId('ID_CAIXA_FECHAMENTO_IMG', 'CAIXA_FECHAMENTO_IMG');

      dmCaixa.cdsFechCXImg_F.First;

      while not dmCaixa.cdsFechCXImg_F.Eof do
      begin
        dmCaixa.cdsFechCXImg_F.Edit;

        if dmCaixa.cdsFechCXImg_F.FieldByName('NOVO').AsBoolean then
        begin
          dmCaixa.cdsFechCXImg_F.FieldByName('ID_CAIXA_FECHAMENTO_IMG').AsInteger := IdImagem;
          Inc(IdImagem);
        end;

        if Copy(dmCaixa.cdsFechCXImg_F.FieldByName('NOME_FIXO').AsString, 1, 1) = '0' then
          dmCaixa.cdsFechCXImg_F.FieldByName('NOME_FIXO').AsString  := IntToStr(IdFech) +
                                                                        Copy(dmCaixa.cdsFechCXImg_F.FieldByName('NOME_FIXO').AsString,
                                                                             2,
                                                                             (Length(dmCaixa.cdsFechCXImg_F.FieldByName('NOME_FIXO').AsString) - 1));

        dmCaixa.cdsFechCXImg_F.FieldByName('ID_CAIXA_FECHAMENTO_FK').AsInteger  := IdFech;
        dmCaixa.cdsFechCXImg_F.Post;

        dmCaixa.cdsFechCXImg_F.Next;
      end;
    end;

    if dmCaixa.cdsFechCXImg_F.RecordCount > 0 then
    begin
      dmCaixa.cdsFechCXImg_F.First;

      while not dmCaixa.cdsFechCXImg_F.Eof do
      begin
        if not dmCaixa.cdsFechCXImg_F.FieldByName('NOME_V_ANTIGO').IsNull then
        begin
          if FileExists(vgConf_DiretorioImagens +
                        IntToStr(YearOf(dmCaixa.cdsFechCX_F.FieldByName('DATA_FECHAMENTO').AsDateTime)) +
                        '\' + dmCaixa.cdsFechCXImg_F.FieldByName('NOME_FIXO').AsString +
                        dmCaixa.cdsFechCXImg_F.FieldByName('NOME_V_ANTIGO').AsString + '.' +
                        dmCaixa.cdsFechCXImg_F.FieldByName('EXTENSAO').AsString) then
          begin
            DeleteFile(vgConf_DiretorioImagens +
                       IntToStr(YearOf(dmCaixa.cdsFechCX_F.FieldByName('DATA_FECHAMENTO').AsDateTime)) +
                       '\' + dmCaixa.cdsFechCXImg_F.FieldByName('NOME_FIXO').AsString +
                       dmCaixa.cdsFechCXImg_F.FieldByName('NOME_V_ANTIGO').AsString + '.' +
                       dmCaixa.cdsFechCXImg_F.FieldByName('EXTENSAO').AsString);
          end;
        end;

        if not dmCaixa.cdsFechCXImg_F.FieldByName('ORIGEM_ANTIGA').IsNull then
        begin
          if FileExists(dmCaixa.cdsFechCXImg_F.FieldByName('ORIGEM_ANTIGA').AsString) then
            DeleteFile(dmCaixa.cdsFechCXImg_F.FieldByName('ORIGEM_ANTIGA').AsString);
        end;

        dmCaixa.cdsFechCXImg_F.Next;
      end;

      dmCaixa.cdsFechCXImg_F.ApplyUpdates(0);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    Msg := 'Fechamento de Caixa gravado com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    lGerarRelatorio := False;
    Result := False;
    Msg := 'Erro na grava��o do Fechamento de Caixa.';
  end;

  if lGerarRelatorio then
  begin
    GerarRelatorioMovimentoCaixa(True);

    dmGerencial.Processando(True, 'Enviando o Fechamento de Caixa', True, 'Por favor, aguarde...');

    if cedDiferenca.Value = 0 then
      sDiferenca := '<font size=3 face=' + QuotedStr('verdana') + '>- Diferen&ccedil;a (Saldo - Fechamento Efetivo): ' + FloatToStrF(cedDiferenca.Value, ffCurrency, 10, 2) + '</font>'
    else if cedDiferenca.Value < 0 then
      sDiferenca := '<b><font size=3 face=' + QuotedStr('verdana') + ' color=' + QuotedStr('red') + '>- Diferen&ccedil;a (Saldo - Fechamento Efetivo): ' + FloatToStrF(cedDiferenca.Value, ffCurrency, 10, 2) + '</font></b>'
    else if cedDiferenca.Value > 0 then
      sDiferenca := '<b><font size=3 face=' + QuotedStr('verdana') + ' color=' + QuotedStr('green') + '>- Diferen&ccedil;a (Saldo - Fechamento Efetivo): ' + FloatToStrF(cedDiferenca.Value, ffCurrency, 10, 2) + '</font></b>';

    if cedVlrRealTContFinal.Value < 0 then
      sContagem := '<font size=3 face=' + QuotedStr('verdana') + ' color=' + QuotedStr('red') + '>- Contagem Total: ' + FloatToStrF(cedVlrRealTContFinal.Value, ffCurrency, 10, 2) + '</font>'
    else if cedDiferenca.Value >= 0 then
      sContagem := '<font size=3 face=' + QuotedStr('verdana') + ' color=' + QuotedStr('green') + '>- Contagem Total: ' + FloatToStrF(cedVlrRealTContFinal.Value, ffCurrency, 10, 2) + '</font>';

    sCorpoEmail := '<b><font size=3 face=' + QuotedStr('verdana') + '>RESUMO DO FECHAMENTO DE CAIXA - ' + FormatDateTime('DD/MM/YYYY', dteDataFechamento.Date) + '</font></b><br /><br />' +

                   '<font size=3 face=' + QuotedStr('verdana') + ' color=' + QuotedStr('green') + '>- Saldo do �ltimo Fechamento (' + sDataUltFechamento + '): ' + FloatToStrF(cedFechCxFechAnterior.Value, ffCurrency, 10, 2) + ' (+)<br />' +
                   '- Valor Total Recebido neste Dia: ' + FloatToStrF(cedFechCxTotalRecebido.Value, ffCurrency, 10, 2) + ' (+)</font><br />' +
                   '<font size=3 face=' + QuotedStr('verdana') + ' color=' + QuotedStr('red') + '>- Valor Total de Despesas: ' + FloatToStrF(cedFechCxTotalDespesas.Value, ffCurrency, 10, 2) + ' (-)</font><br />' +
                   '<font size=3 face=' + QuotedStr('verdana') + '>- Saldo Final deste Dia: ' + FloatToStrF(cedFechCxSaldoDia.Value, ffCurrency, 10, 2) + ' (=)</font><br /><br />' +

                   '<font size=3 face=' + QuotedStr('verdana') + '>- Contagem (Notas + Moedas): ' + FloatToStrF((cedVlrRealNotas.Value + cedVlrRealMoedas.Value), ffCurrency, 10, 2) + '<br />' +
                   '- Contagem (Saldo de Contas): ' + FloatToStrF(cedVlrRealSaldoConta.Value, ffCurrency, 10, 2) + '</font><br />' +
                   sContagem + '<br />' +
                   '<font size=3 face=' + QuotedStr('verdana') + ' color=' + QuotedStr('red') + '>- Dep&oacute;sitos sem Baixa: ' + FloatToStrF(cedFlutDeposSBaixaS.Value, ffCurrency, 10, 2) + ' (-)</font><br />' +
                   '<font size=3 face=' + QuotedStr('verdana') + '>- Fechamento Efetivo em Caixa: ' + FloatToStrF((cedVlrRealTContFinal.Value - cedFlutDeposSBaixaS.Value), ffCurrency, 10, 2) + ' (=)</font><br /><br />' +

                   sDiferenca + '<br /><br />' +

                   '<font size=3 face=' + QuotedStr('verdana') + '>Observa��es do Fechamento: <br />' + IfThen(Trim(mmObservacoes.Text) = '', '-', Trim(mmObservacoes.Text)) + '</font><br /><br />' +

                   '<font size=3 face=' + QuotedStr('verdana') + '>- Total Geral de Receitas Pendentes: ' + FloatToStrF(cedRecPendTotal.Value, ffCurrency, 10, 2) + '</font><br /><br />' +
                   '<b><font size=3 face=' + QuotedStr('verdana') + '>- Saldo Geral: ' + FloatToStrF(cedSaldoGeral.Value, ffCurrency, 10, 2) + '</font></b>';

    QryDest := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

    QryDest.Close;
    QryDest.SQL.Clear;
    QryDest.SQL.Text := 'SELECT * ' +
                        '  FROM EMAIL_DESTINATARIO ' +
                        ' WHERE FLG_ATIVO = ' + QuotedStr('S') +
                        '   AND FLG_REL_FECHAMENTO = ' + QuotedStr('S');
    QryDest.Open;

    QryDest.First;

    while not QryDest.Eof do
    begin
      sMsgEmail := '';

      dmGerencial.EnviarEmail('Fechamento de Caixa',
                              QryDest.FieldByName('EMAIL_DESTINATARIO').AsString,
                              sNomeRelatorio,
                              '',
                              sCorpoEmail,
                              '',
                              dmPrincipal.cdsRemetente,
                              sMsgEmail);

      QryDest.Next;
    end;

    FreeAndNil(QryDest);

    dmGerencial.Processando(False);
  end;
end;

procedure TFCadastroFechamentoCaixa.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta de Dados de Fechamento de Caixa',
                          vgIdConsulta, 'CAIXA_FECHAMENTO');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclus�o de Dados de Fechamento de Caixa',
                          vgIdConsulta, 'CAIXA_FECHAMENTO');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da edi��o desse Fechamento de Caixa:', '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de Fechamento de Caixa';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          vgIdConsulta, 'CAIXA_FECHAMENTO');
    end;
  end;
end;

procedure TFCadastroFechamentoCaixa.HabDesabBotoesCRUD(Inc, Ed, Exc: Boolean);
begin
  btnAcionarScanner.Enabled   := Inc;
  btnAbrirPastaImagem.Enabled := Inc;
  btnColarImagem.Enabled      := Inc;
  btnAssociarPDF.Enabled      := Inc;
  btnEditarImagem.Enabled     := Ed;
  btnExcluirImagem.Enabled    := Exc;
  edtNomeArquivo.Enabled      := not (Inc or Ed or Exc);
  btnConfirmarAcao.Enabled    := not (Inc or Ed or Exc);
  btnCancelarAcao.Enabled     := not (Inc or Ed or Exc);
end;

procedure TFCadastroFechamentoCaixa.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroFechamentoCaixa.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;

  case Aba of
    PRINC:
    begin
      ntbCadastro.PageIndex := 0;

      if cedVlrRealNotas.CanFocus then
        cedVlrRealNotas.SetFocus;
    end;
    DIG:
    begin
      ntbCadastro.PageIndex := 1;
    end;
  end;

  btnDadosPrincipais.Transparent  := not (Aba = PRINC);
  btnAbaAnalitico.Transparent     := not (Aba = PRINC);
  btnAbaDigitalizacao.Transparent := not (Aba = DIG);
end;

procedure TFCadastroFechamentoCaixa.mmObservacoesKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    CalcularFechamentoDia;
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    CalcularFechamentoDia;
    btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    btnCancelar.Click;
end;

function TFCadastroFechamentoCaixa.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if dteDataFechamento.Date = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente a DATA DO FECHAMENTO.';
    DadosMsgErro.Componente := dteDataFechamento;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if (Trim(medHoraFechamento.Text) = '') or
    (Trim(medHoraFechamento.Text) = ':  :') then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente a HORA DO FECHAMENTO.';
    DadosMsgErro.Componente := medHoraFechamento;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if (dteDataFechamento.Date > 0) and
    (Trim(medHoraFechamento.Text) <> '') and
    (Trim(medHoraFechamento.Text) <> ':  :') then
  begin
    if (VarToDateTime(FormatDateTime('DD-MM-YYYY', vgDataUltAb) +
                      ' ' +
                      TimeToStr(vgHoraUltAb)) >= VarToDateTime(FormatDateTime('DD-MM-YYYY', dteDataFechamento.Date) +
                                                               ' ' +
                                                               Trim(medHoraFechamento.Text))) then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') A HORA DO FECHAMENTO deve ser maior que a Hora da Abertura (' + TimeToStr(vgHoraUltAb) + ').';
      DadosMsgErro.Componente := medHoraFechamento;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;
  end;

  if cedVlrRealNotas.Value < 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o VALOR TOTAL DAS NOTAS.';
    DadosMsgErro.Componente := cedVlrRealNotas;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if cedVlrRealMoedas.Value < 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o VALOR TOTAL DAS MOEDAS.';
    DadosMsgErro.Componente := cedVlrRealMoedas;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroFechamentoCaixa.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

initialization
  TPicture.UnRegisterGraphicClass(TdxBMPImage);
  TPicture.UnRegisterGraphicClass(TdxPNGImage);
  TPicture.UnRegisterGraphicClass(TdxSmartImage);
  TPicture.RegisterFileFormat('BMP', 'Imagens BMP', TdxBMPImage);
  TPicture.RegisterFileFormat('PNG', 'Imagens PNG', TdxPNGImage);
  TPicture.RegisterFileFormat('JPG;*.JPEG;*.TIF;*.GIF;*.PNG;*.BMP', 'Todas as imagens', TdxSmartImage);

end.
