object FPrincipal: TFPrincipal
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'TOTAL MONITORAMENTO'
  ClientHeight = 557
  ClientWidth = 966
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  object pnlMenu: TPanel
    Left = 0
    Top = 0
    Width = 966
    Height = 92
    Align = alTop
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 0
    object pnlMenuAbas: TPanel
      Left = 0
      Top = 0
      Width = 966
      Height = 22
      Align = alTop
      BevelOuter = bvNone
      Color = 1006847
      ParentBackground = False
      TabOrder = 0
      object btnMenuAbaAtalhos: TJvTransparentButton
        Tag = 1001
        Left = 0
        Top = 0
        Width = 80
        Height = 22
        Cursor = crHandPoint
        Align = alLeft
        Caption = '&ATALHOS'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -13
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsNone
        ParentFont = False
        OnClick = btnMenuAbaAtalhosClick
      end
      object btnMenuAbaConfiguracoes: TJvTransparentButton
        Tag = 1006
        Left = 420
        Top = 0
        Width = 122
        Height = 22
        Cursor = crHandPoint
        Align = alLeft
        Caption = '&CONFIGURA'#199#213'ES'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -13
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsNone
        ParentFont = False
        OnClick = btnMenuAbaConfiguracoesClick
        ExplicitLeft = 400
      end
      object btnMenuAbaSuporte: TJvTransparentButton
        Tag = 1007
        Left = 542
        Top = 0
        Width = 80
        Height = 22
        Cursor = crHandPoint
        Align = alLeft
        Caption = '&SUPORTE'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -13
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsNone
        ParentFont = False
        OnClick = btnMenuAbaSuporteClick
        ExplicitLeft = 522
      end
      object btnMenuAbaOficios: TJvTransparentButton
        Tag = 1003
        Left = 176
        Top = 0
        Width = 74
        Height = 22
        Cursor = crHandPoint
        Align = alLeft
        Caption = '&OF'#205'CIOS'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -13
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsNone
        ParentFont = False
        OnClick = btnMenuAbaOficiosClick
      end
      object btnMenuAbaFinanceiro: TJvTransparentButton
        Tag = 1002
        Left = 80
        Top = 0
        Width = 96
        Height = 22
        Cursor = crHandPoint
        Align = alLeft
        Caption = '&FINANCEIRO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -13
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsNone
        ParentFont = False
        OnClick = btnMenuAbaFinanceiroClick
      end
      object btnMenuAbaAjuda: TJvTransparentButton
        Tag = 1005
        Left = 896
        Top = 0
        Width = 70
        Height = 22
        Cursor = crHandPoint
        Align = alRight
        Caption = 'A&JUDA'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -13
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsNone
        ParentFont = False
        Spacing = 7
        TextAlign = ttaRight
        OnClick = btnMenuAbaAjudaClick
        Images.ActiveImage = dmGerencial.Im16
        Images.ActiveIndex = 37
        Images.DisabledImage = dmGerencial.Im16
        Images.DisabledIndex = 37
        ExplicitLeft = 901
      end
      object btnAbrirCalculadora: TJvTransparentButton
        Tag = 1005
        Left = 874
        Top = 0
        Width = 22
        Height = 22
        Cursor = crHandPoint
        Hint = 'Clique para abrir a Calculadora do Windows'
        Align = alRight
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -13
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        Spacing = 7
        Transparent = False
        OnClick = btnAbrirCalculadoraClick
        Images.ActiveImage = dmGerencial.Im16
        Images.ActiveIndex = 23
        Images.DisabledImage = dmGerencial.Im16
        Images.DisabledIndex = 23
        ExplicitTop = 7
      end
      object btnMenuAbaFolhas: TJvTransparentButton
        Tag = 1004
        Left = 250
        Top = 0
        Width = 74
        Height = 22
        Cursor = crHandPoint
        Align = alLeft
        Caption = 'FO&LHAS'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -13
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsNone
        ParentFont = False
        OnClick = btnMenuAbaFolhasClick
      end
      object btnMenuAbaEtiquetas: TJvTransparentButton
        Tag = 1005
        Left = 324
        Top = 0
        Width = 96
        Height = 22
        Cursor = crHandPoint
        Align = alLeft
        Caption = '&ETIQUETAS'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -13
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsNone
        ParentFont = False
        OnClick = btnMenuAbaEtiquetasClick
      end
      object btnMenuAlerta: TJvTransparentButton
        Tag = 1008
        Left = 622
        Top = 0
        Width = 90
        Height = 22
        Cursor = crHandPoint
        Align = alLeft
        Caption = 'A&VISO'
        Color = 11578959
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -13
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsNone
        ParentFont = False
        Spacing = 8
        TextAlign = ttaRight
        Transparent = False
        OnClick = btnMenuAlertaClick
        Glyph.Data = {
          B60F0000424DB60F0000000000003600000028000000200000001F0000000100
          200000000000800F000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000002020223070707670A0A0A990C0C0CBB0E0E0ECB0D0D0DC80B0B
          0BB60A0A0A910606065A01010116000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000030606
          06570D0D0DC8111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FC0B0B0BB40404043F00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000030303340E0E0ECE1111
          11FF111111FF111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF111111FF0C0C0CB10202021C0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000707076A111111FA111111FF1111
          11FF111111FF111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF111111FF111111FF101010ED0505
          0545000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000009090980111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FB060606530000000000000000000000000000000000000000000000000000
          0000000000000000000007070767111111FF111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF111111F90404043C00000000000000000000000000000000000000000000
          0000000000000303032F111111F9111111FF111111FF111111FF111111FF1111
          11FF101B1EFF0C4459FF086587FF067FABFF0490C2FF0399CFFF0398CEFF048E
          C1FF067DA8FF086384FF0C4155FF10181BFF111111FF111111FF111111FF1111
          11FF111111FF0D0D0DE501010111000000000000000000000000000000000000
          0000000000010D0D0DC8111111FF111111FF111111FF111111FF101F24FF0676
          9EFF00AFEEFF00B3F4FF00B3F4FF01ACEAFF029CD4FF0396CBFF0398CEFF029F
          D7FF01AEEDFF00B3F4FF00B3F4FF01AEECFF077197FF101B1FFF111111FF1111
          11FF111111FF111111FF09090996000000000000000000000000000000000000
          00000505054C111111FF111111FF111111FF111111FF111111FF0587B6FF00B3
          F4FF0396CCFF0A4F69FF0F242CFF111212FF111111FF0F2932FF0F252EFF1111
          11FF111213FF0F262FFF0A536EFF029BD3FF00B3F4FF067EAAFF111111FF1111
          11FF111111FF111111FF111111FB020202200000000000000000000000000000
          00000B0B0BB7111111FF111111FF111111FF111111FF111111FF067AA4FF01A9
          E7FF0F1F25FF111111FF111111FF111111FF0F2932FF00B0F0FF01ADEBFF101F
          25FF111111FF111111FF111111FF0F2830FF00AFEEFF076D93FF111111FF1111
          11FF111111FF111111FF111111FF090909850000000000000000000000000101
          0111111111FD111111FF111111FF111111FF111111FF111111FF0E2933FF00B2
          F3FF01A6E2FF08698CFF0C3F51FF0F252DFF101E23FF0D3B4BFF0D3B4BFF101C
          20FF0F262FFF0C4155FF076D92FF01AAE8FF00AEEDFF0F2026FF111111FF1111
          11FF111111FF111111FF111111FF0F0F0FDB0000000000000000000000000505
          054E111111FF111111FF111111FF111111FF111111FF111111FF111111FF0584
          B2FF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B2F3FF00B2F3FF00B3
          F4FF00B3F4FF00B3F4FF00B3F4FF00B3F4FF06769EFF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF0202021C00000000000000000808
          0879111111FF111111FF111111FF111111FF111111FF111111FF111111FF0C45
          5AFF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3
          F4FF00B3F4FF00B3F4FF00B3F4FF00B3F4FF0D3746FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF0505054700000000000000000A0A
          0A91111111FF111111FF111111FF111111FF111111FF111111FF111111FF1116
          18FF01ACEAFF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3
          F4FF00B3F4FF00B3F4FF00B3F4FF02A3DDFF111213FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF0606066000000000000000000A0A
          0A99111111FF111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF0580ACFF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3
          F4FF00B3F4FF00B3F4FF00B3F4FF07759CFF111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF0707076900000000000000000909
          098E111111FF111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF0A5874FF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3
          F4FF00B3F4FF00B3F4FF00B3F4FF0B4C63FF111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF0606065C00000000000000000707
          076F111111FF111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF0D3240FF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3
          F4FF00B3F4FF00B3F4FF00B3F4FF0F2830FF111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF0404043D00000000000000000404
          043F111111FF111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF111516FF01ACEAFF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3
          F4FF00B3F4FF00B3F4FF02A2DCFF111212FF111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FE0101011100000000000000000000
          00070E0E0EF3111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF111111FF0679A3FF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3
          F4FF00B3F4FF00B3F4FF076C91FF111111FF111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FF0D0D0DC80000000000000000000000000000
          00000B0B0B9E111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF111111FF0F2831FF01ACEAFF00B3F4FF00B3F4FF00B3F4FF00B3F4FF00B3
          F4FF00B3F4FF01A5E1FF0F2026FF111111FF111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FF0707076D0000000000000000000000000000
          00000303032E111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF111111FF111111FF0D3746FF029FD7FF00B3F4FF00B3F4FF00B3F4FF00B3
          F4FF0399D0FF0E2F3BFF111111FF111111FF111111FF111111FF111111FF1111
          11FF111111FF111111FF101010EE0101010D0000000000000000000000000000
          0000000000000B0B0BA3111111FF111111FF111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111415FF095875FF00B3F4FF00B3F4FF0B4E
          66FF111213FF111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF111111FF111111FF07070770000000000000000000000000000000000000
          000000000000010101150E0E0EE6111111FF111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF111213FF0A4F68FF0B495FFF1112
          12FF111111FF111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF111111FF0C0C0CC500000004000000000000000000000000000000000000
          000000000000000000000404043A0F0F0FF7111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF0D0D0DE30202021B00000000000000000000000000000000000000000000
          0000000000000000000000000000050505470E0E0EF5111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF111111FF111111FF111111FF0D0D
          0DE3030303280000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000030303340F0F0FDF111111FF1111
          11FF111111FF111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF111111FF111111FF0C0C0CC40202
          021B000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000101010E0A0A0A8F1111
          11FB111111FF111111FF111111FF111111FF111111FF111111FF111111FF1111
          11FF111111FF111111FF111111FF111111FF101010F008080871000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000202
          021D090909860F0F0FE0111111FF111111FF111111FF111111FF111111FF1111
          11FF111111FF111111FF0E0E0ED0080808720101010E00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000102020225060606580808087A09090988090909850808
          0874050505500202021900000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
    end
    object pnlMenuBotoes: TPanel
      Left = 0
      Top = 22
      Width = 966
      Height = 70
      Align = alClient
      BevelOuter = bvNone
      Color = 10797567
      ParentBackground = False
      TabOrder = 1
      object ntbMenuBotoes: TNotebook
        Left = 0
        Top = 0
        Width = 793
        Height = 70
        Cursor = crArrow
        Align = alClient
        PageIndex = 6
        TabOrder = 0
        ExplicitLeft = -6
        ExplicitTop = -6
        object TPage
          Left = 0
          Top = 0
          Caption = 'Atalhos'
          object JvTransparentButton2: TJvTransparentButton
            Left = 210
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actCadClientes
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 18
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 18
            ExplicitLeft = 236
            ExplicitTop = -6
          end
          object JvTransparentButton1: TJvTransparentButton
            Left = 0
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actFiltroLancamento
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 3
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 11
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 11
            ExplicitLeft = 70
          end
          object btnAtalhoReceitas: TJvTransparentButton
            Left = 70
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Hint = 'Inclus'#227'o de Receitas'
            Align = alLeft
            Caption = 'Receitas'
            DropDownMenu = ppmReceita
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            OnClick = btnAtalhoReceitasClick
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 30
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 30
            ExplicitLeft = 140
          end
          object btnAtalhoDespesas: TJvTransparentButton
            Left = 140
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Hint = 'Inclus'#227'o de Despesas'
            Align = alLeft
            Caption = 'Despesas'
            DropDownMenu = ppmDespesa
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            OnClick = btnAtalhoDespesasClick
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 6
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 6
            ExplicitLeft = 210
          end
          object JvTransparentButton6: TJvTransparentButton
            Left = 280
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actCadFornecedores
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 17
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 17
          end
          object JvTransparentButton7: TJvTransparentButton
            Left = 350
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actCaixaMenu
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 55
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 4
          end
          object JvTransparentButton30: TJvTransparentButton
            Left = 490
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actOficEnviados
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            Visible = False
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 28
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 28
            ExplicitLeft = 560
          end
          object JvTransparentButton31: TJvTransparentButton
            Left = 420
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actOficRecebidos
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            Visible = False
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 29
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 29
            ExplicitLeft = 490
          end
          object btnLancPendentes: TJvTransparentButton
            Left = 560
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Hint = 'Clique para abrir lista de Lan'#231'amentos Pendentes'
            Align = alLeft
            Caption = 'Lan'#231'amentos Pendentes'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 3
            TextAlign = ttaBottom
            WordWrap = True
            OnClick = btnLancPendentesClick
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 1
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 1
            ExplicitLeft = 706
            ExplicitTop = 6
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'Financeiro'
          object JvTransparentButton3: TJvTransparentButton
            Left = 140
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actCadFornecedores
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 17
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 17
            ExplicitLeft = 134
            ExplicitTop = -6
          end
          object JvTransparentButton9: TJvTransparentButton
            Left = 350
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actFiltroLancamento
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 3
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 11
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 11
            ExplicitLeft = 210
          end
          object btnFinanceiroReceitas: TJvTransparentButton
            Left = 420
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Hint = 'Inclus'#227'o de Receitas'
            Align = alLeft
            Caption = 'Receitas'
            DropDownMenu = ppmReceita
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            OnClick = btnFinanceiroReceitasClick
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 30
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 30
            ExplicitLeft = 414
            ExplicitTop = -6
          end
          object JvTransparentButton11: TJvTransparentButton
            Left = 70
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actCadClientes
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 18
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 18
          end
          object btnFinanceiroDespesas: TJvTransparentButton
            Left = 490
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Hint = 'Inclus'#227'o de Despesas'
            Align = alLeft
            Caption = 'Despesas'
            DropDownMenu = ppmDespesa
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            OnClick = btnFinanceiroDespesasClick
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 6
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 6
            ExplicitLeft = 350
          end
          object JvTransparentButton13: TJvTransparentButton
            Left = 210
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actProdutosMenu
            Align = alLeft
            DropDownMenu = ppmProdutos
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 47
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 47
            ExplicitLeft = 276
            ExplicitTop = -6
          end
          object JvTransparentButton14: TJvTransparentButton
            Left = 280
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actCadServicos
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 13
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 13
            ExplicitLeft = 266
            ExplicitTop = 6
          end
          object JvTransparentButton17: TJvTransparentButton
            Left = 700
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actRelFinanceiroMenu
            Align = alLeft
            DropDownMenu = ppmRelatoriosFinanceiro
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 39
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 39
            ExplicitLeft = 706
            ExplicitTop = 6
          end
          object JvTransparentButton18: TJvTransparentButton
            Left = 0
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actColaboradoresMenu
            Align = alLeft
            DropDownMenu = ppmColaboradores
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 31
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 31
          end
          object JvTransparentButton20: TJvTransparentButton
            Left = 560
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actCaixaMenu
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 55
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 55
            ExplicitLeft = 594
            ExplicitTop = 6
          end
          object JvTransparentButton32: TJvTransparentButton
            Left = 630
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actCarneLeaoMenu
            Align = alLeft
            DropDownMenu = ppmCarneLeao
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 62
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 62
            ExplicitLeft = 770
            ExplicitTop = 6
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'Oficios'
          object JvTransparentButton19: TJvTransparentButton
            Left = 140
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actOficEnviados
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 28
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 28
            ExplicitLeft = 168
            ExplicitTop = 16
            ExplicitHeight = 40
          end
          object JvTransparentButton21: TJvTransparentButton
            Left = 70
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actOficRecebidos
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 29
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 29
            ExplicitLeft = 256
            ExplicitTop = 40
            ExplicitHeight = 40
          end
          object JvTransparentButton33: TJvTransparentButton
            Left = 0
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actFiltroOficio
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 11
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 11
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'Folhas'
          object JvTransparentButton36: TJvTransparentButton
            Left = 0
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actFiltroLoteFolhasMenu
            Align = alLeft
            DropDownMenu = ppmFiltroLoteFolhas
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 3
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 11
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 11
            ExplicitLeft = 4
            ExplicitTop = 6
          end
          object JvTransparentButton39: TJvTransparentButton
            Left = 350
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actRelFolhasMenu
            Align = alLeft
            DropDownMenu = ppmRelatoriosFolhas
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 39
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 39
            ExplicitLeft = 344
            ExplicitTop = 6
          end
          object JvTransparentButton41: TJvTransparentButton
            Left = 70
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actCadLoteFolhaSeguranca
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 3
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 64
            Images.DisabledImage = dmGerencial.Im32Dis
            ExplicitLeft = 64
            ExplicitTop = 6
          end
          object JvTransparentButton43: TJvTransparentButton
            Left = 210
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actFiltroFolhaSeguranca
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 3
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 66
            Images.DisabledImage = dmGerencial.Im32Dis
            ExplicitLeft = 204
            ExplicitTop = -6
          end
          object JvTransparentButton44: TJvTransparentButton
            Left = 140
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actCadLoteFolhaRCPN
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 3
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 68
            Images.GrayIndex = 68
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 68
            Images.DownIndex = 68
            Images.HotIndex = 68
            ExplicitLeft = 134
            ExplicitTop = -6
          end
          object JvTransparentButton46: TJvTransparentButton
            Left = 280
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actFiltroFolhaRCPN
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 67
            Images.GrayIndex = 66
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 66
            Images.DownIndex = 66
            Images.HotIndex = 66
            ExplicitLeft = 288
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'Etiquetas'
          object JvTransparentButton38: TJvTransparentButton
            Left = 0
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actFiltroLoteEtiquetas
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 3
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 11
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 11
            ExplicitLeft = 8
          end
          object JvTransparentButton40: TJvTransparentButton
            Left = 210
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actRelEtiquetasMenu
            Align = alLeft
            DropDownMenu = ppmRelatoriosEtiquetas
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 39
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 39
            ExplicitLeft = 280
            ExplicitTop = -6
          end
          object JvTransparentButton35: TJvTransparentButton
            Left = 140
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actCadEtiqueta
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 47
            Images.DisabledImage = dmGerencial.Im32Dis
            ExplicitLeft = 204
            ExplicitTop = -6
          end
          object JvTransparentButton42: TJvTransparentButton
            Left = 70
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actCadLoteEtiqueta
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 3
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 65
            Images.DisabledImage = dmGerencial.Im32Dis
            ExplicitLeft = 78
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'Configuracoes'
          object JvTransparentButton22: TJvTransparentButton
            Left = 280
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actConfigCadPermissoes
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 35
            Images.GrayIndex = 35
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 35
            Images.DownIndex = 35
            Images.HotIndex = 35
            ExplicitLeft = 210
          end
          object JvTransparentButton23: TJvTransparentButton
            Left = 210
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actConfigCadUsuarios
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 34
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 34
            ExplicitLeft = 140
          end
          object JvTransparentButton24: TJvTransparentButton
            Left = 70
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actConfigSistema
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 40
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 40
          end
          object JvTransparentButton25: TJvTransparentButton
            Left = 0
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actConfigFiltroLog
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 21
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 21
            ExplicitTop = -6
          end
          object JvTransparentButton34: TJvTransparentButton
            Left = 140
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actConfigAtribuicoes
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 59
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 11
            ExplicitLeft = 204
            ExplicitTop = 6
          end
          object JvTransparentButton15: TJvTransparentButton
            Left = 560
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actNiveisLancamentoMenu
            Align = alLeft
            DropDownMenu = ppmMenuNiveisLanc
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 3
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 19
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 19
            ExplicitLeft = 554
            ExplicitTop = 6
          end
          object JvTransparentButton8: TJvTransparentButton
            Left = 350
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actCadFeriado
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 54
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 11
            ExplicitLeft = 504
            ExplicitTop = 6
          end
          object btnFormaPagamento: TJvTransparentButton
            Left = 490
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actCadFormaPagto
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 3
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 33
            Images.DisabledImage = dmGerencial.Im32Dis
            ExplicitLeft = 484
            ExplicitTop = -6
          end
          object JvTransparentButton47: TJvTransparentButton
            Left = 420
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actCadCargo
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 31
            Images.DisabledImage = dmGerencial.Im32Dis
            ExplicitLeft = 504
            ExplicitTop = 6
          end
          object JvTransparentButton16: TJvTransparentButton
            Left = 630
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actConfigCadEmails
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 69
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 69
            ExplicitLeft = 717
            ExplicitTop = 6
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'Suporte'
          object JvTransparentButton26: TJvTransparentButton
            Left = 140
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actSupArquivoIni
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 0
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 0
          end
          object JvTransparentButton28: TJvTransparentButton
            Left = 70
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actSupBackup
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            Visible = False
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 6
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 6
          end
          object JvTransparentButton29: TJvTransparentButton
            Left = 280
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Hint = 'Criptografar e Descriptogravar Strings'
            Align = alLeft
            Caption = 'Aplicativo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            Visible = False
            WordWrap = True
            OnClick = JvTransparentButton29Click
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 9
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 9
            ExplicitLeft = 356
            ExplicitTop = 6
          end
          object JvTransparentButton37: TJvTransparentButton
            Left = 210
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actSupSQL
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 49
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 49
            ExplicitLeft = 218
          end
          object JvTransparentButton27: TJvTransparentButton
            Left = 0
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actSupCriptografia
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 7
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 9
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 9
            ExplicitLeft = 8
          end
          object btnAtualizarBancosDados: TJvTransparentButton
            Left = 350
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actSupAtualizarBancos
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 3
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 4
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 4
            ExplicitLeft = 358
          end
          object JvTransparentButton4: TJvTransparentButton
            Left = 420
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actSupDownloadBancoM
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 3
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 50
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 50
            ExplicitLeft = 428
          end
          object JvTransparentButton5: TJvTransparentButton
            Left = 490
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actSupAtualizarRelatorios
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 3
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 2
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 2
            ExplicitLeft = 496
            ExplicitTop = -6
          end
          object JvTransparentButton10: TJvTransparentButton
            Left = 560
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actSupBaixarHomologacao
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 3
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 6
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 6
            ExplicitLeft = 568
          end
          object JvTransparentButton12: TJvTransparentButton
            Left = 630
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actSupCorrigirImportacoesDuplicadas
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 3
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 13
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 2
            ExplicitLeft = 706
            ExplicitTop = 6
          end
          object JvTransparentButton45: TJvTransparentButton
            Left = 700
            Top = 0
            Width = 70
            Height = 70
            Cursor = crHandPoint
            Action = actSupVoltarVersaoSistema
            Align = alLeft
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            FrameStyle = fsNone
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Spacing = 3
            TextAlign = ttaBottom
            WordWrap = True
            Images.ActiveImage = dmGerencial.Im32
            Images.ActiveIndex = 4
            Images.DisabledImage = dmGerencial.Im32Dis
            Images.DisabledIndex = 4
            ExplicitLeft = 718
            ExplicitTop = -6
          end
        end
      end
      object pnlSistema: TPanel
        Left = 793
        Top = 0
        Width = 173
        Height = 70
        Align = alRight
        BevelOuter = bvNone
        Color = 10797567
        ParentBackground = False
        TabOrder = 1
        object lblVersaoSistema: TLabel
          Left = 0
          Top = 0
          Width = 173
          Height = 23
          Align = alTop
          Alignment = taRightJustify
          AutoSize = False
          Caption = #218'ltima Versao: '
          Color = 10797567
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 5511957
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Layout = tlBottom
          ExplicitWidth = 165
        end
        object lblUsuarioLogado: TLabel
          Left = 0
          Top = 23
          Width = 173
          Height = 23
          Align = alTop
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'Usu'#225'rio: '
          Color = 10797567
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 5511957
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          ExplicitTop = 22
          ExplicitWidth = 165
        end
        object lblCaixaVigente: TLabel
          Left = 0
          Top = 47
          Width = 173
          Height = 23
          Align = alBottom
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'Caixa Aberto: '
          Color = 10797567
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 5511957
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          ExplicitTop = 48
          ExplicitWidth = 165
        end
      end
    end
  end
  object pnVersao: TPanel
    Left = 0
    Top = 92
    Width = 966
    Height = 465
    Cursor = crHandPoint
    Align = alClient
    BevelOuter = bvNone
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 966
      Height = 391
      Align = alClient
      BevelOuter = bvNone
      Color = clWhite
      ParentBackground = False
      TabOrder = 0
      OnCanResize = Panel3CanResize
      object Image1: TImage
        Left = 0
        Top = 0
        Width = 966
        Height = 391
        Align = alClient
        Center = True
        Picture.Data = {
          0B546478504E47496D61676589504E470D0A1A0A0000000D49484452000009B0
          000009B0080600000041629F080000000467414D410000B18E7CFB5193000000
          206348524D0000870F00008C0F0000FD520000814000007D790000E98B00003C
          E5000019CC733C857700000A396943435050686F746F73686F70204943432070
          726F66696C65000048C79D96775454D71687CFBD777AA1CD30025286DEBBC000
          D27B935E456198196028030E3334B121A2021145449A224850C480D150245644
          B1101454B007240828311845542C6F46D68BAEACBCF7F2F2FBE3AC6FEDB3F7B9
          FBECBDCF5A170092A72F9797064B0190CA13F0833C9CE911915174EC0080011E
          608029004C5646BA5FB07B0810C9CBCD859E2172025F0401F07A58BC0270D3D0
          33804E07FF9FA459E97C81E89800119BB339192C11178838254B902EB6CF8A98
          1A972C66182566BE284111CB893961910D3EFB2CB2A398D9A93CB688C539A7B3
          53D962EE15F1B64C2147C488AF880B33B99C2C11DF12B1468A30952BE237E2D8
          540E33030014496C1770588922361131891F12E422E2E500E048095F71DC572C
          E0640BC49772494BCFE173131205741D962EDDD4DA9A41F7E464A5700402C300
          262B99C967D35DD252D399BC1C0016EFFC5932E2DAD24545B634B5B6B4343433
          32FDAA50FF75F36F4ADCDB457A19F8B96710ADFF8BEDAFFCD21A0060CC896AB3
          F38B2DAE0A80CE2D00C8DDFB62D3380080A4A86F1DD7BFBA0F4D3C2F890241BA
          8DB1715656961197C3321217F40FFD4F87BFA1AFBE67243EEE8FF2D05D39F14C
          618A802EAE1B2B2D254DC8A767A433591CBAE19F87F81F07FE751E06419C780E
          9FC313458489A68CCB4B10B59BC7E60AB8693C3A97F79F9AF80FC3FEA4C5B916
          89D2F81150638C80D4752A407EED07280A1120D1FBC55DFFA36FBEF830207E79
          E12A938B73FFEF37FD67C1A5E225839BF039CE252884CE12F23317F7C4CF12A0
          010148022A9007CA401DE800436006AC802D70046EC01BF88310100956031648
          04A9800FB2401ED8040A4131D809F6806A50071A41336805C741273805CE834B
          E01AB8016E83FB60144C80676016BC060B10046121324481E421154813D287CC
          2006640FB941BE50101409C54209100F124279D066A8182A83AAA17AA819FA1E
          3A099D87AE4083D05D680C9A867E87DEC1084C82A9B012AC051BC30CD809F681
          43E0557002BC06CE850BE01D7025DC001F853BE0F3F035F8363C0A3F83E71080
          10111AA28A18220CC405F147A29078848FAC478A900AA4016945BA913EE42632
          8ACC206F51181405454719A26C519EA850140BB506B51E5582AA461D4675A07A
          51375163A859D4473419AD88D647DBA0BDD011E8047416BA105D816E42B7A32F
          A26FA327D0AF31180C0DA38DB1C2786222314998B59812CC3E4C1BE61C661033
          8E99C362B1F2587DAC1DD61FCBC40AB085D82AEC51EC59EC107602FB0647C4A9
          E0CC70EEB8281C0F978FABC01DC19DC10DE126710B7829BC26DE06EF8F67E373
          F0A5F8467C37FE3A7E02BF4090266813EC08218424C2264225A1957091F080F0
          924824AA11AD8981442E7123B192788C789938467C4B9221E9915C48D1242169
          07E910E91CE92EE925994CD6223B92A3C802F20E7233F902F911F98D0445C248
          C24B822DB141A246A2436248E2B9245E5253D24972B564AE6485E409C9EB9233
          5278292D291729A6D47AA91AA99352235273D2146953697FE954E912E923D257
          A4A764B0325A326E326C99029983321764C62908459DE242615136531A291729
          13540C559BEA454DA21653BFA30E506765656497C986C966CBD6C89E961DA521
          342D9A172D85564A3B4E1BA6BD5BA2B4C4690967C9F625AD4B8696CCCB2D9573
          94E3C815C9B5C9DD967B274F9777934F96DF25DF29FF5001A5A0A710A890A5B0
          5FE1A2C2CC52EA52DBA5ACA5454B8F2FBDA7082BEA290629AE553CA8D8AF38A7
          A4ACE4A194AE54A57441694699A6ECA89CA45CAE7C46795A85A262AFC2552957
          39ABF2942E4B77A2A7D02BE9BDF4595545554F55A16ABDEA80EA829AB65AA85A
          BE5A9BDA4375823A433D5EBD5CBD477D564345C34F234FA345E39E265E93A199
          A8B957B34F735E4B5B2B5C6BAB56A7D694B69CB69776AE768BF6031DB28E83CE
          1A9D069D5BBA185D866EB2EE3EDD1B7AB09E855EA25E8DDE757D58DF529FABBF
          4F7FD0006D606DC0336830183124193A19661AB6188E19D18C7C8DF28D3A8D9E
          1B6B184719EF32EE33FE6862619262D26872DF54C6D4DB34DFB4DBF477333D33
          96598DD92D73B2B9BBF906F32EF317CBF4977196ED5F76C78262E167B1D5A2C7
          E283A59525DFB2D572DA4AC32AD6AAD66A84416504304A1897ADD1D6CED61BAC
          4F59BFB5B1B411D81CB7F9CDD6D036D9F688EDD472EDE59CE58DCBC7EDD4EC98
          76F576A3F674FB58FB03F6A30EAA0E4C870687C78EEA8E6CC726C749275DA724
          A7A34ECF9D4D9CF9CEEDCEF32E362EEB5CCEB922AE1EAE45AE036E326EA16ED5
          6E8FDCD5DC13DC5BDC673D2C3CD67A9CF3447BFA78EEF21CF152F26279357BCD
          7A5B79AFF3EEF521F904FB54FB3CF6D5F3E5FB76FBC17EDE7EBBFD1EACD05CC1
          5BD1E90FFCBDFC77FB3F0CD00E5813F06320263020B026F0499069505E505F30
          253826F848F0EB10E790D290FBA13AA1C2D09E30C9B0E8B0E6B0F970D7F0B2F0
          D108E3887511D7221522B9915D51D8A8B0A8A6A8B9956E2BF7AC9C88B6882E8C
          1E5EA5BD2A7BD595D50AAB53569F8E918C61C69C8845C786C71E897DCFF46736
          30E7E2BCE26AE366592EACBDAC676C4776397B9A63C729E34CC6DBC597C54F25
          D825EC4E984E7448AC489CE1BA70ABB92F923C93EA92E693FD930F257F4A094F
          694BC5A5C6A69EE4C9F09279BD69CA69D96983E9FAE985E9A36B6CD6EC5933CB
          F7E137654019AB32BA0454D1CF54BF5047B8453896699F5993F9262B2CEB44B6
          74362FBB3F472F677BCE64AE7BEEB76B516B596B7BF254F336E58DAD735A57BF
          1E5A1FB7BE6783FA86820D131B3D361EDE44D894BCE9A77C93FCB2FC579BC337
          771728156C2C18DFE2B1A5A550A2905F38B2D5766BDD36D436EEB681EDE6DBAB
          B67F2C62175D2D3629AE287E5FC22AB9FA8DE93795DF7CDA11BF63A0D4B274FF
          4ECC4EDECEE15D0EBB0E974997E5968DEFF6DBDD514E2F2F2A7FB52766CF958A
          6515757B097B857B472B7D2BBBAA34AA7656BDAF4EACBE5DE35CD356AB58BBBD
          767E1F7BDFD07EC7FDAD754A75C575EF0E700FDCA9F7A8EF68D06AA838883998
          79F049635863DFB78C6F9B9B149A8A9B3E1CE21D1A3D1C74B8B7D9AAB9F988E2
          91D216B845D8327D34FAE88DEF5CBFEB6A356CAD6FA3B5151F03C784C79E7E1F
          FBFDF0719FE33D2718275A7FD0FCA1B69DD25ED40175E474CC7626768E764576
          0D9EF43ED9D36DDBDDFEA3D18F874EA99EAA392D7BBAF40CE14CC1994F6773CF
          CE9D4B3F37733EE1FC784F4CCFFD0B11176EF506F60E5CF4B978F992FBA50B7D
          4E7D672FDB5D3E75C5E6CAC9AB8CAB9DD72CAF75F45BF4B7FF64F153FB80E540
          C775ABEB5D37AC6F740F2E1F3C33E43074FEA6EBCD4BB7BC6E5DBBBDE2F6E070
          E8F09D91E891D13BEC3B537753EEBEB897796FE1FEC607E807450FA51E563C52
          7CD4F0B3EECF6DA396A3A7C75CC7FA1F073FBE3FCE1A7FF64BC62FEF270A9E90
          9F544CAA4C364F994D9D9A769FBEF174E5D38967E9CF16660A7F95FEB5F6B9CE
          F31F7E73FCAD7F366276E205FFC5A7DF4B5ECABF3CF46AD9AB9EB980B947AF53
          5F2FCC17BD917F73F82DE36DDFBBF077930B59EFB1EF2B3FE87EE8FEE8F3F1C1
          A7D44F9FFE050398F3FCBAC4E8D3000000097048597300002E2300002E230178
          A53F760000F54149444154785EECDD79B0A5757DE7F1A9A994954AF9478A9AB2
          02212621C698C4B2D418E210931835EA38C6988410A51CD3A2A2082A618821C6
          6424C6308C4E880B2EC1142A2E2114C330D79B3BD79E800A228B2DFBE2C2DADD
          74D30D34BDF7BDD0E799FE9D6E100F1FA197FBBDF72CAF3F5E65D55BA0FBDE73
          CEF37CAB7EDF3ACFBFEBBA0E000000000000000000165D8C0000000000000000
          00502D46000000000000000000A81623000000000000000000548B1100000000
          0000000000AAC508000000000000000000D562040000000000000000806A3102
          000000000000000040B518010000000000000000A05A8C000000000000000000
          502D46000000000000000000A81623000000000000000000548B110000000000
          00000000AAC508000000000000000000D562040000000000000000806A310200
          0000000000000040B518010000000000000000A05A8C00000000000000000050
          2D46000000000000000000A81623000000000000000000548B11000000000000
          000000AAC508000000000000000000D562040000000000000000806A31020000
          00000000000040B518010000000000000000A05A8C000000000000000000502D
          46000000000000000000A81623000000000000000000548B1100000000000000
          0000AAC508000000000000000000D562040000000000000000806A3102000000
          000000000040B518010000000000000000A05A8C000000000000000000502D46
          000000000000000000A81623000000000000000000548B110000000000000000
          00AAC508000000000000000000D562040000000000000000806A310200000000
          0000000040B518010000000000000000A05A8C000000000000000000502D4600
          0000000000000000A81623000000000000000000548B11000000000000000000
          AAC508000000000000000000D562040000000000000000806A31020000000000
          00000040B518010000000000000000A05A8C000000000000000000502D460000
          00000000000000A81623000000000000000000548B11000000000000000000AA
          C508000000000000000000D562040000000000000000806A3102000000000000
          000040B518010000000000000000A05A8C000000000000000000502D46000000
          000000000000A81623000000000000000000548B11000000000000000000AAC5
          08000000000000000000D562040000000000000000806A310200000000000000
          0040B518010000000000000000A05A8C000000000000000000502D4600000000
          0000000000A81623000000000000000000548B11000000000000000000AAC508
          000000000000000000D562040000000000000000806A31020000000000000000
          40B518010000000000000000A05A8C000000000000000000502D460000000000
          00000000A81623000000000000000000548B11000000000000000000AAC50800
          0000000000000000D562040000000000000000806A3102000000000000000040
          B518010000000000000000A05A8C000000000000000000502D46000000000000
          000000A81623000000000000000000548B11000000000000000000AAC5080000
          00000000000000D562040000000000000000806A3102000000000000000040B5
          18010000000000000000A05A8C000000000000000000502D4600000000000000
          0000A81623000000000000000000548B11000000000000000000AAC508000000
          000000000000D562040000000000000000806A3102000000000000000040B518
          010000000000000000A05A8C000000000000000000502D460000000000000000
          00A81623000000000000000000548B11000000000000000000AAC50800000000
          0000000000D562040000000000000000806A3102000000000000000040B51801
          0000000000000000A05A8C000000000000000000502D46000000000000000000
          A81623000000000000000000548B11000000000000000000AAC5080000000000
          00000000D562040000000000000000806A3102000000000000000040B5180100
          00000000000000A05A8C000000000000000000502D46000000000000000000A8
          1623000000000000000000548B11000000000000000000AAC508000000000000
          000000D562040000000000000000806A3102000000000000000040B518010000
          000000000000A05A8C000000000000000000502D46000000000000000000A816
          23000000000000000000548B11000000000000000000AAC50800000000000000
          0000D562040000000000000000806A3102000000000000000040B51801000000
          0000000000A05A8C000000000000000000502D46000000000000000000A81623
          000000000000000000548B11000000000000000000AAC5080000000000000000
          00D562040000000000000000806A3102000000000000000040B5180100000000
          00000000A05A8C000000000000000000502D46000000000000000000A8162300
          0000000000000000548B11000000000000000000AAC508000000000000000000
          D562040000000000000000806A3102000000000000000040B518010000000000
          000000A05A8C000000000000000000502D46000000000000000000A816230000
          00000000000000548B11000000000000000000AAC508000000000000000000D5
          62040000000000000000806A3102000000000000000040B51801000000000000
          0000A05A8C000000000000000000502D46000000000000000000A81623000000
          000000000000548B11000000000000000000AAC508000000000000000000D562
          040000000000000000806A3102000000000000000040B5180100000000000000
          00A05A8C000000000000000000502D46000000000000000000A8162300000000
          0000000000548B11000000000000000000AAC508000000000000000000D56204
          0000000000000000806A3102000000000000000040B518010000000000000000
          A05A8C000000000000000000502D46000000000000000000A816230000000000
          00000000548B11000000000000000000AAC508000000000000000000D5620400
          00000000000000806A3102000000000000000040B518010000000000000000A0
          5A8C000000000000000000502D46000000000000000000A81623000000000000
          000000548B11000000000000000000AAC508000000000000000000D562040000
          000000000000806A3102000000000000000040B518010000000000000000A05A
          8C000000000000000000502D46000000000000000000A8162300000000000000
          0000548B11000000000000000000AAC508000000000000000000D56204000000
          0000000000806A3102000000000000000040B518010000000000000000A05A8C
          000000000000000000502D46000000000000000000A816230000000000000000
          00548B11000000000000000000AAC508000000000000000000D5620400000000
          00000000806A3102000000000000000040B518010000000000000000A05A8C00
          0000000000000000502D46000000000000000000A81623000000000000000000
          548B11000000000000000000AAC508000000000000000000D562040000000000
          000000806A3102000000000000000040B518010000000000000000A05A8C0000
          00000000000000502D46000000000000000000A8162300000000000000000054
          8B11000000000000000000AAC508000000000000000000D56204000000000000
          0000806A3102000000000000000040B518010000000000000000A05A8C000000
          000000000000502D46000000000000000000A81623000000000000000000548B
          11000000000000000000AAC508000000000000000000D5620400000000000000
          00806A3102000000000000000040B518010000000000000000A05A8C00000000
          0000000000502D46000000000000000000A81623000000000000000000548B11
          000000000000000000AAC508000000000000000000D562040000000000000000
          806A3102000000000000000040B518010000000000000000A05A8C0000000000
          00000000502D46000000000000000000A81623000000000000000000548B1100
          0000000000000000AAC508000000000000000000D56204000000000000000080
          6A3102000000000000000040B518010000000000000000A05A8C000000000000
          000000502D46000000000000000000A81623000000000000000000548B110000
          00000000000000AAC508000000000000000000D562040000000000000000806A
          3102000000000000000040B518010000000000000000A05A8C00000000000000
          0000502D46000000000000000000A81623000000000000000000548B11000000
          000000000000AAC508000000000000000000D562040000000000000000806A31
          02000000000000000040B518010000000000000000A05A8C0000000000000000
          00502D46000000000000000000A81623000000000000000000548B1100000000
          0000000000AAC508000000000000000000D562040000000000000000806A3102
          000000000000000040B518010000000000000000A05A8C000000000000000000
          502D46000000000000000000A81623000000000000000000548B110000000000
          00000000AAC508000000000000000000D562040000000000000000806A310200
          0000000000000040B518010000000000000000A05A8C00000000000000000050
          2D46000000000000000000A81623000000000000000000548B11000000000000
          000000AAC508000000000000000000D562040000000000000000806A31020000
          00000000000040B518010000000000000000A05A8C000000000000000000502D
          46000000000000000000A81623000000000000000000548B1100000000000000
          0000AAC508000000000000000000D562040000000000000000806A3102000000
          000000000040B518010000000000000000A05A8C000000000000000000502D46
          000000000000000000A81623000000000000000000548B110000000000000000
          00AAC508000000000000000000D562040000000000000000806A310200000000
          0000000040B518010000000000000000A05A8C000000000000000000502D4600
          0000000000000000A81623000000000000000000548B11000000000000000000
          AAC508000000000000000000D562040000000000000000806A31020000000000
          00000040B518010000000000000000A05A8C000000000000000000502D460000
          00000000000000A81623000000000000000000548B11000000000000000000AA
          C508000000000000000000D562040000000000000000806A3102000000000000
          000040B518010000000000000000A05A8C000000000000000000502D46000000
          000000000000A81623000000000000000000548B11000000000000000000AAC5
          08000000000000000000D562040000000000000000806A310200000000000000
          0040B518010000000000000000A05A8C000000000000000000502D4600000000
          0000000000A81623000000000000000000548B11000000000000000000AAC508
          000000000000000000D562040000000000000000806A31020000000000000000
          40B518010000000000000000A05A8C000000000000000000502D460000000000
          00000000A81623000000000000000000548B11000000000000000000AAC50800
          0000000000000000D562040000000000000000806A3102000000000000000040
          B518010000000000000000A05A8C000000000000000000502D46000000000000
          000000A81623000000000000000000548B11000000000000000000AAC5080000
          00000000000000D562040000000000000000806A3102000000000000000040B5
          18010000000000000000A05A8C000000000000000000502D4600000000000000
          0000A81623000000000000000000548B11000000000000000000AAC508000000
          000000000000D562040000000000000000806A3102000000000000000040B518
          010000000000000000A05A8C000000000000000000502D460000000000000000
          00A81623000000000000000000548B11000000000000000000AAC50800000000
          0000000000D562040000000000000000806A3102000000000000000040B51801
          0000000000000000A05A8C000000000000000000502D46000000000000000000
          A81623000000000000000000548B11000000000000000000AAC5080000000000
          00000000D562040000000000000000806A3102000000000000000040B5180100
          00000000000000A05A8C000000000000000000502D46000000000000000000A8
          1623000000000000000000548B11000000000000000000AAC508000000000000
          000000D562040000000000000000806A3102000000000000000040B518010000
          000000000000A05A8C000000000000000000502D46000000000000000000A816
          23000000000000000000548B11000000000000000000AAC50800000000000000
          0000D562040000000000000000806A3102000000000000000040B51801000000
          0000000000A05A8C000000000000000000502D46000000000000000000A81623
          000000000000000000548B11000000000000000000AAC5080000000000000000
          00D562040000000000000000806A3102000000000000000040B5180100000000
          00000000A05A8C000000000000000000502D46000000000000000000A8162300
          0000000000000000548B11000000000000000000AAC508000000000000000000
          D562040000000000000000806A3102000000000000000040B518010000000000
          000000A05A8C000000000000000000502D46000000000000000000A816230000
          00000000000000548B11000000000000000000AAC508000000000000000000D5
          62040000000000000000806A3102000000000000000040B51801000000000000
          0000A05A8C000000000000000000502D46000000000000000000A81623000000
          000000000000548B11000000000000000000AAC508000000000000000000D562
          040000000000000000806A3102000000000000000040B5180100000000000000
          00A05A8C000000000000000000502D46000000000000000000A8162300000000
          0000000000548B11000000000000000000AAC508000000000000000000D56204
          0000000000000000806A3102000000000000000040B518010000000000000000
          A05A8C000000000000000000502D46000000000000000000A816230000000000
          00000000548B11000000000000000000AAC508000000000000000000D5620400
          00000000000000806A3102000000000000000040B518010000000000000000A0
          5A8C000000000000000000502D46000000000000000000A81623000000000000
          000000548B11000000000000000000AAC508000000000000000000D562040000
          000000000000806A3102000000000000000040B518010000000000000000A05A
          8C000000000000000000502D46000000000000000000A8162300000000000000
          0000548B11000000000000000000AAC508000000000000000000D56204000000
          0000000000806A3102000000000000000040B518010000000000000000A05A8C
          000000000000000000502D46000000000000000000A816230000000000000000
          00548B11000000000000000000AAC508000000000000000000D5620400000000
          00000000806A3102000000000000000040B518010000000000000000A05A8C00
          0000000000000000502D46000000000000000000A81623000000000000000000
          548B11000000000000000000AAC508000000000000000000D562040000000000
          000000806A3102000000000000000040B518010000000000000000A05A8C0000
          00000000000000502D46000000000000000000A8162300000000000000000054
          8B11000000000000000000AAC508000000000000000000D56204000000000000
          0000806A3102000000000000000040B518010000000000000000A05A8C000000
          000000000000502D46000000000000000000A81623000000000000000000548B
          11000000000000000000AAC508000000000000000000D5620400000000000000
          00806A3102000000000000000040B518010000000000000000A05A8C00000000
          0000000000502D46000000000000000000A81623000000000000000000548B11
          000000000000000000AAC508000000000000000000D562040000000000000000
          806A3102000000000000000040B518010000000000000000A05A8C0000000000
          00000000502D46000000000000000000A81623000000000000000000548B1100
          0000000000000000AAC508000000000000000000D56204000000000000000080
          6A3102000000000000000040B518010000000000000000A05A8C000000000000
          000000502D46000000000000000000A81623000000000000000000548B110000
          00000000000000AAC508000000000000000000D562040000000000000000806A
          3102000000000000000040B518010000000000000000A05A8C00000000000000
          0000502D46000000000000000000A81623000000000000000000548B11000000
          000000000000AAC508000000000000000000D562040000000000000000806A31
          02000000000000000040B518010000000000000000A05A8C0000000000000000
          00502D46000000000000000000A81623000000000000000000548B1100000000
          0000000000AAC508000000000000000000D562040000000000000000806A3102
          000000000000000040B518010000000000000000A05A8C000000000000000000
          502D46000000000000000000A81623000000000000000000548B110000000000
          00000000AAC508000000000000000000D562040000000000000000806A310200
          0000000000000040B518010000000000000000A05A8C00000000000000000050
          2D46000000000000000000A81623000000000000000000548B11000000000000
          000000AAC508000000000000000000D562040000000000000000806A31020000
          00000000000040B518010000000000000000A05A8C0000000030EA6EFFEA2F3F
          F9CEAF3FFF27565EF1E2A7ADBAEA65CFB8FB5BBFFFDC35D7FCF1F3D75EF75F7E
          73DD8DC7BE74FDCDC7BFF2BEEF9C7CE4FDDFFBCBD76DB8EDBDCB36DEF93FDEB6
          69E5DFBF63CBDD1F3FE5D1B6AD3BE77D3FE873EFDF71FF056724EDFF1BFCE707
          FF7BEDCF687F56FB33DB9FDDFE0EEDEFD2FE4EEDEFD6FE8EEDEFDAFECEEDEFDE
          7E86F6B3A49F1100000000465D8C00000000302CEEB8E4B907ADBAF2A54F6F8B
          5D6DD1EBFE5BFFEAF51BEFFAC0095BD67CE2946DEBBF70DADC03531F7968CB57
          3ED39B5B717E377FED8503BA31F1033F57FB59DBCFDC7EF6F63B68BF8BF63B69
          BF9BF63B6ABFABF63B6BBFBBF43B05000000806111230000000054BAF5E29F7F
          D25DDF78C1A16BAEFEA3C3D7DFF2F6576DB8EDD46336AF3EF35DDBD77FE1B4F9
          4D5F3EABB7E39B83CB6869A98B27F6FDA5B75DBFD3F6BB6DBFE3F6BB6EBFF3F6
          BB6FAF417B2DDA6B925E2B00000000A81423000000001C883BBF7EF85356AFF8
          BD67B7C7626EB8F5AF5FDF1E9DD91EB1B973FB37BEF8A8A5AAB470C5D2E9BF2E
          ED356AAF557BCDDA6BD75EC3F65AB6D734BDD600000000702062040000008027
          72D765BF7EE8DA6B5F7BC47DDF7DD7519B577FE4E4B90D177E68E0319E69498A
          D1D57F5DDB6BDC5EEBF69AB7D7BEBD07DA7B21BD4700000000E089C408000000
          00CD1D97FECA7F688F98BCEFBBA7BC66CBDD9F3865FE81E933BBB9AB2F787899
          69CF6213EC7E3FEC7A6FB4F7487BAFB4F74C7BEFB4F7507A6F01000000401323
          0000000093E58E4B9FF7C8A2DAD6B59F7ACF835B2E3AFB91A5A4BCB0047BABFF
          3E6AEFA9F6DEFAFE62DBF32CB60100000060810D0000006092DC7AD1CFFDC8EA
          6FBEF299F77DE7E423DB32D1435BBEFA9987178CF62C1BC162E9BFEFDA7BB0BF
          D8B6EB3DD9DE9BED3D9ADEBB000000008CA71801000000187DB77FF5994F6EDF
          7475FFAD7FF5FA6DEBBF705A6FC795E7ED591A4ACB44302C2E6CEFD5F69E6DEF
          DDF61E6EEFE5F41E0700000060F4C508000000C068E92FAB5DF3DA231EB8E3EF
          8EDD71FF05677473575FD0168106168360545DD8DED3EDBDDDDEE3EDBD6EA90D
          000000603CC408000000C0705B75E5CB9ED11EB9B86DDD39EFEBEDB8EABC6EFE
          BAA95D3A982053EDBDDF3E03EDB3D03E13E9B302000000C0708B1100000080E1
          71C7A5BFFA947B6E38E6459B56FEC3890F6EBEE8ECB6B8B3475AEA8149D5FF5C
          B4CF48FBACB4CF4CFBECA4CF1400000000C323460000000096CE5D97FDFAA1F7
          DEF2CE576FBBE733A7F6E6BE75C19EC59CB4B0033CBEA9F6196A9FA5F6996A9F
          ADF4990300000060E9C408000000C0E2B9EBF2DFFA99FBBEF35F8FDC7EEF3F9F
          DECD5D73615BBA1958C20116C654FB8CB5CF5AFBCCB5CF5EFA4C02000000B078
          6204000000A0CECACB7FFBB0FBBFF717476FBFEFBC0F74F3D75A5883A5B3EBB3
          77ED85EDB3D83E93EDB3993EB300000000D48911000000808573FB25CF3968FD
          4DC7BD62DBBACFBECF37ACC150EB7F435BFBACB6CF6CFBECA6CF34000000000B
          2746000000000ECC9A6B5EF3FC4DAB3E74D2CE6D977DB1BF1493976580E136D5
          3EC3EDB3DC3ED3E9B30E000000C0818911000000807DB3F28A173F6DC36DEF5D
          36BF69F6936DE9658FB410038CA6FEE7BA7DC6DB67BD7DE6D3B500000000807D
          1323000000004F6CEDB5471FB1E5EE4F9CD2DBB1E2FC3DCB2D69E905184F53ED
          B3DFAE01ED5A90AE11000000003CB1180100000078AC3B2E79EE41F7DE72E2AB
          77DC7FC1196D79658FB4D8024C96FEF5A05D1BDA35A25D2BD2350400000080C7
          8A1100000080DD565DF5F2673C70C7696F7D68EB25E7EC595249CB2B008F36D5
          AE19EDDAD1AE21E9DA02000000C06E31020000004CB2D52B7EEFD99B577FE464
          8F06051640FF51A3ED9AD2AE2DE99A0300000030C9620400000098346BAE3EEA
          F02D777FE294DEDCD517B48593810514808530D5AE31ED5AD3AE39E95A040000
          003069620400000098046BAF3DFA88AD6BCF3EB59BBBE6C2B65832B068025069
          AA5D7BDA35A85D8BD2350A0000006012C40800000030AED65CF3DA23B6ADFBDC
          FBFBCB2396D680E1D0BF1EB56B53BB46A56B17000000C0B88A11000000609CAC
          5EF1BBCF6A8FECF34D6BC008E87F335BBB66B56B57BAA6010000008C93180100
          000046DDCACB7FFBB08D777DE084DE8EABCEEB2F84E44511806136D5AE61ED5A
          D6AE69E95A0700000030EA62040000001845775C7AF85336DCF6DE650F6DFDFA
          E7DBE2C7C02208C0289B6AD7B6768D6BD7BA740D04000000184531020000008C
          92F5379FF0CAF94DCBCFEAE6AF9FDAA503187353ED9AD7AE7DE99A0800000030
          4A62040000001876AB57BCEA595BD79E7D6A5BE4D8232D79008CB3FEF5AF5D0B
          DB35315D2B01000000865D8C00000000C3E8F64B9E73D086DB4E3D66E7B6CBCF
          DDB3B891163A0026D154BB36B66B64BB56A66B28000000C0308A110000006098
          DC73C31B5F32B7E14B67B6058D81850D001E6BAA5D33DBB5335D530100000086
          498C000000004BEDCECB8E3864D3CA0F9DD4CD5D7B615BC61858CE00E0894DB5
          6B68BB96B66B6ABAD6020000002CB5180100000096CA3DD72F7BE1DC03FFFAF1
          FEE2455EC80060DF4DB56B6BBBC6A66B2F000000C05289110000006031DD7EC9
          730E7AE08ED3DEDADBB1E2FCB66431B07401C0C2996AD7DA76CD6DD7DE744D06
          000000584C31020000002C8635D7BCE6F93BEEFB5F67B4858A81050B00EA4DB5
          6B70BB16A76B34000000C0628811000000A0D2FDDF7BF7D13BB75F716E5B9E18
          58A60060F14DB56B72BB36A76B3600000040A5180100000016DA9D5F7FFE4F6C
          5E7DE6BBBAF9EBDAD29AC53580E1B3EBDA7CDD54BB56B76B76BA96030000002C
          B4180100000016CADDDFFAC3E7EDB8FF028F0905182D53EDDADDAEE1E9DA0E00
          0000B05062040000003850EB6F7EFBAB1EDAF2B573DA12C4C0520400A363AA5D
          CBDB353D5DEB010000000E548C00000000FBE3B6AFFCE28F3D70C77F7F6B6FEE
          EA0BDAD2C3C0120400A36BAA5DDBDB35BE5DEBD33D00000000607FC408000000
          B02FEEFCFAAFFDC49635FFF8EEB6E030B0F000C0F8996AD7FC76ED4FF7040000
          00807D1123000000C0DE5875E54B9FBE7DFD3F9FDE961906961B00187F53ED1E
          D0EE05E91E01000000B0376204000000783C775F7DE4F3E61EF8D2996D796160
          990180C933D5EE09EDDE90EE19000000008F274600000080E49EEB8F79D1839B
          2F3EBB2D2B0C2C2F00C054BB47B47B45BA87000000002431020000003CDABDB7
          9CF8EA9DDBBEF1C5B69C30B0AC000083A6DA3DA3DD3BD23D05000000E0D16204
          00000068EEBDE54FFF60E7F62BCF6BCB0803CB0900F044A6DA3DA4DD4BD23D06
          000000A08911000000986C16D700584016D9000000801F2A460000006032595C
          03A0904536000000E031620400000026CBEEC5B5ABCEEBE66F98DEA5038042D3
          ED9E63910D00000068620400000026C3BDDF3EF9C8DE8E15E7B7658281E50200
          A836DDEE41ED5E94EE51000000C0648811000000186FEB6E7CEBCB776EBBFCDC
          B63C30B04C00008B6DBADD93DABD29DDB300000080F11623000000309ED65EFB
          BA173CB4E59273DAB2C0C0F200002CB5E9768F6AF7AA740F03000000C6538C00
          0000C07859BDE2D5CF9EDFF4FFFEA92D070C2C0B00C0B0996EF7AC76EF4AF734
          00000060BCC4080000008C879557BCE4693BEEFF3F1F69CB0003CB010030ECA6
          DB3DACDDCBD23D0E000000180F3102000000A3ED8E4B0F7FCAF6F5E79EDE0EFF
          0796010060D44CB77B5ABBB7A57B1E00000030DA620400000046D7E6551F3DB9
          1DF60F1CFE03C0A89BEEDFE3C2BD0F000000185D3102000000A3E7FEEFFDE5EB
          BAB9EBA6DA01FFC0813F008C8BE976AFEBDFF3C2BD10000000183D3102000000
          A3E39EEB8F79D1CE6D979FDB3FD4CF87FD00306EA6DBBDAFDD03D3BD11000000
          181D3102000000C36FD555FFE997E637CE7EB21DE20F1CEA03C0A4986EF7C276
          4F4CF74A00000060F8C5080000000CAFDBBFF6EC1FDFB6EEF3EF6F87F60387F8
          0030A9A6DBBDB1DD23D3BD13000000185E3102000000C369E39DA7BFAD1DD20F
          1CDA0300BB4DF7EF95E11E0A0000000CA71801000080E172CFF5C7BC68E7F62B
          CF6B07F30307F500C00F9A6EF7CC76EF4CF75400000060B8C4080000000C87BB
          2E7BC1A1731BBE74663B8C1F389C07001EDF74BB87B67B69BAC702000000C321
          4600000060E96D5AF5E193DAE1FBC0613C00B06FA637ADFCD049E95E0B000000
          2CBD1801000080A5B3FEA6E35FD9DBB1E2FC76E03E70000F00EC9FE9766F6DF7
          D874EF05000000964E8C000000C0E25B79C58B9F36BFF1CB67B543F681437700
          60614CB77B6DBBE7A67B31000000B0F8620400000016D7E6551F3DB91DAA0F1C
          B2030035A6FBF7DE704F0600000016578C000000C0E2587BDD9FBC70E7F6ABCE
          6B07E90307EB0040ADE9760F6EF7E2748F0600000016478C00000040ADDBBFFA
          CC276FBFF7BC0FB4C3F381C3740060714DB77B72BB37A77B36000000502B4600
          0000A0CE7DDFFD8BA3BBB9EBA6C2013A00B05476DD9BFBF7E870EF06000000EA
          C4080000002CBC9557BCE4690F6EBAE8EC6EFEC6E95D3A0060E84CB77B75BB67
          A77B39000000B0F0620400000016D6E65567BEAB1D8A0F1C920300C369BA7FEF
          0EF7740000006061C5080000002C8CBBAF3EF2793BB75D716E3B081F38180700
          86DB74BB87B77B79BAC7030000000B2346000000E0C06D5973D67BDAE1F7C061
          3800305AA6FBF7F470AF070000000E5C8C000000C0FE5B73CD6B8FD8B9FD9BE7
          B703EF8103700060344DB77B7BBBC7A77B3F000000B0FF6204000000F6CFD6B5
          9F3EB51D720F1C7A0300E361BA7FAF0F3300000000B07F6204000000F6CDDAEB
          5EFF9BBDED2B7CEB1A008CBFE976CF6FF7FE341300000000FB264600000060EF
          6D5BF785D3DA61F6C0E1360030DEA6FB3340980D00000080BD1723000000F0C4
          D65CF3DA237CEB1A004CB4FEB7B1B59920CD0A000000C0138B11000000787C5B
          EEFEC777B743EB81436C0060324DF767833033000000008F2F46000000205B75
          E5CB9FF1D0D6CBBED80EAA070EAE0180C936DD6684362BA41902000000C86204
          0000001EEB813B4E7F5B3B9C1E38AC060078B4E9FECC106609000000E0B16204
          000000BEEFCEAF3FFF27E6375D74763B901E38A0060048A6DBECD06688345B00
          000000DF1723000000B0DB7DDF3DE535ED107AE0501A00606F4CF76789306300
          000000BBC50800000093EED68B7EEE4776DC77C119EDE079E0201A00605F4CB7
          99A2CD1669E6000000804917230000004CB2BBAFFEA3C37BDB579C1F0EA00100
          F64B9B2DDA8C91660F000000986431020000C0A4DA78D7DFBFA3F3AD6B00408D
          E9FEAC116610000000985431020000C0A4B9FD6BCFFEF1F98DCBCF6A07CB0307
          CD00000B69BACD1C6DF64833090000004C9A180100006092ACBBE1D8977673D7
          4D85036600801ABB668FFE0C126613000000982431020000C0A4D8BAF6EC533B
          DFBA06002C8DE9FE2C126614000000981431020000C0B8BBEBB2171CFAD0964B
          3FDF0E8E070E92010016D3749B49DA6C926616000000187731020000C0385B7F
          D309AFECE66FB0B806000C911BA6FB334A985D000000609CC508000000E36ACB
          9A7FFAEB6EFEA6995D3A00802133D39F55C20C03000000E32A460000001837B7
          7FED39073DB8E9E2B3DBC1F0C0413100C0309969334B9B5DD24C03000000E326
          4600000018276BAF7BFD6FF6E6AE9F0A07C4000043A9CD2E6D8649B30D000000
          8C9318010000605C6C5AF90F2776BE750D00184D33FD5926CC38000000302E62
          040000805177EBC54F7FD2DC03FFF793EDE077E02018006094CCB499A6CD3669
          E6010000805117230000008CB25557BDF299BDEDDFBA201C0003008CA436DBB4
          1927CD3E00000030CA62040000805175EFB74F3EB2F3AD6B00C0789AE9CF3A61
          06020000805115230000008CA22D6B3EF59E76B03B70D00B00304E66FA334F98
          850000006014C508000000A3E4D68B7EFE49730FCC7EB21DE80E1CF002008CA3
          9936FBB41928CD46000000304A620400008051B1F28ADF79FACEED2BCE0F07BB
          000063ADCD406D164A33120000008C8A180100006014ACBFF91DAFEA7CEB1A00
          30D966FA335198950000006014C408000000C36EF3EA8F9FD20E6C070E700100
          26D1CCE6D51F7B579A9900000060D8C508000000C36C6EC397CE6C07B50307B7
          0000936CA63F2385D90900000086598C000000308CEEB8E4B9073DB4E5EB9F6F
          07B40307B61CA8955FEE7ADF39B7FFBFF1FF070046C14C9B95DACC9466290000
          00184631020000C0B059FDCD573DAB3777DD5438A86521B4C5B53F39B8EBDE70
          70D7FBD86BBBEE9CE3F33F07000CBDDE8E6B2F6CB3539AA900000060D8C40800
          0000C364FD2D27BEBAF3AD6BB51E5E60DBA3F7D69FEEBACF1EE71BD9006074CD
          F467A8305B010000C030891100000086C5C6BBCE38B11DC00E1CC8B2D00616D8
          1E5964FBB367EDFE36368B6C00308A66FAB35498B10000006058C408000000C3
          60DBFA7FF9403B781D3888A5C20F59607BC4E92FF3585100184D33FD992ACC5A
          000000300C6204000080A574DBC5CFF8D1F94D179FDDCDDF3CB34BC72258B93C
          2FAE3DDA318774BDB39675DDE74EC8FF0D006058CDB4D9AACD5869F602000080
          A514230000002C953B2F7BC1A13BB7AF383F1CBC52696F16D8F6E89D70D8EE25
          B6F6EFA4FF16003094DA8CD566AD3483010000C052891100000096C2DD2BFEE0
          B9BDB9EBA7D2812BC5F66181ED61BDF71CEEDBD80060C4B459ABCD5C69160300
          0080A510230000002CB675371EF78ACE234397CE7E2CB03DE28C5759640380D1
          32D39FBDC24C060000008B2D46000000584C1B6E3DF59876903A70B0CA623A90
          05B6E64D8776DDA7DFE2B1A200303A66FA335898CD0000006031C5080000008B
          65F3AA8FBDAB1DA00E1CA8B2D80E74816D8FDE3B9FBEFBDBD82CB201C02898E9
          CF62614603000080C512230000002C866DEBCE3DBD1D9C0E1CA4B214166881ED
          61BD535FE0B1A200301A66FA335998D50000006031C408000000D5E61E98FD64
          3B301D384065A92CF0025BDFB283BBDE478FB2C80600C36FA63F9B85990D0000
          00AAC508000000556EFBCA2F3FF9A12D5FFF7C3B281D38386529552CB03DECD8
          A776DD678FF3585100186E336D466BB35A9AE1000000A04A8C00000050E18E4B
          7FF5293BB75D755E383065A9552EB0EDD13BE997767F1B9B453600185A6D566B
          335B9AE5000000A0428C000000B0D0EEBAFCB70FEB6DBFFA827450CA10588405
          B647FCDD8B3D56140086589BD9DAEC96663A000000586831020000C0425AFDCD
          DF7D566FEEFAA97440CA9058CC05B6E60D8774BD4FBEDE221B000CA936BBB519
          2ECD76000000B090620400008085B2E69AA38FE8E66F9E193C1465C82CF602DB
          1EBDB7FD4CD79D73BCC78A02C0709AE9CF7261C6030000808512230000002C84
          75371EF78A76F0397010CA305AA205B687F5FEFCD9BBBF8DCD221B000C9B99FE
          4C17663D000000580831020000C081BAEF3B7F71743BF01C380065582DF102DB
          233EF80A8F150580E133D39FEDC2CC07000000072A4600000038100FDCFE77C7
          B683CE81834F86D9B02CB0356FFCC9AE3B6B99453600182E33FD192FCC7E0000
          0070206204000080FDB5F1CE0F9ED00E38070E3C1976C3B4C0B647EF849FF358
          5100182E33FD592FCC80000000B0BF6204000080FDB179D5C7DED50E36070E3A
          190543B8C0F6B0DE5FFD9A6F630380E131D39FF9C22C08000000FB2346000000
          D85796D746DC102FB03DACF7E13FB4C80600C3C1121B0000000B2646000000D8
          175BEEFED47BDA41E6C0C126A3640416D8FADE7C68D77DFA2D1E2B0A004B6FA6
          3F0386D910000000F6458C000000B0B7B6AEFDF4A9ED0073E0409351332A0B6C
          7BF44EFC85DDDFC666910D0096D24C7F160C3322000000ECAD18010000606F6C
          5DFB9953BBF95B6677E91871ABFE2D2E8A0DBBDEFB7EABEB3EFFCEFC3301008B
          61B63F13865911000000F6468C000000F044B6ADFF970FB403CB81034C46D588
          2EB0F52D3BB8EB7DECB516D90060E9CCF667C33033020000C01389110000001E
          8FE5B53134CA0B6C7BF4DEF2D4AE3BE7F8DD3F4BFA1901804A96D8000000D82F
          31020000C00F63796D4C8DC102DBC37A273F73F7B7B159640380C566890D0000
          807D162300000024DBD6FFF3E9DD83B7CCEED23166568FCF02DB234E7B49D77D
          E19DF9E70500AACCF667C6304B0200004012230000000CDABAF633A7B603C981
          034AC6C5382EB035C71CD2F53EB5CC221B002CAED9FEEC18664A000000181423
          0000003C9AE5B50930AE0B6C7BF4DEF6B35DF7B91376FF9CE9E70700169A2536
          000000F64A8C000000F0B0CD777FE2947600397020C9B819F305B687F5DEFD2B
          BE8D0D0016CF6C7F960C33260000003C2C460000006836AFFEB8E5B54931210B
          6C8F38E35516D9006071CCF667CA306B020000401323000000585E9B3093B6C0
          D6BCE927BBEEEC375B6403807A96D8000000F8A16204000060B26DBCF38327B4
          83C6818347C6D9242EB03DECED4FDBBDC4D67E07E97703002C84D9FE8C19664F
          000000265B8C0000004CAE0DB7FDCD31ED8071E0C0917137C90B6C7BF4FEDB7F
          F46D6C00506BB63F6B861914000080C9152300000093E9DEEFFCD951ED6071E0
          A0914960816DB7650777BD8F1E65910D00EACCF667CE308B0200003099620400
          0060F2DC73C39B5FDA0E14070E18991416D87ED0B13FD5759F3DCE634501A0C6
          6C7FF60C332900000093274600000026CBDD571F75783B481C3858649258608B
          7A7FFA8BBBBF8DCD221B002CB4D9FE0C1A6653000000264B8C0000004C8E5557
          BDE297BAF99B66C2A12293C402DBE3FBDB177AAC28002CB45D33687F160D332A
          00000093234600000026C35DDFF88DA7F6E66F988E078A4C160B6C4FEC0D0777
          BD8F1F6D910D0016509B45DB4C9A665500000026438C0000008CBFDB2F79EE41
          BD1DD75C980E12994016D8F65AEFAD3FDD759F3BC16345016081B499B4CDA669
          6605000060FCC5080000C078BBF5E25FF8D19DDBBF797E3A4064425960DB67BD
          3F7BD6EE6F63B3C8060007ACCDA66D464DB32B000000E32D46000000C6DB835B
          BE764E3A3864825960DB7F1F78B9C78A02C002E8CFA8617605000060BCC50800
          00C0F8DAB161FACCEEC15B66070F0C997016D80ECC318774BD4F2DB3C8060007
          66B63FAB861916000080F11523000000E369EBBACFBFBF1D0C0E1C148205B605
          D23BFE308F1505800333DB9F59C32C0B0000C0788A11000080F1B371E5874FEA
          1EFCF6EC2E1D3CC6EA8BE24216FBA7F79EC3F77C1B5BF85D03004F64B63FBB86
          9916000080F11323000000E3E5BEEFFEE5EBDA41E0C0C1207C9F05B61AFFF06A
          8B6C00B07F66FB336C986D010000182F31020000303ED65E7FCC8BDA01E0C081
          20FC200B6C75DEF4935DF7D9E32CB201C0BE9BEDCFB261C6050000607CC40800
          00C0785875D57F7E663BF81B380884C7B2C056AE77E22FEC5E626BBFEBF41A00
          00C96C7FA60DB32E000000E3214600000046DF1D971EFE94DEFC8DD3E110101E
          CB02DBA2E9FDCD6FF8363600D8076DA66DB36D9A79010000187D3102000030DA
          BEF76F87FDFB87B65D755E3A0084C802DBE25A7670D7FBE85116D900602FB5D9
          B6CDB869F605000060B4C5080000C0689BDBB8FCAC74F0073F9405B6A571EC4F
          75DD39C77BAC2800EC85B98D5F3E2BCDBE0000008CB618010000185D5BEFF9DC
          FBBB07BF3D3B78E0078FCB02DB92EA9DF4CBBBBF8DCD221B003C9ED9FEAC1B66
          6000000046578C0000008CA607EE38EDADED606FE0A00F9E9805B6E1F0772FF6
          585100787CB3FD9937CCC20000008CA618010000183DF7DC70EC4BDB81DEC001
          1FEC1D0B6CC3E30D8774BDB396596403801F6EB63FFB869918000080D1132300
          0000A365E5152F7D7A3BC81B38D883BD67816DE8F4DEF6331E2B0A003FDC6C7F
          060EB331000000A3254600000046C76D5FFDE527F7765C7B6138D483BD67816D
          68F5FEFCD916D900206833709B85D38C0C0000C0E88811000080D1F1E0964B3F
          9F0EF4609F58601B7E1F7C85C78A02C080FE2C1C666400000046478C0000008C
          86EDF75D7046E7D1A12C040B6CA3E18D8774BDB3DF64910D00BE6FB63F138759
          19000080D11023000000C36FE35DFFF31DEDC06EE0000FF68F05B691D23BE1E7
          3C561400BE6FB63F1B879919000080E11723000000C3ED9EEBDFF89276503770
          7007FBCF02DB48EAFDD5AFF9363600D86DB63F2387D919000080E11623000000
          C3EBCECB5E7068377FF34C38B483FD67816DA4F53E72A4453600D83523F767E5
          304303000030BC6204000060783DB4EDAAF3E2811D1C080B6CA3EFCD8776DD39
          C77BAC280013AD3F2B87191A000080E11523000000C369C786E933BB07BFB37C
          970E16D49AAFE6A528464EEFC45FE8BA734FDEFD9AA6D71A00C6DBF2FECC1C66
          6900000086538C0000000C9F4D2B3F72723B901B38A0838561816DECF4FEF6B7
          762FB2A5D71B00C6DBF2FEEC1C666A000000864F8C0000000C9775371EF78A76
          10377030070BC702DB785A7670D7FBF8D116D9009844CBFB337498AD01000018
          2E31020000303CEEBAFCB70FEB1EFCF66C3894838563816DBCBDE5A95DF78577
          7AAC280013E6DBB3FD593ACCD80000000C8F18010000181E0F6DFBE6F9F9400E
          169005B689D03BF999BBBF8DCD221B0013A23F4B87191B000080E11123000000
          C361FB7DFFFB439D4787B2182CB04D96D35FE6B1A2004C8AE5FD993ACCDA0000
          000C8718010000587A1B6EFBDB37B503B7810338A861816DF21C7348D7FBD41B
          2CB201300996F767EB3073030000B0F4620400006069AD5EF1FBCF6D076D0307
          6F50C702DBC4EABDED67BBEE0BEFF4585100C6DDF2FE8C1D666F00000096568C
          0000002C9D5B2F7EFA9376EEB8F6C270E806752CB04DBCDEBB7FC5B7B10130D6
          DA8CDD66ED3483030000B0746204000060E9CC3DF0E5B3D2811B94B2C0C6C3CE
          7895453600C6567FD60E33380000004B274600000096C6C6951F3EA9F3E85096
          8205361EED8D3FD9759F7E8B453600C6D1F2FECC1D667100000096468C000000
          2CBEB5D72D7B613B501B386083C561818DA0F7F6A7ED5E626BEF8FF4BE0180D1
          B4BC3F7B87991C000080C517230000008BEBF6AF3DE7A0DEFC4D33E1700D1687
          05361E47EFBD47F8363600C64A9BBDDB0C9E667300000016578C0000002CAEF9
          4D179F9D0ED660D15860E3892C3BB8EB7DF4288B6C008C8DFE0C1E6673000000
          16578C0000002C9E8D2B3F7C52E7D1A12C350B6CECAD637FAAEB3E7782C78A02
          300E96F767F130A3030000B078620400006071DC7DF51F3FBF1D9C0D1CA4C1E2
          B3C0C63EEAFDE92FEEFE36368B6C008CB6E5FD993CCCEA0000002C8E18010000
          A877EB453FFFA4DE8EEBA6C2211A2C3E0B6CECA7DEDFBED063450118696D26BF
          F5A2A7FD489AD9010000A81723000000F576DCFFA533D3011A2C090B6C1C8837
          1CDCF5FEF1F516D9001859FDD93CCCEC000000D48B110000805AF7DFFADE659D
          47872E9AAD1BAFEB36DCBBA2DBBAE9BAEEC1ED37C77F66E259606301F4DEFAD3
          5DF785777AAC2800A368797F460FB33B000000B562040000A0CECA2B7EE7E9ED
          806CE0C08C42F3DB6EEAD6AEBEFC11EBD75ED96DBCEFEA6EFBE6EBBB9D73B7C4
          7F67E258606301F5FEEC59BBBF8DCD221B00A365797F560F333C000000756204
          0000A0CE43DBAE3A2F1C96516CDD9A2B7E6089EDD1EEBDE7AA6ED3866BBA1D5B
          6FEC7AF3DF8EFFFED8B3C046850FBEC26345011829FD593DCCF0000000D48911
          0000801A5BD77DF1B4EEC1EF2EDFA563716DB8F75BDDDAD56D89EDF1DD73F715
          DDFDEB57745B365ED7CD6F6B8F1BCDFFBDB1B3E66B7901090ED4318774BD4F1F
          BB67912DBCF70060B82CEFCFEC6196070000A0468C0000002CBC7B6E78CBCBDB
          81D8C001198B64EBA6EBE3C2DA1359B7E6CAEE817BAFEEB66DBAA17B68477BDC
          68FEEF8F3C0B6C14EB1D7FD89EC78AEE7AAFA5F720000C8FE5FDD93DCCF40000
          002CBC180100005858B77DE5177FAC377FF34C381C63913CB8FD96B8A0B6AFD6
          AFBDAADB78FF35DDF62D37763BE7DAA3A6F29F37722CB0B1487AEF39DCB7B101
          30F47A7337CDB4193ECDF60000002CAC1801000058583B36FCEBC7D3C1188BAB
          2D9FA5A5B4FDD51E377ADFBA6F769B375CDBCD6DBDA9EBCD8FF0429B05361659
          EFC37F68910D80A1D69FE1C36C0F00FC7FF6FE3CD8B2BA3CF4FF7F75EB56EAD6
          AD5BA9E456BED6B71B44C499ABC6386B88123589184C8C0351E335DD4C0AA288
          A2885122C1212AC61925448D035F1C30C6EB350E04B14564689BA6E986A667A0
          A1E73EDD679ED7E7C7DAEDD6E6F0ECD367D87BEDB5D67EFDF1FAE751E09C7D86
          7D9EFABCEBB300A0BDC221000000EDB37FE385AF4D1E1D5A0AF9CD695188D62E
          79D0D6B7F7D634D4BF364D8EAE0F3F86D212B0D10DA71F95D257DF246403A0AC
          AE69FC2D1FFC8D0F000040FB8443000000DAE3EE9F3FEBFF4D931BC56B25313A
          7847189E75CA9E9DB7A483FB57A791C175696A6C43F8319586808D2ECACE7DEC
          A1882DFF3E8CBE3F01A06B365ED3F89B3EF85B1F000080F60887000000B4C7C4
          C08A2FC7076174C3F4F88630342BCABE5D2BD340DF6D696CE88E073E96923D6E
          54C0460964FFF0476E6303A0741A7FD3077FEB030000D01EE110000080C53B78
          F747CF491E1D5A3AFB76FF328CCB8A963F6E74FF9E5569F0E0ED697CE4CE944D
          C41F6F61046C94C5B22529BBEC35423600CAE49AC6DFF6C1DFFC0000002C5E38
          0400006071B6DFF2678FCF0FBA661C7C51020307D6844159B7EDDE714B3AB0EF
          D634DCBF364D8EDE157EEC1D2560A36CCE7C784A579EEBB1A20094C5358DBFF1
          83BFFD010000589C70080000C0E24C8DACFC5670E845098C0DDF19066465B377
          E7CAD4BF7F751A1D5CD778F469F4B9B495808D92CADEF6BF0EDDC6266403A0CB
          1A7FE3077FFB030000B038E110000080851BB8EF73EF4A6E5F2BAD6C6263E3F1
          9D51345666F9A34FF3DBE3C686EE687C0ED1E7B6280236CAEE432FF2585100BA
          ED9AC6DFFAC10E000000C0C2854300000016E6BE5B4E3A3E3FD89A71D045C9F4
          ED5D154662559107787D7B56A5A183B7A78991F5E1E7386F0236AA60F99294FD
          CB72211B00DD744DE36FFE60170000006061C2210000000B33357C8B478756C0
          50FFED611856557B76DE920EECBB350D0FAC4D93A377859FF31109D8A890ECAC
          633D561480AE69FCCD1FEC020000002C4C3804000060FEFAEFFDC45B93DBD72A
          21BFB52C0AC1EA62EFAE95A9BFEFB6343A78479A1EDF10BE060F2160A382B20B
          9E226403A01BAE69FCED1FEC04000000CC5F38040000607EB6DFFC278FCD0FB2
          661C6C5162F9AD6551FC5547FB76FF320D1C5893C686EF4CD9C4C6F0F510B051
          69FFF4528F1505A068D73476806037000000607EC221000000F3333974E355C1
          A116257670DFEA30F6AABBDD3B6E4E7D7B57351EA39ADF44F7EBD744C046D59D
          B634655F394BC80640611A3B40B01B000000303FE110000080B93B78F787CF4E
          6E5FAB9C91817561E0D56BF29BE8F2986FF4EE6BE328082A263BE7511E2B0A40
          51AE69EC02C18E000000C0DC8543000000E6E6DE1B4F3C363FB89A719045054C
          8DDD15065DBD6AEF9DDF0F6320A8AAECBDCF721B1B0045B8A6B11304BB020000
          0073130E010000989BC9C19F5F191C6251117B77AD0C63AE5E2460A3AEB24FBF
          52C80640473576826057000000606EC22100000047D6B7E5E25393DBD72AADBF
          EFB630E6EA4502366AED8CA353FADA391E2B0A40A75CD3D80D829D0100008023
          0B87000000CC6EDBCF9EF23B6972A378ADE24687EE0863AE5E2460A317646F7D
          DCA1DBD8846C00B4DDC66B1A3B42B03B00000030BB70080000C0ECC6FAFEEF67
          E3832BAA647A7C63DABD230EBA7A8D808D5E92BDFFF91E2B0A40DB3576846077
          0000006076E110000080D676AF3BF34FD3E4E66B1F90A8BEFD7B56A55DF7DFD2
          F3F6ACFF6118FA406D2D5B92B27F7E7D4ADFBC20FCDD00000B706D6357087608
          0000005A0B87000000B4968DADFB7E705845450D1EBC3D0CBA7A8D808D9EF586
          630EDDC6B6EBE7E1EF0800988FC6AE10EC10000000B4160E010000880DEEF8C2
          45C9ED6BB5323EBC3E0CBA7A8D808D5E979DFFC443B7B109D900589C6B1B3B43
          B04B000000100B870000003CD47DB7BCF8F1F981D48C032A2A2E9BD89476EF88
          A3AE5E2260835FF9F09F79AC28008B756D637708760A0000001E2A1C020000F0
          509343BFB82A389CA206FAF6AE0EA3AE5E226083C32C5F9AB22F9D29640360C1
          1ABB43B053000000F050E11000008007EBDBFCBE65C9ED6BB535DCBF2E8CBA7A
          89800D1E2A3BFB91297DE37C8F15056021AE6DEC10C16E010000C08385430000
          007E63EB8A27FD769ADC744D7028454D4C8EDE15465DBD44C006AD65EFFA8343
          B7B109D90098974DD734768960C7000000E037C221000000BF31B2EFDB1F8B0F
          A4A8933D3B57866157AF10B0C11C7CE2651E2B0AC0BC34768960C7000000E037
          C22100000087ECB8F555CF4C1E1DDA130EEEBF2D0CBB7A85800DE6E8B4A352FA
          EA9B846C00CCD5B58D9D22D83500000038241C02000070C8E4F0CDDF080EA1A8
          A191C13BC2B0AB5708D8607EB2373FDA6345019893C64E11EC1A0000001C120E
          01000048FFBFFD9BDFFBFAE4F6B59E3135B6310CBB7A85800D16267BDF73DDC6
          06C0915CDBD82D829D03000000011B00004068CB758FFDAD34B1E147C1E11335
          B66FF7AA30EEEA0502365884654B5276D96B846C00B4F6C06ED1D83182DD0300
          00A0D785430000805E37BCFBCA0F84074FD4DA40DF9A30EEEA0502366883338E
          4EE9CA733D5614805063C708760F0000805E170E0100007AD97D2BFFFC89C9A3
          437BD2D8D09D61DCD50B046CD03ED9794F38741B9B900D8007BBB6B16B043B08
          0000402F0B87000000BD6C62F0E75706874DF4806C6253DABD230EBCEA4EC006
          ED977DF0051E2B0AC08334768D6007010000E865E1100000A057EDBDEBEDAF4C
          6E5FEB69FBF7DC1A065E752760830E59B62465572C13B201D0746D63E7087611
          0000805E150E0100007A55367EE70F8243267AC8E0C1B561E055770236E8ACEC
          8D8F48E91BE77BAC2800A9B17304BB08000040AF0A87000000BDA8FFDE8FBF35
          B97DADE78D8FAC0F03AFBA13B04131B2773CF9D06D6C4236805E766D63F70876
          120000805E140E0100007ACDB69F3DE577D2E4966B1F90E86DD9C496B47BC72F
          D3AEFB57F6943DEB7F14C63640875CFA9294AEBE30FC3D04404FB8B6B18304BB
          09000040AF0987000000BD6664CF373F1A1C2AD1A30EECBB2D8CBCEA4CC0065D
          70EAD2947DF92C211B408F6AEC20C16E020000D06BC2210000402FB9EF96938E
          4F6E5FE330C3FD778491579D09D8A07BB2B38FFBD563456F087F2701505BD736
          76916047010000E825E1100000A0974CF4FFF4CBC161123D6C7274631879D599
          800DBA2F7BCF33DDC606D0631ABB48B0A3000000F49270080000D02B76AD3DED
          45C9ED6B04F6EE5C15865E75256083F2C83EF54A211B40EFB8B6B19304BB0A00
          0040AF0887000000BD626A64F57782432448FDFBD784A1575D09D8A0644E3F2A
          A52BCF15B201F480C64E12EC2A000000BD221C020000F482FD9B2F7A7D72FB1A
          2D8C0EDE19865E7525608372CACE7DECA1886DD70DE1EF2A006AE1DAC66E12EC
          2C000000BD201C020000F4826C62C38F82C32368981EDF14865E7525608372CB
          FEE18FDCC60650638DDD24D8590000007A4138040000A8BBFE7B3FF9B6E4F635
          8E60DFEE5BC3D8AB8E046C5001CB96A4EC73AF15B201D4D3B58D1D25D85D0000
          00EA2E1C020000D4D9D69F1EFFDFD3E466F11A473470606D187BD591800D2AE4
          CC87A7F4F5F33D5614A076365FDBD855821D060000A0CEC2210000409D0DEEF8
          C245F181113CD8D8F05D61EC55470236A89EEC6DC71FBA8D4DC806501B8D5D25
          D861000000EA2C1C020000D4D5DDD73FE361C9A34399A36C6273DABD230EBEEA
          46C00615F6E13FF3585180FAB8B6B1B304BB0C0000405D8543000080BA1ADEF3
          8D0F078744D052DF9ED561F055370236A8B8E54B52F6C5D3856C0035D0D85982
          5D060000A0AEC2210000401DDD7BE31F1F97DCBEC63C0D1D5C17065F75236083
          7AC8CE3A36A56F9CEFB1A200D5766D637709761A0000803A0A870000007534BA
          FF7B9F0E0E87605613231BC2E0AB6E046C502FD9054F39741B9B900DA0921ABB
          4BB0D3000000D45138040000A89BFB56BEF4C9C9ED6B2CD09E9DBF0CA3AF3A11
          B0414DFDD35F78AC2840355DDBD86182DD060000A06EC221000040DD8CF75FF7
          A5E05008E6E4C0BEDBC2E8AB4E046C5063A72E4DD957CE12B201544C63870976
          1B000080BA098700000075B273F56B9E9BDCBEC6220C0FDC11465F75226083FA
          CBCE39CE634501AAE5DAC62E13EC3800000075120E010000EA6462F0E7570687
          41306793631BC3E8AB4E046CD03BB2F73ECB6D6C0015D1D865821D070000A04E
          C2210000405DECBCEDB56E5FA32DF6EE5A15865F75216083DE937DE614211B40
          F95DDBD869825D070000A02EC2210000405D4C0CFCECABC12110CC5B7FDFED61
          F855170236E851A71F9DD295E77AAC28408935769A60D7010000A88B70080000
          5007F7AF7AC5D393DBD76893D1C1F561F855170236E86DD95B1F77E83636211B
          40195DDBD86D829D070000A00EC2210000401D8CF7FFF4CB69726B8276981EDF
          1C865F7521600372D925CF4FE9DFDE13FE1E04A07B1ABB4DB0F3000000D44138
          040000A8BAFB57BDF2E96972EB75330F7E6031F6EF5E1DC65F752060037E6DD9
          92945DFE7A211B40B95CD7D87182DD070000A0EAC221000040D5B97D8D4E183C
          B02E8CBFEA40C0063CC41B8E49E91BE7A7B4FBA6F0772200C5720B1B00005057
          E1100000A0CADCBE46A78C0F6F08E3AF3A10B001AD64EF78D2A1DBD8846C00DD
          E616360000A096C22100004095B97D8D4EC926B6A4DD3B7E190660552760038E
          E8232FF65851802E730B1B00005047E1100000A0AAEE5FF5574F4D6E5FA383FA
          F6DE16066055276003E664F9D2947DF92C211B40F75CD7D879825D080000A0AA
          C221000040558D1DF8D1E5C1210FB4CD50FF1D610056750236603EB2B38F4DE9
          EA0B3D5614A00B1A3B4FB00B010000545538040000A8A27B6F7AE1A393DBD7E8
          B089D18D6100567502366021B277FDC1A1DBD8846C0045BAAEB1FB043B110000
          401585430000802A1AD973F5C782C31D68BB3D3B578511589509D88045F9C4CB
          3C5614A0408DDD27D889000000AA281C02000054CDDDD73FFDF792DBD728C8C1
          7DB78711589509D880453BEDA8947DED1C211B4031AE6BEC40C16E0400005035
          E1100000A06A06777CE1A2E050073A6264607D18815599800D6897EC9C477BAC
          2840011A3B50B01B010000544D38040000A8922DD73DF6B792DBD728D0D4D8E6
          3002AB32011BD06ED9FB4E701B1B40675DD7D885821D090000A04AC221000040
          951CBCE79FDE121CE64047EDDD756B18825595800DE888654B5276D96B846C00
          1DD2D885821D090000A04AC221000040A54C6CBA263ACC814EEAEF5B1B866055
          2560033AEA8CA353FAFAF91E2B0AD06EF92E14ED4800000015120E010000AA62
          FFE6BF5F963C3E942E181BBA2B0CC1AA4AC00614217BEBE30EDDC62664036897
          EB1A3B51B02B010000544538040000A88AE9B13BBE1F1CE240C74D8F6F49BB77
          FC328CC1AA48C0061429FBE00B3D5614A04D1A3B51B02B010000544538040000
          A882DD779CF592E4F635BA68FF9ED5610C56450236A070CB96A4EC5F960BD900
          16EFBAC66E14EC4C00000055100E010000AA6072F017570587375098C183EBC2
          18AC8A046C40B7646F3826A56F5EE0B1A2008BD0D88D829D090000A00AC22100
          0040D9DDB7F2A54F4E6E5FA3CBC687378431581509D8806ECBDEF1E443B7B109
          D90016E2BAC68E14EC4E00000065170E010000CA6E64DF773E1E1CDA40A1B289
          2D69F78E5F864158D508D880D2B8F4251E2B0AB0008D1D29D89D000000CA2E1C
          02000094D9B69F3DF57F26B7AF511207F6AE0983B0AA11B001A572EAD2947DF9
          2C211BC0FC5CD7D895821D0A0000A0CCC22100004099F5DFFB99F3D3E4B60465
          30DCBF3EEDBA3FBF85ADDAF6DC754D1C9100745176F671873D5634FE3D0CC06F
          3476A5608702000028B370080000506A139BAF8D0E6BA01B264737874158D508
          D88032CBDEF3CC5FDDC616FF2E06E057F25D29DAA10000004A2C1C02000094D5
          BE0DEF7A759ADC76DD430E6AA08BF6ECBC358CC2AA44C0065441F69953846C00
          B3BBAEB13305BB14000040598543000080B29A1AB9EDBBC1210D74D5C1FD6BC3
          28AC4A046C40659C7E544A579E2B640368A1B13305BB14000040598543000080
          32DA79DBEB4E486E5FA3844606EF0AA3B02A11B00155939DFBD84311DBEE9BC2
          DFCD003DECBAC6EE14EC5400000065140E010000CA68ACEF4797078733D07553
          635BC228AC4A046C405565FFF0476E630398A1B13B053B150000401985430000
          80B2B9FBFA673C2CB97D8D12DBB77B7518865585800DA8B4654B5276F9FF16B2
          01FCC6758D1D2AD8AD000000CA261C020000944DFF3D9F786B702803A5317060
          5D18865585800DA885331F9ED2372FF0585180073476A860B7020000289B7008
          00005036D9F8C66BA24319288BB1A10D611856150236A04EB2B71D7FE8363621
          1BD0C31A3B54B05B010000944D380400002893DDEBDEF0E2E4F1A1945C36B135
          EDDEB12A8CC3AA40C006D451F691177BAC28D0CBAE6BEC52C18E0500005026E1
          100000A04C26FA577C39388C81D2E9DBB3268CC3AA40C006D4D6F22529FBD299
          4236A0273576A960C70200002893700800005016F7FCE279C724B7AF51114307
          EF0CE3B02A10B095CFEA3FFFDD50B663539A7EFF89E13F03B4969D756C4A575F
          E8B1A240AFB9AEB15305BB16000040598443000080B218B8EF9FDF1D1CC24029
          4D8C6C0AE3B02A10B0954B3356BBF1C5BFF31059961DB26D75CACE7954F8CF03
          AD65173CE5D06D6C4236A0473476AA60D7020000288B70080000501A139BAF8D
          0E61A08CB2896D69F78E556120567602B6F2982D5E6BFA75C496DFC6F6F3AFA4
          B47C69F8EF0266F1F19779AC28D01BF29D2ADAB50000004A221C02000094C1DE
          F56F7D59F2F8502AE6C0BEDBC340ACEC046CE5309778ED97AF79EC6F02B6C343
          B62FBD31FC7702B3387569CABE768E900DA8BBEB1ABB55B07301000094413804
          00002883C9C11BAF0A0E5FA0D48607D6878158D909D8BA6F2EF15A2E0CD89A76
          6C4AD9DF3D23FCF703AD65E71CE7B1A240AD3576AB60E7020000288370080000
          D06DF7DEF8FC6393DBD7A8A0C9D1CD6120567602B6EE9A6BBC969B35606BDA78
          53CADEF888F0BF05B496BDF7596E6303EAEABAC68E15EC5E000000DD160E0100
          00BAAD7FFB67DF191CBA4025ECDD756B1889959980AD7BE613AFE5E614B0E5F2
          DBD87EFCA99496C5FF5DA0B5EC33A708D980DA69EC58C1EE050000D06DE11000
          00A0DBB2F14DD7A4C9BB135451FFFE7569D7FDAB2A65CF5DFF19461C74D67CE3
          B5DC9C03B6A61D9BD2F467FE3AFCEF03B338FDE894BE7E7E4AFFFEBEF0773D40
          D53476AC60F7020000E8B670080000D04D3BD7FCED8969F2EE15330F5CA02A46
          07378491589909D88AB790782D37EF80AD29BF91EDFC27851F0BD05A76EE630F
          456C7B7F19FECE07A890158D5D2BD8C1000000BA291C02000074D3E8FEEF7F36
          386C81CA981EDF1A46626526602BD642E3B5DC8203B65C7E1BDBED3F4ED9990F
          0F3F2EA0B5ECFD27BA8D0DA8BCC6AE15EC60000000DD140E010000BA65EB754F
          F8EFC9ED6BD4C0BEDD6BC250ACAC046CC5594CBC965B54C0D694DFC6F6EF1787
          1F1F308B654B5276C572211B50652B1A3B57B08B010000744B38040000E896FD
          9B2E7A7D70C802953370E0CE30142B2B015B31161BAFE5DA12B035E521DB474E
          0A3F56A0B5C62D86DFBCC06345814A6AEC5CC12E060000D02DE1100000A05BA6
          46567F273A6481AA191BDE188662652560EBBC76C46BB9B6066C4DDBD7A7EC2D
          8F0D3F6EA0B5E9B73E3E4D5EF9F634BDF3A6F0BD00A08C1A3B57B08B01000074
          4B38040000E886EDB79C747CF2F8506A229BD89676EFB8358CC5CA48C0D639CD
          70AD1DF15AAE23015B6EC7A6347DCBD5299D7654F87900AD4D5C74421AB9FC0D
          69E0C01D696C6863E33D207A6F002889158DDD2BD8C9000000BA211C02000074
          C3E08E2F5E141CAE4065F5EDBD3D8CC5CA48C0D619ED0CD79A3A16B035E58F15
          BDF2BCF0F30166B16C491AF9F8ABD2F0E7CF6804CC7D7B6E4F4307D7A78991CD
          E17B0440373576AF6027030000E88670080000D00DD9C4E66BA3C315A8AA3C5C
          8862B13212B0B55F27E2B55CC703B6A6FC46B67F785EF8B901AD4D9F794C2362
          CB7FAFFEFA77ECCE5BD3817D6BD3F0C05D6972744BF89E0150A4C6EE15EC6400
          0000DD100E0100008AB6F3B6D79D903C3E949AC96FDD393C122B33015B7B752A
          5ECB1516B0356D5B9DB2B31F197E9E406B936F7BE24342B6A6BDBB56A7FEBE75
          697470439A1EDF1ABE870074D88AC60E16EC66000000450B87000000451BD973
          F5C7824315A8BC3D3B573F245C2823015BFB74325ECB151EB0E5F2DBD856FC4B
          4ACB97869F33D0DAF8252F6C846CD1EFDEA67DBBD7A4810377A6B1E14D299BD8
          16BE9F00B45B63070B7633000080A28543000080C2B97D8D9ACA1F1917C50A65
          23606B8F4EC76BB9AE046C4D79C8F62FA7859F3BD05AB67C691AF9D4DF1C3164
          CBEDDE716BEADB7B7B1AEA5FDFB8C9337A6F01689315E16E06000050B0700800
          0050A4DDEBCE7A49E3F0243E54814A1B1EB82B0C14CA46C0B67845C46BB9AE06
          6C4D3B36A5ECC2A786AF03D0DAF41B8F6DF958D156F29B3C0FEE5B9B461E783F
          991ADB12BED7002CD08AC62E16EC68000000450A87000000451AEBFBE1E5C161
          0AD4421E1B444142D908D816A7A8782D578A80ADE9CE15297BC331E16B02B436
          F98EA7CCE936B6C8DE5DB7A5FEBE3BD2E8D0C6343DEE71A3C0E23476B1604703
          000028523804000028CA96EB1EF7DF92DBD7A8B93C368822843211B02D5C91F1
          5AAE54015B2EBF8DED871F4B6959FCFA00AD8D7DE8A405876CB9FC71A3FBF7AC
          498307EE4CE3C30FFC2C4E08DA80795BD1D8C9825D0D0000A028E1100000A028
          FB365CF0EAE010056A25BF29270A0FCA44C0B63045C76BB9D2056C4D3B36A5E9
          4FBF2A7C9D80D6B2D38E4AC39F5DB6A890AD290FDAFAF6DE9E86FBD7A7C9D1CD
          E17B12C04C8D9D2CD8D50000008A120E0100008A323170C395D1210AD449FE98
          B72834281301DBFC75235ECB9536606BCA6F647BDBF1E16B06B43675CEA31B11
          5BFEFB38FA3DBD107B76AE4E07F7AF4B23831BD2D4D8D6F03D0AA0B19305BB1A
          00004051C22100004011B65DFFF4DF4B1E1F4A0F981EDFDAB815278A0BCA42C0
          363FDD8AD772A50FD872F96D6CB7FF2865671C1DBE7E406B137FF7ECB6DCC616
          D9B7EBB634D077471A1BDAE871A3C0E1563476B3606703000028423804000028
          42DFE68B4F0D0E4FA096F6EF5E13C6046521609BBB6EC66BB94A046C4DF96D6C
          57BF277C1D81592C5B92462EFDAB8E856CB93CACDEBF674D1A3C78671A1F79E0
          6775227EFF027A4363370B7636000080228443000080224C0EDDF2ADE8F004EA
          68F0C09D6140501602B6B9E976BC96AB54C0D694876C1FFA93F035055ACB6F31
          1CBEECB4B63E56B4953C683BB06F6D1AEEBF2B4D8E6E09DFCB80FA6AEC66C1CE
          060000508470080000D069DB7EF607FF33797C283D647C7853180C948580EDC8
          CA10AFE52A19B035DDBB2E656F7E4CF8FA02AD4D9DFBF8C66D6C45846C4D7B77
          AE4EFDFBD7A5D1C10D8D476147EF6D40ADAC68EC68C1EE060000D069E1100000
          A0D3F66F7CCFEB824313A8AD6C625BE3769B2812280301DBECCA12AFE52A1DB0
          E5766C4AD3377D3DA5539786AF35D0DAC4452774F4B1A2B3D9B7FBB63470E08E
          3436BCB1F19E16BDD701D5D6D8D182DD0D0000A0D3C221000040A74D0CFCE2AA
          34794F825ED2B7776DDA757F1EB195CF9E8DD785B104E58AD772950FD89AF290
          EDAB6F0E5F736016CB97A4914FBC260D5D7176F8FBBC08BB77AC4E7D7BD6A6A1
          8377A58991FC71A3F1FB1E502D8D1D2DD8DD0000003A2D1C02000074D2D6154F
          FAED3479CF8A990726507743FD778521401908D862658BD772B509D89AF290ED
          7D7F18BEFE406BD3671E73E8B1A20FFCFE8E7EAF1769CFCED5E9C0BE75697860
          439A1CCB1F371ABF0F02A5B7A2B1AB053B1C0000402785430000804EDAB7E182
          57078725507B13A35BC283FF3210B03D5419E3B55CED02B6A6ADAB5276F623C3
          AF05D0DAE4DB9FD4B88DAD0C215BD3DE5DB7A5FEBE3BD3E8E0C6343D9E3F6E34
          7E5F04CAA7B1AB053B1C0000402785430000804E1A3F78DD97A2C312E8057B76
          DE161EF6779B80EDC1CA1AAFE56A1BB0E5F2DBD87EF2F9C62312A3AF0BD0DAD8
          07FEB4AB8F159DCDFEDDB7A7C103EBD3F8F0E6944DDC1DBE3F02E5D0D8D5821D
          0E0000A093C221000040A76CB9EEF1FF2D797C283DECE0BE3BC2C3FD6E13B0FD
          4699E3B55CAD03B6A63C64BBFC6FC3AF0FD05A76EAD234F2E9D7973664CBEDDE
          B13AF5ED5DDB78ACF6C4C896F0BD12E8AA158D9D2DD8E50000003A251C020000
          74CA9E3BDFFAB2E090047AC6C8C0C6F040BFDB046C87943D5ECBF544C0D6B463
          53CA2E784AF8B5025A9B3EEB91A57BAC682BF9CDA479DC3D32B0214D8D79DC28
          944163670B76390000804E09870000009D32D6F7E32BA24312E815F9E17C7480
          DF6D02B66AC46BB99E0AD872F96D6C77FE3465671E137EDD80D626DEF5F452DF
          C616D9BB6B4DEAEFBB338D0D3DF0B33FEE71A3D00D8D9D2DD8E50000003A251C
          020000748CC78742E3703E3AB4EFA65E0FD8AA12AFE57A2E606BCA6F63FBC1A5
          292D8BBF86406BA31F7969E542B65CFEB8D1FD7B6E4F8307D7A7F1E1CD299B10
          B441415684BB1C0000408784430000804ED875FBA92F681C86C48724D03306FA
          EE0C0FEABBA99703B62AC56BB99E0DD89AF21BD93EF157E1D712682D3BFDA834
          7CD9699578AC682B79D07660EFBA34DCBF214D8E6E0DDF6381B658D1D8DD829D
          0E0000A013C221000040270CEDBAF203C1E108F49CFCB168D1C17C37F56AC056
          B5782DD7F3015B537E23DB794F08BFAE406B53E73CA6711B5B9543B6A63D3B6F
          4B07F7DF91460637361ED11DBDE7020BD3D8DD829D0E0000A013C22100004027
          64E39BAF8D0E47A0D74C8FDFDDB845263A8CEF965E0CD8AA18AFE5046C87C96F
          63BBF57B293BFDE8F06B0CB436F1DEE756F2B1A2B3D9B77B4D1A3870672314F7
          B851589CC6EE16EC740000009D100E010000DA6DFB2D271D9F3C3E147E6DFF9E
          DBC3C3F76EE9B580ADAAF15A4EC016C86F63FBE685E1D71A98C5B22569E4E3AF
          AA5DC896CB43F1BE3D6BD3D0C1BBD2C4C896944DC4EFC7404B2B1A3B5CB0DB01
          0000B45B3804000068B783775F7A4E7028023D6BF0E0FAF0C0BD5B7A2960AB72
          BC9613B0CD220FD93EF8A2F0EB0EB4969DF1F034FCF9336AF158D156F2A0EDC0
          BE75697860439A1CDD1ABE37030FD6D8E182DD0E0000A0DDC221000040BB4D0E
          DFFA9DE850047AD5F8F0E6F080BD5B7A2560AB7ABC9613B0CDC1B6D5293BE7D1
          E1F700D0DAE479C7376E63AB73C8D6B477D76DA97FFF1D697470639A1EDF16BE
          5743AF6BEC70C16E070000D06EE1100000A09DB6FDEC0FFE67F2F85078906CE2
          EEC66D30D1A17A37F442C05687782D27609BA31D9BD2F44D5F4FE9D4A5E1F703
          D0DAF8C57F5CCBC78ACE66DFEEDBD3C081F5696C7853E33D3A7AEF861EB4A2B1
          CB053B1E0000403B854300008076DA7BD7F9AF0C0E43A0E71DD8BB2E3C44EF86
          BA076C7589D77202B679CA43B67F3D2BFCBE0066B17C491AF9D4DFF45CC896CB
          03F3BEBD6BD3D0C1BBD2C4C896F03D1C7A4563970B763C000080760A87000000
          ED34DAF7C3CBA3C310E875C3FD1BC283F36EA873C056A7782D27605BA01D9B52
          F6DE6787DF23406BD36F7C441AFEFC193DF158D156F6ECBC2D1DD8B72E0D0F6C
          4853635BC3F774A8ABC62E17EC78000000ED140E010000DACAE343213439BA35
          3C28EF863A066CCD70AD4EF15A4EC0B6481B6F4AD959C786DF33406B93EFF8FD
          C66D6CBD1CB235EDDDB526F5F7DD99460737A6E9F16DE17B3CD4C88A70C70300
          0068A370080000D02E3B6EFDEB67370E3DE2C310E879F9AD2ED1E178D1EA16B0
          D5315C6B12B0B5417E1BDBB597A5B42CFEFE015A1BFBD0493DF958D156F2C78D
          EEDF7D7B1A3CB03E8D0F6F4ED9C4DDE1FB3D54D88AC64E17EC7A000000ED120E
          010000DAA5FFDECFBE333804017EE5E0FE3BC203F1A2D52960AB73BC9613B0B5
          D18E4D69FA73AF0BBF8F80D6B25397A6914FBF5EC816C883B6BEBD6BD350FF5D
          6962744BF8DE0F55D3D8E9825D0F0000A05DC221000040BB4C8DDCF6DDE81004
          386464706378005EB4BA046C758FD77202B60EC86F643BFF49E1F714D0DAF459
          8FF458D123C86F5A3DB8EF8E3432B0314D8D79DC28D5D4D8E9825D0F0000A05D
          C2210000403B6C5DF1A4DF4E1E1F0AB39A1EDF161E7817AD0E015B2FC46B3901
          5B87E4B7B1DDF9D3949DF9F0F0FB0B686DE2C267B88D6D8EF6EE5A9306FAEE4C
          63430FFCCE19F7B8512A634563B70B763E000080760887000000EDB07BDDD92F
          4993F7266076FB76DF9E76DDBFBAABF66CFCE94382842AE995782D2760EBB0FC
          36B6EF7D28FC3E03663772E95FFD2A648BDF6B78B0DD3B6E4BFBF7AC4D8307EF
          4AE3C35B5236918742F1DF0AD06D8DDD2ED8F9000000DA211C020000B4C3F0AE
          AF7F383AFC001E6CE0C0FAF060BB48550ED87A295ECB09D80A92876C979E1C7E
          CF01AD65A71F95863F7F46E37D257ABFA1B53C683BB0F78E34DCBF314D8EE68F
          1B8DFF6E806E68EC76C1CE070000D00EE1100000A01DB2F12DD746871FC0838D
          0D6D0E0FB28B54D580ADD7E2B59C80AD60DBD7A7ECAD8F0BBFFF80D6A6CE7D5C
          E3363621DBC2EDD9B9261DDC7F671A19DC94A6C6F2C78DC67F4740111ABB5DB0
          F3010000B4433804000058AC7B6F3CF1D83479EFF5330F3E8087CA1F1996DFBA
          121D5E17A58A015B2FC66B39015B17ECD894A67FF99D944E3F2AFC5E045A9BB8
          E8048F156D93FC91E3F9ADAD79F8EE71A374C1F58D1D2FD8FD000000162B1C02
          00002CD6FE4D17BD3E38F4005AE8DBB32E3CAC2E4AD502B65E8DD77202B62ECA
          1F2B7AD5F9E1F724308B654BD2C827FE5AC8D64679F89EFFED307470439A18D9
          1AFE6D01EDD6D8F182DD0F000060B1C2210000C0628DF5FDF88AE8D00388E507
          D0D1017551AA14B0F572BC9613B095407E23DB3F3C2FFCFE045ACBCE7C781AFE
          FC191E2BDA0179D07660DF1D697860639A1CDD16FEAD018BD5D8F182DD0F0000
          60B1C2210000C0A2797C28CC4B7E7B4A74205D94AA046CBD1EAFE5046C25B26D
          75CADE745CF8BD0AB436F9B627366E6313B275CEDE5D6B52FFFE3BD3E8E0A634
          3D7E77F8B7072CC0F5E1EE070000B048E11000006031EE5BF9174F6E1C6EC487
          1E400BF9ED29D1217411AA10B089D70E11B0954C7E1BDBCFBF92D2F2A5E1F72D
          D0DAD8252FF258D182ECDBBD360D1CB82B8D0D6F49D9C43DE1DF213007D73776
          BD6007040000588C70080000B0187D5B3FF4C6E0B0033882FCD15FD1A17311CA
          1EB089D77E43C0565279C8F6C533C2EF5FA0B56CF9D234F2E9D70BD90A9407F3
          7D7BD7A5A1FE0D8D1B60A3BF49A095C6AE17EC800000008B110E0100001663EC
          C0B55F880E3B80D90D0F6C0C0F9A8B50E6804DBCF66002B692DBB129657FF78C
          F07B19686DFA8DC77AAC6897ECD9B9261DDC77471A79E0EF90A9B16DE1DF28D0
          D4D8F5821D1000006031C2210000C0A2787C282CC8E4E8B6F060B908650DD8C4
          6B0F2560AB888D37A5EC8D8F08BFAF81D626DFF114B7B175D9DE5DB7A7FEBEF5
          697468739A1EF7B8511EE2FA7007040000588470080000B050DB6F79C9F18D43
          8DF8B0033882BDBBD68487C99D56C6804DBC1613B055487E1BDB8F3F95D2B2F8
          7B1C686DF41F4F16B29540FEB8D1FD7BD6A6C10377A5F1E12D299B10B471EFF5
          8D9D2FD805010000162A1C0200002CD4FE4DEF5B161C720073D4DF77677880DC
          69650BD8C46BAD09D82A68C7A634FD99BF0EBFD781D6B2D38E4AC3972D17B295
          481EB4F5ED5D9786FB37A4C9D1ADE1DF32D45F63E70B7641000080850A870000
          000B35DAF7C3CBA3430E606E4607378507C69D56A6804DBC363B015B85E537B2
          BDFD89E1F73DD0DAF49B1ED588D8F2F7AAE83D8CEED9B3734D3AB8FFCE34F2C0
          DF2F536377877FDB503F8D9D2FD805010000162A1C0200002CD8E43D2BA2430E
          606EA6C7EF0E0F883BAD2C019B78EDC8046C1597DFC676FB8F5276C6D1E1CF00
          D0DAC4DF3DC76D6C25B76FD7ED69A06F7D1A1BDAFCC0DF341E375A5F0FEC7CD1
          2E080000B040E11000006021EEBDE9858F4E93F75EFFD0030E603EF6ED5E1B1E
          0A7752190236F1DADC08D86A22BF8DEDDF2E0A7F1680592C5B92463EF672215B
          05E48F1BDDBF676D1A3C78571A1FD992B289F8EF1E2AE9FAC6EE17EC84000000
          0B110E0100001662FFC6F7BC2E38DC00E669E0C05DE1417027753B6013AFCD9D
          80AD66F290ED2327853F17406BF92D86C39F3FC363452B240FDA0EECBB230DF7
          6F4C93A3DBC2BF81A88EC6EE17EC840000000B110E010000166264EF773F191D
          6E00F33336BC253CF8EDA46E066CE2B5F911B0D5D4F6F5297BCB63C39F11A0B5
          A9731FDFB88D4DC8563D7B77AE49FDFBEF4CA3839B1A8F508FFE26A2BC1ABB5F
          B0130200002C443804000058886C62DB7569727B0216277FC4D6EE1D6BD2AEFB
          6F2BCC9E4D3F0BC3804E13AFCD9F80ADC6766C4AD3B75C9DD26947853F2F406B
          E3EF7B7E1AFAC29BC3F738AA217F847A7E0BEDD8F0D6F0EF23CAA5B1FB053B21
          0000C0428443000080F9DA76FD331E9626B75F3FF360035898BEBD778487BB9D
          D28D804DBCB63002B61E903F56F4CAF3C29F1B6016CB97A4914FBC46C8566179
          C03F3AB839FCDB88D2B9BEB10306BB210000C07C8543000080F9DABDF60D2F0E
          0E3580051AEADF181EEC764AD1019B786DE1046C3D24BF91EDE213C29F21A0B5
          E9338F39F458D107DEDBA2F73CCA49BC563D8D1D30D80D010000E62B1C020000
          CC57FFF6CFBD2B3AD400166662645B78B8DB2945066CE2B5C511B0F5A0ADAB52
          76F623C39F27A0B5C9B73FA9711B9B90ADFCC46BD5D4D80183DD10000060BEC2
          210000C07C4D0CDEF2ADE8500358B83D3B6F0F0F793BA1A8804DBCB67802B61E
          95DFC6F6D32B525ABE34FCD9025A1BFBC09F7AAC688989D7AAABB10306BB2100
          00C07C8543000080799BDC7EFDCC030D60710EEEBB333CE8ED84220236F15A7B
          08D87A5C1EB2FDCB69E1CF18D05A76EAD234FC996542B69211AF55DEF5E16E08
          0000304FE1100000603EB6DF7CD2F18DC38BF8500358A091814DE1616F27743A
          6013AFB58F808D861D9B5276E153C39F37A0B5E9B31EE9B1A225215EAB85EB1B
          BB60B023020000CC4738040000988F7D1BDFFDDA34B53D01ED35357E77DAB5E3
          B642ECD9DCB9804DBCD65E02361EE4CE15297BC331E1CF1ED0DAC4854F3F741B
          5BF09E48E7EDDEB9268D0E6D0EFFFEA15A1ABB60B023020000CC473804000098
          8FE13D577F2C3ACC00166FEFEEB5E1C16FBB752A6013AFB59F808D87C86F63FB
          E1C7525A16FF1C02AD8D7EE4A542B68289D7EAA5B10B063B220000C07C844300
          0080F9981EDF784D7498012C5EFF81BBC2C3DF766B77C0D60CD7C46BED2760A3
          A51D9BD2F4275E1EFE4C02AD65A71F95863F7F8690AD00E2B5FA69EC82C18E08
          0000301FE110000060AEB6FEF489FF234D6DBF7EE64106D01EA3C35BC203E076
          6B67C0265CEB2C015B31A6C7C6D2E41D7786FF5BE9E537B29DF784F0E713686D
          EA9CC73422B6FC3D317AAF6471C46BB5757D63270C7645000080B90A87000000
          73B563F56B9F1B1C62006D323D716FE3C0373A086EA776056CE2B5CE13B01563
          BA7F20ED7FDAB3D3E0FB2E4953FBF685FF9F52CB6F635BFDFD949D7174F8B30A
          B436F1DEE7BA8DADCDC46BF5D6D809835D11000060AEC2210000C05CF56DFDE0
          99D12106D03EFBF7AE0B0F83DBA91D019B78AD1802B6623402B6A73EABA1EF84
          3F4E23FFFA95948D8F87FFDF52CB6F63BBFA3DE1CF2C308B654BD2C8C75F2564
          6B03F15AFD3576C2605704000098AB70080000305723FBFECFA7A3430CA07D06
          0F6E080F84DB69B1019B78AD3802B6621C1EB0351DF88B97A7F19FAE08FFFFA5
          97876C1FFA93F0E717682D3BE3E169E88AB33D567481C46BBDA1B11306BB2200
          00C05C8543000080B99A1EDF784D748801B4CFF8C8D6F050B89D1613B089D78A
          25602B4614B0350D9CF5E634B97973F8CF95DEBDEB52F6E6C7843FCB406B93E7
          1DDFB88D4DC83677E2B5DED1D809835D11000060AEC2210000C05C6CF9C9637E
          2B4D6DBF7EE60106D05ED9E4BD8D43E0E870B85D161AB089D78A27602BC66C01
          5BC3D39F93863E7C699A3E7830FCE74B6DC7A6347DD3D7533A7569F8730DB436
          7EF11F7BACE81C88D77ACEF58DDD30D819010000E6221C020000CCC5FDBFFCAB
          A70687174007F4EDBB233C206E9785046CE2B5EE10B015E38801DBAFF49DF827
          69F4EBDF4CD9E464F8EF29B53C64FBCA39E1CF37308BE54BD2C8A75F27646B41
          BCD69B1ABB61B033020000CC4538040000988B7D1BDFF3BAE8F00268BFE1818D
          E12171BBCC376013AF758F80AD18730DD89A0EBEEA3569FCA69BC37F57E9E521
          DBDF3F37FC59075A9B7EC323D2D015677BACE861C46BBDABB11B063B230000C0
          5C8443000080B918DAFDF50F47871740FB4D8E6D0B0F8ADB653E019B78ADBB04
          6CC5986FC0D63470DE3B1EF891DD1EFE3B4B6FE34D293BEBD8F0E71E686DF21D
          BFDFB88DADD74336F15A6F6BEC86C1CE0800003017E1100000602EA646D77D3F
          3ABC003A63CFAEDBC303E376986BC0265EEB3E015B31161AB0353CEB8434FCC9
          CFA4E9A1A1F0DF5D6AF96D6C3FF97CE31189D1EF00A0B5B10FBEB8671F2B2A5E
          A3B11B063B230000C05C84430000803999DA7EFDCC830BA0730EF6AD0F0F8DDB
          612E019B78AD1C046CC55854C0F62B075E74521AFBEEF752363515FE374A2D0F
          D92EFFDBF07701D05A76EAD234FCD9653D15B289D7F895EBC39D110000600EC2
          210000C0916CBFF9C58F4F53F7DDF08004146364684BDAB5634D47ECD97C7D78
          10DF245E2B0F015B31DA11B0351D7CDDB23471DB9AF0BF537A3B36A5EC9DBF1F
          FE5E005A9B3EFBB834F4C5731BEFAFD1FB6E5D1C8AD7B6847FB7D0736E68EC88
          C1EE0800007024E1100000E048F6AE7FDBCB83430BA083A6C6EF0D0F8FDB61B6
          804DBC562E02B662B433606B1A7CF77BD3D4AE5DE17FAFD4F2DBD8EEFC69CACE
          7C78F83B02686DE2DDCF6C846CD17B6FD589D798A9B12306BB230000C0918443
          00008023E9BFEFF2774787164067EDDBBD2E3C445EAC56019B78AD7C046CC5E8
          44C096EB7BEEF3D3C8E5FF92A64747C3FF6EA9E5B7B17DEF43292D7BE8EF0A60
          76231F7B79AD4236F11A91C68E18EC8E00000047120E0100008E64ECE04FBE14
          1D5A009D357060437890BC5851C0265E2B27015B313A15B0351DF8F3BF4C633F
          BA26FC6F975E1EB2FDD35F3EE4770630BBECF4A3D2D0156757FEB1A2E2355A69
          EC88C1EE0800007024E1100000E048B2C9BB5744871640678D0D6F0D0F93176B
          66C0265E2B2F015B313A1DB035F59FF6863479D786F06328BDEDEB5376DE131E
          F4BB0338B2A9731FD7B88DAD8A219B788DD93476C46077040000389270080000
          309B2D3F79EC6FA5A9FB6E9879600174DEF4C4F6C6E17174A8BC1887076CE2B5
          7213B015A3A880ADE169CF4E43977C304DEFDB1F7E2CA5B663539ABEF57B299D
          7ED483021DE0C8262E3AA1528F1515AF3107373476C56087040000984D380400
          0098CDF65B4E3A3E38AC000AB27FEF1DE1C1F262340336F15AF909D88A5168C0
          F62B7D7FF48234F295AFA56C7C3CFC984A2D7FACE8372F7C48A0031CC1B22569
          E493AF297DC8265E63AE1ABB62B043020000CC261C020000CC66CFFAF35E1E1D
          5600C518ECDF181E2E2F461EB089D7AA41C0568C6E046C4D075FF6AA34FEB3EB
          C38FABF4F290EDFD27C6A10ED05276E6C3D3D0156797F2B1A2E235E6A3B12B06
          3B240000C06CC2210000C06CFAB75FF6CEE8B00228C6F8E8B6F080793176AEFA
          AE78AD22046CC5E866C0D63470CEB96972CBD6F0E32BBD6DAB5376CEA3C25007
          686DF26D4F6CDCC65696904DBCC67C3576C56087040000984D3804000098CDD8
          81FFFC4274580114239BBC2FEDDE797B78D0BC5079C016C552948F80AD186508
          D81A9EF1DC34F4D17F4AD307FBC38FB3D4766C4AD33FFF4A4AA72E0D431DA0B5
          B14B5ED4F5C78A8AD75888C6AE18EC90000000B30987000000B3C926EF5E111D
          5600C539B0FFCEF0B079A1046CD521602B466902B65FE97BC19FA5D16F7D3B65
          5353E1C75B6A79C8F6AF6785910E308BE54BD3F0679775256413AFB1508D5D31
          D82101000066130E0100005AD9FC9347FFD73475DF0D330F2A80620D0F6C0E0F
          9C17E4BED569ED7BFE328CA5281F015B31CA16B0351D3CE5B56962E52FC38FB9
          F4766C4AD97B9E15873A404BD36F3C360D7DE1CD853D5654BCC622DDD0D81983
          5D120000A09570080000D0CAF65B4E3A3E38A4000A3639764F78E8BC20F7AD0E
          4329CA49C0568CB2066C4D03E75FF0C0AF82FBC28FBDF436DE94B2B38E0D431D
          A0B5C9773EA5711B5B274336F11AEDD0D819835D120000A09570080000D0CAEE
          3BDE7472744801146FEFAEB5E1E1F3BC09D82A45C0568CB2076C0DCF3E210D7F
          E6B2343D341C7E0EA596DFC676ED65292D8B431DA0B5D17F3CB9238F1515AFD1
          2E8D9D31D8250100005A0987000000AD1CD8F6D173A2430AA078FD7D778507D0
          F32660AB14015B312A11B0FDCA813FFBF334F6BDEFA76C7A3AFC5C4A6DC7A634
          7DD96BC34807682D3B75691AFEFC196D0BD9C46BB45363670C764900008056C2
          210000402BC3BBBFF5D1E89002285E7ED01C1D42CF9B80AD52046CC5A852C0D6
          D4FFFA53D3C49ADBC3CFA7F4F21BD9CE7F5218EA00AD4DBFE9518B7EACA8788D
          766BEC8CC12E090000D04A3804000068656268E5B7A2430AA078D313F78607D1
          F32660AB14015B31AA18B0350DBEE77D696AF79EF0F32AB5FC36B6DB7F9CB233
          1F1E863A406B137FF79C05DDC6265EA3131A3B63B04B020000B4120E0100005A
          9ADA7E7D74480174C7BE3DEBC203E97911B0558A80AD18550ED8727D7F78621A
          F9972FA5E9B1B1F0F32BB5FC36B67FBF388C7480D98D7CFC55730ED9C46B74CE
          033B63B44B020000B4100E010000225B7EF2D8DF4A53F7DDF0D0030AA05B060E
          6E080FA5E745C0562902B662543D606B3A70F2CBD2D87F5E1B7E8EA597876C97
          9E1C463A406BD9E947A7A12BCE9EF5B1A2E2353AEC86C6EE18EC940000009170
          08000010D97EF39F3D3E389C00BA686C786B78303D2F02B64A11B015A32E015B
          53FFE96F4C931B36869F6BE96D5F9FB2731F17863A406B530FFCDCE4B7B1CD0C
          D9C46B14A1B13B063B25000040241C0200004476AD3DE34FA3C309A07BB2C9ED
          8D83E8C30FA6E74DC0562902B662D42D606B78FA73D2D007FE314DEFEF0B3FE7
          52DBB1294DFFF23B299D765418EA00AD8DBFEFF9BF7EACA8788DA23476C760A7
          04000088844300008048DF960F9C191D4E00DDD5B7F78E874669F32160AB1401
          5B316A19B0FD4ADFF35F9846BF7655CA2626C2CFBDD4F2C78A5E757E18E900B3
          58BE248D7EF2D569F25BEF09FF9680766BEC8EC14E09000010098700000091A1
          5D5FFF709ABA3F01E532D4BF29EDDA71FBC2DD775B184A514E02B662D439606B
          3AF8F253D2F8CF7F117EFEA597DFC8F60FCF8B431DA0A5ECEC47A6F4FD0FA574
          605DF83705B44B63770C764A0000804838040000884C0CFCE2AAE87002E8AE89
          D17BE2306DAE046C9522602B462F046C4D036F3E2F4DDE7D77F83A94DEB6D529
          7BD37161A803B496BDE79929FDF06342363AA6B13B063B25000040241C020000
          8426EFBD3E3A9C00BA6FCFAEB5719C361702B64A11B015A39702B68667FE611A
          FED827D2F4C040F87A945A7E1BDBCFBF9CD2F2A561A803B4965DF69A43215BF0
          B7052C4ABE3B463B25000040201C02000084A6EEBFE121071340291CD87F571C
          A7CD8580AD52046CC5E8B980ED57FA5EF86769F4EA7F4BD9D454F8BA945A1EB2
          7DF18C30D2016671DA5129BBFA42211BED7643B85302000004C2210000C04CDB
          AE7FDAEF350E21E2C309A0CB8607B7C471DA5C08D82A45C0568C5E0DD89A0EBE
          FA7569E297B786AF4DE9EDD894B2BF7B461CEA002D656F794C4A3FF8A8C78AD2
          2E373476C860B704000098291C020000CC74DFCABF787270280194C4E4D8BD71
          9C361702B64A11B015A3D703B6A6C10BDE9DA6EEBB3F7C8D4A6FE34D297BC331
          61A803B496FDC31FB98D8DB668EC90C16E0900003053380400009869F7BAB35E
          121D4A00E5B177F7BA38503B12015BA508D88A2160FB8DBE67FF511ABEECF234
          3D3C1CBE56A596DFC6F6E34FA5B42C0E7580D6B22B960BD95894C60E19EC9600
          000033854300008099F66FFE8753A34309A03CFA0F6C8803B52311B0558A80AD
          1802B6873AF0E293D3D87FFC3065D3D3E16B566A3B36A5E94FBF2A8C7480599C
          71744AFFFE3E8F1565411A3B64B05B020000CC140E010000661AB8FF0B174587
          1240798C0E6D8D03B52311B0558A80AD1802B6D6FA979D9626D6DD11BE6EA597
          DFC8F6F627C6A10ED05276DE130EDDC62664631E1A3B64B05B020000CC140E01
          0000661ADDFF1F9F8B0E2580F2989ED81E076A472260AB14015B31046C47F0B4
          67A7C18B2E4E537BF786AF5FA9E5B7B1DDFEA394E5374B05A10ED05AF691177B
          AC2873D6D82183DD12000060A67008000030D3E4C89AEF46871240B9ECDB7347
          1CA9CD46C0562902B66208D8E6A6EF8413D3C817FF356563E3E1EB586AF96D6C
          FF765118E900B358B624655F7993908D236AEC90C16E09000030533804000078
          88A9FB6F9879200194CFC0C18D71A4361B015BA508D88A21609B9F032F7D791A
          FFC94FC3D7B2F4F290EDC32F8E431DA0A5EC8D8F48E97B1FF05851667343B85B
          020000CC100E0100001E42C00695303E72771CA9CD46C0562902B66208D816A6
          FF8D6F4A931B3785AF69E96D5F9FB2373F260C7580D6B20B9E72E83636211B0F
          2560030000E6241C0200001CEEEE1B9EBBB471F8101F4A0025924DDE9776EF0C
          22B5D908D82A45C0560C01DB223CFD3969E8431F49D3070F86AF6DA9EDD894A6
          6FB93AA5539786A10E308B8FBFCC634599E986C62E19EC98000000870B870000
          0087BBEF977FF994E0300228A9BE7DEBE350AD15015BA508D88A21605BBCBE13
          5F9446AFFA46CA2627C3D7B8D4F290EDCAB7C6910ED0DAF2A529FBFAF942367E
          ADB14B063B260000C0E1C2210000C0E176DD7EFA8BA2C308A09C860636C7A15A
          2B02B64A11B01543C0D63E075FF9EA34FE8B1BC3D7B9F4F290EDE213E2500768
          297BD37129FDE0A31E2B4A6AEC92C18E09000070B87008000070B87D1B2E7C6D
          74180194D3C4E83D71A8D68A80AD52046CC510B0B5DFC05BDF9EA6EEB9377CBD
          4B6FEBAA949DFDC830D4015ACBDEFB2CB7B1F5B8C62E19EC98000000870B8700
          0000873BB0EDA3E74487114079EDD9B5368ED52202B64A11B01543C0D621CFFC
          C334FC894FA7E9C1C1F0752FB5FC36B69F5E91D2F238D4015ACB3EF75A215B8F
          6AEC92C18E09000070B87008000070B8C19D5FBD244DED4840751CDCBF21EDDA
          91476C73138552949380AD1802B6CEEA7BD18BD3E8BFFF9F944D4D85AF7FA9E5
          21DB15CBC3480798C5E947A5ECDFDE93D2359F0CFF76A19E1ABB64B063020000
          1C2E1C0200001C6E64FF7F7C2E3A8C00CA6B64706B18AAB5128552949380AD18
          02B6621C7CEDEBD3C4ADABC3AF41E9EDD894B20B9E12873A404BD9B98F3D14B1
          1D5C1FFE0D43BD3476C960C7040000385C3804000038DCC4E0CA6F4587114079
          4D8D6F0F43B556A2508A7212B01543C056ACC177BD274DEDDC197E2D4AEFCE15
          293BF39830D4015ACB3EF802B7B1F580C62E19EC98000000870B8700000087CB
          26EE5E111D4600E5B677F71D61AC16894229CA49C0560C015BF1FA9EF3BC34FC
          F97F4ED3A3A3E1D7A4D4F2DBD87E70694ACBE2500768E1819F99EC4B670AD96A
          ACB14B063B260000C0E1C2210000C0834CEDB871E64104507EFD073685B15A24
          0AA52827015B31046CDD73E0A4BF48633FFC51CAA6A7C3AF4DA9EDD894A63FF1
          F238D4015A3BF3E1297DF7628F15ADA71BC31D130000E030E1100000A069F3B5
          C7FD97C6A1437C180194D8E8F0DD61AC16894229CA49C0560C015BF7F52F3F23
          4DDEB93EFCFA945E7E23DB794F88431DA0A5ECEDFFEBD06D6C42B63AB9B1B153
          06BB26000040533804000068DA76FD331E161C420015303D717FDABD330ED666
          8A4229CA49C0560C015B493CEDD969E8E2F7A7A97DFBC2AF53A9E5B7B1ADFE7E
          CA4E3F3A0C7580D6B24B4FF658D11A69EC94C1AE090000D0140E0100009AB6DF
          F292E3A34308A01AF6EF5D1F066B3345A114E524602B8680AD5CFA4EF8E334F2
          AF5F4DD9F878F8F52AB5FC36B6ABDF13463AC02C962F49D995E70AD96AA0B153
          06BB26000040533804000068DA71EBAB9F1D1D4200D530D8BF390CD6668A4229
          CA49C0560C015B391DF8CB57A4F19FAE08BF66A597876C1FFA9338D4015ACACE
          3A36A51F7CD463452BACB15306BB26000040533804000068DAB5F60D2F8E0E21
          806A181FB9270CD6668A4229CA49C0560C015BB90D9CFD9634B9794BF8B52BBD
          7BD7A5EC9C4787A10ED05AF6AE3F38741B9B90AD721A3B65B06B020000348543
          000080A6BD77BDE394E81002A8866CF2FEB47BE7BA305A3B5C144A514E02B662
          08D82AE0E9CF49431FBE344D1FEC0FBF86A5B663539ABEE9EB299DBA340C7580
          D6B24FBDD263452BA6B15306BB26000040533804000068DABFF9FDA747871040
          751CD8775718AD1D2E0AA52827015B31046CD5D177E29FA4D16F7C2B659393E1
          D7B2D4F290ED2BE784910E308B5397A66CE7CFC3BF7B289FC64E19EC9A000000
          4DE1100000A0E9E03D9F3E3F3A8400AA6378604B18AD1D2E0AA52827015B3104
          6CD573F055AF491337DD127E3D4B2F0FD9FEFEB971A80384A6377E2BFCBB87F2
          69EC94C1AE090000D0140E0100009A06777CF9E2E81002A88EC9B17BC368ED70
          5128453909D88A2160ABAE81B7BD234D6DDF1E7E5D4B6FE34D293BEBD830D601
          0EC9DEF507871E217A707DF8770FE5D3D829835D130000A0291C020000340DEF
          FDF74F46871040B5ECD9B52E0CD79AA2508A7212B01543C05671CF3A210D7FEA
          B3697A6828FCFA965A7E1BDB4F3E9FD2F238DE815ED5883B7FF051E15A053576
          CA60D7040000680A870000004D63077EF2A5E81002A896837D1BC270AD290AA5
          2827015B31046CF570E04F5E92C6BEFBBD944D4F875FE752CB43B6CB5F1F863C
          D053962F49D995E71EBA752DF81B87F26BEC94C1AE090000D0140E0100009A26
          876EFD4E74080154CBC8D0B6305C6B8A4229CA49C0560C015BBDF4BF6E599AB8
          EDF6F06B5D7A3B36A5EC9D4F8EC31EA8B9ECD293856B35D0D829835D130000A0
          291C020000344D8F6FBE363A8400AA656AFCBE305C6B8A4229CA49C0560C015B
          BD1C7CF5EBD2C4CA5F865FEBD2CB03B6F73C338C7BA0AEB2B7FFAF43E19AC785
          D64263A70C764D000080A670080000F06B533B6E9C79000154D3BE3D7786F15A
          2E0AA52827015B31046CF5D0F7823F4BA357FF5BCAA6A6C2AF73A9E58F10FDD7
          B3C2B8076AEBCC87A7F4DD8B856BF57363B86B020000FC4A38040000F835011B
          D4C6C0814D61BC968B4229CA49C0560C015BC53DE3B969E8D27F6A7C1DA3AF6F
          A9E5E1DACFBF92D2F2A571E00375B46C49CABE74A6C785D697800D0000985538
          040000F835011BD4C6D8F0DD61BC968B4229CA49C0560C015B750D9C736E9ADC
          7677F8752DBD6DAB5376CEA3E2C0076A2AFBE00B846BF5276003000066150E01
          00007E4DC006B5914DDE9F76EF14B0559D80AD1802B6EA39F8B257A5F19FDF10
          7E3D4B2FBF75EDFD2786710FD45576EE630F856B1E17DA0B046C0000C0ACC221
          0000C0AF4DEDBCF10109A887FD7BEF4ABB76AC7B882894A29C046CC510B05547
          DFF35E9846BE7A65CA2626C2AF65A9EDD894B2ABCE0FE31EA8ADD38F4AD9B7DF
          F3AB5BD7E2BF57A81D011B000030AB70080000F06B0236A895C1FE2D02B68A13
          B01543C056014F7F4E1A7AFF87D2F4BEFDE1D7B0D4F21BD77EF99D46C813063E
          5053D9E5AF17AEF526011B000030AB70080000F06B0236A89589D17B056C1527
          602B8680ADDCFA4F7F439ABC6B43F8B52BBDEDEBD3F45B1F1FC63D5057D945CF
          11AEF536011B000030AB70080000F06B0236A8956C7267DABDF30E015B8509D8
          8A21602BA7037FFE9769ECC7FF197ECD4A6FC7A6B4F6D58F08E31EA8ABEC4DC7
          1D0AD70EDE15FE5D42CF10B0010000B30A87000000BF266083DA39B07F8380AD
          C2046CC510B0954BDF739F9F46AEF8429A1E1D0DBF5E6595FFBCE69A3FBFABFF
          FC77C3C8076AE7D4A529FBE6056E5DA349C0060000CC2A1C020000E4365F7BDC
          7F691C36C4871040450D0F6E13B0559880AD1802B6F218FCBB8BD2D4AEDDE1D7
          A9CC1AE1DA49BFFBA09F5F011BBD20FBD42B846BCC746363B70C764E0000805C
          38040000C86DF9C9637E2B387C002A6E72EC3E015B8509D88A2160EBBEFED72D
          4B13B7DD1E7E7DCA6CE6AD6B8713B05167D9054FF1B8505A6AEC96C1CE090000
          900B87000000B92DD73DFEBF45870F40F5EDDD75A780ADA2046CC510B075CF81
          3FFDF334F6DDEFA56C7A3AFCDA94D56CE15A93808D3ACADEF88894FEEF87846B
          CCAAB15B063B270000402E1C020000E4B6AE78D26F47870F40F5F5F76D12B055
          9480AD1802B62E78F60969F8539F4DD34343E1D7A4CC8E14AE3509D8A895654B
          5276E5B91E17CA9C3476CB60E7040000C885430000809C800DEA6B74E86E015B
          4509D88A21602BD6C0DBDE91A6B66F0FBF166596FF3CDE74F2C3C29FD588808D
          BAC83EFA12E11AF3226003000066130E01000072F9214336B53301F53335717F
          DAB963DDAF45A105E524602B8680AD18075FF59A3471F32DE1D7A0CCF29FC35C
          F4333A1B011B5597BDEDF8945DF3C9941DBC2BFCFB025A11B0010000B3098700
          0000B9AD2B9EF23BD1E103500F7BF7AC17B0559080AD1802B6CEEA3BF14FD2E8
          37AF4ED9E464F8FA97D942C2B55C1EAF09D8A8AC338E4ED9F73E205C63C1F2DD
          32DA3901000072E110000020B7ED674FFBBDE8F001A887FE839B056C1524602B
          8680AD439EFE9C34F4E14BD3F4C1FEF0752FB385866B39F11A95B56C49CABE74
          C6A15BD782BF2560AEF2DD32DA3901000072E1100000202760837A1B1DB947C0
          564102B66208D8DA6FE0ECB7A4C9CD5BC2D7BBCCF29FB95B5E714CF8F33817E2
          35AA2A7BFF89C235DA46C0060000CC261C020000E4046C506FD3933BD2AE9D02
          B6AA11B01543C0D63E075EF6CA34FED39F85AF7399E53F6B8BB9752D275EA38A
          B2B73CE650B8E671A1B491800D0000984D38040000C809D8A0FEF6EFDB2060AB
          18015B31046C8BD777C21FA7917FFD6ACAC6C7C3D7B8CCDA15AE89D7A894D38E
          4AD9772E72EB1A1D216003000066130E01000072DBAE7FE6C3A2C307A03E06FB
          B70AD82A46C0560C01DB223CEDD969E8E2F7A7A97DFBC2D7B6CC161BAEE5846B
          545176F9FF16AED151F96E19ED9C000000B97008000090BBFBE7CF591A1D3E00
          F5313EBA5DC0563102B66208D816A6FFD433D3E49DEBC3D7B4CCF29F2BF11ABD
          287BEFB33C2E9442E4BB65B473020000E4C2210000404EC006BD61F7AE3BC210
          837212B01543C0363F074EFA8B34F6C31FA76C7A3A7C3DCBAA5DE15A4EBC4695
          64673F32653FFC98708DC208D8000080D984430000809C800D7A43DFFE8D618C
          413909D88A21609B9BBEE73C2F0D7FFE9FD3F4E868F83A9659BBC2B59C788DCA
          58BE34655F3FDFE342299C800D0000984D38040000C809D8A0370C0D6E0B830C
          CA49C0560C01DB910DBEEB3D696AE703BF4782D7AFCC1AE1DA49BF1BFE7C2D84
          788DAAC83EFE32E11A5D236003000066130E010000728702B65D09A8B7C9F1FB
          C328837212B01543C0D6DAC1BFF9DB34B1FAB6F0752BB3FC67A79DB7AEE5C46B
          5441F6CEDF4FD94F2E4BD9C0C6F0EF002882800D0000984D38040000C809D8A0
          77446106E524602B8680EDA1FA5EF4E234FAEFFFE781DF1953E16B5666ED0ED7
          72E235CA2E7BC33129FB8F8F0AD72805011B0000309B700800009013B041EF88
          E20CCA49C0560C01DB619EF98769F8139F4ED38383E16B55669D08D772E2354A
          6DD992945D79EEA15BD782F77CE806011B0000309B7008000090BBFBE7CFFE7F
          A3C307A07EA240837212B01543C076C8C05BCF4F53F7DC1BBE466596FF9CDCFC
          B2A3C29FA1C512AF5166D9475E2C5CA394F2DD32DA3901000072E110000020B7
          ED674FFBBDE8F001A89F28D2A09C046CC5E8F580EDE02B5F9DC67F7163F8DA94
          59FEF3D1A95BD772E235CA2A3BEF0987C2358F0BA5A4F2DD32DA3901000072E1
          10000020276083DE11851A949380AD18BD1AB0F59DF8A2347AD53752363919BE
          2E65D6C9702D275EA394CE383A65DFFB805BD7283D011B0000309B7008000090
          13B041EF88620DCA49C0568C9E0BD89EFE9C34F4A18FA4E98307C3D7A3CC3A1D
          AEE5C46B9451F6A533846B5486800D0000984D38040000C809D8A07744C106E5
          24602B462F056CFD6F7C539ADCB4397C1DCA2CFF595879CA71E1CF49BB34C335
          F11A65925DF23CE11A95236003000066130E01000072DB7EF6D4FF191D3E00F5
          13851B949380AD18BD10B01D78E9CBD3F84F7E1A7EFE6596FF0CB8758D5E94BD
          E53129BBE693291BD818BE974399E5BB65B473020000E4C2210000406EEB8A27
          FD7674F800D44F146F504E02B662D43960EB3BE1C434F2A52FA76C6C3CFCDCCB
          AC88702D275EA3544E3B2A65DFB9C8AD6B545ABE5B463B270000402E1C020000
          E4046CD03BA280837212B015A39601DBD39E9D06FFFE1FD2D4DEBDE1E75C6645
          856B39F11A65927DEEB5C2356A41C0060000CC261C020000E4046CD03BA28883
          7212B015A36E015BFFB2D3D2C4BA3BC2CFB5CCF2EF77F11ABD287BCF330F856B
          1E174A4D08D8000080D98443000080DC96EB8EFFEFD1E103503F51C8413909D8
          8A519780EDC08B4F4E63FFF1C3944D4F879F6759151DAEE5C46B944176D6B129
          BBE693C2356A27DF2DA39D13000020170E010000725BAE7BFC7F8B0E1F80FA89
          620ECA49C0568CAA076C7DCFFEA3347CD9E5697A6424FCFCCAACE8702D275EA3
          EB962F4DD955E77B5C28B595EF96D1CE090000900B87000000B9CDD73EEABF66
          53BB6E9E79F800D44F1474504E02B6625439601BBCE0DD69EABEFBC3CFABCCF2
          EFED9B4EFE7FC2EFFB4E12AFD16DD9C7FF52B846DDDD9CEF96D1CE090000900B
          870000004DF961C38CC307A086A2A8837212B015A38A01DBC157BF2E4DFCF2D6
          F0F329B3FC7BBA1BB7AEE5C46B7453F68E271D0AD73C2E94FABB39DA35010000
          9AC221000040537ED830E3F001A8A128ECA09C046CC5A852C0D6F7C23F4BA3DF
          FECE033FCB53E1E75266DD0AD772E235BAE6CC6352F61F1F15AED14B046C0000
          C0ACC221000040537ED830E3F001A8A128EEA09C046CC5A844C0F6CC3F4CC31F
          FB449A1E18083F8732EB66B89613AFD115CB96A4ECABE7785C28BD48C0060000
          CC2A1C020000346553BB6F7E4002EA2D0A3C2827015B31CA1EB00DBCF9BC3479
          F7DDE1C75E66F9F7EF2DAF3826FCDE2E8A788D6EC8FEF14F52F693CF85EFC1D0
          03046C0000C0ACC221000040537ED830E3F001A8A128F2A09C046CC5286BC076
          F0E5A7A4F19FFF22FC98CB2CFFBEEDF6AD6B39F11A45CBDEFAB843E1DAC0A6F0
          FD177A84800D000098553804000068CA0F1B661C3E003514851E949380AD1865
          0BD8FA9EFFC234FAB5AB523631117EBC6556A6704DBC46614E3F3A65FFF7436E
          5D8343046C0000C0ACC221000040537ED830E3F001A8A128F8A09C046CC5284D
          C0F6F4E7A4A10FFC639ADEDF177E9C655686702D275CA368D915CB856BF06002
          3600006056E1100000A0293F6C9871F800D450147D504E02B662942160EB3FE3
          AC34B96163F8F19559FE3DBAF294E3C2EFDFA289D72852F60F7F245C8398800D
          000098553804000068CA0F1B661C3E003514851F949380AD18DD0CD80E9CFCB2
          34F69FD7861F5799E5DF9B65B9752D275EA328D9398F4AD94F2E4BD9C0A6F03D
          1610B0010000B30B870000004DD393F7DD101C40003513C51F949380AD18DD08
          D8FAFEF0C434F22F5F4AD36363E1C75466650AD772E2350A71EAD2947DE722B7
          AEC111E43B65B46B020000348543000080A6C9913B7F101D4200F51205209493
          80AD1845076C83EF795F9ADABD27FC58CAAC6CE15A4EBC4611B2CF9C225C8339
          CA77CA68D7040000680A870000004D63FD375E151D4200F5124520949380AD18
          45056CFDAF3F354DDCBE36FC18CA2CFF3E14AFD18BB2773FFD50B8E671A13067
          F94E19ED9A0000004DE1100000A06964FF8F2E8F0E21807A894210CA49C0568C
          4E076C07FEECCFD3D8F7BE9FB2E9E9F0BF5F56650DD772E2353A293BEBD894FD
          F893C23558807CA78C764D000080A670080000D034B8EB1B1F8E0E21807A8962
          10CA49C0568C8E056CCF3E210D7FE6B2343D341CFE77CBACACE15A4EBC46C72C
          5F92B2ABCEF7B85058847CA78C764D000080A670080000D0D4BFFD9FDF1D1D42
          00F5120521949380AD189D08D806CEBF204DDD775FF8DF2BB3FC7BEEA6931F16
          7E3F9681788D4EC9FEE92F846BD006F94E19ED9A0000004DE1100000A0A96FEB
          A5E744871040BD445108E524602B463B03B683A7BC364DACFC65F8DF29B3FC7B
          ADCCB7AEE5C46B744276FE130F856B1E170A6D91EF94D1AE090000D0140E0100
          009AF66E7CEFEBA34308A05EA230847212B015A31D015BDF0BFE2C8D7EEBDB0F
          FC8C4D85FF8D322B7BB89613AFD16ED9990F4FD97F7C54B8066D96EF94D1AE09
          0000D0140E0100009A76DFF196BF880E21807A89E210CA49C0568C45056CCF78
          6E1AFAE83F35FE1DD1BFBBCCAA10AEE5C46BB4D5B22529FBF2591E170A1D92EF
          94D1AE090000D0140E0100009A76DCF6B72746871040BD448108E524602BC642
          03B68173CE4D935BB686FFCE32CBBFAF6E79C531E1F75C9934C335F11AED927D
          E845C235E8B07CA78C764D000080A670080000D074DF2F5FF694E81002A89728
          14A19C046CC5986FC076F065AF4AE33FBB3EFC779559FEFDE4D6357A5176EEE3
          0E856B1E170A1D97EF94D1AE090000D0140E0100009AEEF9C589C746871040BD
          44B108E524602BC65C03B6BE3F7A411AF9CAD752363E1EFE7BCAAC2AE15A4EBC
          46DB9C7E54CAFEFD62B7AE4181F29D32DA350100009AC221000040D396EB8EFF
          EFD9D4EE9B671E4200F5120523949380AD18470CD89EF6EC3474C907D3F4BEFD
          E13F5F66550AD772E235DA25BB62B9700D8A7773BE5346BB2600004053380400
          00385C7EE830E31002A899281AA19C046CC5982D60EB3FED0D69F2AE0DE13F57
          66F9F7CECA538E0BBFAFCA4ABC463B647FFF5C8F0B85EEB939DA310100000E17
          0E0100000E974DED12B041CD45E108E524602B4614B01DF8F3BF4C633FBA26FC
          FF9759FE3D53B55BD772E235162B7BD37129FBC965C235E8AA5D02360000E088
          C2210000C0E12647EEFC417C1801D445148F504E02B6621C1EB0F53DF7F969E4
          F27F49D3A3A3E1FFB7CCAA18AEE5C46B2CCAA94B5376F5851E170A2590EF92D1
          8E09000070B87008000070B8D183D77F359BDA9380FA8A0212CA49C0568C66C0
          36F8EEF7A6A95DBBC2FF4F9935C2B5937E37FC1E2A3BF11A8B917DE69494FDF4
          9FC3F73AA078F92E19ED98000000870B87000000871BDC75F5C7A2C308A03EA2
          88847212B015637A6C2C4DDCB626FCDFCA2CFFFEA8EAAD6B39F11A0B955DF8D4
          43E1DAE0B6F07D0EE88E7C978C764C000080C38543000080C31DBCF7F2774787
          11407D442109E524602352F5702D275E6321B2B38E4DD98F3F295C8392CA77C9
          68C7040000385C3804000038DCBE4D979C1E1D4600F511C5249493808D99AA1E
          AEE5C46BCCDBB22529BBEA7C8F0B8592CB77C968C7040000385C3804000038DC
          EE3BDEF217D16104501F5150423909D868CABF176E3AF961E1F7499588D798AF
          ECD293856B5011F92E19ED98000000870B8700000087DBB1FA6F4E880E2380FA
          88A212CA49C046FE3D50875BD772E235E6237BDBF187C2358F0B85CAC877C968
          C7040000385C3804000038DCBD37BEE8D1D16104501F5158423909D87A5B5DC2
          B59C788DB9CACE7C78CAFEE3A3C235A8A07C978C764C000080C38543000080C3
          6DF9E9FFFA1FD9D49E95330F2380FA88E212CA49C0D69BEA18AE89D738A2654B
          52F6E5B33C2E14AA6B65BE4B463B260000C0E1C2210000C04CF9E1C38CC308A0
          46A2C8847212B0F596FCEB7DCB2B8E09BF17AA48B8C65C651F7C81700DAA6F65
          B45B020000CC140E010000669A1ABF6745702001D444149A504E02B6DE907F9D
          EB74EB5A4EBCC65C646F79E077DC4F3EE771A15003F90E19ED96000000338543
          00008099C6FA7F7155742801D443149B504E02B6FAAB5BB89613AF7144A71D95
          B2FFFB21B7AE418DE43B64B45B020000CC140E010000661ADCF5CD8F46871240
          3D44C109E52460ABAF3A866B39F11A47925DFEB7C235A8A17C878C764B000080
          99C2210000C04C07EEFEC45BA34309A01EA2E8847212B0D54FFE3515AFD18BB2
          8B9E7D285CF3B850A8A57C878C764B00008099C2210000C04C7BD6BFE394E850
          02A887283CA19C046CF551E7702D275EA395ECECE352F693CB846B5073F90E19
          ED960000003385430000809976ACFE9B13A24309A01EA2F8847212B0D5439DC3
          B59C788DD0F2A529BBFA428F0B851E91EF90D16E09000030533804000098E99E
          5F3CFFD8E85002A8872840A19C046CD5D608D74EFADDF06B5B17E23522D9A75E
          295C831E93EF90D16E0900003053380400009869F34F1EF35BD9D49E95330F25
          807A882214CA49C0564DF9D7ADEEB7AEE5C46BCC945DF09443E19AC78542AF59
          99EF90D16E09000030533804000088E48710330E25809A884214CA49C0563DBD
          10AEE5C46B1C2E7BC33129FBF127856BD0BB56463B25000040241C0200004426
          4736FC283898006A208A512827015B75F44AB89613AFF16BCB96A4ECFF3BCFE3
          42A1C7E5BB63B45302000044C2210000406478DF0F2F8F0E2780EA8B8214CA49
          C0567EF9D7E8E6971D157EFDEA48BC4653F69193846B4043BE3B463B25000040
          241C020000440EDE73D93BA3C309A0FAA228857212B09557FEB5E9C55BD7C46B
          64E73DE150B8E671A1C0AFE4BB63B45302000044C22100004064CFFA0B5E9D4D
          ED4D40FD44710AE524602BA75E0AD772C2351ACE383A653FFC58CA56FC4BF8DE
          02F4AE7C778C764A000080483804000088DCBFEA946746871340F545810AE524
          602B975E0BD772E23572D997CF12AE012DE5BB63B45302000044C22100004064
          DBCF9EFE7BD9D4DE95330F2780EA8B2215CA49C0560EF9D761E529C7855FA33A
          13AF91BDFF44E11A70242BF3DD31DA2901000022E1100000A095FC3062C6E104
          500351A8423909D8BA2B7FFD7BF1D6B59C78ADB7656F794CCA7EFACF291BDC16
          BE8F001C6665B44B020000B4120E0100005A991ADB7A5D704001545C14AB504E
          02B6EEE9D5702D275EEB61A71D95B27FBFD8AD6BC09CE53B63B44B020000B412
          0E0100005A19D97FED17A2430AA0DAA260857212B015AF97C3B59C78AD776597
          FF6FE11A306FF9CE18ED92000000AD844300008056FAEFFBE245D12105506D51
          B4423909D88A93BFD6E235F15A2FCADEFBAC43E19AC785020B90EF8CD12E0900
          00D04A380400006865EF86BF7B5D744801545B14AE504E02B6CE13AE1D225EEB
          3DD9D98F4CD94F3E275C031625DF19A35D120000A09570080000D0CAFDB7BEE6
          B9D12105506D51BC423909D83A4BB8768878ADC72C5F9AB26F5DE871A1405BE4
          3B63B44B020000B4120E0100005AD9F6B3A7FF5E36B577E5CC430AA0DAA28085
          7212B07546FEBADE74F2FF13BEE6BD46BCD65BA63FF54AE11AD04E2BF39D31DA
          250100005A0987000000B3C90F25661C52001517452C949380ADBDF2D7D3AD6B
          BF215EEB1DD93B9F7C285CF3B850A0BD56463B240000C06CC2210000C06C2686
          6EFF5E705001545814B2504E02B6F611AE3D9878AD47BCE19894FDF893C235A0
          23F25D31DA2101000066130E0100006633B8EBEA8F458715407545310BE52460
          5B3CE1DA8335C335F15ACD2D5B92B2AF9DEB71A14047E5BB62B443020000CC26
          1C020000CC66FFE60F9C191D5600D515452D9493806DE1F2D7EE96571C13BEAE
          BD4AB8D61BB28F9C245C030A91EF8AD10E090000309B70080000309B1DABFFF7
          F3A2C30AA0BAA2B0857212B0CD5FFE9AB975EDA1C46BF5979DF78443E19AC785
          0205C977C56887040000984D3804000098CDB6EB9FF9B06C6AEFCA9987154075
          45710BE524609B1FE15A4CBC5673A71F9DB21F5EEAD635A0682BF35D31DA2101
          000066130E0100008E243F9C98715801545814B8504E02B6B911AEB5265EABB7
          EC4B6708D7806E5919ED8E00000047120E0100008E646268EDF782030BA0A2A2
          C8857212B0CD2E7F7D569E725CF8DA215EABB3EC7D2708D780AECA77C4687704
          0000389270080000702483BBAEFE58746801545314BA504E02B658FEBAB8756D
          76E2B57ACACE7954CA7EFACF291BDC16FE7E07284ABE2346BB230000C0918443
          00008023D9BFF90367468716403545B10BE524607B28E1DA9189D76A68F9D294
          FDFBC56E5D034A23DF11A3DD110000E048C2210000C091EC58FDBA13A2430BA0
          9AA2E0857212B0FD86706D6EC46BF533F9895708D780D2C977C4687704000038
          927008000070245B573CF9B7B3A9BD2B671E5A00D514452F949380CDE342E743
          BC562F936F7F529AF88F8F7B5C2850462BF31D31DA1D0100008E241C020000CC
          C5F4E4AE9BB3A97D09A8BE287CA19C7A396013AECD8F78AD3EA6CF3C260D5E79
          611AD9BF29FC1D0ED06DF96E18ED8C00000073110E010000E662F4C08A2F4787
          1740F544F10BE5D4AB019B706D7EC46B35B16C491ABE6C791AB8EAEFD3C8D0FD
          E1EF6F8032C877C36867040000988B70080000301707EEFEF4F9D1E105503D51
          004339F55AC0967FBE379DFCB0F0B520265EAB87F18BFF38F57FFD7D69D7CE0D
          E235A0F4F2DD30DA19010000E6221C020000CCC5CEDBCFFCD3E8F002A89E2882
          A19C7A2560CB3F4FB7AECD4F335C13AF55DBD4B98F3B14AEDDB346BC065446BE
          1B463B230000C05C8443000080B9B8FBE7CF599A4DED5B35F3F002A89E2886A1
          9C7A216013AECD9F70ADFAB2D38F4A835F7D67235CDBB9E32EF11A5025ABF2DD
          30DA19010000E6221C020000CC557E5831E3F002A8A02888A19CEA1CB009D716
          46BC567D239F784DE3D6B53C5C13AF0115B42ADA15010000E62A1C020000CCD5
          D8C0CA6F05071840C544510CE554C7802DFF9C6E79C531E1E7CBECC46BD536FE
          DEE73E285C13AF015594EF84D1AE0800003057E110000060AEFAEFFBE245D121
          06502D51184339D52960CB3F17B7AE2D9C78ADBAA6CF3EAE11AE351F172A5E03
          AA2CDF09A35D11000060AEC2210000C05CEDBEE3AD2F8B0E31806A89E218CAA9
          2E019B706D71C46BD5949DBA340D7EF12D0FB9754DBC065459BE13CEDC130100
          00E6231C020000CCD53D37BEE0B86C6ADFAA99871840B544810CE554F5804DB8
          B678E2B56A1AB9F4AFC2704DBC0654DCAA7C278C7645000080B90A87000000F3
          911F5ACC38C4002A268A6428A7AA066CF9C7BDF294E3C2CF89B913AF55CFC405
          4F0D1F172A5E036A6255B423020000CC4738040000988FB1FE9BBE111C640015
          1285329453D502B6FCE375EB5A7B88D7AA65FA8DC7A6C1AF5DD8325CCB89D780
          AACB77C16847040000988F70080000301F07EEFECCF9D16106501D512C433955
          296013AEB58F78AD42962F4943579CD5F271A14DE235A00EF25D30DA11010000
          E6231C020000CCC78EDB969D181D6600D511053394531502B646B876D2EF861F
          3FF3275EAB8EB10F9D74C4702D275E03EA22DF05A31D110000603EC2210000C0
          7C6CFDE913FF4736B56FD5CCC30CA03AA26886722A73C0967F6C6E5D6B2FF15A
          354C9EF7BF1AE1DA6C8F0B6D12AF0135B22ADF05A31D110000603EC2210000C0
          7C4D8D6FBF3E38D0002A220A6728A732066CC2B5CE10AF955F76C6C3D3E0D72E
          9C53B89613AF017592EF80D16E080000305FE110000060BE86F67CEFD3D1A106
          500D513C4339952D6013AEB55F335C13AF95D8B22569F8B265737A5C6893780D
          A89B7C078C7643000080F90A87000000F3B577E3DF2F8B0E35806A88221ACAA9
          2C015BFE71DC74F2C3C28F918513AE95DFF8C527CE2B5CCB89D7803ACA77C068
          3704000098AF70080000305FDB6F39F989D9D4FE550F4840F544210DE5D4ED80
          2DFFEFBB75AD33C46BE53675EEE352FF372E49BBEE5D9776EED83067BB766E4C
          23433BC2DFBD0015B62ADF01A3DD10000060BEC2210000C042E48718330E3580
          8A88621ACAA99B019B70AD73C46BE5959D7E541AFCEA3B1BF15A14A8CD46BC06
          D4D8AA6827040000588870080000B010E343B77F2F38D8002A200A6A28A76E04
          6CC2B5CE12AF95D7C8275EB3A0702D275E03EA2CDFFDA29D1000006021C22100
          00C0421CBCF7F3EF8A0E3780F28BA21ACAA9C8802DFF6FDDF28A63C28F83F610
          AF95D3F87B9FBBE0702D275E03EA2EDFFDA29D1000006021C2210000C042EC58
          FDFAE745871B40F945610DE55444C096FF37DCBAD679E2B5F2993EFBB8D4FFF5
          F7A55DF7AE0BC3B4B910AF01BD20DFFDA29D1000006021C2210000C0426CFEC9
          637E2B9BDABF6AE6E106507E515C4339753A6013AE1543BC562ED9A94BD3E017
          DFB2A85BD772E235A047ACCA77BF6827040000588870080000B05013C377FD28
          38E0004A2E0A6C28A74E056CC2B5E288D7CA65F4A37FB9E8702D275E037A45BE
          F345BB200000C042854300008085EADFFEC58BA2430EA0DCA2C886726A77C096
          FFFBC46BC511AF95C7C4054F6D846B8B795C6893780DE825F9CE17ED82000000
          0B150E010000166AE79AD35F141D7200E51685369453BB0236E15AF1C46BE530
          FD8647A481AB2E6A4BB89613AF01BD26DFF9A25D10000060A1C2210000C0426D
          B9EE09FF3D9BDABF6AE62107506E516C4339B5236013AE154FBC5602CB97A4A1
          2BCE6ACBE3429BC46B400F5A95EF7CD12E080000B050E110000060312647B75C
          1B1C74002516053794D36202B646B876D2EF86FF5E3A47BCD67D631F7C715BC3
          B59C780DE845F9AE17ED800000008B110E0100001663E0FEAF7D203AEC00CA2B
          8A6E28A785046CF93FE3D6B5EE10AF75D7E479C737C2B5763D2EB449BC06F4AA
          7CD78B7640000080C508870000008BB16BED592F890E3B80F28AC21BCA69BE01
          9B70AD3B9AE19A78AD3BB2338E4E835FBBB0EDE15A4EBC06F4B27CD78B764000
          0080C508870000008BB175C5EFFF4E36B57FD5CCC30EA0BCA20087729A6BC026
          5CEB1EE15A172D5B92862F5BD6F6C7853689D7801EB72ADFF5A21D1000006031
          C2210000C0624D8E6EBE3638F0004A2A8A7028A723056CF9FF7EF3CB8E0AFF59
          3A4FBCD63DE3EF7B7EC7C2B59C780DE875F98E17ED7E0000008B150E01000016
          AB7FFB172F8A0E3D80728A421CCAA955C096CFDDBAD65DE2B5EE983AE7318D70
          AD138F0B6D12AF01EC4FF98E17ED7E0000008B150E010000166BC7EAD79D9079
          8C28544614E3504E51C0265CEB3EF15AF1B2D38E4A835F7967476F5DCB89D700
          1A56E53B5EB4FB0100002C56380400006887FC9063C6A1075052519043391D1E
          B009D7CA41BC56BC914FFC75C7C3B59C780DE0D756453B1F0000403B84430000
          807618EB5FF9ADE0E00328A128CAA19CF2682DB7F294E3C2FF9D6289D78A35F1
          EE671512AEE5C46B00BF91EF76D1CE070000D00EE1100000A01DF66FF9C8D9D1
          E107503E51984339B975AD3CC46BC5993EEB91A9FFEBEF4BBBEE5D17C666ED26
          5E0378B07CB78B763E000080760887000000ED70EFCD271D9F798C28544214E7
          00AD89D78A919DBA340D7EF12D85DDBA9613AF013CC4AA7CB78B763E00008076
          0887000000ED924DEE59191C80002513053A404CBC568CD18FBCB4D0702D275E
          03083CB0D345BB1E000040BB84430000807619DEFB83CF65537D0928B728D201
          1E4ABCD67993EF784A235CDB75EF1D69E78E8D85D9B573531A19DA19FE8E04E8
          65F94E17ED7A000000ED120E010000DA65CFFA0B5E1D1D8200E512853AC08389
          D73A6BFACC6352FFD7DF5778B89613AF01B496EF74D1AE070000D02EE1100000
          A05DB65DFF8C8765537DAB671E8200E512C53AC06F88D73A68F9923474C559BF
          7A5C681C987592780D6056ABF39D2EDAF5000000DA251C020000B4D3E4E8E66B
          B3E9BE04945714EC00BF09D7C46B9D31F6813F4DFDDFBC24EDDCB9B12B1AF1DA
          F0CEF0F722007D29DFE5A21D0F0000A09DC2210000403B1DB8E7B27746872140
          7944E10EF43AE15AE74C9E777C235CDBB5FD8E302C2B82780DE0C8F25D2EDAF1
          000000DA291C020000B4D3F6952F7B4A36DDB77AE66108501E51BC03BD4CBCD6
          19D91947A7C12B2FEC6AB89613AF01CCC9EA7C978B763C000080760A87000000
          ED363DB56765702002944414F040AF12AF75C6F067FFB6AB8F0B6D12AF01CC4D
          BEC345BB1D000040BB8543000080761BDAF3DD4F468722403944110FF422F15A
          FB8DFFFDF34A11AEE5C46B007397EF70D16E070000D06EE1100000A0DD76AD3D
          EB25D1A108500E51C803BD46BCD65E53E73CBA11AE75FB71A14DE23580F9C977
          B868B703000068B770080000D06E9BAF7DD47FCDA6FB56CF3C1401CA218A79A0
          9788D7DA273BEDA834F8A5F34A73EB5A4EBC06306FABF31D2EDAED000000DA2D
          1C02000074C2D8C0CA6F050723400944410FF40AF15AFB8C7CFC55A50AD772E2
          3580F9CB77B768A7030000E88470080000D009FB36BDFFF4E87004E8BE28EA81
          5E205E6B8F89773FB3548F0B6D12AF012C4CBEBB453B1D000040278443000080
          4EB8FB86138ECE3C46144A290A7BA0EEC46B8B377DD6234B19AEE5C46B000BB6
          3ADFDDA29D0E0000A013C221000040A74C8E6DBD2E382001BA2C8A7BA0CEC46B
          8BB47C691AFCE25B4AF7B8D026F11AC0C2E53B5BB4CB010000744A38040000E8
          9403F77EEE5DD12109D05D51E00375255E5B9CD18FBCB4B4E15A4EBC06B038F9
          CE16ED720000009D120E0100003AE5DE9B5FFCF8CC6344A174A2C807EA48BCB6
          70936F7F52691F17DA245E0358B4D5F9CE16ED720000009D120E0100003A696A
          E2BE1B828312A08BA2D007EAA419AE89D7E66FFACC63D2C05517953A5CCB89D7
          00162FDFD5A21D0E0000A093C221000040271DDCFE858BA2C312A07BA2E007EA
          42B8B640CB96A4E1CF9F56EAC7853689D700DA23DFD5A21D0E0000A093C22100
          0040276DBFE5A54FCE3C46144A258A7EA00EC46B0B3376C90B2B11AEE5C46B00
          6DB33ADFD5A21D0E0000A093C221000040A74D4FEEBC31383001BA240A7FA0EA
          C46BF33775EEE31BE15AD91F17DA245E03689F7C478B76370000804E0B870000
          009DD67FFF972F8E0E4D80EE88E21FA832F1DAFC64A71F9506BF766165C2B59C
          780DA0BDF21D2DDADD0000003A2D1C02000074DA7DAB4E7966367D60F50312D0
          7D51000455255E9B9F91CFBC3EF55FFDC1B473E7A6CAD8B573731A19DE15FE3E
          03604156E73B5AB4BB010000745A3804000028C2F4E4EE9B838313A00BA20808
          AA48BC3677E3179D50B9702D275E0368BF7C378B763600008022844300008022
          0CECFCC687A3C313A07851080455235E9B9BE9373D2AF57FF392B4F3BEF56120
          5666E23580CEC877B3686703000028423804000028C2FDB7BEE6B999C7884229
          443110548978EDC8B25397A6C1AFBEB392B7AEE5C46B001DB33ADFCDA29D0D00
          00A008E1100000A028D393BB3C46144A200A82A02AC46B4736F2F15755365CCB
          89D7003A27DFC9A25D0D0000A028E1100000A028FDF7FDEBC5D1210A50AC280A
          822A10AFCD6EE2C2A71F0AD72AF8B8D026F11A4067E53B59B4AB01000014251C
          0200001465FB2D273F31F31851E8BA280C82B213AFB536FDC66353FF372FA974
          B89613AF0174DCEA7C278B7635000080A28443000080224D8DDFB3223848010A
          14C5415066E2B516962F49835F7C4BA51F17DA245E03E8BC7C178B7634000080
          22854300008022F5DDFDA9B74587294071A24008CAA819AE89D71E6AF41F4FAE
          45B89613AF011423DFC5A21D0D0000A048E1100000A04877DFF0BC63328F1185
          AE8A4221281BE15A6CF26D4F3C14AE55FC71A14DE23580C2ACCE77B168470300
          0028523804000028DAC4C85D3F0A0E54808244B110948978EDA1B2331F9EFABF
          F1BEDA846B39F11A4071F21D2CDACD0000008A160E0100008AB66FF307CE8C0E
          55806244C1109485786D86654BD2F0E7CFA8CDE3429BC46B00C5CA77B0683703
          0000285A3804000028DAD6157FF03F338F1185AE89A2212803F1DA838D5DF2C2
          DA856B39F11A40E156E73B58B49B010000142D1C02000074C3D8C0CA6F05072B
          4001A27008BA4DBCF61B53E73EEE50B856A3C7853689D7008A97EF5ED14E0600
          00D00DE1100000A01B76DFF9B697676E6183AE88E221E826F1DA21D9E947A5C1
          AF5D58CB5BD772E23580AE589DEF5ED14E060000D00DE1100000A05BB2A9FDAB
          820316A0C3A28008BA45BC76C8C8A7FFA6B6E15A4EBC06D0250FEC5CD12E0600
          00D02DE1100000A05B06775DFDB1F09005E8A82822826E10AF2D49E3EF7D6EAD
          C3B59C780DA07BF29D2BDAC5000000BA251C02000074CBF6957FF5D4CC6344A1
          7051480445EBF5786DFA4D8F4AFDDFBC24EDBC6F7D187DD585780DA0AB56E73B
          57B48B010000744B38040000E8A6C9B16DD765D30713509C28268222F572BC96
          9DBA340D7EF59DA9FFDB1F4E3B776EAEB543F1DAEEF0F710009D97EF5AD10E06
          0000D04DE1100000A09BF66FFDE839D1610BD03951500445E9E5786DE4E3AFEA
          89702D275E03E8BE7CD78A76300000806E0A87000000DDB475C5EFFF4E367D70
          CDCCC316A073A2A8088AD0ABF1DAC4054F3D14AEDD7757187BD58D780DA014D6
          E4BB56B483010000745338040000E8B6D103375C191CB8001D128545D049CD70
          ADD7E2B5E9373E22F57FF3929E09D772E2358072C877AC68F7020000E8B67008
          0000D06DBBD69EF592CC2D6C50982830824EE9C95BD7962F49835F38A7671E17
          DA245E03288D35F98E15ED5E000000DD160E010000CA607A6AEFCAE0E005E880
          2832824EE8C5786DEC4327F55CB89613AF019447BE5B453B1700004019844300
          008032E8BFEFCB1747872F40FB45A111B45BAFC56B936F7BE2A170AD871E17DA
          245E0328977CB78A762E0000803208870000006570CF2F9E7F6CE631A2508828
          368276EAA5782D3BE3E1A9FF9B97F464B89613AF0194CE9A7CB78A762E000080
          320887000000653136F0CB6F070730409B45C111B44BCFC46BCB96A4E1CF9FD1
          938F0B6D12AF01944FBE5345BB16000040598443000080B2D8B5F6EC97646E61
          838E8BA22368875E89D7C62FFEE39E0ED772E23580525A93EF54D1AE05000050
          16E1100000A04CA62777DD1C1CC4006D148547B058BD10AF4DBDF9B187C2B51E
          7D5C6893780DA09CF25D2ADAB1000000CA241C02000094C9817B3EF7AEE83006
          689F283E82C5A87BBC969D7E541AFCDA853D7FEB5A4EBC06505EF92E15ED5800
          000065120E010000CA64DBCF9EFE7B99C78842474501122C54DDE3B5914FFF8D
          70ED57C46B00A5B626DFA5A21D0B0000A04CC221000040D98C1EF8F995C1810C
          D0265184040B51E7786DFCEF9E2D5C3B8C780DA0DCF21D2ADAAD000000CA261C
          02000094CD8EDB969D98B9850D3A260A9160BEEA1AAF4D9F7D5CEABFFA8369E7
          7D778521572F12AF0194DE9A7C878A762B000080B209870000006534357EDF0D
          C1C10CD006518C04F351C7782D3B75691AFCEA3BDDBA3683780DA0FCF2DD29DA
          A9000000CA281C02000094D1FEAD979E131DCE008B1705493057758CD7462EFD
          2BE15A40BC06500DF9EE14ED5400000065140E010000CA68F3B58FF9ADCC6344
          A123A228098EA419AED5295E9B7CC7530E856B1E17FA10E23580CA5893EF4ED1
          4E0500005046E1100000A0AC06777DEBA3C1010DB048519C04B3A95BB836FD86
          47A4FE6F5E225C6B41BC06501DF9CE14ED5200000065150E010000CAEA9E1B5F
          705CE6163668BB285082566A15AF2D5F9286AE38DBE34267215E03A89435F9CE
          14ED5200000065150E010000CA6CB4FFA66F040735C02244911244EA14AF8D7D
          F0C5C2B52310AF01544BBE2B453B1400004099854300008032DB71DBB21333B7
          B0415B45A112CC5497786DF2BCE30F856B1E173A2BF11A40E5ACC977A5688702
          000028B3700800005076936377AFC8A6FB13D01E51AC0487AB43BC969D7174EA
          FFE62569E78E4D69E7CE2DCC62D7034686F784BF2F0028A77C478A7627000080
          B20B8700000065B777C3DF2FCBA6FBD7CC3CB40116260A96A0A90EF1DAF065CB
          53FFBF5D1AC65A3C98780DA092D6E43B52B43B010000945D38040000A882E9A9
          FDAB82831B6001A2680972558FD7C6DFF77CE1DA3C88D700AA29DF8DA29D0900
          00A00AC221000040151CDCFE858BA2C31B60FEA27009AA1CAF4D9DF39843E19A
          C785CE99780DA0BAF2DD28DA99000000AA201C02000054C1D69F3DEDF7328F11
          85B688E2257A5B55E3B5ECB4A3D2E0D72E74EBDA3C89D7002A6D4DBE1B453B13
          000040158443000080AA18DAFB83CF050738C03C450113BDABAAF1DAC8A7FF46
          B8B600E235806ACB77A26857020000A88A700800005015F7DEF4A78FCDDCC206
          8B16454CF4A62AC66BE37FF76C8F0B5D20F11A40E5ADC977A26857020000A88A
          700800005025A3077F7155709003CC431432D17BAA16AF4D9FF5C8D4FFED0F0B
          D71648BC06507DF92E14ED4800000055120E010000AAE4BE55A73C33730B1B2C
          4A1433D13B9AE15A55E2B56CF9D234F8A5F33C2E7411C46B00B5B026DF85A21D
          090000A04AC221000040D58C0FADFB7E70A003CC511435D11BAA76EBDAC8A57F
          255C5B24F11A403DE43B50B41B010000544D38040000A89A9DB79FF9A7995BD8
          60C1A2B089FAAB52BC36F98EDF3F14AE795CE8A288D7006A634DBE0345BB1100
          0040D584430000802A9A1CDD765D70B003CC411437516F5589D7A6CF3C26F57F
          F312E15A1B88D700EA23DF7DA29D080000A08AC22100004015ED59FFCE5332B7
          B0C182448113F55589786DD9923474C5D91E17DA26E235805A5993EF3ED14E04
          00005045E1100000A0AAA62676DE181CF0004710454ED45315E2B5B10FFCA970
          AD8DC46B00F592EF3CD12E0400005055E1100000A0AAF66DFEE099995BD860DE
          A2D089FA297BBC3675EEE30F856B1E17DA36E23580DA5993EF3CD12E04000050
          55E1100000A0CAA62777DF1C1CF400B3886227EAA5CCF15A76FAD1A9FFEBEF13
          AEB599780DA07EF25D27DA81000000AA2C1C02000054D9FECDFFF8C6CC2D6C30
          2F51F0447D94395E1BFEEC328F0BED00F11A402DADC9779D6807020000A8B270
          0800005075D353FB5605073E400B51F4443D94355E1BFFFBE709D73A44BC0650
          4FF98E13ED3E00000055170E010000AAAE6FDB27DE9AB9850DE62C0A9FA8BE32
          C66B53E73C3AF57FFBC31E17DA21E23580DA5A93EF38D1EE0300005075E11000
          00A00EDCC2067317C54F545BD9E2B5ECD4A569F06B17BA75AD83C46B00F5E5F6
          350000A0CEC2210000401DB8850DE62E0AA0A8AEB2C56B239FF86BE15A8789D7
          006ACDED6B000040AD8543000080BA700B1BCC4D1441513DCD70AD2CF1DAC4BB
          9F79285CF3B8D08E12AF01D49BDBD7000080BA0B870000007571E816B681B50F
          48406B510C45B594295C9B3EEB91A9FFDB1F4E3B776C4E3B776EA583763D6064
          786FF8730D402DAC75FB1A00005077E1100000A04EA6A7F6AF0A0E8280C34441
          14D5519A786DF9D234F8A5F3D2C1EFFC53185BD15EE23580FACB779968C70100
          00A8937008000050276E6183238BA228AAA12CF1DAE847FF52B85620F11A404F
          70FB1A0000D013C221000040DD4C4FEE5D191C0801BF128551945F19E2B5C9B7
          3FE950B8E671A18511AF01F4867C8789761B000080BA098700000075B37FF387
          CFCEDCC2062D457114E5D6ED786DFACC6352FFD51F14AE154CBC06D033D6E63B
          4CB4DB000000D44D38040000A8A3A9895D37070743C003A2408AF2EA6ABCB66C
          491ABAE26C8F0BED02F11A40EFC8779768A7010000A8A37008000050477BEEBA
          F0B5995BD820144552945337E3B5B10FFC8970AD4BC46B003D656DBEBB443B0D
          0000401D8543000080BA9A1CBD7B457040043D2F0AA5289F6EC56B53E73EEE50
          B8E671A15D215E03E82DF9CE12ED3200000075150E010000EA6AD7BA37FF45E6
          163678882896A25CBA11AF65A71F95FABFFE3EB7AE7591780DA0E7ACCD779668
          97010000A8AB70080000506713C3EB7F101C14414F8B8229CAA31BF1DAF06797
          09D7BA4CBC06D07BF25D25DA61000000EA2C1C020000D4D98EDB969D98B9850D
          1E248AA62887A2E3B5F18B4E10AE9580780DA027ADCD77956887010000A8B370
          08000050776303ABBF131C1841CF8AC229BAAFC8786DFA4D8F4AFDFF7669DAB9
          63731854511CF11A406FCA77946877010000A8BB700800005077DB57BEFCA999
          5BD8E0D7A2788AEE69866B45C46BD9A94BD3E0D72E74EB5A4988D7007AD6DA7C
          47897617000080BA0B87000000BD6064FF755F0A0E8EA027451115DD51E4AD6B
          231F7F9570AD44C46B00BD2BDF4DA29D050000A01784430000805EB0EDE7CF59
          9AB9850D1AA2908AE21515AF4D5CF8F443E19AC7859686780DA0A7ADCD779368
          67010000E805E1100000A057F4DFFFD54B820324E839514C45B18A88D7A6DFF8
          88D4FFED0F0BD74A46BC06D0DBF29D24DA550000007A4538040000E8159BFEF3
          91FF657A6AFFAAE820097A491454519C8EC76BCB97A4C12FBDD5E3424B48BC06
          D0DBF25D24DF49A25D050000A05784430000805EB26FD307CFCC3C4A941E1745
          5514A3D3F1DAE83F9E2C5C2B29F11A40CF5B9BEF22D18E020000D04BC2210000
          40AF991CDB7E7D363D98A0574561159DD7C9786DF26D4F4C07FFCFA7D2CE9DDB
          28A15D0F1819DE17FE3C02D01BF21D24DA4D0000007A4D38040000E8353BD79E
          FD926C7A70EDCC4325E815515C4567752A5ECBCE7878EAFFF687C3688A7210AF
          01F080B5F90E12ED26000000BD261C020000F4A2B18135DF0D0E96A027448115
          9DD391786DD9923474C5D96E5D2B39F11A00B9B181DBBE1BED24000000BD281C
          020000F4A2EDB7BCF4C9995BD8E8515164456774225E1BBBE485C2B50A10AF01
          F02B6BF3DD23DA490000007A5138040000E855437B7F787970C004B5178556B4
          5FBBE3B5A9731F275CAB08F11A004DF9CE11ED22000000BD2A1C0200F0FF67EF
          DFA32DBBEEFAC03783EEDB232361300849E7064487475BC480846DC922922C1B
          02DCC421EE98F0708C43B8964A25C9922C639C0E049C74E834010F427321101E
          B9814E1302344D13DA97101283307E636C3DCAE5A228952CC97A3FAA542A49A5
          47D5A9BBE7A9BD7DCED9F5DB67AFBDCE5A7BCD39D7E78FCFD8BB66559D3DE75C
          6BAFC76F7DCF5AC0581DFDFD8B3F67E3F4937744179AA06651D88A6E75195EDB
          B8F682B3277EE59F0AAF1542780D80CF989C6BA4738EE85C04000060ACC24600
          0080317BE2EE1FBD65C3A344199928704577BA0CAF3DFB93DF29B85610E13500
          B63990CE35A27310000080310B1B010000C6EEC553F7BC37B8E004D58A4257EC
          DD2CB8D64578EDF97F7CA5E05A6184D700D82E9D6344E71E0000006317360200
          008CDD83B7BFF96B37DC858D1189C257EC4D57C1B533377EE9D9277FE37F0D03
          52E44B780D803907D2394674EE0100003076612300000067FFD4B3C7DEFB6F83
          0B4F50A52880457B5D84D736AEF982B327FFDD3F72D7B50209AF01302F9D5B44
          E71C00000008B00100002CF4A93F78E55FD870173646220A61D14E17E1B5677F
          ECDB04D70A25BC0640E0403AB788CE390000001060030000D8D5F17B7EFA7BD3
          05A7B90B50509D2888C5EAF61A5E7BE17B2E115C2B98F01A008103E99C223AD7
          000000E09CB0110000802DA79F7FF043C18528A84A14C662357B09AF9DB9E18B
          CE9EF8F57785A128CA20BC0640249D4B44E7180000006C091B010000D8F2F081
          9B5EB7E12E6C542E0A64D15CEBF0DAD59F7FF6E42F7C97BBAE154E780D80050E
          A47389E81C030000802D61230000003B9D3AFEA15F0E2E484135A25016CDB40D
          AF9DFAE1D709AE5540780D80454E1DFFE02F47E716000000EC1436020000B0D3
          A7FEE0B2BFB871E6297761A35A51308BE5DA84D75E7CFB570AAE5542780D80C5
          9E3A90CE21A2730B000000760A1B01000038DFB1BB7FECBB363C4A944A45E12C
          76B76A786D63FF179E3DF17FFEF33008457984D700D8C58174EE109D53000000
          70BEB011000080D80BCFDEF5BBC1052A285E14D062B195C26B6FFEFCB3CFFCCC
          7E775DAB88F01A00BB49E70CD1B904000000B1B011000080D8A73FFACD976CB8
          0B1B158A425AC45609AF3DFF037F4D70AD32C26B002C71209D3344E712000000
          C4C246000000163BF9F0AFFF68BA303577A10A8A1605B5385FD3F0DAE99B2F14
          5CAB90F01A004B1C48E70AD139040000008B858D000000ECEECC0B8F7E34B860
          05C58AC25AECD424BCB6B1EF82B34FFDF23F115EAB90F01A00CBA47384E8DC01
          000080DD858D000000ECEE91836FFFE68D334F1F9C380B3588025B9C330BAE2D
          0BAF3DFB93DF79F6C977FFE4D9871EBA87CA3C3CF1EC334F84DF1D00983A98CE
          11A27307000000761736020000B0DCA9273FF2ABC1852B285214DCA2D95DD79E
          FFC7570AAE554C780D8026D2B94174CE000000C0726123000000CB7DEA0F2EFB
          8B1BA79F3A105DC082D244E1ADB15B165E3BF3962F39FBE46FFCAF61E8893A08
          AF01D0C8E49C209D1B44E70C0000002C1736020000D0CCE377BDEBC60D8F12A5
          0251806BCC760BAF6D5CFD05674FFEBB7FE4AE6B95135E03A0A183E99C203A57
          000000A099B011000080E69E7BEACE770717B2A02851886BAC760BAF3DFB63DF
          26B83602C26B003495CE05A273040000009A0B1B01000068EE9EF7BFEA0B37DC
          858DC24541AE315A145E7BF17F7CB9E0DA4808AF01B08283F7BCFFCA2F88CE11
          000000682E6C04000060354FDCFD63DF952E60CD5DD082624461AEB189C26B67
          AEFFA2B3277EFD5D61D089FA08AF01B08283E91C203A37000000603561230000
          00AB7BFEE983BF155CD882224481AE31392FBC76F5E79F3DF90BDFE5AE6B2322
          BC06C02AD2B17F744E000000C0EAC2460000005677EF07BFEE4B37DC858D4245
          A1AEB1980FAFBDF8FFF916C1B591115E03604507D3B17F744E000000C0EAC246
          000000DA3976CF4FFE8374416BEE0217642F0A768DC1F6F0DAC63F7CD9D98D3B
          7EFBECE9179E0C434ED449780D80151D4CC7FCD1B900000000ED848D000000B4
          F7C2337FF25F820B5D90B528DC55BB59786DE3FABF7C76E343BF7C76E3F4539F
          998FC71EFB741876A22EC26B00AC2A1DEB47E700000000B41736020000D0DE7D
          1F7EED4B37DC858DC24401AF9A7D26BCF6EBEFDCBCEBDAFC7C9C38F1481878A2
          1EC26B00B470301DEB47E700000000B41736020000B037C73EF52F3D4A94A244
          21AF5AA5E0DA27DEF84561706DE6D4B3C7C2D01375105E03A08583E9183F3AF6
          070000606FC246000000F6EEF99307DE1D5CF8822C4541AFDAA4E0DAC7FF873F
          7FF6E46FFDE48EC78546CE9C3EB919728AC24F944D780D8036D2B17D74CC0F00
          00C0DE858D000000ECDDA7DEF757FFD2C6E9A70E4417C0203751E0AB261FFF5B
          7FEEEC633FFBD65DEFBA36EF89C71F080350944B780D805626C7F4E9D83E3AE6
          07000060EFC246000000BAF1D8E17FFAE60D8F12A50051E8AB1647DE72C9D967
          7EE7E7C271EFE6E4538F862128CA24BC06404B07D3317D74AC0F00004037C246
          000000BAF3ECF10FFE7270210CB21205BF4AF7F137FC7767371E3A188EB789E7
          9F7B320C42511EE13500DA4AC7F2D1313E000000DD091B010000E8CED15BBFFC
          CF9C79F1898F4517C420175100AC6467EEBF3D1CE7AA1E79F8DE3010453984D7
          00682B1DC3A763F9E8181F000080EE848D00000074EBE14FBCF56F6F78942819
          8B426025BAF386AFDED35DD7E61D3BF660188AA20CC26B00ECC1C1740C1F1DDB
          030000D0ADB011000080EE3DFDE87FFCE974216CEEC21864210A8395261AD75E
          3DFDF4E361308AFC09AF01B00707D3B17B744C0F000040F7C246000000FA71FA
          F9073F145C2083C14581B0527CF86FFDF9704C5D78F1851361388ABC09AF01B0
          17E9983D3A96070000A01F6123000000FDB8FF8FBEE5951BEEC24686A2605809
          FA0CAFCD3CFAE87D61488A3C09AF01B04707D3317B742C0F0000403FC2460000
          00FA73EC9E9FFA871B679E39387116721185C372762EB8168FA56B4F1E7FE4EC
          430FDD4B011E9E78F69963E1720480060EA663F5E8181E000080FE848D000000
          F4EBB913B7FD4670C10C061385C47215F5BF4F29101585A5C88BF01A007B958E
          D1A36377000000FA1536020000D0AFA3BF7FD1679F79F1D86DD1853318421414
          CB51D4F7BE9D7EF1E466382A0A4D9107E13500F62A1D9BA763F4E8D81D000080
          7E858D000000F4EFA13BAE7FED8647899289282C9693A8CFEBF4F8630F84C129
          8627BC0640070EA663F3E8981D000080FE858D000000ACC7530FFE1F3F922E98
          CD5D4083B58B426339F8F0DFFAF3617FD7EDA9138F86E1298625BC0640070EA6
          63F2E8581D000080F5081B010000589F179E39F25F820B69B05651786C68513F
          87F2DCA927C30015C3115E03A00BE9583C3A46070000607DC246000000D6E79E
          F75FF5851BA74F1E882EA8C1BA4401B2A144FD1BDA99D34F9F7DF8E1FBC22015
          EB27BC06402726C7E0E9583C3A46070000607DC246000000D6EBD13F7EE7776C
          789428038A8264EB16F52B274F3CF16018A662BD84D700E8C8C1740C1E1D9B03
          0000B05E6123000000EB77F291DFFAA974216DEEC21AAC4514285B970FFFAD3F
          1FF62937274F3E1E06AA581FE135003A72301D7B47C7E4000000AC5FD8080000
          C0305E78E6AEDF0D2EB041EFA260D93A447DC9D5F3CF9F084355AC87F01A005D
          49C7DCD1B138000000C3081B01000018C6A7DE77C5179C397DE28EE8421BF429
          0A97F529EA43091E79F8BE305C45BF84D700E84A3AD64EC7DCD1B138000000C3
          081B01000018CEC39FB8E56F6F7894286B1685CCFA50CAE34217397EECE13060
          457F84D700E8D0C174AC1D1D83030000309CB011000080619D78E0DFFF50BAC0
          3677C10D7A1385CDBA547A706DE699A79F084356F443780D800E1D4CC7D8D1B1
          37000000C30A1B01000018DE734FDDF9EEE0C21BF4220A9D7525FABC52BDF8C2
          5361D08AEE09AF01D0A5746C1D1D7303000030BCB011000080E11DFDFD8B3EFB
          CC8B4F7C2CBA00075D8B82677B157D4E0D1E7BF4FE3070457784D700E8523AA6
          4EC7D6D13137000000C30B1B010000C8C303B7FDFDD76C7894286B1005D0DAAA
          E571A18B9C78F2913074453784D700E8D8C1744C1D1D6B0300009087B0110000
          807C1CBBE75FFDC374E16DEE421C742A0AA2ADAAF6E0DACCA9678F85C12BF64E
          780D808E1D4CC7D2D13136000000F9081B010000C8CB334FDCFA0BE902DCDC05
          39E84C14485B45F4336B75FAC5A737835651008BF684D700E8D8C1740C1D1D5B
          0300009097B011000080FCBCF0ECA76EDD38F3EC59E8C387FEE69F0B8369BB9A
          FC9FE8678DC1E38F3F1886B068E7E187EE3BFBEC33C7C3B9068036D2B173744C
          0D0000407EC246000000F2F3A9F7FDD5BF74E6F4893BA20B74B0572B05D826FF
          F6C3AFFB6FC39F33164F9D782C0C62B13AE13500BA968E99D3B173744C0D0000
          407EC246000000F2F4E06D6FFEDA8D33CF1E9ABF48077BD528C096FECD88EFBA
          B6DD73A74E84612C5623BC06400F0EA563E6E8581A0000803C858D000000E4EB
          D8DD3FFEDDE9C2DCDC853AD893A50136C1B51DCE9C7EF6ECC30FDF1786B26846
          780D801E1C4AC7CAD13134000000F90A1B010000C8DBD38FFDE77F9D2ED0CD5D
          B083D61606D804D7163AF6C44361308BE584D700E8C1A1748C1C1D3B03000090
          B7B011000080FCBDF0CC91FF125CB88356CE0BB04DFEFCE1D7FDB7E1BFE59CA7
          4F3E1186B3D89DF01A007D48C7C6D13133000000F90B1B010000C8DFDDEFBDE4
          F3CEBC78ECB6E8021EACEA3301B6F4EAAE6B8DBCF0FC5361408BC584D700E843
          3A264EC7C6D13133000000F90B1B01000028C3FD7FF4772FDFF0285118CC238F
          7C3A0C6A713EE135007A72281D1347C7CA00000094216C040000A01C8FFEF13B
          BF235DB89BBB9007ACC1F1E38F84612D76125E03A02787D2B170748C0C000040
          39C246000000CAF2E4A7FFED0FA40B787317F4809E3DFBCCB130B0C516E13500
          7A72281D0347C7C600000094256C040000A03CCF3CF1FBFF365DC89BBBB007F4
          E8F48B4F87A12DCE115E03A02787D2B16F744C0C00004079C246000000CAF4FC
          D37FFC3BC1053EA0478F3DFA4018DE1A3BE13500FA928E79A36361000000CA14
          3602000050A6A3B77EC59F39FDFC231F892EF401FD38F1E4A361806BCC84D700
          E84B3AD64DC7BCD1B130000000650A1B01000028D7BD1FFAEB5FB671FAE983D1
          053FA07BA79E7D320C718D95F01A00BD991CE3A663DDE8181800008072858D00
          000094EDA13BDFF28D1B679E3D74DE453FA073674E3FB319DA8AC25C6323BC06
          408F0EA563DCE8D817000080B2858D00000094EF89BB7FECBBD285BEB90B7F40
          0F9E78FCC130D03526C26B00F4E8503AB68D8E79010000285FD8080000401D4E
          3CF02BEF4A17FCE62E00021D3BF9D4E361A86B2C84D700E8D1A1744C1B1DEB02
          00005087B0110000807A3CF3F8EFFD42BAF037772110E8D0F3CF9D08835D6320
          BC06408F0EA563D9E818170000807A848D000000D4E5B9A7EEF8CD740170EE82
          20D0A1871FBE2F0C78D54C780D801E1D4AC7B0D1B12D00000075091B010000A8
          CF0BCFDEF3DEE0C220D09163C71E0E435EB5125E03A04FE9D8353AA6050000A0
          3E6123000000F5B9FBBD2FFFDCD3CF3FFAD1E80221B0774F3FFD4418F4AA91F0
          1A007D4AC7ACE9D8353AA6050000A03E612300000075BAF743DFF09233A74FDC
          115D2804F6E685E74F8661AFDA08AF01D0A774AC9A8E59A36359000000EA1436
          02000050AF076EFBCED76C9C397568E22CD0AD471F79E0EC430FDD57AD871FFA
          F4D9679F79321C3B0074E0503A568D8E61010000A857D808000040DD1E3DF48F
          DE942E10CE5D3004F6E8C9E38F86C1AF1A08AF01D0B343E918353A76050000A0
          6E6123000000F57BE2E8BFB8395D289CBB7008EC410A7845E1AFD209AF01D0B3
          43E9D8343A66050000A07E6123000000E370FCDEFFEF3BD305C3B90B88404BA7
          5F7C260C80954C780D809E1D4AC7A4D1B12A000000E31036020000301E4F3DF8
          6B3F922E1CCE5D48045A7AECB107C320588984D700E8D9A1742C1A1DA3020000
          301E6123000000E3F2F4A3FFE967D305C4B90B8A400B274E3C1E86C14A23BC06
          40CF0EA563D0E8D81400008071091B010000189F679F78DF2FA60B8973171681
          159D3A75220C849544780D809E1D4AC79ED13129000000E31336020000304EA7
          4E7CFC37D205C5B90B8CC00ACE9C7E7633001605C34A20BC0640CF0EA563CEE8
          5814000080710A1B01000018AFE79FFEE3DF092E34022B78E2F187C37058EE84
          D700E85B3AD68C8E4101000018AFB011000080F1BAEB77FFFBFFFAC553F7BD2F
          BAE0083473F2A927C28058CE84D700E85B3AC64CC79AD13128000000E3153602
          0000306E476FBDE8B35F3C75FF07A20B8FC072CF3F77320C89E54A780D80BEA5
          63CB748C191D7B020000306E612300000008B1C1DE3CF2F0FD61582C37C26B00
          F44D780D000080DD848D00000090DCFD07AFFC0BA79F7FE423D185486077C78F
          3D1206C67222BC0640DFD2B1643AA68C8E3501000020091B01000060E6537F70
          D95F146283D53DF3F4B13034960BE13500FA968E21D3B164748C090000003361
          230000006C972E3C9E79E1D86DD1854920F6E20B4F87C1B11C08AF01D0B774EC
          28BC06000040136123000000CCBBE7035FFBC5426CB09A471F7D200C900D4978
          0D80BEA563C674EC181D53020000C0BCB01100000022426CB09A279F7C340C91
          0D45780D80BE09AF010000B0AAB01100000016B9F7437FFDCBCE9C7EEA4074C1
          12D8E9D9679F0C836443105E03A06FE918311D2B46C790000000B048D8080000
          00BBB9F7837FED4BDD890D963BFDE2339BC1B12850B64EC26B00F42D1D1BA663
          C4E8D811000000761336020000C0321E270ACD3CFED88361A86C5D84D700E89B
          C786020000B017612300000034912E549E7EFE918F44173281739E3AF178182C
          5B07E13500FA968E0585D7000000D88BB0110000009AFAD41F5CF6174F3FFFE8
          4737CE3C771638DF73A74E9E7D68F331A2EBF5F043F79F7DF69913619F00A00B
          E918301D0B46C788000000D054D808000000AB106283C5CE9C3E75F6E187EF0F
          43667D115E03A06FC26B00000074256C0400008055DDFDFB2FFFDC174F3DF081
          E802278CDD134F3C1206CDFA20BC0640DFD2315F3AF68B8E0901000060556123
          000000B471F4D68B3E5B880DCEF7F4C9E361D8AC6BC26B00F42D1DEBA563BEE8
          5810000000DA081B010000A0AD7441F38567EEBE35BAE00963F5C2F34F8781B3
          2E09AF01D0B7748C27BC06000040D7C246000000D88B23EFF9D2CF7AFEE4277F
          7BE3CC7387E72F7CC2583DF2C80361F0AC0BC26B00F4EC703AB64BC778D1B11F
          000000EC45D8080000005D38F5E4C77E3D5DF09CBB000AA374FCD8A361F86CAF
          84D700E8D9E1744C171DEB0100004017C246000000E8CA334FBCEF17D385CFB9
          0BA1303ACF3CFD641840DB0BE135007A76381DCB45C778000000D095B0110000
          00BA74F291FFF8D3E902E8DC05511895175F78260CA1B525BC0640CF0E9F7CE4
          B77E2A3AB6030000802E858D000000D0B5130FFCF20FA50BA173174661541E7B
          F4C1308CB62AE135007A76381DBB45C774000000D0B5B011000000FA70EC9E9F
          F9DE744174EE02298CC689271F0F0369AB105E03A06787D3315B742C07000000
          7D081B010000A02F8F1FF9E11BD285D1B90BA5300AA79E3D1186D29A125E03A0
          6787D3B15A740C070000007D091B010000A04F0F7FE2BBBE295D209DBB600AD5
          3B73FAD466082D0AA72D23BC0640CF0EA763B4E8D80D000000FA143602000040
          DFEEFFA3377CF599D3270F04174FA16A8F3FFE701850DB8DF01A007D4AC764E9
          D82C3A6603000080BE858D000000B00EF77CE0357FF9F4F38F7E34BA900AB57A
          EAA927C290DA22C26B00F4291D8BA563B2E8580D000000D6216C040000807539
          7AEB577EF60BCFDC7DEB86478A3212CF9D3A1906D522C26B00F4E8703A064BC7
          62D1311A000000AC4BD808000000EB76EAC98FFF46BA903A776115AA73E6F473
          671F7EF8FE30B0B69DF01A003D3A9C8EBDA2633200000058B7B01100000086F0
          F463EFF937E982EADC0556A8CEB1271E09436B33C26B00F4E8703AE68A8EC500
          0000600861230000000CE5C94FFFBB1FDC38F3FCE189B350ABA74F3E79F6A187
          D25DD8CEF7F0430F9C7DF699A7C2FF07007B74381D6B45C7600000003094B011
          00000086F4D89FFCF3EBD205D6B90BAE508D179E7F46780D80753B9C8EB1A263
          2F0000001852D808000000437BE0B6377FED99D34F1F0C2EBE42151E7DE441E1
          3500D6221D53A563ABE8980B000000861636020000400EEEF9C06BFEF28BCF3D
          FC91E8422C94EEC9E38F0BAF01D0BB742C958EA9A2632D000000C841D8080000
          00B9B8EB772FFC6F9E7BEA13BFB5E191A254E6D9674E08AF01D0A7C3E9182A1D
          4B45C758000000908BB01100000072F3F4A3FFF95FA70BB1731766A158A75F3C
          25BC06405F0EA763A7E8980A000000721336020000408E8EDDF3D3DF9B2EC8CE
          5DA085623D77EAE9B01D00F6E0703A668A8EA500000020476123000000E4EAE1
          83DFFDCD1B674E1DDAD878FE2C0000DB4C8E91D2B152740C05000000B90A1B01
          00002067F7FDE1FFF055A75F78E263E1855B0080114AC746E918293A76020000
          809C858D00000090BBA3B77EF99F79EEE4C1DFDAD878FEF0FC055C008011399C
          8E89D2B15174CC04000000B90B1B010000A014271F79F7BF4C176EE72EE40200
          8CC1E1742C141D230100004029C24600000028C9637FF2CFAE4D1770E72EE802
          00D4EC703A068A8E8D000000A024612300000094E6FE3FFAB6AF3EFDE2F1DB82
          8BBB00005549C73C9FFEA36F7D65744C04000000A5091B010000A044477FFFE2
          CF79FEE9C3BFB3E16E6C00409D0EA7639D74CC131D0B0100004089C246000000
          28D9D38FFECECFA60BBC73177C01004A76381DE344C73E00000050B2B0110000
          004AF7F8911FBE215DE89DBBF00B0050A2C3E9D8263AE601000080D2858D0000
          0050834FFFE1DFFEAAD32F3CFAD1E02230004011D2B14C3AA6898E75000000A0
          066123000000D4E2C87BBEF4B39E7DF20F7F6DC3DDD80080B21C4EC730E95826
          3AC6010000805A848D000000509BE3F7FEECF7A50BC173178601007274F8D83D
          3FF3BDD1310D000000D4266C040000801A3D70DB77BEE6CCE913770417890100
          B2908E55D2314B742C03000000350A1B010000A056477FFFE2CF79EEE4277F7B
          C3DDD80080BC1C4EC728476FBDE8B3A36318000000A855D808000000B53BF1E0
          AFBE2B5D289EBB700C003084C3271EF8957745C72C00000050BBB011000000C6
          E0A13B6FFCC633A74F1ED8D878E12C00C010D2B1483A26898E55000000600CC2
          46000000188BA3BFFF559FF3DC5307DEBDB1F1C291F90BCA00003D3A928E41D2
          B148748C02000000631136020000C0D81CBBE767BF2F5D489EBBB00C00D08723
          E9D8233A2601000080B1091B010000608CEEFFA36FFBEAD32F3CFEB1745179EE
          22330040178EA4638D74CC111D8B000000C018858D0000003066CF1C7BFF2FA5
          0BCC73179C0100F6E2483AC6888E3D00000060CCC24600000018BBC7FEE47FB9
          7663E3F9C3C1C5670080153D7F381D5B44C71C0000003076612300000070F64F
          DDFBC1AF7FC90BCFDEFBDE0D77630300DA39928E25D2314574AC0100000008B0
          010000C05227EEFFF73F942E40CF5D900600D8CD91740C111D5B000000005BC2
          4600000060A7FB3FF6C6CB4FBFF0D847D3C5E8B98BD30000DB1D49C70CE9D821
          3AA600000000760A1B01000080D8C947FFD3CFA60BD37317AA01009223E95821
          3A8600000000626123000000B0D843076E7EDD99D34F1D082E5A030023958E0D
          D2314274EC000000002C163602000000BBBBEBF75EFAA79F3DFE915FDD703736
          0018BB23E998201D1B44C70C000000C0EEC246000000A099C70EFFD3376F9C39
          7528B8980D00D46E720C908E05A26304000000A099B01100000068EEEEF75EFA
          174E9DB8FD3737DC8D0D00C6E248DAF7A76380E8D800000000682E6C04000000
          56F7E81FFFE3EF3C73E659776303808AA57D7DDAE747C702000000C0EAC24600
          0000A09DBB7FFF659FFBECF10FFFEA86BBB101406D8EA47D7CDAD747C7000000
          00403B6123000000B0378F1CFA476F3A73FAE983C1C56F00A030699F9EF6EDD1
          3E1F000000D89BB011000000D8BBA3B77EF99F79E6D8FB7F69C3DDD800A05447
          D2BE3CEDD3A37D3D000000B0776123000000D09D870FBEE35BCFBC78E28EE0A2
          380090A9B4EF4EFBF068DF0E00000074276C04000000BA75E43D5FFA59271FF9
          8F3FBDE16E6C0090BB23699F9DF6DDD13E1D000000E856D808000000F4E3FE8F
          BDF1F2174FDDFF8174717CEE62390030AC23691F9DF6D5D13E1C000000E847D8
          08000000F4EBD83D3FFDBDE942F9DC857300601847D2BE39DA6703000000FD0A
          1B01000080FE7DEAFD577EC1A91377FCE6C6C68B4726CE02006B77E4D489DB7F
          33ED93A37D35000000D0BFB011000000589F473EF90FDF70E6C5A70E0417D501
          809EA47D6FDA0747FB66000000607DC24600000060BD8EBCE74B3EEBE423FFBF
          9FDA70373600E8DB91B4CF4DFBDE689F0C000000AC57D8080000000CE3D37FF8
          4D2F7FFEE9BB7E375D5C9FBBD80E00ECCD91B48F4DFBDA681F0C0000000C236C
          0400000086F5E81FFFE3EF3C73FA647AACA8201B00ECCD91B44F4DFBD6689F0B
          0000000C2B6C04000000F270E2C15FFB9174E17DEE423C00D0CC91B42F8DF6B1
          000000401EC246000000201FF77EE8FFF565CF3D75F0B7D245F8B98BF20040EC
          48DA77A67D68B46F05000000F2113602000000F979F8E03BBEF5F40BC76E4B17
          E5E72ED20300E71C49FBCAB4CF8CF6A5000000407EC246000000205FC7EFFBF9
          7FB2B1F182101B00ECF0C291B48F8CF69D00000040BEC246000000206F77BFF7
          E59FFBF463EFF9371BEEC6060047D23E31ED1BA37D2600000090B7B011000000
          28C37D1F79DD45CF9DFCE46FA78BF77317F301A07647D23E30ED0BA37D240000
          005086B01100000028CB4377DEF88D2F3EF7F047D2C5FCB98BFB00509B23699F
          97F67DD13E11000000284BD80800000094E9F1233F7CC399D3CF1C4C17F7E72E
          F60340E98EA47D5CDAD745FB40000000A04C612300000050B627EFFFA51F4C17
          FAE72EFC0340A98EA47D5BB4CF03000000CA163602000000E5BBFBBD977CDEC9
          477EFBA7D345FFB910000094E248DA97A57D5AB4AF03000000CA173602000000
          F5B8E7035FF3C5CF1CFBC02FA510C05C280000727524EDBBD23E2CDAB7010000
          00F5081B01000080FA7CFA0F5FFFF25327EEF8CD140A980B0900402E8EA47D55
          DA6745FB32000000A03E612300000050AF073EFE1D573DFFF491FF92420273A1
          010018CA91B46F4AFBA868DF05000000D42B6C04000000EAF7D09D377EE30BA7
          EEFFC0C6C6E9A3136701600047D3BE28ED93A27D1500000050BFB01100000018
          8F870FBCED9B04D90058B3CDE05ADA0745FB26000000603CC246000000607C04
          D9005803C1350000006087B011000000182F4136007A20B80600000084C24600
          0000004136003A20B806000000EC2A6C0400000098116403A005C135000000A0
          91B01100000060DE83B7EFFFEBCF9DFCE3DF49A184B9900200CC1C4DFB8AB4CF
          88F62500000000F3C24600000080453EFDD16F7DE5B3C7FFF0D75248612EB400
          C0781D4DFB86B48F88F61D000000008B848D00000000CBDCFBC1AF7FC9C9477F
          E7675368612EC400C0781C4DFB82B44F88F61500000000CB848D000000004DDD
          FDDE4B3EEFC403BFF2AE8D8D178F04C10600AAF4E291B4ED4FFB8068DF000000
          00D054D808000000B0AABB7EF725FFF5B17B7EFA7BCF9C7EFAE086BBB201D4E8
          68DAC6A76D7DDAE647FB020000008055858D000000007BF1C8A1EFFF8E174E7D
          FA7D29EC30177E00A03C47D3363D6DDBA36D3E000000C05E848D000000005DB8
          FF637FEFAA678FFFE1AFA5F0C35C180280FC1D7DF6F8477E356DCBA36D3C0000
          004017C246000000802E7DEAFD577EC189077EF55D1B679E3F9C021173010900
          F271346DABD3363B6DBBA36D3A0000004097C24600000080BE3C7EE45D37BEF8
          FCE31FDB0C49C4E10900D6EF68DA36A76D74B4ED06000000E84BD808000000D0
          B707EFB8FEB5A79EFCF86FA4D0C45C880280F5399AB6C5699B1C6DAB01000000
          FA163602000000ACCBA7DE77C5171CBFEF7FFB8133A79F3998821473C10A00BA
          77346D73D3B6376D83A36D33000000C0BA848D000000004378F813DFFDCDCF9D
          FCE3DF49E18AB9B005007B77346D63D3B636DA06030000000C216C0400000018
          D2BD1FFCFA979C78F0D77E64E3CCF38753E0622E8001407347D3B6346D53D3B6
          35DAE6020000000C296C04000000C8C5A387DEF91DCF3F7DE4BF6C8630E27006
          00E73B9AB69D691B1A6D5B0100000072113602000000E4E69E0F7CCD171FBFEF
          7FFB81D32F3E75200533E6821A004CB68D691B99B695699B196D4B0100000072
          133602000000E4ECC1DBF77DC3334FBCFF973636CE1C9D380B307247D336316D
          1BA36D2600000040CEC2460000008012DCF57B7FE54F3F76E4876E78E1D4031F
          48018EB9400740CD8EA66D5FDA06A66D61B48D040000002841D808000000509A
          FB3EFC8D5FF1E4FDBFFC43674E3F7330053BE6821E0035389AB671695B97B679
          D1B610000000A034612300000040C9EEFFD8DFBBEAE423FFF1A7CF9C79FE700A
          7CCC0540004A72346DCBD2362D6DDBA26D1E00000040C9C246000000805A3C74
          E78DDFF8CC13EFFFA51402998A02220039D9DC5EA56D57DA8645DB3600000080
          5A848D00000000357AE493DFFBC65327EE7CF7341C128546008674346DA3D2B6
          2ADA8601000000D4286C04000000A8D9D15B2FFE9CC70EFFCFD708B30119387A
          EAC41DBF99B64969DB146DB3000000006A1636020000008CC55DBFF7D23FFDC8
          A1EF7BD3B3C73FFA6B294832172C01E8C3D1B4CD49DB9EB40D8AB64D00000000
          631136020000008CD191F77CE9673DFC89EFFEE6678E7DF09753C0642A0A9F00
          AC62737B92B62D691B93B635D13608000000608CC24600000000CEFEA987EEBC
          F9754F3FF67BBF70E6CCF387A7019428980210399AB61D691B92B625D1360600
          00000001360000008046EEFFA3BF7BF9939FFEDFFFD98BCF3FF6D1144C990BAA
          002447D336226D2BD23623DA9600000000B053D808000000C0629F7AFF555FF8
          D8911FBAE1D4893BDF9D022B53519805A8DBE6F73F6D0BD236216D1BA26D0600
          0000008B858D0000000034971E0F78F291FFF4B3674E3F7B681A6889822E401D
          8EA6EFFAC9477EFBA73D1A1400000060EFC24600000000DAB9F7437FE3A58FDF
          F52F6E3E75E2F6DF4C4197A928040394617A97B5DB7F337DB7D3773CFAEE0300
          0000D04ED80800000040371EF8F8775C75FCBE7FFB032F9C7AE003D3204C1490
          01F272347D67D377377D87A3EF3600000000DD081B01000000E8DED15B2FFE9C
          873FF1F66F3EF9F06FFDD4E9174F1E482199B9D00C308CA3E93B99BE9BE93B9A
          BEABD1771800000080EE858D00000000F4EF53EFBBFC2F3DF2C9EF79E3530FBF
          FB5F9E7EE1F86D29443317AA01FA71347DE7D2772F7D07D37731FA8E02000000
          D0BFB01100000080F5BBFBBD977CDEC307DEF64D4F3DF81B3FF6E2738F7C2485
          6CE64237403B47D3772A7DB7D2772C7DD7A2EF2000000000EB17360200000030
          BCA3B75EF4D90FDD79E3373E79FFBFFFA1E79FBEEB775308672A0AE800E76C7E
          4FD277267D77D277287D97A2EF1800000000C30B1B01000000C8D37D1F79DD45
          8FFDC90F5EF7F4A3EFF937A75F3871C734AC138578602C8EA6EF42FA4EA4EF46
          FA8E44DF1D00000000F214360200000050867467A9076FBFF61B8EDDF3AFDF79
          EAC49DEFDED838ED2E6DD46CB26E9F3E9AD6F5B4CEA775DFDDD500000000CA16
          360200000050AE4FBDFF555FF8D09D37BFEEF8BDFFE69F3C7BFC8F7EFDCC99E7
          0F9F0BFE848120C8D5D1B4EEA67538ADCB699D4EEB76B4CE0300000050AEB011
          00000080BADCFDDE577CDE03B75DF3754FDCFD13DFFDCCE3EFFBC5D32F3C7560
          6363E353136721039F4AEB645A37D33A9AD6D5B4CE46EB320000000075091B01
          000000A8DF5DBFFB65FFCDA7FFF09B5EFEC8A1EFFF8E273FFDEF7EF0D493B7FD
          C699D3A70EA530D15CB808BAF2A9B48EA5752DAD7369DD4BEB605A17A3751400
          000080FA858D000000008CD75DBF7BA1601B7BB520A876A1A01A000000003B84
          8D0000000030EFC87BBEE4B3EEFDE0D7BFE4C13B6E78EDE377FD8B9B4F3CF87F
          FDD8A91377BEFBCCE96767E13601B7F1D85CDE69D9A77520AD0B699D48EB465A
          47D2BA12AD4300000000302F6C040000008055DDF381AFF9E2076FDFF70D8FFD
          C90F5E77E2FE5F79D733C73EF4CB2F3C7BFF0736CE9C3E3A0B3C4DC34FE4EFDC
          F29A2CBBB40CD3B24CCB342DDBB48CD3B28ED601000000005855D80800000000
          5DFBD4FBAFFAC2FB3FF6ED573E72F07BDEF8C4DD3FF90F9E7AE83FFCF8B34F7E
          FC375E7CEED18F7E263025E4B60E9F99EB34F76919A4659196495A366919A565
          152D4300000000E85AD8080000000043B8FB0F2EFB8BF77EF86F7EC503B75DFD
          758F7CF27BDF981E4BF9E4A77FE9074F3EFA9FFF757A54E58BA71EFE883BBA85
          CECDC7646ED21CA5B94A7396E62ECD619ACB34A7696ED31C47730F0000000043
          081B01000000207747DEF3259F75F77B5FF179F77CE0AF7DE9A7FFF09B2F79E0
          B66BBEEEE14F7CF7373F7AF807AE79E2EE9FF8EE273FFDBFFFB3A71EFABFFFE5
          D38FDDFA0BCF1EFFD8AF9F3A71E0DDCF3FF3A95B53C0EBF40BC76FFB4CE82B13
          A94FA96FA98FA9AFA9CFA9EF690C692C694C696C698C69AC69CC69EC690ED25C
          44730400000000B90B1B010000000000000000A06F6123000000000000000000
          F42D6C04000000000000000080BE858D000000000000000000D0B7B011000000
          000000000000FA163602000000000000000040DFC246000000000000000000E8
          5BD8080000000000000000007D0B1B010000000000000000A06F612300000000
          0000000000F42D6C04000000000000000080BE858D000000000000000000D0B7
          B011000000000000000000FA163602000000000000000040DFC2460000000000
          00000000E85BD8080000000000000000007D0B1B010000000000000000A06F61
          23000000000000000000F42D6C04000000000000000080BE858D000000000000
          000000D0B7B011000000000000000000FA163602000000000000000040DFC246
          000000000000000000E85BD8080000000000000000007D0B1B01000000000000
          0000A06F6123000000000000000000F42D6C04000000000000000080BE858D00
          0000000000000000D0B7B011000000000000000000FA16360200000000000000
          0040DFC246000000000000000000E85BD8080000000000000000007D0B1B0100
          00000000000000A06F6123000000000000000000F42D6C040000000000000000
          80BE858D000000000000000000D0B7B011000000000000000000FA1636020000
          00000000000040DFC246000000000000000000E85BD808000000000000000000
          7D0B1B010000000000000000A06F6123000000000000000000F42D6C04000000
          000000000080BE858D000000000000000000D0B7B011000000000000000000FA
          163602000000000000000040DFC246000000000000000000E85BD80800000000
          00000000007D0B1B010000000000000000A06F6123000000000000000000F42D
          6C04000000000000000080BE858D000000000000000000D0B7B0110000000000
          00000000FA163602000000000000000040DFC246000000000000000000E85BD8
          080000000000000000007D0B1B010000000000000000A06F6123000000000000
          000000F42D6C04000000000000000080BE858D000000000000000000D0B7B011
          000000000000000000FA163602000000000000000040DFC24600000000000000
          0000E85BD8080000000000000000007D0B1B010000000000000000A06F612300
          0000000000000000F42D6C04000000000000000080BE858D0000000000000000
          00D0B7B011000000000000000000FA163602000000000000000040DFC2460000
          00000000000000E85BD8080000000000000000007D0B1B010000000000000000
          A06F6123000000000000000000F42D6C04000000000000000080BE858D000000
          000000000000D0B7B011000000000000000000FA163602000000000000000040
          DFC246000000000000000000E85BD8080000000000000000007D0B1B01000000
          0000000000A06F6123000000000000000000F42D6C04000000000000000080BE
          858D000000000000000000D0B7B011000000000000000000FA16360200000000
          0000000040DFC246000000000000000000E85BD8080000000000000000007D0B
          1B010000000000000000A06F6123000000000000000000F42D6C040000000000
          00000080BE858D000000000000000000D0B7B011000000000000000000FA1636
          02000000000000000040DFC246000000000000000000E85BD808000000000000
          0000007D0B1B010000000000000000A06F6123000000000000000000F42D6C04
          000000000000000080BE858D000000000000000000D0B7B01100000000000000
          0000FA163602000000000000000040DFC246000000000000000000E85BD80800
          00000000000000007D0B1B010000000000000000A06F61230000000000000000
          00F42D6C04000000000000000080BE858D000000000000000000D0B7B0110000
          00000000000000FA163602000000000000000040DFC246000000000000000000
          E85BD8080000000000000000007D0B1B010000000000000000A06F6123000000
          000000000000F42D6C04000000000000000080BE858D000000000000000000D0
          B7B011000000000000000000FA163602000000000000000040DFC24600000000
          0000000000E85BD8080000000000000000007D0B1B010000000000000000A06F
          6123000000000000000000F42D6C04000000000000000080BE858D0000000000
          00000000D0B7B011000000000000000000FA163602000000000000000040DFC2
          46000000000000000000E85BD8080000000000000000007D0B1B010000000000
          000000A06F6123000000000000000000F42D6C04000000000000000080BE858D
          000000000000000000D0B7B011000000000000000000FA163602000000000000
          000040DFC246000000000000000000E85BD8080000000000000000007D0B1B01
          0000000000000000A06F6123000000000000000000F42D6C0400000000000000
          0080BE858D000000000000000000D0B7B011000000000000000000FA16360200
          0000000000000040DFC246000000000000000000E85BD8080000000000000000
          007D0B1B010000000000000000A06F6123000000000000000000F42D6C040000
          7AF6FFFE7C806CDC7BF5975D7C70DFC5AFBF6DFFA537DC79DDA5DFDB95F4F33E
          B1EFABBE35FACCE244DB720000000000F62C6C0400007A168523007AF6896B2E
          FE86286876EC96CBEE9DFCFDD9BEA49F3FFF9977ECBFE4E643D77CE5E593BF0F
          FB9A9D685B0E00000000C09E858D000040CFA27004A107F6BDF45FC332D1BAC3
          E7FFA9E88E6A93F630643684F9BEDD71EDCBDF30690FC732B8685B0E00000000
          C09E858D001422BAB0C6427F70C355FB9A88FE2F50A168BBBA4E519F084DC349
          61F8051201B62DE9719DDB036193B670CE72357FA7B6ACEED0166DCB01000000
          D812D5543273FF9B2FBCE4D0BE8BBEE5C0FE57DC74E0BA4BDFD999C9CFFBE4BE
          8BDF187DE66845EB082C1036025088E84060A466E1B3C7DE7EC59164E39AFF6E
          6366F2F7E105D265B6FF8CF43385DCA032D176759DA23E1112606399B107D8D2
          5DCBB687BE266DE13C9568FBB88A7ADC28300ADBCF196B76EB5B5EF3E668FC2C
          77F5F7BCE3928D7D5FB4C1F9A2F92A99657DBEFBBEF7B51F8FE60A3645751200
          EA106DF70772E89A8B5E1B05CD8EDF72D97D93BF0F6B515D483F7FFE33EFDC7F
          C977DD75F54B5F35F9FBB0AFD58AD61158206C04A010D181C008CC826AB362FA
          A42D3C40EC5BFAECEDC1B6495BD85F2053D176759DA23E11126063993106D80E
          EEBBF8B5DBC35D93B6706E6A3237DE705E00D62585BA863C1F5DA7C7DE7EE59F
          4C5EC379607729D434790DE775CC52B8693A37E1BC95681AD60AC73B56B904D8
          1EDCF7577E8EE19DB76CA23A4987EEB8EED27F5092F3E667A4A2B9C951D4F72E
          A51A072C13AD3B7CFE9F8AEEA836690F8F558630DFB703FB5EF6A6497B38966A
          04FB6958246C8436FC96D962E1C6BA6096F5F9360B32C1F7A277C1F2A9CD2C1C
          9643606D9959FF66A1B6495B38269A296D5B73CFF7BFEEA3D138C626CD43343F
          390AB7AB6B74F4DA8BFEF993375FF2E9D2A47E47CBBE4FD3A24CB8ED85644C85
          BBB91057381F63309B8383FB2E7EFDE4CFE15C8DDD2C5CC3392980B39B345FDB
          45730ADBA5F566F21A6EA36A331D6B380FEC4E806DB1DA026C9BE798C138C72C
          9700DBC91B2F7E78F21AF691F5982E839DCB26A89374E9D82D977D2A7D760936
          AEFEC28D7584A272379D83708E72D3F7F25207631901B62DE9719DDB036193B6
          70CE72357FA7B62AEFD016ECA76191B011DA509089A5E2456D0519BF51783E01
          B66E9512585B66D6FF59086FD2168E975869FB95B4BD17623B17609BBC867394
          931C026C290C16F52D77026CE4680C85BB5960EBD82D97DD3BF973380F63349B
          97DBF65F7AC3E4CFE1DC8DD5348415CE1BCBCD8EE5A390DBE4EFC339675C4A3E
          575D551AAB75BF1DF5D2C5720937754580ED7C026CCC08B02D27C426C0B69D3A
          18CB8C3DC096EE5AB63DF435690BE7A944DBC7554D982DD84FC3226123B4A120
          B3586D01360599F309B0EDDDF6D0DAE4CFE13C976C76014C90ADB912F72B3586
          965725C0D69C005B730A772C5373E16E16D09ABC0FC7CE39826CE71360EBCFEC
          D87E7BB06DD21E2E07EA949679ADE7AE8BA4757DF21ACE078BA9972E36AD2F86
          F3561ACB3926C0C68C005B33D33EEF9CA7111160DBA20EC632630CB01DBAE6A2
          D76D0F774DDAC2B9A9C9DC78C3792942B09F8645C24668C389FA627EA3B07E02
          6CEDA540D7EC02D0E4CFE1FCD6E6DCC52E8F195DA6D4FD4A4D45F83604D89A13
          606B4EE18E656A2CDC09AEB523C8B645806DBD66E734026DE3300D7385EB42AD
          04D8DA512F5D2C9D934DE7279CBB924C6BBFE138C74C808D1901B666D25DD8C6
          1C621360DBA20EC632630AB0CD85B8C2F91883D91C1CDA77D1B74CFE1CCE55B6
          82FD342C1236421B0A328BF98DC2FA09B0ADA6F6BBAD3535BBC825C8162B39C0
          36E647890AB03527C0D69CC21DCBD454B8135CEBC66C1E0FEEBBF8B5933F8773
          5D3B01B661CDC26C423F751AE3B96C1AB370E6EAD4D17627C0563701366604D8
          9A1BF3A34405D8B6A883B1CC18026CB3C0D6F15B2EBB6FF2E7701EC668362F07
          F6BFE2A6C99FC3B9CB4EB09F8645C2466843416631BF51583F01B66604D7163B
          7781EB8A2393F7E1DC8D51C9FB959AB6FBAB12606B4E80AD39853B96A9A17097
          EE1826BCD6BD6D731ACE7BCD04D8F2310BB309FFD4212DC7B19ED30A64AE4EBD
          7477B9049CF66AF3FC3218DFD809B03123C0B69AB186D804D8B6A883B14CCD01
          B659406BF23E1C3BE71415640BF6D3B048D8086D28C8EC4E80AD6E026CBB1BE3
          6342DB1264DB52FA7E65AC213601B6E604D89A53B86399920B77C7DFFCC52F11
          5CEBD7B15B2EBB37CDEFD81E2B2AC0962761B6F24D435CE1F2AD9D00DBEAD44B
          77370D7E8573570ACB783101366604D85637C647890AB06D510763991A036C82
          6BED1411640BF6D3B048D8086D3859DF9DDF28AC9B005B4C70ADBD3467637FB4
          680DFB951A8AF1AB12606B4E80AD39853B9629B57027B8B65EDBE63B5C1EB511
          60CB9B205BB9C67C7E3B1D7B382FC4D44B7797CECBA67314CE5F09FCB2EF6202
          6CCC08B0AD2EDD856D6C213601B62DEA602C5353804D70AD1BB3793C74CD45AF
          9BFC399CEBC104FB6958246C8436146476E7370AEB26C0B69347857667CC41B6
          5A026CD3405738C61A09B03527C0D69CC21DCB9458B8135E1BC8D5176C06D9EE
          B8F6E56F98FC395C36B510602B83205B59D2721AFB79AE7575356A69CB09B0D5
          4B808D1901B676C6F6285101B62DEA602C5343802DDD314C78AD7BDBE6349CF7
          4104FB6958246C8436146476E7370AEB26C0B64570AD7BE72E6A5D71646C41B6
          5AF62B356CFF5721C0D69C005B730A772C5352E12E3DC652706D78DB0284E172
          AA81005B5904D9CA30E6C787CE788CE86AD44B97CB25E4D4D6E6B965302E04D8
          D822C0D65E0AB14D5E77CE5DA504D8B6A883B14CC901B6136FFEA22F175CEBD7
          F15B2EBB6F738E7379AC68B09F8645C246684341663901B67A09B06D3D2E74F2
          3E9C23F62ECDEF98426C35ED57A605ED709CB511606B4E80AD39853B9629A570
          B72D34158E83F5AA3DC426C056A659906DF23E5CAE0CCB39EFB97574F21ACE0F
          E7532F5DAEE4F3E5B47C05D81613606346806D6FC6F2285101B62DEA602C536A
          804D706DBDB6CD77B83CD626D84FC3226123B4A120B39CDF28ACD7D8036CEEBA
          B63EE72E688DE36E6CB5ED57C6126213606B4E80AD39853B9629A17027BC96A9
          E923450F5E73F1374CFE1C2EBB5209B0954B882D4FBE535BDC29B039F5D2E5D2
          B9D9749EC239CC995FF6DD9D001B33026C7B93EEC23686109B00DB1675309629
          31C026BC3690AB2F381764DBF7B2374DFE1C2E9BDE05FB6958246C8436146496
          2B39BC9096AF00DB62630DB0B9EBDA70D2BCD71E62AB31C0360D7785E3AD8500
          5B73026CCD29DCB14CCE85BB83FB2E7EADE05AFED232BA7DFF25551D5B09DB94
          2F1DF30B0AE5C3E343B7085836A75EDA4CA9BFF42BC0B63B01366604D8F62E85
          D8FA0E4D0D4D806D8B3A18CB9414604B8FB1145C1BDEB60061B89C7A15ECA761
          91B011DA509059CE6F14D66B8C0136E1B5E19DBB33C3154726EFC36554BA1AF7
          2B25EF079A12606B4E80AD39853B96C9B57077DBFE4B6F105E2B475A5677ECBF
          E4E6C9FB7079964680AD0EE78EF9858572E0FC77CB742EC2796227F5D2664A0D
          B06D9E5706E3E11C01366604D8BA91426C93D79DF3581101B62DEA602C534A80
          6D5B682A1C07EB3558882DD84FC3226123B4A120D34CA9051901B6DD8D2DC0E6
          91A179A9F5E241ADFB9569813B1C730D04D89A13606B4EE18E65722CDCA52094
          F05A79D2329B2EB770B9964480AD1E426CC3F37D3A9FBB0336A35EDA4C89E7C9
          69D90AB0ED4E808D1901B6EED4FC285101B62DEA602C5342804D782D53D3478A
          1EBAE6A2D74EFE1C2EBBCE05FB6958246C843614649A2935C0A620B3BBB104D8
          DC752D5F69B9D4F648D19AF72B353F4A5480AD3901B6E614EE5826B7C2DDB610
          54D85FF2564B884DE0A62E426CC3F2F8D0F3591F9B512F6D269D9F4DE72A9CC7
          1C59B6CB09B03123C0D69D7417B65A436C026C5BD4C15826E700DBA16B2E7A9D
          E05AFED232BAF3DA575C3F791F2EC74E05FB6958246C84369CB437E3370AEB34
          86009BF05AFE6A0BB1D5BC5F49DBD45A436C026CCD09B035A770C7323915EE84
          D7EA5043884D80AD3E426CC3712E7CBEE99C84F3C516F5D2E64AFBA5DF697FC3
          B1708E001B33026CDD4A21B6BE0354431060DBA20EC632B906D80EEC7FC54DC2
          6BE5D80CB1EDBFE4BB26EFC3E5D999603F0D8B848DD086824C337EA3B04EB507
          D884D7CA515388ADF66D4F89FB832604D89A13606B4EE18E657229DC09AFD525
          2DCBF428D8C9FB7079E74E80AD4E426CEBE7BBB498C7882EA7A6D65C6901B6CD
          73CA601C6C1160634680AD7B35DE854D806D8B3A18CBE418604B4128E1B5F2A4
          65365D6EE172ED44B09F8645C246684341A6B9D20A32D3FE8663E19C9A036CC2
          6BE5A925C43686FDCAB4E01D8EBF54026CCD09B035A770C7323914EE52D04978
          AD3E6999DEB6FFD21B26EFC3E59E33A19B7AA5E37DC1A1F5F1F8D0C584299753
          2F6DAEA4F3E3B45C05D8961360634680AD1FB585D804D8B6A883B14C6E01B66D
          21A8B0BFE4ADF7105BB09F8645C246684341A6B9D2026C0A32CBD51A607BECED
          571C115E2B530D21B6B1EC576A7B94A8005B73026CCD29DCB1CCD085BB147012
          5EAB575AB607F75DFCDAC9FB70F9E74A80AD6E426CEBE39C78B1E9DC84F3C639
          EAA5CDA573B4E97C85739913CBB51901366604D8FA51DBA34405D8B6A883B14C
          4E0136E1B53AF41A620BF6D3B048D8086D38716FCE6F14D6A7C6009BF05AF94A
          0FB18D65BF92B6B13585D804D89A13606B4EE18E65862CDC1DDC77F1EB85D7EA
          375DC6E13A902B01B6FAB9FB55FFDC7D6D778294CBA997AEA6945FFA9DF6331C
          035B04D8981160EB4F4D213601B62DEA602C934B804D78AD2E6959A647C14EDE
          87CBBBB5603F0D8B848DD086824C737EA3B03EB505D83C36B41E2587D8C6B4FD
          2969BFB08C005B73026CCD29DCB1CC90853BE1B5F1282DC426C056BF74AC2FC4
          D62F01B6E5AC83BB53575B4D2901B6CDF3C9A0FFEC24C0C68C005BBF6A7994A8
          00DB16753096C921C096824EC26BF5D90C25EE7FC54D93F7E1726F25D84FC322
          6123B4A120B39A520A32D37E8663604B4D0136E1B5FA941A621BDB7E655A000F
          E7A224026CCD09B035A770C7324315EE84D7C6252DEF92426C026CE3303D770B
          D701F6267D879C1B2F671DDC9D7AE96A4A392F16606B46808D1901B67EA5BBB0
          D510621360DBA20EC6324307D852C04978AD5E69D91EBAE6A2D74DDE87CB7F65
          C17E1A16091BA10D0599D5941260539069A696009BF05ABD4A0CB18D31C056C3
          A34405D89A13606B4EE18E658628DC6D0B33857DA24E6999DFB6FFD21B26EFC3
          F52227026CE3E10E58FD70F7B566D2B9A6C7882EA65EBA9A749E369DB3703E73
          60993627C0C68C005BFF6A7894A800DB167530961932C07668DF45DF22BC56BF
          E9320ED7819505FB6958246C84369CBCAF661A0C8B37E41911606BA686009BF0
          5AFD4AFBCDF831EE574A28D62F23C0D69C005B730A772CB3EEC2DD1DFB2FB959
          786DBC4AB90B9B00DB780810F5C3F9717342948BA997AE2EF75FFA9DF62FEC3B
          3B09B03123C0B61EA587D804D8B6A883B1CC900136E1B5F1E82CC416ECA76191
          B011DA5090598DDF28AC4B0D0136C5F97178ECED571C99BC86EB406EC6BA0D2A
          3DC426C0D69C005B730A772CB3CEC2DDA16BBEF272E1354A08B109B08D8B0051
          B7D2F7C7397273A5FDB2D43A93E1E2F100007BA449444154A9ADAD4E80AD1E02
          6CCC08B0AD4FC98F121560DBA20EC6324305D884D7C6252DEF4E426CC17E1A16
          091BA10D0599D529C8D46373AE82EF45EF82E5D6C634D4148E8DBAA40B0BA584
          D8C6BC5FD90C570573520201B6E604D89A53B863997516EE84D748D27A70C775
          97BE7DF23E5C4F7220C0362E0244DDF2F8D0D5A4F5CF5D0063EAA5ABCBFD7C78
          DABFB0EFEC24C0C68C00DBFAA4BBB0951A621360DBA20EC6324304D8B68599C2
          3E51A7CDE5BEFF15374DDE87EB4523C17E1A16091BA10D0599D509B0D5A3E400
          9BF0DAF8A48B0BE991B193F7E13A918BB107D8A641B0706E7226C0D69C005B73
          0A772CB3AEC25D0A2D09B03193FB5DD804D8C64780A83BD3406038CFC4DC0530
          A65EBABA74AE369DB7704E876479AE46808D1901B6F52AF551A2026C5BD4C158
          66DD01B63BF75FF25DC26BE3B5E7BBB005FB6958246C84369CC0AFCE6F14D6A3
          D4005B0A3129CC8F5309776818FB7E25E7A2FD6E04D89A13606B4EE18E65D651
          B83B78CDC5DF20BCC6BC9C436C026CE32340D48DF4DD719EBCBA12CE3187A05E
          DA4EAEBFF4EB977D5723C0C68C00DBFAA510DBE475E79C674E806D8B3A18CBAC
          33C076D7D52F7D95F01A7B0AB105FB6958246C8436146456E7370AEB516A804D
          517EDC727F94A8EDD0346415CC4DCE04D89A13606B4EE18E65D651B8135E2392
          D68B3BAE7DF91B26EFC3F56648026CE393CEEFDC856DEF3C3EB41DEB5FCC796D
          3B026C7510606346806D18A53D4A54806D8B3A18CBAC33C026BC4692D6833BF7
          5FF28EC9FB703DD955B09F8645C246684341A61D05993A6CCE57F0BDE85DB0EC
          9AF2E850D205869C1F256ABF724E69213601B6E604D89A53B86399BE0B7729A4
          24C0C622B9DE854D806D9C0488F6CE2F7AB5E72E80E7735EDB4EAEE7C1D37E85
          7DE67C026CCC08B00D23DD85ADA4109B00DB167530965957802D859604D89869
          7D17B6603F0D8B848DD086824C3B026C75282DC0E6D1A1CC4CD783703D199AFD
          CA39A9403E0D8585F3941B01B6E604D89A53B863997504D826AFE167C3B68063
          B8FE0C45806D9C0488F6267D6F723C573EF8D6AB7EE117F77DED97DD7EE3953F
          1AFD7D2E723EBF1C8AF3DA76D2F9DA74EEC2791D42EA8F00DB6A04D89811601B
          4E0AB1F51DB6EA8A00DB16753096594780EDD03517BD56788D79AD426CC17E1A
          16091BA10D059976A6858F78833E200599D5941660135E63BB5C1F256ABFB225
          C7E2FD22026CCD09B035A770C7327D16EE368349575F107E2ECC08B075EBC337
          BD7A4FFB9A5FB8F6EB5FFAA11B5FFD03B7DFFCEA9F79EEAD2F7B6ED2167E4E8D
          0488F626D7C7877EE2AD576DEEE77E7EDFD75D30FF773949EB9FBB00EEE4BCB6
          BDDC7EE9D72FFBAE4E808D1901B661A510DBE475E7FC6748806D8B3A18CBAC23
          C026BC4624AD1707F6BDEC4D93F7E17A130AF6D3B048D8086D28C8B493632821
          F547806D352505D83C3A9479E922438E8F12B55FD969BA5D0EE72A27026CCD09
          B035978A32A599F43B9CBF1244E329C1A4EFE1FAB317775DFDE557BAFB1A4D1C
          BBE5B27B730BB1951C60FBC05BAEFA8793D7705C6DFCD36BFFF6E7A5305B32F9
          73F899B51020DA9B5C7FD9EBD6EBAEFCF6C9EBAC9FE1BFC985BB00EEE4BCB63D
          01B673E7ADE973B78BFE5DAEA6FD0DE7739D04D88627C036BC121E252AC0B665
          5AE3083F1B92BEEA60339B212501361658F92E6CC17E1A16091BA10D0599F672
          39999F29AD189283CD390BBE17BD0B96DF6E3C3A944572BC0B9BFDCAF94A7894
          A8005B73026CF52ABDD0D87711AC24630FAF9DBEEEC2A75715FD9CB11060EB4E
          D701B6ED3E7CD36BFE79ED413601B676D2BCE57ABE7CD57537FC3F26AFB3BE86
          FF2617026C3BE57C5E9BFAF677DFF6D697BCF9EDB7BC7CF2E7EC9EC690DB2F71
          0D313FF335E3D26AB6B9D4BC05D88627C036BC7417B6DC436C026C5B04D85866
          1D01B6C96BF8D9B02DE018AE3FE709F6D3B048D8086D081AB497CBC9FCCC10C5
          902A7EA330F85EF42E587EBB115E6391B46EE4761736FB95F3A56D65EE213601
          B6E604D8EA25C0568783FB2E7EFDD8026CE705D2AEBFF0E4A43D9C9FC879FF7F
          62D21E7E568D72BB0B9B00DBEE6A0EB10910B593EBE343A73ED3CF33D75F787A
          5B7B76D2B9A510E596DC036C93D7CFF4758880D632F37D1C4AEA8700DBEAE6FB
          3F9452036C0FEEFB2B3F37790DC75485A04ED22501B6F3A5105BDFC1ABBD1060
          DB22C0C6327DD6EE368349575F107E2ECC08B0D197B011DAC8BD20E3370A9B53
          9059DD667F83EF45EFB6CDD9321E1DBAE5C51BBFE29979D1BF1B9B69C0315C7F
          8690F37E6548691B9D4B113F22C0D69C005BBD04D8EA30A6F0DA5CE02C9C8F36
          E67E6EF8D9B51160EBC63A026C49AD213601B67672FD85AF6960ED33FD3CF8D6
          AB7E61F677B9B20E6EC9BD5E3A79FD4C5F73ACC5E512801A6A3996B08C7693CB
          F21360CB545027E992005B2C85D826AFF132199800DB96549729CDA4DFE15C95
          201A4F09267D0FD79FBDB8FBEA97BEC6DDD768E2F82D97DDD738C416ECA76191
          B011DA28A920935B802D99EFE350523F04D856B7D9DFE07BD1BB6D73B6CCD8EE
          BE360BA63DF5B657DE7BD75BAFF8C5E443375CF19D93BF0BE7E7AEAB5FFA676F
          BFF18AEF9BFDDBF4FFC6166CCBED2E6C39EF578636DD4E87F3363401B6E604D8
          EA352D2285F35782BE8A602519CBDDD73E13305BF14E6BAB4A3F7F2C41B6B4DE
          E4126213606BA6C6105B6EBF9852829C1F1F3A0DAC7DA6AF026C6529A95E9A63
          2D6EBE5E389421E6269DB396B08C7693CBF21360CB545027E99200DB62B93E4A
          5480AD5CEA60F5187B78EDF475173EB3AAE8E78C85001B7D081BA10D0599BDC9
          E5847EA8E558454126F85EF46EDB9CED662C775FDB1E58FBD85B2EBF71D216CE
          4753EFBFF6955F35B6305B4E17BB72DEAFE420D747890AB03527C0562F85BBF2
          D51E5E3B3D0BAE9D0B948573D0876D9F19F6AB16026C7BB7CE005B525B884D80
          6D75393F3EF4F61BAFF891C9EB67FAFAC1EB2EBF76F677B94AEBA0C7889E5352
          BD34C7BEE6F2CB5BD37E847DEC4B34F622EBA573631882005BA6823A499704D8
          16CBF551A2026CE55207ABC3A17D177DCBD8026CA7E703692BFE82E779FF7F62
          D21E7E568D1ADF852DD84FC3226123B451524126C793FD5C4EE887989B6A7EA3
          30F85EF46EDB9C2D92EEA895EB6F92772585CB52D0ECD0CD57FEABC99FC379D8
          ABF4B3C710644BEB4A2E7761CB79BF9283B4EDCC31C426C0D69C005BBD14EECA
          76E89AAFBCBCE600DBE901826BDBCD3E7FDA87B08FA54BEBCF27F67DD5B74EDE
          8773B02E026CCDFD8B6BBFF1FFF9DC5B5FF6DCE47DD89FD208B0AD2EE773E69F
          DFF775174C5E3FD3D7775CFBCD7F76F677397317B6734AAA97A63F0F11D45A66
          BE9FEB36D4BC44B5E222EBA573631882005BA6823A499704D8769763884D80AD
          5CEA6075185378EDF4CEC059381F6DCCFDDCF0B36B23C046D7C24668A3B482CC
          ECEF72312D84ECDCA00F6088824C347601B686E6E62D52F3DDD766775CFBE44D
          57FCE8E4CFE1F8BB36BB23DBE47DD8A71AE472C12BE7FD4A2ED2F6737E1F3734
          01B6E604D8EAA5705736E1B5F5D8D697B0AFA5CBE12E6C026CABF9F8CDAFFEC9
          C96BD89F4285E3E47C057C57A27E47FF2E2B026CE7E47C5E1B858B86A80B2E33
          74086AA86558CAF2D9CDD0CB6E46802D53419DA44B026CCBE5F6285101B672A9
          83956F2C775F4BC1B24D2BDE696D55E9E7CF3E6BF2E7B02FB548EBCDD2105BB0
          9F8645C2466823E782CCFCC5FDF4E71C4FF8E7FBB96E43CD4B54CC98B685FF3E
          479BFD0DBE17BD9B9BB77935DF7D6D76D7B5C9FB70EC7D7AEFB5977D49FAEC5A
          EFC696CB5DD872DEAFE464BADD0EE77008026CCD09B0D54BE1AE6CB506D85258
          ACEF02DDAA36FB5469884D806D6F8608B02535DD85CDE31B9BCBF9F1A107DF7A
          D52F4C5ECFEBF399EB2F3C3D790DFF4F2EDC09F09C9CCF6B4BA9C745FD5CA7A1
          E624AA130BB0B523C096A9A04ED22501B6E5D25DD8720AB109B0954B1DAC7CB5
          87D76661B264F2E7700EFAB0ED33C37ED542808D2E858DD0466905991C4FF887
          3EA91F6A1996B27C76B33986E07BD1BBB9799B57EBDDD7D21DD03EFE96CB6F99
          BC0FC7BD2E3587D8A6EB4E38EE7511606B266D2F737A94A8005B73026CF552B8
          2BD766E8E8EA0BC27929D9E90CC36B339B7D1362EB8500DBEA6EBFF9D53F3379
          0DFB541A01B6E672FEA5AF4501B6697BF87F72E22E6C026C5D98D60877F4739D
          86A851A6CF1460EB8E005BA6823A499704D89AC9E951A2026CE552072BDB5D57
          BFF4553507D8B685C8C2F1F76DF6F9D33E847D2C5D5A7F3EB9EFE2374EDE8773
          10EDA76191B011DA5090D9BBA14FEA879A936A0A32C1F7A27773F336AFC6BBAF
          A5D0D887AE79C59F9BBC0FC7BC6EB586D872F86D7901B6E61615B78720C0D69C
          005BBD14EECA55E3DDD74E6F05C4C231E760DABFB0FF2513606B6FA800DB6FEC
          FF9ABF36790DFB541A01B66672FF9EBC67FF95AF9FBC9ED7EFDB6FBCE24726AF
          E1FFC989005B7901B65CFB3BE4F9EE1035CAE96766D197BD18BAD63D23C096A9
          A04ED22501B6E67209B109B0954B1DAC6CC26BEBB1AD2F615F4BB7EB5DD882FD
          342C1236421BA51564860A6BED665171625D862AC84445A8220B32C1F7A27773
          F3B6DDF41190617F4B35D4234397A935C436F45DD872DEAFE468D1F674DD04D8
          9A1360AB97C25D990EEEBBF8B5B505D852302C99BC0FC79C8B7477B8693FC371
          942AAD4F87AEF9CACB27EFC371F74D80ADB5B04FA511606B26E7C7874E85FDFE
          F97D5F77C1E435FAF759F1185101B6AE0C15841A6A3E168DB7C87A69308E7513
          60CB545027E99200DB6A727894A8005BB9D4C1CA566B806D333096D9D30836FB
          5469884D808DAE848DD086824C37A6FDDAD1D77519A20832FDCC2CFAB2179BEB
          58F0BDE85D307733B5DD7D2DD7F0DA4C8D21B6A12F36E4BC5FC9D5A26DEA3A09
          B03527C0562F85BB3209AF0D6B5B7FC3F1946AC8BBB009B0B5F3DC5B5FF6DCE4
          35EC5761C2F1B153CEE7CD8B1E1FBA4DF8FF7233F6306569F5D224C79ADC5041
          A8E9E7867DEA5349CB6637432DB779026C990AEA245D12605B4DBA0BDBD02136
          01B672A983956B337474F505E1BC942C85C4720BAFCD6CF66D6C21B6603F0D8B
          848DD086005B37863AB11F6A3EAA2AC804DF8BDE057397D476F7B5A7DEF6CA7B
          EFBAFAA57F76F23E1C6F2EA621BB700CA59AAE4BE178FB26C0B6BAB4ED9C06C8
          C2395D0701B6E604D8EAA57057A61A036C93D770ACB91260EB96005B3BB7DFFC
          EA9F99BC86FD2A85BB5E3593FB776459806DFAF7E1FFCDC9D81F232AC0D68D69
          9FCEEB6BDFA67314F6A94FD3F526EA53F8EF7335549D7B9E005BA6823A499704
          D85637F4A34405D8CAA50E56AE1AEFBEB62D20168E3907026CB058D8086D28C8
          7463A813FBA10A32252D9BDD6C8E23F85EF42E98BB64FAE8C7B0AFA549E1B58F
          DC70F9774CDE8763CD4DEAEFE4351C4B89867C8CA8005B3B69FBB94BB1BB7702
          6CCD09B0D54BE1AE3C07F75DFCFA9A026C290896EB6F9AEE66B3DF9585D8D27A
          35D4634405D8DA11601B8FDC1F1F7AEB75577EFBE435EC7B524A806DECEB6389
          F5D2A16A84CB0C719E3B447D72D1397D89359245EBD8BA09B0652AA893744980
          AD9D14629BBCC6CBAC67026CE552072BD3A16B2E7A5D6D01B6140C9B86C3C231
          E722D5ECA6FD0CC751AAB43EDD75F54B5F3579BF73CCC17E1A16091BA18D120B
          323986A4A67D3AAFAF7D1BAA38B54BF129FCF7B9DA9CBFE07BD1BB78EEAA797C
          687A2467EE8F0E9D77FB4D57FED39A1E253AE4C586128BB3B9186A5F9208B035
          27C0562F85BBF254175E3B17020BC79ABB6DFD0FC757A2A1EEC226C0D6CEC76F
          7EF54F4E5EC37E9562EC77BC6AAA80F3E6B0DF33EFB9EE55DF36798DFE5F76C6
          FC18D112EBA543D5089759D4DFBE0CB5EC169DCF97582359F7325B44802D5341
          9DA44B026CED0DF5285101B672A9839549786D58DBFA1B8EA754E15DD882FD34
          2C1236421B0A32DD99CEE579FDEDD31061BEF499D1588B2DC804DF8BDECDCD5D
          52D3E3434B0BAFCDD4F628D1A11E235AE2B620274385D804D89A1360AB97C25D
          796A0BB04D5EC37196623A86707C2512605BDD9001B6E96787FD2A8500DB72B9
          DF7D6DD9E343B709FF7F6EC6BC4E96582FCDB5CFEB0E430D55372E6DB9EC66DD
          CB6C1101B64C0575922E09B0B597EEC23644884D80AD5CEA6065AA31C036790D
          C79A2B0136385FD8086D94589019AA10B1CCBA4FEE875A76D5FD4661F0BDE85D
          307FB5DC7D2D3D8AF3FDD7BEF2AB26EFC371E6AEA647890EF518D112B7053949
          DBD869982C9CDFBE08B03527C0562F85BBB21CDC77F16B6B09B0A5E0572D01B6
          E938C2719646806D754306D83E7CD3ABD37E2EEC572904D896AB25C0E631A2F9
          CBF9BC76B75F7A9AFE5DF8FF86B25B7FFB3054DD78515DB8D87A693096751360
          CB545027E99200DBDEA410DBBA435A026CE552072BCFA17D177D4B4D01B61404
          4B8FE59CBC0FC79BABCD7E5716624BEBD5798F110DF6D3B048D8086DE47C125D
          DA89FFBA4FEE1564F66E732CC1F7A277C1FCD512602BF5EE6B33A9FFB53C4A74
          A88B0D256E0B72930AFCD3790CE7B80F026CCD09B0D54BE1AE2CEEBE96A7DA02
          6C292839791F8EB52F026CEDDC7EF3AB7F66F21AF6AB14637E5C6313697E723F
          6F7ECFFE2B5F3F790DFBBF5D2901B664ACEB65CEE7B5A505D892759EDF0E3507
          8BC658628D64DD35EE4504D83215D449BA24C0B67729C436798D975F0F04D8CA
          A50E569EEAC26BE74260E15873B7ADFFE1F84A74DE5DD882FD342C1236421B0A
          32DDD9ADBF7D1060DBBB5C026CB53C3E747AF7B2F3C6579A9AEEC236C463444B
          DC16E468DDFB1401B6E604D8EAA570571601B63C4DC7128EB34443DC854D80AD
          9DD2036C299825C0B6BBDCEFBE3615F67DDEADD75DF9ED93D7E8FF6767AC7706
          2CB55E3A54AD70997505A2D2721BA266BCDB3229B65E1A8C65DD04D83215D449
          BA24C0D68D753E4A5480AD5CEA60E5A92DC036790DC7590A0136D81236421BA5
          166486284634319DCFB0CF5D1B6A0E168DB1D8824CF0BDE8DDDCDCB9FB5A5EA6
          E308C7589A211E235AE2B62057EB7C94A8005B73026CF552B82B4B2D01B614F8
          2AF171098BA4B1D4146213605B8D005B7B43DD3DB924B53C3E749BF0E7E466AC
          EB66A9F5D25C036CBBF5B94B438D7FB7F1155B2F0DC6B26E026C990AEA245D12
          60EB46BA0BDBBA426C026CE552072BCBA16B2E7A5D2D01B614FCAA25C0361D47
          38CED208B0B1176123B4A120D3AD759DE0A7E5369D9FB01F7DA9B220137C2F7A
          3737773504D86AB9FBDA4C2D77611BE2624389DB825CA56DEEBA426C026CCD09
          B0D54BE1AE1C775CFBF2379CBDFA82701E4A330D7B85E32CD5744CE1784B23C0
          B69A21036C13619F4A31D6BB5C3595BE17B99F37AF1A60F318D1BC955A2FCDB5
          DFBBF5B94B43D58B77AB07175B2F0DC6B26E026C990AEA245D1260EB4E0AB1AD
          23B025C0562E75B0B2B8FB5A9E6A0BB0A5A0E4E4FDB9F105FB6958246C84364A
          2DC80C559058A6F6824C8945B2DD6CCE63F0BDE8DDB679ABE5F1A1B5DC7D6DA6
          96BBB009B0952F6D77A7731ACE775704D89A1360AB97C25D393C3E346F026C7B
          23C0B6BA9FBAF6AFFFE5C96BD8A7528C3120B48A121E1FFADBFB5FF53726AF61
          FF232505D8C618B02CB55E9AFA3DFDFBF0FF0E691DE7B5438D5D80AD1F026C99
          0AEA245D1260EB560AB14D5EE365D91101B672A9839545802D4F3505D8921D77
          610BF6D3B048D8086D945C9099BC86FF6F48BBF5B94B4305D8AA2CC804DF8BDE
          6D9BB7E9231EC3FE95E2C51BBFE2990F5CF7D57F6DF27EC7D84AF6E1EBFFEAB7
          A4714DDE87632EC93424198EB30F256E0B72B78E7D8B005B73026CF552B82B47
          558F0FAD34C0361D5738EED2DCB6FFD21B26AFE158FB20C0B63A8F0FAD5F2177
          2D0FFBBEC834F016FD9CEC8C711D2DE0BC36EC773254886B99BE435169990D31
          F6F499D3F525ECD75035DCBDE87B593525C096A9A04ED22501B6EEF5FD285101
          B672A98395A596005B0A7C9DBEFEC29393F7E1384B93C692C634791F8EB73402
          6CB41536421B39176476BB603F5451A289DD8A165D196AEC026C1DD9366F8514
          E27755DBE343676A798CE83424198EB10F256E0B4AD0F7A34405D89A1360AB97
          C25D396A0AB04D5EC33196AEA600DBBAEFC226C0B6BAD2036C1E1FBABBF49DC8
          FDBC79D5C7876E13FEBCDCA4F91FDB5D02733FAFDDADF6986B606AB73A6F1786
          5A66CBC625C0D69E005BA6823A499704D8BAD7F7A34405D8CAA50E568E03FB5E
          F6A6B3575F10CE4369A661AF709CA512600301363A5440D0E0BC1DC14CAE01B6
          BE4FF2D3321B62ECE9334B2C90ED66B3CFC1F7A277D3394B77C6AA21C056DBE3
          43673C46B49D02F62B454ADBE03E436C026CCD09B0D54BE1AE0C87AEF9CACB05
          D8F227C0D69E00DB6A3E74E3AB7F60F21AF6A704630C06ADAA84C7877EE2E6AB
          5A85153C46345FB99FD796589F9BD632C33E7761A8712FAB03175B2F0DC6B26E
          026C990AEA245D1260EB479F213601B672A98395C3E343F326C006026C744841
          A67B7D1764865A66CBC6556C4126F85EF46E3A67D3473B867D2B457ACCE6EFEF
          BBEC82C9FB1DEB430D3E78ED2B2FA9E131A2026CF548DBE1DDF68B7B21C0D69C
          005BBD14EECA307D9C633807254901AFDA036CD3F185E32F89005B734304D83C
          3EB47E25FCD2D77FB8F6D5974F5EC3FEEFA6A400DBD8D6D592EBA539F7BDAFF3
          D9645AB70C3FB74FCBC25EC5D64B83B1AC9B005BA6823A499704D8FAD3D7A344
          05D8CAA50E568EAA1E1F5A69806D3AAE70DCA539B0FF15374D5EC3FD342C1236
          421B251764722D00F41D601B6ADCD5166482EF45EFA673367DB463D8B752D4FA
          F8D0995A1E233A0D4B8663EC5AEEFB95D2F5B58F11606B4E80AD5E0A776570F7
          B5724CC7188EBF24026CCDAD3BC0567A782DF1F8D0DDA5EF430901B689B0FFCB
          FCDFD75EF53593D7E8E765272D8731DD2DB0E47A693254986B9965B5C5B6D27C
          0C35E665CBA2D87A693096752B35C096FA9D426C259B8C235C269B823A499704
          D8FAD547884D80AD5CEA60E5A829C036790DC758BAAA026CB3BBB005FB695824
          6C84364A2EC8E4DCF765C58BBD18AA20B3AC78516C4126F85EF46E3A678514E2
          7755EBE343676A798CA8005B3DD23EA08F47890AB03527C0562F85BB3208B095
          A39600DBB15B2E5BEB2F6C08B035F3FB37BC3AFD4670D88F528C2D10D446098F
          0F9DDE452DEC7F43E1CFCDD198029725D74B935C6B74D39A66D8E7BD186A7935
          194FB1F5D2602CEB566A80AD74026C75EBE351A2026CE552072BC35D57BFF455
          026CF9136063ECC24668A3F482CC5061AE65FA3AD14FF331D4984B2D8EED66B3
          CFC1F7A277D339AB21C0F6C99BAFFC89C9EB8E75A126876FBE32156DC2B19764
          7AB7BF708C5DCB7DBF5283B41FE83AC426C0D69C005BBD14EECA20C0568E5A02
          6CC91DD7BEFC0D93D7709C5D13605BEEFFDCFF35AF72F7B57128E19CF913375D
          99D6C5B0FF4D788C689E4AAF97E65AA36B12F86A63A8F136A9FF165B2F0DC6B2
          6E026CC31060AB5FD7213601B672A9839561FA38C7700E4A92025EB507D8A6E3
          0BC75F120136DA081BA10D05997EF45590196A7935194FB10599E07BD1BBC97C
          4DEF8815F6AB142FDEF815D51E6C6E371D673807A51060AB4FDA2E2FDB47AE42
          80AD3901B67A29DC954180AD1CD33186E32FCD3A1F232AC0B6BB5AC26B630A02
          B595BE0B2504D87E7EDFD75D30790DC7D0C427DE7A5531C71F69798CE5AE81A5
          D74B73EE7F97E7B133D3BA65F8797D6A12F42AB65E1A8C65DD04D88621C0360E
          5D3E4A5480AD5CEA606570F7B57208B031666123B4517A4126D722405F01B6A1
          C65B754126F85EF46E325F026CE5A821C0B6CE0B64B9EF576AD2E5BE4680AD39
          01B67A29DCE5EFDEABBFECE2E9E31CC3392889005B5904D89AE93BC0F6BB37BC
          E69A5AC26BEEBEB65C098F0F3D73FD85A727AF61FF9BFAD56BBFE6E593D7F0E7
          E7682CEB6EE9F5D264A850D7324D6A8CAB1A6AAC4D9643B1F5D2602CEB26C036
          0C01B671487761EB2AC426C0562E75B03208B095A39600DBF15B2EBB6FF21AEE
          A76191B011DA28BD209373FF9B143156355441A649E1A2D8824CF0BDE8DD64BE
          A677C40AFB558AA7DEF6CA7401F9BC75A136D3718673500A01B6F3A5C0D6AF7D
          DFDFFB07C9E4CFD916F77793FADCD5A34405D89A1360AB97C25DFEA68F710CC7
          5F9214EC1A4580EDFA0B4FD6126213606BA6CF00DB1FDDFC9A9F78EEAD2F7B6E
          F23EFCEC9208AF3553C2DDD7A68FFF0CFBBFA2F0E7E7689DE796432ABD5E9AE4
          5AA79B9E7B877D6E63A865D5741CC5D64B83B1AC9B00DB3004D89A4941A8DFBB
          FE92FFF1CEFD97BC2DBD4F81B0E8DFE5ACAB47890AB0954B1DAC0C026CE5A825
          C0961CD8F7B23745FB6958246C84366A28C8E41A3AE8E3647FA8B1965C18DBCD
          669F83EF45EF26F3554380EDAEB75EF18B93D7F3D685DA4CC719CE4161C2F175
          2DF7FDCACC7CF02BFD39D7FDC96E529F9B6CA39711606B4E80AD5E0A77F9F3F8
          D0F208B0AD4E806DA7F75CFFEABF5FC35DD76652F8672C8F60DC8B34472504D8
          6EBFF1CA1F9DBC866358C53408177E466EC6B20EE77E5EDBA4E6986B9D6E7ADE
          1DF6B98DA1C6D9641924B92E87DD341D5BDF04D88621C0D6CC7C10AAD43BC74D
          83773B97F18A04D8CAA50E560601B6725415604BEB5DB09F8645C2466823F782
          4CC9C1A9AE0B32432DABA6E328B620137C2F7A3799AF128AF1CB08B09565FAD8
          DA708C5DCA7DBF3213DDB96CBABD0BFF7DCE52BF9BEC2F7723C0D69C005BBD14
          EEF227C0561E01B6D509B09DF3BEB75CF5DD3505D792740EE8EE6BCD94F0F8D0
          E41DD77EF39F9DBC866358C5276EBAB2A8757D0CEB71EEE7B54D0246690CB99E
          E3EEF5FC75BBA1EA914D9641526CBD3418CBBA09B00D4380AD99280855E25DD8
          92BD3E4A5480AD5CEA60F9BBFFCD175E327D9C6338072511602B8B001BAB0A1B
          A18D1A0A32B916029A06BF9A1A6A9C4D8B16B92E87DD6CF639F85EF46E325F35
          04D8DEB7FFAB2F9FBC9EB72ED4E623375CFE1D93D7700E4A22C0B6531460CBB9
          C0BFCC5EF739026CCD09B0D54BE12E7F026CE511605BDD58036C57EEBFE1BF9A
          85D66A7954E83CE1B5E64A385F3E73FD85A727AF61FF57F5F3FBBEEE82C96BF8
          39399A2E9F702CB5A8A15E9AE47A7EDBB4FF4D0C35C6A621BC62EBA5C158D64D
          806D18026CCD4441A8D456EAA344F7126213602B973A58FE361FE3188CBD3429
          D8358A00DBF5179EAC25C426C0C6AAC24668A386824CCE8183A6C58C26862A78
          342D5A145B9009BE177D9B0689C23E95E2C51BBFA2FA83CDEDA6E30DE7A21402
          6C3B2D0A7C9512E49A97C61385F29A12606B4E80AD5E0A77F913602B8F00DBEA
          4A0EB0A5F0595329A4566B502D3286C04F574A797CE8F4B19FE118DA9806E2C2
          CFCA4D5A3EB53F46B4867A69926BAD6ED1F9F8AA865A4EABF4BFD87A69309675
          13601B86005B338B8250A5F47F5E0AB1B50D7709B0954B1D2C7F1E1F5A1E0136
          C62A6C84366A29C84C0B07E1CF18529727FC438DB16908AFD8824CF0BDE89B00
          5B7904D89A2B3DC096E4BA4F5926F5BBE9367B9E005B73026CF552B8CB9F005B
          7904D8565772808DD818C23E5D2AE5F1A11F7BCB15FFD3E4351C431BD3405CF8
          5939AAFD8E82B5D44B73ADD5EDE5DC75BBA1C6D774FE935C97C16E56195F9F04
          D88621C0D6CCA220546A2FF551A2D37E9F37A686C29F991B01B69DD4C1F227C0
          561E0136C62A6C84361464FAB55B406215432DA755FA5F6C4126F85EF44D80AD
          3C3504D81E7BFB154726AFE1F8BA94FB7E6566B7ED5B1AC3F4EFC3FF9BB3B6FB
          1D01B6E604D8EAA570973F01B6F208B0AD4E80AD2EC26BAB2BE1EE6B5361FFDB
          BAFDA657FDF8E435FA9C2C4D975338961AD4522FCDF9DC763AC761BF9B1AAA16
          D974FE9362EBA5C158D64D806D18026CCDEC16844A632835C4369DFF705C4B84
          3F2F37026C3BA983E54F80AD3C026C8C55D8086DD45290C9B518908A440A32F9
          DAEC73F0BDE89B005B7904D89ACB7DBF32B32CE855EAA3449336213601B6E604
          D8EAA570973F01B6F2D412603B76CB65F74E5EC331764D80AD1EC26BAB2B65FD
          9F3EEE331C435B3FBCEF1B3F77F21A7E5E8E6A5FBF6BA99726B906D85619C322
          438D6D955A6FB1F5D2602CEB26C0360C01B6669605A14A19C7BC14BC6B19620B
          7F5E6E04D8765207CB9F005B7904D818ABB011DAA8A52093C6916B416695A2C6
          2243153B562958145B9009BE177DAB21C0F6D4DB5EB9B68B7839988E379C8B52
          ACEB37E473DFAFCC340979E5BA5F5926F57B1A480BC71511606B4E80AD5E0A77
          F913602B4F2D01B6B3575F905EC331764D80AD0EC26BED94F2F8D0E9E33EC331
          ECC53418177E668E6A7E8C682DF5D224D77A5D9373F2DD0C550B5EB5DFC5D64B
          83B1AC9B00DB3004D89A5916844A7F5FF2A3445B04BDC29F951B01B69DD4C1F2
          27C0561E0136C62A6C84366A2AC80C51B468A28B93FEA1C6B64AF8AED8824CF0
          BDE8DBF44E58619F4A21C0561E01B69D9A149D872A887721F57B956DB8005B73
          026CF552B8CB9F005B7904D85627C056BE14EA115E6B677ACE12CE6B4E3E78FD
          15374E5EC331ECC53418177E668E04D886B34AAD31D7B1AC7ACE3A6FA83A6493
          5AC276C5D64B83B1AC9B00DB3004D89A6912844A6319D1A344C39F931B01B69D
          D4C1F227C0561E0136C62A6C84366A2AC8E45A1058B5B0312F2DA3E9CF087F7E
          5F46539009BE177D13602B8F005B73B9EF57669A6EE352B06B886D701756D98E
          0BB03527C0562F85BBFC09B09547806D75026CE54AC7DB35077AFA56D8BA1F8E
          61AFEE78CBE5EF9ABC469F97A59AEF345853BD74A8BA6213D3790EFBBDCC5075
          C855E63E29B65E1A8C65DD04D88621C0D64CD3205429E389AC18620B7F466E04
          D8765207CB9F005B796A09B01DBFE5B2FBA2FD342C1236421BB5156426AFE1CF
          19522A129558905925F890145B9009BE177D13602B8F005B73B9EF576656D9C6
          E55AEC5F26F5BBE9A34405D89A1360AB97C25DFE8EDD7259F1FBE34480AD4002
          6C2C21BCB677D3F90BE737277D3D3E34B972FF0DFFD5E435FCDC5CD5BADED754
          2F4D723DA75D751CDB0D35A6556BBCC5D64B83B1AC9B00DB3004D89A691A844A
          FF6E248F120D7F466E04D8765207CB9F005B796A09B06DD6C182FD342C123642
          1B351564D258722DC8AC5ADCD86EA842C7AAC58A620B32C1F7A26FD32051D8A7
          5208B09547806DA7E9FE221CC3BC9CF72FCBA47E37D90709B03527C0562F85BB
          029C0B11154F80AD40026C2C300BAED57A17AA752AE53CB9CF005B72E6FA0B4F
          4F5EC3CFCE9100DB306AA9D9AD725EBEDD50E7E86DFA5B6CBD3418CBBA09B00D
          4380AD9955EE4E567A886DF21A8E6BBB52C627C0B6933A58FE04D8CA23C0C658
          858DD0466D0599218A174DECE5C47FA83135093C6C576C4126F85EF44D80AD3C
          026CCDE5BE5F99138E2152FBA34405D89A1360AB97C25D0104D88A23C0B63A01
          B672CCC26B93F7E1B2A4B952EEBE96DC7ADD95DF3E790DC7D18569402EFCEC1C
          ADEB3C73DD6AAB97E63A9E747EB76AFD31196A3C4DCEADE7155B2F0DC6B26E02
          6CC310606B6695005B52CAB8224DC62AC0562675B0FC09B095A79A005B12ECA7
          6191B011DAA8AD20936B51A04D812349CB67FA7FC39FDB9751156482EF45DF04
          D8CA23C0D65CEEFB9539E1181619627BDC95658F1215606B4E80AD5E0A770510
          602B4E2D01B63BAFBBF47B27AFE118BB26C0963FC1B5EE9514609B08C7D0958F
          BDE58AFF69F21A7D6EB66AFC3ED4562F1DAABED8C474AEC37E2F32540D72D579
          4F8AAD970663593701B66108B035B36A802D2935C4D6E451A2026C655207CBDF
          F15B2EBB6FF21A8EBF24026C850AF6D3B048D8086DD4589099BC863F6B48A948
          D4A62033D47804D8FA25C0561E01B6E672DFAF6CD766BB5C6A882DF57BB7109B
          005B73026CF552B8CBDF3444148EBF24026CE51160239905D73C2EB45B693E4B
          3947EEFBF1A1DB849F9F2B01B6F56B1330CAF55CB6A4B1B4A921145B2F0DC6B2
          6E026CC310606BA64D80ADF44789EE16FE12602B933A5801EAF9454E01B6D278
          84282B0A1BA18DDA0A32693CB916644A2A72B42954145B9009BE177D13602B8F
          005B73B9EF57B66BB35DCE793FB34CEAF7A23197B2DC36E73ED8AEAE93005BBD
          14EEF227C0561E01B6D509B0E54770AD5F25DD7D6D5D01B633D75F787AF21AF6
          2147EB3AD75CA7DCCF8F6AAADB4DCFAFC33E2F32C439799B7E26C5D64B83B1AC
          9B00DB3004D89A6913604BD2FF2B35C4B6DB9805D8CAA40E560001B6E208B031
          566123B45163416688224613258D6551C06137C5166482EF45DF04D8CA23C0D6
          5C2941A8A4CDB62E49772BCB755FB3CCA2A27B29CB6DB3FFC176759D04D8EAA5
          70973F01B6F208B0AD4E802D0FE9D879165C9BFC395C5674A3A400DB7FB8F6D5
          974F5EC37174691A940BFB90ABDA029EB99F1F2D3AAFDB4DAE634A6359E5DC7C
          A871B499F3A4D87A6930967513601B86005B336D036C4929639C97426A8BC65D
          CA9804D87652072B80005B716A09B01DB8EED27746FB6958246C8436722FC8B4
          3961CEB530D0A6D031FD3FE1CFEBCBE80A32C1F7A26F026CE511606B2EF7FDCA
          76AB14C9E70DB17DEE42EA77F428D15296DBE6BC07DBD5751260AB97C25DFE04
          D8CA23C0B63A01B661CD426BEEB6B61E699E0B3B3F0EC7D1B53FBAFEF2EF9BBC
          469F9FADDAC29EB99F1FB5ADDDE57A1EBBCAB9F950F5C73635EAA4D87A693096
          7513601B86005B337B09B025A5DE852DF53B0A8109B095491D2C7F9B21A260EC
          A511602B8F001BAB0A1BA18D1A0B32B98E298D659582CC50E3685B042BB62013
          7C2FFA26C0569E1A026C8FBDFD8A2393D7707C5DCA7DBFB2DD2ADBE479E9FFE6
          5AFC5F26DA1F95B2DC36E73CD8AEAE93005BBD14EEF227C0561E01B6D509B0AD
          9FD0DA704ABAFBDABA1E1FBA4DD88F5CADEB17A6D625F7F3A3B6B5BB5CCF6157
          094C0D557F6C5B3F28B65E1A8C65DD04D88621C0D6CC5E036C294855D3A34405
          D8CAA40E963F01B6F208B031566123B4A120B35EAB143B862A70B42D52145B90
          09BE177D13602B8F005B73A504A192B605E899921F253A7F17B65296DBE67C07
          DBD5751260AB97C25DFE04D8CA23C0B63A01B6F5125A1B564901B64FBCF5AAB5
          EE67CF5C7FE1E9C96BD8975CD5F47DAAB55E9A6BED6E95F10C710E9E3EB36DFD
          A0D87A6930967513601B86005B337B0DB025E9679418628BEEC226C0562675B0
          FC09B09547808DB10A1BA18D5A0B324314339A58A50030548163740599E07BD1
          B7699028EC53295EBCF12B4673C099D41060FB831BAEDA37790DC7D7A5528250
          49DBEDDD76D32058F8F373362BC07FCF3BDFF137BFE6DFFEFC5794B2DC36F7EF
          C176759D04D8EAA570973F01B6F208B0AD4E806DBD6EBBF1AA7F39790D9705FD
          4AEB7A49BFDCF58BFBBEF6CB26AFE158FA30BDE35BD8975CD5F418D1DCCF8FDA
          D64B731DD7ECFC74F23EECF74CFA3743D47CDBCE77526CBD3418CBBA09B00D43
          80AD992E026C4929E39D370DDE15370E01B69DD4C1F227C0561E0136C62A6C84
          366A2DC8E45A1C58653C4315649A148C22026CCD09B095673ADE702E4A21C076
          BEB6DBBB79436CAFBB74E4276F0EDB73B439D7C176759D04D8EAA57097BF9A02
          6C6309B109B0AD4E806DBD0EDCF4AA9F9FBC86CB827E9574F7B5A9701C7D79FF
          F5575C37798DFA91AD9A1E235A6BBD34C9F5FCB5C9F9F950CB652F81AE62EBA5
          C158D64D806D18026CCD7415602BF951A2251260DB491D2C7F026CE5116063AC
          C24668A3D6824CAEE34AE3695A9019A2A0B4970298005B73026CE511606B2EF7
          FDCA764DB6C74D0CB5CD1EA3CD790EB6ABEB24C0562F85BBFCD512604BC61060
          3B7DFD85276B09B0DDB6FFD21B26AFE138BB26C0B65E353DF2B03425DD7D6D7A
          37B4701C3D0BFB93B35ABE53B99FD7D658BF6B129A1AAAEF7BA91D145B2F0DC6
          B26E026CC310606BA6AB005B22C4B63E026C3BA983E54F80AD3C026C8C55D808
          6DD45C90C9354CD0A4E831D472D94B81A2D8824CF0BDE8DB344814F6A914026C
          E511603BDF5E8AD0F3D2A34485D8FA27C0D69E00DB720A77F913602B4B2DE1B5
          B3575F70F6E035177FC3E47D38CEAE09B0AD570A5125E96E60C26CEB93E6BAA4
          00DBED37BDEAC727AFE158FAE431A2C3A9B95E9A6BFDAE494D7288BEA7B9DE4B
          EDA0D87A6930967513601B86005B335D06D892F4F384D8FA27C0B6933A58FE04
          D8CA23C0C658858DD08682CCFAE55A904946599009BE177DAB21C0967CE486CB
          BF63F21AAE0F3579EFB5977D89005B73B9EF57B6DBCB362F92426C93D7F0B3E8
          86005B7B026CCB29DCE56F7A17AC70FCA511602BC73A1F1F9A08B00D47906D7D
          4A7B7CE80FEFFBC6CF9DBC8663E9538901B66930311C4F496AAE97E63AB62663
          9AFE9BF0FFF7652F739D145B2F0DC6B26E026CC310606BA6EB005B52CAD84B26
          C0B6933A58FE6A0AB08D25C426C0C658858DD046CD05995C0B044D8A0043F43D
          CDF55EC21CC5166482EF45DF6A09B0DDF5D62B7E71F21AAE0F35397CF395A970
          13CE4129D6791121F7FDCA767BD9E62D3244417D4C36E737D8AEAE93005BBD14
          EEF2F7897417ACAB2F08C75F1A01B67208B08D8F205BFF4ABAFBDA99EB2F3C3D
          790DC7D1B75BAFBBF2DB27AF61BF7256C377A7E67A6992E379EBB2BA64FABB21
          FADDA48EBB9B62EBA5C158D64D806D18026CCDF41160F328D1FE09B0EDA40E96
          BF5A026CC918026CA7AFBFF0643501B6FDAFB829DA4FC3226123B451734126D7
          B13519D3100599BDCC7522C0D69C005B59A6E30CE7A014026CB1DD8AE36D0D55
          541F8BCDB90DB6ABEB24C0562F85BB32D4F2185101B67208B08DD72CC836791F
          2E2BDA49EBB800DB4AC27EE5AC86EF4DCDF5D224D71ADE6EE7E8432D93BDD60D
          04D8DA13601B86005B337D04D81221B67E09B0EDA40E963F01B6B2D4125E4BBF
          3C7CE89A8B5E1BEDA76191B011DAA8BD2093638820F569594166887EEFB53851
          6C4126F85EF46E325F2515EC1779EA6DAFBC77F27ADEBA509BE938C33928C563
          6FBFE2C8E4351C5FD772DFAF6CB7D742F422E951A239EE7F6AB039AFD176758D
          04D8EAA57057869A026C3587D8A6BF752AC0D682005B7ED2B99BBBB175A7B4C7
          87A600DB6FEF7FD5DFF80FD7BEFAF275FBE0F557DC18F52977EBFC05AABED45E
          2FCDB586B75B6D72883E2FABE136516CBD3418CBBA09B00D4380AD99BE026C49
          2973502201B69DD4C1F2B77917AC60EC2512602BC7343819EEA76191B011DA50
          9019C66EC58FA196C9680B32C1F7A27793F9AA21C0F6E28D5F51FD0167321D67
          3807A510608BED75BBB71B01B67E08B0B527C0B69CC25D196A09B0255507D82A
          09AF25026C24E9FCCDDDD8BA51C3B930BBAB21F499FB79ED5EEBA5697C399EB3
          EE169C1AA2EEB8D7794E8AAD970663593701B66108B035D367802D7117B67E08
          B0EDA40E96BFCDBB605D7D4138FED208B09543808D36C24668A3F6824CAE4582
          1C0B327B0D72145B9009BE17BD9BCCD7345014F6AB1429D8F5DE7D975D30797F
          DEFA50931A026CD3C7D686E3EB5AEEFB95EDF6BADDDB4DAE17044AB739A7D176
          758D04D8EAA570570601B632D412603B76CB65F71EBCE6E26F98BC0FC7D90701
          B67C09B1ED5D5ABF05D8C6A1F4EF4AEDF5D224C7F3D5DCEAA5BBF5A7A962EBA5
          C158D64D806D18026CCDF41D60F328D17E08B0EDA40E56865A1E232AC0560E01
          36DA081BA18DDA0B32B90608722BC87451F82AB620137C2F7A3799AF1A026CC9
          5D6FBDE21727AFE7AD0FB5988E2F1C7B4904D862D3BE86E3E842AEFBA0926DCE
          67B45D5D2301B67A29DC95E18EEB2E7DFBE4359C83D2A490577AD4E6E47D38D6
          526D8EAB9200DBBAEFBE9608B0E5AF86BB4B0DA5B4C787D2DE34A818AE0725A8
          BD5E9AE458C7DB6D5C439C5B77513328B65E1A8C65DD4A0DB0A50058C9266308
          97C7670475922E09B06D499F21C4D62D01B69DD4C1CA5053806D1AF00AC759BA
          54DF9B8E2F1C7F4904D868236C8436C6509019A2C0B1CC6E8580210A1B5D1426
          8A2DC804DF8BDE4DE66B1A280AFB551201B6FCADFBC241EEFB95EDBA28462F73
          CFF7BFEEA339EE874AB53997D176758D04D8EAA570578643D77CE5E5EEC296B7
          5AC26B89001B8B08B1B5E3EE6BE351FA7744806D188BC69596C7BACFABD3E775
          513328B65E1A8C65DD4A0EB04D5EC3315521A8937449806DA752E6A314026C3B
          A98395A196005B527580AD92F05A22C0461B6123B4A120338CDDC6B5EE824C32
          EA824CF0BDE8DD64BE6A09B04D1FAF79DEFA508B1A1E1F2AC0B65817DBBE2686
          D8AED76A732EA3EDEA1A09B0D54BE1AE1C026C791360DB1B01B67208B1AD26CD
          9500DBB894FC18D131D44BD318733C578DCED387581E5DCC7122C0D69E005BA6
          823A499704D8CEE72E6CDD1160DB491DAC0C026C65A825C076FC96CBEE3B74CD
          45AF9DBC0FF7D3B048D8086D8CA1205352806D88E251FABCA838B42A01B6154C
          E7AC86E27D0A787DFC2D97DF3279BF637DA8C1C7DE72F98D3504D8A68FAB0DC7
          D887DCF72BDB75B1ED6B62886D7BAD36E731DAAEAE91005BBD14EECA515B80AD
          A6C7886E8EA79600DBD5179CBD6DFFA5374CDE8763ED8B005B5984D89AF3F8D0
          F159F72F537529F7F3DA4575C555E5789E1A9DA70F5173EC2AC4556CBD3418CB
          BA09B0652AA893744980ED7C297425C4D60D01B69DD4C1CA70E7FE4BDE31790D
          E7A03429E455530D6C66735C9504D83E73F7B524D84FC3226123B43186824CAE
          C181A82033C4F2E8AAE8556C4126F85EF46E3A67B5FCF679AD8F11ADE1F1A1C9
          F46E7FE118FB90FB7E65BB683BDC178F12EDC6E61C46DBD5351260AB97C25D39
          6A0AB025D3C05738D6D254135E9B18E2EE6B89005B794A0EE9AC532DE7BF3457
          72C033F7F3DA9A6B7951786A887E46FD6823C7395EA6ABB1EF95005BA6823A49
          9704D862E9F384D8F64E806D2775B032DC75F54B5FE52E6C79AB25BC9608B0D1
          56D8086D8CA520936368200A4E28C8ACD7669F83EF45EFA67336BD3356D8B792
          3CF5B657DE3B79DDB13ED4603AAE70CCA518E2629A00DB6229C436790DFB4233
          026CED09B02DA770578E83FB2E7E7D757761AB20C4967E8B56806DEF4A0DB0A5
          E3CE7487AD48FABBDAC34B699C93D7709922983966A57E37C6522FCD719C519D
          72DD35C734BF5DD50B8AAD970663593701B64C0575922E09B02D56CADCE44C80
          6D2775B07208B0E54D800D04D8E8D0580A3239160B7228C8245D1525729CE365
          36FB1C7C2F7A379DB3E99DB1C2BE95243D66F3D0CD57FEABC9FB1DEB44C90EDF
          7CE5CFD5F0F85001B6DD7555905E458E81EA9208B0B5D77580ED0D7FFFEF7E7D
          D45E3285BBB2B80B5B7E6A0AAF25026CAB59165249E3AA39CC96C624C4B6D874
          6EC2B9A36E439C9376612CF5D234CEDCCE51A33AE5BAFBD8D5FC26C5D64B83B1
          AC9B005BA6823A499704D816F328D1BD1360DB491DAC1CB505D86A7A8CE8E678
          6A09B05D7DC1D903FB5F71D3E4FDB9F105FB6958246C8436C6549099BC869F31
          94A818B0EEA2469ADFAE021CC5166482EF45EFB6CD5B2D176F6A7B8C680D775F
          4BA677F90BC7D897DCF72BDB75B5FD5B45FACCDC2E10946473EEA2EDEA1A09B0
          9DF337FECE1B2F8EDA4BA67057961A036CD3005838DEDCD578F7B583FB2E7EED
          E47D38DE3ED51A60DB2E8DB1C6205B1ACF74F985E31EB3DA9635CD95FABDC8FD
          BCB6CB80556EE7A7D1D8D6DDC72E035CC5D64B83B1AC9B005BA6823A499704D8
          7627C4B637026C3BA98395A3A6005B320D7C85632D4D35E1B5891D775F4B82FD
          342C1236421B6329C8E41818888A01EBEE6357F39B145B9009BE17BDDB366FB5
          14F2D3DDCA3EB0FFABAF9ABCDF31BE12DDF696CBBFBB86BBAF25D3BBFC85E3EC
          4BEEFB95EDA67D0DC7D1A7F428D1DCF649A5D89CB768BBBA46026CE708B0E547
          80AD7C4507D8DC7DAD336308B0CDCCEEC836791FFECC12B90BDBF94A5DA7E94E
          89DF8B3105D872ABE7CDC6F677DEF0E317FFCADB6E7EC710CBA2CB0057B1F5D2
          602CEB26C096A9A04ED22501B6E54A99A31C09B0EDA40E568E43FB2EFA96EAEE
          C25641886DFACB9C026C301136421B632AC8E4161688C6B6EE3E2AC84CFA1C7C
          2F7AB76DDE6A798C6852CB5DD8A6E308C75892E9C5C0708C7D12606B26B77D52
          2936E72DDAAEAE91005BBD14EECA72FCCD5FFC127761CBC3B67E87E32A9100DB
          EADA0654D2786B0AB1A5B108B1ED349D8F70BE1887A1CE4DF7624CF5D25CC7FA
          FD37FDF837BCFF07BE27FCBB3EA5B9EDB25620C0D69E005BA6823A499704D89A
          11626B672F01B637FCFDBFFBF5517BC9D4C1CAE22E6CF9998E211C5F8904D8D8
          8BB011DA18534126B782C16C6C7EA370389B7D0EBE17BDDB366F3505D8D25DCB
          3E78FD5F7DFDE4FD8E3196A4A6BBAF0DF1F8D024F7FDCA765D16A5DB98EE07C2
          BE11DB9CB368BBBA46026CF552B82B8FBBB0E5A1C6F0DA1DD7BEFC0D93F7E178
          FB36B6005B5263886DBA1CC3F18E4D4DCB96764AFC4E8CA95E9AC69AE3B9E9CF
          BFF347CF3EF4CE37847FD7A72EE73629B65E1A8C65DD04D83215D449BA24C0D6
          8C4789B6B397009B2711E44780AD6C29FC557288ADC6BBAF1DBAE6A2D74DDE6F
          8D33D84FC3226123B431B682CCE435FC9C21F98DC2E1E410604B6A2AE8977E17
          B65AEEBE960CF1F8D024F7FDCA765D6E03DB489F9FE385829C6DCE57B45D5D23
          01B67A29DC95A7D600DB3410168E3937DBFA1B8EA74443DE7D2D1963802DA92D
          C4E62E6CE794BA3ED3BDD2BE13B99FD7761DB2CAF1BCF4CCFEFF3E6CEF5BD7E1
          2D01B6F604D83215D449BA24C0D65CEA8310DB6A04D87652072B4B6D01B6641A
          000BC79BBB69DFC37195E8BCBBAF25C17E1A16091BA18D311564720D0AF88DC2
          E1E412609BDE292BEC6369D2DDCB0EDD7CE5BF9ABC3F6F9CB94BE1B55AEEBE36
          BDF8178EB36FB9EF57B69BF6351CC7BADCF3FDAFFB688EFBA65C6DCE55B45D5D
          2301B67A29DC9549886D38DBFA198EA354026CED74114EA929C456E21DA7FA30
          5D2FC239625C863C476D636C01B6126B7A7DE93ABC556CBD3418CBBA09B0652A
          A8937449806D35A5CC572EF61260AB913A58594EBCF98BBEDC5DD8F2B0ADDFE1
          B84A24C0C65E858DD0C6D80A32398604FC46E17036FB1C7C2F7A373777E94E59
          35DD6DE0A9B7BDF2DEC9EB79E3CCD9FBF67FF5E5D37E87632ACD508F0F4D04D8
          5627C0D69C005B7B026CCB29DC95A9C6005B928261C9E47D38EEA16DEB5FD8FF
          52A5F5E9D0355F79F9E47D38EE751873802DA929C436F6BBB0D5B42CD9BBD242
          9D63AB9796741EDFA734AF5DD70904D8DA1360CB545027E99200DBEADC85AD39
          01B69DD4C1CAE32E6C7998F6391C4F89D27A7560DFCBDE3479BF73ACC17E1A16
          091BA18DB115644A2C1AF4A5EB6244B10599E07BD1BB60FE6A2BEC97F628D19A
          1E1D9A0CF5F8D0A4A4C277D785E9B6523F84D89A11606B4F806D3985BB32DD71
          DDA56F3F76CB65D584D0B7DB16120BC73E946DFD0AFB5DB2A1EFBE968C3DC096
          A49F55C3F95169819DAEB9FB1AF34A0A758EAD5E9A3827ED675E8BAD97066359
          3701B64C0575922E09B0AD2E85B284D89A1160DB491DAC3CB506D892C9FB70CC
          B9D9D6DF703C250AEFBE9604FB6958246C8436C65690292958D1A734AF5D0737
          04D85610CCDF347014F6B344E9519CA584D86A7A746832BDD8178E751D4ADACE
          76BD1DDC0B8F126D66738EA2EDEA1A09B09DF337FECE1B2F8EDA4BA67057AE5A
          EFC296A4A05832791F8E7DDDB6F527EC6FC9D27A74DBFE4B6F98BC0FC7BE2E02
          6CE7D4127E2A29B0D3350136E609B075A78FA095F3D17E825B026CED09B0652A
          A893744980AD9DD41F21B6E504D87652072B9310DB70B6F5331C47A904D8E842
          D8086D28C88C531FF32AC0B68260FE6A7B8C689242619FBCF9CA9F98BC0FC79C
          83DAC26BC990775F4B04D8DA13625B6E737EA2EDEA1A09B09D23C0961F01B678
          5E6A900263C9E47D38FE75D9D68FB09FA5CBE1EE6B8900DB39691EDC85AD5CB5
          2C3FBA55D2F7618CF5D212EB7A5DEB23B8556CBD3418CBBA09B0652AA8937449
          80ADBD52E66E48026C3BA98395A9C6005B928261C9E47D38EEA16DEB5FD8FF52
          A5F5E9AEAB5FFAAAC9FBF3C71DECA76191B011DA10601B2705997336FB1C7C2F
          7A17CC5FF2D8DBAF3832790DFB5AAAA7DEF6CA7B6FBFF1CA38BD3FB0C3375FF9
          73B585D7A61789C2F1AE8B00DBDED847ED4E80ADBDAE036CDFF46D6F884F6C0B
          A67057AE43D77CE5E5426CFD997DF6F4F3C3FE15EFEA0B04D8F6A88F3B2BD572
          07AF92EE3AD5955A961DDD2BE5FB30C67A6949E7F27DE9A34620C0D69E005BA6
          823A499704D8F6C65DD87627C0B6933A5899EEDC7FC93B8EDF72D97D93F7E1BC
          94EC74A621B66DFD0AFB5DB285775F4B82FD342C1236421B632CC8945838E85A
          1F8588620B32C1F7A277C1FC25B53D46742685D83E79D3153F3A791F8E7B0835
          DE792D998620C331AF4B4945EF3E8AD37B95FA24C4B698005B7B5D07D86AA470
          57B6DA036C490A90CD4CFE1CCE43D7B67D5ED8A75AE4125E4B04D8767217B632
          B9FB1A8B08B075A38F7A6932E673D1BEE6B4D87A693096751360CB545027E992
          00DBDEA4809610DB627B09B07912417EC65C07ABF52E6CC9E9CC426CDBFA13F6
          B764693D3AB0FF15374DDE87638FF6D3B048D8086D8CB1205352B8A22F7D8436
          04D85610CCDF4CAD85FE14624BA1B1C9FB70DCEB546B782DAD3B433F3E342969
          1BDBC7B6B00B1E25BAD8E6BC44DBD5351260AB97C25DD93EB1EFABBE750C21B6
          2405CA66267F0EE763AFE63E23EC474D04D8F6AEAF504A2D77F22A25B4D385B4
          0E0BB0B1C874DD08D79D9C8CB15E9A9458DBEB4A5FA1AD62EBA5C158D64D802D
          53419DA44B026C7B97FA26C4161360DB491DAC5C3507D892D39984D8B6F523EC
          67E976BDFB5A12ECA76191B011DA186B4166CCC10045AE2D9B7D0EBE17BD0BE6
          6FA6D6BBB0252934366488EDAEAB5FFA676B0DAF2539DC7D2D1160EB460AB14D
          5EC37E8F99005B7B026CCB29DC956F2C01B699142E9B99FC399C93556CFF79D3
          9F197E6E6D720AAF25026CE7AB210C95C63096BBB0D5123AA43F25043A05D8C6
          A7AFD056B1F5D2602CEB26C096A9A04ED22501B66E94328FEBB69700DB377DDB
          1B5E15B5974C1DAC5C775DFDD25709B1F567F6D9D3CF0FFB57BCAB2F1060A353
          6123B4A120333E0A325B36FB1C7C2F7A17CCDF76B5FFB67A0A91DD7EE315DF37
          791F8EBF0F876FBEF2E7D25DE026EFC33E952EAD3339DC7D2D1160EBCE98C3D6
          8B08B0B527C0B69CC25DF9EEB8F6E56F185B882D393D173CDB74FD8527277F17
          CED34CF4FF26EDE16754EBEA0B04D83AD26720A596405409A19D2ED47E3ECBDE
          09B0ED5D5FF5D292CEE7BBD6577DA0D87A693096751360CB545027E992005B37
          3C4A34B697005B8DD4C1CA567B802D39BD334816CE43D7B67D5ED8A75A2C0DAF
          25C17E1A16091BA18DB116644A2C1E74A5AF2244B10599E07BD1BB60FEB69BDE
          492BEC732D667763FBC0FEAFBE6AF2E7701EBAF0B1B75C7E63CD775D9B995E24
          0AE760DD4A2A78F755A0EE4AEA9F10DB4E026CED09B02DA77057873106D822A7
          83705A24FABF63925B782D11608BD5108ACAE998BD2F1E1F4A13257C17C65A2F
          4DC6780EDAE77C165B2F0DC6B26E026C990AEA245D1260EB8E10DBF904D87652
          072BDB27F75DFCC63184D892D36B08B2CD7D46D88F9A08B0D1B5B011DA186B41
          A6A48045D7FA0A6C08B0AD2098BF796329FACF826C076EBCE27F9EFC399C8B36
          3E79F3953F3186E05A92D6955CEEBE9694B47DED6B7BD8A5F428D1315E405864
          732EA2EDEA1A09B0D54BE1AE0E07F75DFC7A21369A48EB89005B77FA0EB0D570
          17B674DC5EFB63446B584EAC47EEDF85B1D64B9312EB7B7BD56760ABD87A6930
          96751360CB545027E992005BB74A99CF751160DB491DAC7C6309B0CDCC85CCC2
          3959C5F69F37FD99E1E7D6A651782D09F6D3B048D8086D8CB92033C6408002D7
          4E9B7D0EBE17BD0BE66FDE349014F6BB462968360BB3A5C77D4EDAC279D94DFA
          7FB3D0DA18826B33D33BF68573320401B6EE8D717FB5C8E65C44DBD5351260AB
          97C25D3D04D86822C7F05A22C016ABE5CE5E7DCFD3D0C6F28B58EC5DEEDF0501
          B6F8736BD56760ABD87A693096751360CB545027E992005BF7DC856D8B00DB4E
          EA60E53BB0EF656F1A5B882D990F9E6DBAFEC29393BF0BE76926FA7F93F6F033
          AA75F505026CF4226C84361464E2CFAD9582CC4E9B7D0EBE17BD0BE62F32E6E2
          FF2C84963CF5B657DEBB9BB105D6B6CBEDEE6B89005BF7523F85D8CE11606B4F
          806D3985BBBA08B1B19BB47EDCB6FFD21B26EFC3F56748026C8BD5707E341D43
          38BED2D51232643D72FF2E8CB95E3AC6F3CF3E6B03C5D64B83B1AC9B005BA682
          3A499704D8BAE751A25B04D8765207ABC318036C91289C1689FEEF98340EAF25
          C17E1A16091BA18D3117644A2C20EC559F0588620B32C1F7A277C1FC455230C9
          05007693DBDDD792DCF72BDBF559A4EEDA182F224404D8DA13605B4EE1AE2E29
          A024C4C622B9DE7D2D11605B6CFA19E1E797A4D6BBB0D5B27C589FE9F62E5C9F
          8636E67A6932A673CFBEE7B2D87A693096752B35C056ADAB2FD8D80CE7057592
          2E09B0F523F557884D806D9E3A581D0EEDBBE85B84D86822AD27026CF4256C84
          36C65C90196318A0CFB08600DB0A82F95B44808D4572FD8DF5DCF72BDBF5B94D
          ECC33DDFFFBA8F8E6DBF356F73FCD176758D04D8EAA570571F01362269BD38FE
          E62F7EC9E47DB8DE0C4D806DB15AEEF095FB9DA7DA72EECAAA720E738EB95E9A
          9458E36BABEFB056B1F5D2602CEB26C0961F01B62DA505D89252E6B64F026C3B
          A983D543808D26560AAF25C17E1A16091BA18DB11764C6140450DC3ADF669F83
          EF45EF82F95BC45DD888A47522B74787CE08B0F56B4CFBADC8E6F8A3EDEA1A09
          B0D54BE1AE3E775CFBF23708B1B15D5A1F72BEFB5A22C0B6BB1AEEF2958EE573
          BEF3541BB5840B59AFE93A13AE534313602BAFC6D756DF61AD62EBA5C158D64D
          802D3F026C5B4A0CB02563BF0B9B00DB4EEA6075116263379B775FDBFF8A9B26
          EFC3F52714ECA76191B011DA50905190E94AB10599E07BD1BB60FE76337D4C64
          3806C649A1BF1B2506D8529FC71C6213606B4F806D3985BB3A6D0B2C85F3C6B8
          E41E5E4B04D87657EAFCCCCBF9CE536DD4102C6418B98639C75E2F1DD37967DF
          758162EBA5C158D64D802D3F026C5B4A0DB0A500D798436C026C3BA983D56533
          A024C4C6022BDF7D2D09F6D3B048D8086D8CBD20536211A1ADBE8B0FC5166482
          EF45EF82F95BC66FB33393D6855CEFBE96E4BE5FD9AEEF42755FC6FC285101B6
          F604D89653B8AB97001B495A0F727E74E88C00DB72359C1BE5FC0B296D94BA4C
          D27A9BBE73A52B3940986B9873ECF5D2640CE79CEB98C762EBA5C158D64D802D
          3F026C5B4A0DB025A9EF630DB109B0EDA40E561F01362269BD38F1E62FFAF2C9
          FB70BD5928D84FC3226123B431F6824C1AFF180A3249DF410D01B61504F3B78C
          478992A475607A47BE703DC941EEFB95EDFADE2EF62985D826AFE1B86AB6B9CF
          8EB6AB6B24C0562F85BB7A1DDC77F1EB85D8C62D2DFFDBF65F7AC3E47DB88EE4
          24855126AFE13872B6CE104AC9619DEDA6CB3A1C6349D2384A0EB04D5EC37195
          A6D46530ED7738A6218DBD5E9A9458E75B95798C4DFB1C8E679D04D8F223C0B6
          A5E4005B52CA3C774D806D2775B0FA1CD8F7B23709B1B15D5A1F5ADD7D2D09F6
          D3B048D8086D28C8F88DC2AE145B9009BE17BD0BE6AF098F1225F7F05A22C0B6
          3E63D87FCDDB1C73B45D5D2301B67A29DCD52D059884D8C669DBB20FD78DDC08
          B02D57EA1CCDAB253C556AA03005A76A091126A506D8921C97837A6959E7F66D
          AD23A825C0D69E005B7E04D8B6941E601BEBA34405D8765207ABD3B6C052386F
          8C4BEBF05A12ECA76191B011DA5090F11B855D29B620137C2F7A17CC5F532517
          A5D99BB4EC737E74E88C00DBFAA4FE8F2DC426C0D69E00DB720A77F54B21A663
          B75C76EFE47D3887D4A9A4F05A22C0D64C0DE745B504A84A5D16D37E87632A51
          A941C224C730A77AE938CE37D7511310606B4F802D3F026C5B4A0FB025630CB1
          09B0EDA40E562F013692B41EB47A74E84CB09F8645C246684341C66F14764580
          6D05C1FC35E551A2E394967909E1B5A4A46DEA3A8AD57D4B8F121D53884D80AD
          3D01B6E514EEC6C15DD8C6252DEFE36FFEE2974CDE87EB438E04D89A2939ACB3
          5D8EC19D5594BABE26A5CFFDBC9297458E6142F5D2736A3ED75CD71C0AB0B527
          C0961F01B62D3504D892348E3185D804D8765207ABD7A17D177D8B10DBB8A5E5
          7F60FF2B6E9ABC0FD7914682FD342C1236421B0A327EA3B02B026C2B08E66F15
          426CE3929675098F0E9DC97DBFB2DD3AB68DEB50FB3E6CBBCDB146DBD5351260
          AB97C2DD38DC75F5975F29C4360E6939DF71EDCBDF30791FAE0BB912606BA6E4
          B0CE763906775651729070BA0E85E32A511A4FA97582D4EFDC96877AE93925D6
          FA9A32878B09B0B18800DB965A026C492973DE0501B69DD4C1EAB6196012621B
          A56DCB3E5C371A0BF6D3B048D8086D28C89C53F3C57F0599C54A0DB025D34053
          382EEA5252782D11601B46CDFBB1ED36C7196D57D74880AD5E0A77E391424D42
          6C75DB0CAFEDBFE4E6C9FB701DC899005B7335FC524F8EC19D55084CE5A5E4EF
          446E77C4532F3DA7A4F3FB55AD2BA425C0D69E005B7E04D8B6D414601BD3A344
          05D8765207AB5F0A311DBFE5B2FB26EFC339A44E9D84D792603F0D8B848DD086
          82CC392516139A32878B6DF639F85EF42E98BF366AB860C3EEA6CB385CFEB912
          601B461ACB18426C026CED09B02DA770372E29DC24C456A7B45CA7CB365CF6B9
          13606BAEE4BB7F6D975B70A7A952D7D5A4C4F3AC264AFE4EE4B64CD44BCFA9F9
          3C735DF50001B6F604D8F223C0B6A5A6005B3296109B00DB4EEA60E3E02E6CE3
          9296F789377FD1974FDE87EBC34A82FD342C1236421B0A32E7E43E0F7BB1AEA2
          8300DB0A82F96B4B88AD5E69D9A6C7C54EDE87CB3E57256D4FD755B05E977BBE
          FF751FAD3DC426C0D69E00DB720A77E3B32DE814CE29E5293DBC9608B03597E6
          AA86F3A1348612EF06567258AAD4D0E032A5870A73FA1EA8976EA9F11C739DF3
          27C0D69E005B7E04D8B6D416604B4A99FBBD1060DB491D6C1CEEBEFAA5AF1162
          1B87B49C0FEC7BD99B26EFC3756165C17E1A16091BA10D059973D23CD4589049
          D615D010605B41307F6DA58093105B7D4A0DAF25B9EF57B65BD7F6719D6ADD97
          CD6C8E2FDAAEAE91005BBD14EEC64988AD1E3584D71201B6D5D4722E5462A0AA
          E4B92F3130D8441A57C9CB25A7EF817AE99612EB7DCB98BFDD09B0B18800DB96
          1A036C49ED77611360DB491D6C3C52A84988AD6E69F9DEB9FF92EF9ABC0FD781
          5682FD342C1236421B0A325B6ABCE8AF20B3BB1A026C89105B5D4A0EAF25026C
          C34A63AA717F36B339B668BBBA46026CF552B81B2F21B6F2D5125E4B04D85633
          FDDCB04F25999ECF8563CC51A9EB6992E6BAD6005B52726D20A7EF817AE99692
          CEF19B5A67404B80AD3D01B6FC08B06DA935C056FBA34405D87652071B97146E
          1262ABD3E69DD7CE2DDB70D9B716ECA76191B011DA5090D95262416119F3B7BB
          CD3E07DF8BDE05F3B757426C7548CBB0E4F05A5252717BDAD7701C254BE3AA35
          C426C0D69E00DB720A77E326C456AE9AC26B8900DB6A4ABFE3D44C1A4349A1AA
          9283833985A4FA50FAB2C9E57BA05EBA536DE797EBAC0508B0B527C0961F01B6
          2DB506D89234B65A436C026C3BA9838DCFB6A05338A794A7B7F05A12ECA76191
          B011DA5090D992FB5CB4B1CE828300DB0A82F9EB82105BD9D2B22B3DBC9694B4
          2D5D67D17ADDEEF9FED77DB4C6109B005B7B026CCB29DC21C4569EDAC26B8900
          DBEA6A39071A720E5755F29C9734CF6D941C604B72593EEAA53BD5746E99C6B2
          CE5A80005B7B026CF91160DB5273802D296539AC4A806D2775B0711262AB47AF
          E1B524D84FC3226123B4A120B3534D059944416677B505D81221B632D5125E4B
          72DFAF6CB7CE6DE4106ADBA7259B638AB6AB6B24C0562F853B92DBF65F7A8310
          5B19D272BAE3BA4BDF3E791F2ECB5209B0ADAEF4C0CECCF43C2E1C634E4A9FEF
          92EE74D7461A5FC935815CBE07EAA53B9558F35BC4DC2D27C0C622026C5B6A0F
          B02535DE854D806D2775B0F112622B5FEFE1B524D84FC3226123B4A120B3534D
          17FBD35804D8765763802D11622B4B4DE1B544802D1F697C35EDD792CDF144DB
          D5351260AB97C21D339FD8F755DF2AC496B7CDF0DAB52F7FC3E47DB80C4B26C0
          B6BAD2033BDBE572F7A9DD941C60CB251CD5B792BF0FA9EF39840CD54B772AE9
          3C7F997587B304D8DA1360CB8F00DB963104D852D8ABB6109B00DB4EEA60E326
          C456AEB584D792603F0D8B848DD08682CC4E251615163177CB6DF639F85EF42E
          98BFAE09B195212DA39AC26B494985ED695FC371D4A2B647890AB0B527C0B69C
          C21DF352484A902D2FB36572E89AAFBC7CF2E770B9954E80AD9D5ACE7D720FB0
          B9BB57194A0E1926397C0FD44BCF57CB79E5BAEB00026CED09B0E547806DCB18
          026C491A674D213601B69DD4C110622BCFDAC26B49B09F8645C24668434166A7
          928217CBACBBD820C0B68260FEFAF2D8DBAF3832790DC7CFB06ABD7852D27674
          DD85EBA1A410DBE4359C83D208B0B527C0B69CC21D1121B67C6C5B16E1B2AA85
          005B3BA5077666D239420E779F5A4430AA0CA52FA71CCE95D54BCF5762DD6F5E
          9AB775D7018AAD970663593701B6FC08B06D194B802D2965993421C0B6933A18
          C981FDAFB84988AD0C6939DDB9FF92774CDE87CBB273C17E1A16091BA10D0599
          F34D3F33EC4F491464961B43802D4921B6927F4BBE3669594C8385E1F22A5DEE
          FB95EDD6BD9D1C522DFBB6CD7144DBD5351260AB97C21D8BDCB6FFD21B84D886
          95E6FF8EEB2E7DFBE47DB88C6A22C0D64EE97706DB2EE79055E9739C7338B04B
          A56E4766D27A36F4B2522F3D5F2D01B6C96B38BEBE08B0B527C0961F01B62D63
          0AB0D5F4285101B69DD4C198F9E4BE8BDF28C496B7CD3BAFED7BD99B26EFC365
          D88B603F0D8B848DD08682CCF96A29C808B02DB7D9E7E07BD1BB60FEFAE691A2
          7948CBA0B64786CE1360CB531AEB749F1ACE452936C7106D57D74880AD5E0A77
          2CB3ED0E60E132A07BB339AFF991A1F304D8DAABE57C673A8E708C432A3D2498
          EBBCF6A5F4EFC3D0DB14F5D2F39574AEBFC810C1AC62EBA5C158D64D802D3F02
          6C5BC614604B6A09B109B0EDA40EC6BCCD9094205B5666CBE4AEAB5FFAAAC99F
          C3E5D69B603F0D8B848DD08682CCF94A2C2CCC336FCD6CF639F85EF42E98BF75
          10621B569AFBDAC36B494945ED695FC371D4283D4AB4F4109B005B7B026CCB29
          DCD18410DBFA6C9BEB7059D44A80ADBD691FC2FE9566E8BB4F454A9FDFB105D8
          2CAFBD512F8D957E3E39440D4080AD3D01B6FC08B06D195B802D2965D9EC4680
          6D27753022426CF9D8B62CC265D5BB603F0D8B848DD08682CCF94A0A5F2C3244
          A141806D05C1FCAD93478AAE579AEB9A1F193AAFA46DE810C5EBA109B0ED9D00
          5BBD14EE5885205B7F66733BA6BBAE6D27C0D65EE97708DB2E87F99C57FADCE6
          38A77DAA21C036649053BD345662ED6F26CDD910358062EBA5C158D64D802D3F
          026C5BC618604B4A0FB109B0EDA40EC62207F6BFE22621B661A5F9BF73FF25EF
          98BC0F97D15A04FB6958246C8436146462A55FE057906966AC01B6C4DDD8D623
          CDF118EEBAB65DEEFB95ED86D856E6A0E47DDC66DFA3EDEA1A09B0D54BE18E55
          DDB6FFD21B04D9BA339BCB34AF933F87733E06026C7B537A686766E8F0CEBC1A
          C28139CDE73A94BA2DD96EC8ED8A7A69ACF400DBE4351C579F04D8DA1360CB8F
          00DB96B106D84A7F94A800DB4EEA602CB3ED0E60E132A07BB3391FE491A1F382
          FD342C1236421B0A32B1D20B32026CCD6CF639F85EF42E98BFA1B81B5B3FD29C
          8EE9AE6BDB09B0E52F8D7BBA7F0DE725679BFD8EB6AB6B24C0562F853BDAFA4C
          88EDEA0BC265C3129379FBCC1C06F33B36026C7B534B802DC9654E93D2E7757A
          CE1B8EAD66A59FEB0BB02D3654BDB4E473C9A14259C5D64B83B1AC9B005B7E04
          D8B68C35C096FCFFDBBB831DC9CEBB0EC3B90D94052C58808809491409C4368B
          2C90588010CA8AD83120458A14B1E4125940128217B000B648445C41E89A4C79
          7AC66FDB9E76FFAAEB543F8B47D6BC96BBCFF9AAFB9CA9AFFF3E7D3AF7A30EB1
          19607B9B7D30BE0C436C97736FADF3B5B8B8B84FC34332C263D8906947DC5C38
          B3665FDEAB638EEF8BB958BFE7747E1A9B41B6AFEEBC8E2FEDA96BF75DFB7DE5
          BED7C79AE771EBFEF31FFFEC9F8EF8838757C75CD7D50B32C076BB6CDCF15519
          647B7F06D73ECB00DB57772BEF6B5E9F479EE3A51D7D4DAFE9EBF3928EFEBA9D
          8EFFB99E9C67BFF461477C1F79F25CEFFF0DB03D9E01B6EB6380ED8D973CC076
          7294D7E95D06D8DE661F8CF761906DE7BCB657F1D4B5FBE23E0D0FC9088F6143
          A69DD6E5A81B32CFB5C96080ED3DC4FA5D03BF56F4AB39ADDD4B1E5C3BBBF6FB
          CA7DCFB5817D2D8E789F7B75CC755DBD20036CB7CBC61D4FC520DB17F0C4B5CF
          6580EDAB3BFAD3C2CE9E7378E7BED3311CFD7DE24B1D60BB85EF85E77AEDEC97
          3EEC88FB7FA7F57AAEF7FF87DD2F8D73B934036CD7C700DB1B2F7D80EDE4884F
          6133C0F636FB60BCAF5F7CFCAD1F1B647B3AE7B53CADEBDD9F73CD9F55DCA7E1
          2119E1316CC83CEC883FD83FB121F3E519606B06D9DE8FC1B5B75DFB7DE5BEE7
          BA5E5E8BD3F91FED5EF7EA78EBBA7A4106D86E978D3B9EDA7948EBF5A056BE6E
          2FC93BEB916B8601B6A770D4352CD7B0AEB73004750D8380CFE116BE170CB0B5
          E7DC2F3DEA00DBDD3FF37CD60CB03D9E01B6EB6380ED0D036CBFF5B5D330D8D1
          86D80CB0BDCD3E188FF5E9109BFF71F371EED6EDD3358CF5BD1A719F86876484
          C7B021F3B0A36EC83CD740C6613764E2FB622ED6EF1A1964FB7C06D7DAB5DF57
          EE7BAEEBE53539DAAF127D75AC755DBD20036CB7CBC61D2BFFFCF177FEFE250F
          B29DCFFDB40E777FCE35E20D036C4FE356DEC7BC3E8F3CC74B39FA5A5EC31A3E
          97D3F5C4EBF738F64B1F765A9B23BD873C79CE81ACC3EE97C6B95C9A01B6EB63
          80ED0D036CBF715A87230DB119607B9B7D30BE2A836CEFEF10836B67719F8687
          6484C7B021F3B0236E3058AFF7F3EA98E3FB622ED6EF9A9D07D98EBEF1FD544E
          EB6070ED6106D88EE748436CAF8EB3AEAB176480ED76D9B863ED577FF33BBF7B
          1EE6BAF561B6FBE7F9C9877FF0C7772DD784CF32C0F6346EE1A96167CFB9B6B7
          3000756D5F9B97760BEFE39FE335B45FFAF98EF2FEF1EC3907B20EBB5F1AE772
          6906D8AE8F01B6370CB0BD7194D7ECC400DBDBEC83F1540CB27D81A33C71ED5D
          719F86876484C7B021F3B0D3DAD890F9F20EBB2113DF1773B17E47701ADAFA9F
          9FFEC9BFBFC441B6D3399FCEDDE0DA1733C0764C47B9DFBD3ACEBAAE5E9001B6
          DB65E38E4BFAE5871F7CEFFE90D75DCBD7F548EE9FCF2F3FFAE0CFEF5A9E3B9F
          CF00DBD338EA3A96D7EFBFF23CD76E6110F0A50FB0790D1FC77EE9E73BD21EE0
          69AD9EF3BDFF61F74BE35C2ECD00DBF531C0F68601B6378EF4AB440DB0BDCD3E
          184FED3CA4F57A502B5FB797E49DF5C835BB6A719F86876484C7B021F3F98EF2
          03FDB3E7DC6038EC864C7C5FCCC5FA1DC96988EB253C95ED7C7E06D7DE8F01B6
          633AADC511EE79AF8EB1AEAB176480ED769D36BE8EAECE8BEB777A42D9FDE1AF
          FFFDC977FFEBAEE7F7F235391DE7FDE3F6A4B5A76180EDE9DCCA7B95D379BCFE
          BAC8F35C3ABF273AB2E75ABB6B713AFF5A97A3A9735B3ABF3FBA6675DC977284
          F5B9AFCEE1524E7B8F754CD7CC001B0F31C0F68601B6B71D6588CD00DBDB5EEF
          23E55A1D817DB0EBF58B8FBFF5E3973CC8F6E9E0DADD3ADCFD39D7E810E23E0D
          0FC9088F71ED8306A737CD77FFEC0BE7051C6928EBB456CF398C6180ED3DC4FA
          1DD5FDA7B2BDDE54CEB53E8AF379185A7BBC6BBFAFDCF79CD7CC6B74845F25FA
          EAF8EABA7A4106D880B5D3D3CBEE0F869DDCF5FCDEBEA4778FE95F3FFAC3BFBC
          EB790E3C9E01B6A7730B4F9E3ABBC6F5058025036CD7C700DB1B06D83EEBB426
          D73EC46680ED6D06D858FBBFBFF9EDDFFF7498EBC687D9EE9FE77FFCF0F7FEF4
          AEE59A1C4ADCA7E12119E1318EF07FCCE545F3428EB03EF7D5395CCA6918AC8E
          E99A19607B5A477D32DBF978CFC77FD7F2FCF8728E74DD34C0F659E721B66B96
          D7D50B32C0063C879FFDE88FFEEADD01B2935FFFF0EBF93DFF68771FAF3ECFE9
          F3DFFDFB3C369ED6519F96748D0356B7F2E4A9B33A4700B855A76129AECBABD7
          26F6499ED269C0E828DEFD9AE5374F62BB6675CC2F9901362EE9930FBFF1FDFB
          435E772D5FD723B97F3E9F7CF48DBFB86B79EE8715F769784846000EA2FE2270
          83AE75A0ED7C3C06D6E0A0EABA7A49754C00CFE8971F7EF0BD7FF9F8DB1FFDEC
          6FBFF3D31A3EFB22A7FFEE34A076FA38F5F10FAFAEE50000C0F5A9BFCF0313A7
          01B0A3ABF3E2FA9D9E50767FF8EB573FF9EE7FDFF5FC79DE35391DE7FDE3BE99
          27AD3DA4EED3F0808C001C44FD45E005380F8C9D7E3DE7257EEDE8F9E39F3F9F
          8135B811755D0500000000001EA7F6E22FE0F4F4B2FB836127773D7FEE7749EF
          1ED3BF7DF4C15FDFF53C879B545F23F0808C001C44FD45E0053B0F96DD771E3A
          3B3B0FA3BD3B9476561FA33E177003EABA0A00000000003C4EEDC53F935F7CF4
          CD1FBC3B4076F2EB1F7E3D87CD1EEDEEE3D5E7397DFEBB7F9FC7F662D4D7083C
          202300000000000000007C6935C474853EF9F01BDFFFF98FBEF5773FFFF8DBFF
          90C3675FE0D57FF7D1377F70FA38F5F179ADBE46E00119010000000000000000
          602D23000000000000000000AC6504000000000000000080B58C000000000000
          000000B09611000000000000000000D632020000000000000000C05A46000000
          00000000000058CB080000000000000000006B19010000000000000000602D23
          000000000000000000AC6504000000000000000080B58C000000000000000000
          B09611000000000000000000D632020000000000000000C05A46000000000000
          00000058CB080000000000000000006B19010000000000000000602D23000000
          000000000000AC6504000000000000000080B58C000000000000000000B09611
          000000000000000000D632020000000000000000C05A46000000000000000000
          58CB080000000000000000006B19010000000000000000602D23000000000000
          000000AC6504000000000000000080B58C000000000000000000B09611000000
          000000000000D632020000000000000000C05A4600000000000000000058CB08
          0000000000000000006B19010000000000000000602D23000000000000000000
          AC6504000000000000000080B58C000000000000000000B09611000000000000
          000000D632020000000000000000C05A4600000000000000000058CB08000000
          0000000000006B19010000000000000000602D23000000000000000000AC6504
          000000000000000080B58C000000000000000000B09611000000000000000000
          D632020000000000000000C05A4600000000000000000058CB08000000000000
          0000006B19010000000000000000602D23000000000000000000AC6504000000
          000000000080B58C000000000000000000B09611000000000000000000D63202
          0000000000000000C05A4600000000000000000058CB08000000000000000000
          6B19010000000000000000602D23000000000000000000AC6504000000000000
          000080B58C000000000000000000B09611000000000000000000D63202000000
          0000000000C05A4600000000000000000058CB080000000000000000006B1901
          0000000000000000602D23000000000000000000AC6504000000000000000080
          B58C000000000000000000B09611000000000000000000D63202000000000000
          0000C05A4600000000000000000058CB080000000000000000006B1901000000
          0000000000602D23000000000000000000AC6504000000000000000080B58C00
          0000000000000000B09611000000000000000000D632020000000000000000C0
          5A4600000000000000000058CB080000000000000000006B1901000000000000
          0000602D23000000000000000000AC6504000000000000000080B58C00000000
          0000000000B09611000000000000000000D632020000000000000000C05A4600
          000000000000000058CB080000000000000000006B1901000000000000000060
          2D23000000000000000000AC6504000000000000000080B58C00000000000000
          0000B09611000000000000000000D632020000000000000000C05A4600000000
          000000000058CB080000000000000000006B19010000000000000000602D2300
          0000000000000000AC6504000000000000000080B58C000000000000000000B0
          9611000000000000000000D632020000000000000000C05A4600000000000000
          000058CB080000000000000000006B19010000000000000000602D2300000000
          0000000000AC6504000000000000000080B58C000000000000000000B0961100
          0000000000000000D632020000000000000000C05A4600000000000000000058
          CB080000000000000000006B19010000000000000000602D2300000000000000
          0000AC6504000000000000000080B58C000000000000000000B0961100000000
          0000000000D632020000000000000000C05A4600000000000000000058CB0800
          00000000000000006B19010000000000000000602D23000000000000000000AC
          6504000000000000000080B58C000000000000000000B0961100000000000000
          0000D632020000000000000000C05A4600000000000000000058CB0800000000
          00000000006B19010000000000000000602D23000000000000000000AC650400
          0000000000000080B58C000000000000000000B09611000000000000000000D6
          32020000000000000000C05A4600000000000000000058CB0800000000000000
          00006B19010000000000000000602D23000000000000000000AC650400000000
          0000000080B58C000000000000000000B09611000000000000000000D6320200
          00000000000000C05A4600000000000000000058CB080000000000000000006B
          19010000000000000000602D23000000000000000000AC650400000000000000
          0080B58C000000000000000000B09611000000000000000000D6320200000000
          00000000C05A4600000000000000000058CB080000000000000000006B190100
          00000000000000602D23000000000000000000AC6504000000000000000080B5
          8C000000000000000000B09611000000000000000000D6320200000000000000
          00C05A4600000000000000000058CB080000000000000000006B190100000000
          00000000602D23000000000000000000AC6504000000000000000080B58C0000
          00000000000000B09611000000000000000000D632020000000000000000C05A
          4600000000000000000058CB080000000000000000006B190100000000000000
          00602D23000000000000000000AC6504000000000000000080B58C0000000000
          00000000B09611000000000000000000D632020000000000000000C05A460000
          0000000000000058CB080000000000000000006B19010000000000000000602D
          23000000000000000000AC6504000000000000000080B58C0000000000000000
          00B09611000000000000000000D632020000000000000000C05A460000000000
          0000000058CB080000000000000000006B19010000000000000000602D230000
          00000000000000AC6504000000000000000080B58C000000000000000000B096
          11000000000000000000D632020000000000000000C05A460000000000000000
          0058CB080000000000000000006B19010000000000000000602D230000000000
          00000000AC6504000000000000000080B58C000000000000000000B096110000
          00000000000000D632020000000000000000C05A4600000000000000000058CB
          080000000000000000006B19010000000000000000602D230000000000000000
          00AC6504000000000000000080B58C000000000000000000B096110000000000
          00000000D632020000000000000000C05A4600000000000000000058CB080000
          000000000000006B19010000000000000000602D23000000000000000000AC65
          04000000000000000080B58C000000000000000000B096110000000000000000
          00D632020000000000000000C05A4600000000000000000058CB080000000000
          000000006B19010000000000000000602D23000000000000000000AC65040000
          00000000000080B58C000000000000000000B09611000000000000000000D632
          020000000000000000C05A4600000000000000000058CB080000000000000000
          006B19010000000000000000602D23000000000000000000AC65040000000000
          00000080B58C000000000000000000B09611000000000000000000D632020000
          000000000000C05A4600000000000000000058CB080000000000000000006B19
          010000000000000000602D23000000000000000000AC65040000000000000000
          80B58C000000000000000000B09611000000000000000000D632020000000000
          000000C05A4600000000000000000058CB080000000000000000006B19010000
          000000000000602D23000000000000000000AC6504000000000000000080B58C
          000000000000000000B09611000000000000000000D632020000000000000000
          C05A4600000000000000000058CB080000000000000000006B19010000000000
          000000602D23000000000000000000AC6504000000000000000080B58C000000
          000000000000B09611000000000000000000D632020000000000000000C05A46
          00000000000000000058CB080000000000000000006B19010000000000000000
          602D23000000000000000000AC6504000000000000000080B58C000000000000
          000000B09611000000000000000000D632020000000000000000C05A46000000
          00000000000058CB080000000000000000006B19010000000000000000602D23
          000000000000000000AC6504000000000000000080B58C000000000000000000
          B09611000000000000000000D632020000000000000000C05A46000000000000
          00000058CB080000000000000000006B19010000000000000000602D23000000
          000000000000AC6504000000000000000080B58C000000000000000000B09611
          000000000000000000D632020000000000000000C05A46000000000000000000
          58CB080000000000000000006B19010000000000000000602D23000000000000
          000000AC6504000000000000000080B58C000000000000000000B09611000000
          000000000000D632020000000000000000C05A4600000000000000000058CB08
          0000000000000000006B19010000000000000000602D23000000000000000000
          AC6504000000000000000080B58C000000000000000000B09611000000000000
          000000D632020000000000000000C05A4600000000000000000058CB08000000
          0000000000006B19010000000000000000602D23000000000000000000AC6504
          000000000000000080B58C000000000000000000B09611000000000000000000
          D632020000000000000000C05A4600000000000000000058CB08000000000000
          0000006B19010000000000000000602D23000000000000000000AC6504000000
          000000000080B58C000000000000000000B09611000000000000000000D63202
          0000000000000000C05A4600000000000000000058CB08000000000000000000
          6B19010000000000000000602D23000000000000000000AC6504000000000000
          000080B58C000000000000000000B09611000000000000000000D63202000000
          0000000000C05A4600000000000000000058CB080000000000000000006B1901
          0000000000000000602D23000000000000000000AC6504000000000000000080
          B58C000000000000000000B09611000000000000000000D63202000000000000
          0000C05A4600000000000000000058CB080000000000000000006B1901000000
          0000000000602D23000000000000000000AC6504000000000000000080B58C00
          0000000000000000B09611000000000000000000D632020000000000000000C0
          5A4600000000000000000058CB080000000000000000006B1901000000000000
          0000602D23000000000000000000AC6504000000000000000080B58C00000000
          0000000000B09611000000000000000000D632020000000000000000C05A4600
          000000000000000058CB080000000000000000006B1901000000000000000060
          2D23000000000000000000AC6504000000000000000080B58C00000000000000
          0000B09611000000000000000000D632020000000000000000C05A4600000000
          000000000058CB080000000000000000006B19010000000000000000602D2300
          0000000000000000AC6504000000000000000080B58C000000000000000000B0
          9611000000000000000000D632020000000000000000C05A4600000000000000
          000058CB080000000000000000006B19010000000000000000602D2300000000
          0000000000AC6504000000000000000080B58C000000000000000000B0961100
          0000000000000000D632020000000000000000C05A4600000000000000000058
          CB080000000000000000006B19010000000000000000602D2300000000000000
          0000AC6504000000000000000080B58C000000000000000000B0961100000000
          0000000000D632020000000000000000C05A4600000000000000000058CB0800
          00000000000000006B19010000000000000000602D23000000000000000000AC
          6504000000000000000080B58C000000000000000000B0961100000000000000
          0000D632020000000000000000C05A4600000000000000000058CB0800000000
          00000000006B19010000000000000000602D23000000000000000000AC650400
          0000000000000080B58C000000000000000000B09611000000000000000000D6
          32020000000000000000C05A4600000000000000000058CB0800000000000000
          00006B19010000000000000000602D23000000000000000000AC650400000000
          0000000080B58C000000000000000000B09611000000000000000000D6320200
          00000000000000C05A4600000000000000000058CB080000000000000000006B
          19010000000000000000602D23000000000000000000AC650400000000000000
          0080B58C000000000000000000B09611000000000000000000D6320200000000
          00000000C05A4600000000000000000058CB080000000000000000006B190100
          00000000000000602D23000000000000000000AC6504000000000000000080B5
          8C000000000000000000B09611000000000000000000D6320200000000000000
          00C05A4600000000000000000058CB080000000000000000006B190100000000
          00000000602D23000000000000000000AC6504000000000000000080B58C0000
          00000000000000B09611000000000000000000D632020000000000000000C05A
          4600000000000000000058CB080000000000000000006B190100000000000000
          00602D23000000000000000000AC6504000000000000000080B58C0000000000
          00000000B09611000000000000000000D632020000000000000000C05A460000
          0000000000000058CB080000000000000000006B19010000000000000000602D
          23000000000000000000AC6504000000000000000080B58C0000000000000000
          00B09611000000000000000000D632020000000000000000C05A460000000000
          0000000058CB080000000000000000006B19010000000000000000602D230000
          00000000000000AC6504000000000000000080B58C000000000000000000B096
          11000000000000000000D632020000000000000000C05A460000000000000000
          0058CB080000000000000000006B19010000000000000000602D230000000000
          00000000AC6504000000000000000080B58C000000000000000000B096110000
          00000000000000D632020000000000000000C05A4600000000000000000058CB
          080000000000000000006B19010000000000000000602D230000000000000000
          00AC6504000000000000000080B58C000000000000000000B096110000000000
          00000000D632020000000000000000C05A4600000000000000000058CB080000
          000000000000006B19010000000000000000602D23000000000000000000AC65
          04000000000000000080B58C000000000000000000B096110000000000000000
          00D632020000000000000000C05A4600000000000000000058CB080000000000
          000000006B19010000000000000000602D23000000000000000000AC65040000
          00000000000080B58C000000000000000000B09611000000000000000000D632
          020000000000000000C05A4600000000000000000058CB080000000000000000
          006B19010000000000000000602D23000000000000000000AC65040000000000
          00000080B58C000000000000000000B09611000000000000000000D632020000
          000000000000C05A4600000000000000000058CB080000000000000000006B19
          010000000000000000602D23000000000000000000AC65040000000000000000
          80B58C000000000000000000B09611000000000000000000D632020000000000
          000000C05A4600000000000000000058CB080000000000000000006B19010000
          000000000000602D23000000000000000000AC6504000000000000000080B58C
          000000000000000000B09611000000000000000000D632020000000000000000
          C05A4600000000000000000058CB080000000000000000006B19010000000000
          000000602D23000000000000000000AC6504000000000000000080B58C000000
          000000000000B09611000000000000000000D632020000000000000000C05A46
          00000000000000000058CB080000000000000000006B19010000000000000000
          602D23000000000000000000AC6504000000000000000080B58C000000000000
          000000B09611000000000000000000D632020000000000000000C05A46000000
          00000000000058CB080000000000000000006B19010000000000000000602D23
          000000000000000000AC6504000000000000000080B58C000000000000000000
          B09611000000000000000000D632020000000000000000C05A46000000000000
          00000058CB080000000000000000006B19010000000000000000602D23000000
          000000000000AC6504000000000000000080B58C000000000000000000B09611
          000000000000000000D632020000000000000000C05A46000000000000000000
          58CB080000000000000000006B19010000000000000000602D23000000000000
          000000AC6504000000000000000080B58C000000000000000000B09611000000
          000000000000D632020000000000000000C05A4600000000000000000058CB08
          0000000000000000006B19010000000000000000602D23000000000000000000
          AC6504000000000000000080B58C000000000000000000B09611000000000000
          000000D632020000000000000000C05A4600000000000000000058CB08000000
          0000000000006B19010000000000000000602D23000000000000000000AC6504
          000000000000000080B58C000000000000000000B09611000000000000000000
          D632020000000000000000C05A4600000000000000000058CB08000000000000
          0000006B19010000000000000000602D23000000000000000000AC6504000000
          000000000080B58C000000000000000000B09611000000000000000000D63202
          0000000000000000C05A4600000000000000000058CB08000000000000000000
          6B19010000000000000000602D23000000000000000000AC6504000000000000
          000080B58C000000000000000000B09611000000000000000000D63202000000
          0000000000C05A4600000000000000000058CB080000000000000000006B1901
          0000000000000000602D23000000000000000000AC6504000000000000000080
          B58C000000000000000000B09611000000000000000000D63202000000000000
          0000C05A4600000000000000000058CB080000000000000000006B1901000000
          0000000000602D23000000000000000000AC6504000000000000000080B58C00
          0000000000000000B09611000000000000000000D632020000000000000000C0
          5A4600000000000000000058CB080000000000000000006B1901000000000000
          0000602D23000000000000000000AC6504000000000000000080B58C00000000
          0000000000B09611000000000000000000D632020000000000000000C05A4600
          000000000000000058CB080000000000000000006B1901000000000000000060
          2D23000000000000000000AC6504000000000000000080B58C00000000000000
          0000B09611000000000000000000D632020000000000000000C05A4600000000
          000000000058CB080000000000000000006B19010000000000000000602D2300
          0000000000000000AC6504000000000000000080B58C000000000000000000B0
          9611000000000000000000D632020000000000000000C05A4600000000000000
          000058CB080000000000000000006B19010000000000000000602D2300000000
          0000000000AC6504000000000000000080B58C000000000000000000B0961100
          0000000000000000D632020000000000000000C05A4600000000000000000058
          CB080000000000000000006B19010000000000000000602D2300000000000000
          0000AC6504000000000000000080B58C000000000000000000B0961100000000
          0000000000D632020000000000000000C05A4600000000000000000058CB0800
          00000000000000006B19010000000000000000602D23000000000000000000AC
          6504000000000000000080B58C000000000000000000B0961100000000000000
          0000D632020000000000000000C05A4600000000000000000058CB0800000000
          00000000006B19010000000000000000602D23000000000000000000AC650400
          0000000000000080B58C000000000000000000B09611000000000000000000D6
          32020000000000000000C05A4600000000000000000058CB0800000000000000
          00006B19010000000000000000602D23000000000000000000AC650400000000
          0000000080B58C000000000000000000B09611000000000000000000D6320200
          00000000000000C05A4600000000000000000058CB080000000000000000006B
          19010000000000000000602D23000000000000000000AC650400000000000000
          0080B58C000000000000000000B09611000000000000000000D6320200000000
          00000000C05A4600000000000000000058CB080000000000000000006B190100
          00000000000000602D23000000000000000000AC6504000000000000000080B5
          8C000000000000000000B09611000000000000000000D6320200000000000000
          00C05A4600000000000000000058CB080000000000000000006B190100000000
          00000000602D23000000000000000000AC6504000000000000000080B58C0000
          00000000000000B09611000000000000000000D632020000000000000000C05A
          4600000000000000000058CB080000000000000000006B190100000000000000
          00602D23000000000000000000AC6504000000000000000080B58C0000000000
          00000000B09611000000000000000000D632020000000000000000C05A460000
          0000000000000058CB080000000000000000006B19010000000000000000602D
          23000000000000000000AC6504000000000000000080B58C0000000000000000
          00B09611000000000000000000D632020000000000000000C05A460000000000
          0000000058CB080000000000000000006B19010000000000000000602D230000
          00000000000000AC6504000000000000000080B58C000000000000000000B096
          11000000000000000000D632020000000000000000C05A460000000000000000
          0058CB080000000000000000006B19010000000000000000602D230000000000
          00000000AC6504000000000000000080B58C000000000000000000B096110000
          00000000000000D632020000000000000000C0D6AFBFF6FFE8CFC6DA14D148F6
          0000000049454E44AE426082}
        Proportional = True
        ExplicitTop = 3
      end
    end
    object pnTotalSistemas: TPanel
      Left = 0
      Top = 391
      Width = 966
      Height = 74
      Align = alBottom
      BevelOuter = bvNone
      Color = clWhite
      ParentBackground = False
      TabOrder = 1
      DesignSize = (
        966
        74)
      object lbCallCenter: TLabel
        Left = 880
        Top = 9
        Width = 82
        Height = 16
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = '(21)2621-4570'
        Font.Charset = ANSI_CHARSET
        Font.Color = 5511957
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object lbMsn: TLabel
        Left = 825
        Top = 31
        Width = 137
        Height = 16
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'sos_total@hotmail.com'
        Font.Charset = ANSI_CHARSET
        Font.Color = 5511957
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 830
        Top = 53
        Width = 132
        Height = 16
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'www.totalsistemas.info'
        Font.Charset = ANSI_CHARSET
        Font.Color = 5511957
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
    end
  end
  object acmMenu: TActionManager
    ActionBars = <
      item
        Items = <
          item
            ChangesAllowed = [caModify]
            Items = <
              item
                Caption = '&ActionClientItem0'
              end>
            Caption = '&ActionClientItem0'
            KeyTip = 'F'
          end>
        AutoSize = False
      end
      item
        Items = <
          item
            Action = actSupCriptografia
            Caption = '&Criptografia'
            ImageIndex = 9
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end
          item
            Action = actSupBackup
            Caption = '&Backup'
            ImageIndex = 6
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end
          item
            Action = actSupArquivoIni
            Caption = '&Ini'
            ImageIndex = 0
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end
          item
            Action = actSupSQL
            Caption = '&SQL'
            ImageIndex = 49
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end>
      end
      item
        Items = <
          item
            Action = actConfigFiltroLog
            Caption = '&Log'
            ImageIndex = 21
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end
          item
            Action = actConfigSistema
            Caption = '&Sistema'
            ImageIndex = 40
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end>
      end
      item
        Items = <
          item
            Action = actFiltroLancamento
            ImageIndex = 11
            CommandProperties.ButtonSize = bsLarge
          end
          item
            Items = <
              item
                Caption = '&Incluir'
              end
              item
                Caption = '&Consultar'
              end
              item
                Caption = 'I&mprimir'
              end>
            Action = actCadReceita
            ImageIndex = 30
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end
          item
            Action = actCadDespesa
            ImageIndex = 6
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end>
      end
      item
        Items = <
          item
            Items = <
              item
                Caption = '&Incluir'
              end
              item
                Caption = '&Consultar'
              end
              item
                Caption = '&Excluir'
              end>
            Action = actCadDespesa
            ImageIndex = 30
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end>
      end
      item
        Items = <
          item
            Action = actFiltroLancamento
            ImageIndex = 11
            CommandProperties.ButtonSize = bsLarge
          end
          item
            Action = actCadReceita
            ImageIndex = 30
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end
          item
            Action = actCadDespesa
            ImageIndex = 6
            CommandProperties.ButtonSize = bsLarge
          end>
      end
      item
      end
      item
        Items = <
          item
            Action = actConfigCadUsuarios
            Caption = '&Usu'#225'rios'
            ImageIndex = 34
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end
          item
            Action = actConfigCadPermissoes
            Caption = '&Permiss'#245'es'
            ImageIndex = 29
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end>
      end
      item
      end
      item
        Items = <
          item
            Action = actCadColaboradores
            Caption = 'Co&laboradores'
            ImageIndex = 31
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end
          item
            Action = actCadClientes
            Caption = '&Clientes'
            ImageIndex = 18
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end
          item
            Action = actCadFornecedores
            Caption = 'F&ornecedores'
            ImageIndex = 17
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end>
      end
      item
      end
      item
        Items = <
          item
            Action = actCadCategoriaReceitas
            Caption = '&Categorias de Receita'
            ImageIndex = 21
            CommandProperties.ButtonSize = bsLarge
          end
          item
            Action = actCadSubcatReceitas
            Caption = '&Subcategorias de Receitas'
            ImageIndex = 19
            CommandProperties.ButtonSize = bsLarge
          end
          item
            Action = actCadCategoriaDespesas
            Caption = 'C&ategorias de Despesas'
            ImageIndex = 21
            CommandProperties.ButtonSize = bsLarge
          end
          item
            Action = actCadSubcatDespesas
            Caption = 'S&ubcategorias de Despesas'
            ImageIndex = 19
            CommandProperties.ButtonSize = bsLarge
          end
          item
            Action = actCadFormaPagto
            Caption = '&Formas de Pagamento'
            ImageIndex = 33
            CommandProperties.ButtonSize = bsLarge
          end>
      end
      item
        Items = <
          item
            Action = actRelFechamentoCaixaFP
            ImageIndex = 1
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end
          item
            Action = actRelRecibosSelos
            Caption = 'R&ecibos x Selos'
            ImageIndex = 39
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end
          item
            Action = actRelFaturamentoMensal
            Caption = '&Faturamento Mensal'
            ImageIndex = 50
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end
          item
            Action = actRelInadimplencias
            ImageIndex = 42
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end>
      end
      item
        Items = <
          item
            Action = actRelExtratoContas
            ImageIndex = 55
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end
          item
            Action = actRelRecibo1Via
            ImageIndex = 39
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end
          item
            Action = actRelFlutuantes
            Caption = '&Flutuantes'
            ImageIndex = 29
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end
          item
            Action = actRelRepasses
            Caption = 'R&epasses'
            ImageIndex = 4
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end>
      end
      item
      end
      item
        Items = <
          item
            Action = actCadProdutos
            Caption = '&Produtos'
            ImageIndex = 47
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end
          item
            Action = actCadServicos
            Caption = '&Servi'#231'os'
            ImageIndex = 13
            CommandProperties.ButtonSize = bsLarge
            CommandProperties.GroupPosition = gpMiddle
          end>
      end
      item
      end
      item
      end
      item
        Items = <
          item
            Action = actCadClientes
            Caption = '&Clientes'
            ImageIndex = 18
            CommandProperties.ButtonSize = bsLarge
          end
          item
            Action = actCadFornecedores
            Caption = '&Fornecedores'
            ImageIndex = 17
            CommandProperties.ButtonSize = bsLarge
          end>
      end>
    DisabledImages = dmGerencial.Im32Dis
    LargeDisabledImages = dmGerencial.Im32Dis
    LargeImages = dmGerencial.Im32
    Images = dmGerencial.Im32
    Left = 48
    Top = 288
    StyleName = 'Platform Default'
    object actSupCriptografia: TAction
      Category = 'Suporte'
      Caption = 'Criptografia'
      Hint = 'Criptografar e Descriptogravar Strings'
      ImageIndex = 9
      OnExecute = actSupCriptografiaExecute
    end
    object actConfigFiltroLog: TAction
      Category = 'Configuracoes'
      Caption = 'Log'
      Hint = 'Consulta de Log de Usu'#225'rios'
      ImageIndex = 21
      OnExecute = actConfigFiltroLogExecute
    end
    object actConfigSistema: TAction
      Category = 'Configuracoes'
      Caption = 'Sistema'
      Hint = 'Configura'#231#245'es de par'#226'metros do Sistema'
      ImageIndex = 40
      OnExecute = actConfigSistemaExecute
    end
    object actCadColaboradores: TAction
      Category = 'Financeiro'
      Caption = 'Colaboradores'
      Hint = 'Inclus'#227'o, Edi'#231#227'o, Exclus'#227'o e Impress'#227'o de Colaboradores'
      ImageIndex = 31
      OnExecute = actCadColaboradoresExecute
    end
    object actColaboradoresMenu: TAction
      Category = 'Financeiro'
      Caption = 'Colaboradores'
      ImageIndex = 31
      OnExecute = actColaboradoresMenuExecute
    end
    object actCadFechamentoComissoes: TAction
      Category = 'Financeiro'
      Caption = 'Fechamento de Comiss'#245'es'
      ImageIndex = 23
      OnExecute = actCadFechamentoComissoesExecute
    end
    object actCadFechamentoPagtoFuncs: TAction
      Category = 'Financeiro'
      Caption = 'Fechamento de Pagamentos'
      ImageIndex = 30
      OnExecute = actCadFechamentoPagtoFuncsExecute
    end
    object actCadClientes: TAction
      Category = 'Financeiro'
      Caption = 'Clientes'
      ImageIndex = 18
      OnExecute = actCadClientesExecute
    end
    object actCadFornecedores: TAction
      Category = 'Financeiro'
      Caption = 'Fornecedores'
      Hint = 'Inclus'#227'o, Edi'#231#227'o, Exclus'#227'o e Impress'#227'o de Fornecedores'
      ImageIndex = 17
      OnExecute = actCadFornecedoresExecute
    end
    object actCadProdutos: TAction
      Category = 'Financeiro'
      Caption = 'Produtos'
      ImageIndex = 47
      OnExecute = actCadProdutosExecute
    end
    object actProdutosMenu: TAction
      Category = 'Financeiro'
      Caption = 'Produtos'
      ImageIndex = 47
      OnExecute = actProdutosMenuExecute
    end
    object actEstoque: TAction
      Category = 'Financeiro'
      Caption = 'Estoque'
      ImageIndex = 1
      OnExecute = actEstoqueExecute
    end
    object actCadServicos: TAction
      Category = 'Financeiro'
      Caption = 'Servi'#231'os'
      ImageIndex = 13
      OnExecute = actCadServicosExecute
    end
    object actFiltroLancamento: TAction
      Category = 'Financeiro'
      Caption = 'Busca Lan'#231'amentos'
      Hint = 'Busca de Receitas e Despesas'
      ImageIndex = 11
      OnExecute = actFiltroLancamentoExecute
    end
    object actCadReceita: TAction
      Category = 'Financeiro'
      Caption = 'Lan'#231'ar Receita'
      Hint = 'Inclus'#227'o de Receitas'
      ImageIndex = 30
      OnExecute = actCadReceitaExecute
    end
    object actCadDespesa: TAction
      Category = 'Financeiro'
      Caption = 'Lan'#231'ar Despesa'
      Hint = 'Inclus'#227'o de Despesas'
      ImageIndex = 6
      OnExecute = actCadDespesaExecute
    end
    object actRepasseRecMenu: TAction
      Category = 'Financeiro'
      Caption = 'Repasse de Receitas'
      ImageIndex = 56
      OnExecute = actRepasseRecMenuExecute
    end
    object actDepositosFlutuantesMenu: TAction
      Category = 'Financeiro'
      Caption = 'Dep'#243'sitos'
      ImageIndex = 56
      OnExecute = actDepositosFlutuantesMenuExecute
    end
    object actCadDepositoFlutuante: TAction
      Category = 'Financeiro'
      Caption = 'Cadastro'
      ImageIndex = 56
      OnExecute = actCadDepositoFlutuanteExecute
    end
    object actCadVinculoLancDeposito: TAction
      Category = 'Financeiro'
      Caption = 'Vincular Receitas e Dep'#243'sitos'
      ImageIndex = 58
      OnExecute = actCadVinculoLancDepositoExecute
    end
    object actCadBaixaUmFlutuante: TAction
      Category = 'Financeiro'
      Caption = 'Vincular '#224' Despesa'
      ImageIndex = 58
      OnExecute = actCadBaixaUmFlutuanteExecute
    end
    object actCadBaixaGpFlutuantes: TAction
      Category = 'Financeiro'
      Caption = 'Gerar Nova Despesa'
      ImageIndex = 59
      OnExecute = actCadBaixaGpFlutuantesExecute
    end
    object actConfigAtribuicoes: TAction
      Category = 'Configuracoes'
      Caption = 'Atribui'#231#245'es'
      Hint = 'Configura'#231#227'o das Atribui'#231#245'es ativas na Serventia'
      ImageIndex = 59
      OnExecute = actConfigAtribuicoesExecute
    end
    object actConfigCadUsuarios: TAction
      Category = 'Configuracoes'
      Caption = 'Usu'#225'rios'
      Hint = 'Inclus'#227'o, Edi'#231#227'o e Exclus'#227'o de Usu'#225'rios'
      ImageIndex = 34
      OnExecute = actConfigCadUsuariosExecute
    end
    object actConfigCadPermissoes: TAction
      Category = 'Configuracoes'
      Caption = 'Permiss'#245'es'
      Hint = 'Inclus'#227'o, Edi'#231#227'o e Exclus'#227'o de Permiss'#245'es de Usu'#225'rios'
      ImageIndex = 35
      OnExecute = actConfigCadPermissoesExecute
    end
    object actCadFeriado: TAction
      Category = 'Configuracoes'
      Caption = 'Feriados'
      ImageIndex = 54
      OnExecute = actCadFeriadoExecute
    end
    object actCadNatureza: TAction
      Category = 'Configuracoes'
      Caption = 'Naturezas'
      ImageIndex = 10
      OnExecute = actCadNaturezaExecute
    end
    object actCadCargo: TAction
      Category = 'Configuracoes'
      Caption = 'Cargos'
      ImageIndex = 31
      OnExecute = actCadCargoExecute
    end
    object actCadFormaPagto: TAction
      Category = 'Configuracoes'
      Caption = 'Formas de Pagamento'
      ImageIndex = 33
      OnExecute = actCadFormaPagtoExecute
    end
    object actNiveisLancamentoMenu: TAction
      Category = 'Configuracoes'
      Caption = 'N'#237'veis Lan'#231'amento'
      ImageIndex = 19
      OnExecute = actNiveisLancamentoMenuExecute
    end
    object actCadCategoriaReceitas: TAction
      Category = 'Configuracoes'
      Caption = 'Categorias de Receita'
      Hint = 'Inclus'#227'o, Edi'#231#227'o, Exclus'#227'o e Impress'#227'o de Tipos de Receitas'
      ImageIndex = 21
      OnExecute = actCadCategoriaReceitasExecute
    end
    object actCadSubcatReceitas: TAction
      Category = 'Configuracoes'
      Caption = 'Subcategorias de Receitas'
      ImageIndex = 19
      OnExecute = actCadSubcatReceitasExecute
    end
    object actCadCategoriaDespesas: TAction
      Category = 'Configuracoes'
      Caption = 'Categorias de Despesas'
      Hint = 'Inclus'#227'o, Edi'#231#227'o, Exclus'#227'o e Impress'#227'o de Tipos de Despesas'
      ImageIndex = 21
      OnExecute = actCadCategoriaDespesasExecute
    end
    object actCadSubcatDespesas: TAction
      Category = 'Configuracoes'
      Caption = 'Subcategorias de Despesas'
      ImageIndex = 19
      OnExecute = actCadSubcatDespesasExecute
    end
    object actRelCReceberCPagar: TAction
      Category = 'Relatorios'
      Caption = 'C. Receber x C. Pagar'
      ImageIndex = 60
      OnExecute = actRelCReceberCPagarExecute
    end
    object actRelFechamentoCaixaFP: TAction
      Category = 'Relatorios'
      Caption = 'Por Forma de Pagamento'
      ImageIndex = 57
      OnExecute = actRelFechamentoCaixaFPExecute
    end
    object actRelFechamentoCaixaTA: TAction
      Category = 'Relatorios'
      Caption = 'Por Tipo de Ato'
      ImageIndex = 10
      OnExecute = actRelFechamentoCaixaTAExecute
    end
    object actRelRecibosSelos: TAction
      Category = 'Relatorios'
      Caption = 'Recibos x Selos'
      ImageIndex = 39
    end
    object actRelFaturamentoMensal: TAction
      Category = 'Relatorios'
      Caption = 'Faturamento Mensal'
      ImageIndex = 30
    end
    object actRelExtratoContas: TAction
      Category = 'Relatorios'
      Caption = 'Extrato de Contas'
      ImageIndex = 55
      OnExecute = actRelExtratoContasExecute
    end
    object actSupBackup: TAction
      Category = 'Suporte'
      Caption = 'Backup'
      ImageIndex = 6
    end
    object actSupArquivoIni: TAction
      Category = 'Suporte'
      Caption = 'Ini'
      ImageIndex = 0
      OnExecute = actSupArquivoIniExecute
    end
    object actSupSQL: TAction
      Category = 'Suporte'
      Caption = 'SQL'
      ImageIndex = 49
    end
    object actRelMovimentos: TAction
      Category = 'Relatorios'
      Caption = 'Movimento de Caixa'
      ImageIndex = 50
      OnExecute = actRelMovimentosExecute
    end
    object actRelFlutuantes: TAction
      Category = 'Relatorios'
      Caption = 'Flutuantes'
      ImageIndex = 56
      OnExecute = actRelFlutuantesExecute
    end
    object actRelRepasses: TAction
      Category = 'Relatorios'
      Caption = 'Repasses'
      ImageIndex = 4
      OnExecute = actRelRepassesExecute
    end
    object actRelAtos: TAction
      Category = 'Relatorios'
      Caption = 'Atos'
      ImageIndex = 13
      OnExecute = actRelAtosExecute
    end
    object actCarneLeaoMenu: TAction
      Category = 'Financeiro'
      Caption = 'Carn'#234'-Le'#227'o'
      ImageIndex = 62
      OnExecute = actCarneLeaoMenuExecute
    end
    object actCadRelacaoPlanoContas: TAction
      Category = 'Financeiro'
      Caption = 'Rela'#231#227'o de Plano de Contas'
      ImageIndex = 63
      OnExecute = actCadRelacaoPlanoContasExecute
    end
    object actCadCarneLeao: TAction
      Category = 'Financeiro'
      Caption = 'Gera'#231#227'o de Carn'#234'-Le'#227'o'
      ImageIndex = 62
      OnExecute = actCadCarneLeaoExecute
    end
    object actRelFinanceiroMenu: TAction
      Category = 'Financeiro'
      Caption = 'Relat'#243'rios'
      ImageIndex = 39
      OnExecute = actRelFinanceiroMenuExecute
    end
    object actCadVales: TAction
      Category = 'Financeiro'
      Caption = 'Vales'
      ImageIndex = 41
      OnExecute = actCadValesExecute
    end
    object actCaixaMenu: TAction
      Category = 'Financeiro'
      Caption = 'Caixa'
      Hint = 'Abertura, Fechamento e Movimenta'#231#227'o de Caixa'
      ImageIndex = 55
      OnExecute = actCaixaMenuExecute
    end
    object actOficRecebidos: TAction
      Category = 'Oficios'
      Caption = 'Receber'
      ImageIndex = 29
      OnExecute = actOficRecebidosExecute
    end
    object actOficEnviados: TAction
      Category = 'Oficios'
      Caption = 'Enviar'
      ImageIndex = 28
      OnExecute = actOficEnviadosExecute
    end
    object actCadOutrosLancamentos: TAction
      Category = 'Financeiro'
      Caption = 'Outros Lan'#231'amentos'
      ImageIndex = 6
      OnExecute = actCadOutrosLancamentosExecute
    end
    object actRelDespesasColaborador: TAction
      Category = 'Relatorios'
      Caption = 'Despesas com Colaboradores'
      ImageIndex = 41
      OnExecute = actRelDespesasColaboradorExecute
    end
    object actRelInadimplencias: TAction
      Category = 'Relatorios'
      Caption = 'Inadimpl'#234'ncias'
      ImageIndex = 42
    end
    object actRelRecibo1Via: TAction
      Category = 'Relatorios'
      Caption = 'Recibo (1'#170' Via)'
      ImageIndex = 39
      OnExecute = actRelRecibo1ViaExecute
    end
    object actFiltroOficio: TAction
      Category = 'Oficios'
      Caption = 'Busca Of'#237'cios'
      Hint = 'Busca Of'#237'cios Enviados e Recebidos'
      ImageIndex = 11
      OnExecute = actFiltroOficioExecute
    end
    object actRelHistoricoRecibos: TAction
      Category = 'Relatorios'
      Caption = 'Hist'#243'rico de Recibos'
      ImageIndex = 29
      OnExecute = actRelHistoricoRecibosExecute
    end
    object actRelRecibosCancelados: TAction
      Category = 'Relatorios'
      Caption = 'Recibos Cancelados'
      ImageIndex = 7
      OnExecute = actRelRecibosCanceladosExecute
    end
    object actRelDiferencaFechCaixa: TAction
      Category = 'Relatorios'
      Caption = 'Diferen'#231'as'
      ImageIndex = 6
      OnExecute = actRelDiferencaFechCaixaExecute
    end
    object actRelImpostoRenda: TAction
      Category = 'Relatorios'
      Caption = 'Imposto de Renda'
      ImageIndex = 62
      OnExecute = actRelImpostoRendaExecute
    end
    object actFiltroLoteFolhasMenu: TAction
      Category = 'Folhas'
      Caption = 'Busca Lote Folhas'
      ImageIndex = 11
      OnExecute = actFiltroLoteFolhasMenuExecute
    end
    object actFiltroLoteFolhaSeguranca: TAction
      Category = 'Folhas'
      Caption = 'Lote de Folhas de Seguran'#231'a'
      Hint = 'Busca Lotes de Folhas de Seguran'#231'a'
      ImageIndex = 64
      OnExecute = actFiltroLoteFolhaSegurancaExecute
    end
    object actFiltroLoteFolhaRCPN: TAction
      Category = 'Folhas'
      Caption = 'Lote de Folhas RCPN'
      Hint = 'Busca Lotes de Folhas de RCPN'
      ImageIndex = 68
      OnExecute = actFiltroLoteFolhaRCPNExecute
    end
    object actCadLoteFolhaSeguranca: TAction
      Category = 'Folhas'
      Caption = 'Lt. Folhas Seguran'#231'a'
      Hint = 'Inclus'#227'o de Lote de Folhas de Seguran'#231'a'
      ImageIndex = 64
      OnExecute = actCadLoteFolhaSegurancaExecute
    end
    object actCadLoteFolhaRCPN: TAction
      Category = 'Folhas'
      Caption = 'Lt. Folhas RCPN'
      Hint = 'Inclus'#227'o de Lote de Folhas de RCPN'
      ImageIndex = 68
      OnExecute = actCadLoteFolhaRCPNExecute
    end
    object actFiltroLoteEtiquetas: TAction
      Category = 'Etiquetas'
      Caption = 'Busca Lote Etiquetas'
      Hint = 'Busca de Lote de Etiquetas'
      ImageIndex = 11
      OnExecute = actFiltroLoteEtiquetasExecute
    end
    object actCadLoteEtiqueta: TAction
      Category = 'Etiquetas'
      Caption = 'Lote Etiquetas'
      Hint = 'Inclus'#227'o de Lote de Etiqueta'
      ImageIndex = 65
      OnExecute = actCadLoteEtiquetaExecute
    end
    object actFiltroFolhaSeguranca: TAction
      Category = 'Folhas'
      Caption = 'Folha de Seguran'#231'a'
      Hint = 'Busca de Folha de Seguran'#231'a'
      ImageIndex = 66
      OnExecute = actFiltroFolhaSegurancaExecute
    end
    object actFiltroFolhaRCPN: TAction
      Category = 'Folhas'
      Caption = 'Folha RCPN'
      Hint = 'Busca de Folha de RCPN'
      ImageIndex = 67
      OnExecute = actFiltroFolhaRCPNExecute
    end
    object actCadEtiqueta: TAction
      Category = 'Etiquetas'
      Caption = 'Etiqueta'
      Hint = 'Inclus'#227'o de Etiqueta'
      ImageIndex = 47
      OnExecute = actCadEtiquetaExecute
    end
    object actRelEtiquetasMenu: TAction
      Category = 'Etiquetas'
      Caption = 'Relat'#243'rios'
      ImageIndex = 39
      OnExecute = actRelEtiquetasMenuExecute
    end
    object actRelFolhasMenu: TAction
      Category = 'Folhas'
      Caption = 'Relat'#243'rios'
      ImageIndex = 39
      OnExecute = actRelFolhasMenuExecute
    end
    object actRelEtqLoteEtiquetas: TAction
      Category = 'Etiquetas'
      Caption = 'Lote de Etiquetas'
      ImageIndex = 65
      OnExecute = actRelEtqLoteEtiquetasExecute
    end
    object actRelEtqEtiquetas: TAction
      Category = 'Etiquetas'
      Caption = 'Etiquetas'
      ImageIndex = 47
      OnExecute = actRelEtqEtiquetasExecute
    end
    object actRelFlsSegLoteFolhasSeguranca: TAction
      Category = 'Folhas'
      Caption = 'Lote de Folhas de Seguran'#231'a'
      ImageIndex = 64
      OnExecute = actRelFlsSegLoteFolhasSegurancaExecute
    end
    object actRelFlsSegLoteFolhasSegurancaRCPN: TAction
      Category = 'Folhas'
      Caption = 'Lote de Folhas de Seguran'#231'a RCPN'
      ImageIndex = 68
      OnExecute = actRelFlsSegLoteFolhasSegurancaRCPNExecute
    end
    object actRelFlsSegFolhasSeguranca: TAction
      Category = 'Folhas'
      Caption = 'Folhas de Seguran'#231'a'
      ImageIndex = 66
      OnExecute = actRelFlsSegFolhasSegurancaExecute
    end
    object actRelFlsSegFolhasSegurancaRCPN: TAction
      Category = 'Folhas'
      Caption = 'Folhas de Seguran'#231'a RCPN'
      ImageIndex = 67
      OnExecute = actRelFlsSegFolhasSegurancaRCPNExecute
    end
    object actConfigCadEmails: TAction
      Category = 'Configuracoes'
      Caption = 'Emails'
      ImageIndex = 69
      OnExecute = actConfigCadEmailsExecute
    end
    object actSupAtualizarBancos: TAction
      Category = 'Suporte'
      Caption = 'Atualizar Bancos'
      ImageIndex = 4
      OnExecute = actSupAtualizarBancosExecute
    end
    object actSupDownloadBancoM: TAction
      Category = 'Suporte'
      Caption = 'Download Banco Vazio'
      ImageIndex = 50
      OnExecute = actSupDownloadBancoMExecute
    end
    object actSupAtualizarRelatorios: TAction
      Category = 'Suporte'
      Caption = 'Atualizar Relat'#243'rios'
      ImageIndex = 2
      OnExecute = actSupAtualizarRelatoriosExecute
    end
    object actSupBaixarHomologacao: TAction
      Category = 'Suporte'
      Caption = 'Material para Homologa'#231#227'o'
      ImageIndex = 6
      OnExecute = actSupBaixarHomologacaoExecute
    end
    object actSupCorrigirImportacoesDuplicadas: TAction
      Category = 'Suporte'
      Caption = 'Corrigir Importa'#231#245'es Duplicadas'
      ImageIndex = 13
      OnExecute = actSupCorrigirImportacoesDuplicadasExecute
    end
    object actSupVoltarVersaoSistema: TAction
      Category = 'Suporte'
      Caption = 'Voltar Vers'#227'o Sistema'
      ImageIndex = 4
      OnExecute = actSupVoltarVersaoSistemaExecute
    end
  end
  object ppmMenuNiveisLanc: TJvPopupMenu
    Images = dmGerencial.Im32
    Cursor = crHandPoint
    DisabledImages = dmGerencial.Im32Dis
    ImageMargin.Left = 0
    ImageMargin.Top = 0
    ImageMargin.Right = 0
    ImageMargin.Bottom = 0
    ImageSize.Height = 0
    ImageSize.Width = 0
    Left = 144
    Top = 148
    object Natureza1: TMenuItem
      Action = actCadNatureza
    end
    object N1: TMenuItem
      Caption = '-'
      Enabled = False
    end
    object CategoriasdeReceita1: TMenuItem
      Action = actCadCategoriaReceitas
    end
    object SubcategoriasdeReceitas1: TMenuItem
      Action = actCadSubcatReceitas
    end
    object N2: TMenuItem
      Caption = '-'
      Enabled = False
    end
    object CategoriasdeDespesas1: TMenuItem
      Action = actCadCategoriaDespesas
    end
    object SubcategoriasdeDespesas1: TMenuItem
      Action = actCadSubcatDespesas
    end
  end
  object ppmRelatoriosFinanceiro: TJvPopupMenu
    Images = dmGerencial.Im32
    Cursor = crHandPoint
    DisabledImages = dmGerencial.Im32Dis
    ImageMargin.Left = 0
    ImageMargin.Top = 0
    ImageMargin.Right = 0
    ImageMargin.Bottom = 0
    ImageSize.Height = 0
    ImageSize.Width = 0
    Left = 400
    Top = 148
    object CReceberxCPagar1: TMenuItem
      Action = actRelCReceberCPagar
    end
    object Movimentos1: TMenuItem
      Action = actRelExtratoContas
    end
    object MovimentodeCaixa1: TMenuItem
      Action = actRelMovimentos
    end
    object ReceitasxDespesas1: TMenuItem
      Caption = 'Fechamento de Caixa'
      ImageIndex = 1
      object PorFormaPagamento1: TMenuItem
        Action = actRelFechamentoCaixaFP
      end
      object PorTipoAto1: TMenuItem
        Action = actRelFechamentoCaixaTA
      end
      object DiferenadoFechamentodoCaixa1: TMenuItem
        Action = actRelDiferencaFechCaixa
      end
    end
    object I1: TMenuItem
      Action = actRelImpostoRenda
    end
    object N3: TMenuItem
      Caption = '-'
      Enabled = False
    end
    object Atos1: TMenuItem
      Action = actRelAtos
    end
    object Flutuantes1: TMenuItem
      Action = actRelFlutuantes
    end
    object Repasses1: TMenuItem
      Action = actRelRepasses
    end
    object DespesascomColaboradores1: TMenuItem
      Action = actRelDespesasColaborador
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object Inadimplentes1: TMenuItem
      Action = actRelInadimplencias
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Recibo1: TMenuItem
      Caption = 'Recibos'
      ImageIndex = 57
      object ModelodeRecibo1: TMenuItem
        Action = actRelRecibo1Via
      end
      object HistricodeRecibos1: TMenuItem
        Action = actRelHistoricoRecibos
      end
      object RecibosCancelados1: TMenuItem
        Action = actRelRecibosCancelados
      end
      object RecibosxSelos1: TMenuItem
        Action = actRelRecibosSelos
      end
    end
  end
  object ppmColaboradores: TJvPopupMenu
    Images = dmGerencial.Im32
    Cursor = crHandPoint
    DisabledImages = dmGerencial.Im32Dis
    ImageMargin.Left = 0
    ImageMargin.Top = 0
    ImageMargin.Right = 0
    ImageMargin.Bottom = 0
    ImageSize.Height = 0
    ImageSize.Width = 0
    Left = 144
    Top = 196
    object Colaboradores1: TMenuItem
      Action = actCadColaboradores
    end
    object FechamentodeComisses1: TMenuItem
      Action = actCadFechamentoComissoes
    end
    object Pagamento1: TMenuItem
      Action = actCadFechamentoPagtoFuncs
    end
    object O1: TMenuItem
      Caption = 'Despesas com Colaboradores'
      ImageIndex = 6
      object Vales2: TMenuItem
        Action = actCadVales
      end
      object OutrasDespesas1: TMenuItem
        Action = actCadOutrosLancamentos
      end
    end
  end
  object ppmProdutos: TJvPopupMenu
    Images = dmGerencial.Im32
    Cursor = crHandPoint
    DisabledImages = dmGerencial.Im32Dis
    ImageMargin.Left = 0
    ImageMargin.Top = 0
    ImageMargin.Right = 0
    ImageMargin.Bottom = 0
    ImageSize.Height = 0
    ImageSize.Width = 0
    Left = 144
    Top = 244
    object Estoque1: TMenuItem
      Action = actEstoque
    end
    object Produtos1: TMenuItem
      Action = actCadProdutos
    end
  end
  object ppmReceita: TJvPopupMenu
    Images = dmGerencial.Im32
    Cursor = crHandPoint
    DisabledImages = dmGerencial.Im32Dis
    ImageMargin.Left = 0
    ImageMargin.Top = 0
    ImageMargin.Right = 0
    ImageMargin.Bottom = 0
    ImageSize.Height = 0
    ImageSize.Width = 0
    Left = 272
    Top = 148
    object L5: TMenuItem
      Action = actCadReceita
    end
    object Depsito1: TMenuItem
      Action = actDepositosFlutuantesMenu
      object Depsito2: TMenuItem
        Action = actCadDepositoFlutuante
      end
      object VincularReceitaseDepsitos1: TMenuItem
        Action = actCadVinculoLancDeposito
      end
    end
  end
  object ppmCarneLeao: TJvPopupMenu
    Images = dmGerencial.Im32
    Cursor = crHandPoint
    DisabledImages = dmGerencial.Im32Dis
    ImageMargin.Left = 0
    ImageMargin.Top = 0
    ImageMargin.Right = 0
    ImageMargin.Bottom = 0
    ImageSize.Height = 0
    ImageSize.Width = 0
    Left = 272
    Top = 244
    object RelaodePlanodeContas1: TMenuItem
      Action = actCadRelacaoPlanoContas
    end
    object GeraodeCarnLeo1: TMenuItem
      Action = actCadCarneLeao
    end
  end
  object ppmFiltroLoteFolhas: TJvPopupMenu
    Images = dmGerencial.Im32
    Cursor = crHandPoint
    DisabledImages = dmGerencial.Im32Dis
    ImageMargin.Left = 0
    ImageMargin.Top = 0
    ImageMargin.Right = 0
    ImageMargin.Bottom = 0
    ImageSize.Height = 0
    ImageSize.Width = 0
    Left = 600
    Top = 140
    object L1: TMenuItem
      Action = actFiltroLoteFolhaSeguranca
      Bitmap.Data = {
        36100000424D3610000000000000360000002800000020000000200000000100
        2000000000000010000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000030000000900000007000000020000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000000070000002006132374030A145A0000001B0000
        0005000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000010000000C010305360E2B4FBF2361A0FF195191FF091C36A90000
        012C000000080000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00030000001605101B6317487DEA2C7CB9FF53AADEFF2E92D5FF2064A5FF0F2E
        5CDB010203410000000E00000002000000000000000000000000000000000000
        00000000000000000000000000000000000000000000000000010000000A0000
        001C0000001B0000000A00000001000000000000000000000001000000070000
        00200E2B49A12567A5FD3A97D0FF3EA3DBFF62B3E1FF2F95D6FF2E92D4FF2679
        BBFF133C76F60308106500000017000000040000000000000000000000000000
        000000000000000000000000000000000000000000040000001802080E570E2C
        58D40D2A56D402050A500000001600000004000000020000000D040C14431A4D
        7DD63485BEFF48AADDFF46A9DEFF43A7DDFF71BCE5FF3197D6FF2F94D5FF2D92
        D4FF2A88CBFF174988FE06132895000000230000000700000000000000000000
        00000000000000000000000000020000000B0000002A0A203CA6194E8BFB3987
        C2FF2576B8FF143D7AF80714299700000028000000130D283E782870A9F648A4
        D5FF51B4E3FF4EB2E1FF4BAFE0FF48ACDFFF81C5E8FF3299D8FF3097D5FF2F94
        D4FF2D91D4FF2C8DD2FF1B5698FF0A2042C20000013300000009000000000000
        000000000000000000050000001A050E1963164073E32872B0FF379BD8FF53AB
        E0FF2E91D4FF2986CAFF1B5598FF0C2451D80104095A2772B1FF5ABCE5FF5ABE
        E6FF58BBE5FF54B8E4FF51B5E3FF4EB1E2FF90CDECFF349CD9FF319AD7FF3096
        D6FF2F94D5FF2E91D3FF2B8DD2FF2068AAFF0D2C5AE000000021000000000000
        00020000000D01020431113051B12566A5FD3794CFFF3DA4DEFF3DA3DDFF60B3
        E4FF3094D5FF2D8FD3FF2A8AD1FF216DB3FF11356CF92775B4FF64C7EBFF61C4
        E9FF5DC1E8FF5BBEE7FF57BBE5FF54B7E4FF9DD5EFFF349ED9FF339CD8FF3299
        D6FF3095D6FF2E93D4FF2D90D3FF2C8DD2FF133B7BFF00000030000000050000
        001A0919287020588EEB3589C2FF44ABDFFF43ABE0FF41A9E0FF40A6DFFF6DBB
        E7FF3197D7FF2E92D4FF2C8DD2FF2988D0FF2477BFFF2877B5FF6ACDEEFF66CA
        ECFF63C8EBFF61C4E9FF5EC1E8FF5ABEE7FFAADDF2FF35A1DAFF349ED9FF329B
        D8FF3198D7FF2F95D5FF2E93D4FF2D90D3FF133C7CFF00000031000000131942
        68B8317EBAFF47ABDBFF4BB5E4FF49B2E4FF48B1E3FF44AEE1FF43ACE0FF7AC3
        E9FF339BD8FF3096D6FF2E90D3FF2B8CD1FF2780C6FF297AB8FF6FD3F0FF6CD0
        EFFF6ACEEEFF66CAECFF64C7EBFF61C5EAFFB4E2F4FF43A9DEFF35A0DAFF349D
        D8FF329BD7FF3197D6FF2F95D4FF2E92D3FF143E7FFF000000300000001F2B73
        B2FF54BFE8FF53BDE7FF51BBE6FF4FB8E5FF4CB5E4FF4AB3E3FF48B1E3FF87CB
        ECFF349DDAFF3298D7FF2F94D5FF2D8FD3FF2981C7FF2A7BB9FF75D8F3FF73D6
        F1FF6FD3EFFF6CD0EFFF6ACDEDFF7FD3EFFFB3C9E2FF8BCFEDFF4FB2E2FF37A1
        DAFF339DD8FF329AD7FF3097D6FF2F94D5FF143F80FF0000002E000000202C75
        B4FF5CC4E9FF58C2E9FF56C0E8FF53BEE7FF51BBE6FF4FBAE5FF4CB6E4FF95D3
        EFFF35A1DAFF339CD9FF3197D6FF2E92D4FF2A84C8FF2B7DBAFF7ADDF4FF77DB
        F3FF75D9F2FF7CD8F2FFC1EBF9FFC8F1FCFF7FBBDEFF97D4F0FF51B7E5FF52B6
        E4FF49ADE0FF339CD9FF3299D7FF3097D6FF154081FF0000002D0000001E2D77
        B6FF60CAECFF5EC7EBFF5CC5EAFF59C3E9FF57C1E8FF54BEE7FF52BCE7FFA2DA
        F2FF37A3DCFF359FDAFF339AD8FF3095D6FF2B87C8FF2C7FBCFF7EE1F6FF7EDF
        F5FFB5ECF9FFD3F5FCFFADECFAFFA5E8FAFF7FBBDEFFA2D8F1FF51B7E5FF51B7
        E5FF51B7E5FF55B9E6FF40A6DDFF3298D7FF154282FF0000002B0000001C2F7B
        B7FF66CFEEFF63CCEDFF62CAECFF5FC8EBFF5DC6EAFF59C3E9FF57C2E8FFAFE1
        F4FF39A6DDFF37A2DBFF349DD9FF3198D7FF2D8BCBFF2C81BDFF8BD6EEFFCCED
        F8FF6FC0E4FF50B1DEFF4BAFDDFF46ACDBFF3591CAFFABDCF2FF9FD7F1FF95D3
        EFFF8ACEEDFF7FCAECFF76C6EBFF5DABDAFF1F5294FF0002072F0000001A307D
        BAFF6BD4EFFF69D2EFFF66D0EEFF65CDEDFF62CBECFF5FC9ECFF5DC7EBFFBBE7
        F6FF3AA9DFFF38A4DDFF36A0DAFF339CD9FF3092D2FF2B83C1FF2A7CBAFF6CAD
        D6FFC2E7F5FF96D3EFFF74C5EAFF74C5EAFF6FBDE5FF3491CAFF7DBBDEFF7CB9
        DEFF7AB8DEFF64A6D4FF346BA9FF0E326CE1051020680000000E00000018317F
        BCFF71D9F2FF6ED7F1FF6CD5F0FF6AD2EFFF68D1EFFF65CEEEFF63CCECFFC7EC
        F8FF3BABDFFF3AA7DDFF37A3DCFF349FDAFF3299D6FF2F92D2FF2B88CBFF287B
        BEFF2877B7FF6BAAD2FFC2E7F5FF96D3EFFF74C5EAFF42AADAFFA2E6F9FF84D1
        EEFF4689BCFF143F6FD4061322630000001A0000000A00000002000000163280
        BDFF75DDF3FF74DBF2FF71D9F2FF6FD8F2FF6CD5F1FF6BD3EFFF83D9F2FFEAF8
        FDFF86CDECFF63BCE6FF38A5DEFF37A1DBFF349CD9FF3198D7FF2F91D4FF2B8B
        D0FF2981C8FF2677BBFF2672B2FF6AA7D0FFC1E7F5FF63B7DFFF4A91C3FF1747
        78CE071524590000001400000008000000020000000000000000000000143482
        BFFF7BE0F5FF79DFF5FF76DDF3FF74DBF2FF7EDCF3FFBAEDF9FFD9F3FAFFA1D5
        EBFF78C7EBFF5CBBE6FF82CBEBFF52B0E0FF369FDBFF339BD8FF3096D6FF2D91
        D4FF2B8CD1FF2885CCFF257CC5FF184785FF1B5383CF1B507FC70818274E0000
        000D000000060000000100000000000000000000000000000000000000123585
        C0FF7FE4F7FF7CE2F6FF80E1F5FFB0EDF9FFD7F7FDFFB7E9F6FF91D1E9FF6FAC
        D0FF81CBECFF51B7E5FF51B7E5FF5FBCE7FF7BC6E9FF43A6DCFF3299D7FF2F95
        D5FF2D8FD3FF2A8AD1FF2785CDFF123775FF0000002B00000005000000030000
        0001000000000000000000000000000000000000000000000000000000103787
        C2FF83E6F8FFA4EDFAFFD1F7FDFFC5F4FDFFACEAF7FF9AD9EDFF88C6DFFF5F94
        C1FF8CD0EEFF51B7E5FF51B7E5FF51B7E5FF51B7E5FF64BFE7FF72C2E8FF3A9E
        D8FF2E92D4FF2C8DD2FF2988D0FF133977FF0000002700000000000000000000
        00000000000000000000000000000000000000000000000000000000000C3887
        C2FFB4E8F4FFB0DDECFF99CFE3FF92CBE0FF8AC2DAFF7AB2D0FF6AA1C5FF4777
        ABFF97D4F0FF51B7E5FF51B7E5FF51B7E5FF51B7E5FF51B7E5FF51B7E5FF6DC6
        EBFF64BAE6FF3294D4FF2B8BD1FF143B7AFF0000002500000000000000000000
        0000000000000000000000000000000000000000000000000000000000061C42
        5E884A8DBDF288C3DFFFA1D9EEFFA1D9EEFFA1D9EEFFA1D9EEFFA1D9EEFF7EB0
        D6FFA2D8F1FF51B7E5FF51B7E5FF51B7E5FF51B7E5FF51B7E5FF51B7E5FF51B7
        E5FF54B9E6FF71C9EDFF58B0E1FF153D7DFF0000002400000000000000000000
        0000000000000000000000000000000000000000000000000000000000010000
        0005010202101431476F3572A0DD68ADD1FF83CAE6FF86CEE9FF86CEE9FF6FB0
        D7FFABDCF2FF9FD7F1FF95D3EFFF8ACEEDFF7FCAECFF76C6EBFF6EC3E9FF66C0
        E8FF60BDE7FF5BBBE7FF58B7E3FF29649DFF0000001C00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000001000000040000000B0B1C2847275980B95BA0C9FD7FC6E3FF80C6
        E4FF6FB0D7FF69A8D2FF7EB0D6FF487DB0FF609BC8FF68A7D0FF68ADD7FF4E94
        C8FF2E6AA9FF153E7AEE0C2346A7040B164D0000000A00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000000010000000300000008040A0F261B405E924E90
        BCF679BEDEFF86CEE9FFA1D9EEFF67A6C9FF67B7DBFF4590C3FF1E528CEE1131
        54AB0612205A0000001A0000000E000000060000000100000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000002000000060101
        0212102A406D407DA7E388C2DFFF25619DF6173F67B30A1B2D63000000160000
        000D000000060000000100000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0001000000050000000C0A192849010304180000000B00000005000000010000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000100000002000000010000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000}
    end
    object o2: TMenuItem
      Action = actFiltroLoteFolhaRCPN
      Bitmap.Data = {
        36100000424D3610000000000000360000002800000020000000200000000100
        2000000000000010000000000000000000000000000000000000000000020000
        0004000000050000000600000006000000060000000600000006000000060000
        0006000000060000000600000007000000070000000700000007000000070000
        0007000000070000000700000007000000070000000600000004000000020000
        0001000000000000000000000000000000000000000000000000000000070000
        0010000000150000001700000017000000170000001800000018000000180000
        001800000018000000190000001900000019000000190000001A0000001A0000
        001A0000001B0000001B0000001B0000001A0000001700000010000000080000
        00030000000100000000000000000000000000000000000000000000000E357D
        BDFF2F72BBFF2E70BBFF2E70BBFF2E6FBAFF2D6FB9FF2E6EB9FF2D6EB8FF2C6D
        B8FF2C6CB7FF2C6BB7FF2B6BB6FF2B6AB6FF2A69B6FF2A69B6FF2B68B6FF2A68
        B5FF2A67B4FF2A67B4FF2A67B4FF2966B4FF2966B3FF09193C94000000160000
        000900000003000000010000000000000000000000000000000000000012357D
        BDFF60BAEBFF5EBAECFF5EB9EBFF5EB9ECFF5DB8EBFF5CB8EAFF5CB7EBFF5BB7
        EBFF5BB6EAFF5AB6EAFF5AB6EAFF59B5EAFF59B5EAFF58B4EAFF58B4E9FF57B4
        E9FF57B3EAFF57B3EAFF56B3E9FF55B3E9FF79CDF2FF19438EFF091A3F950000
        001600000009000000030000000100000000000000000000000000000012357E
        BDFF60BBEBFF60BBECFF5FBAECFF5EB9EBFF5EB9ECFF5DB8ECFF5DB8EBFF5DB8
        EBFF5CB8EAFF5BB7EBFF5BB6EAFF5BB6EBFF5AB5EBFF59B5EAFF58B5EAFF58B5
        EAFF58B4E9FF58B4E9FF57B4E9FF56B4E9FF7CCEF3FF2053A1FF1A4691FF0A1C
        409400000015000000090000000300000001000000000000000000000011367E
        BDFF62BCECFF62BCECFF60BBECFF60BBEBFF60BAEBFF5FBAECFF5FB9EBFF5DB9
        EBFF5DB9EBFF5CB8EBFF5CB7EAFF5CB8EAFF5BB7EBFF5AB6EBFF59B5EAFF59B6
        EAFF59B5EAFF58B5E9FF57B4E9FF58B4E9FF81D0F3FF2256A4FF2356A3FF1C49
        93FF0A1D4294000000140000000800000002000000010000000000000010367E
        BDFF63BDEDFF62BCECFF63BCECFF6AC0EEFF6FC4EFFF73C6EFFF78C8F0FF7AC9
        F0FF7CCBF1FF7BCAF1FF7AC9F1FF79C9F1FF75C6F0FF71C4EFFF6CC1EEFF67BE
        EDFF60B9EBFF5BB6EAFF59B5EAFF58B4EAFF86D2F4FF255AA6FF255AA6FF255B
        A6FF1E4C96FF0B1F4493000000130000000800000002000000010000000F367F
        BDFF6DC2EEFF7ACAF1FF84D0F3FF85D0F3FF84CFF3FF83CFF2FF82CEF2FF81CD
        F2FF80CDF2FF7FCDF2FF7FCBF1FF7DCBF1FF7CCBF1FF7BCAF1FF7AC9F1FF79C8
        F0FF78C8F0FF76C7EFFF6CC1EEFF60B9EBFF8AD4F4FF275EA9FF265EA8FF275E
        A8FF275EA9FF1F5099FF0C2146920000001200000006000000020000000D367F
        BDFF8BD4F4FF8BD3F4FF8AD3F4FF89D2F4FF88D2F4FF87D1F4FF86D0F3FF85D0
        F3FF84CFF3FF83CEF3FF82CEF2FF81CEF2FF80CDF2FF7FCCF2FF7ECCF1FF7DCB
        F1FF7CCBF1FF7BCAF1FF7AC9F0FF79C9F1FF94D9F6FF2A62ABFF2962ABFF2962
        ABFF2A62ACFF2962ABFF22549CFF0B1E42900000000C000000030000000C367F
        BDFF8FD6F5FF8ED6F5FF8DD5F4FF8DD5F4FF8BD4F4FF8BD3F4FF8AD3F4FF89D2
        F3FF88D2F3FF87D1F3FF86D0F3FF86D0F3FF85CFF3FF83CEF2FF82CEF2FF81CD
        F2FF80CDF2FF80CDF2FF7ECCF2FF7ECBF1FF9ADDF7FF2C66AEFF2C66ADFF2C66
        AEFF2C66AEFF2B66AEFF2C67AEFF21549DFF000000110000000400000009367F
        BDFFCFF1FCFFCEF1FCFFCEF1FCFFCEF1FCFFCCF0FBFFCBF0FCFFCBF0FCFFCAEF
        FBFFC9EFFBFFC8EFFBFFC7EEFBFFC6EEFBFFC4EDFBFFC3EDFBFFC2EDFAFFC1EC
        FAFFC0ECFBFFBEECFAFFBDEBFAFFBCEBFAFFCFF1FCFF407CBBFF2F6BB1FF2E6B
        B1FF2E6BB0FF2F6BB1FF2E6BB0FF22559FFF0000001200000005000000061B3F
        5E86599CD0FFA8E4F9FF3B6AAAFF16428DFF163D89FF133A88FF227CD3FF237C
        D3FF227CD5FF237CD3FF227CD5FF237FD3FF237CD5FF237FD3FF237CD3FF227C
        D3FF227CD3FF237CD5FF237FD5FF237CD3FF2F8ADAFFA8E4F9FF4280BDFF316F
        B4FF306FB3FF316FB4FF316FB3FF2257A1FF0000001100000004000000020000
        0007274F6986589BCFFFA8E4F9FF467FBCFF255BA6FF2357A3FF3A9CE0FF3A9B
        DFFF3A9ADEFF3A9ADEFF3A9ADEFF399ADEFF3A9ADEFF399ADDFF399ADDFF389A
        DDFF399ADDFF399ADDFF399ADEFF3A9CDFFF3A9DE1FF4AA7E5FFA8E4F9FF4484
        BFFF3374B7FF3373B6FF3374B6FF245AA2FF0000001000000004000000010000
        000300000007284F69855799CEFFA7E3F8FF437BBAFF5980B8FF97C6E7FF93C1
        E0FF90BEDCFF90BDDCFF90BDDCFF8FBCDBFF8FBCDAFF8FBCDAFF8FBCD9FF8FBB
        D8FF8EBBD9FF8EBBD9FF90BCDAFF94C3E3FF80BCE7FF3F9FE1FF3EA0E3FFA8E4
        F9FF4687C2FF3578B8FF3678B9FF255CA4FF0000000F00000004000000000000
        00010000000200000006284F69845597CCFFBAE5F5FF97B0CEFF838089FF9867
        59FF976659FF976657FF976557FF966457FF966356FF946355FF946354FF9463
        54FF936253FF936153FF936152FF907875FF95C4E2FF42A0DFFF42A2E3FF50AD
        E7FFA8E4F9FF498BC5FF397CBBFF275FA6FF0000000E00000003000000000000
        00000000000100000002000000085D798CA5A0BFD8FFA69A97FFC6ABA3FFFBF6
        F3FFFBF6F3FFFBF6F3FFFBF6F3FFFBF6F3FFFBF6F3FFFBF6F3FFFBF5F3FFFBF5
        F3FFFBF5F3FFFBF5F3FFFBF5F3FFA67667FF8FBBD8FF4AA3DCFF4AA7E2FF48A8
        E4FF53AFE7FFA8E4F9FF4A8EC6FF2861A8FF0000000D00000003000000000000
        0000000000000000000200000006000000125C5559C8C7ACA4FFF9F1EEFFF8ED
        E9FFF8EEE9FFF8EDE8FFFBF6F3FFFBF6F3FFFBF6F4FFFBF6F3FFFBF6F3FFFBF6
        F3FFFBF6F3FFF8ECE8FFFBF6F3FFA77869FFB0C8D0FFBDD9E2FFC7E3EDFFC3E7
        F4FFAAE3F7FFA9E4F9FFA8E4F9FF2963ABFF0000000A00000003000000000000
        00000000000000000002000000084C332C8CC8AEA6FFF9F2EEFFF8EEEAFFF8ED
        E9FFF8EEE9FFF7EDE9FFAD7F72FFAC7E70FFAC7D70FFAB7D6FFFAB7C6EFFAA7C
        6DFFAA7B6DFFF7EDE8FFFBF6F4FFA8796AFFBDA8A3FF9A6D61FF9C8580FFA6CA
        E0FF66AFDAFF61AEDBFF5EAEDBFF5BA8D7FF0101010800000002000000000000
        000000000000000000020000000AB2887AFFFBF6F4FFD6BEB5FFBE988DFFDBC4
        BCFFF8EEEAFFF8EEEAFFF3E2DAFFF3E2DCFFF1E2DAFFF3E2DCFFF3E2DAFFF3E2
        DAFFF3E2DAFFF7EDE9FFFBF6F3FFAA7A6DFFE7E3E1FFF6F1EFFFAE8276FF0303
        0324020202140101010D0000000A000000070000000300000001000000000000
        000000000000000000020000000AB48A7DFFFFFFFFFFAE8275FF3427236DB790
        82FFF8EFEAFFF8EEEAFFB08475FFAF8275FFAE8274FFAE8173FFAD8072FFAD80
        71FFAC7F70FFF8EEE9FFFBF6F4FFAB7D6EFFE6DCD9FFF6F2EFFFB08578FF0707
        07300505051F030303130101010A000000030000000100000000000000000000
        0000000000000000000200000008B58D80FFFFFFFFFFCDB0A6FFA47465FFCEB1
        A8FFF8EFEBFFF8EFEBFFF3E4DEFFF3E4DEFFF3E4DEFFF5E4DCFFF3E2DCFFF3E4
        DCFFF3E4DCFFF8EFEAFFFBF6F4FFAC7E71FFE7DFDAFFF7F3F1FFB18679FFC4B1
        ABFFA57C70FF795C53C103030310010101040000000100000000000000000000
        00000000000000000001000000055E4C4785DECBC5FFFFFFFFFFF8F0ECFFF8F0
        EBFFF8F0ECFFF9F0EBFFB3887AFFB3877AFFB28778FFB18578FFB18576FFAF84
        76FFAF8375FFF8EFEAFFFBF7F4FFAE8172FFE8E0DCFFF7F3F0FFB2877BFFE9E6
        E4FFF7F3F1FFB68F83FF04040416010101060000000100000000000000000000
        0000000000000000000000000002000000065F4E4885DDC8C2FFFFFFFFFFF9F1
        ECFFF8F0EDFFF8F0ECFFF8F0ECFFF9F0ECFFF9F0ECFFF9F0EBFFF9F0ECFFF8F0
        ECFFF8EFEBFFF8EFEBFFFCF7F5FFAF8274FFEBE3DEFFF7F3F1FFB38A7CFFE9E0
        DCFFF7F4F1FFB89185FF04040417010101060000000100000000000000000000
        000000000000000000000000000100000002000000075F4E4988DECAC3FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFB18577FFEEE7E2FFF8F4F2FFB48B7FFFEAE3
        DEFFF7F5F3FFB99286FF04040416010101060000000100000000000000000000
        00000000000000000000000000000000000000000003000000099C8A82C3D4B9
        B0FFD3B8AEFFD2B7ADFFD1B5ABFFD0B4AAFFD0B3A9FFCFB2A7FFCEB1A7FFCDAF
        A5FFCCAEA3FFCCADA2FFCAACA1FFD2B9B0FFF2EAE6FFF9F6F3FFB68D80FFEBE3
        E0FFF7F5F2FFBA9388FF04040414010101050000000100000000000000000000
        000000000000000000000000000000000000000000000000000301010109695D
        5987E3D6D2FFF9F9F9FFF4ECE8FFF2EAE8FFF2EAE7FFF2EAE7FFF2E9E7FFF2E9
        E7FFF2E9E6FFF1E8E6FFF1E9E7FFF3EBE7FFF6EEEAFFFBF7F5FFB78E82FFEEE5
        E1FFF7F5F3FFBA9589FF04040413010101050000000000000000000000000000
        0000000000000000000000000000000000000000000000000001000000030101
        01086B5F5B88E8DAD5FFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFDFDFDFFFDFD
        FDFFFDFDFDFFFDFDFDFFFDFDFDFFFEFEFEFFFEFEFEFFFFFFFFFFB99184FFF0E9
        E5FFF8F5F3FFBB968CFF03030311010101040000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000301010109A18F89C4D8C0B8FFD7BFB6FFD6BEB5FFD6BCB3FFD5BBB2FFD5BA
        B1FFD4BAB0FFD3B9B0FFD2B7AEFFD1B6ACFFD1B5ABFFCFB4AAFFD7C0B8FFF4EC
        E8FFF9F7F4FFBD988CFF03030310010101040000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000003010101096C615D86E6DAD6FFFAFAFAFFF5EEEBFFF4ECEBFFF4EC
        EAFFF4ECEAFFF3EBE9FFF3EBE9FFF3EBE8FFF2EAE8FFF3EBE9FFF5EDEAFFF7EF
        ECFFFBF8F6FFBE998EFF0303030E010101040000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000100000003010101066D625F84EADED9FFFEFEFEFFFEFEFEFFFEFE
        FEFFFEFEFEFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFEFEFEFFFEFE
        FEFFFFFFFFFFC09C90FF0202020B010101030000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000002010101046E635F83DCC6BFFFDBC5BDFFDAC4
        BCFFDAC3BAFFD9C2BAFFD9C1B9FFD8C1B8FFD7C0B8FFD6BEB6FFD6BDB4FFD6BC
        B3FFD4BBB2FF9E8B84C101010107000000020000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000010101010301010104010101050101
        0106010101060101010701010107010101070202020802020208020202080202
        0209020202080101010601010103000000010000000000000000}
    end
  end
  object ppmRelatoriosFolhas: TJvPopupMenu
    Images = dmGerencial.Im32
    Cursor = crHandPoint
    DisabledImages = dmGerencial.Im32Dis
    ImageMargin.Left = 0
    ImageMargin.Top = 0
    ImageMargin.Right = 0
    ImageMargin.Bottom = 0
    ImageSize.Height = 0
    ImageSize.Width = 0
    Left = 600
    Top = 185
    object L2: TMenuItem
      Action = actRelFlsSegLoteFolhasSeguranca
    end
    object L3: TMenuItem
      Action = actRelFlsSegLoteFolhasSegurancaRCPN
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object MenuItem7: TMenuItem
      Action = actRelFlsSegFolhasSeguranca
    end
    object MenuItem8: TMenuItem
      Action = actRelFlsSegFolhasSegurancaRCPN
    end
  end
  object ppmRelatoriosEtiquetas: TJvPopupMenu
    Images = dmGerencial.Im32
    Cursor = crHandPoint
    DisabledImages = dmGerencial.Im32Dis
    ImageMargin.Left = 0
    ImageMargin.Top = 0
    ImageMargin.Right = 0
    ImageMargin.Bottom = 0
    ImageSize.Height = 0
    ImageSize.Width = 0
    Left = 720
    Top = 137
    object MenuItem6: TMenuItem
      Action = actRelEtqLoteEtiquetas
    end
    object MenuItem13: TMenuItem
      Action = actRelEtqEtiquetas
    end
  end
  object ppmDespesa: TJvPopupMenu
    Images = dmGerencial.Im32
    Cursor = crHandPoint
    DisabledImages = dmGerencial.Im32Dis
    ImageMargin.Left = 0
    ImageMargin.Top = 0
    ImageMargin.Right = 0
    ImageMargin.Bottom = 0
    ImageSize.Height = 0
    ImageSize.Width = 0
    Left = 272
    Top = 196
    object L4: TMenuItem
      Action = actCadDespesa
    end
    object ppmiRepasseReceita: TMenuItem
      Caption = 'Repasse de Receita'
      ImageIndex = 56
      object MenuItem3: TMenuItem
        Action = actCadBaixaUmFlutuante
      end
      object MenuItem4: TMenuItem
        Action = actCadBaixaGpFlutuantes
      end
    end
  end
end
