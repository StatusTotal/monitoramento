{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UDMOutrasDespesasColaborador.pas
  Descricao:   Data Module de Outras Despesas de Colaborador
  Author   :   Cristina
  Date:        14-jul-2016
  Last Update: 01-dez-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UDMOutrasDespesasColaborador;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Datasnap.DBClient,
  Datasnap.Provider, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TdmOutrasDespesasColaborador = class(TDataModule)
    qryFuncionarios: TFDQuery;
    dsFuncionarios: TDataSource;
    qryOutraDespFunc: TFDQuery;
    dspOutraDespFunc: TDataSetProvider;
    cdsOutraDespFunc: TClientDataSet;
    dsOutraDespFunc: TDataSource;
    dsTipoODespF: TDataSource;
    qryTipoODespF: TFDQuery;
    cdsOutraDespFuncID_OUTRADESPESA_FUNC: TIntegerField;
    cdsOutraDespFuncID_FUNCIONARIO_FK: TIntegerField;
    cdsOutraDespFuncID_TIPO_OUTRADESP_FUNC_FK: TIntegerField;
    cdsOutraDespFuncVR_OUTRADESPESA_FUNC: TBCDField;
    cdsOutraDespFuncDESCR_OUTRADESPESA_FUNC: TStringField;
    cdsOutraDespFuncCAD_ID_USUARIO: TIntegerField;
    cdsOutraDespFuncDATA_QUITACAO: TDateField;
    cdsOutraDespFuncQUIT_ID_USUARIO: TIntegerField;
    cdsOutraDespFuncFLG_CANCELADO: TStringField;
    cdsOutraDespFuncDATA_CANCELAMENTO: TDateField;
    cdsOutraDespFuncCANCEL_ID_USUARIO: TIntegerField;
    cdsOutraDespFuncID_FECHAMENTO_SALARIO_FK: TIntegerField;
    cdsOutraDespFuncVR_ODESP_ANTERIOR: TCurrencyField;
    cdsOutraDespFuncDATA_OUTRADESPESA_FUNC: TDateField;
    cdsOutraDespFuncDATA_CADASTRO: TSQLTimeStampField;
    cdsOutraDespFuncFLG_MODALIDADE: TStringField;
    cdsOutraDespFuncFLG_RECORRENTE: TStringField;
    cdsOutraDespFuncFLG_REPLICADO: TStringField;
    procedure cdsOutraDespFuncVR_OUTRADESPESA_FUNCGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmOutrasDespesasColaborador: TdmOutrasDespesasColaborador;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{$R *.dfm}

procedure TdmOutrasDespesasColaborador.cdsOutraDespFuncVR_OUTRADESPESA_FUNCGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

end.
