{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UDMFolhaSeguranca.pas
  Descricao:   Data Module de Folhas de Seguranca
  Author   :   Cristina
  Date:        25-ago-2017
  Last Update: 28-ago-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UDMFolhaSeguranca;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TdmFolhaSeguranca = class(TDataModule)
    qryLoteFls: TFDQuery;
    qryFolha: TFDQuery;
    qrySistema: TFDQuery;
    qryNatAto: TFDQuery;
    dsSistema: TDataSource;
    dsNatAto: TDataSource;
    qryFuncionarios: TFDQuery;
    dsFuncionarios: TDataSource;
    dspLoteFls: TDataSetProvider;
    dspFolha: TDataSetProvider;
    cdsLoteFls: TClientDataSet;
    cdsFolha: TClientDataSet;
    dsLoteFls: TDataSource;
    dsFolha: TDataSource;
    cdsLoteFlsID_LOTEFOLHA: TIntegerField;
    cdsLoteFlsNUM_LOTE: TStringField;
    cdsLoteFlsLETRA: TStringField;
    cdsLoteFlsNUM_INICIAL: TStringField;
    cdsLoteFlsNUM_FINAL: TStringField;
    cdsLoteFlsQTD_FOLHAS: TIntegerField;
    cdsLoteFlsMASCARA: TStringField;
    cdsLoteFlsFLG_RCPN: TStringField;
    cdsLoteFlsFLG_SITUACAO: TStringField;
    cdsLoteFlsDATA_CADASTRO: TSQLTimeStampField;
    cdsLoteFlsCAD_ID_USUARIO: TIntegerField;
    cdsLoteFlsDATA_COMPRA: TDateField;
    cdsLoteFlsDATA_CANCELAMENTO: TDateField;
    cdsLoteFlsCANCEL_ID_USUARIO: TIntegerField;
    cdsFolhaID_FOLHASEGURANCA: TIntegerField;
    cdsFolhaID_LOTEFOLHA_FK: TIntegerField;
    cdsFolhaORDEM: TIntegerField;
    cdsFolhaNUM_FOLHA: TStringField;
    cdsFolhaDATA_UTILIZACAO: TDateField;
    cdsFolhaID_SISTEMA_FK: TIntegerField;
    cdsFolhaFLG_RCPN: TStringField;
    cdsFolhaFLG_SITUACAO: TStringField;
    cdsFolhaID_FUNCIONARIO_FK: TIntegerField;
    cdsFolhaID_NATUREZAATO_FK: TIntegerField;
    cdsFolhaOBS_FOLHASEGURANCA: TStringField;
    cdsFolhaDATA_CADASTRO: TSQLTimeStampField;
    cdsFolhaCAD_ID_USUARIO: TIntegerField;
    cdsFolhaDATA_CANCELAMENTO: TDateField;
    cdsFolhaCANCEL_ID_USUARIO: TIntegerField;
    qryFlxAux: TFDQuery;
    dsFlxAux: TDataSource;
    qryFlxAuxID_FOLHASEGURANCA: TIntegerField;
    qryFlxAuxID_LOTEFOLHA_FK: TIntegerField;
    qryFlxAuxORDEM: TIntegerField;
    qryFlxAuxLETRA: TStringField;
    qryFlxAuxNUMERO: TIntegerField;
    qryFlxAuxALEATORIO: TStringField;
    qryFlxAuxNUM_FOLHA: TStringField;
    qryFlxAuxDATA_UTILIZACAO: TDateField;
    qryFlxAuxID_SISTEMA_FK: TIntegerField;
    qryFlxAuxFLG_RCPN: TStringField;
    qryFlxAuxFLG_SITUACAO: TStringField;
    qryFlxAuxID_FUNCIONARIO_FK: TIntegerField;
    qryFlxAuxID_NATUREZAATO_FK: TIntegerField;
    qryFlxAuxOBS_FOLHASEGURANCA: TStringField;
    qryFlxAuxDATA_CADASTRO: TSQLTimeStampField;
    qryFlxAuxCAD_ID_USUARIO: TIntegerField;
    qryFlxAuxDATA_CANCELAMENTO: TDateField;
    qryFlxAuxCANCEL_ID_USUARIO: TIntegerField;
    qryNatAtoID_NATUREZAATO: TIntegerField;
    qryNatAtoDESCR_NATUREZAATO: TStringField;
    qryNatAtoID_SISTEMA_FK: TIntegerField;
    qryNatureza: TFDQuery;
    dspNatureza: TDataSetProvider;
    cdsNatureza: TClientDataSet;
    cdsNaturezaID_NATUREZAATO: TIntegerField;
    cdsNaturezaDESCR_NATUREZAATO: TStringField;
    cdsNaturezaID_SISTEMA_FK: TIntegerField;
    dsNatureza: TDataSource;
    qrySistN: TFDQuery;
    dsSistN: TDataSource;
    cdsFolhaLETRA: TStringField;
    cdsFolhaNUMERO: TIntegerField;
    cdsFolhaALEATORIO: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmFolhaSeguranca: TdmFolhaSeguranca;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{$R *.dfm}

end.
