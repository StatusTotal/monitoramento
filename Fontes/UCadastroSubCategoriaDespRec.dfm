inherited FCadastroSubCategoriaDespRec: TFCadastroSubCategoriaDespRec
  Caption = 'Cadastro de Subcategoria de Lan'#231'amento'
  ClientHeight = 209
  ClientWidth = 483
  ExplicitWidth = 489
  ExplicitHeight = 238
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 477
    Height = 203
    ExplicitWidth = 477
    ExplicitHeight = 203
    inherited pnlDados: TPanel
      Width = 363
      Height = 197
      ExplicitWidth = 363
      ExplicitHeight = 197
      object lblNomeSubCategoria: TLabel [0]
        Left = 0
        Top = 93
        Width = 124
        Height = 14
        Caption = 'Nome da Subcategoria'
      end
      object lblNomeCategoria: TLabel [1]
        Left = 0
        Top = 49
        Width = 104
        Height = 14
        Caption = 'Nome da Categoria'
      end
      object lblNatureza: TLabel [2]
        Left = 0
        Top = 5
        Width = 48
        Height = 14
        Caption = 'Natureza'
      end
      inherited pnlMsgErro: TPanel
        Top = 136
        Width = 363
        TabOrder = 4
        ExplicitTop = 136
        ExplicitWidth = 363
        inherited lblMensagemErro: TLabel
          Width = 363
        end
        inherited lbMensagemErro: TListBox
          Width = 363
          OnDblClick = lbMensagemErroDblClick
          ExplicitWidth = 363
        end
      end
      object edtNomeSubCategoria: TDBEdit
        Left = 0
        Top = 108
        Width = 361
        Height = 22
        CharCase = ecUpperCase
        Color = 16114127
        DataField = 'DESCR_SUBCATEGORIA_DESPREC'
        DataSource = dsCadastro
        TabOrder = 3
        OnKeyPress = edtNomeSubCategoriaKeyPress
      end
      object rgTipo: TDBRadioGroup
        Left = 208
        Top = 3
        Width = 153
        Height = 40
        Caption = 'Tipo'
        Columns = 2
        DataField = 'TIPO'
        DataSource = dsCadastro
        Enabled = False
        Items.Strings = (
          'Despesa'
          'Receita')
        TabOrder = 1
      end
      object lcbNomeCategoria: TDBLookupComboBox
        Left = 0
        Top = 65
        Width = 361
        Height = 22
        Color = 16114127
        DataField = 'ID_CATEGORIA_DESPREC_FK'
        DataSource = dsCadastro
        KeyField = 'ID_CATEGORIA_DESPREC'
        ListField = 'DESCR_CATEGORIA_DESPREC'
        ListSource = dsCategoria
        TabOrder = 2
        OnClick = lcbNomeCategoriaClick
        OnExit = lcbNomeCategoriaExit
        OnKeyPress = FormKeyPress
      end
      object edtNatureza: TEdit
        Left = 0
        Top = 21
        Width = 202
        Height = 22
        Color = 16114127
        TabOrder = 0
      end
    end
    inherited pnlMenu: TPanel
      Height = 197
      ExplicitHeight = 197
      inherited btnDadosPrincipais: TJvTransparentButton
        Visible = False
      end
      inherited btnOk: TJvTransparentButton
        Top = 107
        ExplicitTop = 59
      end
      inherited btnCancelar: TJvTransparentButton
        Top = 152
        ExplicitTop = 104
      end
    end
  end
  inherited dsCadastro: TDataSource
    DataSet = cdsSubCategoria
    Left = 422
    Top = 150
  end
  object qrySubCategoria: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM SUBCATEGORIA_DESPREC'
      ' WHERE ID_SUBCATEGORIA_DESPREC = :ID_SUBCATEGORIA_DESPREC')
    Left = 146
    Top = 153
    ParamData = <
      item
        Position = 1
        Name = 'ID_SUBCATEGORIA_DESPREC'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspSubCategoria: TDataSetProvider
    DataSet = qrySubCategoria
    Left = 244
    Top = 152
  end
  object cdsSubCategoria: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_SUBCATEGORIA_DESPREC'
        ParamType = ptInput
      end>
    ProviderName = 'dspSubCategoria'
    Left = 340
    Top = 152
    object cdsSubCategoriaID_SUBCATEGORIA_DESPREC: TIntegerField
      FieldName = 'ID_SUBCATEGORIA_DESPREC'
      Required = True
    end
    object cdsSubCategoriaID_CATEGORIA_DESPREC_FK: TIntegerField
      FieldName = 'ID_CATEGORIA_DESPREC_FK'
    end
    object cdsSubCategoriaTIPO: TStringField
      FieldName = 'TIPO'
      FixedChar = True
      Size = 1
    end
    object cdsSubCategoriaDESCR_SUBCATEGORIA_DESPREC: TStringField
      FieldName = 'DESCR_SUBCATEGORIA_DESPREC'
      Size = 250
    end
  end
  object dsCategoria: TDataSource
    DataSet = qryCategoria
    Left = 42
    Top = 89
  end
  object qryCategoria: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT C.*,'
      '       N.DESCR_NATUREZA'
      '  FROM CATEGORIA_DESPREC C'
      '  LEFT JOIN NATUREZA N'
      '    ON C.ID_NATUREZA_FK = N.ID_NATUREZA'
      ' WHERE C.TIPO = :TIPO'
      'ORDER BY C.ID_CATEGORIA_DESPREC')
    Left = 42
    Top = 38
    ParamData = <
      item
        Name = 'TIPO'
        DataType = ftString
        ParamType = ptInput
      end>
  end
end
