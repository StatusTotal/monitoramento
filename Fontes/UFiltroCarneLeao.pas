{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroCarneLeao.pas
  Descricao:   Filtro Arquivos de Carne Leao
  Author   :   Cristina
  Date:        25-nov-2016
  Last Update: 11-out-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroCarneLeao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.StdCtrls, System.DateUtils, System.IniFiles, System.StrUtils;

type
  TFFiltroCarneLeao = class(TFFiltroSimplesPadrao)
    qryGridPaiPadraoID: TIntegerField;
    qryGridPaiPadraoANO_DECLARACAO: TIntegerField;
    qryGridPaiPadraoTIPO_ENVIO: TStringField;
    qryGridPaiPadraoSISTEMA_GERADOR: TStringField;
    qryGridPaiPadraoCPF_DECLARANTE: TStringField;
    qryGridPaiPadraoNOME_DECLARANTE: TStringField;
    lblAno: TLabel;
    edtAno: TEdit;
    qryGridPaiPadraoDATA_GERACAO: TSQLTimeStampField;
    qryGridPaiPadraoGER_ID_USUARIO: TIntegerField;
    qryGridPaiPadraoFLG_CANCELADO: TStringField;
    qryGridPaiPadraoDATA_CANCELAMENTO: TDateField;
    qryGridPaiPadraoCANCEL_ID_USUARIO: TIntegerField;
    qryGridPaiPadraoARQUIVO: TStringField;
    procedure btnEditarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure dbgGridPaiPadraoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnExportarClick(Sender: TObject);
    procedure edtAnoKeyPress(Sender: TObject; var Key: Char);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroCarneLeao: TFFiltroCarneLeao;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMCarneLeao,
  UGeracaoCarneLeao;

{ TFFiltroSimplesPadrao1 }

procedure TFFiltroCarneLeao.btnEditarClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao     := E;
  vgIdConsulta   := qryGridPaiPadrao.FieldByName('ID').AsInteger;
  vgOrigemFiltro := True;

  try
    Application.CreateForm(TdmCarneLeao, dmCarneLeao);
    Application.CreateForm(TFGeracaoCarneLeao, FGeracaoCarneLeao);

    FGeracaoCarneLeao.TpEnvio := 'E';  //Exportacao

    FGeracaoCarneLeao.ShowModal;
  finally
    FGeracaoCarneLeao.Free;
    vgOrigemCadastro := False;
    FreeAndNil(dmCarneLeao);
  end;

  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroCarneLeao.btnExcluirClick(Sender: TObject);
var
  qryExclusao: TFDQuery;
begin
  inherited;

  if lPodeExcluir then
  begin
    qryExclusao := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      with qryExclusao, SQL do
      begin
        Close;
        Clear;
        Text := 'UPDATE CARNELEAO_IDENTIFICACAO ' +
                '   SET FLG_CANCELADO     = :FLG_CANCELADO, ' +
                '       DATA_CANCELAMENTO = :DATA_CANCELAMENTO, ' +
                '       CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO ' +
                ' WHERE ID_CARNELEAO_IDENTIFICACAO = :ID_CARNELEAO_IDENTIFICACAO';
        Params.ParamByName('FLG_CANCELADO').Value              := 'S';
        Params.ParamByName('DATA_CANCELAMENTO').Value          := Date;
        Params.ParamByName('CANCEL_ID_USUARIO').Value          := vgUsu_Id;
        Params.ParamByName('ID_CARNELEAO_IDENTIFICACAO').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      GravarLog;
      Application.MessageBox('Carn�-Le�o cancelado com sucesso!', 'Aviso', MB_OK)
    except
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Application.MessageBox('Erro no cancelamento do Carn�-Le�o.', 'Erro', MB_OK + MB_ICONERROR)
    end;

    FreeAndNil(qryExclusao);

    btnFiltrar.Click;
  end;
end;

procedure TFFiltroCarneLeao.btnExportarClick(Sender: TObject);
var
  sMsg: String;
  TpMsg: Integer;
begin
  sMsg    := '';
  TpMsg   := 0;

  PegarPosicaoGrid;

  Application.CreateForm(TdmCarneLeao, dmCarneLeao);

  if dmCarneLeao.GerarArquivoExportacaoCarneLeao(qryGridPaiPadrao.FieldByName('ID').AsInteger,
                                                 sMsg,
                                                 TpMsg,
                                                 False) then
    Application.MessageBox(PChar('Arquivo de Carn� Le�o ' +
                                 qryGridPaiPadrao.FieldByName('ANO_DECLARACAO').AsString +
                                 ' gerado com sucesso!'),
                           'Sucesso!',
                           MB_OK)
  else
  begin
    if TpMsg = 1 then  //Aviso
    begin
      Application.MessageBox(PChar(sMsg),
                             'Aviso',
                             MB_OK + MB_ICONWARNING);
    end
    else if TpMsg = 2 then  //Aviso para regerar arquivo
    begin
      if Application.MessageBox(PChar(sMsg),
                                'Aviso',
                                MB_YESNO + MB_ICONWARNING) = ID_YES then
      begin
        if dmCarneLeao.GerarArquivoExportacaoCarneLeao(qryGridPaiPadrao.FieldByName('ID').AsInteger,
                                                       sMsg,
                                                       TpMsg,
                                                       True) then
          Application.MessageBox(PChar('Arquivo de Carn� Le�o ' +
                                 qryGridPaiPadrao.FieldByName('ANO_DECLARACAO').AsString +
                                 ' gerado com sucesso!'),
                           'Sucesso!',
                           MB_OK)
        else
          Application.MessageBox('Erro ao sobrescrever o arquivo.',
                                 'Erro',
                                  MB_OK + MB_ICONERROR);
      end;
    end
    else if TpMsg = 3 then  //Erro
    begin
      Application.MessageBox(PChar(sMsg),
                             'Erro',
                             MB_OK + MB_ICONERROR);
    end;
  end;

  FreeAndNil(dmCarneLeao);

  GravarLog;

  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroCarneLeao.btnFiltrarClick(Sender: TObject);
begin
  qryGridPaiPadrao.Close;

  if Trim(edtAno.Text) = '' then
  begin
    qryGridPaiPadrao.Params.ParamByName('ANO1').Value := 0;
    qryGridPaiPadrao.Params.ParamByName('ANO2').Value := 0;
  end
  else
  begin
    qryGridPaiPadrao.Params.ParamByName('ANO1').Value := Trim(edtAno.Text);
    qryGridPaiPadrao.Params.ParamByName('ANO2').Value := Trim(edtAno.Text);
  end;

  qryGridPaiPadrao.Open;

  inherited;
end;

procedure TFFiltroCarneLeao.btnIncluirClick(Sender: TObject);
begin
  inherited;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := True;

  try
    Application.CreateForm(TdmCarneLeao, dmCarneLeao);
    Application.CreateForm(TFGeracaoCarneLeao, FGeracaoCarneLeao);

    FGeracaoCarneLeao.TpEnvio := 'E';   //Exportacao

    FGeracaoCarneLeao.ShowModal;
  finally
    FGeracaoCarneLeao.Free;
    vgOrigemCadastro := False;
    FreeAndNil(dmCarneLeao);
  end;

  btnFiltrar.Click;
end;

procedure TFFiltroCarneLeao.btnLimparClick(Sender: TObject);
begin
  edtAno.Text := IntToStr(YearOf(Date));

  inherited;

  if edtAno.CanFocus then
    edtAno.SetFocus;
end;

procedure TFFiltroCarneLeao.dbgGridPaiPadraoDblClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao   := C;
  vgIdConsulta := qryGridPaiPadrao.FieldByName('ID').AsInteger;

  Application.CreateForm(TdmCarneLeao, dmCarneLeao);
  dmGerencial.CriarForm(TFGeracaoCarneLeao, FGeracaoCarneLeao);
  FreeAndNil(dmCarneLeao);

  DefinirPosicaoGrid;
end;

procedure TFFiltroCarneLeao.dbgGridPaiPadraoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Grid: TDBGrid;
begin
  inherited;

  Grid := Sender as TDBGrid;

  if Grid.DataSource.DataSet.FieldByName('FLG_CANCELADO').AsString = 'S' then
  begin
    Grid.Canvas.Font.Color := clGray;
    Grid.Canvas.FillRect(Rect);
    Grid.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TFFiltroCarneLeao.edtAnoKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroCarneLeao.FormCreate(Sender: TObject);
begin
  inherited;

  BS.VerificarExistenciaPastasSistema;

  edtAno.Text := IntToStr(YearOf(Date));
end;

procedure TFFiltroCarneLeao.FormShow(Sender: TObject);
begin
  inherited;

  btnImprimir.Visible := False;

  btnFiltrar.Click;

  ShowScrollBar(dbgGridPaiPadrao.Handle, SB_HORZ, False);

  if edtAno.CanFocus then
    edtAno.SetFocus;
end;

procedure TFFiltroCarneLeao.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    X:  //EXCLUSAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo do cancelamento desse Carn�-Le�o:', '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'CARNELEAO_IDENTIFICACAO');
    end;
    P:  //IMPRESSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente impress�o de Lista de Carn�s-Le�o',
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'CARNELEAO_IDENTIFICACAO');
    end;
    T:  //EXPORTACAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente exporta��o de arquivo de Carn�-Le�o',
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'CARNELEAO_IDENTIFICACAO');
    end;
  end;
end;

function TFFiltroCarneLeao.PodeExcluir(var Msg: String): Boolean;
begin
  inherited;

  Result := True;

  if qryGridPaiPadrao.FieldByName('FLG_CANCELADO').AsString = 'S' then
    Msg := 'N�o � poss�vel excluir o Carn�-Le�o informado, pois o mesmo j� foi cancelado no Sistema.'
  else if not qryGridPaiPadrao.FieldByName('DATA_GERACAO').IsNull then
    Msg := 'N�o � poss�vel excluir o Carn�-Le�o informado, pois o Arquivo de exporta��o j� foi gerado pelo Sistema.';

  Result := (qryGridPaiPadrao.FieldByName('DATA_GERACAO').IsNull) and
            (qryGridPaiPadrao.FieldByName('FLG_CANCELADO').AsString = 'N') and
            (qryGridPaiPadrao.RecordCount > 0);
end;

procedure TFFiltroCarneLeao.VerificarPermissoes;
begin
  inherited;

  btnIncluir.Enabled  := vgPrm_IncCLeao;
  btnEditar.Enabled   := vgPrm_EdCLeao and (qryGridPaiPadrao.RecordCount > 0);
  btnExcluir.Enabled  := vgPrm_ExcCLeao and (qryGridPaiPadrao.RecordCount > 0);
  btnImprimir.Enabled := vgPrm_ImpCLeao and (qryGridPaiPadrao.RecordCount > 0);
end;

end.
