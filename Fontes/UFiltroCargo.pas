{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroCargo.pas
  Descricao:   Filtro de cargos de Colaboradores
  Author   :   Cristina
  Date:        28-ago-2017
  Last Update: 08-nov-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroCargo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.StdCtrls;

type
  TFFiltroCargo = class(TFFiltroSimplesPadrao)
    lblDescrCargo: TLabel;
    edtDescrCargo: TEdit;
    qryGridPaiPadraoID: TIntegerField;
    qryGridPaiPadraoDESCR_CARGO: TStringField;
    qryGridPaiPadraoCOD_CARGO: TIntegerField;
    procedure btnEditarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure edtDescrCargoKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroCargo: TFFiltroCargo;

  IdCargo: Integer;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UCadastroCargo;

{ TFFiltroCargo }

procedure TFFiltroCargo.btnEditarClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao     := E;
  vgIdConsulta   := qryGridPaiPadrao.FieldByName('ID').AsInteger;
  vgOrigemFiltro := True;

  dmGerencial.CriarForm(TFCadastroCargo, FCadastroCargo);

  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroCargo.btnExcluirClick(Sender: TObject);
var
  qryExclusaoS, qryExclusaoG: TFDQuery;
begin
  inherited;

  if lPodeExcluir then
  begin
    qryExclusaoG := dmGerencial.CriarFDQuery(nil, vgConGER);
    qryExclusaoS := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    try
      if vgConGER.Connected then
        vgConGER.StartTransaction;

      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      //Gerencial
      with qryExclusaoG, SQL do
      begin
        Close;
        Clear;
        Text := 'DELETE FROM CARGO WHERE ID_CARGO = :ID_CARGO';
        Params.ParamByName('ID_CARGO').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;
      end;

      //Monitoramento
      with qryExclusaoS, SQL do
      begin
        Close;
        Clear;
        Text := 'DELETE FROM CARGO WHERE ID_CARGO = :ID_CARGO';
        Params.ParamByName('ID_CARGO').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;
      end;

      if vgConGER.InTransaction then
        vgConGER.Commit;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      GravarLog;
      Application.MessageBox('Cargo exclu�do com sucesso!', 'Aviso', MB_OK)
    except
      if vgConGER.InTransaction then
        vgConGER.Rollback;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Application.MessageBox('Erro na exclus�o do Cargo.', 'Erro', MB_OK + MB_ICONERROR)
    end;

    FreeAndNil(qryExclusaoG);
    FreeAndNil(qryExclusaoS);

    btnFiltrar.Click;
  end;
end;

procedure TFFiltroCargo.btnFiltrarClick(Sender: TObject);
begin
  inherited;

  with qryGridPaiPadrao, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_CARGO AS ID, ' +
            '       DESCR_CARGO, ' +
            '       COD_CARGO ' +
            '  FROM CARGO ';

    if Trim(edtDescrCargo.Text) <> '' then
    begin
      Add(' WHERE CAST(UPPER(DESCR_CARGO) AS VARCHAR(250)) LIKE CAST(:DESCR_CARGO AS VARCHAR(250))');
      Params.ParamByName('DESCR_CARGO').Value := '%' + Trim(UpperCase(edtDescrCargo.Text)) + '%';
    end;

    Add('ORDER BY DESCR_CARGO');
    Open;
  end;
end;

procedure TFFiltroCargo.btnIncluirClick(Sender: TObject);
begin
  inherited;

  vgOperacao     := I;
  vgIdConsulta   := 0;
  vgOrigemFiltro := True;

  dmGerencial.CriarForm(TFCadastroCargo, FCadastroCargo);

  btnFiltrar.Click;
end;

procedure TFFiltroCargo.btnLimparClick(Sender: TObject);
begin
  inherited;

  edtDescrCargo.Clear;

  inherited;

  if edtDescrCargo.CanFocus then
    edtDescrCargo.SetFocus;
end;

procedure TFFiltroCargo.dbgGridPaiPadraoDblClick(Sender: TObject);
var
  sTexto: String;
begin
  inherited;

  if vgOrigemCadastro then
  begin
    if qryGridPaiPadrao.RecordCount = 0 then
      IdCargo := 0
    else
    begin
      IdCargo := qryGridPaiPadrao.FieldByName('ID').AsInteger;

      sTexto := 'Confirma a sele��o de ' + qryGridPaiPadrao.FieldByName('DESCR_CARGO').AsString + '?';

      if Application.MessageBox(PChar(sTexto), 'Confirma��o', MB_YESNO + MB_ICONQUESTION) = ID_YES then
        Self.Close
      else
        IdCargo := 0;
    end;
  end
  else
  begin
    PegarPosicaoGrid;

    vgOperacao     := C;
    vgIdConsulta   := qryGridPaiPadrao.FieldByName('ID').AsInteger;
    vgOrigemFiltro := True;

    dmGerencial.CriarForm(TFCadastroCargo, FCadastroCargo);

    DefinirPosicaoGrid;
  end;
end;

procedure TFFiltroCargo.edtDescrCargoKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroCargo.FormCreate(Sender: TObject);
begin
  inherited;

  IdCargo := 0;
end;

procedure TFFiltroCargo.FormShow(Sender: TObject);
begin
  inherited;

  with qryGridPaiPadrao, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_CARGO AS ID, ' +
            '       DESCR_CARGO, ' +
            '       COD_CARGO ' +
            '  FROM CARGO ' +
            ' ORDER BY DESCR_CARGO';
    Open;
  end;

  ShowScrollBar(dbgGridPaiPadrao.Handle, SB_HORZ, False);

  inherited;

  btnImprimir.Visible := False;

  if edtDescrCargo.CanFocus then
    edtDescrCargo.SetFocus;
end;

procedure TFFiltroCargo.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  if vgOperacao = X then  //EXCLUSAO
  begin
    repeat
      sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da exclus�o desse Cargo:', '');
    until sObservacao <> '';

    BS.GravarUsuarioLog(2, '', sObservacao,
                        qryGridPaiPadrao.FieldByName('ID').AsInteger, 'CARGO');
  end;
end;

function TFFiltroCargo.PodeExcluir(var Msg: String): Boolean;
var
  QryVerif: TFDQuery;
begin
  inherited;

  Result := True;

  QryVerif := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryVerif, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT * FROM FUNCIONARIO WHERE ID_CARGO_FK = :ID_CARGO';
    Params.ParamByName('ID_CARGO').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
    Open;

    if QryVerif.RecordCount > 0 then
      Msg := 'N�o � poss�vel excluir o Cargo informado, pois o mesmo j� possui registro no Sistema.';
  end;

  Result := (QryVerif.RecordCount = 0) and (qryGridPaiPadrao.RecordCount > 0);

  FreeAndNil(QryVerif);
end;

procedure TFFiltroCargo.VerificarPermissoes;
begin
  inherited;

  btnIncluir.Enabled  := vgPrm_IncCrg;
  btnEditar.Enabled   := vgPrm_EdCrg and (qryGridPaiPadrao.RecordCount > 0);
  btnExcluir.Enabled  := vgPrm_ExcCrg and (qryGridPaiPadrao.RecordCount > 0);
  btnImprimir.Enabled := False;
end;

end.
