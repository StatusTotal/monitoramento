{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UOpcoesAcaoBaixaDeposito.pas
  Descricao:   Tela de Opcoes de Acao com Sobra ou falta durante baixa de Deposito
  Author   :   Cristina
  Date:        10-jan-2017
  Last Update: 07-dez-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UOpcoesAcaoBaixaDeposito;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UOpcoesAcaoPadrao, JvExControls,
  JvButton, JvTransparentButton, Vcl.ExtCtrls, Vcl.StdCtrls;

type
  TFOpcoesAcaoBaixaDeposito = class(TFOpcoesAcaoPadrao)
    btnOpcao6: TJvTransparentButton;
    btnOpcao5: TJvTransparentButton;
    btnOpcao4: TJvTransparentButton;
    btnOpcao3: TJvTransparentButton;
    pnlValores: TPanel;
    lblSobra: TLabel;
    lblFalta: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btnOpcao3Click(Sender: TObject);
    procedure btnOpcao6Click(Sender: TObject);
    procedure btnOpcao5Click(Sender: TObject);
    procedure btnOpcao1Click(Sender: TObject);
    procedure btnOpcao4Click(Sender: TObject);
    procedure btnOpcao2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FOpcoesAcaoBaixaDeposito: TFOpcoesAcaoBaixaDeposito;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDesmembramentoDeposito,
  UDMLancamento, UCadastroLancamento;

procedure TFOpcoesAcaoBaixaDeposito.btnOpcao1Click(Sender: TObject);
begin
  inherited;

  //Lancamento de Sobra de Caixa
  Self.Close;
end;

procedure TFOpcoesAcaoBaixaDeposito.btnOpcao2Click(Sender: TObject);
begin
  inherited;

  //Desmembramento de Deposito
  try
    Application.CreateForm(TFDesmembramentoDeposito, FDesmembramentoDeposito);

    FDesmembramentoDeposito.iIdDeposFlutP   := dmLancamento.cdsParcela_F.FieldByName('ID_DEPOSITO_FLUTUANTE_FK').AsInteger;
    FDesmembramentoDeposito.cValorUtilizado := dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency;

    FDesmembramentoDeposito.ShowModal;
  finally
    FDesmembramentoDeposito.Free;
  end;

  dmLancamento.cdsParcela_F.FieldByName('TP_DIFERENCA').Value := Null;
  dmLancamento.cdsParcela_F.FieldByName('VR_DIFERENCA').Value := Null;

  Self.Close;
end;

procedure TFOpcoesAcaoBaixaDeposito.btnOpcao3Click(Sender: TObject);
begin
  inherited;

  //Lancamento de Juros em Parcela
  dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_JUROS').AsCurrency := dmLancamento.cdsParcela_F.FieldByName('VR_DIFERENCA').AsCurrency;
  dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency  := (dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency +
                                                                           dmLancamento.cdsParcela_F.FieldByName('VR_DIFERENCA').AsCurrency);
  dmLancamento.cdsParcela_F.FieldByName('TP_DIFERENCA').Value          := Null;
  dmLancamento.cdsParcela_F.FieldByName('VR_DIFERENCA').Value          := Null;

  Self.Close;
end;

procedure TFOpcoesAcaoBaixaDeposito.btnOpcao4Click(Sender: TObject);
begin
  inherited;

  //Lancamento de Falta de Caixa
  Self.Close;
end;

procedure TFOpcoesAcaoBaixaDeposito.btnOpcao5Click(Sender: TObject);
begin
  inherited;

  //Lancamento de Nova Parcela
  FCadastroLancamento.lGerarNovaParcela := True;

  Self.Close;
end;

procedure TFOpcoesAcaoBaixaDeposito.btnOpcao6Click(Sender: TObject);
begin
  inherited;

  //Lancamento de Desconto de Parcela
  dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_DESCONTO').AsCurrency := dmLancamento.cdsParcela_F.FieldByName('VR_DIFERENCA').AsCurrency;
  dmLancamento.cdsParcela_F.FieldByName('TP_DIFERENCA').Value             := Null;
  dmLancamento.cdsParcela_F.FieldByName('VR_DIFERENCA').Value             := Null;

  Self.Close;
end;

procedure TFOpcoesAcaoBaixaDeposito.FormShow(Sender: TObject);
begin
  inherited;

  btnOpcao1.Enabled  := (dmLancamento.cdsParcela_F.FieldByName('TP_DIFERENCA').AsString = 'S');   //Lancamento de Sobra de Caixa
  btnOpcao2.Enabled  := (dmLancamento.cdsParcela_F.FieldByName('TP_DIFERENCA').AsString = 'S') and vgPrm_IncDepo;  //Desmembramento de Deposito
  btnOpcao3.Enabled  := (dmLancamento.cdsParcela_F.FieldByName('TP_DIFERENCA').AsString = 'S');   //Lancamento de Juros em Parcela
  btnOpcao4.Enabled  := (dmLancamento.cdsParcela_F.FieldByName('TP_DIFERENCA').AsString = 'F');  //Lancamento de Falta de Caixa
  btnOpcao5.Enabled  := (dmLancamento.cdsParcela_F.FieldByName('TP_DIFERENCA').AsString = 'F');   //Lancamento de Nova Parcela
  btnOpcao6.Enabled  := (dmLancamento.cdsParcela_F.FieldByName('TP_DIFERENCA').AsString = 'F');  //Lancamento de Desconto de Parcela

  if dmLancamento.cdsParcela_F.FieldByName('TP_DIFERENCA').AsString = 'S' then
    lblSobra.Caption := 'Sobra: R$' + dmLancamento.cdsParcela_F.FieldByName('VR_DIFERENCA').AsString
  else if dmLancamento.cdsParcela_F.FieldByName('TP_DIFERENCA').AsString = 'F' then
    lblFalta.Caption := 'Falta: R$' + dmLancamento.cdsParcela_F.FieldByName('VR_DIFERENCA').AsString;
end;

end.
