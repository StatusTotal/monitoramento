{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroOficio.pas
  Descricao:   Formulario de cadastro de Oficio Recebido e Enviado
  Author   :   Cristina
  Date:        12-jan-2017
  Last Update: 23-out-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroOficio;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Mask, Vcl.DBCtrls, JvMaskEdit, JvDBControls, JvExMask, JvToolEdit,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxEdit, dxSkinsCore, dxSkinMetropolis, dxGDIPlusClasses, cxImage, Vcl.Grids,
  Vcl.DBGrids, JvExExtCtrls, JvExtComponent, JvItemsPanel, Vcl.ExtDlgs,
  Vcl.Clipbrd, JvImage, Vcl.Imaging.jpeg, Vcl.Imaging.pngimage, UDM,
  System.StrUtils, System.DateUtils, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, Vcl.Menus, Vcl.OleCtrls, gtScrollingPanel,
  gtPDFViewer, gtPDFClasses, gtCstPDFDoc, gtExPDFDoc, gtExProPDFDoc, gtPDFDoc;

type
  TFCadastroOficio = class(TFCadastroGeralPadrao)
    btnAbaDigitalizacao: TJvTransparentButton;
    ntbCadastro: TNotebook;
    lblCodOficio: TLabel;
    Label2: TLabel;
    lblDataOficio: TLabel;
    lblHoraOficio: TLabel;
    lblAssunto: TLabel;
    lblPessoa: TLabel;
    lblNumControle: TLabel;
    lblFormaEnvio: TLabel;
    lblNumProcesso: TLabel;
    lblOrgao: TLabel;
    lblObservacao: TLabel;
    lblNumRastreio: TLabel;
    lblNumProtocolo: TLabel;
    lblDataSituacaoAR: TLabel;
    lblFuncionario: TLabel;
    edtCodOficio: TDBEdit;
    edtAnoOficio: TDBEdit;
    dteDataOficio: TJvDBDateEdit;
    medHoraOficio: TJvDBMaskEdit;
    edtAssunto: TDBEdit;
    lcbPessoa: TDBLookupComboBox;
    edtNumControle: TDBEdit;
    edtNumProcesso: TDBEdit;
    lcbOrgao: TDBLookupComboBox;
    lcbFormaEnvio: TDBLookupComboBox;
    edtNumRastreio: TDBEdit;
    dteDataSituacaoAR: TJvDBDateEdit;
    mmObservacao: TDBMemo;
    btnPessoa: TJvTransparentButton;
    btnOrgao: TJvTransparentButton;
    btnFormaEnvio: TJvTransparentButton;
    lcbFuncionario: TDBLookupComboBox;
    btnAcionarScanner: TJvTransparentButton;
    btnAbrirPastaImagem: TJvTransparentButton;
    btnColarImagem: TJvTransparentButton;
    btnConfirmarAcao: TJvTransparentButton;
    btnCancelarAcao: TJvTransparentButton;
    edtNomeArquivo: TEdit;
    btnEditarImagem: TJvTransparentButton;
    btnExcluirImagem: TJvTransparentButton;
    dbgImagens: TDBGrid;
    ipnlHistoricoOficios: TJvItemsPanel;
    lblHistoricoOficios: TLabel;
    edtNumProtocolo: TDBEdit;
    rgSituacaoAR: TDBRadioGroup;
    chbReservado: TDBCheckBox;
    lblAnoOficio: TLabel;
    odAbrirPastaImagem: TOpenPictureDialog;
    ppmImagem: TPopupMenu;
    btnExportar: TMenuItem;
    ntbArquivo: TNotebook;
    btnAssociarPDF: TJvTransparentButton;
    pnlImagem: TPanel;
    imgImagemOficio: TcxImage;
    odAbrirPDF: TOpenDialog;
    docPDF: TgtPDFDocument;
    vwPDF: TgtPDFViewer;
    odSalvarPDF: TOpenDialog;
    procedure dbgImagensColEnter(Sender: TObject);
    procedure dbgImagensDblClick(Sender: TObject);
    procedure dbgImagensDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure edtNomeArquivoKeyPress(Sender: TObject; var Key: Char);
    procedure btnEditarImagemClick(Sender: TObject);
    procedure btnExcluirImagemClick(Sender: TObject);
    procedure btnAcionarScannerClick(Sender: TObject);
    procedure btnAbrirPastaImagemClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnColarImagemClick(Sender: TObject);
    procedure btnConfirmarAcaoClick(Sender: TObject);
    procedure btnCancelarAcaoClick(Sender: TObject);
    procedure btnDadosPrincipaisClick(Sender: TObject);
    procedure btnAbaDigitalizacaoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure ipnlHistoricoOficiosItemClick(Sender: TObject;
      ItemIndex: Integer);
    procedure btnPessoaClick(Sender: TObject);
    procedure btnOrgaoClick(Sender: TObject);
    procedure btnFormaEnvioClick(Sender: TObject);
    procedure lcbFuncionarioKeyPress(Sender: TObject; var Key: Char);
    procedure mmObservacaoKeyPress(Sender: TObject; var Key: Char);
    procedure btnExportarClick(Sender: TObject);
    procedure btnAssociarPDFClick(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }

    lTabAuxDig: Boolean;

    iIdIncImg,
    IdPrimeiroOficio: Integer;

    sNomeOriginal: String;

    procedure HabDesabBotoesCRUD(Inc, Ed, Exc: Boolean);
    procedure DesabilitaHabilitaCampos(Hab: Boolean);
    procedure ClassificarOficio;
  public
    { Public declarations }
  end;

var
  FCadastroOficio: TFCadastroOficio;

  TpOfc: TTipoOficio;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UGDM, UVariaveisGlobais, UDMOficio,
  UCadastroOficioFormaEnvio, UCadastroOficioOrgao, UCadastroOficioPessoa,
  UConsultaOficio, UCapturaImagem;

{ TFCadastroOficio }

procedure TFCadastroOficio.btnAbaDigitalizacaoClick(Sender: TObject);
begin
  inherited;

  //DIGITALIZACAO
  if not lTabAuxDig then
  begin
    with dmOficio do
    begin
      if not (vgOperacao in [C, I]) then
      begin
        if cdsOficioImg_F.RecordCount > 0 then
          HabDesabBotoesCRUD(True, True, True)
        else
          HabDesabBotoesCRUD(True, False, False);
      end;

      if (vgOperacao <> I) and
        (dmOficio.cdsOficioImg_F.FieldByName('CAMINHO_COMPLETO').AsString <> '') then
      begin
        if dmOficio.cdsOficioImg_F.FieldByName('EXTENSAO').AsString = 'jpg' then
          imgImagemOficio.Picture.LoadFromFile(dmOficio.cdsOficioImg_F.FieldByName('CAMINHO_COMPLETO').AsString)
        else if dmOficio.cdsOficioImg_F.FieldByName('EXTENSAO').AsString = 'pdf' then
        begin
          docPDF.Reset;
          docPDF.LoadFromFile(dmOficio.cdsOficioImg_F.FieldByName('CAMINHO_COMPLETO').AsString);
          vwPDF.Align := alClient;
          vwPDF.PDFDocument := docPDF;
          vwPDF.Active := True;
        end;
      end;

      DesabilitaHabilitaCampos(False);
    end;

    lTabAuxDig := True;

    if dmOficio.cdsOficioImg_F.RecordCount > 0 then
    begin
      imgImagemOficio.PopupMenu := ppmImagem;
      vwPDF.PopupMenu          := ppmImagem;
    end
    else
    begin
      imgImagemOficio.PopupMenu := nil;
      vwPDF.PopupMenu          := nil;
    end;

    ntbArquivo.PageIndex := 0;
  end;

  MarcarDesmarcarAbas(DIG);
end;

procedure TFCadastroOficio.btnAbrirPastaImagemClick(Sender: TObject);
var
  sCEscolhido, sCaminho: String;
  j, iPos: Integer;
begin
  inherited;

  imgImagemOficio.PopupMenu := nil;

  ntbArquivo.PageIndex := 0;

  odAbrirPastaImagem.InitialDir := vgConf_DiretorioImagens +
                                   IntToStr(vgOfc_Ano) + '\';
  iPos := 0;

  if odAbrirPastaImagem.Execute then
  begin
    if odAbrirPastaImagem.FileName <> '' then
    begin
      sCEscolhido := odAbrirPastaImagem.FileName;

      for j := 0 to Length(sCEscolhido) - 1 do
      begin
        if sCEscolhido[j] = '\' then
          iPos := j;
      end;

      sCaminho  := Copy(sCEscolhido, 1, iPos);

      try
        //Tela
        imgImagemOficio.Picture.LoadFromFile(odAbrirPastaImagem.FileName);

        //Banco
        Inc(iIdIncImg);
        dmOficio.cdsOficioImg_F.Append;
        dmOficio.cdsOficioImg_F.FieldByName('NOVO').AsBoolean          := True;
        dmOficio.cdsOficioImg_F.FieldByName('ID_OFICIO_IMG').AsInteger := iIdIncImg;

        if UpperCase(sCaminho) <> UpperCase(vgConf_DiretorioImagens +
                                            IntToStr(vgOfc_Ano) + '\') then
          dmOficio.cdsOficioImg_F.FieldByName('ORIGEM_ANTIGA').AsString := sCEscolhido;

        HabDesabBotoesCRUD(False, False, False);
        DesabilitaHabilitaCampos(True);

        if edtNomeArquivo.CanFocus then
          edtNomeArquivo.SetFocus;
      except
        //Nao exibir erro caso nao consiga carregar a Imagem
        if dmOficio.cdsOficioImg_F.State = dsInsert then
          dmOficio.cdsOficioImg_F.Cancel;

        HabDesabBotoesCRUD(True, True, True);
        DesabilitaHabilitaCampos(False);

        if dmOficio.cdsOficioImg_F.RecordCount > 0 then
        begin
          imgImagemOficio.PopupMenu := ppmImagem;
          vwPDF.PopupMenu           := ppmImagem;
        end;
      end;
    end;
  end;
end;

procedure TFCadastroOficio.btnAcionarScannerClick(Sender: TObject);
var
  Imagem: TBitmap;
begin
  inherited;

  imgImagemOficio.PopupMenu := nil;

  Imagem := nil;

  ntbArquivo.PageIndex := 0;

  try
    Application.CreateForm(TFCapturaImagem, FCapturaImagem);
    FCapturaImagem.ShowModal;
  finally
    Imagem := FCapturaImagem.ImagemCapturada;
    FreeAndNil(FCapturaImagem);
  end;

  if Imagem <> nil then
  begin
    try
      //Tela
      imgImagemOficio.Picture.Assign(Imagem);

      //Banco
      Inc(iIdIncImg);
      dmOficio.cdsOficioImg_F.Append;
      dmOficio.cdsOficioImg_F.FieldByName('NOVO').AsBoolean          := True;
      dmOficio.cdsOficioImg_F.FieldByName('ID_OFICIO_IMG').AsInteger := iIdIncImg;

      HabDesabBotoesCRUD(False, False, False);
      DesabilitaHabilitaCampos(True);

      if edtNomeArquivo.CanUndo then
        edtNomeArquivo.SetFocus;
    except
      HabDesabBotoesCRUD(True, True, True);
      DesabilitaHabilitaCampos(False);

      if dmOficio.cdsOficioImg_F.RecordCount > 0 then
      begin
        imgImagemOficio.PopupMenu := ppmImagem;
        vwPDF.PopupMenu           := ppmImagem;
      end;
    end;
  end;
end;

procedure TFCadastroOficio.btnAssociarPDFClick(Sender: TObject);
var
  sCEscolhido, sCaminho: String;
  j, iPos: Integer;
begin
  inherited;

  sNomeOriginal := '';

  vwPDF.PopupMenu := nil;

  ntbArquivo.PageIndex := 1;

  odAbrirPDF.InitialDir := vgConf_DiretorioImagens +
                           IntToStr(vgOfc_Ano) + '\';
  iPos := 0;

  if odAbrirPDF.Execute then
  begin
    if odAbrirPDF.FileName <> '' then
    begin
      sNomeOriginal := odAbrirPDF.FileName;
      sCEscolhido   := odAbrirPDF.FileName;

      for j := 0 to Length(sCEscolhido) - 1 do
      begin
        if sCEscolhido[j] = '\' then
          iPos := j;
      end;

      sCaminho := Copy(sCEscolhido, 1, iPos);

      try
        //Tela
        docPDF.Reset;
        docPDF.LoadFromFile(odAbrirPDF.FileName);
        vwPDF.Align := alClient;
        vwPDF.PDFDocument := docPDF;
        vwPDF.Active := True;

        //Banco
        Inc(iIdIncImg);
        dmOficio.cdsOficioImg_F.Append;
        dmOficio.cdsOficioImg_F.FieldByName('NOVO').AsBoolean          := True;
        dmOficio.cdsOficioImg_F.FieldByName('ID_OFICIO_IMG').AsInteger := iIdIncImg;

        if UpperCase(sCaminho) <> UpperCase(vgConf_DiretorioImagens +
                                            IntToStr(vgOfc_Ano) + '\') then
          dmOficio.cdsOficioImg_F.FieldByName('ORIGEM_ANTIGA').AsString := sCEscolhido;

        HabDesabBotoesCRUD(False, False, False);
        DesabilitaHabilitaCampos(True);

        if edtNomeArquivo.CanFocus then
          edtNomeArquivo.SetFocus;
      except
        //Nao exibir erro caso nao consiga carregar o Arquivo
        if dmOficio.cdsOficioImg_F.State = dsInsert then
          dmOficio.cdsOficioImg_F.Cancel;

        HabDesabBotoesCRUD(True, True, True);
        DesabilitaHabilitaCampos(False);

        if dmOficio.cdsOficioImg_F.RecordCount > 0 then
        begin
          imgImagemOficio.PopupMenu := ppmImagem;
          vwPDF.PopupMenu           := ppmImagem;
        end;
      end;
    end;
  end;
end;

procedure TFCadastroOficio.btnCancelarAcaoClick(Sender: TObject);
begin
  inherited;

  dmOficio.cdsOficioImg_F.Cancel;

  edtNomeArquivo.Clear;

  if dmOficio.cdsOficioImg_F.RecordCount = 0 then
    HabDesabBotoesCRUD(True, False, False)
  else
    HabDesabBotoesCRUD(True, True, True);

  DesabilitaHabilitaCampos(False);

  if dmOficio.cdsOficioImg_F.RecordCount > 0 then
  begin
    imgImagemOficio.PopupMenu := ppmImagem;
    vwPDF.PopupMenu           := ppmImagem;
  end;
end;

procedure TFCadastroOficio.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
  begin
    { IMAGENS }
    if dmOficio.cdsOficioImg_F.RecordCount > 0 then
    begin
      dmOficio.cdsOficioImg_F.First;

      while not dmOficio.cdsOficioImg_F.Eof do
      begin
        if not dmOficio.cdsOficioImg_F.FieldByName('NOME_V_ANTIGO').IsNull then
        begin
          if FileExists(vgConf_DiretorioImagens +
                        IntToStr(vgOfc_Ano) +
                        '\' +
                        dmOficio.cdsOficioImg_F.FieldByName('NOME_FIXO').AsString +
                        dmOficio.cdsOficioImg_F.FieldByName('NOME_V_ANTIGO').AsString + '.' +
                        dmOficio.cdsOficioImg_F.FieldByName('EXTENSAO').AsString) then
          begin
            DeleteFile(vgConf_DiretorioImagens +
                       IntToStr(vgOfc_Ano) +
                       '\' +
                       dmOficio.cdsOficioImg_F.FieldByName('NOME_FIXO').AsString +
                       dmOficio.cdsOficioImg_F.FieldByName('NOME_V_ANTIGO').AsString + '.' +
                       dmOficio.cdsOficioImg_F.FieldByName('EXTENSAO').AsString);
          end;
        end;

        if dmOficio.cdsOficioImg_F.FieldByName('NOVO').AsBoolean and
          dmOficio.cdsOficioImg_F.FieldByName('ID_OFICIO_FK').IsNull then
        begin
          if FileExists(vgConf_DiretorioImagens +
                        IntToStr(vgOfc_Ano) +
                        '\' +
                        dmOficio.cdsOficioImg_F.FieldByName('NOME_FIXO').AsString +
                        dmOficio.cdsOficioImg_F.FieldByName('NOME_VARIAVEL').AsString + '.' +
                        dmOficio.cdsOficioImg_F.FieldByName('EXTENSAO').AsString) then
          begin
            DeleteFile(vgConf_DiretorioImagens +
                       IntToStr(vgOfc_Ano) +
                       '\' +
                       dmOficio.cdsOficioImg_F.FieldByName('NOME_FIXO').AsString +
                       dmOficio.cdsOficioImg_F.FieldByName('NOME_VARIAVEL').AsString + '.' +
                       dmOficio.cdsOficioImg_F.FieldByName('EXTENSAO').AsString);
          end;
        end;

        dmOficio.cdsOficioImg_F.Next;
      end;
    end;

    Self.Close;
  end;
end;

procedure TFCadastroOficio.btnColarImagemClick(Sender: TObject);
var
  imgBitmap: TBitmap;
begin
  inherited;

  imgImagemOficio.PopupMenu := nil;

  ntbArquivo.PageIndex := 0;

  try
    Clipboard.Open;

    if Clipboard.HasFormat(CF_PICTURE) or
      Clipboard.HasFormat(CF_BITMAP) then
    begin
      //Tela
      imgBitmap := TBitmap.Create;
      imgBitmap.Assign(Clipboard);
      imgImagemOficio.Picture.Bitmap.Assign(imgBitmap);

      //Banco
      Inc(iIdIncImg);
      dmOficio.cdsOficioImg_F.Append;
      dmOficio.cdsOficioImg_F.FieldByName('NOVO').AsBoolean          := True;
      dmOficio.cdsOficioImg_F.FieldByName('ID_OFICIO_IMG').AsInteger := iIdIncImg;

      HabDesabBotoesCRUD(False, False, False);
      DesabilitaHabilitaCampos(True);

      if edtNomeArquivo.CanFocus then
        edtNomeArquivo.SetFocus;
    end;

    Clipboard.Close;

    FreeAndNil(imgBitmap);
  except
    FreeAndNil(imgBitmap);

    if dmOficio.cdsOficioImg_F.RecordCount > 0 then
    begin
      imgImagemOficio.PopupMenu := ppmImagem;
      vwPDF.PopupMenu           := ppmImagem;
    end;
  end;
end;

procedure TFCadastroOficio.btnConfirmarAcaoClick(Sender: TObject);
var
  sNomeFoto, MsgErro: String;
  imgJpg: TJPEGImage;
  arqPDF: TgtPDFDocument;
begin
  inherited;

  MsgErro := '';

  if Trim(edtNomeArquivo.Text) = '' then
  begin
    MsgErro := 'Nenhum nome foi escolhido para o arquivo.' + #13 +
               'Por favor, digite um nome para o mesmo.';

    Application.MessageBox(PChar(MsgErro), 'Erro', MB_OK + MB_ICONERROR);

    if edtNomeArquivo.CanFocus then
      edtNomeArquivo.SetFocus;
  end
  else
  begin
    sNomeFoto := dmOficio.cdsOficio_F.FieldByName('COD_OFICIO').AsString + '_' +
                 vgOfc_TpOfc + '_' +
                 Trim(edtNomeArquivo.Text) +
                 IfThen(ntbArquivo.PageIndex = 0, '.jpg', '.pdf');

    if FileExists(vgConf_DiretorioImagens +
                  IntToStr(vgOfc_Ano) +
                  '\' + sNomeFoto) then
    begin
      MsgErro := 'J� existe um arquivo com o nome escolhido.' + #13 +
                 'Por favor, digite outro nome para o arquivo.';

      Application.MessageBox(PChar(MsgErro), 'Aviso', MB_OK + MB_ICONEXCLAMATION);

      if edtNomeArquivo.CanFocus then
        edtNomeArquivo.SetFocus;
    end
    else
    begin
      if dmOficio.cdsOficioImg_F.State = dsInsert then
      begin
        //Arquivo
        if ntbArquivo.PageIndex = 0 then
        begin
          dmGerencial.Processando(True, 'Salvando Imagem', True, 'Por favor, aguarde...');
          imgJpg := TJPEGImage.Create;
          imgJpg.Assign(imgImagemOficio.Picture.Graphic);
          imgJpg.SaveToFile(vgConf_DiretorioImagens +
                            IntToStr(vgOfc_Ano) +
                            '\' + sNomeFoto);
          dmGerencial.Processando(False);
        end
        else if ntbArquivo.PageIndex = 1 then
        begin
          if Trim(sNomeOriginal) <> '' then
          begin
            dmGerencial.Processando(True, 'Salvando arquivo PDF', True, 'Por favor, aguarde...');
            arqPDF := TgtPDFDocument.Create(nil);
            arqPDF.LoadFromFile(sNomeOriginal);
            arqPDF.SaveToFile(vgConf_DiretorioImagens +
                              IntToStr(vgOfc_Ano) +
                              '\' +
                              sNomeFoto);
            arqPDF.OpenAfterSave := True;
            dmGerencial.Processando(False);
          end;
        end;
      end;

      if not dmOficio.cdsOficioImg_F.FieldByName('NOME_V_ANTIGO').IsNull then
      begin
        //Arquivo
        if ntbArquivo.PageIndex = 0 then
        begin
          dmGerencial.Processando(True, 'Salvando Imagem', True, 'Por favor, aguarde...');
          imgJpg := TJPEGImage.Create;
          imgJpg.Assign(imgImagemOficio.Picture.Graphic);
          imgJpg.SaveToFile(vgConf_DiretorioImagens +
                            IntToStr(vgOfc_Ano) +
                            '\' + sNomeFoto);
          dmGerencial.Processando(False);
        end
        else if ntbArquivo.PageIndex = 1 then
        begin
          if Trim(sNomeOriginal) <> '' then
          begin
            dmGerencial.Processando(True, 'Salvando arquivo PDF', True, 'Por favor, aguarde...');
            arqPDF := TgtPDFDocument.Create(nil);
            arqPDF.LoadFromFile(sNomeOriginal);
            arqPDF.SaveToFile(vgConf_DiretorioImagens +
                              IntToStr(vgOfc_Ano) +
                              '\' +
                              sNomeFoto);
            arqPDF.OpenAfterSave := True;
            dmGerencial.Processando(False);
          end;
        end;
      end;

      dmOficio.cdsOficioImg_F.FieldByName('NOME_VARIAVEL').AsString := Trim(edtNomeArquivo.Text);
      dmOficio.cdsOficioImg_F.FieldByName('NOME_FIXO').AsString     := dmOficio.cdsOficio_F.FieldByName('COD_OFICIO').AsString + '_' +
                                                                       vgOfc_TpOfc + '_';
      dmOficio.cdsOficioImg_F.FieldByName('EXTENSAO').AsString      := IfThen(ntbArquivo.PageIndex = 0, 'jpg', 'pdf');

      dmOficio.cdsOficioImg_F.Post;

      HabDesabBotoesCRUD(True, True, True);
      DesabilitaHabilitaCampos(False);

      FreeAndNil(imgJpg);
      FreeAndNil(arqPDF);
    end;
  end;

  imgImagemOficio.PopupMenu := ppmImagem;

  if (ntbArquivo.PageIndex = 1) and
    (Trim(sNomeOriginal) <> '') then
  begin
    docPDF.Reset;
    docPDF.LoadFromFile(vgConf_DiretorioImagens +
                        IntToStr(vgOfc_Ano) +
                        '\' +
                        sNomeFoto);
    vwPDF.Align := alClient;
    vwPDF.PDFDocument := docPDF;
    vwPDF.Active := True;
  end;

  vwPDF.PopupMenu := ppmImagem;

  edtNomeArquivo.Clear;
end;

procedure TFCadastroOficio.btnDadosPrincipaisClick(Sender: TObject);
begin
  inherited;

  MarcarDesmarcarAbas(PRINC);
end;

procedure TFCadastroOficio.btnEditarImagemClick(Sender: TObject);
begin
  inherited;

  if dmOficio.cdsOficioImg_F.RecordCount > 0 then
  begin
    dmOficio.cdsOficioImg_F.Edit;
    dmOficio.cdsOficioImg_F.FieldByName('NOME_V_ANTIGO').AsString := dmOficio.cdsOficioImg_F.FieldByName('NOME_VARIAVEL').AsString;
    edtNomeArquivo.Text := dmOficio.cdsOficioImg_F.FieldByName('NOME_VARIAVEL').AsString;
    HabDesabBotoesCRUD(False, False, False);
    DesabilitaHabilitaCampos(True);
  end;
end;

procedure TFCadastroOficio.btnExcluirImagemClick(Sender: TObject);
begin
  inherited;

  if dmOficio.cdsOficioImg_F.RecordCount > 0 then
    dmOficio.cdsOficioImg_F.Delete;
end;

procedure TFCadastroOficio.btnExportarClick(Sender: TObject);
var
  sCEscolhido: String;
  j, iPos: Integer;
  imgJpg: TJPEGImage;
  arqPDF: TgtPDFDocument;
begin
  inherited;

  //Exporta para outro diretorio, a escolha do usuario
  if Trim(dmOficio.cdsOficioImg_F.FieldByName('EXTENSAO').AsString) = 'jpg' then
  begin
    odAbrirPastaImagem.InitialDir := 'C:\';
    iPos := 0;

    if odAbrirPastaImagem.Execute then
    begin
      if odAbrirPastaImagem.FileName <> '' then
      begin
        sCEscolhido := odAbrirPastaImagem.FileName;

        for j := 0 to Length(sCEscolhido) - 1 do
        begin
          if sCEscolhido[j] = '\' then
            iPos := j;
        end;

        imgJpg := TJPEGImage.Create;
        imgJpg.Assign(imgImagemOficio.Picture.Graphic);
        imgJpg.SaveToFile(sCEscolhido);
      end;
    end;
  end
  else if Trim(dmOficio.cdsOficioImg_F.FieldByName('EXTENSAO').AsString) = 'pdf' then
  begin
    odSalvarPDF.InitialDir := 'C:\';
    iPos := 0;

    if odSalvarPDF.Execute then
    begin
      if odSalvarPDF.FileName <> '' then
      begin
        sCEscolhido := odSalvarPDF.FileName;

        for j := 0 to Length(sCEscolhido) - 1 do
        begin
          if sCEscolhido[j] = '\' then
            iPos := j;
        end;

        arqPDF := TgtPDFDocument.Create(nil);
        arqPDF.LoadFromFile(dmOficio.cdsOficioImg_F.FieldByName('CAMINHO_COMPLETO').AsString);
        arqPDF.SaveToFile(sCEscolhido);
        arqPDF.OpenAfterSave := True;
      end;
    end;
  end;
end;

procedure TFCadastroOficio.btnFormaEnvioClick(Sender: TObject);
var
  Op: TOperacao;
  IdCons: Integer;
  OrigF, OrigC: Boolean;
begin
  inherited;

  Op     := vgOperacao;
  IdCons := vgIdConsulta;
  OrigF  := vgOrigemFiltro;
  OrigC  := vgOrigemCadastro;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := False;
  vgOrigemCadastro := True;

  try
    Application.CreateForm(TFCadastroOficioFormaEnvio, FCadastroOficioFormaEnvio);
    FCadastroOficioFormaEnvio.ShowModal;
  finally
    dmOficio.qryOfcFrmEnv.Close;
    dmOficio.qryOfcFrmEnv.Open;

    if FCadastroOficioFormaEnvio.IdNovaFEnvio <> 0 then
      dmOficio.cdsOficio_F.FieldByName('ID_OFICIO_FORMAENVIO_FK').AsInteger := FCadastroOficioFormaEnvio.IdNovaFEnvio;

    FCadastroOficioFormaEnvio.Free;
  end;

  vgOperacao       := Op;
  vgIdConsulta     := IdCons;
  vgOrigemFiltro   := OrigF;
  vgOrigemCadastro := OrigC;

  if Trim(lcbPessoa.Text) <> '' then
  begin
    if edtNumRastreio.CanFocus then
      edtNumRastreio.SetFocus;
  end;
end;

procedure TFCadastroOficio.btnOkClick(Sender: TObject);
var
  QryAux: TFDQuery;
  lCancelado: Boolean;
  sIdPrmOfc: String;
begin
  inherited;

  IdPrimeiroOficio := 0;
  lCancelado       := False;
  sIdPrmOfc        := '';

  if vgOperacao <> C then
  begin
    if (vgOperacao = I) and
      (TpOfc = ENV) then
    begin
      QryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

      QryAux.Close;
      QryAux.SQL.Clear;
      QryAux.SQL.Text := 'SELECT FIRST 1 ID_OFICIO ' +
                         '  FROM OFICIO ' +
                         ' WHERE TIPO_OFICIO = ' + QuotedStr('E');
      QryAux.Open;

      if QryAux.RecordCount = 0 then
      begin
        Application.MessageBox(PChar('Esse � o primeiro Of�cio Enviado a ser cadastrado no sistema.' + #13#10 +
                                     'Para prosseguir, ser� preciso informar o N�mero sequencial desse Of�cio (os pr�ximos ter�o esse N�mero gerado do automaticamente).'),
                               'Aviso',
                               MB_OK);

        repeat
          if not InputQuery('N� DO OF�CIO',
                            'Por favor, informe o N�MERO SEQUENCIAL DO OF�CIO (SEM o Ano):',
                            sIdPrmOfc) then
            lCancelado := True;
        until (Trim(sIdPrmOfc) <> '') or lCancelado;

        IdPrimeiroOficio := StrToInt(sIdPrmOfc);
      end;

      if lCancelado then
        Exit
      else
        GravarGenerico;
    end
    else
      GravarGenerico;
  end
  else
    Self.Close;
end;

procedure TFCadastroOficio.btnOrgaoClick(Sender: TObject);
var
  Op: TOperacao;
  IdCons: Integer;
  OrigF, OrigC: Boolean;
begin
  inherited;

  Op     := vgOperacao;
  IdCons := vgIdConsulta;
  OrigF  := vgOrigemFiltro;
  OrigC  := vgOrigemCadastro;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := False;
  vgOrigemCadastro := True;

  try
    Application.CreateForm(TFCadastroOficioOrgao, FCadastroOficioOrgao);
    FCadastroOficioOrgao.ShowModal;
  finally
    dmOficio.qryOfcOrgao.Close;
    dmOficio.qryOfcOrgao.Open;

    if FCadastroOficioOrgao.IdNovoOrgao <> 0 then
      dmOficio.cdsOficio_F.FieldByName('ID_OFICIO_ORGAO_FK').AsInteger := FCadastroOficioOrgao.IdNovoOrgao;

    FCadastroOficioOrgao.Free;
  end;

  vgOperacao       := Op;
  vgIdConsulta     := IdCons;
  vgOrigemFiltro   := OrigF;
  vgOrigemCadastro := OrigC;

  if Trim(lcbPessoa.Text) <> '' then
  begin
    if edtNumProtocolo.CanFocus then
      edtNumProtocolo.SetFocus;
  end;
end;

procedure TFCadastroOficio.btnPessoaClick(Sender: TObject);
var
  Op: TOperacao;
  IdCons: Integer;
  OrigF, OrigC: Boolean;
begin
  inherited;

  Op     := vgOperacao;
  IdCons := vgIdConsulta;
  OrigF  := vgOrigemFiltro;
  OrigC  := vgOrigemCadastro;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := False;
  vgOrigemCadastro := True;

  try
    Application.CreateForm(TFCadastroOficioPessoa, FCadastroOficioPessoa);
    FCadastroOficioPessoa.ShowModal;
  finally
    dmOficio.qryOfcPes.Close;
    dmOficio.qryOfcPes.Open;

    if FCadastroOficioPessoa.IdNovaPessoa <> 0 then
      dmOficio.cdsOficio_F.FieldByName('ID_OFICIO_PESSOA_FK').AsInteger := FCadastroOficioPessoa.IdNovaPessoa;

    FCadastroOficioPessoa.Free;
  end;

  vgOperacao       := Op;
  vgIdConsulta     := IdCons;
  vgOrigemFiltro   := OrigF;
  vgOrigemCadastro := OrigC;

  if Trim(lcbPessoa.Text) <> '' then
  begin
    if edtNumProcesso.CanFocus then
      edtNumProcesso.SetFocus;
  end;
end;

procedure TFCadastroOficio.ClassificarOficio;
begin
  if vgOperacao = I then
  begin
    if TpOfc = ENV then
    begin
      if vgOfc_IdAssoc = 0 then
        Self.Caption := 'Cadastro de Of�cio a Enviar'
      else
        Self.Caption := 'Cadastro de Of�cio a Enviar (Resposta)';
    end
    else if TpOfc = RCB then
    begin
      if vgOfc_IdAssoc = 0 then
        Self.Caption := 'Cadastro de Of�cio Recebido'
      else
        Self.Caption := 'Cadastro de Of�cio Recebido (Resposta)';
    end;
  end
  else
  begin
    if TpOfc = ENV then
    begin
      if vgOfc_IdAssoc = 0 then
      begin
        if vgOfc_Status = 'R' then
          Self.Caption := 'Cadastro de Of�cio Reservado'
        else
          Self.Caption := 'Cadastro de Of�cio Enviado';
      end
      else
      begin
        if vgOfc_Status = 'R' then
          Self.Caption := 'Cadastro de Of�cio Reservado (Resposta)'
        else
          Self.Caption := 'Cadastro de Of�cio Enviado (Resposta)';
      end;
    end
    else if TpOfc = RCB then
    begin
      if vgOfc_IdAssoc = 0 then
        Self.Caption := 'Cadastro de Of�cio Recebido'
      else
        Self.Caption := 'Cadastro de Of�cio Recebido (Resposta)';
    end;
  end;
end;

procedure TFCadastroOficio.dbgImagensColEnter(Sender: TObject);
begin
  inherited;

  if dmOficio.cdsOficioImg_F.FieldByName('CAMINHO_COMPLETO').AsString <> '' then
  begin
    if dmOficio.cdsOficioImg_F.FieldByName('EXTENSAO').AsString = 'jpg' then
    begin
      ntbArquivo.PageIndex := 0;
      imgImagemOficio.Picture.LoadFromFile(dmOficio.cdsOficioImg_F.FieldByName('CAMINHO_COMPLETO').AsString);
    end
    else if dmOficio.cdsOficioImg_F.FieldByName('EXTENSAO').AsString = 'pdf' then
    begin
      ntbArquivo.PageIndex := 1;
      docPDF.Reset;
      docPDF.LoadFromFile(dmOficio.cdsOficioImg_F.FieldByName('CAMINHO_COMPLETO').AsString);
      vwPDF.Align := alClient;
      vwPDF.PDFDocument := docPDF;
      vwPDF.Active := True;
    end;
  end;
end;

procedure TFCadastroOficio.dbgImagensDblClick(Sender: TObject);
begin
  inherited;

  if vgOperacao in [I, E] then
    btnEditarImagem.Click;
end;

procedure TFCadastroOficio.dbgImagensDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;

  AlternarCorGrid(Sender, Rect, DataCol, Column, State);
end;

procedure TFCadastroOficio.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtNumControle.MaxLength  := 8;
  edtAssunto.MaxLength      := 250;
  edtNumProcesso.MaxLength  := 25;
  edtNumProtocolo.MaxLength := 25;
  edtNumRastreio.MaxLength  := 25;
  mmObservacao.MaxLength    := 1000;
end;

procedure TFCadastroOficio.DesabilitaHabilitaCampos(Hab: Boolean);
begin
  edtNomeArquivo.Enabled := Hab;
  dbgImagens.Enabled := not Hab;
end;

procedure TFCadastroOficio.DesabilitarComponentes;
begin
  inherited;

  if TpOfc = ENV then
  begin
    btnOk.Enabled       := (vgPrm_IncOficEnv or vgPrm_EdOficEnv) and
                           (vgOperacao in [I, E, C]);
    btnCancelar.Enabled := (vgPrm_IncOficEnv or vgPrm_EdOficEnv) and
                           (vgOperacao in [I, E, C]);
  end;

  if TpOfc = RCB then
  begin
    btnOk.Enabled       := (vgPrm_IncOficRec or vgPrm_EdOficRec) and
                           (vgOperacao in [I, E, C]);
    btnCancelar.Enabled := (vgPrm_IncOficRec or vgPrm_EdOficRec) and
                           (vgOperacao in [I, E, C]);
  end;

  { OFICIO }
  //Codigo
  edtCodOficio.Enabled := (TpOfc = RCB);
  lblCodOficio.Visible := ((TpOfc = RCB) or
                           ((TpOfc = ENV) and
                            (vgOperacao in [C, E])));
  edtCodOficio.Visible := ((TpOfc = RCB) or
                           ((TpOfc = ENV) and
                            (vgOperacao in [C, E])));

  //Ano
  edtAnoOficio.Enabled := (TpOfc = RCB);
  Label2.Visible       := ((TpOfc = RCB) or
                           ((TpOfc = ENV) and
                            (vgOperacao in [C, E])));
  lblAnoOficio.Visible := ((TpOfc = RCB) or
                           ((TpOfc = ENV) and
                            (vgOperacao in [C, E])));
  edtAnoOficio.Visible := ((TpOfc = RCB) or
                           ((TpOfc = ENV) and
                            (vgOperacao in [C, E])));

  //Reservado
  chbReservado.Enabled := (TpOfc = ENV);
  chbReservado.Visible := (TpOfc = ENV);  //((TpOfc = ENV) and (vgOperacao in [C, E]));

  //Controle
  edtNumControle.Enabled := False;
  lblNumControle.Visible := (vgOperacao in [C, E]);
  edtNumControle.Visible := (vgOperacao in [C, E]);

  //Funcionario
  lcbFuncionario.Enabled := ((TpOfc = RCB) and
                             (vgOperacao in [I, E]));;

  if vgOperacao = C then
  begin
    { OFICIO }
    dteDataOficio.Enabled     := False;
    medHoraOficio.Enabled     := False;
    edtCodOficio.Enabled      := False;
    edtAnoOficio.Enabled      := False;
    edtAssunto.Enabled        := False;
    lcbPessoa.Enabled         := False;
    edtNumProcesso.Enabled    := False;
    lcbOrgao.Enabled          := False;
    edtNumProtocolo.Enabled   := False;
    lcbFormaEnvio.Enabled     := False;
    edtNumRastreio.Enabled    := False;
    rgSituacaoAR.Enabled      := False;
    dteDataSituacaoAR.Enabled := False;
    lcbFuncionario.Enabled    := False;

    mmObservacao.ReadOnly := True;

    { IMAGEM }
    btnAcionarScanner.Enabled   := False;
    btnAbrirPastaImagem.Enabled := False;
    btnColarImagem.Enabled      := False;
    btnAssociarPDF.Enabled      := False;
    btnConfirmarAcao.Enabled    := False;
    btnCancelarAcao.Enabled     := False;
    edtNomeArquivo.Enabled      := False;
    btnEditarImagem.Enabled     := False;
    btnExcluirImagem.Enabled    := False;

    dbgImagens.ReadOnly := True;
  end;
end;

procedure TFCadastroOficio.edtNomeArquivoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
     SelectNext(ActiveControl, True, True);
     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnConfirmarAcao.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroOficio.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;

  if lPodeCancelar then
  begin
    dmOficio.cdsOficioImg_F.Cancel;
    dmOficio.cdsOficio_F.Cancel;

    dmOficio.cdsOficioImg_F.Close;
    dmOficio.cdsOficio_F.Close;

    vgOfc_Codigo  := 0;
    vgOfc_Ano     := 0;
    vgOfc_Status  := '';
    vgOfc_IdAssoc := 0;
    vgOfc_TpOfc   := '';
  end
  else
    Action := caNone;
end;

procedure TFCadastroOficio.FormCreate(Sender: TObject);
begin
  inherited;

  BS.VerificarExistenciaPastasSistema;

  lTabAuxDig := False;

  iIdIncImg := 0;
end;

procedure TFCadastroOficio.FormShow(Sender: TObject);
var
  QryAux: TFDQuery;
  IdOfc, SeqOfc, iPessoa, iOrgao, iCont, j: Integer;
  sAssunto, sNumProcesso, sNumProtocolo: String;
  lPreenchido: Boolean;
  LstOfc: TListBox;
begin
  QryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

  SeqOfc := 0;
  IdOfc  := 0;

  iPessoa := 0;
  iOrgao  := 0;

  sAssunto      := '';
  sNumProcesso  := '';
  sNumProtocolo := '';

  lPreenchido := False;

  with dmOficio do
  begin
    //TABELAS AUXILIARES
    qryOfcPes.Close;
    qryOfcPes.Open;

    qryOfcFrmEnv.Close;
    qryOfcFrmEnv.Open;

    qryOfcOrgao.Close;
    qryOfcOrgao.Open;

    qryFuncionarios.Close;
    qryFuncionarios.Open;

    //OFICIO
    cdsOficio_F.Close;
    cdsOficio_F.Params.ParamByName('ID_OFICIO').Value := vgIdConsulta;
    cdsOficio_F.Open;

    vgOfc_TpOfc := IfThen((TpOfc = ENV),
                            'E',
                            IfThen((TpOfc = RCB),
                                   'R',
                                   ''));

    //IMAGENS
    cdsOficioImg_F.Close;
    cdsOficioImg_F.Params.ParamByName('ID_OFICIO').Value := vgIdConsulta;
    cdsOficioImg_F.Open;

    iIdIncImg := cdsOficioImg_F.RecordCount;

    //OFICIOS ASSOCIADOS
    LstOfc := TListBox.Create(Self);
    LstOfc.Parent  := Self;
    LstOfc.Visible := False;

    ipnlHistoricoOficios.Items.Clear;

    if vgOperacao <> I then
    begin
      iCont := 0;

      IdOfc := dmOficio.cdsOficio_F.FieldByName('ID_OFICIO').AsInteger;

      //O proprio Oficio
      LstOfc.Items.Add('(Pr�prio) ' +
                       dmGerencial.CompletaString(dmOficio.cdsOficio_F.FieldByName('ID_OFICIO').AsString,
                                                  '0',
                                                  'E',
                                                  8));
      //Proximos Oficios Associados
      repeat
        QryAux.Close;
        QryAux.SQL.Clear;

        QryAux.SQL.Text := 'SELECT ID_OFICIO, ' +
                           '       TIPO_OFICIO, ' +
                           '       ID_OFICIO_FK ' +
                           '  FROM OFICIO ' +
                           ' WHERE ID_OFICIO_FK = :ID_OFICIO';

        QryAux.Params.ParamByName('ID_OFICIO').Value := IdOfc;
        QryAux.Open;

        if QryAux.RecordCount > 0 then
        begin
          LstOfc.Items.Add('Controle: ' +
                           dmGerencial.CompletaString(QryAux.FieldByName('ID_OFICIO').AsString,
                                                      '0',
                                                      'E',
                                                      8));

          IdOfc  := QryAux.FieldByName('ID_OFICIO').AsInteger;
          Inc(iCont);
        end;

      until QryAux.RecordCount = 0;

      if iCont> 0 then
      begin
        for j := iCont Downto 0 do
          ipnlHistoricoOficios.Items.Add(LstOfc.Items.Strings[j]);
      end;
    end;

    if vgOperacao = I then
    begin
      if vgOfc_IdAssoc > 0 then
        IdOfc := vgOfc_IdAssoc;
    end
    else
      if not dmOficio.cdsOficio_F.FieldByName('ID_OFICIO_FK').IsNull then
        IdOfc := dmOficio.cdsOficio_F.FieldByName('ID_OFICIO_FK').AsInteger;

    //Oficos Associados Anteriormente
    repeat
      QryAux.Close;
      QryAux.SQL.Clear;

      QryAux.SQL.Text := 'SELECT ID_OFICIO, ' +
                         '       TIPO_OFICIO, ' +
                         '       ASSUNTO_OFICIO, ' +
                         '       ID_OFICIO_PESSOA_FK, ' +
                         '       NUM_PROCESSO, ' +
                         '       ID_OFICIO_ORGAO_FK, ' +
                         '       NUM_PROTOCOLO, ' +
                         '       ID_OFICIO_FK, ' +
                         '       SEQ_OFICIO ' +
                         '  FROM OFICIO ' +
                         ' WHERE ID_OFICIO = :ID_OFICIO';

      QryAux.Params.ParamByName('ID_OFICIO').Value := IdOfc;
      QryAux.Open;

      if QryAux.RecordCount > 0 then
      begin
        ipnlHistoricoOficios.Items.Add('Controle: ' + dmGerencial.CompletaString(QryAux.FieldByName('ID_OFICIO').AsString,
                                                                                 '0',
                                                                                 'E',
                                                                                 8));

        IdOfc  := QryAux.FieldByName('ID_OFICIO_FK').AsInteger;
        SeqOfc := QryAux.FieldByName('SEQ_OFICIO').AsInteger;

        if vgOperacao = I then
        begin
          if (((Trim(QryAux.FieldByName('TIPO_OFICIO').AsString) = 'R') and
               (TpOfc = RCB)) or
              ((Trim(QryAux.FieldByName('TIPO_OFICIO').AsString) = 'R') and
               (TpOfc = ENV)) or
              ((Trim(QryAux.FieldByName('TIPO_OFICIO').AsString) = 'E') and
               (TpOfc = ENV)) or
              ((Trim(QryAux.FieldByName('TIPO_OFICIO').AsString) = 'E') and
               (TpOfc = RCB))) and
            (not lPreenchido) then
          begin
            if not QryAux.FieldByName('ID_OFICIO_PESSOA_FK').IsNull then
              iPessoa := QryAux.FieldByName('ID_OFICIO_PESSOA_FK').AsInteger;

            if not QryAux.FieldByName('ID_OFICIO_ORGAO_FK').IsNull then
              iOrgao := QryAux.FieldByName('ID_OFICIO_ORGAO_FK').AsInteger;

            if not QryAux.FieldByName('ASSUNTO_OFICIO').IsNull then
              sAssunto := QryAux.FieldByName('ASSUNTO_OFICIO').AsString;

            if not QryAux.FieldByName('NUM_PROCESSO').IsNull then
              sNumProcesso := QryAux.FieldByName('NUM_PROCESSO').AsString;

            if not QryAux.FieldByName('NUM_PROTOCOLO').IsNull then
              sNumProtocolo := QryAux.FieldByName('NUM_PROTOCOLO').AsString;

            lPreenchido := True;
          end;
        end;
      end;

    until QryAux.RecordCount = 0;

    ipnlHistoricoOficios.Update;

    FreeAndNil(LstOfc);

    if vgOperacao = I then
    begin
      vgOfc_Codigo := 0;
      vgOfc_Ano    := YearOf(Date);
      vgOfc_Status := 'U';

      cdsOficio_F.Append;
      cdsOficio_F.FieldByName('DATA_OFICIO').AsDateTime      := Date;
      cdsOficio_F.FieldByName('HORA_OFICIO').AsString        := TimeToStr(Now);
      cdsOficio_F.FieldByName('TIPO_OFICIO').AsString        := vgOfc_TpOfc;
      cdsOficio_F.FieldByName('ANO_OFICIO').AsInteger        := YearOf(Date);
      cdsOficio_F.FieldByName('CAD_ID_USUARIO').AsInteger    := vgUsu_Id;
      cdsOficio_F.FieldByName('FLG_STATUS').AsString         := 'U';
      cdsOficio_F.FieldByName('DATA_STATUS').AsDateTime      := Now;

      if vgUsu_FlgSuporte = 'N' then
        cdsOficio_F.FieldByName('ID_FUNCIONARIO_FK').AsInteger := vgUsu_IdFuncionario;

      if vgOfc_IdAssoc = 0 then
        cdsOficio_F.FieldByName('SEQ_OFICIO').AsInteger   := 1
      else
      begin
        cdsOficio_F.FieldByName('SEQ_OFICIO').AsInteger   := (SeqOfc + 1);
        cdsOficio_F.FieldByName('ID_OFICIO_FK').AsInteger := vgOfc_IdAssoc;

        if iPessoa > 0 then
          cdsOficio_F.FieldByName('ID_OFICIO_PESSOA_FK').AsInteger := iPessoa;

        if iOrgao > 0 then
          cdsOficio_F.FieldByName('ID_OFICIO_ORGAO_FK').AsInteger := iOrgao;

        if Trim(sAssunto) <> '' then
          cdsOficio_F.FieldByName('ASSUNTO_OFICIO').AsString := sAssunto;

        if Trim(sNumProcesso) <> '' then
          cdsOficio_F.FieldByName('NUM_PROCESSO').AsString := sNumProcesso;

        if Trim(sNumProtocolo) <> '' then
          cdsOficio_F.FieldByName('NUM_PROTOCOLO').AsString := sNumProtocolo;
      end;
    end
    else
    begin
      //OFICIO
      cdsOficio_F.Edit;
    end;
  end;

  FreeAndNil(QryAux);

  inherited;

  ClassificarOficio;

  lblPessoa.Caption := IfThen((TpOfc = ENV),
                              'Destinat�rio',
                              IfThen((TpOfc = RCB),
                                     'Remetente',
                                     ''));

  lblFormaEnvio.Caption := 'Forma de ' + IfThen((TpOfc = ENV),
                                                'Envio',
                                                IfThen((TpOfc = RCB),
                                                       'Recebimento',
                                                       ''));

  MarcarDesmarcarAbas(PRINC);
end;

function TFCadastroOficio.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroOficio.GravarDadosGenerico(var Msg: String): Boolean;
var
  IdOficio, IdImagem: Integer;
  sMsgTela: String;
begin
  Result := True;
  Msg := '';
  sMsgTela := '';

  IdOficio := 0;
  IdImagem := 0;

  if vgOperacao = C then
    Exit;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    if dmOficio.cdsOficio_F.State in [dsInsert, dsEdit] then
    begin
      if vgOperacao = I then
      begin
        IdOficio := BS.ProximoId('ID_OFICIO', 'OFICIO');

        if TpOfc = ENV then
        begin
          if IdPrimeiroOficio > 0 then
            dmOficio.cdsOficio_F.FieldByName('COD_OFICIO').AsInteger := IdPrimeiroOficio
          else
            dmOficio.cdsOficio_F.FieldByName('COD_OFICIO').AsInteger := dmGerencial.ProximoCodigoOficio;

          dmOficio.cdsOficio_F.FieldByName('ANO_OFICIO').AsInteger := YearOf(Date);
        end;
      end
      else
        IdOficio := dmOficio.cdsOficio_F.FieldByName('ID_OFICIO').AsInteger;

      dmOficio.cdsOficio_F.FieldByName('ID_OFICIO').AsInteger := IdOficio;

      dmOficio.cdsOficio_F.Post;
      dmOficio.cdsOficio_F.ApplyUpdates(0);

      if TpOfc = ENV then
        sMsgTela := 'Controle: ' + dmOficio.cdsOficio_F.FieldByName('ID_OFICIO').AsString + #13#10 +
                    'Of�cio: ' + dmOficio.cdsOficio_F.FieldByName('COD_OFICIO').AsString + '/' +
                                 dmOficio.cdsOficio_F.FieldByName('ANO_OFICIO').AsString + #13#10 +
                    'Data: ' + dmOficio.cdsOficio_F.FieldByName('DATA_OFICIO').AsString + #13#10 +
                    'Hora: ' + dmOficio.cdsOficio_F.FieldByName('HORA_OFICIO').AsString + #13#10 +
                    'Assunto: ' + dmOficio.cdsOficio_F.FieldByName('ASSUNTO_OFICIO').AsString
      else if TpOfc = RCB then
        sMsgTela := 'Controle: ' + dmOficio.cdsOficio_F.FieldByName('ID_OFICIO').AsString + #13#10 +
                    'Data: ' + dmOficio.cdsOficio_F.FieldByName('DATA_OFICIO').AsString + #13#10 +
                    'Hora: ' + dmOficio.cdsOficio_F.FieldByName('HORA_OFICIO').AsString + #13#10 +
                    'Assunto: ' + dmOficio.cdsOficio_F.FieldByName('ASSUNTO_OFICIO').AsString;
    end;

    { IMAGENS }
    if dmOficio.cdsOficioImg_F.RecordCount > 0 then
    begin
      IdImagem := BS.ProximoId('ID_OFICIO_IMG', 'OFICIO_IMG');

      dmOficio.cdsOficioImg_F.First;

      while not dmOficio.cdsOficioImg_F.Eof do
      begin
        dmOficio.cdsOficioImg_F.Edit;

        if dmOficio.cdsOficioImg_F.FieldByName('NOVO').AsBoolean then
        begin
          dmOficio.cdsOficioImg_F.FieldByName('ID_OFICIO_IMG').AsInteger := IdImagem;
          Inc(IdImagem);
        end;

        if Copy(dmOficio.cdsOficioImg_F.FieldByName('NOME_FIXO').AsString, 1, 1) = '0' then
          dmOficio.cdsOficioImg_F.FieldByName('NOME_FIXO').AsString  := IntToStr(IdOficio) +
                                                                        Copy(dmOficio.cdsOficioImg_F.FieldByName('NOME_FIXO').AsString,
                                                                             2,
                                                                             (Length(dmOficio.cdsOficioImg_F.FieldByName('NOME_FIXO').AsString) - 1));

        dmOficio.cdsOficioImg_F.FieldByName('ID_OFICIO_FK').AsInteger  := IdOficio;
        dmOficio.cdsOficioImg_F.Post;

        dmOficio.cdsOficioImg_F.Next;
      end;
    end;

    if dmOficio.cdsOficioImg_F.RecordCount > 0 then
    begin
      dmOficio.cdsOficioImg_F.First;

      while not dmOficio.cdsOficioImg_F.Eof do
      begin
        if not dmOficio.cdsOficioImg_F.FieldByName('NOME_V_ANTIGO').IsNull then
        begin
          if FileExists(vgConf_DiretorioImagens +
                        IntToStr(vgOfc_Ano) +
                        '\' + dmOficio.cdsOficioImg_F.FieldByName('NOME_FIXO').AsString +
                        dmOficio.cdsOficioImg_F.FieldByName('NOME_V_ANTIGO').AsString + '.' +
                        dmOficio.cdsOficioImg_F.FieldByName('EXTENSAO').AsString) then
          begin
            DeleteFile(vgConf_DiretorioImagens +
                        IntToStr(vgOfc_Ano) +
                       '\' + dmOficio.cdsOficioImg_F.FieldByName('NOME_FIXO').AsString +
                       dmOficio.cdsOficioImg_F.FieldByName('NOME_V_ANTIGO').AsString + '.' +
                       dmOficio.cdsOficioImg_F.FieldByName('EXTENSAO').AsString);
          end;
        end;

        if not dmOficio.cdsOficioImg_F.FieldByName('ORIGEM_ANTIGA').IsNull then
        begin
          if FileExists(dmOficio.cdsOficioImg_F.FieldByName('ORIGEM_ANTIGA').AsString) then
            DeleteFile(dmOficio.cdsOficioImg_F.FieldByName('ORIGEM_ANTIGA').AsString);
        end;

        dmOficio.cdsOficioImg_F.Next;
      end;

      dmOficio.cdsOficioImg_F.ApplyUpdates(0);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    if TpOfc = ENV then
    begin
      Application.MessageBox(PChar(sMsgTela),
                             PChar(Self.Caption),
                             MB_OK + MB_ICONINFORMATION);
    end
    else if TpOfc = RCB then
    begin
      Application.MessageBox(PChar(sMsgTela),
                             PChar(Self.Caption),
                             MB_OK + MB_ICONINFORMATION);
    end;

    Msg := 'Of�cio gravado com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na grava��o do Of�cio.';
  end;
end;

procedure TFCadastroOficio.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(3, '', 'Somente consulta de ' + Self.Caption);
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(3, '', 'Somente inclus�o de ' + Self.Caption);
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO',
                                  'Por favor, indique o motivo da edi��o desse ' + Self.Caption + ':',
                                  '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de ' + Self.Caption;

      BS.GravarUsuarioLog(3, '', sObservacao);
    end;
  end;
end;

procedure TFCadastroOficio.HabDesabBotoesCRUD(Inc, Ed, Exc: Boolean);
begin
  btnAcionarScanner.Enabled   := Inc;
  btnAbrirPastaImagem.Enabled := Inc;
  btnColarImagem.Enabled      := Inc;
  btnAssociarPDF.Enabled      := Inc;
  btnEditarImagem.Enabled     := Ed;
  btnExcluirImagem.Enabled    := Exc;
  edtNomeArquivo.Enabled      := not (Inc or Ed or Exc);
  btnConfirmarAcao.Enabled    := not (Inc or Ed or Exc);
  btnCancelarAcao.Enabled     := not (Inc or Ed or Exc);
end;

procedure TFCadastroOficio.ipnlHistoricoOficiosItemClick(Sender: TObject;
  ItemIndex: Integer);
var
  Op: TOperacao;
  OrigF, OrigC: Boolean;
  CodOfc, AnoOfc: Integer;
  TpOfc, StaOfc: String;
begin
  inherited;

  if dmGerencial.PegarNumero(ipnlHistoricoOficios.Items[ItemIndex]) = dmOficio.cdsOficio_F.FieldByName('ID_OFICIO').AsInteger then
    Exit;

  Op    := vgOperacao;
  OrigF := vgOrigemFiltro;
  OrigC := vgOrigemCadastro;

  vgOperacao       := C;
  vgOrigemFiltro   := False;
  vgOrigemCadastro := True;

  CodOfc := vgOfc_Codigo;
  AnoOfc := vgOfc_Ano;
  TpOfc  := vgOfc_TpOfc;
  StaOfc := vgOfc_Status;

  try
    Application.CreateForm(TFConsultaOficio, FConsultaOficio);

    FConsultaOficio.IdOficio := dmGerencial.PegarNumero(ipnlHistoricoOficios.Items[ItemIndex]);

    FConsultaOficio.ShowModal;
  finally
    FConsultaOficio.Free;
  end;

  vgOperacao       := Op;
  vgOrigemFiltro   := OrigF;
  vgOrigemCadastro := OrigC;

  vgOfc_Codigo := CodOfc;
  vgOfc_Ano    := AnoOfc;
  vgOfc_TpOfc  := TpOfc;
  vgOfc_Status := StaOfc;
end;

procedure TFCadastroOficio.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(ntbCadastro);
end;

procedure TFCadastroOficio.lcbFuncionarioKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if Application.MessageBox('Deseja incluir alguma Imagem?', 'Confirma��o', MB_YESNO + MB_ICONQUESTION) = ID_YES then
      btnAbaDigitalizacao.Click
    else
      btnOk.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroOficio.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;

  case Aba of
    PRINC:
    begin
      ntbCadastro.PageIndex := 0;

      if dteDataOficio.CanFocus then
        dteDataOficio.SetFocus;
    end;
    DIG:
    begin
      ntbCadastro.PageIndex := 1;
    end;
  end;

  btnDadosPrincipais.Transparent  := not (Aba = PRINC);
  btnAbaDigitalizacao.Transparent := not (Aba = DIG);
end;

procedure TFCadastroOficio.mmObservacaoKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if TpOfc = RCB then
      SelectNext(ActiveControl, True, True)
    else
    begin
      if vgOperacao in [I, E] then
      begin
        if Application.MessageBox('Deseja incluir alguma Imagem?', 'Confirma��o', MB_YESNO + MB_ICONQUESTION) = ID_YES then
          btnAbaDigitalizacao.Click
        else
          btnOk.Click;
      end
      else
        SelectNext(ActiveControl, True, True);
    end;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

function TFCadastroOficio.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if dteDataOficio.Date = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a DATA DO OF�CIO. (Aba: Of�cio)';
    DadosMsgErro.Componente := dteDataOficio;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(medHoraOficio.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar a HORA DO OF�CIO. (Aba: Of�cio)';
    DadosMsgErro.Componente := medHoraOficio;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if TpOfc = RCB then
  begin
    if Trim(edtCodOficio.Text) = '' then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar o C�DIGO DO OF�CIO. (Aba: Of�cio)';
      DadosMsgErro.Componente := edtCodOficio;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;

    if Trim(edtAnoOficio.Text) = '' then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar o ANO DO OF�CIO. (Aba: Of�cio)';
      DadosMsgErro.Componente := edtAnoOficio;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;
  end;

  if Trim(edtAssunto.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar o ASSUNTO DO OF�CIO. (Aba: Of�cio)';
    DadosMsgErro.Componente := edtAssunto;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if not chbReservado.Checked then
  begin
    if Trim(lcbPessoa.Text) = '' then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar o ' +
                                 IfThen((TpOfc = ENV),
                                         'DESTINAT�RIO',
                                         IfThen((TpOfc = RCB),
                                                'REMETENTE',
                                                ''))
                                 + ' DO OF�CIO. (Aba: Of�cio)';
      DadosMsgErro.Componente := lcbPessoa;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;

  {  if Trim(edtNumProcesso.Text) = '' then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar o N� DE PROCESSO DO OF�CIO. (Aba: Of�cio)';
      DadosMsgErro.Componente := edtNumProcesso;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;  }

    if Trim(lcbOrgao.Text) = '' then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar o �RG�O DO OF�CIO. (Aba: Of�cio)';
      DadosMsgErro.Componente := lcbOrgao;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;

  {  if Trim(edtNumProtocolo.Text) = '' then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar o N� DE PROTOCOLO DO OF�CIO. (Aba: Of�cio)';
      DadosMsgErro.Componente := edtNumProtocolo;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;  }

    if Trim(lcbFormaEnvio.Text) = '' then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar a FORMA DE ' +
                                 IfThen((TpOfc = ENV),
                                         'ENVIO',
                                         IfThen((TpOfc = RCB),
                                                'RECEBIMENTO',
                                                ''))
                                 + ' DO OF�CIO. (Aba: Of�cio)';
      DadosMsgErro.Componente := lcbFormaEnvio;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;

    if (Trim(lcbFuncionario.Text) = '') and
      (vgUsu_FlgSuporte = 'N') then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar o NOME DO COLABORADOR QUE ' +
                                 IfThen((TpOfc = ENV),
                                         IfThen((vgOfc_Status = 'R'),
                                                'ENVIAR�',
                                                'ENVIOU'),
                                         IfThen((TpOfc = RCB),
                                                'RECEBEU',
                                                ''))
                                 + ' DO OF�CIO. (Aba: Of�cio)';
      DadosMsgErro.Componente := lcbFuncionario;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroOficio.VerificarLayoutAto(var QtdErros: Integer): Boolean;
begin
  //
end;

initialization
  TPicture.UnRegisterGraphicClass(TdxBMPImage);
  TPicture.UnRegisterGraphicClass(TdxPNGImage);
  TPicture.UnRegisterGraphicClass(TdxSmartImage);
  TPicture.RegisterFileFormat('BMP', 'Imagens BMP', TdxBMPImage);
  TPicture.RegisterFileFormat('PNG', 'Imagens PNG', TdxPNGImage);
  TPicture.RegisterFileFormat('JPG;*.JPEG;*.TIF;*.GIF;*.PNG;*.BMP', 'Todas as imagens', TdxSmartImage);

end.
