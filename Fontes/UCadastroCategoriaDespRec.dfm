inherited FCadastroCategoriaDespRec: TFCadastroCategoriaDespRec
  Caption = 'Cadastro de Categoria de Lan'#231'amento'
  ClientHeight = 189
  ClientWidth = 483
  ExplicitWidth = 489
  ExplicitHeight = 218
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 477
    Height = 183
    ExplicitWidth = 477
    ExplicitHeight = 183
    inherited pnlDados: TPanel
      Width = 363
      Height = 177
      ExplicitWidth = 363
      ExplicitHeight = 177
      object lblNome: TLabel [0]
        Left = 0
        Top = 72
        Width = 104
        Height = 14
        Caption = 'Nome da Categoria'
      end
      object lblNomeNatureza: TLabel [1]
        Left = 0
        Top = 30
        Width = 101
        Height = 14
        Caption = 'Nome da Natureza'
      end
      inherited pnlMsgErro: TPanel
        Top = 116
        Width = 363
        TabOrder = 2
        ExplicitTop = 116
        ExplicitWidth = 363
        inherited lblMensagemErro: TLabel
          Width = 363
        end
        inherited lbMensagemErro: TListBox
          Width = 363
          OnDblClick = lbMensagemErroDblClick
          ExplicitWidth = 363
        end
      end
      object edtNome: TDBEdit
        Left = 0
        Top = 88
        Width = 361
        Height = 22
        CharCase = ecUpperCase
        Color = 16114127
        DataField = 'DESCR_CATEGORIA_DESPREC'
        DataSource = dsCadastro
        MaxLength = 250
        TabOrder = 0
        OnKeyPress = edtNomeKeyPress
      end
      object rgTipo: TDBRadioGroup
        Left = 208
        Top = 0
        Width = 153
        Height = 40
        Caption = 'Tipo'
        Columns = 2
        DataField = 'TIPO'
        DataSource = dsCadastro
        Enabled = False
        Items.Strings = (
          'Despesa'
          'Receita')
        TabOrder = 1
      end
      object lcbNomeNatureza: TDBLookupComboBox
        Left = 0
        Top = 46
        Width = 361
        Height = 22
        Color = 16114127
        DataField = 'ID_NATUREZA_FK'
        DataSource = dsCadastro
        KeyField = 'ID_NATUREZA'
        ListField = 'DESCR_NATUREZA'
        ListSource = dsNatureza
        TabOrder = 3
        OnKeyPress = FormKeyPress
      end
    end
    inherited pnlMenu: TPanel
      Height = 177
      ExplicitHeight = 177
      inherited btnDadosPrincipais: TJvTransparentButton
        Visible = False
      end
      inherited btnOk: TJvTransparentButton
        Top = 87
        ExplicitTop = 59
      end
      inherited btnCancelar: TJvTransparentButton
        Top = 132
        ExplicitTop = 104
      end
    end
  end
  inherited dsCadastro: TDataSource
    DataSet = cdsCategoria
    Left = 414
    Top = 132
  end
  object qryCategoria: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM CATEGORIA_DESPREC'
      ' WHERE ID_CATEGORIA_DESPREC = :ID_CATEGORIA_DESPREC')
    Left = 186
    Top = 135
    ParamData = <
      item
        Position = 1
        Name = 'ID_CATEGORIA_DESPREC'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspCategoria: TDataSetProvider
    DataSet = qryCategoria
    Left = 260
    Top = 134
  end
  object cdsCategoria: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_CATEGORIA_DESPREC'
        ParamType = ptInput
      end>
    ProviderName = 'dspCategoria'
    Left = 340
    Top = 134
  end
  object qryNatureza: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM NATUREZA'
      ' WHERE TIPO_NATUREZA = :TIPO_NATUREZA'
      'ORDER BY ID_NATUREZA')
    Left = 42
    Top = 38
    ParamData = <
      item
        Name = 'TIPO_NATUREZA'
        DataType = ftString
        ParamType = ptInput
      end>
  end
  object dsNatureza: TDataSource
    DataSet = qryNatureza
    Left = 42
    Top = 89
  end
end
