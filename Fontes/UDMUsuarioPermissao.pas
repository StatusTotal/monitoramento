{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UDMUsuarioPermissao.pas
  Descricao:   Data Module do cadastro de Permissoes de Usuario
  Author   :   Cristina
  Date:        15-fev-2016
  Last Update: 27-set-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UDMUsuarioPermissao;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TdmUsuarioPermissao = class(TDataModule)
    qryFinOrigem: TFDQuery;
    qryOficOrigem: TFDQuery;
    qryRelOrigem: TFDQuery;
    qryConfigOrigem: TFDQuery;
    dspFinOrigem: TDataSetProvider;
    dspOficOrigem: TDataSetProvider;
    dspRelOrigem: TDataSetProvider;
    dspConfigOrigem: TDataSetProvider;
    cdsFinOrigem: TClientDataSet;
    cdsOficOrigem: TClientDataSet;
    cdsRelOrigem: TClientDataSet;
    cdsConfigOrigem: TClientDataSet;
    dsFinOrigem: TDataSource;
    dsOficOrigem: TDataSource;
    dsRelOrigem: TDataSource;
    dsConfigOrigem: TDataSource;
    cdsFinOrigemID_PERMISSAO: TIntegerField;
    cdsFinOrigemID_MODULO_FK: TIntegerField;
    cdsFinOrigemDESCR_PERMISSAO: TStringField;
    cdsOficOrigemID_PERMISSAO: TIntegerField;
    cdsOficOrigemID_MODULO_FK: TIntegerField;
    cdsOficOrigemDESCR_PERMISSAO: TStringField;
    cdsRelOrigemID_PERMISSAO: TIntegerField;
    cdsRelOrigemID_MODULO_FK: TIntegerField;
    cdsRelOrigemDESCR_PERMISSAO: TStringField;
    cdsConfigOrigemID_PERMISSAO: TIntegerField;
    cdsConfigOrigemID_MODULO_FK: TIntegerField;
    cdsConfigOrigemDESCR_PERMISSAO: TStringField;
    cdsRelOrigemCOD_PERMISSAO: TIntegerField;
    cdsConfigOrigemCOD_PERMISSAO: TIntegerField;
    cdsOficOrigemCOD_PERMISSAO: TIntegerField;
    cdsFinOrigemCOD_PERMISSAO: TIntegerField;
    qryFlsOrigem: TFDQuery;
    qryEtqOrigem: TFDQuery;
    dspFlsOrigem: TDataSetProvider;
    dspEtqOrigem: TDataSetProvider;
    cdsFlsOrigem: TClientDataSet;
    cdsEtqOrigem: TClientDataSet;
    dsFlsOrigem: TDataSource;
    dsEtqOrigem: TDataSource;
    cdsFlsOrigemID_PERMISSAO: TIntegerField;
    cdsFlsOrigemCOD_PERMISSAO: TIntegerField;
    cdsFlsOrigemID_MODULO_FK: TIntegerField;
    cdsFlsOrigemDESCR_PERMISSAO: TStringField;
    cdsEtqOrigemID_PERMISSAO: TIntegerField;
    cdsEtqOrigemCOD_PERMISSAO: TIntegerField;
    cdsEtqOrigemID_MODULO_FK: TIntegerField;
    cdsEtqOrigemDESCR_PERMISSAO: TStringField;
    cdsFinOrigemCOD_PERMISSAO_UP: TIntegerField;
    cdsFinOrigemSELECIONADO: TIntegerField;
    cdsOficOrigemCOD_PERMISSAO_UP: TIntegerField;
    cdsOficOrigemSELECIONADO: TIntegerField;
    cdsFlsOrigemCOD_PERMISSAO_UP: TIntegerField;
    cdsFlsOrigemSELECIONADO: TIntegerField;
    cdsEtqOrigemCOD_PERMISSAO_UP: TIntegerField;
    cdsEtqOrigemSELECIONADO: TIntegerField;
    cdsRelOrigemCOD_PERMISSAO_UP: TIntegerField;
    cdsRelOrigemSELECIONADO: TIntegerField;
    cdsConfigOrigemCOD_PERMISSAO_UP: TIntegerField;
    cdsConfigOrigemSELECIONADO: TIntegerField;
    procedure cdsFinOrigemAfterOpen(DataSet: TDataSet);
    procedure cdsOficOrigemAfterOpen(DataSet: TDataSet);
    procedure cdsFlsOrigemAfterOpen(DataSet: TDataSet);
    procedure cdsEtqOrigemAfterOpen(DataSet: TDataSet);
    procedure cdsRelOrigemAfterOpen(DataSet: TDataSet);
    procedure cdsConfigOrigemAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmUsuPrm: TdmUsuarioPermissao;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses UDM, UGDM, UBibliotecaSistema, UVariaveisGlobais;

{$R *.dfm}

procedure TdmUsuarioPermissao.cdsConfigOrigemAfterOpen(DataSet: TDataSet);
begin
  cdsConfigOrigem.First;

  while not cdsConfigOrigem.Eof do
  begin
    cdsConfigOrigem.Edit;

    if cdsConfigOrigem.FieldByName('COD_PERMISSAO_UP').IsNull then
      cdsConfigOrigem.FieldByName('SELECIONADO').AsInteger := 0
    else
      cdsConfigOrigem.FieldByName('SELECIONADO').AsInteger := 1;

    cdsConfigOrigem.Post;

    cdsConfigOrigem.Next;
  end;
end;

procedure TdmUsuarioPermissao.cdsEtqOrigemAfterOpen(DataSet: TDataSet);
begin
  cdsEtqOrigem.First;

  while not cdsEtqOrigem.Eof do
  begin
    cdsEtqOrigem.Edit;

    if cdsEtqOrigem.FieldByName('COD_PERMISSAO_UP').IsNull then
      cdsEtqOrigem.FieldByName('SELECIONADO').AsInteger := 0
    else
      cdsEtqOrigem.FieldByName('SELECIONADO').AsInteger := 1;

    cdsEtqOrigem.Post;

    cdsEtqOrigem.Next;
  end;
end;

procedure TdmUsuarioPermissao.cdsFinOrigemAfterOpen(DataSet: TDataSet);
begin
  cdsFinOrigem.First;

  while not cdsFinOrigem.Eof do
  begin
    cdsFinOrigem.Edit;

    if cdsFinOrigem.FieldByName('COD_PERMISSAO_UP').IsNull then
      cdsFinOrigem.FieldByName('SELECIONADO').AsInteger := 0
    else
      cdsFinOrigem.FieldByName('SELECIONADO').AsInteger := 1;

    cdsFinOrigem.Post;

    cdsFinOrigem.Next;
  end;
end;

procedure TdmUsuarioPermissao.cdsFlsOrigemAfterOpen(DataSet: TDataSet);
begin
  cdsFlsOrigem.First;

  while not cdsFlsOrigem.Eof do
  begin
    cdsFlsOrigem.Edit;

    if cdsFlsOrigem.FieldByName('COD_PERMISSAO_UP').IsNull then
      cdsFlsOrigem.FieldByName('SELECIONADO').AsInteger := 0
    else
      cdsFlsOrigem.FieldByName('SELECIONADO').AsInteger := 1;

    cdsFlsOrigem.Post;

    cdsFlsOrigem.Next;
  end;
end;

procedure TdmUsuarioPermissao.cdsOficOrigemAfterOpen(DataSet: TDataSet);
begin
  cdsOficOrigem.First;

  while not cdsOficOrigem.Eof do
  begin
    cdsOficOrigem.Edit;

    if cdsOficOrigem.FieldByName('COD_PERMISSAO_UP').IsNull then
      cdsOficOrigem.FieldByName('SELECIONADO').AsInteger := 0
    else
      cdsOficOrigem.FieldByName('SELECIONADO').AsInteger := 1;

    cdsOficOrigem.Post;

    cdsOficOrigem.Next;
  end;
end;

procedure TdmUsuarioPermissao.cdsRelOrigemAfterOpen(DataSet: TDataSet);
begin
  cdsRelOrigem.First;

  while not cdsRelOrigem.Eof do
  begin
    cdsRelOrigem.Edit;

    if cdsRelOrigem.FieldByName('COD_PERMISSAO_UP').IsNull then
      cdsRelOrigem.FieldByName('SELECIONADO').AsInteger := 0
    else
      cdsRelOrigem.FieldByName('SELECIONADO').AsInteger := 1;

    cdsRelOrigem.Post;

    cdsRelOrigem.Next;
  end;
end;

end.
