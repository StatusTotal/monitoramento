inherited FFiltroRelatorioRecibosCancelados: TFFiltroRelatorioRecibosCancelados
  Caption = 'Relat'#243'rio de Recibos Cancelados'
  ClientHeight = 177
  ExplicitHeight = 206
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlFiltro: TPanel
    Height = 171
    ExplicitHeight = 201
    object lblPeriodo: TLabel [0]
      Left = 5
      Top = 8
      Width = 130
      Height = 14
      Caption = 'Per'#237'odo de Lan'#231'amento'
    end
    object lblPeriodoInicio: TLabel [1]
      Left = 5
      Top = 31
      Width = 32
      Height = 14
      Caption = 'In'#237'cio:'
    end
    object lblPeriodoFim: TLabel [2]
      Left = 149
      Top = 31
      Width = 22
      Height = 14
      Caption = 'Fim:'
    end
    inherited pnlBotoes: TPanel
      Top = 124
      TabOrder = 6
      ExplicitTop = 154
      inherited btnLimpar: TJvTransparentButton
        OnClick = btnLimparClick
      end
    end
    inherited gbTipo: TGroupBox
      Top = 56
      TabOrder = 2
      ExplicitTop = 56
    end
    inherited chbExibirGrafico: TCheckBox
      Top = 58
      TabOrder = 3
      ExplicitTop = 58
    end
    inherited chbExportarPDF: TCheckBox
      Top = 81
      TabOrder = 4
      ExplicitTop = 81
    end
    inherited chbExportarExcel: TCheckBox
      Top = 104
      TabOrder = 5
      ExplicitTop = 104
    end
    object dteDataInicio: TJvDateEdit
      Left = 43
      Top = 28
      Width = 100
      Height = 22
      Color = 16114127
      ShowNullDate = False
      TabOrder = 0
      OnKeyPress = FormKeyPress
    end
    object dteDataFim: TJvDateEdit
      Left = 177
      Top = 28
      Width = 100
      Height = 22
      Color = 16114127
      ShowNullDate = False
      TabOrder = 1
      OnKeyPress = FormKeyPress
    end
  end
end
