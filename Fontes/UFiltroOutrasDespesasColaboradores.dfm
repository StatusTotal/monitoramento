inherited FFiltroOutrasDespesasColaboradores: TFFiltroOutrasDespesasColaboradores
  Caption = 'Filtro de Outros Lan'#231'amentos com Colaboradores'
  ClientHeight = 394
  ClientWidth = 959
  OnCreate = FormCreate
  ExplicitWidth = 965
  ExplicitHeight = 423
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 953
    Height = 388
    ExplicitWidth = 953
    ExplicitHeight = 388
    inherited pnlGrid: TPanel
      Top = 97
      Width = 869
      ExplicitTop = 97
      ExplicitWidth = 869
      inherited pnlGridPai: TPanel
        Width = 869
        ExplicitWidth = 869
        inherited dbgGridPaiPadrao: TDBGrid
          Width = 869
          OnTitleClick = dbgGridPaiPadraoTitleClick
          Columns = <
            item
              Expanded = False
              FieldName = 'ID'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'DATA_OUTRADESPESA_FUNC'
              Title.Caption = 'Data'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_FUNCIONARIO_FK'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'ID_TIPO_OUTRADESP_FUNC_FK'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'NOMEFUNCIONARIO'
              Title.Caption = 'Nome Colaborador'
              Width = 270
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VR_OUTRADESPESA_FUNC'
              Title.Caption = 'Vlr. Lan'#231'amento'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DESCR_OUTRADESPESA_FUNC'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'TIPODESPESA'
              Title.Caption = 'Tipo Lan'#231'amento'
              Width = 205
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATA_CADASTRO'
              Title.Caption = 'Dt. Cadastro'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'CAD_ID_USUARIO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'DATA_QUITACAO'
              Title.Caption = 'Dt. Quita'#231#227'o'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QUIT_ID_USUARIO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'FLG_CANCELADO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'DATA_CANCELAMENTO'
              Title.Caption = 'Dt. Cancel.'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CANCEL_ID_USUARIO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'FLG_MODALIDADE'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'DESCR_MODALIDADE'
              Title.Caption = 'Modalidade de Lan'#231'amento'
              Width = 156
              Visible = True
            end>
        end
      end
    end
    inherited pnlFiltro: TPanel
      Width = 953
      Height = 94
      ExplicitWidth = 953
      ExplicitHeight = 94
      inherited btnFiltrar: TJvTransparentButton
        Left = 867
        Top = 64
        ExplicitLeft = 867
        ExplicitTop = 64
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 867
        Top = 34
        ExplicitLeft = 867
        ExplicitTop = 34
      end
      object lblNomeFuncionario: TLabel
        Left = 81
        Top = 52
        Width = 100
        Height = 14
        Caption = 'Nome Colaborador'
      end
      object lblPeriodoCadastro: TLabel
        Left = 81
        Top = 8
        Width = 110
        Height = 14
        Caption = 'Per'#237'odo de Cadastro'
      end
      object Label3: TLabel
        Left = 187
        Top = 27
        Width = 6
        Height = 14
        Caption = 'a'
      end
      object lblTipoODespF: TLabel
        Left = 488
        Top = 52
        Width = 95
        Height = 14
        Caption = 'Tipo Lan'#231'amento'
      end
      object lcbNomeFuncionario: TDBLookupComboBox
        Left = 81
        Top = 68
        Width = 401
        Height = 22
        Color = 16114127
        KeyField = 'ID_FUNCIONARIO'
        ListField = 'NOME_FUNCIONARIO'
        ListSource = dsFuncionarios
        NullValueKey = 16460
        TabOrder = 2
        OnKeyPress = lcbNomeFuncionarioKeyPress
      end
      object dteIniPeriodoCadastro: TJvDateEdit
        Left = 81
        Top = 24
        Width = 100
        Height = 22
        Color = 16114127
        ShowNullDate = False
        TabOrder = 0
        OnKeyPress = dteIniPeriodoCadastroKeyPress
      end
      object dteFimPeriodoCadastro: TJvDateEdit
        Left = 199
        Top = 24
        Width = 100
        Height = 22
        Color = 16114127
        ShowNullDate = False
        TabOrder = 1
        OnKeyPress = dteFimPeriodoCadastroKeyPress
      end
      object rgSituacaoVale: TRadioGroup
        Left = 760
        Top = 8
        Width = 101
        Height = 82
        Caption = 'Situa'#231#227'o'
        ItemIndex = 0
        Items.Strings = (
          'Todas'
          'N'#227'o quitado'
          'Quitado')
        TabOrder = 4
        OnExit = rgSituacaoValeExit
      end
      object lcbTipoODespF: TDBLookupComboBox
        Left = 488
        Top = 68
        Width = 266
        Height = 22
        Color = 16114127
        KeyField = 'ID_TIPO_OUTRADESP_FUNC'
        ListField = 'DESCR_TIPO_OUTRADESP_FUNC'
        ListSource = dsTipoODespF
        NullValueKey = 16460
        TabOrder = 3
        OnKeyPress = lcbTipoODespFKeyPress
      end
    end
    inherited pnlMenu: TPanel
      Top = 97
      ExplicitTop = 97
    end
  end
  inherited dsGridPaiPadrao: TDataSource
    DataSet = cdsGridPaiPadrao
    Left = 884
    Top = 315
  end
  inherited qryGridPaiPadrao: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT ID_OUTRADESPESA_FUNC AS ID,'
      '       ID_FUNCIONARIO_FK,'
      '       ID_TIPO_OUTRADESP_FUNC_FK,'
      '       DATA_OUTRADESPESA_FUNC,'
      '       VR_OUTRADESPESA_FUNC,'
      '       DESCR_OUTRADESPESA_FUNC,'
      '       ID_FECHAMENTO_SALARIO_FK,'
      '       DATA_CADASTRO,'
      '       CAD_ID_USUARIO,'
      '       DATA_QUITACAO,'
      '       QUIT_ID_USUARIO,'
      '       FLG_CANCELADO,'
      '       DATA_CANCELAMENTO,'
      '       CANCEL_ID_USUARIO,'
      '       FLG_MODALIDADE'
      '  FROM OUTRADESPESA_FUNC'
      
        ' WHERE ID_FUNCIONARIO_FK = (CASE WHEN (:ID_FUNC01 = 0) THEN ID_F' +
        'UNCIONARIO_FK ELSE :ID_FUNC02 END)'
      
        '   AND ID_TIPO_OUTRADESP_FUNC_FK = (CASE WHEN (:ID_ODESPF01 = 0)' +
        ' THEN ID_TIPO_OUTRADESP_FUNC_FK ELSE :ID_ODESPF02 END)'
      '   AND DATA_OUTRADESPESA_FUNC BETWEEN :DATA_INI AND :DATA_FIM'
      
        'ORDER BY ID_FUNCIONARIO_FK ASC, ID_TIPO_OUTRADESP_FUNC_FK, DATA_' +
        'OUTRADESPESA_FUNC DESC')
    Left = 884
    Top = 159
    ParamData = <
      item
        Position = 1
        Name = 'ID_FUNC01'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'ID_FUNC02'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 3
        Name = 'ID_ODESPF01'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 4
        Name = 'ID_ODESPF02'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 5
        Name = 'DATA_INI'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Position = 6
        Name = 'DATA_FIM'
        DataType = ftDate
        ParamType = ptInput
      end>
  end
  object dspGridPaiPadrao: TDataSetProvider
    DataSet = qryGridPaiPadrao
    Left = 884
    Top = 215
  end
  object cdsGridPaiPadrao: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftUnknown
        Name = 'ID_FUNC01'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'ID_FUNC02'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'ID_ODESPF01'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'ID_ODESPF02'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'DATA_INI'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'DATA_FIM'
        ParamType = ptInput
      end>
    ProviderName = 'dspGridPaiPadrao'
    OnCalcFields = cdsGridPaiPadraoCalcFields
    Left = 884
    Top = 263
    object cdsGridPaiPadraoID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object cdsGridPaiPadraoID_FUNCIONARIO_FK: TIntegerField
      FieldName = 'ID_FUNCIONARIO_FK'
    end
    object cdsGridPaiPadraoID_TIPO_OUTRADESP_FUNC_FK: TIntegerField
      FieldName = 'ID_TIPO_OUTRADESP_FUNC_FK'
    end
    object cdsGridPaiPadraoDATA_OUTRADESPESA_FUNC: TDateField
      FieldName = 'DATA_OUTRADESPESA_FUNC'
    end
    object cdsGridPaiPadraoVR_OUTRADESPESA_FUNC: TBCDField
      FieldName = 'VR_OUTRADESPESA_FUNC'
      OnGetText = cdsGridPaiPadraoVR_OUTRADESPESA_FUNCGetText
      Precision = 18
      Size = 2
    end
    object cdsGridPaiPadraoDESCR_OUTRADESPESA_FUNC: TStringField
      FieldName = 'DESCR_OUTRADESPESA_FUNC'
      Size = 1000
    end
    object cdsGridPaiPadraoDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsGridPaiPadraoCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsGridPaiPadraoDATA_QUITACAO: TDateField
      FieldName = 'DATA_QUITACAO'
    end
    object cdsGridPaiPadraoQUIT_ID_USUARIO: TIntegerField
      FieldName = 'QUIT_ID_USUARIO'
    end
    object cdsGridPaiPadraoFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsGridPaiPadraoDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsGridPaiPadraoCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsGridPaiPadraoNOMEFUNCIONARIO: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEFUNCIONARIO'
      Size = 250
    end
    object cdsGridPaiPadraoTIPODESPESA: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'TIPODESPESA'
      Size = 200
    end
    object cdsGridPaiPadraoID_FECHAMENTO_SALARIO_FK: TIntegerField
      FieldName = 'ID_FECHAMENTO_SALARIO_FK'
    end
    object cdsGridPaiPadraoFLG_MODALIDADE: TStringField
      FieldName = 'FLG_MODALIDADE'
      FixedChar = True
      Size = 1
    end
    object cdsGridPaiPadraoDESCR_MODALIDADE: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCR_MODALIDADE'
      Size = 150
    end
  end
  object qryFuncionarios: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT ID_FUNCIONARIO, NOME_FUNCIONARIO'
      '  FROM FUNCIONARIO'
      ' WHERE NOME_FUNCIONARIO <> '#39'FUNCION'#193'RIO'#39
      'ORDER BY NOME_FUNCIONARIO')
    Left = 140
    Top = 167
  end
  object dsFuncionarios: TDataSource
    DataSet = qryFuncionarios
    Left = 140
    Top = 215
  end
  object qryTipoODespF: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM TIPO_OUTRADESP_FUNC'
      'ORDER BY DESCR_TIPO_OUTRADESP_FUNC')
    Left = 232
    Top = 168
  end
  object dsTipoODespF: TDataSource
    DataSet = qryTipoODespF
    Left = 232
    Top = 216
  end
end
