{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UOpcoesAcaoBaixaLancGrupo.pas
  Descricao:   Tela de Opcoes de Acao com Sobra ou falta durante baixa de Lancamento Flutuante em Grupo
  Author   :   Cristina
  Date:        07-fev-2017
  Last Update: 30-mar-2017 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UOpcoesAcaoBaixaLancGrupo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UOpcoesAcaoPadrao, JvExControls,
  JvButton, JvTransparentButton, Vcl.ExtCtrls, Vcl.StdCtrls;

type
  TFOpcoesAcaoBaixaLancGrupo = class(TFOpcoesAcaoPadrao)
    pnlValores: TPanel;
    lblSobra: TLabel;
    lblFalta: TLabel;
    btnOpcao3: TJvTransparentButton;
    btnOpcao6: TJvTransparentButton;
    btnOpcao4: TJvTransparentButton;
    btnOpcao5: TJvTransparentButton;
    procedure FormShow(Sender: TObject);
    procedure btnOpcao1Click(Sender: TObject);
    procedure btnOpcao2Click(Sender: TObject);
    procedure btnOpcao3Click(Sender: TObject);
    procedure btnOpcao6Click(Sender: TObject);
    procedure btnOpcao4Click(Sender: TObject);
    procedure btnOpcao5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    sObservacao: String;
  end;

var
  FOpcoesAcaoBaixaLancGrupo: TFOpcoesAcaoBaixaLancGrupo;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMBaixa;

procedure TFOpcoesAcaoBaixaLancGrupo.btnOpcao1Click(Sender: TObject);
begin
  inherited;

  //Observacao de Sobra de Caixa
  dmBaixa.sTipoBaixa := 'O';
  sObservacao := 'Baixa: Sobra de R$' + CurrToStr(dmBaixa.cDifTot);
  Self.Close;
end;

procedure TFOpcoesAcaoBaixaLancGrupo.btnOpcao2Click(Sender: TObject);
begin
  inherited;

  //Observacao de Falta de Caixa
  dmBaixa.sTipoBaixa := 'O';
  sObservacao := 'Baixa: Falta de R$' + CurrToStr(dmBaixa.cDifTot);
  Self.Close;
end;

procedure TFOpcoesAcaoBaixaLancGrupo.btnOpcao3Click(Sender: TObject);
begin
  inherited;

  //Lancamento de Nova Parcela
  dmBaixa.sTipoBaixa := 'P';

  Self.Close;
end;

procedure TFOpcoesAcaoBaixaLancGrupo.btnOpcao4Click(Sender: TObject);
begin
  inherited;

  //Lancamento de Sobra de Caixa
  Self.Close;
end;

procedure TFOpcoesAcaoBaixaLancGrupo.btnOpcao5Click(Sender: TObject);
begin
  inherited;

  //Lancamento de Falta de Caixa
  Self.Close;
end;

procedure TFOpcoesAcaoBaixaLancGrupo.btnOpcao6Click(Sender: TObject);
begin
  inherited;

  //Desmembrar Flutuante
  dmBaixa.sTipoBaixa := 'D';

  Self.Close;
end;

procedure TFOpcoesAcaoBaixaLancGrupo.FormShow(Sender: TObject);
begin
  inherited;

  btnOpcao1.Enabled := (dmBaixa.sTipoBaixa = 'S');  //Observacao de Sobra de Caixa
  btnOpcao6.Enabled := (dmBaixa.sTipoBaixa = 'S');  //Desmembrar Flutuante
  btnOpcao4.Enabled := (dmBaixa.sTipoBaixa = 'S');  //Lancamento de Sobra de Caixa
  btnOpcao3.Enabled := (dmBaixa.sTipoBaixa = 'S');  //Lancamento de Nova Parcela no Flutuante
  btnOpcao2.Enabled := (dmBaixa.sTipoBaixa = 'F');  //Observacao de Falta de Caixa
  btnOpcao5.Enabled := (dmBaixa.sTipoBaixa = 'F');  //Lancamento de Falta de Caixa

  if dmBaixa.sTipoBaixa = 'S' then
    lblSobra.Caption := 'Sobra: R$' + CurrToStr(dmBaixa.cDifTot)
  else if dmBaixa.sTipoBaixa = 'F' then
    lblFalta.Caption := 'Falta: R$' + CurrToStr(dmBaixa.cDifTot);
end;

end.
