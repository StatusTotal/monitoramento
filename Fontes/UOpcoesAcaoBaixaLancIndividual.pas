{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UOpcoesAcaoBaixaLancIndividual.pas
  Descricao:   Tela de Opcoes de Acao com Sobra ou falta durante baixa de Lancamento Flutuante Individual
  Author   :   Cristina
  Date:        06-fev-2017
  Last Update: 30-mar-2017 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UOpcoesAcaoBaixaLancIndividual;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UOpcoesAcaoPadrao, JvExControls,
  JvButton, JvTransparentButton, Vcl.ExtCtrls, Vcl.StdCtrls;

type
  TFOpcoesAcaoBaixaLancIndividual = class(TFOpcoesAcaoPadrao)
    pnlValores: TPanel;
    lblSobra: TLabel;
    lblFalta: TLabel;
    btnOpcao3: TJvTransparentButton;
    btnOpcao4: TJvTransparentButton;
    btnOpcao5: TJvTransparentButton;
    btnOpcao6: TJvTransparentButton;
    procedure FormShow(Sender: TObject);
    procedure btnOpcao1Click(Sender: TObject);
    procedure btnOpcao2Click(Sender: TObject);
    procedure btnOpcao3Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btnOpcao4Click(Sender: TObject);
    procedure btnOpcao5Click(Sender: TObject);
    procedure btnOpcao6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    lAcaoCancelada: Boolean;

    sObservacao: String;
  end;

var
  FOpcoesAcaoBaixaLancIndividual: TFOpcoesAcaoBaixaLancIndividual;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMBaixa;

procedure TFOpcoesAcaoBaixaLancIndividual.btnOpcao1Click(Sender: TObject);
begin
  inherited;

  //Observacao de Sobra de Caixa
  dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString := 'O';
  sObservacao := 'Baixa: Sobra de R$' + dmBaixa.cdsBaixaLancI.FieldByName('VR_DIFERENCA').AsString;
  Self.Close;
end;

procedure TFOpcoesAcaoBaixaLancIndividual.btnOpcao2Click(Sender: TObject);
begin
  inherited;

  //Observacao de Falta de Caixa
  dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString := 'O';
  sObservacao := 'Baixa: Falta de R$' + dmBaixa.cdsBaixaLancI.FieldByName('VR_DIFERENCA').AsString;
  Self.Close;
end;

procedure TFOpcoesAcaoBaixaLancIndividual.btnOpcao3Click(Sender: TObject);
begin
  inherited;

  //Lancamento de Nova Parcela
  dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString := 'P';

  Self.Close;
end;

procedure TFOpcoesAcaoBaixaLancIndividual.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    lAcaoCancelada := True;
    Self.Close;
  end;
end;

procedure TFOpcoesAcaoBaixaLancIndividual.FormShow(Sender: TObject);
begin
  inherited;

  lAcaoCancelada := False;

  btnOpcao4.Enabled := (dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'S');  //Observacao de Sobra de Caixa
  btnOpcao6.Enabled := (dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'S');  //Desmembrar Flutuante
  btnOpcao1.Enabled := (dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'S');  //Lancamento de Sobra de Caixa
  btnOpcao3.Enabled := (dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'S');  //Lancamento de Nova Parcela no Flutuante
  btnOpcao2.Enabled := (dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'F');  //Observacao de Falta de Caixa
  btnOpcao5.Enabled := (dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'F');  //Lancamento de Falta de Caixa

  if dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'S' then
    lblSobra.Caption := 'Sobra: R$' + dmBaixa.cdsBaixaLancI.FieldByName('VR_DIFERENCA').AsString
  else if dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'F' then
    lblFalta.Caption := 'Falta: R$' + dmBaixa.cdsBaixaLancI.FieldByName('VR_DIFERENCA').AsString;
end;

procedure TFOpcoesAcaoBaixaLancIndividual.btnOpcao5Click(
  Sender: TObject);
begin
  inherited;

  //Lancamento de Falta de Caixa
  Self.Close;
end;

procedure TFOpcoesAcaoBaixaLancIndividual.btnOpcao6Click(Sender: TObject);
begin
  inherited;

  //Desmembrar Flutuante
  dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString := 'D';

  Self.Close;
end;

procedure TFOpcoesAcaoBaixaLancIndividual.btnOpcao4Click(
  Sender: TObject);
begin
  inherited;

  //Lancamento de Sobra de Caixa
  Self.Close;
end;

end.
