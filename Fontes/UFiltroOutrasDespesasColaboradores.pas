{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroOutrasDespesasColaboradores.pas
  Descricao:   Tela de filtro de Outras Despesas de Funcionarios
  Author   :   Cristina
  Date:        14-jul-2016
  Last Update: 01-dez-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroOutrasDespesasColaboradores;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, frxClass, frxDBSet, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, JvExControls, JvButton, JvTransparentButton, Vcl.Grids,
  Vcl.DBGrids, Vcl.ExtCtrls, Datasnap.DBClient, Datasnap.Provider, Vcl.StdCtrls,
  Vcl.Mask, JvExMask, JvToolEdit, Vcl.DBCtrls, System.DateUtils, System.StrUtils;

type
  TFFiltroOutrasDespesasColaboradores = class(TFFiltroSimplesPadrao)
    dspGridPaiPadrao: TDataSetProvider;
    cdsGridPaiPadrao: TClientDataSet;
    lblNomeFuncionario: TLabel;
    lblPeriodoCadastro: TLabel;
    Label3: TLabel;
    lcbNomeFuncionario: TDBLookupComboBox;
    dteIniPeriodoCadastro: TJvDateEdit;
    dteFimPeriodoCadastro: TJvDateEdit;
    rgSituacaoVale: TRadioGroup;
    lcbTipoODespF: TDBLookupComboBox;
    lblTipoODespF: TLabel;
    qryFuncionarios: TFDQuery;
    dsFuncionarios: TDataSource;
    qryTipoODespF: TFDQuery;
    dsTipoODespF: TDataSource;
    cdsGridPaiPadraoID: TIntegerField;
    cdsGridPaiPadraoID_FUNCIONARIO_FK: TIntegerField;
    cdsGridPaiPadraoID_TIPO_OUTRADESP_FUNC_FK: TIntegerField;
    cdsGridPaiPadraoVR_OUTRADESPESA_FUNC: TBCDField;
    cdsGridPaiPadraoDESCR_OUTRADESPESA_FUNC: TStringField;
    cdsGridPaiPadraoCAD_ID_USUARIO: TIntegerField;
    cdsGridPaiPadraoDATA_QUITACAO: TDateField;
    cdsGridPaiPadraoQUIT_ID_USUARIO: TIntegerField;
    cdsGridPaiPadraoFLG_CANCELADO: TStringField;
    cdsGridPaiPadraoDATA_CANCELAMENTO: TDateField;
    cdsGridPaiPadraoCANCEL_ID_USUARIO: TIntegerField;
    cdsGridPaiPadraoNOMEFUNCIONARIO: TStringField;
    cdsGridPaiPadraoTIPODESPESA: TStringField;
    cdsGridPaiPadraoID_FECHAMENTO_SALARIO_FK: TIntegerField;
    cdsGridPaiPadraoDATA_OUTRADESPESA_FUNC: TDateField;
    cdsGridPaiPadraoDATA_CADASTRO: TSQLTimeStampField;
    cdsGridPaiPadraoFLG_MODALIDADE: TStringField;
    cdsGridPaiPadraoDESCR_MODALIDADE: TStringField;
    procedure dteIniPeriodoCadastroKeyPress(Sender: TObject; var Key: Char);
    procedure dteFimPeriodoCadastroKeyPress(Sender: TObject; var Key: Char);
    procedure lcbNomeFuncionarioKeyPress(Sender: TObject; var Key: Char);
    procedure lcbTipoODespFKeyPress(Sender: TObject; var Key: Char);
    procedure rgSituacaoValeExit(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure dbgGridPaiPadraoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgGridPaiPadraoTitleClick(Column: TColumn);
    procedure cdsGridPaiPadraoCalcFields(DataSet: TDataSet);
    procedure cdsGridPaiPadraoVR_OUTRADESPESA_FUNCGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure FormShow(Sender: TObject);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }

    procedure AbrirListaDespesas;
  public
    { Public declarations }
  end;

var
  FFiltroOutrasDespesasColaboradores: TFFiltroOutrasDespesasColaboradores;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais,
  UDMOutrasDespesasColaborador, UCadastroOutrasDespesasColaborador;

{ TFFiltroOutrasDespesasColaboradores }

procedure TFFiltroOutrasDespesasColaboradores.AbrirListaDespesas;
begin
  dmPrincipal.AtualizarLancamentosColaboradorRecorrentes;

  with qryGridPaiPadrao, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_OUTRADESPESA_FUNC AS ID, ' +
            '       ID_FUNCIONARIO_FK, ' +
            '       ID_TIPO_OUTRADESP_FUNC_FK, ' +
            '       DATA_OUTRADESPESA_FUNC, ' +
            '       VR_OUTRADESPESA_FUNC, ' +
            '       DESCR_OUTRADESPESA_FUNC, ' +
            '       ID_FECHAMENTO_SALARIO_FK,' +
            '       DATA_CADASTRO, ' +
            '       CAD_ID_USUARIO, ' +
            '       DATA_QUITACAO, ' +
            '       QUIT_ID_USUARIO, ' +
            '       FLG_CANCELADO, ' +
            '       DATA_CANCELAMENTO, ' +
            '       CANCEL_ID_USUARIO, ' +
            '       FLG_MODALIDADE ' +
            '  FROM OUTRADESPESA_FUNC ' +
            ' WHERE ID_FUNCIONARIO_FK = (CASE WHEN (:ID_FUNC01 = 0) THEN ID_FUNCIONARIO_FK ELSE :ID_FUNC02 END) ' +
            '   AND ID_TIPO_OUTRADESP_FUNC_FK = (CASE WHEN (:ID_ODESPF01 = 0) THEN ID_TIPO_OUTRADESP_FUNC_FK ELSE :ID_ODESPF02 END) ' +
            '   AND DATA_OUTRADESPESA_FUNC BETWEEN :DATA_INI AND :DATA_FIM ';

    if rgSituacaoVale.ItemIndex = 1 then
      Add(' AND DATA_QUITACAO IS NULL')
    else if rgSituacaoVale.ItemIndex = 2 then
      Add(' AND DATA_QUITACAO IS NOT NULL');

    if Trim(lcbNomeFuncionario.Text) <> '' then
    begin
      Params.ParamByName('ID_FUNC01').Value := lcbNomeFuncionario.KeyValue;
      Params.ParamByName('ID_FUNC02').Value := lcbNomeFuncionario.KeyValue;
    end
    else
    begin
      Params.ParamByName('ID_FUNC01').Value := 0;
      Params.ParamByName('ID_FUNC02').Value := 0;
    end;

    if Trim(lcbTipoODespF.Text) <> '' then
    begin
      Params.ParamByName('ID_ODESPF01').Value := lcbTipoODespF.KeyValue;
      Params.ParamByName('ID_ODESPF02').Value := lcbTipoODespF.KeyValue;
    end
    else
    begin
      Params.ParamByName('ID_ODESPF01').Value := 0;
      Params.ParamByName('ID_ODESPF02').Value := 0;
    end;

    Params.ParamByName('DATA_INI').Value := FormatDateTime('YYYY-MM-DD', dteIniPeriodoCadastro.Date);
    Params.ParamByName('DATA_FIM').Value := FormatDateTime('YYYY-MM-DD', dteFimPeriodoCadastro.Date);

    Add('ORDER BY ID_FUNCIONARIO_FK ASC, ID_TIPO_OUTRADESP_FUNC_FK, DATA_OUTRADESPESA_FUNC DESC');
  end;

  cdsGridPaiPadrao.Close;

  if Trim(lcbNomeFuncionario.Text) <> '' then
  begin
    cdsGridPaiPadrao.Params.ParamByName('ID_FUNC01').Value := lcbNomeFuncionario.KeyValue;
    cdsGridPaiPadrao.Params.ParamByName('ID_FUNC02').Value := lcbNomeFuncionario.KeyValue;
  end
  else
  begin
    cdsGridPaiPadrao.Params.ParamByName('ID_FUNC01').Value := 0;
    cdsGridPaiPadrao.Params.ParamByName('ID_FUNC02').Value := 0;
  end;

  if Trim(lcbTipoODespF.Text) <> '' then
  begin
    cdsGridPaiPadrao.Params.ParamByName('ID_ODESPF01').Value := lcbTipoODespF.KeyValue;
    cdsGridPaiPadrao.Params.ParamByName('ID_ODESPF02').Value := lcbTipoODespF.KeyValue;
  end
  else
  begin
    cdsGridPaiPadrao.Params.ParamByName('ID_ODESPF01').Value := 0;
    cdsGridPaiPadrao.Params.ParamByName('ID_ODESPF02').Value := 0;
  end;

  cdsGridPaiPadrao.Params.ParamByName('DATA_INI').Value := FormatDateTime('YYYY-MM-DD', dteIniPeriodoCadastro.Date);
  cdsGridPaiPadrao.Params.ParamByName('DATA_FIM').Value := FormatDateTime('YYYY-MM-DD', dteFimPeriodoCadastro.Date);

  cdsGridPaiPadrao.Open;
end;

procedure TFFiltroOutrasDespesasColaboradores.btnEditarClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao   := E;
  vgIdConsulta := cdsGridPaiPadrao.FieldByName('ID').AsInteger;

  Application.CreateForm(TdmOutrasDespesasColaborador, dmOutrasDespesasColaborador);
  dmGerencial.CriarForm(TFCadastroOutrasDespesasColaborador, FCadastroOutrasDespesasColaborador);
  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroOutrasDespesasColaboradores.btnExcluirClick(Sender: TObject);
var
  qryExclusao: TFDQuery;
  CodLanc, AnoLanc: Integer;
begin
  inherited;

  if lPodeExcluir then
  begin
    CodLanc := 0;
    AnoLanc := 0;

    qryExclusao := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      with qryExclusao, SQL do
      begin
        //Lancamento (Consulta)
        Close;
        Clear;
        Text := 'SELECT COD_LANCAMENTO, ANO_LANCAMENTO ' +
                '  FROM LANCAMENTO' +
                ' WHERE ID_ORIGEM      = :ID_ORIGEM ' +
                '   AND ID_NATUREZA_FK = :ID_NATUREZA';
        Params.ParamByName('ID_ORIGEM').Value   := cdsGridPaiPadrao.FieldByName('ID').AsInteger;
        Params.ParamByName('ID_NATUREZA').Value := 7;  //OUTROS LANCAMENTOS COM COLABORADOR
        Open;

        CodLanc := qryExclusao.FieldByName('COD_LANCAMENTO').AsInteger;
        AnoLanc := qryExclusao.FieldByName('ANO_LANCAMENTO').AsInteger;

        //Parcela Lancamento
        Close;
        Clear;

        Text := 'UPDATE LANCAMENTO_PARC ' +
                '                   SET FLG_STATUS        = :FLG_STATUS, ' +
                '                       DATA_CANCELAMENTO = :DATA_CANCELAMENTO, ' +
                '                       CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO ' +
                ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO';

        Params.ParamByName('FLG_STATUS').Value        := 'C';
        Params.ParamByName('DATA_CANCELAMENTO').Value := Now;
        Params.ParamByName('CANCEL_ID_USUARIO').Value := vgUsu_Id;
        Params.ParamByName('COD_LANCAMENTO').Value    := CodLanc;
        Params.ParamByName('ANO_LANCAMENTO').Value    := AnoLanc;
        ExecSQL;

        //Lancamento
        Close;
        Clear;

        Text := 'UPDATE LANCAMENTO ' +
                '              SET FLG_CANCELADO     = :FLG_CANCELADO, ' +
                '                  DATA_CANCELAMENTO = :DATA_CANCELAMENTO, ' +
                '                  CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO ' +
                ' WHERE COD_LANCAMENTO = :COD_LANCAMENTO ' +
                '   AND ANO_LANCAMENTO = :ANO_LANCAMENTO';

        Params.ParamByName('FLG_CANCELADO').Value     := 'S';
        Params.ParamByName('DATA_CANCELAMENTO').Value := Now;
        Params.ParamByName('CANCEL_ID_USUARIO').Value := vgUsu_Id;
        Params.ParamByName('COD_LANCAMENTO').Value    := CodLanc;
        Params.ParamByName('ANO_LANCAMENTO').Value    := AnoLanc;
        ExecSQL;

        //Vale
        Close;
        Clear;
        Text := 'UPDATE OUTRADESPESA_FUNC ' +
                '        SET FLG_CANCELADO     = :FLG_CANCELADO, ' +
                '            DATA_CANCELAMENTO = :DATA_CANCELAMENTO, ' +
                '            CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO ' +
                ' WHERE ID_OUTRADESPESA_FUNC = :ID_OUTRADESPESA_FUNC';
        Params.ParamByName('FLG_CANCELADO').Value        := 'S';
        Params.ParamByName('DATA_CANCELAMENTO').Value    := Date;
        Params.ParamByName('CANCEL_ID_USUARIO').Value    := vgUsu_Id;
        Params.ParamByName('ID_OUTRADESPESA_FUNC').Value := cdsGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;

        dmPrincipal.RecalcularFechamentoSalarios(cdsGridPaiPadrao.FieldByName('ID_FECHAMENTO_SALARIO_FK').AsInteger,
                                                 cdsGridPaiPadrao.FieldByName('ID_FUNCIONARIO_FK').AsInteger,
                                                 False, False, True);
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      GravarLog;
      Application.MessageBox('Lan�amento de Colaborador exclu�do com sucesso!', 'Aviso', MB_OK)
    except
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Application.MessageBox('Erro na exclus�o do Lan�amento de Colaborador.', 'Erro', MB_OK + MB_ICONERROR)
    end;

    FreeAndNil(qryExclusao);

    btnFiltrar.Click;
  end;
end;

procedure TFFiltroOutrasDespesasColaboradores.btnFiltrarClick(Sender: TObject);
begin
  AbrirListaDespesas;

  inherited;
end;

procedure TFFiltroOutrasDespesasColaboradores.btnIncluirClick(Sender: TObject);
begin
  inherited;

  vgOperacao   := I;
  vgIdConsulta := 0;

  Application.CreateForm(TdmOutrasDespesasColaborador, dmOutrasDespesasColaborador);
  dmGerencial.CriarForm(TFCadastroOutrasDespesasColaborador, FCadastroOutrasDespesasColaborador);
  btnFiltrar.Click;
end;

procedure TFFiltroOutrasDespesasColaboradores.btnLimparClick(Sender: TObject);
begin
  inherited;

  dteIniPeriodoCadastro.Date := StartOfTheMonth(Date);
  dteFimPeriodoCadastro.Date := EndOfTheMonth(Date);

  lcbNomeFuncionario.KeyValue := Null;
  lcbTipoODespF.KeyValue      := Null;
  rgSituacaoVale.ItemIndex    := 0;

  inherited;

  if dteIniPeriodoCadastro.CanFocus then
    dteIniPeriodoCadastro.SetFocus;
end;

procedure TFFiltroOutrasDespesasColaboradores.cdsGridPaiPadraoCalcFields(
  DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  //Nome do Funcionario
  qryAux := dmGerencial.CriarFDQuery(nil, vgConGER);

  with qryAux, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_FUNCIONARIO, NOME_FUNCIONARIO ' +
            '  FROM FUNCIONARIO ' +
            ' WHERE ID_FUNCIONARIO = :ID_FUNCIONARIO';
    Params.ParamByName('ID_FUNCIONARIO').AsInteger := cdsGridPaiPadrao.FieldByName('ID_FUNCIONARIO_FK').AsInteger;
    Open;
  end;

  cdsGridPaiPadrao.FieldByName('NOMEFUNCIONARIO').AsString := qryAux.FieldByName('NOME_FUNCIONARIO').AsString;

  FreeAndNil(qryAux);

  //Tipo da Despesa
  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with qryAux, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_TIPO_OUTRADESP_FUNC, DESCR_TIPO_OUTRADESP_FUNC ' +
            '  FROM TIPO_OUTRADESP_FUNC ' +
            ' WHERE ID_TIPO_OUTRADESP_FUNC = :ID_TIPO_OUTRADESP_FUNC';
    Params.ParamByName('ID_TIPO_OUTRADESP_FUNC').AsInteger := cdsGridPaiPadrao.FieldByName('ID_TIPO_OUTRADESP_FUNC_FK').AsInteger;
    Open;
  end;

  cdsGridPaiPadrao.FieldByName('TIPODESPESA').AsString := qryAux.FieldByName('DESCR_TIPO_OUTRADESP_FUNC').AsString;

  FreeAndNil(qryAux);

  //Modalidade de Despesa
  case AnsiIndexStr(UpperCase(cdsGridPaiPadrao.FieldByName('FLG_MODALIDADE').AsString),
                    ['L', 'A', 'D']) of
    0: cdsGridPaiPadrao.FieldByName('DESCR_MODALIDADE').AsString := 'Lan�ar como Despesa';
    1: cdsGridPaiPadrao.FieldByName('DESCR_MODALIDADE').AsString := 'Acrescentar ao Sal�rio';
    2: cdsGridPaiPadrao.FieldByName('DESCR_MODALIDADE').AsString := 'Deduzir do Sal�rio';
  end;
end;

procedure TFFiltroOutrasDespesasColaboradores.cdsGridPaiPadraoVR_OUTRADESPESA_FUNCGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFFiltroOutrasDespesasColaboradores.dbgGridPaiPadraoDblClick(
  Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao   := C;
  vgIdConsulta := cdsGridPaiPadrao.FieldByName('ID').AsInteger;

  Application.CreateForm(TdmOutrasDespesasColaborador, dmOutrasDespesasColaborador);
  dmGerencial.CriarForm(TFCadastroOutrasDespesasColaborador, FCadastroOutrasDespesasColaborador);

  DefinirPosicaoGrid;
end;

procedure TFFiltroOutrasDespesasColaboradores.dbgGridPaiPadraoDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  Grid: TDBGrid;
begin
  inherited;

  Grid := Sender as TDBGrid;

  if Grid.DataSource.DataSet.FieldByName('FLG_CANCELADO').AsString = 'S' then
  begin
    Grid.Canvas.Font.Color := clGray;
    Grid.Canvas.FillRect(Rect);
    Grid.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TFFiltroOutrasDespesasColaboradores.dbgGridPaiPadraoTitleClick(
  Column: TColumn);
begin
  inherited;

  cdsGridPaiPadrao.IndexFieldNames := Column.FieldName;
end;

procedure TFFiltroOutrasDespesasColaboradores.dteFimPeriodoCadastroKeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
     if dteFimPeriodoCadastro.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteFimPeriodoCadastro.CanFocus then
        dteFimPeriodoCadastro.SetFocus;
    end
    else if dteIniPeriodoCadastro.Date > dteFimPeriodoCadastro.Date then
    begin
      Application.MessageBox('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso', MB_OK + MB_ICONWARNING);

      dteFimPeriodoCadastro.Clear;

      if dteFimPeriodoCadastro.CanFocus then
        dteFimPeriodoCadastro.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dteFimPeriodoCadastro.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteFimPeriodoCadastro.CanFocus then
        dteFimPeriodoCadastro.SetFocus;
    end
    else if dteIniPeriodoCadastro.Date > dteFimPeriodoCadastro.Date then
    begin
      Application.MessageBox('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso', MB_OK + MB_ICONWARNING);

      dteFimPeriodoCadastro.Clear;

      if dteFimPeriodoCadastro.CanFocus then
        dteFimPeriodoCadastro.SetFocus;
    end
    else
      btnFiltrar.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroOutrasDespesasColaboradores.dteIniPeriodoCadastroKeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
     if dteIniPeriodoCadastro.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteIniPeriodoCadastro.CanFocus then
        dteIniPeriodoCadastro.SetFocus;
    end
    else if dteIniPeriodoCadastro.Date > dteFimPeriodoCadastro.Date then
    begin
      Application.MessageBox('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso', MB_OK + MB_ICONWARNING);

      dteIniPeriodoCadastro.Clear;

      if dteIniPeriodoCadastro.CanFocus then
        dteIniPeriodoCadastro.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dteIniPeriodoCadastro.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteIniPeriodoCadastro.CanFocus then
        dteIniPeriodoCadastro.SetFocus;
    end
    else if dteIniPeriodoCadastro.Date > dteFimPeriodoCadastro.Date then
    begin
      Application.MessageBox('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso', MB_OK + MB_ICONWARNING);

      dteIniPeriodoCadastro.Clear;

      if dteIniPeriodoCadastro.CanFocus then
        dteIniPeriodoCadastro.SetFocus;
    end
    else
      btnFiltrar.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroOutrasDespesasColaboradores.FormCreate(Sender: TObject);
begin
  inherited;

  qryFuncionarios.Close;
  qryFuncionarios.Open;

  qryTipoODespF.Close;
  qryTipoODespF.Open;

  dteIniPeriodoCadastro.Date := StartOfTheMonth(Date);
  dteFimPeriodoCadastro.Date := EndOfTheMonth(Date);

  AbrirListaDespesas;
end;

procedure TFFiltroOutrasDespesasColaboradores.FormShow(Sender: TObject);
begin
  inherited;

  if dteIniPeriodoCadastro.CanFocus then
    dteIniPeriodoCadastro.SetFocus;
end;

procedure TFFiltroOutrasDespesasColaboradores.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    X:  //EXCLUSAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da exclus�o desse Lan�amento de Colaborador:', '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          cdsGridPaiPadrao.FieldByName('ID').AsInteger,
                          'OUTRADESPESA_FUNC');
    end;
    P:  //IMPRESSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente impress�o de Lista de Lan�amentos de Colaborador',
                          cdsGridPaiPadrao.FieldByName('ID').AsInteger,
                          'OUTRADESPESA_FUNC');
    end;
    T:  //EXPORTACAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente exporta��o de Lista de Lan�amentos de Colaborador',
                          cdsGridPaiPadrao.FieldByName('ID').AsInteger,
                          'OUTRADESPESA_FUNC');
    end;
  end;
end;

procedure TFFiltroOutrasDespesasColaboradores.lcbNomeFuncionarioKeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroOutrasDespesasColaboradores.lcbTipoODespFKeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

function TFFiltroOutrasDespesasColaboradores.PodeExcluir(
  var Msg: String): Boolean;
begin
  Result := True;

  if not cdsGridPaiPadrao.FieldByName('DATA_QUITACAO').IsNull then
    msg := 'N�o � poss�vel excluir o Lan�amento informado, pois a mesma j� est� quitada.';

  Result := (cdsGridPaiPadrao.FieldByName('DATA_QUITACAO').IsNull) and (cdsGridPaiPadrao.RecordCount > 0);
end;

procedure TFFiltroOutrasDespesasColaboradores.rgSituacaoValeExit(
  Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroOutrasDespesasColaboradores.VerificarPermissoes;
begin
  inherited;

  btnIncluir.Enabled  := vgPrm_IncODespF;
  btnEditar.Enabled   := vgPrm_EdODespF and (cdsGridPaiPadrao.RecordCount > 0);
  btnExcluir.Enabled  := vgPrm_ExcODespF and (cdsGridPaiPadrao.RecordCount > 0);
  btnImprimir.Enabled := vgPrm_ImpODespF and (cdsGridPaiPadrao.RecordCount > 0);
end;

end.
