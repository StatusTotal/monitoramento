{ -----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UBaixaLancamento.pas
  Descricao:   Formulario de Baixa de Lancamentos de Despesas e Receitas
  Author   :   Cristina
  Date:        24-out-2016
  Last Update: 06-dez-2017 (Cristina)
  ------------------------------------------------------------------------------------------------------------------------ }

unit UBaixaLancamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  UCadastroGeralPadrao, Data.DB, JvExControls, JvButton, JvTransparentButton,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.DBCtrls, Vcl.Mask, JvBaseEdits, JvDBControls,
  JvExMask, JvToolEdit, System.StrUtils, UDM, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.DBClient,
  Datasnap.Provider, Vcl.Grids, Vcl.DBGrids, System.DateUtils;

type
  TFBaixaLancamento = class(TFCadastroGeralPadrao)
    gbFlutuantes: TGroupBox;
    gbDespesasPagas: TGroupBox;
    qryDespPagas: TFDQuery;
    qryFlutuantes: TFDQuery;
    dspDespPagas: TDataSetProvider;
    cdsDespPagas: TClientDataSet;
    dsDespPagas: TDataSource;
    dsFlutuantes: TDataSource;
    pnlBaixa: TPanel;
    lblDataEntrada: TLabel;
    lblDescricaoBaixa: TLabel;
    lblValorPago: TLabel;
    lblObservacoes: TLabel;
    lblDataBaixa: TLabel;
    lblValorPrevisto: TLabel;
    lblNumRecibo: TLabel;
    mmObservacoes: TDBMemo;
    dteDataEntrada: TJvDBDateEdit;
    cedValorPago: TJvDBCalcEdit;
    dteDataBaixa: TJvDBDateEdit;
    edtDescricaoBaixa: TDBEdit;
    cedValorPrevisto: TJvDBCalcEdit;
    edtNumRecibo: TDBEdit;
    dbgFlutuantes: TDBGrid;
    dbgDespesasPagas: TDBGrid;
    lblPeriodo: TLabel;
    Label2: TLabel;
    rgSituacao: TRadioGroup;
    dtePeriodoIni: TJvDateEdit;
    dtePeriodoFim: TJvDateEdit;
    btnConfirmarBaixa: TJvTransparentButton;
    btnCancelarBaixa: TJvTransparentButton;
    btnFiltrar: TJvTransparentButton;
    cdsDespPagasSELECIONADO: TBooleanField;
    cdsDespPagasCOD_LANCAMENTO_FK: TIntegerField;
    cdsDespPagasANO_LANCAMENTO_FK: TIntegerField;
    cdsDespPagasID_LANCAMENTO_PARC: TIntegerField;
    cdsDespPagasDATA_LANCAMENTO_PARC: TDateField;
    cdsDespPagasVR_PARCELA_PREV: TBCDField;
    cdsDespPagasVR_PARCELA_PAGO: TBCDField;
    cdsDespPagasDATA_PAGAMENTO: TDateField;
    cdsDespPagasDESCR_ITEM: TStringField;
    qryQtdParcs: TFDQuery;
    dsQtdParcs: TDataSource;
    lblValorFlutuante: TLabel;
    cedValorFlutuante: TJvCalcEdit;
    lblNumReciboFlutuante: TLabel;
    edtNumReciboFlutuante: TEdit;
    qryQtdParcsQTD: TIntegerField;
    qryQtdParcsTOTAL_PAGO: TBCDField;
    qryQtdParcsTOTAL_PREV: TBCDField;
    qryFlutuantesCOD_LANCAMENTO: TIntegerField;
    qryFlutuantesANO_LANCAMENTO: TIntegerField;
    qryFlutuantesDATA_LANCAMENTO: TDateField;
    qryFlutuantesTIPO_LANCAMENTO: TStringField;
    qryFlutuantesTIPO_CADASTRO: TStringField;
    qryFlutuantesID_CATEGORIA_DESPREC_FK: TIntegerField;
    qryFlutuantesID_SUBCATEGORIA_DESPREC_FK: TIntegerField;
    qryFlutuantesID_NATUREZA_FK: TIntegerField;
    qryFlutuantesID_CLIENTE_FORNECEDOR_FK: TIntegerField;
    qryFlutuantesFLG_IMPOSTORENDA: TStringField;
    qryFlutuantesFLG_PESSOAL: TStringField;
    qryFlutuantesFLG_AUXILIAR: TStringField;
    qryFlutuantesFLG_REAL: TStringField;
    qryFlutuantesFLG_FLUTUANTE: TStringField;
    qryFlutuantesQTD_PARCELAS: TIntegerField;
    qryFlutuantesVR_TOTAL_PREV: TBCDField;
    qryFlutuantesVR_TOTAL_JUROS: TBCDField;
    qryFlutuantesVR_TOTAL_DESCONTO: TBCDField;
    qryFlutuantesVR_TOTAL_PAGO: TBCDField;
    qryFlutuantesCAD_ID_USUARIO: TIntegerField;
    qryFlutuantesDATA_CADASTRO: TSQLTimeStampField;
    qryFlutuantesFLG_STATUS: TStringField;
    qryFlutuantesFLG_CANCELADO: TStringField;
    qryFlutuantesDATA_CANCELAMENTO: TDateField;
    qryFlutuantesCANCEL_ID_USUARIO: TIntegerField;
    qryFlutuantesFLG_RECORRENTE: TStringField;
    qryFlutuantesFLG_REPLICADO: TStringField;
    qryFlutuantesNUM_RECIBO: TIntegerField;
    qryFlutuantesID_ORIGEM: TIntegerField;
    qryFlutuantesID_SISTEMA_FK: TIntegerField;
    qryFlutuantesOBSERVACAO: TStringField;
    qryFlutuantesID_CARNELEAO_EQUIVALENCIA_FK: TIntegerField;
    qryFlutuantesID_LANCAMENTO_PARC: TIntegerField;
    qryFlutuantesCOD_LANCAMENTO_FK: TIntegerField;
    qryFlutuantesANO_LANCAMENTO_FK: TIntegerField;
    qryFlutuantesDATA_LANCAMENTO_PARC: TDateField;
    qryFlutuantesNUM_PARCELA: TIntegerField;
    qryFlutuantesDATA_VENCIMENTO: TDateField;
    qryFlutuantesVR_PARCELA_PREV: TBCDField;
    qryFlutuantesVR_PARCELA_JUROS: TBCDField;
    qryFlutuantesVR_PARCELA_DESCONTO: TBCDField;
    qryFlutuantesVR_PARCELA_PAGO: TBCDField;
    qryFlutuantesID_FORMAPAGAMENTO_FK: TIntegerField;
    qryFlutuantesAGENCIA: TStringField;
    qryFlutuantesCONTA: TStringField;
    qryFlutuantesID_BANCO_FK: TIntegerField;
    qryFlutuantesNUM_CHEQUE: TStringField;
    qryFlutuantesNUM_COD_DEPOSITO: TStringField;
    qryFlutuantesFLG_STATUS_1: TStringField;
    qryFlutuantesDATA_CANCELAMENTO_1: TDateField;
    qryFlutuantesCANCEL_ID_USUARIO_1: TIntegerField;
    qryFlutuantesDATA_PAGAMENTO: TDateField;
    qryFlutuantesPAG_ID_USUARIO: TIntegerField;
    qryFlutuantesOBS_LANCAMENTO_PARC: TStringField;
    qryFlutuantesCAD_ID_USUARIO_1: TIntegerField;
    qryFlutuantesDATA_CADASTRO_1: TDateField;
    qryFlutuantesFLG_STATUS_PARC: TStringField;
    cdsDespPagasOBSERVACAO: TStringField;
    qryFlutuantesFLG_FORAFECHCAIXA: TStringField;
    cdsDespPagasBAIXADO: TStringField;
    procedure FormShow(Sender: TObject);
    procedure mmObservacoesKeyPress(Sender: TObject; var Key: Char);
    procedure btnCancelarBaixaClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnConfirmarBaixaClick(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure dbgFlutuantesTitleClick(Column: TColumn);
    procedure dbgDespesasPagasTitleClick(Column: TColumn);
    procedure qryFlutuantesVR_TOTAL_PREVGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryFlutuantesVR_PARCELA_PREVGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryFlutuantesVR_PARCELA_PAGOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsDespPagasCalcFields(DataSet: TDataSet);
    procedure btnFiltrarClick(Sender: TObject);
    procedure dtePeriodoIniKeyPress(Sender: TObject; var Key: Char);
    procedure dtePeriodoFimKeyPress(Sender: TObject; var Key: Char);
    procedure rgSituacaoClick(Sender: TObject);
    procedure rgSituacaoExit(Sender: TObject);
    procedure lcbFormaPagamentoKeyPress(Sender: TObject; var Key: Char);
    procedure dbgFlutuantesDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgDespesasPagasDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgFlutuantesDblClick(Sender: TObject);
    procedure dbgDespesasPagasCellClick(Column: TColumn);
    procedure dbgDespesasPagasColEnter(Sender: TObject);
    procedure dbgDespesasPagasColExit(Sender: TObject);
    procedure cdsDespPagasAfterPost(DataSet: TDataSet);
    procedure cdsDespPagasVR_PARCELA_PAGOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsDespPagasVR_PARCELA_PREVGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure cedValorFlutuanteKeyPress(Sender: TObject; var Key: Char);
    procedure edtNumReciboFlutuanteKeyPress(Sender: TObject; var Key: Char);
    procedure qryFlutuantesVR_TOTAL_JUROSGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryFlutuantesVR_TOTAL_DESCONTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryFlutuantesVR_TOTAL_PAGOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryFlutuantesVR_PARCELA_DESCONTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryFlutuantesVR_PARCELA_JUROSGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure dteDataBaixaExit(Sender: TObject);
    procedure dteDataBaixaKeyPress(Sender: TObject; var Key: Char);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
    FOriginalOptions: TDBGridOptions;

    lPodeFechar: Boolean;

    sNovaObservacao: String;

    dDataPagtoOriginal: TDatetime;

    procedure AbrirListaReceitasFlutuantes;

    procedure SalvarBoleano;

    procedure HabDesabBotoes(Hab: Boolean);
    procedure DesabilitaHabilitaCampos(Hab: Boolean);
  public
    { Public declarations }
  end;

var
  FBaixaLancamento: TFBaixaLancamento;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UGDM, UVariaveisGlobais, UDMLancamento,
  UOpcoesAcaoBaixaLancIndividual, UDMBaixa;

{ TFBaixaLancamento }

procedure TFBaixaLancamento.AbrirListaReceitasFlutuantes;
begin
  { Mesma query da tela UListaBaixaFlutuantes (qryGridListaPadrao) }
  qryFlutuantes.Close;
  qryFlutuantes.SQL.Clear;
  qryFlutuantes.SQL.Text := 'SELECT DISTINCT L.*, ' +
                            '       LP.*, ' +
                            '       LP.FLG_STATUS AS FLG_STATUS_PARC ' +
                            '  FROM LANCAMENTO L ' +
                            ' INNER JOIN LANCAMENTO_PARC LP ' +
                            '    ON L.COD_LANCAMENTO = LP.COD_LANCAMENTO_FK ' +
                            '   AND L.ANO_LANCAMENTO = LP.ANO_LANCAMENTO_FK ' +
                            '  LEFT JOIN BAIXA_LANC_DERIVADO BLD ' +
                            '    ON L.COD_LANCAMENTO = BLD.ORIG_COD_LANCAMENTO_FK ' +
                            '   AND L.ANO_LANCAMENTO = BLD.ORIG_ANO_LANCAMENTO_FK ' +
                            ' WHERE L.FLG_FLUTUANTE = ' + QuotedStr('S') +
                            '   AND L.TIPO_LANCAMENTO = ' + QuotedStr('R') +
                            '   AND L.FLG_CANCELADO = ' + QuotedStr('N') +
                            '   AND LP.FLG_STATUS <> ' + QuotedStr('C') +
                            '   AND LP.DATA_LANCAMENTO_PARC BETWEEN :DATA_INI AND :DATA_FIM ';

  qryFlutuantes.Params.ParamByName('DATA_INI').Value := dtePeriodoIni.Date;
  qryFlutuantes.Params.ParamByName('DATA_FIM').Value := dtePeriodoFim.Date;

  // Valor
  if cedValorFlutuante.Value > 0 then
    qryFlutuantes.SQL.Add(' AND L.VR_TOTAL_PREV = ' +
      ReplaceStr(cedValorFlutuante.EditText, ',', '.'));

  // Recibo
  if Trim(edtNumReciboFlutuante.Text) <> '' then
    qryFlutuantes.SQL.Add(' AND L.NUM_RECIBO = ' +
      Trim(edtNumReciboFlutuante.Text));

  // Situacao
  case rgSituacao.ItemIndex of
    1: // Sem Baixa
      qryFlutuantes.SQL.Add(' AND BLD.DERIV_COD_LANCAMENTO_FK IS NULL');
    2: // Baixados
      qryFlutuantes.SQL.Add(' AND BLD.DERIV_COD_LANCAMENTO_FK IS NOT NULL');
  end;

  qryFlutuantes.SQL.Add(' ORDER BY LP.DATA_LANCAMENTO_PARC, ' +
                        '          L.COD_LANCAMENTO, ' +
                        '          L.ANO_LANCAMENTO');

  qryFlutuantes.Open;
end;

procedure TFBaixaLancamento.btnCancelarBaixaClick(Sender: TObject);
begin
  inherited;

  if dmBaixa.cdsBaixaLancI.State in [dsInsert, dsEdit] then
    dmBaixa.cdsBaixaLancI.Cancel;

  HabDesabBotoes(False);
  DesabilitaHabilitaCampos(False);
end;

procedure TFBaixaLancamento.btnCancelarClick(Sender: TObject);
begin
  inherited;

  lPodeFechar := True;

  if lPodeCancelar then
  begin
    dmBaixa.cdsBaixaLancI.Cancel;
    dmBaixa.cdsBaixaLancI.Close;

    Self.Close;
  end;
end;

procedure TFBaixaLancamento.btnConfirmarBaixaClick(Sender: TObject);
var
  lPagar, lCancelar: Boolean;
  Op: TOperacao;
  IdCons, Cod, Ano: Integer;
  TpL: String;
  OrigF, OrigC: Boolean;
begin
  inherited;

  lPagar := False;
  lCancelar := False;

  sNovaObservacao := '';

  if vgOperacao <> C then
  begin
    if dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PAGO').AsCurrency <
      dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PREV').AsCurrency then
    begin
      if Application.MessageBox('O valor total da(s) Despesa(s) est� MENOR que o Valor previsto para a Parcela. Isso configura uma SOBRA de Valor em Caixa, deseja continuar?',
                                'Aviso', MB_YESNO + MB_ICONQUESTION) = ID_YES then
      begin
        lPagar := True;

        dmBaixa.cdsBaixaLancI.FieldByName('VR_DIFERENCA').AsCurrency := (dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PREV').AsCurrency -
                                                                         dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PAGO').AsCurrency);
        dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString := 'S';
      end;
    end
    else if dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PAGO').AsCurrency > dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PREV').AsCurrency then
    begin
      if Application.MessageBox('O valor total da(s) Despesa(s) est� MAIOR que o Valor previsto para a Parcela. Isso configura uma FALTA de Valor em Caixa, deseja continuar?',
                                'Aviso', MB_YESNO + MB_ICONQUESTION) = ID_YES then
      begin
        lPagar := True;

        dmBaixa.cdsBaixaLancI.FieldByName('VR_DIFERENCA').AsCurrency := (dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PAGO').AsCurrency -
                                                                         dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PREV').AsCurrency);
        dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString := 'F';
      end;
    end
    else
      lPagar := True;

    if lPagar then
    begin
      dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_JUROS').AsCurrency    := 0;
      dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_DESCONTO').AsCurrency := 0;
{      dmBaixa.cdsBaixaLancI.FieldByName('PAG_ID_USUARIO').AsInteger       := vgUsu_Id;
      dmBaixa.cdsBaixaLancI.FieldByName('FLG_STATUS').AsString            := 'G';  }

      if dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PAGO').AsCurrency <> dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PREV').AsCurrency then
      begin
        // Solicitar ao Usuario a forma como ele deseja tratar a diferenca de valor
        Op     := vgOperacao;
        IdCons := vgIdConsulta;
        OrigF  := vgOrigemFiltro;
        OrigC  := vgOrigemCadastro;
        Cod    := vgLanc_Codigo;
        Ano    := vgLanc_Ano;
        TpL    := vgLanc_TpLanc;

        vgOperacao       := I;
        vgIdConsulta     := 0;
        vgOrigemFiltro   := False;
        vgOrigemCadastro := True;

        try
          Application.CreateForm(TFOpcoesAcaoBaixaLancIndividual, FOpcoesAcaoBaixaLancIndividual);
          FOpcoesAcaoBaixaLancIndividual.ShowModal;
        finally
          lCancelar       := FOpcoesAcaoBaixaLancIndividual.lAcaoCancelada;
          sNovaObservacao := FOpcoesAcaoBaixaLancIndividual.sObservacao;
          FOpcoesAcaoBaixaLancIndividual.Free;
        end;

        vgOperacao       := Op;
        vgIdConsulta     := IdCons;
        vgOrigemFiltro   := OrigF;
        vgOrigemCadastro := OrigC;
        vgLanc_Codigo    := Cod;
        vgLanc_Ano       := Ano;
        vgLanc_TpLanc    := TpL;
      end;

      if lCancelar then
        btnCancelarBaixa.Click
      else
      begin
        GravarGenerico;

        dmBaixa.cdsBaixaLancI.Close;

        AbrirListaReceitasFlutuantes;

        cdsDespPagas.Close;
        cdsDespPagas.Open;

        HabDesabBotoes(False);
        DesabilitaHabilitaCampos(False);
      end;
    end;
  end;
end;

procedure TFBaixaLancamento.btnFiltrarClick(Sender: TObject);
begin
  inherited;

  AbrirListaReceitasFlutuantes;
end;

procedure TFBaixaLancamento.cdsDespPagasAfterPost(DataSet: TDataSet);
var
  iPos: Integer;
  cValorTotal: Currency;
begin
  inherited;

  cValorTotal := 0;

  iPos := cdsDespPagas.RecNo;

  cdsDespPagas.DisableControls;

  cdsDespPagas.First;

  while not cdsDespPagas.Eof do
  begin
    if cdsDespPagas.FieldByName('SELECIONADO').AsBoolean then
      cValorTotal := (cValorTotal + cdsDespPagas.FieldByName('VR_PARCELA_PAGO')
        .AsCurrency);

    cdsDespPagas.Next;
  end;

  dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PAGO').AsCurrency :=
    cValorTotal;

  cdsDespPagas.EnableControls;

  cdsDespPagas.RecNo := iPos;
end;

procedure TFBaixaLancamento.cdsDespPagasCalcFields(DataSet: TDataSet);
var
  qryAux: TFDQuery;
  sTexto: String;
begin
  inherited;

  sTexto := '';

  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with qryAux, SQL do
  begin
    // Item
    if cdsDespPagas.RecordCount > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT DISTINCT(I.DESCR_ITEM) AS DESCR_ITEM ' +
              '  FROM LANCAMENTO_DET LD ' + ' INNER JOIN ITEM I ' +
              '    ON LD.ID_ITEM_FK = I.ID_ITEM ' +
              ' WHERE LD.COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
              '   AND LD.ANO_LANCAMENTO_FK = :ANO_LANCAMENTO';
      Params.ParamByName('COD_LANCAMENTO').AsInteger := cdsDespPagas.FieldByName('COD_LANCAMENTO_FK').AsInteger;
      Params.ParamByName('ANO_LANCAMENTO').AsInteger := cdsDespPagas.FieldByName('ANO_LANCAMENTO_FK').AsInteger;
      Open;
    end;

    if qryAux.RecordCount > 0 then
    begin
      if Trim(sTexto) = '' then
        sTexto := qryAux.FieldByName('DESCR_ITEM').AsString
      else
        sTexto := sTexto + ' | ' + qryAux.FieldByName('DESCR_ITEM').AsString;

      if Trim(sTexto) = '' then
        cdsDespPagas.FieldByName('DESCR_ITEM').AsString := '--'
      else
        cdsDespPagas.FieldByName('DESCR_ITEM').AsString := sTexto;
    end;
  end;

  FreeAndNil(qryAux);
end;

procedure TFBaixaLancamento.cdsDespPagasVR_PARCELA_PAGOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFBaixaLancamento.cdsDespPagasVR_PARCELA_PREVGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFBaixaLancamento.cedValorFlutuanteKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then // TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    btnFiltrar.Click;
    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFBaixaLancamento.dbgDespesasPagasCellClick(Column: TColumn);
begin
  inherited;

  if dbgDespesasPagas.SelectedField.DataType = ftBoolean then
    SalvarBoleano;
end;

procedure TFBaixaLancamento.dbgDespesasPagasColEnter(Sender: TObject);
begin
  inherited;

  if dbgDespesasPagas.SelectedField.DataType = ftBoolean then
  begin
    FOriginalOptions := dbgDespesasPagas.Options;
    dbgDespesasPagas.Options := dbgDespesasPagas.Options - [dgEditing];
  end;
end;

procedure TFBaixaLancamento.dbgDespesasPagasColExit(Sender: TObject);
begin
  inherited;

  if dbgDespesasPagas.SelectedField.DataType = ftBoolean then
    dbgDespesasPagas.Options := FOriginalOptions;
end;

procedure TFBaixaLancamento.dbgDespesasPagasDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
const
  CtrlState: array [Boolean] of Integer = (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or
    DFCS_CHECKED);
var
  CheckBoxRectangle: TRect;
begin
  inherited;

  if cdsDespPagas.RecordCount > 0 then
  begin
    AlternarCorGrid(Sender, Rect, DataCol, Column, State);

    if (Column.Field.FieldName = 'SELECIONADO') then
    begin
      dbgDespesasPagas.Canvas.FillRect(Rect);

      CheckBoxRectangle.Left := (Rect.Left + 2);
      CheckBoxRectangle.Right := (Rect.Right - 2);
      CheckBoxRectangle.Top := (Rect.Top + 2);
      CheckBoxRectangle.Bottom := (Rect.Bottom - 2);

      DrawFrameControl(dbgDespesasPagas.Canvas.Handle, CheckBoxRectangle,
        DFC_BUTTON, CtrlState[Column.Field.AsBoolean]);
    end;
  end;
end;

procedure TFBaixaLancamento.dbgDespesasPagasTitleClick(Column: TColumn);
begin
  inherited;

  qryDespPagas.IndexFieldNames := Column.FieldName;
end;

procedure TFBaixaLancamento.dbgFlutuantesDblClick(Sender: TObject);
begin
  inherited;

  HabDesabBotoes(True);
  DesabilitaHabilitaCampos(True);

  dmBaixa.cdsBaixaLancI.Close;
  dmBaixa.cdsBaixaLancI.Params.ParamByName('ID_LANCAMENTO_PARC').Value := qryFlutuantes.FieldByName('ID_LANCAMENTO_PARC').AsInteger;
  dmBaixa.cdsBaixaLancI.Open;

  dmBaixa.cdsBaixaLancI.Edit;
{  dmBaixa.cdsBaixaLancI.FieldByName('FLG_STATUS').AsString       := 'G';
  dmBaixa.cdsBaixaLancI.FieldByName('PAG_ID_USUARIO').AsInteger  := vgUsu_Id;  }

  if dmBaixa.cdsBaixaLancI.FieldByName('DATA_PAGAMENTO').IsNull then
  begin
{    if (Date < vgDataLancVigente) or
      (Date < dmBaixa.cdsBaixaLancI.FieldByName('DATA_LANCAMENTO_PARC').AsDateTime) then
    begin
      if vgDataLancVigente < dmBaixa.cdsBaixaLancI.FieldByName('DATA_LANCAMENTO_PARC').AsDateTime then
        dmBaixa.cdsBaixaLancI.FieldByName('DATA_PAGAMENTO').AsDateTime := dmBaixa.cdsBaixaLancI.FieldByName('DATA_LANCAMENTO_PARC').AsDateTime
      else
        dmBaixa.cdsBaixaLancI.FieldByName('DATA_PAGAMENTO').AsDateTime := vgDataLancVigente;
    end
    else
      dmBaixa.cdsBaixaLancI.FieldByName('DATA_PAGAMENTO').AsDateTime := Now;  }

    dDataPagtoOriginal := 0;

    dteDataBaixa.Enabled := True;
  end
  else
  begin
    dDataPagtoOriginal   := dmBaixa.cdsBaixaLancI.FieldByName('DATA_PAGAMENTO').AsDateTime;
    dteDataBaixa.Enabled := False;
  end;

  cdsDespPagas.Last;
  cdsDespPagas.First;
end;

procedure TFBaixaLancamento.dbgFlutuantesDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;

  AlternarCorGrid(Sender, Rect, DataCol, Column, State);
end;

procedure TFBaixaLancamento.dbgFlutuantesTitleClick(Column: TColumn);
begin
  inherited;

  qryFlutuantes.IndexFieldNames := Column.FieldName;
end;

procedure TFBaixaLancamento.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtNumRecibo.MaxLength := 8;
  edtDescricaoBaixa.MaxLength := 250;
  mmObservacoes.MaxLength := 1000;
end;

procedure TFBaixaLancamento.DesabilitaHabilitaCampos(Hab: Boolean);
begin
  dteDataEntrada.Enabled   := False;
  edtNumRecibo.Enabled     := False;
  cedValorPrevisto.Enabled := False;

  dteDataBaixa.Enabled      := Hab;
  cedValorPago.Enabled      := Hab;
  edtDescricaoBaixa.Enabled := Hab;
  mmObservacoes.Enabled     := Hab;

  dtePeriodoIni.Enabled         := not Hab;
  dtePeriodoFim.Enabled         := not Hab;
  cedValorFlutuante.Enabled     := not Hab;
  edtNumReciboFlutuante.Enabled := not Hab;
  rgSituacao.Enabled            := not Hab;
  btnFiltrar.Enabled            := not Hab;

  dbgFlutuantes.Enabled    := not Hab;
  dbgDespesasPagas.Enabled := Hab;
end;

procedure TFBaixaLancamento.DesabilitarComponentes;
begin
  inherited;

  btnOk.Visible := False;

  DesabilitaHabilitaCampos(False);
  HabDesabBotoes(False);

  if vgOperacao = C then
  begin
    dteDataBaixa.Enabled      := False;
    cedValorPago.Enabled      := False;
    edtDescricaoBaixa.Enabled := False;
    mmObservacoes.ReadOnly    := True;
  end;
end;

procedure TFBaixaLancamento.dteDataBaixaExit(Sender: TObject);
begin
  inherited;

  if (vgOperacao in [I, E]) and
    (dmBaixa.cdsBaixaLancI.State in [dsInsert, dsEdit]) then
  begin
    if (dDataPagtoOriginal = 0) and
      (dteDataBaixa.Date = 0) then
    begin
      if (dteDataBaixa.Date < vgDataLancVigente) or
        (dteDataBaixa.Date < dteDataEntrada.Date) then
      begin
        if vgDataLancVigente < dteDataEntrada.Date then
          dteDataBaixa.Date := dteDataEntrada.Date
        else
          dteDataBaixa.Date := vgDataLancVigente;
      end;
    end;
  end;
end;

procedure TFBaixaLancamento.dteDataBaixaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if (vgOperacao in [I, E]) and
      (dmBaixa.cdsBaixaLancI.State in [dsInsert, dsEdit]) then
    begin
      if (dDataPagtoOriginal = 0) and
        (dteDataBaixa.Date = 0) then
      begin
        if (dteDataBaixa.Date < vgDataLancVigente) or
          (dteDataBaixa.Date < dteDataEntrada.Date) then
        begin
          if vgDataLancVigente < dteDataEntrada.Date then
            dteDataBaixa.Date := dteDataEntrada.Date
          else
            dteDataBaixa.Date := vgDataLancVigente;
        end;
      end;
    end;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFBaixaLancamento.dtePeriodoFimKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then // TAB
  begin
    if dtePeriodoFim.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso',
        MB_OK + MB_ICONWARNING);

      if dtePeriodoFim.CanFocus then
        dtePeriodoFim.SetFocus;
    end
    else if dtePeriodoIni.Date > dtePeriodoFim.Date then
    begin
      Application.MessageBox
        ('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso',
        MB_OK + MB_ICONWARNING);

      dtePeriodoFim.Clear;

      if dtePeriodoFim.CanFocus then
        dtePeriodoFim.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    if dtePeriodoFim.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso',
        MB_OK + MB_ICONWARNING);

      if dtePeriodoFim.CanFocus then
        dtePeriodoFim.SetFocus;
    end
    else if dtePeriodoIni.Date > dtePeriodoFim.Date then
    begin
      Application.MessageBox
        ('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso',
        MB_OK + MB_ICONWARNING);

      dtePeriodoFim.Clear;

      if dtePeriodoFim.CanFocus then
        dtePeriodoFim.SetFocus;
    end
    else
      btnFiltrar.Click;

    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFBaixaLancamento.dtePeriodoIniKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then // TAB
  begin
    if dtePeriodoIni.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso',
        MB_OK + MB_ICONWARNING);

      if dtePeriodoIni.CanFocus then
        dtePeriodoIni.SetFocus;
    end
    else if dtePeriodoIni.Date > dtePeriodoFim.Date then
    begin
      Application.MessageBox
        ('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso',
        MB_OK + MB_ICONWARNING);

      dtePeriodoIni.Clear;

      if dtePeriodoIni.CanFocus then
        dtePeriodoIni.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    if dtePeriodoIni.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso',
        MB_OK + MB_ICONWARNING);

      if dtePeriodoIni.CanFocus then
        dtePeriodoIni.SetFocus;
    end
    else if dtePeriodoIni.Date > dtePeriodoFim.Date then
    begin
      Application.MessageBox
        ('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso',
        MB_OK + MB_ICONWARNING);

      dtePeriodoIni.Clear;

      if dtePeriodoIni.CanFocus then
        dtePeriodoIni.SetFocus;
    end
    else
      btnFiltrar.Click;

    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFBaixaLancamento.edtNumReciboFlutuanteKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then // TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    btnFiltrar.Click;
    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFBaixaLancamento.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if ModalResult = mrCancel then
    CanClose := True
  else
  begin
    if not lPodeFechar then
      CanClose := False;
  end;

  inherited;
end;

procedure TFBaixaLancamento.FormShow(Sender: TObject);
begin
  inherited;

  dtePeriodoIni.Date := StartOfTheMonth(Date);
  dtePeriodoFim.Date := Date;

  cedValorFlutuante.Value := 0;

  edtNumReciboFlutuante.Clear;

  rgSituacao.ItemIndex := 1;

  AbrirListaReceitasFlutuantes;

  cdsDespPagas.Close;
  cdsDespPagas.Open;

  lPodeFechar := False;

  if dtePeriodoIni.CanFocus then
    dtePeriodoIni.SetFocus;
end;

function TFBaixaLancamento.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFBaixaLancamento.GravarDadosGenerico(var Msg: String): Boolean;
var
  qryAux, QryLancDeriv: TFDQuery;
  iQtdParcsPagas, iQtdTotParcs, IdDetalhe, m: Integer;
  lIncParcelas: Boolean;
  IdNat, Cod, iCont: Integer;
  Descr, Obs, sIR, sCP, sLA: String;
  cTotalPrevL: Currency;
  dDtPagto: TDatetime;
begin
  Result := True;

  Msg := '';

  if vgOperacao = C then
    Exit;

  dDtPagto := dmBaixa.cdsBaixaLancI.FieldByName('DATA_PAGAMENTO').AsDateTime;

  QryLancDeriv := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);
  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

  iQtdParcsPagas := 0;
  iQtdTotParcs := 0;
  IdDetalhe := 0;
  cTotalPrevL := 0;

  lIncParcelas := False;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    dmBaixa.cdsBaixaLancI.FieldByName('DATA_PAGAMENTO').Value := Null;
    dmBaixa.cdsBaixaLancI.Post;

    cdsDespPagas.First;

    while not cdsDespPagas.Eof do
    begin
      if cdsDespPagas.FieldByName('SELECIONADO').AsBoolean then
      begin
        try
          if vgConSISTEMAAux.Connected then
            vgConSISTEMAAux.StartTransaction;

          QryLancDeriv.Close;
          QryLancDeriv.SQL.Clear;
          QryLancDeriv.SQL.Text :=
            'INSERT INTO BAIXA_LANC_DERIVADO (ID_BAIXA_LANC_DERIVADO, ' +
            '                                 DATA_BAIXA_LANC_DERIVADO, ' +
            '                                 DESCR_BAIXA_LANC_DERIVADO, ' +
            '                                 ORIG_COD_LANCAMENTO_FK, ' +
            '                                 ORIG_ANO_LANCAMENTO_FK, ' +
            '                                 DERIV_COD_LANCAMENTO_FK, ' +
            '                                 DERIV_ANO_LANCAMENTO_FK, ' +
            '                                 OBS_BAIXA_LANC_DERIVADO) ' +
            '                         VALUES (:ID_BAIXA_LANC_DERIVADO, ' +
            '                                 :DATA_BAIXA_LANC_DERIVADO, ' +
            '                                 :DESCR_BAIXA_LANC_DERIVADO, ' +
            '                                 :ORIG_COD_LANCAMENTO_FK, ' +
            '                                 :ORIG_ANO_LANCAMENTO_FK, ' +
            '                                 :DERIV_COD_LANCAMENTO_FK, ' +
            '                                 :DERIV_ANO_LANCAMENTO_FK, ' +
            '                                 :OBS_BAIXA_LANC_DERIVADO)';

          QryLancDeriv.Params.ParamByName('ID_BAIXA_LANC_DERIVADO').Value    := BS.ProximoId('ID_BAIXA_LANC_DERIVADO', 'BAIXA_LANC_DERIVADO');
          QryLancDeriv.Params.ParamByName('DESCR_BAIXA_LANC_DERIVADO').Value := dmBaixa.cdsBaixaLancI.FieldByName('DESCR_BAIXA_LANC_DERIVADO').AsString;
          QryLancDeriv.Params.ParamByName('ORIG_COD_LANCAMENTO_FK').Value    := dmBaixa.cdsBaixaLancI.FieldByName('COD_LANCAMENTO_FK').AsInteger;
          QryLancDeriv.Params.ParamByName('ORIG_ANO_LANCAMENTO_FK').Value    := dmBaixa.cdsBaixaLancI.FieldByName('ANO_LANCAMENTO_FK').AsInteger;
          QryLancDeriv.Params.ParamByName('DERIV_COD_LANCAMENTO_FK').Value   := cdsDespPagas.FieldByName('COD_LANCAMENTO_FK').AsInteger;
          QryLancDeriv.Params.ParamByName('DERIV_ANO_LANCAMENTO_FK').Value   := cdsDespPagas.FieldByName('ANO_LANCAMENTO_FK').AsInteger;
          QryLancDeriv.Params.ParamByName('OBS_BAIXA_LANC_DERIVADO').Value   := dmBaixa.cdsBaixaLancI.FieldByName('OBS_BAIXA_LANC_DERIVADO').AsString;

          if Date < vgDataLancVigente then
            QryLancDeriv.Params.ParamByName('DATA_BAIXA_LANC_DERIVADO').Value := vgDataLancVigente
          else
            QryLancDeriv.Params.ParamByName('DATA_BAIXA_LANC_DERIVADO').Value := Date;

          QryLancDeriv.ExecSQL;

          if vgConSISTEMAAux.InTransaction then
            vgConSISTEMAAux.Commit;
        except
          if vgConSISTEMAAux.InTransaction then
            vgConSISTEMAAux.Rollback;
        end;
      end;

      cdsDespPagas.Next;
    end;

    // Grava dados do Pagamento da Parcela
    if dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'O' then
    begin
      if not(dmBaixa.cdsBaixaLancI.State in [dsInsert, dsEdit]) then
        dmBaixa.cdsBaixaLancI.Edit;

      dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PAGO').AsCurrency := dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PREV').AsCurrency;
      dmBaixa.cdsBaixaLancI.Post;
    end
    else if dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'D' then
    begin

      if not(dmBaixa.cdsBaixaLancI.State in [dsInsert, dsEdit]) then
        dmBaixa.cdsBaixaLancI.Edit;

      dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PREV').AsCurrency := (dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PREV').AsCurrency -
                                                                          dmBaixa.cdsBaixaLancI.FieldByName('VR_DIFERENCA').AsCurrency);
      dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PAGO').AsCurrency := dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PAGO').AsCurrency;
      dmBaixa.cdsBaixaLancI.Post;
    end
    else if dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'S' then
    begin
      if not(dmBaixa.cdsBaixaLancI.State in [dsInsert, dsEdit]) then
        dmBaixa.cdsBaixaLancI.Edit;

      dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PREV').AsCurrency := dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PAGO').AsCurrency;
      dmBaixa.cdsBaixaLancI.Post;
    end
    else if dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'F' then
    begin

      if not(dmBaixa.cdsBaixaLancI.State in [dsInsert, dsEdit]) then
        dmBaixa.cdsBaixaLancI.Edit;

      dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PAGO').AsCurrency := dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PREV').AsCurrency;
      dmBaixa.cdsBaixaLancI.Post;
    end;

    dmBaixa.cdsBaixaLancI.ApplyUpdates(0);

    //Em caso de divergencia de Valores
    if not dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').IsNull then
    begin
      //Lancamento de Sobra ou Falta de Caixa
      if (dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'S') or
        (dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'F') then
      begin
        //Natureza
        IdNat := 0;
        Cod := 0;
        Descr := '';
        Obs := '';
        sIR := '';
        sCP := '';
        sLA := '';

        IdNat := 0;
        Obs := 'Inclus�o AUTOM�TICA de Natureza';

        if dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'S' then
        begin
          Cod := 2; //Receita
          Descr := 'SOBRA DE CAIXA';
        end
        else if dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'F'
        then
        begin
          Cod := 1; //Receita
          Descr := 'FALTA DE CAIXA';
        end;

        dmPrincipal.RetornaNatureza(IdNat, Cod, Descr, Obs);
        dmPrincipal.RetornarClassificacaoNatureza(IdNat, sIR, sCP, sLA);

        dmPrincipal.InicializarComponenteLancamento;

        //Lancamento
        DadosLancamento := Lancamento.Create;

        DadosLancamento.DataLancamento := dmBaixa.cdsBaixaLancI.FieldByName('DATA_LANCAMENTO_PARC').AsDateTime;

        if dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'S' then
          DadosLancamento.TipoLancamento := 'R'
        else if dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'F'
        then
          DadosLancamento.TipoLancamento := 'D';

        DadosLancamento.TipoCadastro     := 'A';
        DadosLancamento.FlgReal          := 'S';
        DadosLancamento.FlgIR            := sIR;
        DadosLancamento.FlgPessoal       := sCP;
        DadosLancamento.FlgFlutuante     := 'N';
        DadosLancamento.FlgAuxiliar      := sLA;
        DadosLancamento.FlgForaFechCaixa := 'N';
        DadosLancamento.IdNatureza       := IdNat;
        DadosLancamento.VlrTotalPrev     := dmBaixa.cdsBaixaLancI.FieldByName('VR_DIFERENCA').AsCurrency;
        DadosLancamento.VlrTotalPago     := dmBaixa.cdsBaixaLancI.FieldByName('VR_DIFERENCA').AsCurrency;
        DadosLancamento.FlgStatus        := 'G';

        if dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'S' then
        begin
          DadosLancamento.Observacao := 'Lan�amento gerado por pagamento de Parcela (' +
                                        dmBaixa.cdsBaixaLancI.FieldByName('COD_LANCAMENTO_FK').AsString +
                                        '/' + dmBaixa.cdsBaixaLancI.FieldByName('ANO_LANCAMENTO_FK').AsString +
                                        ' - Parc. N�: ' + dmBaixa.cdsBaixaLancI.FieldByName('NUM_PARCELA').AsString +
                                        ' - Recibo: ' + dmBaixa.cdsBaixaLancI.FieldByName('NUM_RECIBO').AsString +
                                        ') com valor acima do Previsto.';
        end
        else
        begin
          DadosLancamento.Observacao := 'Lan�amento gerado por pagamento de Parcela (' +
                                        dmBaixa.cdsBaixaLancI.FieldByName('COD_LANCAMENTO_FK').AsString +
                                        '/' + dmBaixa.cdsBaixaLancI.FieldByName('ANO_LANCAMENTO_FK').AsString +
                                        ' - Parc. N�: ' + dmBaixa.cdsBaixaLancI.FieldByName('NUM_PARCELA').AsString +
                                        ' - Recibo: ' + dmBaixa.cdsBaixaLancI.FieldByName('NUM_RECIBO').AsString +
                                        ') com valor abaixo do Previsto.';
        end;

        DadosLancamento.QtdParcelas   := 1;
        DadosLancamento.FlgRecorrente := 'N';
        DadosLancamento.FlgReplicado  := 'N';
        DadosLancamento.DataCadastro  := Now;
        DadosLancamento.CadIdUsuario  := vgUsu_Id;
        DadosLancamento.IdSistema     := dmBaixa.cdsBaixaLancI.FieldByName('ID_SISTEMA_FK').AsInteger;

        ListaLancamentos.Add(DadosLancamento);

        dmPrincipal.InsertLancamento(0);

        //Parcela
        DadosLancamentoParc := LancamentoParc.Create;

        DadosLancamentoParc.DataLancParc   := dmBaixa.cdsBaixaLancI.FieldByName('DATA_LANCAMENTO_PARC').AsDateTime;
        DadosLancamentoParc.NumParcela     := 1;
        DadosLancamentoParc.DataVencimento := dDtPagto;  //dmBaixa.cdsBaixaLancI.FieldByName('DATA_PAGAMENTO').AsDateTime;
        DadosLancamentoParc.VlrPrevisto    := dmBaixa.cdsBaixaLancI.FieldByName('VR_DIFERENCA').AsCurrency;
        DadosLancamentoParc.IdFormaPagto   := dmBaixa.cdsBaixaLancI.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger;
        DadosLancamentoParc.CadIdUsuario   := vgUsu_Id;
        DadosLancamentoParc.DataCadastro   := Now;
        DadosLancamentoParc.VlrPago        := dmBaixa.cdsBaixaLancI.FieldByName('VR_DIFERENCA').AsCurrency;
        DadosLancamentoParc.FlgStatus      := 'G';
        DadosLancamentoParc.PagIdUsuario   := vgUsu_Id;
        DadosLancamentoParc.DataPagamento  := dDtPagto;  //dmBaixa.cdsBaixaLancI.FieldByName('DATA_PAGAMENTO').AsDateTime;

        ListaLancamentoParcs.Add(DadosLancamentoParc);

        dmPrincipal.InsertLancamentoParc(0, 0);

        dmPrincipal.FinalizarComponenteLancamento;
      end;

      //Observacao de Sobra ou Falta de Caixa
      if dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'O' then
      begin
        iCont := 0;

        dmPrincipal.InicializarComponenteLancamento;

        //FLUTUANTE
        DadosLancamento := Lancamento.Create;

        DadosLancamento.CodLancamento := dmBaixa.cdsBaixaLancI.FieldByName('COD_LANCAMENTO_FK').AsInteger;
        DadosLancamento.AnoLancamento := dmBaixa.cdsBaixaLancI.FieldByName('ANO_LANCAMENTO_FK').AsInteger;
        DadosLancamento.VlrTotalPago := dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PREV').AsCurrency;

        ListaLancamentos.Add(DadosLancamento);

        dmPrincipal.UpdateLancamento(iCont);

        //DESPESAS
        cdsDespPagas.First;

        while not cdsDespPagas.Eof do
        begin
          if cdsDespPagas.FieldByName('SELECIONADO').AsBoolean then
          begin
            Inc(iCont);

            DadosLancamento := Lancamento.Create;

            DadosLancamento.CodLancamento := cdsDespPagas.FieldByName('COD_LANCAMENTO_FK').AsInteger;
            DadosLancamento.AnoLancamento := cdsDespPagas.FieldByName('ANO_LANCAMENTO_FK').AsInteger;

            if Trim(cdsDespPagas.FieldByName('OBSERVACAO').AsString) = '' then
              DadosLancamento.Observacao := sNovaObservacao
            else
              DadosLancamento.Observacao := cdsDespPagas.FieldByName('OBSERVACAO').AsString + #13#10 + sNovaObservacao;

            ListaLancamentos.Add(DadosLancamento);

            dmPrincipal.UpdateLancamento(iCont);
          end;

          cdsDespPagas.Next;
        end;

        dmPrincipal.FinalizarComponenteLancamento;
      end;

      //Desmembramento de Flutuante
      if dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'D' then
      begin
        IdDetalhe := BS.ProximoId('ID_LANCAMENTO_DET', 'LANCAMENTO_DET');

        dmPrincipal.InicializarComponenteLancamento;

        //Lancamento
        DadosLancamento := Lancamento.Create;

        DadosLancamento.DataLancamento := qryFlutuantes.FieldByName('DATA_LANCAMENTO').AsDateTime;
        DadosLancamento.TipoLancamento := qryFlutuantes.FieldByName('TIPO_LANCAMENTO').AsString;
        DadosLancamento.TipoCadastro := 'A';

        if qryFlutuantes.FieldByName('ID_CLIENTE_FORNECEDOR_FK').IsNull then
          DadosLancamento.IdCliFor := 0
        else
          DadosLancamento.IdCliFor := qryFlutuantes.FieldByName('ID_CLIENTE_FORNECEDOR_FK').AsInteger;

        if qryFlutuantes.FieldByName('ID_CATEGORIA_DESPREC_FK').IsNull then
          DadosLancamento.IdCategoria := 0
        else
          DadosLancamento.IdCategoria := qryFlutuantes.FieldByName('ID_CATEGORIA_DESPREC_FK').AsInteger;

        if qryFlutuantes.FieldByName('ID_SUBCATEGORIA_DESPREC_FK').IsNull then
          DadosLancamento.IdSubCategoria := 0
        else
          DadosLancamento.IdSubCategoria := qryFlutuantes.FieldByName('ID_SUBCATEGORIA_DESPREC_FK').AsInteger;

        DadosLancamento.FlgReal          := qryFlutuantes.FieldByName('FLG_REAL').AsString;
        DadosLancamento.FlgIR            := qryFlutuantes.FieldByName('FLG_IMPOSTORENDA').AsString;
        DadosLancamento.FlgPessoal       := qryFlutuantes.FieldByName('FLG_PESSOAL').AsString;
        DadosLancamento.FlgFlutuante     := qryFlutuantes.FieldByName('FLG_FLUTUANTE').AsString;
        DadosLancamento.FlgAuxiliar      := qryFlutuantes.FieldByName('FLG_AUXILIAR').AsString;
        DadosLancamento.FlgForaFechCaixa := qryFlutuantes.FieldByName('FLG_FORAFECHCAIXA').AsString;
        DadosLancamento.IdNatureza       := qryFlutuantes.FieldByName('ID_NATUREZA_FK').AsInteger;
        DadosLancamento.VlrTotalPrev     := dmBaixa.cdsBaixaLancI.FieldByName('VR_DIFERENCA').AsCurrency;

        if Trim(qryFlutuantes.FieldByName('FLG_STATUS').AsString) = 'G' then
        begin
          DadosLancamento.VlrTotalJuros    := qryFlutuantes.FieldByName('VR_TOTAL_JUROS').AsCurrency;
          DadosLancamento.VlrTotalDesconto := qryFlutuantes.FieldByName('VR_TOTAL_DESCONTO').AsCurrency;
          DadosLancamento.VlrTotalPago     := dmBaixa.cdsBaixaLancI.FieldByName('VR_DIFERENCA').AsCurrency;
        end
        else
        begin
          DadosLancamento.VlrTotalJuros    := 0;
          DadosLancamento.VlrTotalDesconto := 0;
          DadosLancamento.VlrTotalPago     := 0;
        end;

        DadosLancamento.FlgStatus      := qryFlutuantes.FieldByName('FLG_STATUS').AsString;
        DadosLancamento.NumRecibo      := qryFlutuantes.FieldByName('NUM_RECIBO').AsInteger;
        DadosLancamento.IdOrigem       := qryFlutuantes.FieldByName('ID_ORIGEM').AsInteger;
        DadosLancamento.IdEquivalencia := qryFlutuantes.FieldByName('ID_CARNELEAO_EQUIVALENCIA_FK').AsInteger;
        DadosLancamento.Observacao     := 'Lan�amento gerado por desmembramento de Parcela (' +
                                          qryFlutuantes.FieldByName('COD_LANCAMENTO').AsString + '/' +
                                          qryFlutuantes.FieldByName('ANO_LANCAMENTO').AsString +
                                          ' - Parc. N�: ' + qryFlutuantes.FieldByName('NUM_PARCELA').AsString +
                                          ' - Recibo: ' + qryFlutuantes.FieldByName('NUM_RECIBO').AsString +
                                          ') com valor acima do Previsto.';
        DadosLancamento.QtdParcelas    := 1;
        DadosLancamento.FlgRecorrente  := 'N';
        DadosLancamento.FlgReplicado   := 'N';
        DadosLancamento.DataCadastro   := Now;
        DadosLancamento.CadIdUsuario   := vgUsu_Id;
        DadosLancamento.IdSistema      := qryFlutuantes.FieldByName('ID_SISTEMA_FK').AsInteger;

        ListaLancamentos.Add(DadosLancamento);

        dmPrincipal.InsertLancamento(0);

        //Detalhe
        qryAux.Close;
        qryAux.SQL.Clear;

        qryAux.SQL.Text := 'SELECT * FROM LANCAMENTO_DET ' +
                           '  WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                           '    AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ' +
                           '    AND FLG_CANCELADO = ' + QuotedStr('N') +
                           'ORDER BY ID_LANCAMENTO_DET';

        qryAux.Params.ParamByName('COD_LANCAMENTO').Value := qryFlutuantes.FieldByName('COD_LANCAMENTO').AsString;
        qryAux.Params.ParamByName('ANO_LANCAMENTO').Value := qryFlutuantes.FieldByName('ANO_LANCAMENTO').AsString;
        qryAux.Open;

        qryAux.First;
        m := 0;

        while not qryAux.Eof do
        begin
          DadosLancamentoDet := LancamentoDet.Create;

          DadosLancamentoDet.IdLancamentoDet := IdDetalhe;
          DadosLancamentoDet.IdItem := qryAux.FieldByName('ID_ITEM_FK').AsInteger;
          DadosLancamentoDet.Quantidade := qryAux.FieldByName('QUANTIDADE').AsInteger;
          DadosLancamentoDet.VlrUnitario := qryAux.FieldByName('VR_UNITARIO').AsCurrency;
          DadosLancamentoDet.VlrTotal := qryAux.FieldByName('VR_TOTAL').AsCurrency;

          if Trim(qryAux.FieldByName('SELO_ORIGEM').AsString) <> '' then
            DadosLancamentoDet.SeloOrigem := qryAux.FieldByName('SELO_ORIGEM').AsString;

          if Trim(qryAux.FieldByName('ALEATORIO_ORIGEM').AsString) <> '' then
            DadosLancamentoDet.AleatorioOrigem := qryAux.FieldByName('ALEATORIO_ORIGEM').AsString;

          if Trim(qryAux.FieldByName('TIPO_COBRANCA').AsString) <> '' then
            DadosLancamentoDet.TipoCobranca := qryAux.FieldByName('TIPO_COBRANCA').AsString;

          if not qryAux.FieldByName('COD_ADICIONAL').IsNull then
            DadosLancamentoDet.CodAdicional := qryAux.FieldByName('COD_ADICIONAL').AsInteger;

          if not qryAux.FieldByName('EMOLUMENTOS').IsNull then
            DadosLancamentoDet.Emolumentos := qryAux.FieldByName('EMOLUMENTOS').AsCurrency;

          if not qryAux.FieldByName('FETJ').IsNull then
            DadosLancamentoDet.FETJ := qryAux.FieldByName('FETJ').AsCurrency;

          if not qryAux.FieldByName('FUNDPERJ').IsNull then
            DadosLancamentoDet.FUNDPERJ := qryAux.FieldByName('FUNDPERJ').AsCurrency;

          if not qryAux.FieldByName('FUNPERJ').IsNull then
            DadosLancamentoDet.FUNPERJ := qryAux.FieldByName('FUNPERJ').AsCurrency;

          if not qryAux.FieldByName('FUNARPEN').IsNull then
            DadosLancamentoDet.FUNARPEN := qryAux.FieldByName('FUNARPEN').AsCurrency;

          if not qryAux.FieldByName('PMCMV').IsNull then
            DadosLancamentoDet.PMCMV := qryAux.FieldByName('PMCMV').AsCurrency;

          if not qryAux.FieldByName('ISS').IsNull then
            DadosLancamentoDet.ISS := qryAux.FieldByName('ISS').AsCurrency;

          if not qryAux.FieldByName('MUTUA').IsNull then
            DadosLancamentoDet.Mutua := qryAux.FieldByName('MUTUA').AsCurrency;

          if not qryAux.FieldByName('ACOTERJ').IsNull then
            DadosLancamentoDet.Acoterj := qryAux.FieldByName('ACOTERJ').AsCurrency;

          if not qryAux.FieldByName('NUM_PROTOCOLO').IsNull then
            DadosLancamentoDet.NumProtocolo := qryAux.FieldByName('NUM_PROTOCOLO').AsInteger;

          ListaLancamentoDets.Add(DadosLancamentoDet);

          dmPrincipal.InsertLancamentoDet(m, 0);

          IdDetalhe := ListaLancamentoDets[m].IdLancamentoDet;

          Inc(IdDetalhe);
          Inc(m);

          qryAux.Next;
        end;

        //Parcela
        DadosLancamentoParc := LancamentoParc.Create;

        DadosLancamentoParc.DataLancParc   := qryFlutuantes.FieldByName('DATA_LANCAMENTO_PARC').AsDateTime;
        DadosLancamentoParc.NumParcela     := 1;
        DadosLancamentoParc.DataVencimento := qryFlutuantes.FieldByName('DATA_VENCIMENTO').AsDateTime;
        DadosLancamentoParc.VlrPrevisto    := dmBaixa.cdsBaixaLancI.FieldByName('VR_DIFERENCA').AsCurrency;
        DadosLancamentoParc.IdFormaPagto   := qryFlutuantes.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger;
        DadosLancamentoParc.CadIdUsuario   := vgUsu_Id;
        DadosLancamentoParc.DataCadastro   := Now;

        if Trim(qryFlutuantes.FieldByName('FLG_STATUS_PARC').AsString) = 'G' then
        begin
          DadosLancamentoParc.VlrJuros      := qryFlutuantes.FieldByName('VR_PARCELA_JUROS').AsCurrency;
          DadosLancamentoParc.VlrDesconto   := qryFlutuantes.FieldByName('VR_PARCELA_DESCONTO').AsCurrency;
          DadosLancamentoParc.VlrPago       := dmBaixa.cdsBaixaLancI.FieldByName('VR_DIFERENCA').AsCurrency;
          DadosLancamentoParc.FlgStatus     := qryFlutuantes.FieldByName('FLG_STATUS_PARC').AsString;
          DadosLancamentoParc.PagIdUsuario  := vgUsu_Id;
          DadosLancamentoParc.DataPagamento := qryFlutuantes.FieldByName('DATA_PAGAMENTO').AsDateTime;
        end;

        DadosLancamentoParc.Observacao := qryFlutuantes.FieldByName('OBS_LANCAMENTO_PARC').AsString;

        ListaLancamentoParcs.Add(DadosLancamentoParc);

        dmPrincipal.InsertLancamentoParc(0, 0);

        dmPrincipal.FinalizarComponenteLancamento;
      end;

      //Nova Parcela
      if dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'P' then
      begin
        lIncParcelas := True;

        dmPrincipal.InicializarComponenteLancamento;

        //Lancamento
        DadosLancamento := Lancamento.Create;

        DadosLancamento.CodLancamento := dmBaixa.cdsBaixaLancI.FieldByName('COD_LANCAMENTO_FK').AsInteger;
        DadosLancamento.AnoLancamento := dmBaixa.cdsBaixaLancI.FieldByName('ANO_LANCAMENTO_FK').AsInteger;
        DadosLancamento.VlrTotalPago  := dmBaixa.cdsBaixaLancI.FieldByName('VR_PARCELA_PAGO').AsCurrency;
        DadosLancamento.QtdParcelas   := (qryFlutuantes.FieldByName('QTD_PARCELAS').AsInteger + 1);

        ListaLancamentos.Add(DadosLancamento);

        dmPrincipal.UpdateLancamento(0);

        //Parcela Nova
        DadosLancamentoParc := LancamentoParc.Create;

        DadosLancamentoParc.DataLancParc   := dmBaixa.cdsBaixaLancI.FieldByName('DATA_LANCAMENTO_PARC').AsDateTime;
        DadosLancamentoParc.CodLancamento  := dmBaixa.cdsBaixaLancI.FieldByName('COD_LANCAMENTO_FK').AsInteger;
        DadosLancamentoParc.AnoLancamento  := dmBaixa.cdsBaixaLancI.FieldByName('ANO_LANCAMENTO_FK').AsInteger;
        DadosLancamentoParc.NumParcela     := (qryFlutuantes.FieldByName('QTD_PARCELAS').AsInteger + 1);
        DadosLancamentoParc.DataVencimento := IncMonth(Date, 1);
        DadosLancamentoParc.VlrPrevisto    := dmBaixa.cdsBaixaLancI.FieldByName('VR_DIFERENCA').AsCurrency;
        DadosLancamentoParc.IdFormaPagto   := dmBaixa.cdsBaixaLancI.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger;
        DadosLancamentoParc.FlgStatus      := 'P';
        DadosLancamentoParc.CadIdUsuario   := vgUsu_Id;
        DadosLancamentoParc.DataCadastro   := Now;
        DadosLancamentoParc.Observacao     := 'Parcela gerada por valor insuficiente no pagamento da parcela N� ' +
                                              dmBaixa.cdsBaixaLancI.FieldByName('NUM_PARCELA').AsString +
                                              ' - Recibo: ' + dmBaixa.cdsBaixaLancI.FieldByName('NUM_RECIBO').AsString;

        ListaLancamentoParcs.Add(DadosLancamentoParc);

        dmPrincipal.InsertLancamentoParc(0, 0);

        dmPrincipal.FinalizarComponenteLancamento;
      end;
    end;

    if lIncParcelas then
      iQtdTotParcs := (qryFlutuantes.FieldByName('QTD_PARCELAS').AsInteger + 1)
    else
      iQtdTotParcs := qryFlutuantes.FieldByName('QTD_PARCELAS').AsInteger;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    //Quitacao do Lancamento
    qryQtdParcs.Close;
    qryQtdParcs.Params.ParamByName('COD_LANCAMENTO').Value := dmBaixa.cdsBaixaLancI.FieldByName('COD_LANCAMENTO_FK').AsInteger;
    qryQtdParcs.Params.ParamByName('ANO_LANCAMENTO').Value := dmBaixa.cdsBaixaLancI.FieldByName('ANO_LANCAMENTO_FK').AsInteger;
    qryQtdParcs.Open;

    iQtdParcsPagas := qryQtdParcs.FieldByName('QTD').AsInteger;

    //Acertar valores do Lancamento
    try
      if vgConSISTEMAAux.Connected then
        vgConSISTEMAAux.StartTransaction;

      vgLanc_Codigo := dmBaixa.cdsBaixaLancI.FieldByName('COD_LANCAMENTO_FK').AsInteger;
      vgLanc_Ano := dmBaixa.cdsBaixaLancI.FieldByName('ANO_LANCAMENTO_FK').AsInteger;

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Text := 'UPDATE LANCAMENTO ' +
                         '   SET VR_TOTAL_PREV = :VR_TOTAL_PREV ' +
                         ' WHERE COD_LANCAMENTO = :COD_LANCAMENTO ' +
                         '   AND ANO_LANCAMENTO = :ANO_LANCAMENTO';

      if dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'S' then
        qryAux.Params.ParamByName('VR_TOTAL_PREV').Value := (qryQtdParcs.FieldByName('TOTAL_PREV').AsCurrency -
                                                             dmBaixa.cdsBaixaLancI.FieldByName('VR_DIFERENCA').AsCurrency)
      else if dmBaixa.cdsBaixaLancI.FieldByName('TP_DIFERENCA').AsString = 'F'
      then
        qryAux.Params.ParamByName('VR_TOTAL_PREV').Value := (qryQtdParcs.FieldByName('TOTAL_PREV').AsCurrency +
                                                             dmBaixa.cdsBaixaLancI.FieldByName('VR_DIFERENCA').AsCurrency);

      qryAux.Params.ParamByName('VR_TOTAL_PREV').Value := qryQtdParcs.FieldByName('TOTAL_PREV').AsCurrency;
      qryAux.Params.ParamByName('COD_LANCAMENTO').Value := vgLanc_Codigo;
      qryAux.Params.ParamByName('ANO_LANCAMENTO').Value := vgLanc_Ano;
      qryAux.ExecSQL;

{      if iQtdTotParcs = iQtdParcsPagas then
      begin
        Application.CreateForm(TdmLancamento, dmLancamento);
        dmLancamento.QuitarLancamento(qryQtdParcs.FieldByName('TOTAL_PAGO').AsCurrency);
        FreeAndNil(dmLancamento);
      end;  }

      vgLanc_Codigo := 0;
      vgLanc_Ano := 0;

      if vgConSISTEMAAux.InTransaction then
        vgConSISTEMAAux.Commit;
    except
      if vgConSISTEMAAux.InTransaction then
        vgConSISTEMAAux.Rollback;
    end;

    FreeAndNil(QryLancDeriv);
    FreeAndNil(qryAux);

    Msg := 'Baixa gravada com sucesso!';
  except
    on E: Exception do
    begin
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Result := False;

      Msg := 'Erro na grava��o da Baixa';
    end;
  end;
end;

procedure TFBaixaLancamento.GravarLog;
begin
  inherited;

  BS.GravarUsuarioLog(2, '', PChar('Baixa de Lan�amento (C�digo: ' +
                                   qryFlutuantes.FieldByName('COD_LANCAMENTO').AsString + '/Ano:' +
                                   qryFlutuantes.FieldByName('ANO_LANCAMENTO').AsString + ')'),
                      qryFlutuantes.FieldByName('ID_LANCAMENTO_PARC').AsInteger,
                      'LANCAMENTO_PARC');
end;

procedure TFBaixaLancamento.HabDesabBotoes(Hab: Boolean);
begin
  btnConfirmarBaixa.Enabled := Hab;
  btnCancelarBaixa.Enabled := Hab;
end;

procedure TFBaixaLancamento.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFBaixaLancamento.lcbFormaPagamentoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then // TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    btnFiltrar.Click;
    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFBaixaLancamento.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

procedure TFBaixaLancamento.mmObservacoesKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then // TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    btnConfirmarBaixa.Click;
    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    btnCancelarBaixa.Click;
    Key := #0;
  end;
end;

procedure TFBaixaLancamento.qryFlutuantesVR_PARCELA_DESCONTOGetText
  (Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFBaixaLancamento.qryFlutuantesVR_PARCELA_JUROSGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFBaixaLancamento.qryFlutuantesVR_PARCELA_PAGOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFBaixaLancamento.qryFlutuantesVR_PARCELA_PREVGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFBaixaLancamento.qryFlutuantesVR_TOTAL_DESCONTOGetText
  (Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFBaixaLancamento.qryFlutuantesVR_TOTAL_JUROSGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFBaixaLancamento.qryFlutuantesVR_TOTAL_PAGOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFBaixaLancamento.qryFlutuantesVR_TOTAL_PREVGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFBaixaLancamento.rgSituacaoClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFBaixaLancamento.rgSituacaoExit(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFBaixaLancamento.SalvarBoleano;
begin
  dbgDespesasPagas.SelectedField.DataSet.Edit;
  dbgDespesasPagas.SelectedField.AsBoolean := not dbgDespesasPagas.SelectedField.AsBoolean;
  dbgDespesasPagas.SelectedField.DataSet.Post;
end;

function TFBaixaLancamento.VerificarDadosGenerico(var QtdErros
  : Integer): Boolean;
begin
  Result := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if dteDataBaixa.Date = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg := QtdErros;
    DadosMsgErro.Mensagem := IntToStr(QtdErros + 1) + ') Falta informar a DATA DA BAIXA.';
    DadosMsgErro.Componente := dteDataBaixa;
    DadosMsgErro.PageIndex := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if cedValorPago.Value = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg := QtdErros;
    DadosMsgErro.Mensagem := IntToStr(QtdErros + 1) + ') Falta informar o VALOR DA BAIXA.';
    DadosMsgErro.Componente := cedValorPago;
    DadosMsgErro.PageIndex := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if QtdErros > 0 then
    Result := False;
end;

function TFBaixaLancamento.VerificarLayoutAto(var QtdErros: Integer): Boolean;
begin
  //
end;

end.
