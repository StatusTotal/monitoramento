inherited FListaCarneLeaoPendentes: TFListaCarneLeaoPendentes
  Caption = 'Lista de Equival'#234'ncias Pendentes do Carn'#234'-Le'#227'o'
  ClientHeight = 477
  ClientWidth = 982
  ExplicitWidth = 988
  ExplicitHeight = 506
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 976
    Height = 471
    ExplicitWidth = 976
    ExplicitHeight = 471
    inherited pnlGrid: TPanel
      Top = 40
      Width = 970
      Height = 428
      ExplicitTop = 40
      ExplicitWidth = 970
      ExplicitHeight = 428
      object btnRelacionarPLC: TJvTransparentButton [0]
        Left = 0
        Top = 219
        Width = 970
        Height = 22
        Hint = 'Confirmar inclus'#227'o/edi'#231#227'o da Equival'#234'ncia selecionada'
        Caption = '&Relacionar Plano de Contas'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        Spacing = 402
        TextAlign = ttaRight
        Transparent = False
        OnClick = btnRelacionarPLCClick
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000014331348050B0410000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000030603093F9D40DE1E4C1F6C050D0512193D1957265F27862B6C2C99265F
          2786122E13410101010200000000000000000000000000000000000000000000
          0000245B288047B64FFF44AE4CF446B44EFC47B64FFF47B64FFF47B64FFF47B6
          4FFF47B64FFF3A9541D109180B22000000000000000000000000000000000918
          0B2144B151F647B754FF47B754FF47B754FF47B754FF47B754FF47B754FF47B7
          54FF47B754FF47B754FF3D9D48DB040A050E000000000000000000000000307F
          3EB046B85AFF46B85AFF46B85AFF46B85AFF46B85AFF46B85AFF46B85AFF46B8
          5AFF46B85AFF46B85AFF46B85AFF2664318A000000000000000014351B4945BA
          5FFF45BA5FFF45BA5FFF45BA5FFF45BA5FFF45BA5FFF42B15BF337944CCB2F7F
          41AE2F7E40AD3DA454E145BA5FFF41AE59EF02040206000000002159307A3DA6
          5AE245BB65FF45BB65FF45BB65FF45BB65FF2C7841A30309050C000000000000
          00000000000000000000102C183C348D4CC0112E193F00000000000000000000
          00000C20122C1F563074328A4EBB42B767F8379956CF000000000A07010E3426
          044B0806010B00000000000000000102010308170D1F00000000000000001610
          011F0202000300000000000000000308050B1439214C030A060D00000000966D
          0AD2B1800CF8856109BB523C05731F16022B0000000000000000000000003325
          04458B660ABD2A1F0339000000000000000000000000000000000906010C7959
          08A5BB890DFFBB890DFFBB890DFFBB890DFFA6790CE257400677000000000705
          0009B8860CF3C18D0DFFA97B0BDF825F09AC846009AE9A710ACCB9870CF4C18D
          0DFFC18D0DFFC18D0DFFC18D0DFFC18D0DFFC18D0DFF35270446000000000000
          000071540892C6920EFFC6920EFFC6920EFFC6920EFFC6920EFFC6920EFFC692
          0EFFC6920EFFC6920EFFC6920EFFC6920EFF866309AD00000000000000000000
          00000E0A0111B2840CDFCC970EFFCC970EFFCC970EFFCC970EFFCC970EFFCC97
          0EFFCC970EFFCC970EFFCC970EFFC4910DF51912021F00000000000000000000
          0000000000001E160225AF810DD5D19B0FFFD19B0FFFD19B0FFFD19B0FFFD19B
          0FFFCF990FFCC9950EF5D19B0FFF664B077C0000000000000000000000000000
          0000000000000000000003020003382A04437154088681600A99715408864936
          05560F0B01125E460770B9890EDB070501080000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000100C01123D2D0446000000000000000000000000}
      end
      inherited dbgGridListaPadrao: TDBGrid
        Top = 247
        Width = 970
        Height = 181
        Align = alBottom
        Font.Height = -11
        ParentFont = False
        TabOrder = 1
        Columns = <
          item
            Alignment = taLeftJustify
            Expanded = False
            FieldName = 'COD_LANCAMENTO'
            Title.Caption = 'C'#243'd.'
            Width = 55
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ANO_LANCAMENTO'
            Title.Caption = 'Ano'
            Width = 45
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TIPO_LANCAMENTO'
            Title.Alignment = taCenter
            Title.Caption = 'Tipo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA_LANCAMENTO'
            Title.Caption = 'Dt. Lan'#231'amento'
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'NUM_RECIBO'
            Title.Alignment = taCenter
            Title.Caption = 'N'#186' Rec.'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TIPO_CADASTRO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_NATUREZA_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DESCR_NATUREZA'
            Title.Caption = 'Natureza'
            Width = 115
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ID_CATEGORIA_DESPREC_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DESCR_CATEGORIA_DESPREC'
            Title.Caption = 'Categoria'
            Width = 115
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ID_SUBCATEGORIA_DESPREC_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DESCR_SUBCATEGORIA_DESPREC'
            Title.Caption = 'Subcategoria'
            Width = 115
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ID_CLIENTE_FORNECEDOR_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_IMPOSTORENDA'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_PESSOAL'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_AUXILIAR'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_REAL'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'QTD_PARCELAS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_TOTAL_PREV'
            Title.Alignment = taRightJustify
            Title.Caption = 'Vlr. Tot. Prev.'
            Width = 85
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VR_TOTAL_JUROS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_TOTAL_DESCONTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_TOTAL_PAGO'
            Title.Alignment = taRightJustify
            Title.Caption = 'Vlr. Tot. Pago'
            Width = 85
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CAD_ID_USUARIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_CADASTRO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_STATUS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_CANCELADO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_CANCELAMENTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'CANCEL_ID_USUARIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_RECORRENTE'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_REPLICADO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_ORIGEM'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_SISTEMA_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'OBSERVACAO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_FLUTUANTE'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_CARNELEAO_EQUIVALENCIA_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DESCR_CARNELEAO_CLASSIFICACAO'
            Title.Caption = 'Carn'#234'-Le'#227'o'
            Width = 132
            Visible = True
          end>
      end
      object chbSelecionarTodos: TCheckBox
        Left = 0
        Top = 0
        Width = 113
        Height = 17
        Caption = 'Marcar Todos'
        TabOrder = 0
        OnClick = chbSelecionarTodosClick
      end
      object rgCarneLeao: TRadioGroup
        Left = 696
        Top = 0
        Width = 274
        Height = 213
        Caption = 'Carn'#234'-Le'#227'o'
        Items.Strings = (
          'TRABALHO N'#195'O ASSALARIADO (R)'
          'ALUGU'#201'IS (R)'
          'OUTROS (R)'
          'EXTERIOR (R)'
          'PREVID'#202'NCIA OFICIAL (D)'
          'DEPENDENTES (D)'
          'PENS'#195'O ALIMENT'#205'CIA (D)'
          'LIVRO CAIXA (D)'
          'IMPOSTO NO EXTERIOR A COMPENSAR (D)'
          'IMPOSTO PAGO (D)')
        TabOrder = 2
      end
      object dbgEquivP: TDBGrid
        Left = 0
        Top = 19
        Width = 690
        Height = 194
        DataSource = dsEquivPend_F
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 3
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnCellClick = dbgEquivPCellClick
        OnColEnter = dbgEquivPColEnter
        OnColExit = dbgEquivPColExit
        OnDrawColumnCell = dbgEquivPDrawColumnCell
        OnKeyPress = FormKeyPress
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'SELECIONADO'
            Title.Alignment = taCenter
            Title.Caption = 'Sel.'
            Width = 25
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ID_CARNELEAO_EQUIVALENCIA'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'M_TIPO_LANCAMENTO'
            Title.Caption = 'Tipo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'M_ID_CATEGORIA_DESPREC_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'M_ID_SUBCATEGORIA_DESPREC_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'M_ID_NATUREZA_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_CARNELEAO_CLASSIFICACAO_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'OBS_CARNELEAO_EQUIVALENCIA'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_CADASTRO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'CAD_ID_USUARIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_CANCELADO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_CANCELAMENTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'CANCEL_ID_USUARIO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DESCR_NATUREZA'
            Title.Caption = 'Natureza'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCR_CATEGORIA_DESPREC'
            Title.Caption = 'Categoria'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCR_SUBCATEGORIA_DESPREC'
            Title.Caption = 'Subcategoria'
            Width = 200
            Visible = True
          end>
      end
    end
    inherited pnlFiltro: TPanel
      Width = 976
      Height = 37
      ExplicitWidth = 976
      ExplicitHeight = 37
      inherited btnFiltrar: TJvTransparentButton
        Left = 801
        Top = 8
        Visible = False
        ExplicitLeft = 801
        ExplicitTop = 8
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 890
        Top = 8
        Visible = False
        ExplicitLeft = 890
        ExplicitTop = 8
      end
    end
  end
  inherited dsGridListaPadrao: TDataSource
    DataSet = cdsGridListaPadrao
    Left = 876
    Top = 399
  end
  inherited qryGridListaPadrao: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    Left = 587
    Top = 399
  end
  object dsEquivPend_F: TDataSource
    DataSet = cdsEquivPend_F
    Left = 630
    Top = 171
  end
  object qryEquivPend_F: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT PLE.*'
      '  FROM CARNELEAO_EQUIVALENCIA PLE'
      ' WHERE (1 = 0)')
    Left = 366
    Top = 171
  end
  object dspEquivPend_F: TDataSetProvider
    DataSet = qryEquivPend_F
    Left = 454
    Top = 171
  end
  object cdsEquivPend_F: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspEquivPend_F'
    OnCalcFields = cdsEquivPend_FCalcFields
    Left = 542
    Top = 171
    object cdsEquivPend_FSELECIONADO: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'SELECIONADO'
    end
    object cdsEquivPend_FID_CARNELEAO_EQUIVALENCIA: TIntegerField
      FieldName = 'ID_CARNELEAO_EQUIVALENCIA'
      Required = True
    end
    object cdsEquivPend_FM_TIPO_LANCAMENTO: TStringField
      FieldName = 'M_TIPO_LANCAMENTO'
      FixedChar = True
      Size = 1
    end
    object cdsEquivPend_FM_ID_CATEGORIA_DESPREC_FK: TIntegerField
      FieldName = 'M_ID_CATEGORIA_DESPREC_FK'
    end
    object cdsEquivPend_FM_ID_SUBCATEGORIA_DESPREC_FK: TIntegerField
      FieldName = 'M_ID_SUBCATEGORIA_DESPREC_FK'
    end
    object cdsEquivPend_FM_ID_NATUREZA_FK: TIntegerField
      FieldName = 'M_ID_NATUREZA_FK'
    end
    object cdsEquivPend_FID_CARNELEAO_CLASSIFICACAO_FK: TIntegerField
      FieldName = 'ID_CARNELEAO_CLASSIFICACAO_FK'
    end
    object cdsEquivPend_FOBS_CARNELEAO_EQUIVALENCIA: TStringField
      FieldName = 'OBS_CARNELEAO_EQUIVALENCIA'
      Size = 300
    end
    object cdsEquivPend_FDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsEquivPend_FCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsEquivPend_FFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsEquivPend_FDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsEquivPend_FCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsEquivPend_FDESCR_CATEGORIA_DESPREC: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCR_CATEGORIA_DESPREC'
      Size = 250
    end
    object cdsEquivPend_FDESCR_SUBCATEGORIA_DESPREC: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCR_SUBCATEGORIA_DESPREC'
      Size = 250
    end
    object cdsEquivPend_FDESCR_NATUREZA: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCR_NATUREZA'
      Size = 250
    end
  end
  object dspGridListaPadrao: TDataSetProvider
    DataSet = qryGridListaPadrao
    Left = 684
    Top = 399
  end
  object cdsGridListaPadrao: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspGridListaPadrao'
    OnCalcFields = cdsGridListaPadraoCalcFields
    Left = 782
    Top = 399
    object cdsGridListaPadraoCOD_LANCAMENTO: TIntegerField
      FieldName = 'COD_LANCAMENTO'
      Required = True
    end
    object cdsGridListaPadraoANO_LANCAMENTO: TIntegerField
      FieldName = 'ANO_LANCAMENTO'
      Required = True
    end
    object cdsGridListaPadraoTIPO_LANCAMENTO: TStringField
      FieldName = 'TIPO_LANCAMENTO'
      FixedChar = True
      Size = 1
    end
    object cdsGridListaPadraoTIPO_CADASTRO: TStringField
      FieldName = 'TIPO_CADASTRO'
      FixedChar = True
      Size = 1
    end
    object cdsGridListaPadraoID_CATEGORIA_DESPREC_FK: TIntegerField
      FieldName = 'ID_CATEGORIA_DESPREC_FK'
    end
    object cdsGridListaPadraoID_SUBCATEGORIA_DESPREC_FK: TIntegerField
      FieldName = 'ID_SUBCATEGORIA_DESPREC_FK'
    end
    object cdsGridListaPadraoID_NATUREZA_FK: TIntegerField
      FieldName = 'ID_NATUREZA_FK'
    end
    object cdsGridListaPadraoID_CLIENTE_FORNECEDOR_FK: TIntegerField
      FieldName = 'ID_CLIENTE_FORNECEDOR_FK'
    end
    object cdsGridListaPadraoFLG_IMPOSTORENDA: TStringField
      FieldName = 'FLG_IMPOSTORENDA'
      FixedChar = True
      Size = 1
    end
    object cdsGridListaPadraoFLG_PESSOAL: TStringField
      FieldName = 'FLG_PESSOAL'
      FixedChar = True
      Size = 1
    end
    object cdsGridListaPadraoFLG_AUXILIAR: TStringField
      FieldName = 'FLG_AUXILIAR'
      FixedChar = True
      Size = 1
    end
    object cdsGridListaPadraoFLG_REAL: TStringField
      FieldName = 'FLG_REAL'
      FixedChar = True
      Size = 1
    end
    object cdsGridListaPadraoQTD_PARCELAS: TIntegerField
      FieldName = 'QTD_PARCELAS'
    end
    object cdsGridListaPadraoVR_TOTAL_PREV: TBCDField
      FieldName = 'VR_TOTAL_PREV'
      OnGetText = cdsGridListaPadraoVR_TOTAL_PREVGetText
      Precision = 18
      Size = 2
    end
    object cdsGridListaPadraoVR_TOTAL_JUROS: TBCDField
      FieldName = 'VR_TOTAL_JUROS'
      OnGetText = cdsGridListaPadraoVR_TOTAL_JUROSGetText
      Precision = 18
      Size = 2
    end
    object cdsGridListaPadraoVR_TOTAL_DESCONTO: TBCDField
      FieldName = 'VR_TOTAL_DESCONTO'
      OnGetText = cdsGridListaPadraoVR_TOTAL_DESCONTOGetText
      Precision = 18
      Size = 2
    end
    object cdsGridListaPadraoVR_TOTAL_PAGO: TBCDField
      FieldName = 'VR_TOTAL_PAGO'
      OnGetText = cdsGridListaPadraoVR_TOTAL_PAGOGetText
      Precision = 18
      Size = 2
    end
    object cdsGridListaPadraoCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsGridListaPadraoDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsGridListaPadraoFLG_STATUS: TStringField
      FieldName = 'FLG_STATUS'
      FixedChar = True
      Size = 1
    end
    object cdsGridListaPadraoFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsGridListaPadraoDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsGridListaPadraoCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsGridListaPadraoFLG_RECORRENTE: TStringField
      FieldName = 'FLG_RECORRENTE'
      FixedChar = True
      Size = 1
    end
    object cdsGridListaPadraoFLG_REPLICADO: TStringField
      FieldName = 'FLG_REPLICADO'
      FixedChar = True
      Size = 1
    end
    object cdsGridListaPadraoNUM_RECIBO: TIntegerField
      FieldName = 'NUM_RECIBO'
    end
    object cdsGridListaPadraoID_ORIGEM: TIntegerField
      FieldName = 'ID_ORIGEM'
    end
    object cdsGridListaPadraoID_SISTEMA_FK: TIntegerField
      FieldName = 'ID_SISTEMA_FK'
    end
    object cdsGridListaPadraoOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 3000
    end
    object cdsGridListaPadraoDATA_LANCAMENTO: TDateField
      FieldName = 'DATA_LANCAMENTO'
    end
    object cdsGridListaPadraoFLG_FLUTUANTE: TStringField
      FieldName = 'FLG_FLUTUANTE'
      FixedChar = True
      Size = 1
    end
    object cdsGridListaPadraoID_CARNELEAO_EQUIVALENCIA_FK: TIntegerField
      FieldName = 'ID_CARNELEAO_EQUIVALENCIA_FK'
    end
    object cdsGridListaPadraoDESCR_CATEGORIA_DESPREC: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCR_CATEGORIA_DESPREC'
      Size = 250
    end
    object cdsGridListaPadraoDESCR_SUBCATEGORIA_DESPREC: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCR_SUBCATEGORIA_DESPREC'
      Size = 250
    end
    object cdsGridListaPadraoDESCR_NATUREZA: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCR_NATUREZA'
      Size = 250
    end
    object cdsGridListaPadraoDESCR_CARNELEAO_CLASSIFICACAO: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCR_CARNELEAO_CLASSIFICACAO'
      Size = 250
    end
  end
end
