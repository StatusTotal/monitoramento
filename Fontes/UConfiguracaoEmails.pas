{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UConfiguracaoEmails.pas
  Descricao:   Tela de configuracao dos Emails de Remetente e Destinatarios
               (para envio automatico pelo sistema)
  Author:      Cristina
  Date:        14-nov-2017
  Last Update: 23-out-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UConfiguracaoEmails;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Grids, Vcl.DBGrids, Vcl.DBCtrls, Vcl.Mask, JvExMask, JvToolEdit,
  JvMaskEdit, JvDBControls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Datasnap.DBClient,
  Datasnap.Provider, FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.StrUtils;

type
  TFConfiguracaoEmails = class(TFCadastroGeralPadrao)
    gbDestinatarios: TGroupBox;
    lblNomeDestinatario: TLabel;
    lblEmailDestinatario: TLabel;
    edtNomeDestinatario: TDBEdit;
    edtEmailDestinatario: TDBEdit;
    chbAtivoDestinatario: TDBCheckBox;
    dbgDestinatarios: TDBGrid;
    btnIncluirDest: TJvTransparentButton;
    btnEditarDest: TJvTransparentButton;
    btnExcluirDest: TJvTransparentButton;
    btnConfirmarDest: TJvTransparentButton;
    btnCancelarDest: TJvTransparentButton;
    gbRemetente: TGroupBox;
    lblNomeRemetente: TLabel;
    lblEmailRemetente: TLabel;
    lblSenha: TLabel;
    lblServidorSMTP: TLabel;
    lblPortaSMTP: TLabel;
    edtNomeRemetente: TDBEdit;
    edtEmailRemetente: TDBEdit;
    edtServidorSMTP: TDBEdit;
    edtPortaSMTP: TDBEdit;
    chbSSL: TDBCheckBox;
    dsDestinatario: TDataSource;
    chbFlgFechCaixa: TDBCheckBox;
    edtSenha: TEdit;
    lblUsername: TLabel;
    edtUsername: TDBEdit;
    btnEnviarTeste: TJvTransparentButton;
    chbTSL: TDBCheckBox;
    chbAutenticacao: TDBCheckBox;
    Label1: TLabel;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure dbgDestinatariosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure btnIncluirDestClick(Sender: TObject);
    procedure btnEditarDestClick(Sender: TObject);
    procedure btnExcluirDestClick(Sender: TObject);
    procedure btnConfirmarDestClick(Sender: TObject);
    procedure btnCancelarDestClick(Sender: TObject);
    procedure chbFlgFechCaixaKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnEnviarTesteClick(Sender: TObject);
    procedure edtEmailRemetenteExit(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }

    iIdIncDest: Integer;

    procedure HabDesabBotoesCRUD(Inc, Ed, Exc: Boolean);
    procedure RetornarPortaSMTP(Email: String; var Servidor, Autenticacao, SSL, TLS: String; var Porta: Integer);

    function VerificarDestinatario(var Msg: String; var QtdErros: Integer): Boolean;
  public
    { Public declarations }
  end;

var
  FConfiguracaoEmails: TFConfiguracaoEmails;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{ TFConfiguracaoEmails }

procedure TFConfiguracaoEmails.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFConfiguracaoEmails.btnCancelarDestClick(Sender: TObject);
begin
  inherited;

  dmPrincipal.cdsDestinatario.Cancel;

  if dmPrincipal.cdsDestinatario.RecordCount = 0 then
    HabDesabBotoesCRUD(True, False, False)
  else
    HabDesabBotoesCRUD(True, True, True);
end;

procedure TFConfiguracaoEmails.btnConfirmarDestClick(Sender: TObject);
var
  Msg: String;
  iCount, Qtd: Integer;
  lInserindo: Boolean;
begin
  inherited;

  lblMensagemErro.Caption := '  Mensagem:';
  lbMensagemErro.Color    := clWindow;
  lbMensagemErro.Clear;

  Msg    := '';
  iCount := 0;
  Qtd    := 0;
  lInserindo := False;

  if not VerificarDestinatario(Msg, Qtd) then
  begin
    case Qtd of
      0:
      begin
        lblMensagemErro.Caption := '  Mensagem:';
        lbMensagemErro.Color    := clWindow;
      end;
      1:
      begin
        lblMensagemErro.Caption := '  Durante a verifica��o dos dados, o seguinte erro foi encontrado:';
        lbMensagemErro.Color    := clInfoBk;
      end
      else
      begin
        lblMensagemErro.Caption := '  Durante a verifica��o dos dados, os seguintes erros foram encontrados:';
        lbMensagemErro.Color    := clInfoBk;
      end;
    end;

    for iCount := 0 to Pred(ListaMsgErro.Count) do
      lbMensagemErro.Items.Add(ListaMsgErro[iCount].Mensagem);
  end
  else
  begin
    if dmPrincipal.cdsDestinatario.State = dsInsert then
    begin
      Inc(iIdIncDest);
      dmPrincipal.cdsDestinatario.FieldByName('ID_EMAIL_DESTINATARIO').AsInteger := iIdIncDest;

      lInserindo := True;
    end;

    dmPrincipal.cdsDestinatario.Post;

    if lInserindo then
    begin
      if Application.MessageBox('Deseja incluir outro Destinat�rio?', 'Confirma��o', MB_YESNO + MB_ICONQUESTION) = ID_YES then
        btnIncluirDest.Click
      else
        HabDesabBotoesCRUD(True, True, True);
    end;
  end;
end;

procedure TFConfiguracaoEmails.btnEditarDestClick(Sender: TObject);
begin
  inherited;
if dmPrincipal.cdsDestinatario.RecordCount > 0 then
  begin
    dmPrincipal.cdsDestinatario.Edit;
    HabDesabBotoesCRUD(False, false, False);
  end;
end;

procedure TFConfiguracaoEmails.btnEnviarTesteClick(Sender: TObject);
var
  sDestinatario, sMsgEmail: String;
  lCancelado: Boolean;
begin
  inherited;

  sDestinatario := '';
  sMsgEmail     := '';
  lCancelado    := False;

  repeat
    if not InputQuery('DESTINAT�RIO', 'Por favor, informe o E-MAIL DO DESTINAT�RIO:', sDestinatario) then
      lCancelado := True;
  until (Trim(sDestinatario) <> '') or lCancelado;

  if Trim(sDestinatario) <> '' then
  begin
    Application.MessageBox(PChar('Verifique se o provedor de e-mail do Remetente precisa de permiss�o para que outros aplicativos usem os m�todos de acesso.' + #13#10 +
                                 'Essa permiss�o � dada nas configura��es do pr�prio e-mail, n�o no sistema Monitoramento.'),
                           'ATEN��O',
                           MB_OK);

    dmGerencial.Processando(True, 'Enviando e-mail de teste', True, 'Por favor, aguarde...');

    dmGerencial.EnviarEmail('Teste de Remetente',
                            sDestinatario,
                            '',
                            'Prezado(a)',
                            'Este e-mail est&aacute; sendo enviado para fins de teste das configura&ccedil;&otilde;es do Remetente cadastradas no sistema Monitoramento.',
                            '',
                            dmPrincipal.cdsRemetente,
                            sMsgEmail);

    dmGerencial.Processando(False);

    if Trim(sMsgEmail) = '' then
      Application.MessageBox('Envio de teste realizado com sucesso!', 'TESTE DE E-MAIL', MB_OK)
    else
      Application.MessageBox(PChar(sMsgEmail), 'TESTE DE E-MAIL', MB_OK);
  end;
end;

procedure TFConfiguracaoEmails.btnExcluirDestClick(Sender: TObject);
begin
  inherited;

  if dmPrincipal.cdsDestinatario.RecordCount > 0 then
    dmPrincipal.cdsDestinatario.Delete;
end;

procedure TFConfiguracaoEmails.btnIncluirDestClick(Sender: TObject);
begin
  inherited;

  dmPrincipal.cdsDestinatario.Append;

  dmPrincipal.cdsDestinatario.FieldByName('NOVO').AsBoolean              := True;
  dmPrincipal.cdsDestinatario.FieldByName('FLG_REL_FECHAMENTO').AsString := 'S';
  dmPrincipal.cdsDestinatario.FieldByName('FLG_ATIVO').AsString          := 'S';

  HabDesabBotoesCRUD(False, False, False);

  if edtNomeDestinatario.CanFocus then
    edtNomeDestinatario.SetFocus;
end;

procedure TFConfiguracaoEmails.btnOkClick(Sender: TObject);
var
  Mensagem: String;
  Qtd: Integer;
  iCount: Integer;
begin
  inherited;

  if vgOperacao <> C then
  begin
    lbMensagemErro.Clear;

    Mensagem := '';
    Qtd      := 0;

    if not VerificarDadosGenerico(Qtd) then
    begin
      case Qtd of
        0:
        begin
          lblMensagemErro.Caption := '  Mensagem:';
          lbMensagemErro.Color    := clInfoBk;
        end;
        1:
        begin
          lblMensagemErro.Caption := '  Durante a verifica��o dos dados, o seguinte erro foi encontrado:';
          lbMensagemErro.Color    := clWhite;
        end
        else
        begin
          lblMensagemErro.Caption := '  Durante a verifica��o dos dados, os seguintes erros foram encontrados:';
          lbMensagemErro.Color    := clWhite;
        end;
      end;

      for iCount := 0 to Pred(ListaMsgErro.Count) do
        lbMensagemErro.Items.Add(ListaMsgErro[iCount].Mensagem);
    end
    else
    begin
      if not GravarDadosGenerico(Mensagem) then
        Application.MessageBox(PChar(Mensagem), 'Erro', MB_OK + MB_ICONERROR)
      else
      begin
        GravarLog;
        Application.MessageBox(PChar(Mensagem), 'Aviso', MB_OK);

        dmPrincipal.cdsRemetente.Close;
        dmPrincipal.cdsRemetente.Open;
      end;
    end;
  end
  else
    Self.Close;
end;

procedure TFConfiguracaoEmails.chbFlgFechCaixaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dmPrincipal.cdsDestinatario.State in [dsInsert, dsEdit] then
      btnConfirmarDest.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFConfiguracaoEmails.dbgDestinatariosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;

  AlternarCorGrid(Sender, Rect, DataCol, Column, State);
end;

procedure TFConfiguracaoEmails.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtNomeRemetente.MaxLength     := 250;
  edtUsername.MaxLength          := 250;
  edtEmailRemetente.MaxLength    := 250;
  edtSenha.MaxLength             := 30;
  edtServidorSMTP.MaxLength      := 150;
  edtPortaSMTP.MaxLength         := 5;
  edtNomeDestinatario.MaxLength  := 250;
  edtEmailDestinatario.MaxLength := 250;
end;

procedure TFConfiguracaoEmails.DesabilitarComponentes;
begin
  inherited;

  btnDadosPrincipais.Visible := False;
end;

procedure TFConfiguracaoEmails.edtEmailRemetenteExit(Sender: TObject);
var
  sServidor,  sAutenticacao, sSSL, sTLS: String;
  iPorta: Integer;
begin
  inherited;

  iPorta := 0;

  sServidor     := '';
  sSSL          := '';
  sTLS          := '';
  sAutenticacao := '';

  if Trim(edtEmailRemetente.Text) <> '' then
    RetornarPortaSMTP(edtEmailRemetente.Text, sServidor, sAutenticacao, sSSL, sTLS, iPorta);

  if dmPrincipal.cdsRemetente.State in [dsInsert, dsEdit] then
  begin
    if iPorta = 0 then
    begin
      dmPrincipal.cdsRemetente.FieldByName('SERVIDOR_SMTP').Value       := Null;
      dmPrincipal.cdsRemetente.FieldByName('PORTA_SMTP').Value          := Null;
      dmPrincipal.cdsRemetente.FieldByName('FLG_AUTENTICACAO').AsString := 'S';
      dmPrincipal.cdsRemetente.FieldByName('FLG_SSL_SMTP').AsString     := 'S';
      dmPrincipal.cdsRemetente.FieldByName('FLG_TLS_SMTP').AsString     := 'N';
    end
    else
    begin
      dmPrincipal.cdsRemetente.FieldByName('SERVIDOR_SMTP').AsString    := sServidor;
      dmPrincipal.cdsRemetente.FieldByName('PORTA_SMTP').AsInteger      := iPorta;
      dmPrincipal.cdsRemetente.FieldByName('FLG_AUTENTICACAO').AsString := sAutenticacao;
      dmPrincipal.cdsRemetente.FieldByName('FLG_SSL_SMTP').AsString     := sSSL;
      dmPrincipal.cdsRemetente.FieldByName('FLG_TLS_SMTP').AsString     := sTLS;
    end;
  end;
end;

procedure TFConfiguracaoEmails.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  if lPodeCancelar then
  begin
    dmPrincipal.cdsRemetente.Cancel;
    dmPrincipal.cdsDestinatario.Cancel;

    vgOperacao   := VAZIO;
    vgIdConsulta := 0;
  end
  else
    Action := caNone;
end;

procedure TFConfiguracaoEmails.FormCreate(Sender: TObject);
begin
  inherited;

  ShowScrollBar(dbgDestinatarios.Handle, SB_HORZ, False);

  dmPrincipal.cdsRemetente.Close;
  dmPrincipal.cdsRemetente.Open;

  if vgOperacao = E then
  begin
    if dmPrincipal.cdsRemetente.RecordCount = 0 then
      dmPrincipal.cdsRemetente.Append
    else
    begin
      dmPrincipal.cdsRemetente.Edit;

      edtSenha.Text := dmGerencial.DecryptStr(dmPrincipal.cdsRemetente.FieldByName('SENHA_REMETENTE').AsString);
    end;

    if dmPrincipal.cdsRemetente.FieldByName('FLG_AUTENTICACAO').IsNull then
      dmPrincipal.cdsRemetente.FieldByName('FLG_AUTENTICACAO').AsString := 'S';

    if dmPrincipal.cdsRemetente.FieldByName('FLG_SSL_SMTP').IsNull then
      dmPrincipal.cdsRemetente.FieldByName('FLG_SSL_SMTP').AsString := 'S';

    if dmPrincipal.cdsRemetente.FieldByName('FLG_TLS_SMTP').IsNull then
      dmPrincipal.cdsRemetente.FieldByName('FLG_TLS_SMTP').AsString := 'N';

    if dmPrincipal.cdsRemetente.FieldByName('FLG_ATIVO').IsNull then
      dmPrincipal.cdsRemetente.FieldByName('FLG_ATIVO').AsString := 'S';
  end;

  dmPrincipal.cdsDestinatario.Close;
  dmPrincipal.cdsDestinatario.Open;

  iIdIncDest := dmPrincipal.cdsDestinatario.RecordCount;
end;

procedure TFConfiguracaoEmails.FormShow(Sender: TObject);
begin
  inherited;

  if dmPrincipal.cdsDestinatario.RecordCount > 0 then
    HabDesabBotoesCRUD(True, True, True)
  else
    HabDesabBotoesCRUD(True, False, False);
end;

function TFConfiguracaoEmails.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFConfiguracaoEmails.GravarDadosGenerico(var Msg: String): Boolean;
begin
  Result := True;
  Msg := '';

  if vgOperacao = C then
    Exit;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    { REMETENTE }
    if dmPrincipal.cdsRemetente.State = dsInsert then
      dmPrincipal.cdsRemetente.FieldByName('ID_EMAIL_REMETENTE').AsInteger := BS.ProximoId('ID_EMAIL_REMETENTE', 'EMAIL_REMETENTE')
    else if not (dmPrincipal.cdsRemetente.State in [dsInsert, dsEdit]) then
      dmPrincipal.cdsRemetente.Edit;

    dmPrincipal.cdsRemetente.FieldByName('SENHA_REMETENTE').AsString := '';
    dmPrincipal.cdsRemetente.FieldByName('SENHA_REMETENTE').AsString := dmGerencial.EncryptStr(edtSenha.Text);

    dmPrincipal.cdsRemetente.Post;
    dmPrincipal.cdsRemetente.ApplyUpdates(0);

    { DESTINATARIO }
    dmPrincipal.cdsDestinatario.First;

    while not dmPrincipal.cdsDestinatario.Eof do
    begin
      if dmPrincipal.cdsDestinatario.FieldByName('NOVO').AsBoolean then
      begin
        dmPrincipal.cdsDestinatario.Edit;
        dmPrincipal.cdsDestinatario.FieldByName('ID_EMAIL_DESTINATARIO').AsInteger := BS.ProximoId('ID_EMAIL_DESTINATARIO', 'EMAIL_DESTINATARIO');
        dmPrincipal.cdsDestinatario.Post;
      end;

      dmPrincipal.cdsDestinatario.Next;
    end;

    dmPrincipal.cdsDestinatario.ApplyUpdates(0);

    Msg := 'Configura��es de E-mails gravadas com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na grava��o das Configura��es de E-mails.';
  end;
end;

procedure TFConfiguracaoEmails.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    I, C:  //INCLUSAO /CONSULTA
    begin
      BS.GravarUsuarioLog(80, '', 'Somente consulta', vgIdConsulta, 'EMAIL_REMETENTE');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da edi��o dos E-mails:', '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de E-mails';

      BS.GravarUsuarioLog(80, '', sObservacao, vgIdConsulta, 'EMAIL_REMETENTE');
    end;
  end;
end;

procedure TFConfiguracaoEmails.HabDesabBotoesCRUD(Inc, Ed,
  Exc: Boolean);
begin
  btnIncluirDest.Enabled       := Inc and vgPrm_IncRemDest;
  btnEditarDest.Enabled        := Ed and vgPrm_EdRemDest;
  btnExcluirDest.Enabled       := Exc and vgPrm_ExcRemDest;

  btnConfirmarDest.Enabled     := not (btnIncluirDest.Enabled or btnEditarDest.Enabled or btnExcluirDest.Enabled);
  btnCancelarDest.Enabled      := not (btnIncluirDest.Enabled or btnEditarDest.Enabled or btnExcluirDest.Enabled);
  edtNomeDestinatario.Enabled  := not (btnIncluirDest.Enabled or btnEditarDest.Enabled or btnExcluirDest.Enabled);
  edtEmailDestinatario.Enabled := not (btnIncluirDest.Enabled or btnEditarDest.Enabled or btnExcluirDest.Enabled);
  chbFlgFechCaixa.Enabled      := not (btnIncluirDest.Enabled or btnEditarDest.Enabled or btnExcluirDest.Enabled);
  chbAtivoDestinatario.Enabled := not (btnIncluirDest.Enabled or btnEditarDest.Enabled or btnExcluirDest.Enabled);
end;

procedure TFConfiguracaoEmails.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFConfiguracaoEmails.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

procedure TFConfiguracaoEmails.RetornarPortaSMTP(Email: String; var Servidor,
  Autenticacao, SSL, TLS: String; var Porta: Integer);
var
  sEmail: AnsiString;
begin
  sEmail := Email;

  if AnsiContainsStr(sEmail, '@hotmail.com') then
  begin
    Servidor     := 'smtp.live.com';
    Porta        := 587;
    Autenticacao := 'S';
    SSL          := 'S';
    TLS          := 'N';
  end
  else if AnsiContainsStr(sEmail, '@hotmail.com.br') then
  begin
    Servidor     := 'smtp.live.com';
    Porta        := 587;
    Autenticacao := 'S';
    SSL          := 'S';
    TLS          := 'N';
  end
  else if AnsiContainsStr(sEmail, '@msn.com') then
  begin
    Servidor     := 'smtp.live.com';
    Porta        := 587;
    Autenticacao := 'S';
    SSL          := 'S';
    TLS          := 'N';
  end
  else if AnsiContainsStr(sEmail, '@live.com') then
  begin
    Servidor     := 'smtp.live.com';
    Porta        := 587;
    Autenticacao := 'S';
    SSL          := 'S';
    TLS          := 'N';
  end
  else if AnsiContainsStr(sEmail, '@yahoo.com') then
  begin
    Servidor     := 'smtp.mail.yahoo.com.br';
    Porta        := 465;
    Autenticacao := 'S';
    SSL          := 'S';
    TLS          := 'N';
  end
  else if AnsiContainsStr(sEmail, '@yahoo.com.br') then
  begin
    Servidor     := 'smtp.mail.yahoo.com.br';
    Porta        := 465;
    Autenticacao := 'S';
    SSL          := 'S';
    TLS          := 'N';
  end
  else if AnsiContainsStr(sEmail, '@ymail.com') then
  begin
    Servidor     := 'smtp.mail.yahoo.com.br';
    Porta        := 465;
    Autenticacao := 'S';
    SSL          := 'S';
    TLS          := 'N';
  end
  else if AnsiContainsStr(sEmail, '@rocketmail.com') then
  begin
    Servidor     := 'smtp.mail.yahoo.com.br';
    Porta        := 465;
    Autenticacao := 'S';
    SSL          := 'S';
    TLS          := 'N';
  end
  else if AnsiContainsStr(sEmail, '@bol.com.br') then
  begin
    Servidor     := 'smtps.bol.com.br';
    Porta        := 465;
    Autenticacao := 'S';
    SSL          := 'S';
    TLS          := 'N';
  end
  else if AnsiContainsStr(sEmail, '@gmail.com') then
  begin
    Servidor     := 'smtp.gmail.com';
    Porta        := 465;
    Autenticacao := 'S';
    SSL          := 'S';
    TLS          := 'N';
  end
  else if AnsiContainsStr(sEmail, '@bol.com.br') then
  begin
    Servidor     := 'smtps.bol.com.br';
    Porta        := 465;
    Autenticacao := 'S';
    SSL          := 'S';
    TLS          := 'N';
  end
  else if AnsiContainsStr(sEmail, '@ig.com.br') then
  begin
    Servidor     := 'smtp.ig.com.br';
    Porta        := 465;
    Autenticacao := 'S';
    SSL          := 'S';
    TLS          := 'N';
  end
  else if AnsiContainsStr(sEmail, '@uol.com.br') then
  begin
    Servidor     := 'smtps.uol.com.br';
    Porta        := 465;
    Autenticacao := 'S';
    SSL          := 'S';
    TLS          := 'N';
  end
  else if AnsiContainsStr(sEmail, '@terra.com.br') then
  begin
    Servidor     := 'smtp.terra.com.br';
    Porta        := 587;
    Autenticacao := 'S';
    SSL          := 'N';
    TLS          := 'S';
  end
  else if AnsiContainsStr(sEmail, '@ibest.com.br') then
  begin
    Servidor     := 'smtp.ibest.com.br';
    Porta        := 465;
    Autenticacao := 'S';
    SSL          := 'S';
    TLS          := 'N';
  end
  else if AnsiContainsStr(sEmail, '@itelefonica.com.br') then
  begin
    Servidor     := 'smtp.itelefonica.com.br';
    Porta        := 25;
    Autenticacao := 'S';
    SSL          := 'N';
    TLS          := 'S';
  end
end;

function TFConfiguracaoEmails.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  { REMETENTE }
  if Trim(edtNomeRemetente.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher o NOME DO REMETENTE.';
    DadosMsgErro.Componente := edtNomeRemetente;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtUsername.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher o NOME DE USU�RIO DO REMETENTE.';
    DadosMsgErro.Componente := edtUsername;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtEmailRemetente.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher o E-MAIL DO REMETENTE.';
    DadosMsgErro.Componente := edtEmailRemetente;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtSenha.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher a SENHA DO E-MAIL DO REMETENTE.';
    DadosMsgErro.Componente := edtSenha;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtServidorSMTP.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher o SERVIDOR SMTP DO E-MAIL DO REMETENTE.';
    DadosMsgErro.Componente := edtServidorSMTP;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtPortaSMTP.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher a PORTA SMTP DO E-MAIL DO REMETENTE.';
    DadosMsgErro.Componente := edtPortaSMTP;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFConfiguracaoEmails.VerificarDestinatario(var Msg: String;
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  Msg      := '';
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  { DESTINATARIOS }

  if Trim(edtNomeDestinatario.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher o NOME DO DESTINAT�RIO';
    DadosMsgErro.Componente := edtNomeDestinatario;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtEmailDestinatario.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta informar se o E-MAIL DO DESTINAT�RIO';
    DadosMsgErro.Componente := edtEmailDestinatario;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if QtdErros > 0 then
    Result := False;
end;

function TFConfiguracaoEmails.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

end.
