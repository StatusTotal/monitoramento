{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroFolhaSeguranca.pas
  Descricao:   Formulario de cadastro de Folha de Seguranca
  Author   :   Pedro
  Date:        19-jun-2017
  Last Update: 23-out-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroFolhaSeguranca;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Mask, sMaskEdit, sCustomComboEdit, sToolEdit, sDBDateEdit, Vcl.DBCtrls,
  sDBEdit, sDBMemo, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  sDBComboBox;

type
  TFCadastroFolhaSeguranca = class(TFCadastroGeralPadrao)
    edDataPratica: TsDBDateEdit;
    edFolha: TsDBEdit;
    edLetra: TsDBEdit;
    edNumero: TsDBEdit;
    edAleatorio: TsDBEdit;
    MMObs: TsDBMemo;
    lcbNomeFuncionario: TDBLookupComboBox;
    lblNomeFuncionario: TLabel;
    lcbNatureza: TDBLookupComboBox;
    btnIncluirNatureza: TJvTransparentButton;
    lblNatureza: TLabel;
    lblSistema: TLabel;
    lcbSistema: TDBLookupComboBox;
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnIncluirNaturezaClick(Sender: TObject);
    procedure MMObsKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure lcbSistemaClick(Sender: TObject);
    procedure lcbSistemaExit(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }

    procedure AbrirListaNatureza;
  public
    { Public declarations }
  end;

var
  FCadastroFolhaSeguranca: TFCadastroFolhaSeguranca;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais,
  UCadastroNaturezaAto, UDMFolhaSeguranca;

procedure TFCadastroFolhaSeguranca.AbrirListaNatureza;
begin
  dmFolhaSeguranca.qryNatAto.Close;

  if Trim(lcbSistema.Text) = '' then
  begin
    dmFolhaSeguranca.qryNatAto.Params.ParamByName('ID_SIST01').Value := 0;
    dmFolhaSeguranca.qryNatAto.Params.ParamByName('ID_SIST02').Value := 0;
  end
  else
  begin
    dmFolhaSeguranca.qryNatAto.Params.ParamByName('ID_SIST01').Value := dmFolhaSeguranca.qrySistema.FieldByName('ID_SISTEMA').AsInteger;
    dmFolhaSeguranca.qryNatAto.Params.ParamByName('ID_SIST02').Value := dmFolhaSeguranca.qrySistema.FieldByName('ID_SISTEMA').AsInteger;
  end;

  dmFolhaSeguranca.qryNatAto.Open;
end;

procedure TFCadastroFolhaSeguranca.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroFolhaSeguranca.btnIncluirNaturezaClick(
  Sender: TObject);
var
  Op: TOperacao;
  IdCons: Integer;
  OrigF, OrigC: Boolean;
begin
  inherited;

  Op     := vgOperacao;
  IdCons := vgIdConsulta;
  OrigF  := vgOrigemFiltro;
  OrigC  := vgOrigemCadastro;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := False;
  vgOrigemCadastro := False;

  try
    Application.CreateForm(TFCadastroNaturezaAto, FCadastroNaturezaAto);

    FCadastroNaturezaAto.iOrigem := 4;

    if lcbSistema.KeyValue > -1 then
      FCadastroNaturezaAto.iIdSistema := lcbSistema.KeyValue;

    FCadastroNaturezaAto.ShowModal;
  finally
    AbrirListaNatureza;

    if FCadastroNaturezaAto.IdNovaNatureza <> 0 then
      lcbNatureza.KeyValue := IntToStr(FCadastroNaturezaAto.IdNovaNatureza);

    FCadastroNaturezaAto.Free;
  end;

  vgOperacao       := Op;
  vgIdConsulta     := IdCons;
  vgOrigemFiltro   := OrigF;
  vgOrigemCadastro := OrigC;
end;

procedure TFCadastroFolhaSeguranca.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroFolhaSeguranca.DefinirTamanhoMaxCampos;
begin
  inherited;
//
end;

procedure TFCadastroFolhaSeguranca.DesabilitarComponentes;
begin
  inherited;

  if vgOperacao = C then
    pnlDados.Enabled := False;
end;

procedure TFCadastroFolhaSeguranca.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  if lPodeCancelar then
  begin
    dmFolhaSeguranca.cdsFolha.Cancel;

    vgOperacao     := VAZIO;
    vgIdConsulta   := 0;
    vgOrigemFiltro := False;
  end
  else
    Action := caNone;
end;

procedure TFCadastroFolhaSeguranca.FormCreate(Sender: TObject);
begin
  inherited;

  dmFolhaSeguranca.qrySistema.Close;
  dmFolhaSeguranca.qrySistema.Open;

  dmFolhaSeguranca.qryFuncionarios.Close;
  dmFolhaSeguranca.qryFuncionarios.Open;

  AbrirListaNatureza;

  dmFolhaSeguranca.cdsFolha.Close;
  dmFolhaSeguranca.cdsFolha.Params.ParamByName('ID_FOLHASEGURANCA').AsInteger := vgIdConsulta;
  dmFolhaSeguranca.cdsFolha.Open;

  if vgOperacao=I then
  begin
    dmFolhaSeguranca.cdsFolha.Append;
    dmFolhaSeguranca.cdsFolha.FieldByName('FLG_RCPN').AsString        := 'N';
    dmFolhaSeguranca.cdsFolha.FieldByName('FLG_SITUACAO').AsString    := 'N';
    dmFolhaSeguranca.cdsFolha.FieldByName('CAD_ID_USUARIO').AsInteger := vgUsu_Id;
  end;

  if vgOperacao = E then
    dmFolhaSeguranca.cdsFolha.Edit;
end;

procedure TFCadastroFolhaSeguranca.FormShow(Sender: TObject);
begin
  inherited;

  if edFolha.CanFocus then
    edFolha.SetFocus;
end;

function TFCadastroFolhaSeguranca.GravarDadosAto(var Msg: String): Boolean;
begin
//
end;

function TFCadastroFolhaSeguranca.GravarDadosGenerico(var Msg: String): Boolean;
begin
  Result := True;
  Msg := '';

  if vgOperacao = C then
    Exit;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    if dmGerencial.Pergunta('CONFIRMA A UTILIZA��O: ' + #13#13 +
                            'DATA PR�TICA: ' + FormatDateTime('dd/mm/yyyy', edDataPratica.Date) + #13 +
                            'SELO: ' + edLetra.Text + edNumero.Text + ' ' + edAleatorio.Text) then
    begin
      dmFolhaSeguranca.cdsFolhaID_FUNCIONARIO_FK.AsInteger := lcbNomeFuncionario.KeyValue;
      dmFolhaSeguranca.cdsFolhaID_NATUREZAATO_FK.AsInteger := lcbNatureza.KeyValue;
      dmFolhaSeguranca.cdsFolhaFLG_SITUACAO.AsString       := 'U';
      dmFolhaSeguranca.cdsFolha.Post;
      dmFolhaSeguranca.cdsFolha.ApplyUpdates(0);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    Msg := 'Folha de Seguran�a ' + edFolha.Text + ' gravada com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na grava��o da Folha de Seguran�a ' + edFolha.Text + '.';
  end;
end;

procedure TFCadastroFolhaSeguranca.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(4, '', 'Somente consulta de Dados de Folha de Seguran�a',
                          vgIdConsulta, 'FOLHASEGURANCA');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(4, '', 'Somente inclus�o de Dados Folha de Seguran�a',
                          vgIdConsulta, 'FOLHASEGURANCA');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da edi��o desse Folha de Seguran�a:', '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de Folha de Seguran�a';

      BS.GravarUsuarioLog(4, '', sObservacao,
                          vgIdConsulta, 'FOLHASEGURANCA');
    end;
  end;
end;

procedure TFCadastroFolhaSeguranca.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroFolhaSeguranca.lcbSistemaClick(Sender: TObject);
begin
  inherited;

  AbrirListaNatureza;
end;

procedure TFCadastroFolhaSeguranca.lcbSistemaExit(Sender: TObject);
begin
  inherited;

  AbrirListaNatureza;
end;

procedure TFCadastroFolhaSeguranca.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
//
end;

procedure TFCadastroFolhaSeguranca.MMObsKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

function TFCadastroFolhaSeguranca.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if edDataPratica.Date = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha o campo Data.';
    DadosMsgErro.Componente := edDataPratica;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Length(edLetra.Text) < 4 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo Letra.';
    DadosMsgErro.Componente := edletra;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Length(edNumero.Text) < 5 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo N�mero.';
    DadosMsgErro.Componente := edNumero;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Length(edAleatorio.Text) < 3 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo Aleat�rio.';
    DadosMsgErro.Componente := edAleatorio;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if (lcbSistema.KeyValue = -1) then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo Sistema.';
    DadosMsgErro.Componente := lcbSistema;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if (lcbNatureza.KeyValue = -1) then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo Natureza.';
    DadosMsgErro.Componente := lcbNatureza;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if (lcbNomeFuncionario.KeyValue = -1) then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo Nome do Colaborador.';
    DadosMsgErro.Componente := lcbNomeFuncionario;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroFolhaSeguranca.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
//
end;

end.
