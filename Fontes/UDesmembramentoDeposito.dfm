inherited FDesmembramentoDeposito: TFDesmembramentoDeposito
  Caption = 'FDesmembramentoDeposito'
  ClientHeight = 477
  ClientWidth = 940
  ExplicitWidth = 946
  ExplicitHeight = 506
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 934
    Height = 471
    ExplicitWidth = 934
    ExplicitHeight = 471
    inherited pnlDados: TPanel
      Width = 820
      Height = 465
      ExplicitWidth = 820
      ExplicitHeight = 465
      object lblTotalDesmembrado: TLabel [0]
        Left = 709
        Top = 362
        Width = 110
        Height = 14
        Alignment = taRightJustify
        Caption = 'Total Desmembrado'
      end
      inherited pnlMsgErro: TPanel
        Top = 404
        Width = 820
        TabOrder = 2
        ExplicitTop = 404
        ExplicitWidth = 820
        inherited lblMensagemErro: TLabel
          Width = 820
        end
        inherited lbMensagemErro: TListBox
          Width = 820
          OnDblClick = lbMensagemErroDblClick
          ExplicitWidth = 820
        end
      end
      object gbDepositoPrincipal: TGroupBox
        Left = 5
        Top = 5
        Width = 814
        Height = 105
        Caption = 'Dep'#243'sito Principal'
        TabOrder = 0
        object lblDataDepositoP: TLabel
          Left = 5
          Top = 16
          Width = 69
          Height = 14
          Caption = 'Dt. Dep'#243'sito'
        end
        object lblNumCodDepositoP: TLabel
          Left = 111
          Top = 16
          Width = 121
          Height = 14
          Caption = 'Identifica'#231#227'o Dep'#243'sito'
        end
        object lblDescricaoDepositoP: TLabel
          Left = 267
          Top = 16
          Width = 51
          Height = 14
          Caption = 'Descri'#231#227'o'
        end
        object lblValorDepositoPUtilizado: TLabel
          Left = 473
          Top = 16
          Width = 75
          Height = 14
          Caption = 'Valor Utilizado'
        end
        object lblSituacaoP: TLabel
          Left = 579
          Top = 16
          Width = 46
          Height = 14
          Caption = 'Situa'#231#227'o'
        end
        object lblDataSituacaoP: TLabel
          Left = 709
          Top = 16
          Width = 67
          Height = 14
          Caption = 'Dt. Situa'#231#227'o'
        end
        object lblValorDiferenca: TLabel
          Left = 111
          Top = 60
          Width = 92
          Height = 14
          Hint = 'Valor restante a ser desmembrado'
          Caption = 'Valor Diferen'#231'a'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object lblValorDepositoPAntigo: TLabel
          Left = 5
          Top = 60
          Width = 67
          Height = 14
          Caption = 'Valor Antigo'
        end
        object dteDataDepositoP: TJvDBDateEdit
          Left = 5
          Top = 32
          Width = 100
          Height = 22
          DataField = 'DATA_DEPOSITO'
          DataSource = dsDepoP
          ReadOnly = True
          Color = clMenu
          ShowNullDate = False
          TabOrder = 0
          OnKeyPress = FormKeyPress
        end
        object edtNumCodDepositoP: TDBEdit
          Left = 111
          Top = 32
          Width = 150
          Height = 22
          Color = clMenu
          DataField = 'NUM_COD_DEPOSITO'
          DataSource = dsDepoP
          ReadOnly = True
          TabOrder = 1
          OnKeyPress = FormKeyPress
        end
        object edtDescricaoDepositoP: TDBEdit
          Left = 267
          Top = 32
          Width = 200
          Height = 22
          CharCase = ecUpperCase
          Color = clMenu
          DataField = 'DESCR_DEPOSITO_FLUTUANTE'
          DataSource = dsDepoP
          ReadOnly = True
          TabOrder = 2
          OnKeyPress = FormKeyPress
        end
        object cedValorDepositoPUtilizado: TJvDBCalcEdit
          Left = 473
          Top = 32
          Width = 100
          Height = 22
          Color = clInfoBk
          DisplayFormat = ',0.00'
          ReadOnly = True
          TabOrder = 3
          DecimalPlacesAlwaysShown = False
          OnKeyPress = FormKeyPress
          DataField = 'VR_DEPOSITO_FLUTUANTE'
          DataSource = dsDepoP
        end
        object cbSituacaoP: TJvDBComboBox
          Left = 579
          Top = 32
          Width = 124
          Height = 22
          Style = csOwnerDrawFixed
          Color = clMenu
          DataField = 'FLG_STATUS'
          DataSource = dsDepoP
          Enabled = False
          EnableValues = False
          Items.Strings = (
            'FLUTUANTE'
            'BAIXADO'
            'CANCELADO')
          ReadOnly = True
          TabOrder = 4
          Values.Strings = (
            'F'
            'B'
            'C')
          ListSettings.OutfilteredValueFont.Charset = DEFAULT_CHARSET
          ListSettings.OutfilteredValueFont.Color = clRed
          ListSettings.OutfilteredValueFont.Height = -11
          ListSettings.OutfilteredValueFont.Name = 'Tahoma'
          ListSettings.OutfilteredValueFont.Style = []
          OnKeyPress = FormKeyPress
        end
        object dteDataSituacaoP: TJvDBDateEdit
          Left = 706
          Top = 32
          Width = 100
          Height = 22
          DataField = 'DATA_STATUS'
          DataSource = dsDepoP
          ReadOnly = True
          Color = clMenu
          ShowNullDate = False
          TabOrder = 5
          OnKeyPress = FormKeyPress
        end
        object cedValorDiferenca: TJvDBCalcEdit
          Left = 111
          Top = 76
          Width = 100
          Height = 22
          Color = 14680031
          DisplayFormat = ',0.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 6
          DecimalPlacesAlwaysShown = False
          OnKeyPress = FormKeyPress
          DataField = 'VR_DIFERENCA'
          DataSource = dsDepoP
        end
        object cedValorDepositoPAntigo: TJvDBCalcEdit
          Left = 5
          Top = 76
          Width = 100
          Height = 22
          Color = clMenu
          DisplayFormat = ',0.00'
          ReadOnly = True
          TabOrder = 7
          DecimalPlacesAlwaysShown = False
          OnKeyPress = FormKeyPress
          DataField = 'VR_ANTIGO'
          DataSource = dsDepoP
        end
      end
      object gbDepositosDerivados: TGroupBox
        Left = 5
        Top = 116
        Width = 814
        Height = 240
        Hint = 'Dep'#243'sitos derivados da SOBRA do Dep'#243'sito Principal'
        Caption = 'Dep'#243'sitos Derivados'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        object lblDescricaoDepositoD: TLabel
          Left = 267
          Top = 16
          Width = 51
          Height = 14
          Caption = 'Descri'#231#227'o'
        end
        object lblNumCodDepositoD: TLabel
          Left = 111
          Top = 16
          Width = 121
          Height = 14
          Caption = 'Identifica'#231#227'o Dep'#243'sito'
        end
        object lblDataDepositoD: TLabel
          Left = 5
          Top = 16
          Width = 69
          Height = 14
          Caption = 'Dt. Dep'#243'sito'
        end
        object lblValorDepositoD: TLabel
          Left = 473
          Top = 16
          Width = 27
          Height = 14
          Caption = 'Valor'
        end
        object lblSituacaoD: TLabel
          Left = 579
          Top = 16
          Width = 46
          Height = 14
          Caption = 'Situa'#231#227'o'
        end
        object lblDataSituacaoD: TLabel
          Left = 709
          Top = 16
          Width = 67
          Height = 14
          Caption = 'Dt. Situa'#231#227'o'
        end
        object btnIncluirParcelaDepo: TJvTransparentButton
          Left = 7
          Top = 212
          Width = 30
          Height = 22
          Hint = 'Incluir uma parcial do Dep'#243'sito'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          FrameStyle = fsIndent
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          Transparent = False
          OnClick = btnIncluirParcelaDepoClick
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            1800000000000006000000000000000000000000000000000000000000000000
            81A84B81A84B81A84B81A84B81A84B6363636363636363636363636363636363
            636363636363636363636B6B6B6B6B6BB9B9B9B9B9B9B9B9B9B9B9B9B9B9B9A4
            A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A400000081A84B
            81A84B81A84B81A84B81A84B81A84B81A84BFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF6666666B6B6BB9B9B9B9B9B9B9B9B9B9B9B9B9B9B9B9B9B9B9
            B9B9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6A6A681A84B81A84B
            81A84B81A84BFFFFFF81A84B81A84B81A84B81A84BFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF666666B9B9B9B9B9B9B9B9B9B9B9B9FFFFFFB9B9B9B9B9B9B9
            B9B9B9B9B9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6A6A681A84B81A84B
            81A84B81A84BFFFFFF81A84B81A84B81A84B81A84BFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF666666B9B9B9B9B9B9B9B9B9B9B9B9FFFFFFB9B9B9B9B9B9B9
            B9B9B9B9B9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6A6A681A84B81A84B
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF81A84B81A84BFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF666666B9B9B9B9B9B9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB9
            B9B9B9B9B9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6A6A681A84B81A84B
            81A84B81A84BFFFFFF81A84B81A84B81A84B81A84BFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF666666B9B9B9B9B9B9B9B9B9B9B9B9FFFFFFB9B9B9B9B9B9B9
            B9B9B9B9B9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6A6A681A84B81A84B
            81A84B81A84BFFFFFF81A84B81A84B81A84B81A84BFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF666666B9B9B9B9B9B9B9B9B9B9B9B9FFFFFFB9B9B9B9B9B9B9
            B9B9B9B9B9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6A6A600000081A84B
            81A84B81A84B81A84B81A84B81A84B81A84BFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF6666666B6B6BB9B9B9B9B9B9B9B9B9B9B9B9B9B9B9B9B9B9B9
            B9B9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6A6A6000000000000
            81A84B81A84B81A84B81A84B81A84BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE
            FEFEFEFEFEFEFE6666666B6B6B6B6B6BB9B9B9B9B9B9B9B9B9B9B9B9B9B9B9FF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFEFEFEA6A6A6000000000000
            666666000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F6363
            636363636363636363636B6B6B6B6B6BA6A6A66B6B6B6B6B6BFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFB4B4B4A4A4A4A4A4A4A4A4A4A4A4A4000000000000
            666666000000666666FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF646464FDFD
            FDFFFFFFB1B1B16363636B6B6B6B6B6BA6A6A66B6B6BA6A6A6FFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFA5A5A5FDFDFDFFFFFFD1D1D1A4A4A4000000000000
            666666000000666666FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF646464FDFD
            FDB1B1B16363636464646B6B6B6B6B6BA6A6A66B6B6BA6A6A6FFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFA5A5A5FDFDFDD1D1D1A4A4A4A5A5A5000000000000
            666666000000666666FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF666666AFAF
            AF6363636464640000006B6B6B6B6B6BA6A6A66B6B6BA6A6A6FFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFA6A6A6D0D0D0A4A4A4A5A5A56B6B6B000000000000
            6666660000006363636363636363636363636363636363636363636363636363
            636464640000000000006B6B6B6B6B6BA6A6A66B6B6BA4A4A4A4A4A4A4A4A4A4
            A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A5A5A56B6B6B6B6B6B000000000000
            6666660000000000000000000000000000000000000000000000000000000000
            000000000000000000006B6B6B6B6B6BA6A6A66B6B6B6B6B6B6B6B6B6B6B6B6B
            6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B000000000000
            6363636363636363636363636363636363636363636363636363636464640000
            000000000000000000006B6B6B6B6B6BA4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4
            A4A4A4A4A4A4A4A4A4A4A4A5A5A56B6B6B6B6B6B6B6B6B6B6B6B}
          NumGlyphs = 2
        end
        object btnEditarParcelaDepo: TJvTransparentButton
          Left = 43
          Top = 212
          Width = 30
          Height = 22
          Hint = 'Editar parcial do Dep'#243'sito selecionada'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          FrameStyle = fsIndent
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          Transparent = False
          OnClick = btnEditarParcelaDepoClick
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            1800000000000006000000000000000000000000000000000000000000000000
            636363636363000000A96A2BAB6B2BAB6B2B6363636363636363636363636363
            636363630000000000006B6B6B6B6B6BA4A4A4A4A4A46B6B6BA1A1A1A2A2A2A2
            A2A2A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A46B6B6B6B6B6B000000000000
            666666FFFFFFFFFFFFAB6B2BAB6B2BA96A2BAB6B2B000000FFFFFFFFFFFFFFFF
            FF6666660000000000006B6B6B6B6B6BA6A6A6FFFFFFFFFFFFA2A2A2A2A2A2A1
            A1A1A2A2A26B6B6BFFFFFFFFFFFFFFFFFFA6A6A66B6B6B6B6B6B636363000000
            666666FFFFFFFFFFFFAB6B2BA96A2B000000000000A96A2B000000FFFFFFFFFF
            FF666666000000000000A4A4A46B6B6BA6A6A6FFFFFFFFFFFFA2A2A2A1A1A16B
            6B6B6B6B6BA1A1A16B6B6BFFFFFFFFFFFFA6A6A66B6B6B6B6B6B666666000000
            666666FFFFFFFFFFFFFFFFFFAB6B2B000000AB6B2BAB6B2BA96A2B000000FFFF
            FF666666000000000000A6A6A66B6B6BA6A6A6FFFFFFFFFFFFFFFFFFA2A2A26B
            6B6BA2A2A2A2A2A2A1A1A16B6B6BFFFFFFA6A6A66B6B6B6B6B6B666666000000
            666666FFFFFFFFFFFFFFFFFFFFFFFFA96A2BAB6B2BAB6B2BAB6B2BA96A2B0000
            00666666000000000000A6A6A66B6B6BA6A6A6FFFFFFFFFFFFFFFFFFFFFFFFA1
            A1A1A2A2A2A2A2A2A2A2A2A1A1A16B6B6BA6A6A66B6B6B6B6B6B666666000000
            666666FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA96A2BAB6B2BAB6B2BAB6B2BA96A
            2B000000000000000000A6A6A66B6B6BA6A6A6FFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFA1A1A1A2A2A2A2A2A2A2A2A2A1A1A16B6B6B6B6B6B6B6B6B666666000000
            666666FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA96A2BAB6B2BAB6B2BAB6B
            2BA96A2B000000000000A6A6A66B6B6BA6A6A6FFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFA1A1A1A2A2A2A2A2A2A2A2A2A1A1A16B6B6B6B6B6B666666000000
            666666FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA96A2BAB6B2BAB6B
            2BAB6B2B000000000000A6A6A66B6B6BA6A6A6FFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFA1A1A1A2A2A2A2A2A2A2A2A26B6B6B6B6B6B666666000000
            666666FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEA96A2BAB6B
            2B000000000000A96A2BA6A6A66B6B6BA6A6A6FFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFEFEFEA1A1A1A2A2A26B6B6B6B6B6BA1A1A1666666000000
            666666FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F6363636363630000
            00000000A96A2BA96A2BA6A6A66B6B6BA6A6A6FFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFB4B4B4A4A4A4A4A4A46B6B6B6B6B6BA1A1A1A1A1A1666666000000
            666666FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF646464FDFDFDFFFFFFB1B1
            B1A96A2BA96A2BAB6B2BA6A6A66B6B6BA6A6A6FFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFA5A5A5FDFDFDFFFFFFD1D1D1A1A1A1A1A1A1A2A2A2666666000000
            666666FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF646464FDFDFDB1B1B16363
            63646464000000000000A6A6A66B6B6BA6A6A6FFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFA5A5A5FDFDFDD1D1D1A4A4A4A5A5A56B6B6B6B6B6B666666000000
            666666FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF666666AFAFAF6363636464
            64000000000000000000A6A6A66B6B6BA6A6A6FFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFA6A6A6D0D0D0A4A4A4A5A5A56B6B6B6B6B6B6B6B6B666666000000
            6363636363636363636363636363636363636363636363636363636464640000
            00000000000000000000A6A6A66B6B6BA4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4
            A4A4A4A4A4A4A4A4A4A4A4A5A5A56B6B6B6B6B6B6B6B6B6B6B6B666666000000
            0000000000000000000000000000000000000000000000000000000000000000
            00000000000000000000A6A6A66B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B
            6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B636363636363
            6363636363636363636363636363636363636363636464640000000000000000
            00000000000000000000A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4
            A4A4A4A4A4A5A5A56B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B}
          NumGlyphs = 2
        end
        object btnExcluirParcelaDepo: TJvTransparentButton
          Left = 79
          Top = 212
          Width = 30
          Height = 22
          Hint = 'Excluir parcial do Dep'#243'sito selecionada'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          FrameStyle = fsIndent
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          Transparent = False
          OnClick = btnExcluirParcelaDepoClick
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            1800000000000006000000000000000000000000000000000000000000000000
            2742C92742C92742C92742C92742C96363636363636363636363636363636363
            636363636363636363636B6B6B6B6B6BA6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A4
            A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A40000002742C9
            2742C92742C92742C92742C92742C92742C9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF6666666B6B6BA6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6
            A6A6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6A6A62742C92742C9
            2742C92742C92742C92742C92742C92742C92742C9FFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF666666A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6
            A6A6A6A6A6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6A6A62742C92742C9
            8A97E18A97E18A97E18A97E18A97E12742C92742C9FFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF666666A6A6A6A6A6A6CECECECECECECECECECECECECECECEA6
            A6A6A6A6A6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6A6A62742C92742C9
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2742C92742C9FFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF666666A6A6A6A6A6A6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6
            A6A6A6A6A6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6A6A62742C92742C9
            8A97E18A97E18A97E18A97E18A97E12742C92742C9FFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF666666A6A6A6A6A6A6CECECECECECECECECECECECECECECEA6
            A6A6A6A6A6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6A6A62742C92742C9
            2742C92742C92742C92742C92742C92742C92742C9FFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF666666A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6
            A6A6A6A6A6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6A6A60000002742C9
            2742C92742C92742C92742C92742C92742C9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF6666666B6B6BA6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6
            A6A6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6A6A6000000000000
            2742C92742C92742C92742C92742C9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE
            FEFEFEFEFEFEFE6666666B6B6B6B6B6BA6A6A6A6A6A6A6A6A6A6A6A6A6A6A6FF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFEFEFEA6A6A6000000000000
            666666000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F6363
            636363636363636363636B6B6B6B6B6BA6A6A66B6B6B6B6B6BFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFB4B4B4A4A4A4A4A4A4A4A4A4A4A4A4000000000000
            666666000000666666FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF646464FDFD
            FDFFFFFFB1B1B16363636B6B6B6B6B6BA6A6A66B6B6BA6A6A6FFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFA5A5A5FDFDFDFFFFFFD1D1D1A4A4A4000000000000
            666666000000666666FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF646464FDFD
            FDB1B1B16363636464646B6B6B6B6B6BA6A6A66B6B6BA6A6A6FFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFA5A5A5FDFDFDD1D1D1A4A4A4A5A5A5000000000000
            666666000000666666FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF666666AFAF
            AF6363636464640000006B6B6B6B6B6BA6A6A66B6B6BA6A6A6FFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFA6A6A6D0D0D0A4A4A4A5A5A56B6B6B000000000000
            6666660000006363636363636363636363636363636363636363636363636363
            636464640000000000006B6B6B6B6B6BA6A6A66B6B6BA4A4A4A4A4A4A4A4A4A4
            A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A5A5A56B6B6B6B6B6B000000000000
            6666660000000000000000000000000000000000000000000000000000000000
            000000000000000000006B6B6B6B6B6BA6A6A66B6B6B6B6B6B6B6B6B6B6B6B6B
            6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B000000000000
            6363636363636363636363636363636363636363636363636363636464640000
            000000000000000000006B6B6B6B6B6BA4A4A4A4A4A4A4A4A4A4A4A4A4A4A4A4
            A4A4A4A4A4A4A4A4A4A4A4A5A5A56B6B6B6B6B6B6B6B6B6B6B6B}
          NumGlyphs = 2
        end
        object btnConfirmarParcelaDepo: TJvTransparentButton
          Left = 743
          Top = 212
          Width = 30
          Height = 22
          Hint = 'Confirmar inclus'#227'o/edi'#231#227'o da parcial do Dep'#243'sito selecionada'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          FrameStyle = fsIndent
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          Transparent = False
          OnClick = btnConfirmarParcelaDepoClick
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            1800000000000006000000000000000000000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFF4F9EFDDE4ECD8E0ECD8E0EFDDE4FFF4F9FFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFAE6E6E6E2
            E2E2E2E2E2E6E6E6FAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFF3FAA3B6AA329064008A47008D49008D49008A47329064A3B6AAFFF3
            FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBABABAB6161613A3A3A39
            39393939393A3A3A616161ABABABFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFECF62F8D5F009A5B34CDA539DDB23AE1B33AE1B239DDB234CDA5009A5A2F8D
            5FFFECF6FFFFFFFFFFFFFFFFFFFFFFFFF8F8F85E5E5E47474788888892929295
            95959595959292928888884747475E5E5EF8F8F8FFFFFFFFFFFFFFFFFFFFF5FE
            1A8B550DB5833EE0B600D79700D49600D49700D49700D49600D7973EE0B50DB5
            831A8B55FFF5FEFFFFFFFFFFFFFFFFFF53535367676797979770707070707070
            7070707070707070707070979797676767535353FFFFFFFFFFFFFFFFFF75A78C
            00A97233DFB200D29500D29600CB8700CE8F00D39900D39900D29800D29533DF
            B200A97275A78CFFFFFFFFFFFF8E8E8E5959599191916D6D6D70707061616168
            68687373737373737272726D6D6D9191915959598E8E8EFFFFFFFFF6FE008641
            45DDB800D19400CF9404CE93FFFFFFA1EDD700C78400D29A00D19900D19900D1
            9445DDB6008641FFF6FEFFFFFF3333339999996D6D6D6D6D6D717171FFFFFFCA
            CACA5C5C5C7272727272727272726D6D6D999999333333FFFFFFA7C6B605AA75
            19D8A700CF9605CC93FFFFFFFFFFFFFFFFFF9DECD600C68400D09A00D09900CF
            9819D8A606AB74A5C9B6B9B3B65E5E5E8181816E6E6E707070FFFFFFFFFFFFFF
            FFFFC8C8C85B5B5B7272727272727070708282825E5E5EB6B6B678B7962EC69C
            00D09800C789FFFFFFFFFFFFA0EAD3FFFFFFFFFFFF9DEBD600C48400CF9A00CE
            9900D0982BC49974BC979C93978080806F6F6F5F5F5FFFFFFFFFFFFFC7C7C7FF
            FFFFFFFFFFC7C7C75B5B5B7171717070706F6F6F7F7F7F9898987CB79738D0A8
            00CC9400C99270E2C4B5EFDF00C18004CA92FFFFFFFFFFFF9DEAD600C28400CD
            9900CE972CC49979BC9A9D95998B8B8B6C6C6C696969AFAFAFD4D4D45454546F
            6F6FFFFFFFFFFFFFC8C8C85959597171716F6F6F7F7F7F9A9A9A75B49250DDB9
            00C99300CB9900C89100C78F00CB9900C99404C893FFFFFFFFFFFFA8ECD900C7
            8F00CC982BC49874BC979990949E9E9E6969697070706767676464647070706A
            6A6A6E6E6EFFFFFFFFFFFFCDCDCD6464646E6E6E7F7F7F989898B1D2BF38C69B
            18D3A700C99700CA9900CA9900CA9900CA9900C79404C793FFFFFFEFFBFA00C2
            8A19D3A604AA73B3D8C4C3BEC18585857F7F7F6C6C6C6E6E6E6E6E6E6E6E6E6E
            6E6E6969696F6F6FFFFFFFF6F6F65E5E5E8080805C5C5CC5C5C5FFFFFF07985C
            64F5D700BF8D00C89900C89900C89900C89900C89900C69421CCA116CC9E00C5
            9245DBB800853FFFFFFFFFFFFF525252B5B5B56262626F6F6F6F6F6F6F6F6F6F
            6F6F6F6F6F6969697F7F7F7A7A7A666666989898313131FFFFFFFFFFFF81BB9C
            6FE1C12FE0B800BF8D00C79900C79900C79900C79900C79900C69700C39233D8
            B300A76D90C3A8FFFFFFFFFFFF9E9E9EACACAC9191916363636E6E6E6E6E6E6D
            6D6D6D6D6D6D6D6D6A6A6A6565658E8E8E545454AAAAAAFFFFFFFFFFFFFFFFFF
            319B66A9FFF043E8C300BA8900C09200C29400C29400C29400C2963ED9B708B2
            7E2B9C68FFFFFFFFFFFFFFFFFFFFFFFF666666DADADA9E9E9E5C5C5C66666668
            6868686868686868676767959595636363656565FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFF41A2706ED5B49FFFF64DECCA43E2C042E1BF45E1C140D6B00094524AA8
            7AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF717171A5A5A5DADADAA6A6A69B
            9B9B9B9B9B9D9D9D9292923E3E3E797979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFCDE3D568B7913AAD7D33AC7B20A671109D6363B68FDCEBE1FFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D7D790909076767672
            72726666665959598D8D8DE3E3E3FFFFFFFFFFFFFFFFFFFFFFFF}
          NumGlyphs = 2
        end
        object btnCancelarParcelaDepo: TJvTransparentButton
          Left = 779
          Top = 212
          Width = 30
          Height = 22
          Hint = 'Cancelar inclus'#227'o/edi'#231#227'o da parcial do Dep'#243'sito selecionada'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          FrameStyle = fsIndent
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          Transparent = False
          OnClick = btnCancelarParcelaDepoClick
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            1800000000000006000000000000000000000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFF7F5EAE4E2D2E6E4D4E6E4D4E6E4D4E7E5D4E5E3D2F7F5EAFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1F1F1DCDCDCDEDEDEDF
            DFDFDFDFDFDFDFDFDDDDDDF1F1F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFDFAE4424EB71426BA1829B91627B71526B71223B70C1EB6424FB7FEFB
            E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F2F27474745A5A5A5C5C5C5A
            5A5A595959565656545454737373F3F3F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FDFAE4414EB44252D57184FF6D80FF6D80FF6D81FF6E81FF7387FF3142D0414D
            B4FEFBE4FFFFFFFFFFFFFFFFFFFFFFFFF2F2F2727272808080B3B3B3AEAEAEAE
            AEAEAFAFAFAFAFAFB4B4B4747474727272F3F3F3FFFFFFFFFFFFFFFFFFFCFAE4
            434FB54B5BD8667AFF5469FA5E72FB5E71FB5E71FB5E72FB5469FA697DFF3041
            D0414DB5FEFBE4FFFFFFFFFFFFF2F2F2737373878787A9A9A99B9B9BA1A1A1A0
            A0A0A0A0A0A1A1A19B9B9BACACAC737373737373F3F3F3FFFFFFFFFCEA434FB5
            5464D95D72FF3C54F8A5AFFC3F55F85B6FF95B6FF93D55F8A5AFFC3C54F86579
            FF2F41CF404DB4FFFFF1F5F5F57373738D8D8DA4A4A48C8C8CC9C9C98D8D8D9E
            9E9E9E9E9E8D8D8DC9C9C98C8C8CA9A9A9737373727272F6F6F68991D35564DA
            556BFF435BF6C2CAFCFFFFFFC8CEFC324BF6324BF6CCD2FCFFFFFFC3CBFC435A
            F65E74FF2639CC9AA2E8ABAAA28E8E8E9E9E9E8F8F8FDADADAFFFFFFDEDEDE85
            8585858585E1E1E1FFFFFFDBDBDB909090A4A4A46D6D6DADADAD8E95D45E70E7
            4E64F83B54F4E1E5FDFFFFFFFFFFFFA6B1FAA6B1FAFFFFFFFFFFFFDDE1FC3C55
            F45368F9384CDC9EA6E8AEADA49898989696968A8A8AEEEEEEFFFFFFFFFFFFCA
            CACACACACAFFFFFFFFFFFFEBEBEB8A8A8A9A9A9A7F7F7FAFAFAF8E95D26373E7
            495FF54C62F32540F1DADFFDFFFFFFFFFFFFFFFFFFFFFFFFDADFFD2641F14C62
            F34C63F73A4DDA9EA6E8ADACA49C9C9C9292929494947C7C7CE9E9E9FFFFFFFF
            FFFFFFFFFFFFFFFFE9E9E97C7C7C9494949595957F7F7FAFAFAF8D94D26878E7
            425BF1475EF04960F00324EBF2F3FEFFFFFFFFFFFFF2F3FE0324EB4960F0475E
            F0475FF43B4FD99EA6E8ACABA39D9D9D8D8D8D8F8F8F919191666666F7F7F7FF
            FFFFFFFFFFF7F7F76666669191918F8F8F919191808080AFAFAF8C93D26C7CE9
            3D57EF435CEE1C3AEBB2BCF7FFFFFFFFFFFFFFFFFFFFFFFFB2BCF71C3AEB435C
            EE425BF23C50DA9FA6E8ACABA3A2A2A28989898C8C8C757575D0D0D0FFFFFFFF
            FFFFFFFFFFFFFFFFD0D0D07575758C8C8C8E8E8E808080AFAFAF8991D16E7FEA
            3852ED2B46EBB9C2F8FFFFFFFFFFFFBFC7F9BFC7F9FFFFFFFFFFFFB9C2F82B46
            EB3D57EE3D51DB9CA4E8ABAAA1A3A3A38686867D7D7DD4D4D4FFFFFFFFFFFFD8
            D8D8D8D8D8FFFFFFFFFFFFD4D4D47D7D7D898989818181AEAEAE8991D78894E5
            2241EB203DE8E3E6FBFFFFFFD5DAFA0F2EE60F2EE6D5DAFAFFFFFFE3E6FB203D
            E83350F03D4DD0959EE6ADACA4B0B0B0787878767676EDEDEDFFFFFFE5E5E56B
            6B6B6B6B6BE5E5E5FFFFFFEDEDED7676768585857C7C7CB1B1B1FFFFFF5C66CB
            97A1E91A3AE90F2EE4ABB6F50E2DE4354FE8354FE80E2DE4ABB6F50E2EE42B49
            ED4D5DD35963C9FFFFFFFFFFFF8A8A8ABABABA7474746A6A6ACBCBCB6A6A6A83
            83838383836A6A6ACBCBCB6A6A6A7F7F7F868686878787FFFFFFFFFFFFFFFFFF
            5E69CBA4AEEC1333E52240E5314CE6304BE6304BE6314CE6223FE42341EA5767
            D65964C9FFFFFFFFFFFFFFFFFFFFFFFF8C8C8CC3C3C36D6D6D7777777F7F7F7E
            7E7E7E7E7E7F7F7F7676767878788D8D8D888888FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFF606BCCABB2EE0E30E51C3CE61D3DE61E3DE7203FE71A3BE9606FD95B66
            CAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EC7C7C76A6A6A73737374
            7474757575767676747474949494898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFF737CD3727CD56974D26470D2616CD05A67CF5560CC6D78D1FFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9B9B9B9C9C9C95959593
            93938F8F8F8B8B8B878787979797FFFFFFFFFFFFFFFFFFFFFFFF}
          NumGlyphs = 2
        end
        object dbgDepositosDerivados: TDBGrid
          Left = 7
          Top = 60
          Width = 804
          Height = 149
          DataSource = dsCadastro
          FixedColor = clWhite
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 6
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          OnDrawColumnCell = dbgDepositosDerivadosDrawColumnCell
          OnDblClick = dbgDepositosDerivadosDblClick
          OnKeyPress = FormKeyPress
          Columns = <
            item
              Expanded = False
              FieldName = 'ID_DEPOSITO_FLUTUANTE'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'DATA_DEPOSITO'
              Title.Caption = 'Dt. Dep'#243'sito'
              Width = 85
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NUM_COD_DEPOSITO'
              Title.Caption = 'Identifica'#231#227'o Dep'#243'sito'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DESCR_DEPOSITO_FLUTUANTE'
              Title.Caption = 'Descri'#231#227'o'
              Width = 335
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VR_DEPOSITO_FLUTUANTE'
              Title.Caption = 'Valor'
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'FLG_STATUS'
              Title.Alignment = taCenter
              Title.Caption = 'Situa'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATA_STATUS'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'ST_ID_USUARIO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'CAD_ID_USUARIO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'DATA_CADASTRO'
              Visible = False
            end>
        end
        object dteDataDepositoD: TJvDBDateEdit
          Left = 5
          Top = 32
          Width = 100
          Height = 22
          DataField = 'DATA_DEPOSITO'
          DataSource = dsCadastro
          ReadOnly = True
          Color = clMenu
          ShowNullDate = False
          TabOrder = 0
          OnKeyPress = FormKeyPress
        end
        object edtNumCodDepositoD: TDBEdit
          Left = 111
          Top = 32
          Width = 150
          Height = 22
          Color = clMenu
          DataField = 'NUM_COD_DEPOSITO'
          DataSource = dsCadastro
          ReadOnly = True
          TabOrder = 1
          OnKeyPress = FormKeyPress
        end
        object edtDescricaoDepositoD: TDBEdit
          Left = 267
          Top = 32
          Width = 200
          Height = 22
          CharCase = ecUpperCase
          Color = 16114127
          DataField = 'DESCR_DEPOSITO_FLUTUANTE'
          DataSource = dsCadastro
          TabOrder = 2
          OnKeyPress = FormKeyPress
        end
        object cedValorDepositoD: TJvDBCalcEdit
          Left = 473
          Top = 32
          Width = 100
          Height = 22
          Color = 16114127
          DisplayFormat = ',0.00'
          TabOrder = 3
          DecimalPlacesAlwaysShown = False
          OnKeyPress = cedValorDepositoDKeyPress
          DataField = 'VR_DEPOSITO_FLUTUANTE'
          DataSource = dsCadastro
        end
        object dteDataSituacaoD: TJvDBDateEdit
          Left = 709
          Top = 32
          Width = 100
          Height = 22
          DataField = 'DATA_FLUTUANTE'
          DataSource = dsCadastro
          ReadOnly = True
          Color = clMenu
          ShowNullDate = False
          TabOrder = 5
          OnKeyPress = FormKeyPress
        end
        object edtSituacaoD: TEdit
          Left = 579
          Top = 32
          Width = 124
          Height = 22
          Color = clMenu
          ReadOnly = True
          TabOrder = 4
          Text = 'FLUTUANTE'
        end
      end
      object cedTotalDesmembrado: TJvCalcEdit
        Left = 720
        Top = 378
        Width = 100
        Height = 22
        Color = clMenu
        DisplayFormat = ',0.00'
        ReadOnly = True
        TabOrder = 3
        DecimalPlacesAlwaysShown = False
        OnKeyPress = FormKeyPress
      end
    end
    inherited pnlMenu: TPanel
      Height = 465
      ExplicitHeight = 465
      inherited btnDadosPrincipais: TJvTransparentButton
        Caption = 'Desmembramento de Dep'#243'sito'
      end
      inherited btnOk: TJvTransparentButton
        Top = 375
        ExplicitTop = 375
      end
      inherited btnCancelar: TJvTransparentButton
        Top = 420
        ExplicitTop = 420
      end
    end
  end
  inherited dsCadastro: TDataSource
    DataSet = cdsDepoD
    Left = 889
    Top = 422
  end
  object qryDepoD: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT D.*,'
      '       CAST((CASE D.FLG_STATUS'
      '                  WHEN '#39'P'#39' THEN CAST(D.DATA_CADASTRO AS DATE)'
      '                  WHEN '#39'F'#39' THEN D.DATA_FLUTUANTE'
      '                  WHEN '#39'B'#39' THEN D.DATA_BAIXADO'
      '                  WHEN '#39'C'#39' THEN D.DATA_CANCELAMENTO'
      '             END) AS DATE) AS DATA_STATUS'
      '  FROM DEPOSITO_FLUTUANTE D'
      ' WHERE (1 = 0)')
    Left = 731
    Top = 422
  end
  object qryDepoP: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT D.*,'
      '       CAST((CASE D.FLG_STATUS'
      '                  WHEN '#39'P'#39' THEN CAST(D.DATA_CADASTRO AS DATE)'
      '                  WHEN '#39'F'#39' THEN D.DATA_FLUTUANTE'
      '                  WHEN '#39'B'#39' THEN D.DATA_BAIXADO'
      '                  WHEN '#39'C'#39' THEN D.DATA_CANCELAMENTO'
      '             END) AS DATE) AS DATA_STATUS'
      '  FROM DEPOSITO_FLUTUANTE D'
      ' WHERE D.ID_DEPOSITO_FLUTUANTE = :ID_DEPOSITO_FLUTUANTE')
    Left = 411
    Top = 422
    ParamData = <
      item
        Position = 1
        Name = 'ID_DEPOSITO_FLUTUANTE'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspDepoD: TDataSetProvider
    DataSet = qryDepoD
    Left = 783
    Top = 422
  end
  object cdsDepoD: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspDepoD'
    Left = 834
    Top = 422
    object cdsDepoDID_DEPOSITO_FLUTUANTE: TIntegerField
      FieldName = 'ID_DEPOSITO_FLUTUANTE'
      Required = True
    end
    object cdsDepoDDESCR_DEPOSITO_FLUTUANTE: TStringField
      FieldName = 'DESCR_DEPOSITO_FLUTUANTE'
      Size = 250
    end
    object cdsDepoDDATA_DEPOSITO: TDateField
      FieldName = 'DATA_DEPOSITO'
    end
    object cdsDepoDNUM_COD_DEPOSITO: TStringField
      FieldName = 'NUM_COD_DEPOSITO'
      Size = 25
    end
    object cdsDepoDVR_DEPOSITO_FLUTUANTE: TBCDField
      FieldName = 'VR_DEPOSITO_FLUTUANTE'
      OnGetText = cdsDepoDVR_DEPOSITO_FLUTUANTEGetText
      Precision = 18
      Size = 2
    end
    object cdsDepoDFLG_STATUS: TStringField
      FieldName = 'FLG_STATUS'
      FixedChar = True
      Size = 1
    end
    object cdsDepoDDATA_STATUS: TDateField
      FieldName = 'DATA_STATUS'
    end
    object cdsDepoDCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsDepoDDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsDepoDFLUT_ID_USUARIO: TIntegerField
      FieldName = 'FLUT_ID_USUARIO'
    end
    object cdsDepoDBAIXA_ID_USUARIO: TIntegerField
      FieldName = 'BAIXA_ID_USUARIO'
    end
    object cdsDepoDCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsDepoDDATA_FLUTUANTE: TDateField
      FieldName = 'DATA_FLUTUANTE'
    end
    object cdsDepoDDATA_BAIXADO: TDateField
      FieldName = 'DATA_BAIXADO'
    end
    object cdsDepoDDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
  end
  object dsDepoP: TDataSource
    DataSet = cdsDepoP
    Left = 562
    Top = 422
  end
  object dspDepoP: TDataSetProvider
    DataSet = qryDepoP
    Left = 462
    Top = 422
  end
  object cdsDepoP: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_DEPOSITO_FLUTUANTE'
        ParamType = ptInput
      end>
    ProviderName = 'dspDepoP'
    Left = 514
    Top = 422
    object cdsDepoPID_DEPOSITO_FLUTUANTE: TIntegerField
      FieldName = 'ID_DEPOSITO_FLUTUANTE'
      Required = True
    end
    object cdsDepoPDESCR_DEPOSITO_FLUTUANTE: TStringField
      FieldName = 'DESCR_DEPOSITO_FLUTUANTE'
      Size = 250
    end
    object cdsDepoPDATA_DEPOSITO: TDateField
      FieldName = 'DATA_DEPOSITO'
    end
    object cdsDepoPNUM_COD_DEPOSITO: TStringField
      FieldName = 'NUM_COD_DEPOSITO'
      Size = 25
    end
    object cdsDepoPVR_DEPOSITO_FLUTUANTE: TBCDField
      FieldName = 'VR_DEPOSITO_FLUTUANTE'
      OnGetText = cdsDepoPVR_DEPOSITO_FLUTUANTEGetText
      Precision = 18
      Size = 2
    end
    object cdsDepoPFLG_STATUS: TStringField
      FieldName = 'FLG_STATUS'
      FixedChar = True
      Size = 1
    end
    object cdsDepoPDATA_STATUS: TDateField
      FieldName = 'DATA_STATUS'
    end
    object cdsDepoPCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsDepoPDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsDepoPVR_ANTIGO: TCurrencyField
      FieldKind = fkInternalCalc
      FieldName = 'VR_ANTIGO'
    end
    object cdsDepoPVR_DIFERENCA: TCurrencyField
      FieldKind = fkInternalCalc
      FieldName = 'VR_DIFERENCA'
    end
    object cdsDepoPFLUT_ID_USUARIO: TIntegerField
      FieldName = 'FLUT_ID_USUARIO'
    end
    object cdsDepoPBAIXA_ID_USUARIO: TIntegerField
      FieldName = 'BAIXA_ID_USUARIO'
    end
    object cdsDepoPCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsDepoPDATA_FLUTUANTE: TDateField
      FieldName = 'DATA_FLUTUANTE'
    end
    object cdsDepoPDATA_BAIXADO: TDateField
      FieldName = 'DATA_BAIXADO'
    end
    object cdsDepoPDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
  end
end
