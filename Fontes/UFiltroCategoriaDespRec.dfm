inherited FFiltroCategoriaDespRec: TFFiltroCategoriaDespRec
  Caption = 'Filtro de Categorias de Lan'#231'amento'
  ClientHeight = 274
  ClientWidth = 809
  OnCreate = FormCreate
  ExplicitWidth = 815
  ExplicitHeight = 303
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 803
    Height = 268
    ExplicitWidth = 803
    ExplicitHeight = 268
    inherited pnlGrid: TPanel
      Top = 49
      Width = 719
      Height = 216
      ExplicitTop = 49
      ExplicitWidth = 719
      ExplicitHeight = 216
      inherited pnlGridPai: TPanel
        Width = 719
        Height = 216
        ExplicitWidth = 719
        ExplicitHeight = 216
        inherited dbgGridPaiPadrao: TDBGrid
          Width = 719
          Height = 216
          Columns = <
            item
              Expanded = False
              FieldName = 'ID'
              Title.Caption = 'Id'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'TIPO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'DESCR_NATUREZA'
              Title.Caption = 'Nome da Natureza'
              Width = 230
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DESCR_CATEGORIA_DESPREC'
              Title.Caption = 'Nome da Categoria'
              Width = 455
              Visible = True
            end>
        end
      end
    end
    inherited pnlFiltro: TPanel
      Width = 803
      Height = 46
      ExplicitWidth = 803
      ExplicitHeight = 46
      inherited btnFiltrar: TJvTransparentButton
        Left = 628
        Top = 17
        ExplicitLeft = 628
        ExplicitTop = 17
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 717
        Top = 17
        ExplicitLeft = 717
        ExplicitTop = 17
      end
      object lblCategoria: TLabel
        Left = 345
        Top = 6
        Width = 51
        Height = 14
        Caption = 'Categoria'
      end
      object lblNatureza: TLabel
        Left = 81
        Top = 5
        Width = 48
        Height = 14
        Caption = 'Natureza'
      end
      object edtCategoria: TEdit
        Left = 345
        Top = 21
        Width = 277
        Height = 22
        Color = 16114127
        TabOrder = 1
        OnKeyPress = edtCategoriaKeyPress
      end
      object cbNatureza: TComboBox
        Left = 81
        Top = 21
        Width = 258
        Height = 22
        Style = csOwnerDrawFixed
        Color = 16114127
        TabOrder = 0
        OnKeyPress = cbNaturezaKeyPress
      end
    end
    inherited pnlMenu: TPanel
      Top = 49
      Height = 216
      ExplicitTop = 49
      ExplicitHeight = 216
      inherited btnImprimir: TJvTransparentButton
        Visible = False
      end
    end
  end
  inherited dsGridPaiPadrao: TDataSource
    Left = 716
    Top = 171
  end
  inherited qryGridPaiPadrao: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT C.ID_CATEGORIA_DESPREC AS ID,'
      '       C.TIPO,'
      '       C.DESCR_CATEGORIA_DESPREC,'
      '       C.ID_NATUREZA_FK,'
      '       N.DESCR_NATUREZA'
      '  FROM CATEGORIA_DESPREC C'
      ' INNER JOIN NATUREZA N'
      '    ON C.ID_NATUREZA_FK = N.ID_NATUREZA'
      ' WHERE C.TIPO = :TIPO'
      'ORDER BY C.DESCR_CATEGORIA_DESPREC')
    Left = 716
    Top = 119
    ParamData = <
      item
        Name = 'TIPO'
        DataType = ftString
        ParamType = ptInput
      end>
    object qryGridPaiPadraoID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID_CATEGORIA_DESPREC'
      Required = True
    end
    object qryGridPaiPadraoTIPO: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      FixedChar = True
      Size = 1
    end
    object qryGridPaiPadraoDESCR_CATEGORIA_DESPREC: TStringField
      FieldName = 'DESCR_CATEGORIA_DESPREC'
      Origin = 'DESCR_CATEGORIA_DESPREC'
      Size = 250
    end
    object qryGridPaiPadraoID_NATUREZA_FK: TIntegerField
      FieldName = 'ID_NATUREZA_FK'
      Origin = 'ID_NATUREZA_FK'
    end
    object qryGridPaiPadraoDESCR_NATUREZA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DESCR_NATUREZA'
      Origin = 'DESCR_NATUREZA'
      ProviderFlags = []
      ReadOnly = True
      Size = 250
    end
  end
  object qryNatureza: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM NATUREZA'
      ' WHERE DESCR_NATUREZA = :DESCR_NATUREZA'
      '   AND TIPO_NATUREZA = :TIPO_NATUREZA')
    Left = 116
    Top = 116
    ParamData = <
      item
        Name = 'DESCR_NATUREZA'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'TIPO_NATUREZA'
        ParamType = ptInput
      end>
  end
end
