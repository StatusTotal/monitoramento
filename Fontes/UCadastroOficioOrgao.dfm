inherited FCadastroOficioOrgao: TFCadastroOficioOrgao
  Caption = 'Cadastro de '#211'rg'#227'o do Of'#237'cio'
  ClientHeight = 126
  ClientWidth = 488
  ExplicitWidth = 494
  ExplicitHeight = 155
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 482
    Height = 120
    ExplicitWidth = 482
    ExplicitHeight = 120
    inherited pnlDados: TPanel
      Width = 368
      Height = 114
      ExplicitWidth = 368
      ExplicitHeight = 114
      object lblNome: TLabel [0]
        Left = 5
        Top = 8
        Width = 32
        Height = 14
        Caption = 'Nome'
      end
      inherited pnlMsgErro: TPanel
        Top = 53
        Width = 368
        ExplicitTop = 53
        ExplicitWidth = 368
        inherited lblMensagemErro: TLabel
          Width = 368
        end
        inherited lbMensagemErro: TListBox
          Width = 368
          OnDblClick = lbMensagemErroDblClick
          ExplicitWidth = 368
        end
      end
      object edtNome: TDBEdit
        Left = 5
        Top = 24
        Width = 361
        Height = 22
        CharCase = ecUpperCase
        Color = 16114127
        DataField = 'NOME_OFICIO_ORGAO'
        DataSource = dsCadastro
        TabOrder = 1
        OnKeyPress = edtNomeKeyPress
      end
    end
    inherited pnlMenu: TPanel
      Height = 114
      ExplicitHeight = 114
      inherited btnOk: TJvTransparentButton
        Top = 24
        ExplicitTop = 42
      end
      inherited btnCancelar: TJvTransparentButton
        Top = 69
        ExplicitTop = 73
      end
    end
  end
  inherited dsCadastro: TDataSource
    DataSet = cdsOfcOrgao
    Left = 438
    Top = 69
  end
  object qryOfcOrgao: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM OFICIO_ORGAO'
      ' WHERE ID_OFICIO_ORGAO = :ID_OFICIO_ORGAO')
    Left = 238
    Top = 69
    ParamData = <
      item
        Position = 1
        Name = 'ID_OFICIO_ORGAO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspOfcOrgao: TDataSetProvider
    DataSet = qryOfcOrgao
    Left = 306
    Top = 69
  end
  object cdsOfcOrgao: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_OFICIO_ORGAO'
        ParamType = ptInput
      end>
    ProviderName = 'dspOfcOrgao'
    Left = 374
    Top = 69
    object cdsOfcOrgaoID_OFICIO_ORGAO: TIntegerField
      FieldName = 'ID_OFICIO_ORGAO'
      Required = True
    end
    object cdsOfcOrgaoNOME_OFICIO_ORGAO: TStringField
      FieldName = 'NOME_OFICIO_ORGAO'
      Size = 250
    end
  end
end
