inherited FFiltroRelatorioFinDespRec: TFFiltroRelatorioFinDespRec
  Caption = 'FFiltroRelatorioFinDespRec'
  ExplicitWidth = 522
  ExplicitHeight = 174
  PixelsPerInch = 96
  TextHeight = 14
  inherited frptRelatorioSNT: TfrxReport
    ReportOptions.LastChange = 42599.622501550930000000
    Top = 74
    Datasets = <
      item
        DataSet = fdsRelAnalitico
        DataSetName = 'fdsRANL'
      end
      item
        DataSet = fdsFirmas
        DataSetName = 'fdsRFirmas'
      end
      item
        DataSet = fdsRelSintetico
        DataSetName = 'fdsRSNT'
      end>
    Variables = <>
    Style = <>
    inherited pgSintetico: TfrxReportPage
      inherited pghCabecalhoS: TfrxPageHeader
        inherited mmPeriodoRelatorioS: TfrxMemoView
          Left = 258.897805000000000000
        end
        inherited mmDataRelatorioS: TfrxMemoView
          Width = 83.370130000000000000
        end
        inherited mmHoraRelatorioS: TfrxMemoView
          Width = 83.370130000000000000
        end
        inherited mmUsuarioRelatorioS: TfrxMemoView
          Width = 83.370130000000000000
        end
        object SysMemo1: TfrxSysMemoView
          Left = 360.833333330000000000
          Top = 81.102350000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
        end
      end
      inherited pgfRodapeS: TfrxPageFooter
        Top = 1018.744279990000000000
        inherited mmPaginasRelatorioS: TfrxMemoView
          Top = 3.779530000000000000
          Width = 128.504020000000000000
        end
      end
      object Memo1: TfrxMemoView
        Align = baWidth
        Top = 151.833333330000000000
        Width = 359.999999990000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          'RECEITAS')
        ParentFont = False
      end
      object Memo2: TfrxMemoView
        Align = baWidth
        Left = 359.999999990000000000
        Top = 152.000000000000000000
        Width = 358.110700010000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          'DESPESAS')
        ParentFont = False
      end
      object Memo3: TfrxMemoView
        Top = 171.000000000000000000
        Width = 94.488250000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          'Descri'#231#227'o')
        ParentFont = False
      end
      object Memo4: TfrxMemoView
        Left = 244.999999990000000000
        Top = 171.000000000000000000
        Width = 94.488250000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          'Valor')
        ParentFont = False
      end
      object Memo5: TfrxMemoView
        Left = 379.833333330000000000
        Top = 171.000000000000000000
        Width = 94.488250000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          'Descri'#231#227'o')
        ParentFont = False
      end
      object Memo6: TfrxMemoView
        Left = 623.999999990000000000
        Top = 171.000000000000000000
        Width = 94.488250000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          'Valor')
        ParentFont = False
      end
    end
  end
  inherited qryRelAnalitico: TFDQuery
    Left = 287
    Top = 58
  end
  inherited fdsRelAnalitico: TfrxDBDataset
    Left = 286
    Top = 106
  end
  inherited fdsRelSintetico: TfrxDBDataset
    Left = 134
  end
  inherited qryRelSintetico: TFDQuery
    Left = 134
  end
  inherited frptRelatorioANL: TfrxReport
    Top = 122
    Datasets = <
      item
        DataSet = fdsRelAnalitico
        DataSetName = 'fdsRANL'
      end
      item
        DataSet = fdsRelSintetico
        DataSetName = 'fdsRSNT'
      end>
    Variables = <>
    Style = <>
  end
  object fdsFirmas: TfrxDBDataset
    UserName = 'fdsRFirmas'
    CloseDataSource = False
    DataSet = qryFirmas
    BCDToCurrency = False
    Left = 201
    Top = 114
  end
  object qryFirmas: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT LD.COD_ADICIONAL,'
      '       (CASE LD.COD_ADICIONAL'
      '          WHEN 2013 THEN '#39'Autentica'#231#227'o'#39
      '          WHEN 2024 THEN '#39'Abertura de Firma'#39
      '          WHEN 2355 THEN '#39'Rec. Autenticidade'#39
      '          WHEN 2356 THEN '#39'Rec. Semelhan'#231'a'#39
      '          ELSE '#39#39
      '        END) AS DESCR_SERVICO,'
      '       COUNT(LD.ID_LANCAMENTO_DET) AS QTD,'
      '       SUM(L.VR_TOTAL_PAGO) AS TOTAL_PAGO,'
      '       (CASE WHEN (L.ID_FORMAPAGAMENTO_FK = 4)'
      '             THEN '#39'FT'#39
      '             ELSE LD.TIPO_COBRANCA'
      '        END) AS TIPO_COB'
      '  FROM LANCAMENTO L'
      ' INNER JOIN LANCAMENTO_DET LD'
      '    ON L.COD_LANCAMENTO = LD.COD_LANCAMENTO_FK'
      '   AND L.ANO_LANCAMENTO = LD.ANO_LANCAMENTO_FK'
      ' WHERE L.TIPO_LANCAMENTO = '#39'R'#39
      '   AND L.ID_NATUREZA_FK IN (4, 5)'
      '   AND L.FLG_STATUS = '#39'G'#39
      '   AND L.DATA_CADASTRO BETWEEN :DATA_INI AND :DATA_FIM'
      'GROUP BY LD.COD_ADICIONAL,'
      '         TIPO_COB')
    Left = 202
    Top = 66
    ParamData = <
      item
        Name = 'DATA_INI'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'DATA_FIM'
        DataType = ftDate
        ParamType = ptInput
      end>
  end
end
