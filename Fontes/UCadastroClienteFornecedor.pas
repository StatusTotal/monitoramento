{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroClienteFornecedor.pas
  Descricao:   Tela de cadastro de Cliente e Fornecedor
  Author   :   Cristina
  Date:        18-mar-2016
  Last Update: 23-out-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroClienteFornecedor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.DBClient, Datasnap.Provider,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Mask, Vcl.DBCtrls, JvExMask,
  JvToolEdit, JvMaskEdit, JvDBControls, JvBaseEdits, UDM, System.StrUtils;

type
  TFCadastroClienteFornecedor = class(TFCadastroGeralPadrao)
    qryCliFor: TFDQuery;
    dspCliFor: TDataSetProvider;
    cdsCliFor: TClientDataSet;
    rgTipoPessoa: TDBRadioGroup;
    edtNomeFantasia: TDBEdit;
    lblNomeFantasia: TLabel;
    lblRazaoSocial: TLabel;
    edtRazaoSocial: TDBEdit;
    lblLogradouro: TLabel;
    lblNumero: TLabel;
    lblComplemento: TLabel;
    edtLogradouro: TDBEdit;
    edtNumero: TDBEdit;
    edtComplemento: TDBEdit;
    lblBairro: TLabel;
    lblCEP: TLabel;
    lblUF: TLabel;
    lblCidade: TLabel;
    edtBairro: TDBEdit;
    medCEP: TJvDBMaskEdit;
    lcbUF: TDBLookupComboBox;
    edtCidade: TDBEdit;
    lcbCidade: TDBLookupComboBox;
    gbTelefones: TGroupBox;
    gbCelulares: TGroupBox;
    gbEmailSite: TGroupBox;
    lblRepresentante: TLabel;
    medTelefone1: TJvDBMaskEdit;
    medTelefone2: TJvDBMaskEdit;
    medTelefone3: TJvDBMaskEdit;
    medCelular1: TJvDBMaskEdit;
    medCelular2: TJvDBMaskEdit;
    medCelular3: TJvDBMaskEdit;
    edtEmail1: TDBEdit;
    edtEmail2: TDBEdit;
    edtSite: TDBEdit;
    lblEmail1: TLabel;
    lblEmail2: TLabel;
    lblSite: TLabel;
    edtRepresentante: TDBEdit;
    mmObservacao: TDBMemo;
    lblObservacao: TLabel;
    lblSaldoCredor: TLabel;
    lblSaldoDevedor: TLabel;
    cedSaldoCredor: TJvDBCalcEdit;
    cedSaldoDevedor: TJvDBCalcEdit;
    ntbFisJur: TNotebook;
    lblCPF: TLabel;
    lblDataNascimento: TLabel;
    dteDataNascimento: TJvDBDateEdit;
    lblCNPJ: TLabel;
    lblInscricaoEstadual: TLabel;
    lblInscricaoMunicipal: TLabel;
    edtInscricaoEstadual: TDBEdit;
    edtInscricaoMunicipal: TDBEdit;
    lblDataCadastro: TLabel;
    dteDataCadastro: TJvDBDateEdit;
    rgFlgAtivo: TDBRadioGroup;
    lblDataInativacao: TLabel;
    lblDataBloqueio: TLabel;
    dteDataInativacao: TJvDBDateEdit;
    dteDataBloqueio: TJvDBDateEdit;
    lblBloqueado: TLabel;
    cbBloqueado: TDBComboBox;
    medCPF: TJvMaskEdit;
    medCNPJ: TJvMaskEdit;
    dsUF: TDataSource;
    dsCidade: TDataSource;
    qryUF: TFDQuery;
    qryCidade: TFDQuery;
    cdsCliForID_CLIENTE_FORNECEDOR: TIntegerField;
    cdsCliForTIPO_CLIENTE_FORNECEDOR: TStringField;
    cdsCliForNOME_FANTASIA: TStringField;
    cdsCliForRAZAO_SOCIAL: TStringField;
    cdsCliForTIPO_PESSOA: TStringField;
    cdsCliForCPF_CNPJ: TStringField;
    cdsCliForINSCRICAO_ESTADUAL: TStringField;
    cdsCliForINSCRICAO_MUNICIPAL: TStringField;
    cdsCliForDATA_NASCIMENTO: TDateField;
    cdsCliForLOGRADOURO: TStringField;
    cdsCliForNUM_LOGR: TStringField;
    cdsCliForCOMPLEMENTO_LOGR: TStringField;
    cdsCliForBAIRRO_LOGR: TStringField;
    cdsCliForUF_LOGR: TStringField;
    cdsCliForTELEFONE_1: TStringField;
    cdsCliForTELEFONE_2: TStringField;
    cdsCliForTELEFONE_3: TStringField;
    cdsCliForCELULAR_1: TStringField;
    cdsCliForCELULAR_2: TStringField;
    cdsCliForCELULAR_3: TStringField;
    cdsCliForEMAIL_1: TStringField;
    cdsCliForEMAIL_2: TStringField;
    cdsCliForSITE: TStringField;
    cdsCliForOBSERVACAO: TStringField;
    cdsCliForNOME_REPRESENTANTE: TStringField;
    cdsCliForSALDO_CREDOR: TBCDField;
    cdsCliForSALDO_DEVEDOR: TBCDField;
    cdsCliForDATA_CADASTRO: TDateField;
    cdsCliForCAD_ID_USUARIO: TIntegerField;
    cdsCliForFLG_ATIVO: TStringField;
    cdsCliForDATA_DESATIVACAO: TDateField;
    cdsCliForFLG_BLOQUEADO: TStringField;
    cdsCliForDATA_BLOQUEIO: TDateField;
    cdsCliForCEP_LOGR: TStringField;
    cdsCliForDOCUMENTO: TStringField;
    cdsCliForCOD_MUNICIPIO_LOGR: TStringField;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure rgFlgAtivoExit(Sender: TObject);
    procedure cbBloqueadoExit(Sender: TObject);
    procedure mmObservacaoKeyPress(Sender: TObject; var Key: Char);
    procedure btnOkClick(Sender: TObject);
    procedure lcbUFExit(Sender: TObject);
    procedure edtCidadeExit(Sender: TObject);
    procedure lcbCidadeExit(Sender: TObject);
    procedure cdsCliForSALDO_CREDORGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsCliForSALDO_DEVEDORGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure rgTipoPessoaClick(Sender: TObject);
    procedure rgTipoPessoaExit(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }

    IdNovoCliFor: Integer;

    TipoCliFor: TTipoClienteFornecedor;
  end;

var
  FCadastroClienteFornecedor: TFCadastroClienteFornecedor;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UGDM, UVariaveisGlobais;

{ TFCadastroClienteFornecedor }

procedure TFCadastroClienteFornecedor.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroClienteFornecedor.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroClienteFornecedor.cbBloqueadoExit(Sender: TObject);
begin
  inherited;

  if Trim(cbBloqueado.Text) = 'S' then
  begin
    cdsCliFor.FieldByName('FLG_BLOQUEADO').AsString := 'S';
    dteDataBloqueio.Date := Date;
  end
  else
  begin
    cdsCliFor.FieldByName('FLG_BLOQUEADO').AsString := 'N';
    dteDataBloqueio.Clear;
  end;
end;

procedure TFCadastroClienteFornecedor.cdsCliForSALDO_CREDORGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFCadastroClienteFornecedor.cdsCliForSALDO_DEVEDORGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFCadastroClienteFornecedor.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtNomeFantasia.MaxLength  := 250;
  edtRazaoSocial.MaxLength   := 250;
  edtLogradouro.MaxLength    := 250;
  edtNumero.MaxLength        := 25;
  edtComplemento.MaxLength   := 100;
  edtBairro.MaxLength        := 250;
  edtCidade.MaxLength        := 7;
  edtEmail1.MaxLength        := 250;
  edtEmail2.MaxLength        := 250;
  edtSite.MaxLength          := 300;
  mmObservacao.MaxLength     := 1000;
  edtRepresentante.MaxLength := 250;
end;

procedure TFCadastroClienteFornecedor.DesabilitarComponentes;
begin
  inherited;

  if vgOperacao = I then
  begin
    lblNomeFantasia.Caption := 'Nome';

    lblRazaoSocial.Visible := False;
    edtRazaoSocial.Visible := False;
  end;

  dteDataCadastro.Enabled   := False;
  dteDataInativacao.Enabled := False;
  dteDataBloqueio.Enabled   := False;

  if vgOperacao = C then
    pnlDados.Enabled := False;
end;

procedure TFCadastroClienteFornecedor.edtCidadeExit(Sender: TObject);
begin
  inherited;

  if Trim(edtCidade.Text) <> '' then
    cdsCliFor.FieldByName('COD_MUNICIPIO_LOGR').AsInteger := StrToInt(edtCidade.Text);
end;

procedure TFCadastroClienteFornecedor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  if lPodeCancelar then
  begin
    qryCliFor.Cancel;

    vgOperacao     := VAZIO;
    vgIdConsulta   := 0;
    vgOrigemFiltro := False;
  end
  else
    Action := caNone;
end;

procedure TFCadastroClienteFornecedor.FormCreate(Sender: TObject);
begin
  inherited;

  IdNovoCliFor := 0;

  qryUF.Close;
  qryUF.Open;

  cdsCliFor.Close;
  cdsCliFor.Params.ParamByName('ID_CLIENTE_FORNECEDOR').Value := vgIdConsulta;
  cdsCliFor.Open;

  if vgOperacao = I then
    cdsCliFor.Append;

  if vgOperacao = E then
  begin
    qryCidade.Close;
    qryCidade.Params.ParamByName('sUF').Value := cdsCliFor.FieldByName('UF_LOGR').AsString;
    qryCidade.Open;

    cdsCliFor.Edit;
  end;
end;

procedure TFCadastroClienteFornecedor.FormShow(Sender: TObject);
begin
  inherited;

  if vgOperacao = I then
  begin
    dteDataCadastro.Date   := Date;
    rgTipoPessoa.ItemIndex := 0;
    ntbFisJur.Left := 0;
    ntbFisJur.PageIndex    := 0;
    rgFlgAtivo.ItemIndex   := 0;
    cbBloqueado.ItemIndex  := 1;

    cdsCliFor.FieldByName('SALDO_CREDOR').AsCurrency  := 0.00;
    cdsCliFor.FieldByName('SALDO_DEVEDOR').AsCurrency := 0.00;

    edtCidade.Enabled := False;
    lcbCidade.Enabled := False;
  end
  else
  begin
    if rgTipoPessoa.ItemIndex = 0 then  //Pessoa Fisica
    begin
      ntbFisJur.PageIndex := 0;
      ntbFisJur.Left := 0;

      lblNomeFantasia.Caption := 'Nome';

      lblRazaoSocial.Visible  := False;
      edtRazaoSocial.Visible  := False;

      medCPF.Text := cdsCliFor.FieldByName('CPF_CNPJ').AsString;
    end
    else  //Pessoa Juridica
    begin
      ntbFisJur.PageIndex := 1;
      ntbFisJur.Left := 304;

      lblNomeFantasia.Caption := 'Nome Fantasia';

      lblRazaoSocial.Visible  := True;
      edtRazaoSocial.Visible  := True;

      medCNPJ.Text := cdsCliFor.FieldByName('CPF_CNPJ').AsString;
    end;
  end;

  if edtNomeFantasia.CanFocus then
    edtNomeFantasia.SetFocus;
end;

function TFCadastroClienteFornecedor.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroClienteFornecedor.GravarDadosGenerico(
  var Msg: String): Boolean;
var
  sTipo: String;
begin
  Result := True;
  Msg := '';
  sTipo := '';

  if vgOperacao = C then
    Exit;

  sTipo := IfThen((TipoCliFor = CLIE), 'Cliente', 'Fornecedor');

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    if cdsCliFor.State in [dsInsert, dsEdit] then
    begin
      if vgOperacao = I then
      begin
        cdsCliFor.FieldByName('ID_CLIENTE_FORNECEDOR').AsInteger := BS.ProximoId('ID_CLIENTE_FORNECEDOR', 'CLIENTE_FORNECEDOR');

        if vgOrigemCadastro then
          IdNovoCliFor := cdsCliFor.FieldByName('ID_CLIENTE_FORNECEDOR').AsInteger;

        cdsCliFor.FieldByName('CAD_ID_USUARIO').AsInteger := vgUsu_Id;
      end;

      cdsCliFor.FieldByName('TIPO_CLIENTE_FORNECEDOR').AsString := IfThen((TipoCliFor = CLIE), 'C', 'F');

      if rgTipoPessoa.ItemIndex = 0 then  //Pessoa Fisica
      begin
        cdsCliFor.FieldByName('CPF_CNPJ').AsString         := Trim(medCPF.Text);
        cdsCliFor.FieldByName('RAZAO_SOCIAL').Value        := Trim(edtNomeFantasia.Text);
        cdsCliFor.FieldByName('INSCRICAO_ESTADUAL').Value  := Null;
        cdsCliFor.FieldByName('INSCRICAO_MUNICIPAL').Value := Null;
      end
      else  //Pessoa Juridica
      begin
        cdsCliFor.FieldByName('CPF_CNPJ').AsString     := Trim(medCNPJ.Text);
        cdsCliFor.FieldByName('DATA_NASCIMENTO').Value := Null;
      end;

      cdsCliFor.Post;
      cdsCliFor.ApplyUpdates(0);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    Msg := Trim(sTipo) + ' gravado com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na grava��o do ' + Trim(sTipo) + '.';
  end;
end;

procedure TFCadastroClienteFornecedor.GravarLog;
var
  sObservacao: String;
  sTipo: String;
begin
  inherited;

  sObservacao := '';

  sTipo := IfThen((TipoCliFor = CLIE), 'Cliente', 'Fornecedor');

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta de ' + sTipo,
                          vgIdConsulta, 'CLIENTE_FORNECEDOR');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclus�o de ' + sTipo,
                          vgIdConsulta, 'CLIENTE_FORNECEDOR');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO',
                                  'Por favor, indique o motivo da edi��o desse ' + sTipo + ':',
                                  '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de ' + sTipo;

      BS.GravarUsuarioLog(2, '', sObservacao,
                          vgIdConsulta, 'CLIENTE_FORNECEDOR');
    end;
  end;
end;

procedure TFCadastroClienteFornecedor.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroClienteFornecedor.lcbCidadeExit(Sender: TObject);
begin
  inherited;

  if lcbCidade.KeyValue > -1 then
    edtCidade.Text := qryCidade.FieldByName('COD_MUNICIPIO').AsString;
end;

procedure TFCadastroClienteFornecedor.lcbUFExit(Sender: TObject);
begin
  inherited;

  if Trim(lcbUF.Text) <> '' then
  begin
    edtCidade.Enabled := True;
    lcbCidade.Enabled := True;

    qryCidade.Close;
    qryCidade.Params.ParamByName('SUF').Value := Trim(lcbUF.Text);
    qryCidade.Open;

    if edtCidade.CanFocus then
      edtCidade.SetFocus;
  end
  else
  begin
    edtCidade.Enabled := False;
    lcbCidade.Enabled := False;
  end;
end;

procedure TFCadastroClienteFornecedor.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

procedure TFCadastroClienteFornecedor.mmObservacaoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroClienteFornecedor.rgFlgAtivoExit(Sender: TObject);
begin
  inherited;

  if rgFlgAtivo.ItemIndex = 1 then
    dteDataInativacao.Date := Date
  else
    dteDataInativacao.Clear;
end;

procedure TFCadastroClienteFornecedor.rgTipoPessoaClick(Sender: TObject);
begin
  inherited;

  if rgTipoPessoa.ItemIndex = 0 then  //Pessoa Fisica
  begin
    edtRazaoSocial.Clear;
    medCNPJ.Clear;
    edtInscricaoEstadual.Clear;
    edtInscricaoMunicipal.Clear;

    ntbFisJur.PageIndex := 0;
    ntbFisJur.Left := 0;

    lblNomeFantasia.Caption := 'Nome';

    lblRazaoSocial.Visible  := False;
    edtRazaoSocial.Visible  := False;

    if medCPF.CanFocus then
      medCPF.SetFocus;
  end
  else  //Pessoa Juridica
  begin
    medCPF.Clear;
    dteDataNascimento.Clear;

    ntbFisJur.PageIndex := 1;
    ntbFisJur.Left := 304;

    lblNomeFantasia.Caption := 'Nome Fantasia';

    lblRazaoSocial.Visible  := True;
    edtRazaoSocial.Visible  := True;

    if edtRazaoSocial.CanFocus then
      edtRazaoSocial.SetFocus;
  end;
end;

procedure TFCadastroClienteFornecedor.rgTipoPessoaExit(Sender: TObject);
begin
  inherited;

  if rgTipoPessoa.ItemIndex = 0 then  //Pessoa Fisica
  begin
    edtRazaoSocial.Clear;
    medCNPJ.Clear;
    edtInscricaoEstadual.Clear;
    edtInscricaoMunicipal.Clear;

    ntbFisJur.PageIndex := 0;
    ntbFisJur.Left := 0;

    lblNomeFantasia.Caption := 'Nome';

    lblRazaoSocial.Visible  := False;
    edtRazaoSocial.Visible  := False;

    if medCPF.CanFocus then
      medCPF.SetFocus;
  end
  else  //Pessoa Juridica
  begin
    medCPF.Clear;
    dteDataNascimento.Clear;

    ntbFisJur.PageIndex := 1;
    ntbFisJur.Left := 304;

    lblNomeFantasia.Caption := 'Nome Fantasia';

    lblRazaoSocial.Visible  := True;
    edtRazaoSocial.Visible  := True;

    if edtRazaoSocial.CanFocus then
      edtRazaoSocial.SetFocus;
  end;
end;

function TFCadastroClienteFornecedor.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
var
  sTipo, sCampo, sTel1, sTel2, sTel3: String;
begin
  Result   := True;
  QtdErros := 0;

  sTipo := IfThen((TipoCliFor = CLIE), 'CLIENTE', 'FORNECEDOR');

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if Trim(edtNomeFantasia.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;

    if rgTipoPessoa.ItemIndex = 0 then
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o NOME DO ' + sTipo + '.'
    else
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o NOME FANTASIA DO ' + sTipo + '.';

    DadosMsgErro.Componente := edtNomeFantasia;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if rgTipoPessoa.ItemIndex = -1 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar o TIPO PESSOA DO ' + sTipo + '.';
    DadosMsgErro.Componente := rgTipoPessoa;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if rgTipoPessoa.ItemIndex = 0 then  //Pessoa Fisica
  begin
    sCampo := StringReplace(StringReplace(medCPF.Text,
                                          '.',
                                          '',
                                          [rfReplaceAll, rfIgnoreCase]),
                            '-',
                            '',
                            [rfReplaceAll, rfIgnoreCase]);

    if Trim(sCampo) = '' then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o CPF DO ' + sTipo + '.';
      DadosMsgErro.Componente := medCPF;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;
  end
  else  //Pessoa Juridica
  begin
    sCampo := StringReplace(StringReplace(StringReplace(medCNPJ.Text,
                                                        '.',
                                                        '',
                                                        [rfReplaceAll, rfIgnoreCase]),
                                          '/',
                                          '',
                                          [rfReplaceAll, rfIgnoreCase]),
                            '-',
                            '',
                            [rfReplaceAll, rfIgnoreCase]);

    if Trim(sCampo) = '' then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o CNPJ DO ' + sTipo + '.';
      DadosMsgErro.Componente := medCNPJ;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;
  end;

  if Trim(edtLogradouro.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o LOGRADOURO DO ' + sTipo + '.';
    DadosMsgErro.Componente := edtLogradouro;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtNumero.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o N�MERO DO LOGRADOURO DO ' + sTipo + '.';
    DadosMsgErro.Componente := edtNumero;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtComplemento.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o COMPLEMENTO DO LOGRADOURO DO ' + sTipo + '.';
    DadosMsgErro.Componente := edtComplemento;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtBairro.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o BAIRRO DO ' + sTipo + '.';
    DadosMsgErro.Componente := edtBairro;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(lcbUF.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a UF DO ' + sTipo + '.';
    DadosMsgErro.Componente := lcbUF;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(lcbCidade.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a CIDADE DO ' + sTipo + '.';
    DadosMsgErro.Componente := lcbCidade;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  sTel1 := StringReplace(StringReplace(StringReplace(medTelefone1.Text,
                                                     '(',
                                                     '',
                                                     [rfReplaceAll, rfIgnoreCase]),
                                       ')',
                                       '',
                                       [rfReplaceAll, rfIgnoreCase]),
                         '-',
                         '',
                         [rfReplaceAll, rfIgnoreCase]);

  sTel2 := StringReplace(StringReplace(StringReplace(medTelefone2.Text,
                                                     '(',
                                                     '',
                                                     [rfReplaceAll, rfIgnoreCase]),
                                       ')',
                                       '',
                                       [rfReplaceAll, rfIgnoreCase]),
                         '-',
                         '',
                         [rfReplaceAll, rfIgnoreCase]);

  sTel3 := StringReplace(StringReplace(StringReplace(medTelefone3.Text,
                                                     '(',
                                                     '',
                                                     [rfReplaceAll, rfIgnoreCase]),
                                       ')',
                                       '',
                                       [rfReplaceAll, rfIgnoreCase]),
                         '-',
                         '',
                         [rfReplaceAll, rfIgnoreCase]);

  if (Trim(sTel1) = '') and
    (Trim(sTel2) = '') and
    (Trim(sTel3) = '') then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher um TELEFONE DO ' + sTipo + '.';
    DadosMsgErro.Componente := medTelefone1;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if rgFlgAtivo.ItemIndex = -1 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;

    if TipoCliFor = CLIE then
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar a SITUA��O DO ' + sTipo + '.'
    else
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar a SITUA��O DO ' + sTipo + '.';

    DadosMsgErro.Componente := rgFlgAtivo;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(cbBloqueado.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;

    if TipoCliFor = CLIE then
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar de o ' + sTipo + ' est� BLOQUEADO ou n�o.'
    else
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar de o ' + sTipo + ' est� BLOQUEADO ou n�o.';

    DadosMsgErro.Componente := cbBloqueado;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroClienteFornecedor.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

end.
