{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroUsuarioPermissao.pas
  Descricao:   tela de cadastro de Permissoes de Usuario
  Author   :   Cristina
  Date:        15-fev-2016
  Last Update: 23-out-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroUsuarioPermissao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  Vcl.CheckLst, Vcl.ExtCtrls, Vcl.StdCtrls, JvExControls, JvButton,
  JvTransparentButton, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.Provider, Datasnap.DBClient,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.DBCtrls, Vcl.ComCtrls;

type
  TFCadastroUsuarioPermissao = class(TFCadastroGeralPadrao)
    btnConfiguracoes: TJvTransparentButton;
    btnRelatorios: TJvTransparentButton;
    btnOficios: TJvTransparentButton;
    gbUsuario: TGroupBox;
    edtUsuario: TEdit;
    ntbUsuarioPermissao: TNotebook;
    chlbFinanceiro: TCheckListBox;
    chlbOficios: TCheckListBox;
    chlbRelatorios: TCheckListBox;
    chlbConfiguracoes: TCheckListBox;
    btnFolhas: TJvTransparentButton;
    btnEtiquetas: TJvTransparentButton;
    chlbEtiquetas: TCheckListBox;
    chlbFolhas: TCheckListBox;
    chbSelecionarTodas: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure btnDadosPrincipaisClick(Sender: TObject);
    procedure btnOficiosClick(Sender: TObject);
    procedure btnRelatoriosClick(Sender: TObject);
    procedure btnConfiguracoesClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure btnFolhasClick(Sender: TObject);
    procedure btnEtiquetasClick(Sender: TObject);
    procedure chbSelecionarTodasClick(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
    procedure AbrirListasCheckbox(CdsLts: TClientDataSet; ChLstB: TCheckListBox);
    procedure GravarPermissoesModulo(ChLstB: TCheckListBox; Indice: Integer);
    procedure MarcarDesmarcarTodas(CdsLts: TClientDataSet; ChLstB: TCheckListBox);
  public
    { Public declarations }
  end;

var
  FCadastroUsuarioPermissao: TFCadastroUsuarioPermissao;

implementation

{$R *.dfm}

uses UDM, UGDM, UBibliotecaSistema, UVariaveisGlobais, UDMUsuarioPermissao;

procedure TFCadastroUsuarioPermissao.AbrirListasCheckbox(CdsLts: TClientDataSet;
  ChLstB: TCheckListBox);
var
  j: Integer;
begin
  with CdsLts do
  begin
    Close;
    Params.ParamByName('ID_USUARIO').Value := vgIdConsulta;
    Open;

    First;
    j := 0;

    while not Eof do
    begin
      ChLstB.Items.AddObject(FieldByName('DESCR_PERMISSAO').AsString,
                             TObject(FieldByName('COD_PERMISSAO').AsInteger));

      j := ChLstB.Items.IndexOf(FieldByName('DESCR_PERMISSAO').AsString);

      if FieldByName('SELECIONADO').AsInteger = 1 then
        ChLstB.Checked[j] := True
      else
        ChLstB.Checked[j] := False;

      Next;
    end;
  end;
end;

procedure TFCadastroUsuarioPermissao.btnOficiosClick(Sender: TObject);
begin
  inherited;

  MarcarDesmarcarAbas(OFIC);
end;

procedure TFCadastroUsuarioPermissao.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroUsuarioPermissao.btnConfiguracoesClick(Sender: TObject);
begin
  inherited;

  MarcarDesmarcarAbas(CONFIG);
end;

procedure TFCadastroUsuarioPermissao.btnDadosPrincipaisClick(Sender: TObject);
begin
  inherited;

  MarcarDesmarcarAbas(FIN);
end;

procedure TFCadastroUsuarioPermissao.btnEtiquetasClick(Sender: TObject);
begin
  inherited;

  MarcarDesmarcarAbas(ETQ);
end;

procedure TFCadastroUsuarioPermissao.btnFolhasClick(Sender: TObject);
begin
  inherited;

  MarcarDesmarcarAbas(FLS);
end;

procedure TFCadastroUsuarioPermissao.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroUsuarioPermissao.btnRelatoriosClick(Sender: TObject);
begin
  inherited;

  MarcarDesmarcarAbas(RELAT);
end;

procedure TFCadastroUsuarioPermissao.chbSelecionarTodasClick(Sender: TObject);
begin
  inherited;

  case ntbUsuarioPermissao.PageIndex of
    0: MarcarDesmarcarTodas(dmUsuPrm.cdsFinOrigem, chlbFinanceiro);
    1: MarcarDesmarcarTodas(dmUsuPrm.cdsOficOrigem, chlbOficios);
    2: MarcarDesmarcarTodas(dmUsuPrm.cdsFlsOrigem, chlbFolhas);
    3: MarcarDesmarcarTodas(dmUsuPrm.cdsEtqOrigem, chlbEtiquetas);
    4: MarcarDesmarcarTodas(dmUsuPrm.cdsRelOrigem, chlbRelatorios);
    5: MarcarDesmarcarTodas(dmUsuPrm.cdsConfigOrigem, chlbConfiguracoes);
  end;
end;

procedure TFCadastroUsuarioPermissao.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtUsuario.MaxLength := 25;
end;

procedure TFCadastroUsuarioPermissao.DesabilitarComponentes;
begin
  inherited;

  edtUsuario.Enabled := False;

  if vgOperacao = C then
    ntbUsuarioPermissao.Enabled := False;
end;

procedure TFCadastroUsuarioPermissao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  if lPodeCancelar then
  begin
    FreeAndNil(dmUsuPrm);

    vgOperacao   := VAZIO;
    vgIdConsulta := 0;
  end;
end;

procedure TFCadastroUsuarioPermissao.FormCreate(Sender: TObject);
var
  QryNomeUsu: TFDQuery;
  j: Integer;
begin
  inherited;

  //Nome do usuario
  QryNomeUsu := dmGerencial.CriarFDQuery(nil, vgConGER);

  with QryNomeUsu, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT NOME FROM USUARIO WHERE ID_USUARIO = :ID_USUARIO';
    Params.ParamByName('ID_USUARIO').Value := vgIdConsulta;
    Open;

    edtUsuario.Text := QryNomeUsu.FieldByName('NOME').AsString;
  end;

  FreeAndNil(QryNomeUsu);

  //Listas das Abas com CheckListBox
  { FINANCEIRO }
  AbrirListasCheckbox(dmUsuPrm.cdsFinOrigem, chlbFinanceiro);

  { OFICIOS }
  AbrirListasCheckbox(dmUsuPrm.cdsOficOrigem, chlbOficios);

  { FOLHAS }
  AbrirListasCheckbox(dmUsuPrm.cdsFlsOrigem, chlbFolhas);

  { ETIQUETAS }
  AbrirListasCheckbox(dmUsuPrm.cdsEtqOrigem, chlbEtiquetas);

  { RELATORIOS }
  AbrirListasCheckbox(dmUsuPrm.cdsRelOrigem, chlbRelatorios);

  { CONFIGURACOES }
  AbrirListasCheckbox(dmUsuPrm.cdsConfigOrigem, chlbConfiguracoes);
end;

procedure TFCadastroUsuarioPermissao.FormShow(Sender: TObject);
begin
  inherited;

  MarcarDesmarcarAbas(FIN);
end;

function TFCadastroUsuarioPermissao.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroUsuarioPermissao.GravarDadosGenerico(
  var Msg: String): Boolean;
var
  j: integer;
begin
  inherited;

  Result := True;

  Msg := '';

  if vgOperacao = C then
    Exit;

  try
    j := 0;

    for j := 0 to (chlbFinanceiro.Items.Count - 1) do
      GravarPermissoesModulo(chlbFinanceiro, j);

    j := 0;

    for j := 0 to (chlbOficios.Items.Count - 1) do
      GravarPermissoesModulo(chlbOficios, j);

    j := 0;

    for j := 0 to (chlbFolhas.Items.Count - 1) do
      GravarPermissoesModulo(chlbFolhas, j);

    j := 0;

    for j := 0 to (chlbEtiquetas.Items.Count - 1) do
      GravarPermissoesModulo(chlbEtiquetas, j);

    j := 0;

    for j := 0 to (chlbRelatorios.Items.Count - 1) do
      GravarPermissoesModulo(chlbRelatorios, j);

    j := 0;

    for j := 0 to (chlbConfiguracoes.Items.Count - 1) do
      GravarPermissoesModulo(chlbConfiguracoes, j);

    Msg := 'Permiss�es do Usu�rio gravadas com sucesso!';
  except
    Result := False;
    Msg := 'Erro na grava��o das Permiss�es do Usu�rio.';
  end;
end;

procedure TFCadastroUsuarioPermissao.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    I, C:  //INCLUSAO /CONSULTA
    begin
      BS.GravarUsuarioLog(80, '', 'Somente consulta', vgIdConsulta, 'USUARIO_PERMISSAO');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da edi��o das permiss�es desse Usu�rio:', '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de Permiss�o de Usu�rio';

      BS.GravarUsuarioLog(80, '', sObservacao, vgIdConsulta, 'USUARIO_PERMISSAO');
    end;
  end;
end;

procedure TFCadastroUsuarioPermissao.GravarPermissoesModulo(
  ChLstB: TCheckListBox; Indice: Integer);
var
  QryUsuPrm: TFDQuery;
  lExiste: Boolean;
begin
  lExiste := False;

  QryUsuPrm := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    with QryUsuPrm, SQL do
    begin
      if vgOperacao = I then
      begin
        if ChLstB.Checked[Indice] then
        begin
          Close;
          Clear;

          Text := 'INSERT INTO USUARIO_PERMISSAO (ID_USUARIO_PERMISSAO, ' +
                  '                               ID_USUARIO, COD_PERMISSAO) ' +
                  '                       VALUES (:ID_USUARIO_PERMISSAO, ' +
                  '                               :ID_USUARIO, :COD_PERMISSAO)';

          Params.ParamByName('ID_USUARIO_PERMISSAO').Value := BS.ProximoId('ID_USUARIO_PERMISSAO', 'USUARIO_PERMISSAO');
          Params.ParamByName('ID_USUARIO').Value           := vgIdConsulta;
          Params.ParamByName('COD_PERMISSAO').Value        := dmGerencial.PegarNumero(ChLstB.Items[Indice]);

          ExecSQL;
        end;
      end;

      if vgOperacao = E then
      begin
        Close;
        Clear;

        Text := 'SELECT ID_USUARIO_PERMISSAO ' +
                '  FROM USUARIO_PERMISSAO ' +
                ' WHERE ID_USUARIO    = :ID_USUARIO ' +
                '   AND COD_PERMISSAO = :COD_PERMISSAO';

        Params.ParamByName('ID_USUARIO').Value   := vgIdConsulta;
        Params.ParamByName('COD_PERMISSAO').Value := dmGerencial.PegarNumero(ChLstB.Items[Indice]);

        Open;

        if RecordCount > 0 then
          lExiste := True;

        if ChLstB.Checked[Indice] then
        begin
          if not lExiste then
          begin
            Close;
            Clear;

            Text := 'INSERT INTO USUARIO_PERMISSAO (ID_USUARIO_PERMISSAO, ' +
                    '                               ID_USUARIO, COD_PERMISSAO) ' +
                    '                       VALUES (:ID_USUARIO_PERMISSAO, ' +
                    '                               :ID_USUARIO, :COD_PERMISSAO)';

            Params.ParamByName('ID_USUARIO_PERMISSAO').Value := BS.ProximoId('ID_USUARIO_PERMISSAO', 'USUARIO_PERMISSAO');
            Params.ParamByName('ID_USUARIO').Value           := vgIdConsulta;
            Params.ParamByName('COD_PERMISSAO').Value        := dmGerencial.PegarNumero(ChLstB.Items[Indice]);

            ExecSQL;
          end;
        end
        else
        begin
          if lExiste then
          begin
            Close;
            Clear;

            Text := 'DELETE FROM USUARIO_PERMISSAO ' +
                    ' WHERE ID_USUARIO    = :ID_USUARIO ' +
                    '   AND COD_PERMISSAO = :COD_PERMISSAO';

            Params.ParamByName('ID_USUARIO').Value    := vgIdConsulta;
            Params.ParamByName('COD_PERMISSAO').Value := dmGerencial.PegarNumero(ChLstB.Items[Indice]);

            ExecSQL;
          end;
        end;
      end;
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;
  end;

  FreeAndNil(QryUsuPrm);
end;

procedure TFCadastroUsuarioPermissao.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(ntbUsuarioPermissao);
end;

procedure TFCadastroUsuarioPermissao.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;

  case Aba of
    FIN:
    begin
      ntbUsuarioPermissao.PageIndex := 0;

      if chlbFinanceiro.CanFocus then
        chlbFinanceiro.SetFocus;
    end;
    OFIC:
    begin
      ntbUsuarioPermissao.PageIndex := 1;

      if chlbOficios.CanFocus then
        chlbOficios.SetFocus;
    end;
    FLS:
    begin
      ntbUsuarioPermissao.PageIndex := 2;

      if chlbFolhas.CanFocus then
        chlbFolhas.SetFocus;
    end;
    ETQ:
    begin
      ntbUsuarioPermissao.PageIndex := 3;

      if chlbEtiquetas.CanFocus then
        chlbEtiquetas.SetFocus;
    end;
    RELAT:
    begin
      ntbUsuarioPermissao.PageIndex := 4;

      if chlbRelatorios.CanFocus then
        chlbRelatorios.SetFocus;
    end;
    CONFIG:
    begin
      ntbUsuarioPermissao.PageIndex := 5;

      if chlbConfiguracoes.CanFocus then
        chlbConfiguracoes.SetFocus;
    end;
  end;

  btnDadosPrincipais.Transparent := not (Aba = FIN);
  btnOficios.Transparent         := not (Aba = OFIC);
  btnFolhas.Transparent          := not (Aba = FLS);
  btnEtiquetas.Transparent       := not (Aba = ETQ);
  btnRelatorios.Transparent      := not (Aba = RELAT);
  btnConfiguracoes.Transparent   := not (Aba = CONFIG);
end;

procedure TFCadastroUsuarioPermissao.MarcarDesmarcarTodas(CdsLts: TClientDataSet;
  ChLstB: TCheckListBox);
begin
  CdsLts.First;

  while not CdsLts.Eof do
  begin
    CdsLts.Edit;

    if chbSelecionarTodas.Checked then
    begin
      CdsLts.FieldByName('SELECIONADO').AsInteger := 1;
      ChLstB.Checked[ChLstB.Items.IndexOf(CdsLts.FieldByName('DESCR_PERMISSAO').AsString)] := True;
    end
    else
    begin
      CdsLts.FieldByName('SELECIONADO').AsInteger := 0;
      ChLstB.Checked[ChLstB.Items.IndexOf(CdsLts.FieldByName('DESCR_PERMISSAO').AsString)] := False;
    end;

    CdsLts.Post;

    CdsLts.Next;
  end;

  if chbSelecionarTodas.Checked then
    chbSelecionarTodas.Caption := 'Desmarcar Todas'
  else
    chbSelecionarTodas.Caption := 'Marcar Todas';
end;

function TFCadastroUsuarioPermissao.VerificarDadosGenerico(var QtdErros: Integer): Boolean;
var
  j: Integer;
  lComSelecao: Boolean;
begin
  Result      := True;
  QtdErros    := 0;
  lComSelecao := False;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  j := 0;

  for j := 0 to (chlbFinanceiro.Items.Count - 1) do
  begin
    if chlbFinanceiro.Checked[j] then
    begin
      lComSelecao := True;
      Break;
    end;
  end;

  if not lComSelecao then
  begin
    j := 0;

    for j := 0 to (chlbOficios.Items.Count - 1) do
    begin
      if chlbOficios.Checked[j] then
      begin
        lComSelecao := True;
        Break;
      end;
    end;
  end;

  if not lComSelecao then
  begin
    j := 0;

    for j := 0 to (chlbFolhas.Items.Count - 1) do
    begin
      if chlbFolhas.Checked[j] then
      begin
        lComSelecao := True;
        Break;
      end;
    end;
  end;

  if not lComSelecao then
  begin
    j := 0;

    for j := 0 to (chlbEtiquetas.Items.Count - 1) do
    begin
      if chlbEtiquetas.Checked[j] then
      begin
        lComSelecao := True;
        Break;
      end;
    end;
  end;

  if not lComSelecao then
  begin
    j := 0;

    for j := 0 to (chlbRelatorios.Items.Count - 1) do
    begin
      if chlbRelatorios.Checked[j] then
      begin
        lComSelecao := True;
        Break;
      end;
    end;
  end;

  if not lComSelecao then
  begin
    j := 0;

    for j := 0 to (chlbConfiguracoes.Items.Count - 1) do
    begin
      if chlbConfiguracoes.Checked[j] then
      begin
        lComSelecao := True;
        Break;
      end;
    end;
  end;

  if not lComSelecao then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Nenhuma permiss�o foi dada ao usu�rio.';
    DadosMsgErro.Componente := edtUsuario;
    DadosMsgErro.PageIndex  := 1;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);

    if vgOperacao = I then
      Result := False
    else
      Result := True;
  end
  else
    Result := True;
end;

function TFCadastroUsuarioPermissao.VerificarLayoutAto(var QtdErros: Integer): Boolean;
begin
  //
end;

end.
