{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   ULancamentosVincularDepositos.pas
  Descricao:   Formulario de Vinculo de Lancamentos e Depositos
  Author   :   Cristina
  Date:        06-dez-2017
  Last Update: 31-jan-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit ULancamentosVincularDepositos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Data.DB, Vcl.Grids,
  Vcl.DBGrids, JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls,
  JvBaseEdits, Vcl.Mask, JvExMask, JvToolEdit, Vcl.DBCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, System.DateUtils, System.StrUtils, JvExDBGrids, JvDBGrid,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, dxSkinMetropolis, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, cxCheckBox,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, JvDBUltimGrid;

type
  TFLancamentosVincularDepositos = class(TForm)
    pnlRepasse: TPanel;
    gbLancamentos: TGroupBox;
    gbDepositos: TGroupBox;
    lblPeriodoL: TLabel;
    dtePeriodoIniL: TJvDateEdit;
    Label2: TLabel;
    dtePeriodoFimL: TJvDateEdit;
    cedValorLancamento: TJvCalcEdit;
    lblValorLancamento: TLabel;
    edtNumReciboL: TEdit;
    lblNumReciboL: TLabel;
    rgSituacaoL: TRadioGroup;
    btnFiltrarLancamentos: TJvTransparentButton;
    lblPeriodoD: TLabel;
    dtePeriodoIniD: TJvDateEdit;
    Label3: TLabel;
    dtePeriodoFimD: TJvDateEdit;
    cedValorDeposito: TJvCalcEdit;
    lblValorDeposito: TLabel;
    btnFiltrarDepositos: TJvTransparentButton;
    lblObservacoes: TLabel;
    btnConfirmarVinculo: TJvTransparentButton;
    btnCancelarVinculo: TJvTransparentButton;
    lblDataVinculo: TLabel;
    lblDescricaoVinculo: TLabel;
    lblValorTotalLancamento: TLabel;
    lblValorTotalDespesa: TLabel;
    edtDescricaoVinculo: TEdit;
    dteDataVinculo: TJvDateEdit;
    cedValorTotalLancamentos: TJvCalcEdit;
    cedValorTotalDepositos: TJvCalcEdit;
    dsLanc: TDataSource;
    chbSelecionarTodosL: TCheckBox;
    mmObservacoes: TMemo;
    spLegendaSemVinculoL: TShape;
    lblLegendaSemVinculoL: TLabel;
    spLegendaVinculadoL: TShape;
    lblLegendaVinculadoL: TLabel;
    dsDepositos: TDataSource;
    lblNumCodDeposito: TLabel;
    edtNumCodDeposito: TEdit;
    rgSituacaoD: TRadioGroup;
    chbSelecionarTodosD: TCheckBox;
    Shape1: TShape;
    Label1: TLabel;
    Shape2: TShape;
    Label4: TLabel;
    btnEditarLancamento: TJvTransparentButton;
    btnDesfazerVinculo: TJvTransparentButton;
    dbgLancamentos: TJvDBUltimGrid;
    dbgDepositos: TJvDBUltimGrid;
    btnVisualizarVincL: TJvTransparentButton;
    btnVisualizarVincD: TJvTransparentButton;
    procedure FormShow(Sender: TObject);
    procedure btnFiltrarLancamentosClick(Sender: TObject);
    procedure btnFiltrarDepositosClick(Sender: TObject);
    procedure cedValorLancamentoKeyPress(Sender: TObject; var Key: Char);
    procedure cedValorDepositoKeyPress(Sender: TObject; var Key: Char);
    procedure chbSelecionarTodosLClick(Sender: TObject);
    procedure dteDataVinculoExit(Sender: TObject);
    procedure dteDataVinculoKeyPress(Sender: TObject; var Key: Char);
    procedure dtePeriodoFimLKeyPress(Sender: TObject; var Key: Char);
    procedure dtePeriodoFimDKeyPress(Sender: TObject; var Key: Char);
    procedure dtePeriodoIniLKeyPress(Sender: TObject; var Key: Char);
    procedure dtePeriodoIniDKeyPress(Sender: TObject; var Key: Char);
    procedure edtNumReciboLKeyPress(Sender: TObject; var Key: Char);
    procedure mmObservacoesKeyPress(Sender: TObject; var Key: Char);
    procedure rgSituacaoLClick(Sender: TObject);
    procedure rgSituacaoLExit(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btnConfirmarVinculoClick(Sender: TObject);
    procedure btnCancelarVinculoClick(Sender: TObject);
    procedure edtNumCodDepositoKeyPress(Sender: TObject; var Key: Char);
    procedure rgSituacaoDClick(Sender: TObject);
    procedure rgSituacaoDExit(Sender: TObject);
    procedure chbSelecionarTodosDClick(Sender: TObject);
    procedure btnEditarLancamentoClick(Sender: TObject);
    procedure btnDesfazerVinculoClick(Sender: TObject);
    procedure dbgLancamentosCellClick(Column: TColumn);
    procedure dbgLancamentosTitleClick(Column: TColumn);
    procedure dbgLancamentosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgDepositosCellClick(Column: TColumn);
    procedure dbgDepositosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgDepositosTitleClick(Column: TColumn);
    procedure btnVisualizarVincLClick(Sender: TObject);
    procedure btnVisualizarVincDClick(Sender: TObject);
  private
    { Private declarations }

    FOriginalOptionsD: TDBGridOptions;

    procedure MarcarDesmarcarTodas(Tipo: String);
    procedure SalvarBoleano(Tipo: String);
  public
    { Public declarations }
  end;

var
  FLancamentosVincularDepositos: TFLancamentosVincularDepositos;

implementation

{$R *.dfm}

uses UVariaveisGlobais, UBibliotecaSistema, UDM, UGDM, UDMBaixa,
  UOpcoesAcaoVinculoDeposito, UDMLancamento, UCadastroLancamento;

procedure TFLancamentosVincularDepositos.btnCancelarVinculoClick(Sender: TObject);
begin
  btnFiltrarLancamentos.Click;
  btnFiltrarDepositos.Click;

  chbSelecionarTodosL.Checked := False;
  chbSelecionarTodosD.Checked := False;

  cedValorTotalLancamentos.Value := 0;
  cedValorTotalDepositos.Value   := 0;

  dteDataVinculo.Date := vgDataLancVigente;

  edtDescricaoVinculo.Clear;
  mmObservacoes.Clear;
end;

procedure TFLancamentosVincularDepositos.btnConfirmarVinculoClick(Sender: TObject);
var
  lGravarVinculos: Boolean;
  QryVinc, QryL, QryLAux: TFDQuery;
  iPos, iCont, IdCons: Integer;
  sLstLanc, sLstDepo, sLstDepoVlr, sMsg: String;
  Op: TOperacao;
  OrigF, OrigC: Boolean;
  cTotalLancPago, cTotalParcPago: Currency;
begin
  inherited;

  if (dteDataVinculo.Date = 0) or
    (dteDataVinculo.Date = Null) or
    (Trim(dmGerencial.PegarNumeroTexto(dteDataVinculo.Text)) = '') then
  begin
    if Application.MessageBox('Por favor, informe a Data de V�nculo.',
                              'Aviso',
                              MB_OK + MB_ICONWARNING) = ID_NO then
      Abort;
  end
  else if cedValorTotalLancamentos.Value <= 0 then
  begin
    if Application.MessageBox('N�o h� Lan�amentos selecionados.',
                              'Aviso',
                              MB_OK + MB_ICONWARNING) = ID_NO then
      Abort;
  end
  else if cedValorTotalDepositos.Value <= 0 then
  begin
    if Application.MessageBox('N�o h� Dep�sitos selecionados.',
                              'Aviso',
                              MB_OK + MB_ICONWARNING) = ID_NO then
      Abort;
  end;

  lGravarVinculos := False;

  iCont := 0;

  sLstLanc    := 'LAN�AMENTOS' + #13#10;
  sLstDepoVlr := #13#10 + 'DEP�SITOS' + #13#10;
  sLstDepo    := '';

  sMsg := '';

  dmBaixa.dDataVinculo := dteDataVinculo.Date;

  QryVinc := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
  QryL    := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
  QryLAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  dmBaixa.sTipoBaixa := '';  //'' = Nenhuma / S = Sobra de Caixa / F = Falta / P = Nova Parcela

  //CONTAGEM DE LANCAMENTOS
  iPos := dbgLancamentos.DataSource.DataSet.RecNo;

  dmBaixa.cdsLanc.DisableControls;

  dmBaixa.cValorTotalLancamentos := 0;

  dmBaixa.cdsLanc.First;

  while not dmBaixa.cdsLanc.Eof do
  begin

    if dmBaixa.cdsLanc.FieldByName('SELECIONADO').AsBoolean then
    begin
      sLstLanc := sLstLanc + #13#10 + dmBaixa.cdsLanc.FieldByName('NUM_RECIBO').AsString +
                  ' (Valor: ' + FloatToStrF(dmBaixa.cdsLanc.FieldByName('VR_PARCELA_PREV').AsFloat, ffCurrency, 10, 2) + ')';

      dmBaixa.cValorTotalLancamentos := (dmBaixa.cValorTotalLancamentos + dmBaixa.cdsLanc.FieldByName('VR_PARCELA_PREV').AsCurrency);
    end;

    dmBaixa.cdsLanc.Next;
  end;

  dmBaixa.cdsLanc.EnableControls;

  dbgLancamentos.DataSource.DataSet.RecNo := iPos;

  //CONTAGEM DE DEPOSITOS
  iPos := dbgDepositos.DataSource.DataSet.RecNo;

  dmBaixa.cdsDepos.DisableControls;

  dmBaixa.cValorTotalDepositos := 0;

  dmBaixa.cdsDepos.First;

  while not dmBaixa.cdsDepos.Eof do
  begin

    if dmBaixa.cdsDepos.FieldByName('SELECIONADO').AsBoolean and
      (Trim(dmBaixa.cdsDepos.FieldByName('FLG_STATUS').AsString) = 'F') then
    begin
      sLstDepoVlr := sLstDepoVlr + #13#10 + dmBaixa.cdsDepos.FieldByName('NUM_COD_DEPOSITO').AsString +
                                            ' (Valor: ' + FloatToStrF(dmBaixa.cdsDepos.FieldByName('VR_DEPOSITO_FLUTUANTE').AsFloat, ffCurrency, 10, 2) + ')';

      if Trim(sLstDepo) = '' then
        sLstDepo := dmBaixa.cdsDepos.FieldByName('NUM_COD_DEPOSITO').AsString
      else
        sLstDepo := sLstDepo + ', ' + dmBaixa.cdsDepos.FieldByName('NUM_COD_DEPOSITO').AsString;

      Inc(iCont);

      dmBaixa.cValorTotalDepositos := (dmBaixa.cValorTotalDepositos + dmBaixa.cdsDepos.FieldByName('VR_DEPOSITO_FLUTUANTE').AsCurrency);
    end;

    dmBaixa.cdsDepos.Next;
  end;

  dmBaixa.cdsDepos.EnableControls;

  dbgDepositos.DataSource.DataSet.RecNo := iPos;

  sMsg := 'Confirma a sele��o dos Lan�amentos e Dep�sitos abaixo? ' + #13#10 + #13#10 + sLstLanc + #13#10 + sLstDepoVlr;

  if Application.MessageBox(PChar(sMsg), 'Aviso', MB_YESNO + MB_ICONQUESTION) = ID_YES then
  begin
    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      if dmBaixa.cValorTotalLancamentos = dmBaixa.cValorTotalDepositos then  //OK
        lGravarVinculos := True
      else
      begin
        if dmBaixa.cValorTotalLancamentos < dmBaixa.cValorTotalDepositos then  //Sobra Deposito
        begin
          Application.MessageBox('O valor total dos Dep�sitos est� MAIOR que o Valor das Parcelas de Lan�amento. Isso configura uma SOBRA de Valor em Caixa.',
                                 'Aviso',
                                 MB_OK);

          dmBaixa.cDifTot := (dmBaixa.cValorTotalDepositos - dmBaixa.cValorTotalLancamentos);
          dmBaixa.sTipoBaixa := 'S';
        end
        else if dmBaixa.cValorTotalLancamentos > dmBaixa.cValorTotalDepositos then  //Falta Deposito
        begin
          Application.MessageBox('O valor total dos Dep�sitos est� MENOR que o Valor das Parcelas de Lan�amento. Isso configura uma FALTA de Valor em Caixa.',
                                 'Aviso',
                                 MB_OK);

          dmBaixa.cDifTot := (dmBaixa.cValorTotalLancamentos - dmBaixa.cValorTotalDepositos);
          dmBaixa.sTipoBaixa := 'F';
        end;

        //Solicitar ao Usuario a forma como ele deseja tratar a diferenca de valor
        Op     := vgOperacao;
        IdCons := vgIdConsulta;
        OrigF  := vgOrigemFiltro;
        OrigC  := vgOrigemCadastro;

        vgOperacao       := I;
        vgIdConsulta     := 0;
        vgOrigemFiltro   := False;
        vgOrigemCadastro := True;

//        dmBaixa.dDataVinculo := dteDataVinculo.Date;

        try
          Application.CreateForm(TFOpcoesAcaoVinculoDeposito, FOpcoesAcaoVinculoDeposito);
          FOpcoesAcaoVinculoDeposito.sLstDepo := sLstDepo;
          FOpcoesAcaoVinculoDeposito.ShowModal;
        finally
          lGravarVinculos := FOpcoesAcaoVinculoDeposito.lGravarVinculos;
          FOpcoesAcaoVinculoDeposito.Free;
        end;

        vgOperacao       := Op;
        vgIdConsulta     := IdCons;
        vgOrigemFiltro   := OrigF;
        vgOrigemCadastro := OrigC;
      end;

      if lGravarVinculos then
      begin
        dmBaixa.cdsLanc.First;

        while not dmBaixa.cdsLanc.Eof do
        begin
          if dmBaixa.cdsLanc.FieldByName('SELECIONADO').AsBoolean then
          begin
            dmBaixa.cdsDepos.First;

            while not dmBaixa.cdsDepos.Eof do
            begin
              if dmBaixa.cdsDepos.FieldByName('SELECIONADO').AsBoolean and
               (Trim(dmBaixa.cdsDepos.FieldByName('FLG_STATUS').AsString) = 'F') then
              begin
                //VINCULO_LANCDEPO
                QryVinc.Close;
                QryVinc.SQL.Clear;

                QryVinc.SQL.Text := 'INSERT INTO VINCULO_LANCDEPO (ID_VINCULO_LANCDEPO, ' +
                                    '                              DATA_VINCULO_LANCDEPO, ' +
                                    '                              DESCR_VINCULO_LANCDEPO, ' +
                                    '                              COD_LANCAMENTO_FK, ' +
                                    '                              ANO_LANCAMENTO_FK, ' +
                                    '                              ID_LANCAMENTO_PARC_FK, ' +
                                    '                              ID_DEPOSITO_FLUTUANTE_FK, ' +
                                    '                              OBS_VINCULO_LANCDEPO) ' +
                                    '                      VALUES ((NEXT VALUE FOR GEN_VINCULO_LANCDEPO_ID), ' +
                                    '                              :DATA_VINCULO_LANCDEPO, ' +
                                    '                              :DESCR_VINCULO_LANCDEPO, ' +
                                    '                              :COD_LANCAMENTO, ' +
                                    '                              :ANO_LANCAMENTO, ' +
                                    '                              :ID_LANCAMENTO_PARC, ' +
                                    '                              :ID_DEPOSITO_FLUTUANTE, ' +
                                    '                              :OBS_VINCULO_LANCDEPO)';

                QryVinc.Params.ParamByName('DATA_VINCULO_LANCDEPO').Value  := dmBaixa.dDataVinculo;  //dteDataVinculo.Date;
                QryVinc.Params.ParamByName('DESCR_VINCULO_LANCDEPO').Value := Trim(edtDescricaoVinculo.Text);
                QryVinc.Params.ParamByName('COD_LANCAMENTO').Value         := dmBaixa.cdsLanc.FieldByName('COD_LANCAMENTO').AsInteger;
                QryVinc.Params.ParamByName('ANO_LANCAMENTO').Value         := dmBaixa.cdsLanc.FieldByName('ANO_LANCAMENTO').AsInteger;
                QryVinc.Params.ParamByName('ID_LANCAMENTO_PARC').Value     := dmBaixa.cdsLanc.FieldByName('ID_LANCAMENTO_PARC').AsInteger;
                QryVinc.Params.ParamByName('ID_DEPOSITO_FLUTUANTE').Value  := dmBaixa.cdsDepos.FieldByName('ID_DEPOSITO_FLUTUANTE').AsInteger;
                QryVinc.Params.ParamByName('OBS_VINCULO_LANCDEPO').Value   := Trim(mmObservacoes.Text);

                QryVinc.ExecSQL;

                //DEPOSITO_FLUTUANTE
                QryVinc.Close;
                QryVinc.SQL.Clear;

                QryVinc.SQL.Text := 'UPDATE DEPOSITO_FLUTUANTE ' +
                                    '   SET BAIXA_ID_USUARIO = :BAIXA_ID_USUARIO, ' +
                                    '       DATA_BAIXADO     = :DATA_BAIXADO, ' +
                                    '       FLG_STATUS       = :FLG_STATUS ' +
                                    ' WHERE ID_DEPOSITO_FLUTUANTE = :ID_DEPOSITO_FLUTUANTE';

                QryVinc.Params.ParamByName('ID_DEPOSITO_FLUTUANTE').Value := dmBaixa.cdsDepos.FieldByName('ID_DEPOSITO_FLUTUANTE').AsInteger;
                QryVinc.Params.ParamByName('BAIXA_ID_USUARIO').Value      := vgUsu_Id;
                QryVinc.Params.ParamByName('DATA_BAIXADO').Value          := dmBaixa.dDataVinculo;  //dteDataVinculo.Date;
                QryVinc.Params.ParamByName('FLG_STATUS').Value            := 'B';

                QryVinc.ExecSQL;
              end;

              dmBaixa.cdsDepos.Next;
            end;

            QryVinc.Close;
            QryVinc.SQL.Clear;

            QryVinc.SQL.Text := 'UPDATE LANCAMENTO_PARC ' +
                                '   SET NUM_COD_DEPOSITO = :NUM_COD_DEPOSITO, ' +
                                '       FLG_STATUS       = :FLG_STATUS, ' +
                                '       PAG_ID_USUARIO   = :PAG_ID_USUARIO, ' +
                                '       DATA_PAGAMENTO   = :DATA_PAGAMENTO ' +
                                ' WHERE ID_LANCAMENTO_PARC = :ID_LANCAMENTO_PARC';

            QryVinc.Params.ParamByName('ID_LANCAMENTO_PARC').Value := dmBaixa.cdsLanc.FieldByName('ID_LANCAMENTO_PARC').AsInteger;
            QryVinc.Params.ParamByName('NUM_COD_DEPOSITO').Value   := sLstDepo;
            QryVinc.Params.ParamByName('FLG_STATUS').Value         := 'G';
            QryVinc.Params.ParamByName('PAG_ID_USUARIO').Value     := vgUsu_Id;
            QryVinc.Params.ParamByName('DATA_PAGAMENTO').Value     := dmBaixa.dDataVinculo;  //dteDataVinculo.Date;

            QryVinc.ExecSQL;
          end;

          dmBaixa.cdsLanc.Next;
        end;

        if iCont = 1 then
          Application.MessageBox('V�nculo realizado com sucesso!',
                                 'Sucesso!',
                                 MB_OK)
        else if iCont > 1 then
          Application.MessageBox('V�nculos realizados com sucesso!',
                                 'Sucesso!',
                                 MB_OK);

        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Commit;

        try
          if vgConSISTEMA.Connected then
            vgConSISTEMA.StartTransaction;

          dmBaixa.cdsLanc.First;

          while not dmBaixa.cdsLanc.Eof do
          begin
            if dmBaixa.cdsLanc.FieldByName('SELECIONADO').AsBoolean then
            begin
              //ACERTAR VALOR TOTAL PAGO DA PARCELA
              QryL.Close;
              QryL.SQL.Clear;

              QryL.SQL.Text := 'UPDATE LANCAMENTO_PARC ' +
                               '   SET VR_PARCELA_PAGO = ((VR_PARCELA_PREV + VR_PARCELA_JUROS) - VR_PARCELA_DESCONTO) ' +
                               ' WHERE ID_LANCAMENTO_PARC = :ID_LANCAMENTO_PARC';

              QryL.Params.ParamByName('ID_LANCAMENTO_PARC').Value := dmBaixa.cdsLanc.FieldByName('ID_LANCAMENTO_PARC').AsInteger;
              QryL.ExecSQL;

              //VERIFICAR QUITACAO DE LANCAMENTO
              cTotalLancPago := 0;
              cTotalParcPago := 0;

              //Lancamento
              QryLAux.Close;
              QryLAux.SQL.Clear;

              QryLAux.SQL.Text := 'SELECT ((VR_TOTAL_PREV + VR_TOTAL_JUROS) - VR_TOTAL_DESCONTO) AS TOTAL_PAGO_LANC '+
                                  '  FROM LANCAMENTO ' +
                                  ' WHERE COD_LANCAMENTO = :COD_LANCAMENTO ' +
                                  '   AND ANO_LANCAMENTO = :ANO_LANCAMENTO';

              QryLAux.Params.ParamByName('COD_LANCAMENTO').Value := dmBaixa.cdsLanc.FieldByName('COD_LANCAMENTO').AsInteger;
              QryLAux.Params.ParamByName('ANO_LANCAMENTO').Value := dmBaixa.cdsLanc.FieldByName('ANO_LANCAMENTO').AsInteger;
              QryLAux.Open;

              cTotalLancPago := QryLAux.FieldByName('TOTAL_PAGO_LANC').AsCurrency;

              //Parcelas
              QryLAux.Close;
              QryLAux.SQL.Clear;

              QryLAux.SQL.Text := 'SELECT SUM(VR_PARCELA_PAGO) AS TOTAL_PAGO_PARC '+
                                  '  FROM LANCAMENTO_PARC ' +
                                  ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                                  '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ' +
                                  '   AND FLG_STATUS = ' + QuotedStr('G');

              QryLAux.Params.ParamByName('COD_LANCAMENTO').Value := dmBaixa.cdsLanc.FieldByName('COD_LANCAMENTO').AsInteger;
              QryLAux.Params.ParamByName('ANO_LANCAMENTO').Value := dmBaixa.cdsLanc.FieldByName('ANO_LANCAMENTO').AsInteger;
              QryLAux.Open;

              cTotalParcPago := QryLAux.FieldByName('TOTAL_PAGO_PARC').AsCurrency;

              if cTotalLancPago = cTotalParcPago then
              begin
                QryL.Close;
                QryL.SQL.Clear;

                QryL.SQL.Text := 'UPDATE LANCAMENTO ' +
                                 '   SET VR_TOTAL_PAGO = :VR_TOTAL_PAGO, ' +
                                 '       FLG_STATUS = ' + QuotedStr('G') +
                                 ' WHERE COD_LANCAMENTO = :COD_LANCAMENTO ' +
                                 '   AND ANO_LANCAMENTO = :ANO_LANCAMENTO';

                QryL.Params.ParamByName('VR_TOTAL_PAGO').Value  := cTotalParcPago;
                QryL.Params.ParamByName('COD_LANCAMENTO').Value := dmBaixa.cdsLanc.FieldByName('COD_LANCAMENTO').AsInteger;
                QryL.Params.ParamByName('ANO_LANCAMENTO').Value := dmBaixa.cdsLanc.FieldByName('ANO_LANCAMENTO').AsInteger;
                QryL.ExecSQL;
              end;
            end;

            dmBaixa.cdsLanc.Next;
          end;

          if vgConSISTEMA.InTransaction then
            vgConSISTEMA.Commit;
        except
          if vgConSISTEMA.InTransaction then
            vgConSISTEMA.Rollback;
        end;

        btnCancelarVinculo.Click;
      end
      else
      begin
        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Rollback;

        if iCont = 1 then
          Application.MessageBox('V�nculo n�o realizado',
                                 'Erro',
                                 MB_OK + MB_ICONERROR)
        else if iCont > 1 then
          Application.MessageBox('V�nculos n�o realizados',
                                 'Erro',
                                 MB_OK + MB_ICONERROR);
      end;

      FreeAndNil(QryVinc);
      FreeAndNil(QryL);
      FreeAndNil(QryLAux);
    except
      on E: Exception do
      begin
        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Rollback;

        if iCont = 1 then
          Application.MessageBox('Erro ao realizar o V�nculo.',
                                 'Erro',
                                 MB_OK + MB_ICONERROR)
        else if iCont > 1 then
          Application.MessageBox('Erro ao realizar os V�nculos.',
                                 'Erro',
                                 MB_OK + MB_ICONERROR);

        FreeAndNil(QryVinc);
        FreeAndNil(QryL);
        FreeAndNil(QryLAux);
      end;
    end;
  end;
end;

procedure TFLancamentosVincularDepositos.btnDesfazerVinculoClick(
  Sender: TObject);
var
  QryDesv, QryDesvAux1, QryDesvAux2: TFDQuery;
  sTexto, sDepositos: String;
  lDesvincOutroDepo, lZerarParcela, lDesvinculou: Boolean;
  iPos: Integer;
begin
  lDesvinculou := False;

  iPos := dmBaixa.cdsDepos.RecNo;

  dmBaixa.cdsDepos.DisableControls;

  dmBaixa.cdsDepos.First;

  while not dmBaixa.cdsDepos.Eof do
  begin
    if dmBaixa.cdsDepos.FieldByName('SELECIONADO').AsBoolean then
    begin
      if Trim(dmBaixa.cdsDepos.FieldByName('FLG_STATUS').AsString) <> 'B' then
        Application.MessageBox('O Dep�sito n�o possui V�nculos.',
                               'Aviso',
                               MB_OK + MB_ICONWARNING)
      else if (not dmBaixa.cdsDepos.FieldByName('DATA_BAIXADO').IsNull) and
       (dmBaixa.cdsDepos.FieldByName('DATA_BAIXADO').AsDateTime <= vgDataUltFech) then
        Application.MessageBox(PChar('O Caixa do dia ' +
                                     FormatDateTime('DD/MM/YYYY', dmBaixa.cdsDepos.FieldByName('DATA_BAIXADO').AsDateTime) +
                                     ' j� se encontra Fechado.'),
                               'Aviso',
                               MB_OK + MB_ICONWARNING)
      else
      begin
        if Application.MessageBox(PChar('Deseja desfazer Vinculo(s) do Dep�sito ' + dmBaixa.cdsDepos.FieldByName('NUM_COD_DEPOSITO').AsString +
                                      ' (' + FloatToStrF(dmBaixa.cdsDepos.FieldByName('VR_DEPOSITO_FLUTUANTE').AsFloat, ffCurrency, 10, 2) + ')?'),
                                'Desfazer V�nculo(s) de Dep�sito',
                                MB_YESNO) = ID_YES then
        begin
          try
            if vgConSISTEMA.Connected then
              vgConSISTEMA.StartTransaction;

            QryDesv     := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
            QryDesvAux1 := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);
            QryDesvAux2 := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

            QryDesvAux1.Close;
            QryDesvAux1.SQL.Clear;
            QryDesvAux1.SQL.Text := 'SELECT V.*, ' +
                                    '       LP.NUM_PARCELA, ' +
                                    '       LP.NUM_COD_DEPOSITO, ' +
                                    '       LP.VR_PARCELA_PAGO, ' +
                                    '       L.VR_TOTAL_PAGO, ' +
                                    '       L.NUM_RECIBO ' +
                                    '  FROM VINCULO_LANCDEPO V ' +
                                    ' INNER JOIN LANCAMENTO_PARC LP ' +
                                    '    ON V.ID_LANCAMENTO_PARC_FK = LP.ID_LANCAMENTO_PARC ' +
                                    ' INNER JOIN LANCAMENTO L ' +
                                    '    ON LP.COD_LANCAMENTO_FK = L.COD_LANCAMENTO ' +
                                    '   AND LP.ANO_LANCAMENTO_FK = L.ANO_LANCAMENTO ' +
                                    ' WHERE V.CANCEL_ID_USUARIO IS NULL ' +
                                    '   AND V.ID_DEPOSITO_FLUTUANTE_FK = :ID_DEPOSITO_FLUTUANTE';
            QryDesvAux1.Params.ParamByName('ID_DEPOSITO_FLUTUANTE').Value := dmBaixa.cdsDepos.FieldByName('ID_DEPOSITO_FLUTUANTE').AsInteger;
            QryDesvAux1.Open;

            if QryDesvAux1.RecordCount > 0 then
            begin
              QryDesvAux1.First;

              while not QryDesvAux1.Eof do
              begin
                lDesvincOutroDepo := False;
                lZerarParcela     := True;

                //Outros Depositos Vinculados a Parcela
                QryDesvAux2.Close;
                QryDesvAux2.SQL.Clear;
                QryDesvAux2.SQL.Text := 'SELECT D.ID_DEPOSITO_FLUTUANTE, ' +
                                        '       D.DATA_DEPOSITO, ' +
                                        '       D.NUM_COD_DEPOSITO, ' +
                                        '       D.DESCR_DEPOSITO_FLUTUANTE, ' +
                                        '       D.VR_DEPOSITO_FLUTUANTE ' +
                                        '  FROM DEPOSITO_FLUTUANTE D ' +
                                        ' INNER JOIN VINCULO_LANCDEPO V ' +
                                        '    ON D.ID_DEPOSITO_FLUTUANTE = V.ID_DEPOSITO_FLUTUANTE_FK ' +
                                        ' WHERE V.DATA_CANCELAMENTO IS NULL ' +
                                        '   AND V.ID_LANCAMENTO_PARC_FK = :ID_LANCAMENTO_PARC ' +
                                        '   AND V.ID_DEPOSITO_FLUTUANTE_FK <> :ID_DEPOSITO_FLUTUANTE';
                QryDesvAux2.Params.ParamByName('ID_LANCAMENTO_PARC').Value    := QryDesvAux1.FieldByName('ID_LANCAMENTO_PARC_FK').AsInteger;
                QryDesvAux2.Params.ParamByName('ID_DEPOSITO_FLUTUANTE').Value := QryDesvAux1.FieldByName('ID_DEPOSITO_FLUTUANTE_FK').AsInteger;
                QryDesvAux2.Open;

                if QryDesvAux2.RecordCount > 0 then
                begin
                  sDepositos := '';

                  QryDesvAux2.First;

                  while not QryDesvAux2.Eof do
                  begin
                    if Trim(sDepositos) = '' then
                      sDepositos := '* Dt. Dep�sito: ' + FormatDateTime('DD/MM/YYYY', QryDesvAux2.FieldByName('DATA_DEPOSITO').AsDateTime) + ' | ' +
                                    'ID: ' + QryDesvAux2.FieldByName('NUM_COD_DEPOSITO').AsString + ' | ' +
                                    'Descr.: ' + QryDesvAux2.FieldByName('DESCR_DEPOSITO_FLUTUANTE').AsString + ' | ' +
                                    'Valor: ' + FloatToStrF(QryDesvAux2.FieldByName('VR_DEPOSITO_FLUTUANTE').AsFloat, ffCurrency, 10, 2)
                    else
                      sDepositos := sDepositos + #13#10 +
                                    '* Dt. Dep�sito: ' + FormatDateTime('DD/MM/YYYY', QryDesvAux2.FieldByName('DATA_DEPOSITO').AsDateTime) + ' | ' +
                                    'ID: ' + QryDesvAux2.FieldByName('NUM_COD_DEPOSITO').AsString + ' | ' +
                                    'Descr.: ' + QryDesvAux2.FieldByName('DESCR_DEPOSITO_FLUTUANTE').AsString + ' | ' +
                                    'Valor: ' + FloatToStrF(QryDesvAux2.FieldByName('VR_DEPOSITO_FLUTUANTE').AsFloat, ffCurrency, 10, 2);

                    QryDesvAux2.Next;
                  end;

                  if Application.MessageBox(PChar('A parcela N� ' + QryDesvAux1.FieldByName('NUM_PARCELA').AsString +
                                                  ' (Recibo: ' + QryDesvAux1.FieldByName('NUM_RECIBO').AsString + ' | ' +
                                                  'Valor: ' + FloatToStrF(QryDesvAux1.FieldByName('VR_PARCELA_PAGO').AsFloat, ffCurrency, 10, 2) + ')' +
                                                  ' tamb�m possui V�nculo ao(s) seguinte(s) Dep�sito(s):' + #13#10 + #13#10 +
                                                  sDepositos + #13#10 + #13#10 +
                                                  'Deseja cancelar tamb�m esse(s) V�nculo(s)?'),
                                            'Desfazer V�nculo(s) a Outro(s) Dep�sito(s)',
                                            MB_YESNO) = ID_YES then
                    lDesvincOutroDepo := True
                  else
                  begin
                    if Application.MessageBox('Zerar o ValorPago dessa Parcela assim mesmo?',
                                              'Zerar Parcela',
                                              MB_YESNO) = ID_NO then
                      lZerarParcela := False;
                  end;
                end;

                //Outros Depositos
                if lDesvincOutroDepo then
                begin
                  QryDesvAux2.First;

                  while not QryDesvAux2.Eof do
                  begin
                    QryDesv.Close;
                    QryDesv.SQL.Clear;
                    QryDesv.SQL.Text := 'UPDATE DEPOSITO_FLUTUANTE ' +
                                        '   SET FLG_STATUS       = :FLG_STATUS, ' +
                                        '       DATA_BAIXADO     = :DATA_BAIXADO, ' +
                                        '       BAIXA_ID_USUARIO = :BAIXA_ID_USUARIO ' +
                                        ' WHERE ID_DEPOSITO_FLUTUANTE = :ID_DEPOSITO_FLUTUANTE';
                    QryDesv.Params.ParamByName('FLG_STATUS').Value            := 'F';
                    QryDesv.Params.ParamByName('DATA_BAIXADO').Value          := Null;
                    QryDesv.Params.ParamByName('BAIXA_ID_USUARIO').Value      := Null;
                    QryDesv.Params.ParamByName('ID_DEPOSITO_FLUTUANTE').Value := QryDesvAux2.FieldByName('ID_DEPOSITO_FLUTUANTE').AsInteger;
                    QryDesv.ExecSQL;

                    QryDesv.Close;
                    QryDesv.SQL.Clear;
                    QryDesv.SQL.Text := 'UPDATE VINCULO_LANCDEPO ' +
                                        '   SET CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO, ' +
                                        '       DATA_CANCELAMENTO = :DATA_CANCELAMENTO ' +
                                        ' WHERE ID_DEPOSITO_FLUTUANTE_FK = :ID_DEPOSITO_FLUTUANTE';
                    QryDesv.Params.ParamByName('CANCEL_ID_USUARIO').Value     := vgUsu_Id;
                    QryDesv.Params.ParamByName('DATA_CANCELAMENTO').Value     := Date;
                    QryDesv.Params.ParamByName('ID_DEPOSITO_FLUTUANTE').Value := QryDesvAux2.FieldByName('ID_DEPOSITO_FLUTUANTE').AsInteger;
                    QryDesv.ExecSQL;

                    QryDesvAux2.Next;
                  end;
                end;

                if lZerarParcela then
                begin
                  //Parcela de Lancamento
                  sTexto := '';

                  sTexto := Trim(ReplaceText(QryDesvAux1.FieldByName('NUM_COD_DEPOSITO').AsString,
                                             dmBaixa.cdsDepos.FieldByName('NUM_COD_DEPOSITO').AsString,
                                             ''));

                  if Trim(sTexto) <> '' then
                    sTexto := Trim(ReplaceStr(sTexto, ', ,', ', '));

                  QryDesv.Close;
                  QryDesv.SQL.Clear;
                  QryDesv.SQL.Text := 'UPDATE LANCAMENTO_PARC ' +
                                      '   SET NUM_COD_DEPOSITO = :NUM_COD_DEPOSITO, ' +
                                      '       FLG_STATUS       = :FLG_STATUS, ' +
                                      '       VR_PARCELA_PAGO  = :VR_PARCELA_PAGO, ' +
                                      '       DATA_PAGAMENTO   = :DATA_PAGAMENTO, ' +
                                      '       PAG_ID_USUARIO   = :PAG_ID_USUARIO ' +
                                      ' WHERE ID_LANCAMENTO_PARC = :ID_LANCAMENTO_PARC ' +
                                      '   AND FLG_STATUS <> ' + QuotedStr('C');
                  QryDesv.Params.ParamByName('NUM_COD_DEPOSITO').Value   := sTexto;
                  QryDesv.Params.ParamByName('ID_LANCAMENTO_PARC').Value := QryDesvAux1.FieldByName('ID_LANCAMENTO_PARC_FK').AsInteger;
                  QryDesv.Params.ParamByName('FLG_STATUS').Value         := 'P';
                  QryDesv.Params.ParamByName('VR_PARCELA_PAGO').Value    := 0.00;
                  QryDesv.Params.ParamByName('DATA_PAGAMENTO').Value     := Null;
                  QryDesv.Params.ParamByName('PAG_ID_USUARIO').Value     := Null;
                  QryDesv.ExecSQL;

                  //Lancamento
                  QryDesv.Close;
                  QryDesv.SQL.Clear;
                  QryDesv.SQL.Text := 'UPDATE LANCAMENTO ' +
                                      '   SET FLG_STATUS    = :FLG_STATUS, ' +
                                      '       VR_TOTAL_PAGO = :VR_TOTAL_PAGO ' +
                                      ' WHERE COD_LANCAMENTO = :COD_LANCAMENTO ' +
                                      '   AND ANO_LANCAMENTO = :ANO_LANCAMENTO ' +
                                      '   AND FLG_STATUS <> ' + QuotedStr('C');
                  QryDesv.Params.ParamByName('COD_LANCAMENTO').Value := QryDesvAux1.FieldByName('COD_LANCAMENTO_FK').AsInteger;
                  QryDesv.Params.ParamByName('ANO_LANCAMENTO').Value := QryDesvAux1.FieldByName('ANO_LANCAMENTO_FK').AsInteger;
                  QryDesv.Params.ParamByName('FLG_STATUS').Value     := 'P';
                  QryDesv.Params.ParamByName('VR_TOTAL_PAGO').Value  := (QryDesvAux1.FieldByName('VR_TOTAL_PAGO').AsCurrency -
                                                                         QryDesvAux1.FieldByName('VR_PARCELA_PAGO').AsCurrency);
                  QryDesv.ExecSQL;
                end;

                QryDesvAux1.Next;
              end;

              QryDesv.Close;
              QryDesv.SQL.Clear;
              QryDesv.SQL.Text := 'UPDATE DEPOSITO_FLUTUANTE ' +
                                  '   SET FLG_STATUS       = :FLG_STATUS, ' +
                                  '       DATA_BAIXADO     = :DATA_BAIXADO, ' +
                                  '       BAIXA_ID_USUARIO = :BAIXA_ID_USUARIO ' +
                                  ' WHERE ID_DEPOSITO_FLUTUANTE = :ID_DEPOSITO_FLUTUANTE';
              QryDesv.Params.ParamByName('FLG_STATUS').Value            := 'F';
              QryDesv.Params.ParamByName('DATA_BAIXADO').Value          := Null;
              QryDesv.Params.ParamByName('BAIXA_ID_USUARIO').Value      := Null;
              QryDesv.Params.ParamByName('ID_DEPOSITO_FLUTUANTE').Value := dmBaixa.cdsDepos.FieldByName('ID_DEPOSITO_FLUTUANTE').AsInteger;
              QryDesv.ExecSQL;

              QryDesv.Close;
              QryDesv.SQL.Clear;
              QryDesv.SQL.Text := 'UPDATE VINCULO_LANCDEPO ' +
                                  '   SET CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO, ' +
                                  '       DATA_CANCELAMENTO = :DATA_CANCELAMENTO ' +
                                  ' WHERE ID_DEPOSITO_FLUTUANTE_FK = :ID_DEPOSITO_FLUTUANTE';
              QryDesv.Params.ParamByName('CANCEL_ID_USUARIO').Value     := vgUsu_Id;
              QryDesv.Params.ParamByName('DATA_CANCELAMENTO').Value     := Date;
              QryDesv.Params.ParamByName('ID_DEPOSITO_FLUTUANTE').Value := dmBaixa.cdsDepos.FieldByName('ID_DEPOSITO_FLUTUANTE').AsInteger;
              QryDesv.ExecSQL;
            end;

            if vgConSISTEMA.InTransaction then
              vgConSISTEMA.Commit;

            lDesvinculou := True;

            Application.MessageBox('Dep�sito desvinculado com sucesso!',
                                   'Sucesso!',
                                   MB_OK);

            FreeAndNil(QryDesv);
            FreeAndNil(QryDesvAux1);
            FreeAndNil(QryDesvAux2);
          except
            if vgConSISTEMA.InTransaction then
              vgConSISTEMA.Rollback;

            Application.MessageBox('Erro ao desvincular Dep�sito.',
                                   'Erro',
                                   MB_OK + MB_ICONERROR);

            FreeAndNil(QryDesv);
            FreeAndNil(QryDesvAux1);
            FreeAndNil(QryDesvAux2);
          end;
        end;
      end;
    end;

    dmBaixa.cdsDepos.Next;
  end;

  dmBaixa.cdsDepos.EnableControls;

  if lDesvinculou then
  begin
    btnFiltrarDepositos.Click;
    btnFiltrarLancamentos.Click;
  end
  else
    dmBaixa.cdsDepos.RecNo := iPos;
end;

procedure TFLancamentosVincularDepositos.btnFiltrarDepositosClick(Sender: TObject);
var
  QryAux: TFDQuery;
  sVinculos: String;
begin
  QryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

  dmBaixa.cdsDepos.Close;
  dmBaixa.qryDepos.Close;

  dmBaixa.qryDepos.SQL.Clear;
  dmBaixa.qryDepos.SQL.Text := 'SELECT * ' +
                               '  FROM DEPOSITO_FLUTUANTE ' +
                               ' WHERE DATA_DEPOSITO BETWEEN :DATA_INI AND :DATA_FIM ';

  //Valor
  if cedValorDeposito.Value > 0 then
    dmBaixa.qryDepos.SQL.Add(' AND VR_DEPOSITO_FLUTUANTE = ' + ReplaceStr(cedValorDeposito.EditText, ',', '.'));

  //Numero de Identificacao do Deposito
  if Trim(edtNumCodDeposito.Text) <> '' then
    dmBaixa.qryDepos.SQL.Add(' AND NUM_COD_DEPOSITO LIKE ' + QuotedStr('%' + Trim(UpperCase(edtNumCodDeposito.Text)) + '%'));

  //Situacao
  case rgSituacaoD.ItemIndex of
    0: //Todos
      dmBaixa.qryDepos.SQL.Add(' AND (FLG_STATUS = ' + QuotedStr('F') +
                               '  OR  FLG_STATUS = ' + QuotedStr('B') + ')');
    1: //Sem Vinculo
      dmBaixa.qryDepos.SQL.Add(' AND FLG_STATUS = ' + QuotedStr('F'));
    2: //Vinculados
      dmBaixa.qryDepos.SQL.Add(' AND FLG_STATUS = ' + QuotedStr('B'));
  end;

  dmBaixa.qryDepos.SQL.Add(' ORDER BY DATA_DEPOSITO DESC');

  dmBaixa.cdsDepos.Params.ParamByName('DATA_INI').Value := dtePeriodoIniD.Date;
  dmBaixa.cdsDepos.Params.ParamByName('DATA_FIM').Value := dtePeriodoFimD.Date;
  dmBaixa.cdsDepos.Open;

  dmBaixa.cdsDepos.DisableControls;

  dmBaixa.cdsDepos.First;

  while not dmBaixa.cdsDepos.Eof do
  begin
    //Vinculos
    if Trim(dmBaixa.cdsDepos.FieldByName('FLG_STATUS').AsString) = 'B' then
    begin
      QryAux.Close;
      QryAux.SQL.Clear;

      QryAux.SQL.Text := 'SELECT L.NUM_RECIBO, ' +
                         '       LP.VR_PARCELA_PREV ' +
                         '  FROM LANCAMENTO L ' +
                         ' INNER JOIN LANCAMENTO_PARC LP' +
                         '    ON L.COD_LANCAMENTO = LP.COD_LANCAMENTO_FK ' +
                         '   AND L.ANO_LANCAMENTO = LP.ANO_LANCAMENTO_FK ' +
                         ' INNER JOIN VINCULO_LANCDEPO V ' +
                         '    ON LP.ID_LANCAMENTO_PARC = V.ID_LANCAMENTO_PARC_FK ' +
                         ' WHERE V.DATA_CANCELAMENTO IS NULL ' +
                         '   AND V.ID_DEPOSITO_FLUTUANTE_FK = :ID_DEPOSITO_FLUTUANTE';

      QryAux.Params.ParamByName('ID_DEPOSITO_FLUTUANTE').Value := dmBaixa.cdsDepos.FieldByName('ID_DEPOSITO_FLUTUANTE').AsInteger;
      QryAux.Open;

      if QryAux.RecordCount > 0 then
      begin
        sVinculos := '';

        QryAux.First;

        while not QryAux.Eof do
        begin
          if Trim(sVinculos) = '' then
            sVinculos := '* Recibo: ' + QryAux.FieldByName('NUM_RECIBO').AsString + ' | ' +
                         'Valor: ' + FloatToStrF(QryAux.FieldByName('VR_PARCELA_PREV').AsFloat, ffCurrency, 10, 2)
          else
            sVinculos := sVinculos + #13#10 +
                         '* Recibo: ' + QryAux.FieldByName('NUM_RECIBO').AsString + ' | ' +
                         'Valor: ' + FloatToStrF(QryAux.FieldByName('VR_PARCELA_PREV').AsFloat, ffCurrency, 10, 2);

          QryAux.Next;
        end;
      end;
    end;

    dmBaixa.cdsDepos.Edit;
    dmBaixa.cdsDepos.FieldByName('VINCULOS').AsString := IfThen(Trim(sVinculos) = '',
                                                                '',
                                                                Trim(sVinculos));
    dmBaixa.cdsDepos.Post;

    dmBaixa.cdsDepos.Next;
  end;

  dmBaixa.cdsDepos.EnableControls;

  MarcarDesmarcarTodas('D');

  dmBaixa.cdsDepos.First;

  FreeAndNil(QryAux);
end;

procedure TFLancamentosVincularDepositos.btnFiltrarLancamentosClick(Sender: TObject);
var
  QryAux: TFDQuery;
  sVinculado, sDescricao, sVinculos: String;
begin
  QryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

  dmBaixa.cdsLanc.Close;
  dmBaixa.qryLanc.Close;

  dmBaixa.qryLanc.SQL.Clear;
  dmBaixa.qryLanc.SQL.Text := 'SELECT DISTINCT L.*, ' +
                              '       N.DESCR_NATUREZA, ' +
                              '       LP.*, ' +
                              '       LP.FLG_STATUS AS FLG_STATUS_PARC ' +
                              '  FROM LANCAMENTO L ' +
                              ' INNER JOIN NATUREZA N ' +
                              '    ON L.ID_NATUREZA_FK = N.ID_NATUREZA ' +
                              ' INNER JOIN LANCAMENTO_PARC LP ' +
                              '    ON L.COD_LANCAMENTO = LP.COD_LANCAMENTO_FK ' +
                              '   AND L.ANO_LANCAMENTO = LP.ANO_LANCAMENTO_FK ' +
                              ' WHERE L.TIPO_LANCAMENTO = ' + QuotedStr('R') +
                              '   AND L.FLG_CANCELADO = ' + QuotedStr('N') +
                              '   AND LP.FLG_STATUS <> ' + QuotedStr('C') +
                              '   AND LP.ID_FORMAPAGAMENTO_FK = 5 ' +
                              '   AND LP.DATA_LANCAMENTO_PARC BETWEEN :DATA_INI AND :DATA_FIM ';

  //Valor
  if cedValorLancamento.Value > 0 then
    dmBaixa.qryLanc.SQL.Add(' AND LP.VR_PARCELA_PREV = ' + ReplaceStr(cedValorLancamento.EditText, ',', '.'));

  //Recibo
  if Trim(edtNumReciboL.Text) <> '' then
    dmBaixa.qryLanc.SQL.Add(' AND L.NUM_RECIBO = ' + Trim(edtNumReciboL.Text));

  //Situacao
  case rgSituacaoL.ItemIndex of
    1: //Sem Vinculo
      dmBaixa.qryLanc.SQL.Add(' AND NOT EXISTS(SELECT V.ID_DEPOSITO_FLUTUANTE_FK ' +
                              '                  FROM VINCULO_LANCDEPO V ' +
                              '                 WHERE V.ID_LANCAMENTO_PARC_FK = LP.ID_LANCAMENTO_PARC ' +
                              '                   AND V.CANCEL_ID_USUARIO IS NULL)');
    2: //Vinculados
      dmBaixa.qryLanc.SQL.Add(' AND EXISTS(SELECT V.ID_DEPOSITO_FLUTUANTE_FK ' +
                              '              FROM VINCULO_LANCDEPO V ' +
                              '             WHERE V.ID_LANCAMENTO_PARC_FK = LP.ID_LANCAMENTO_PARC ' +
                              '               AND V.CANCEL_ID_USUARIO IS NULL)');
  end;

  dmBaixa.qryLanc.SQL.Add(' ORDER BY LP.DATA_LANCAMENTO_PARC DESC, ' +
                          '          L.COD_LANCAMENTO DESC, ' +
                          '          L.ANO_LANCAMENTO DESC');

  dmBaixa.cdsLanc.Params.ParamByName('DATA_INI').Value := dtePeriodoIniL.Date;
  dmBaixa.cdsLanc.Params.ParamByName('DATA_FIM').Value := dtePeriodoFimL.Date;
  dmBaixa.cdsLanc.Open;

  if dmBaixa.cdsLanc.RecordCount > 0 then
    btnEditarLancamento.Visible := True;

  dmBaixa.cdsLanc.DisableControls;

  dmBaixa.cdsLanc.First;

  while not dmBaixa.cdsLanc.Eof do
  begin
    //Vinculado
    QryAux.Close;
    QryAux.SQL.Clear;

    QryAux.SQL.Text := 'SELECT ID_VINCULO_LANCDEPO ' +
                       '  FROM VINCULO_LANCDEPO ' +
                       ' WHERE ID_LANCAMENTO_PARC_FK = :ID_LANCAMENTO_PARC ' +
                       '   AND CANCEL_ID_USUARIO IS NULL';

    QryAux.Params.ParamByName('ID_LANCAMENTO_PARC').Value := dmBaixa.cdsLanc.FieldByName('ID_LANCAMENTO_PARC').AsInteger;
    QryAux.Open;

    sVinculado := IfThen(QryAux.RecordCount > 0 , 'S', 'N');

    //Descricao
    if Trim(dmBaixa.cdsLanc.FieldByName('DESCR_NATUREZA').AsString) = 'SOBRA DE CAIXA' then
      sDescricao := dmBaixa.cdsLanc.FieldByName('DESCR_NATUREZA').AsString
    else
    begin
      QryAux.Close;
      QryAux.SQL.Clear;

      QryAux.SQL.Text := 'SELECT UPPER(I.DESCR_ITEM) AS DESCR_ITEM ' +
                         '  FROM LANCAMENTO_DET LD ' +
                         ' INNER JOIN ITEM I ' +
                         '    ON LD.ID_ITEM_FK = I.ID_ITEM ' +
                         ' WHERE LD.COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                         '   AND LD.ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ' +
                         '   AND LD.FLG_CANCELADO = ' + QuotedStr('N');

      QryAux.Params.ParamByName('COD_LANCAMENTO').Value := dmBaixa.cdsLanc.FieldByName('COD_LANCAMENTO').AsInteger;
      QryAux.Params.ParamByName('ANO_LANCAMENTO').Value := dmBaixa.cdsLanc.FieldByName('ANO_LANCAMENTO').AsInteger;
      QryAux.Open;

      if QryAux.RecordCount > 0 then
      begin
        sDescricao := '';

        QryAux.First;

        while not QryAux.Eof do
        begin
          if Trim(sDescricao) = '' then
            sDescricao := QryAux.FieldByName('DESCR_ITEM').AsString
          else
            sDescricao := sDescricao + ' | ' + QryAux.FieldByName('DESCR_ITEM').AsString;

          QryAux.Next;
        end;
      end;
    end;

    //Vinculos
    if Trim(sVinculado) = 'S' then
    begin
      QryAux.Close;
      QryAux.SQL.Clear;

      QryAux.SQL.Text := 'SELECT D.DATA_DEPOSITO, ' +
                         '       D.NUM_COD_DEPOSITO, ' +
                         '       D.DESCR_DEPOSITO_FLUTUANTE, ' +
                         '       D.VR_DEPOSITO_FLUTUANTE ' +
                         '  FROM DEPOSITO_FLUTUANTE D ' +
                         ' INNER JOIN VINCULO_LANCDEPO V ' +
                         '    ON D.ID_DEPOSITO_FLUTUANTE = V.ID_DEPOSITO_FLUTUANTE_FK ' +
                         ' WHERE V.DATA_CANCELAMENTO IS NULL ' +
                         '   AND V.ID_LANCAMENTO_PARC_FK = :ID_LANCAMENTO_PARC';

      QryAux.Params.ParamByName('ID_LANCAMENTO_PARC').Value := dmBaixa.cdsLanc.FieldByName('ID_LANCAMENTO_PARC').AsInteger;
      QryAux.Open;

      if QryAux.RecordCount > 0 then
      begin
        sVinculos := '';

        QryAux.First;

        while not QryAux.Eof do
        begin
          if Trim(sVinculos) = '' then
            sVinculos := '* Dt. Dep�sito: ' + FormatDateTime('DD/MM/YYYY', QryAux.FieldByName('DATA_DEPOSITO').AsDateTime) + ' | ' +
                         'ID: ' + QryAux.FieldByName('NUM_COD_DEPOSITO').AsString + ' | ' +
                         'Descr.: ' + QryAux.FieldByName('DESCR_DEPOSITO_FLUTUANTE').AsString + ' | ' +
                         'Valor: ' + FloatToStrF(QryAux.FieldByName('VR_DEPOSITO_FLUTUANTE').AsFloat, ffCurrency, 10, 2)
          else
            sVinculos := sVinculos + #13#10 +
                         '* Dt. Dep�sito: ' + FormatDateTime('DD/MM/YYYY', QryAux.FieldByName('DATA_DEPOSITO').AsDateTime) + ' | ' +
                         'ID: ' + QryAux.FieldByName('NUM_COD_DEPOSITO').AsString + ' | ' +
                         'Descr.: ' + QryAux.FieldByName('DESCR_DEPOSITO_FLUTUANTE').AsString + ' | ' +
                         'Valor: ' + FloatToStrF(QryAux.FieldByName('VR_DEPOSITO_FLUTUANTE').AsFloat, ffCurrency, 10, 2);

          QryAux.Next;
        end;
      end;
    end;

    dmBaixa.cdsLanc.Edit;
    dmBaixa.cdsLanc.FieldByName('VINCULADO').AsString := sVinculado;
    dmBaixa.cdsLanc.FieldByName('DESCRICAO').AsString := IfThen(Trim(sDescricao) = '',
                                                                'INDEFINIDO',
                                                                Trim(sDescricao));
    dmBaixa.cdsLanc.FieldByName('VINCULOS').AsString  := IfThen(Trim(sVinculos) = '',
                                                                '',
                                                                Trim(sVinculos));
    dmBaixa.cdsLanc.Post;

    dmBaixa.cdsLanc.Next;
  end;

  dmBaixa.cdsLanc.EnableControls;

  MarcarDesmarcarTodas('L');

  dmBaixa.cdsLanc.First;

  FreeAndNil(QryAux);
end;

procedure TFLancamentosVincularDepositos.btnVisualizarVincDClick(
  Sender: TObject);
begin
  if dmBaixa.cdsDepos.RecordCount > 0 then
  begin
    if Trim(dmBaixa.cdsDepos.FieldByName('VINCULOS').AsString) <> '' then
      Application.MessageBox(PChar(dmBaixa.cdsDepos.FieldByName('VINCULOS').AsString),
                             'Lan�amentos vinculados ao Dep�sito',
                             MB_OK + MB_ICONINFORMATION);
  end;
end;

procedure TFLancamentosVincularDepositos.btnVisualizarVincLClick(
  Sender: TObject);
begin
  if dmBaixa.cdsLanc.RecordCount > 0 then
  begin
    if Trim(dmBaixa.cdsLanc.FieldByName('VINCULOS').AsString) <> '' then
      Application.MessageBox(PChar(dmBaixa.cdsLanc.FieldByName('VINCULOS').AsString),
                             'Dep�sitos vinculados ao Lan�amento',
                             MB_OK + MB_ICONINFORMATION);
  end;
end;

procedure TFLancamentosVincularDepositos.cedValorDepositoKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then // TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    btnFiltrarDepositos.Click;
    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFLancamentosVincularDepositos.cedValorLancamentoKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then // TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    btnFiltrarLancamentos.Click;
    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFLancamentosVincularDepositos.chbSelecionarTodosDClick(
  Sender: TObject);
begin
  MarcarDesmarcarTodas('D');
end;

procedure TFLancamentosVincularDepositos.chbSelecionarTodosLClick(Sender: TObject);
begin
  MarcarDesmarcarTodas('L');
end;

procedure TFLancamentosVincularDepositos.dteDataVinculoExit(Sender: TObject);
begin
  if dteDataVinculo.Date < vgDataLancVigente then
    dteDataVinculo.Date := vgDataLancVigente;
end;

procedure TFLancamentosVincularDepositos.dteDataVinculoKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dteDataVinculo.Date < vgDataLancVigente then
      dteDataVinculo.Date := vgDataLancVigente;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFLancamentosVincularDepositos.dtePeriodoFimDKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then // TAB
  begin
    if dtePeriodoFimD.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso',
        MB_OK + MB_ICONWARNING);

      if dtePeriodoFimD.CanFocus then
        dtePeriodoFimD.SetFocus;
    end
    else if dtePeriodoIniD.Date > dtePeriodoFimD.Date then
    begin
      Application.MessageBox
        ('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso',
        MB_OK + MB_ICONWARNING);

      dtePeriodoFimD.Clear;

      if dtePeriodoFimD.CanFocus then
        dtePeriodoFimD.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    if dtePeriodoFimD.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso',
        MB_OK + MB_ICONWARNING);

      if dtePeriodoFimD.CanFocus then
        dtePeriodoFimD.SetFocus;
    end
    else if dtePeriodoIniD.Date > dtePeriodoFimD.Date then
    begin
      Application.MessageBox
        ('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso',
        MB_OK + MB_ICONWARNING);

      dtePeriodoFimD.Clear;

      if dtePeriodoFimD.CanFocus then
        dtePeriodoFimD.SetFocus;
    end
    else
      btnFiltrarDepositos.Click;

    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFLancamentosVincularDepositos.dtePeriodoFimLKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then // TAB
  begin
    if dtePeriodoFimL.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso',
        MB_OK + MB_ICONWARNING);

      if dtePeriodoFimL.CanFocus then
        dtePeriodoFimL.SetFocus;
    end
    else if dtePeriodoIniL.Date > dtePeriodoFimL.Date then
    begin
      Application.MessageBox
        ('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso',
        MB_OK + MB_ICONWARNING);

      dtePeriodoFimL.Clear;

      if dtePeriodoFimL.CanFocus then
        dtePeriodoFimL.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    if dtePeriodoFimL.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso',
        MB_OK + MB_ICONWARNING);

      if dtePeriodoFimL.CanFocus then
        dtePeriodoFimL.SetFocus;
    end
    else if dtePeriodoIniL.Date > dtePeriodoFimL.Date then
    begin
      Application.MessageBox
        ('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso',
        MB_OK + MB_ICONWARNING);

      dtePeriodoFimL.Clear;

      if dtePeriodoFimL.CanFocus then
        dtePeriodoFimL.SetFocus;
    end
    else
      btnFiltrarLancamentos.Click;

    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFLancamentosVincularDepositos.dtePeriodoIniDKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then // TAB
  begin
    if dtePeriodoIniD.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso',
        MB_OK + MB_ICONWARNING);

      if dtePeriodoIniD.CanFocus then
        dtePeriodoIniD.SetFocus;
    end
    else if dtePeriodoIniD.Date > dtePeriodoFimD.Date then
    begin
      Application.MessageBox
        ('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso',
        MB_OK + MB_ICONWARNING);

      dtePeriodoIniD.Clear;

      if dtePeriodoIniD.CanFocus then
        dtePeriodoIniD.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    if dtePeriodoIniD.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso',
        MB_OK + MB_ICONWARNING);

      if dtePeriodoIniD.CanFocus then
        dtePeriodoIniD.SetFocus;
    end
    else if dtePeriodoIniD.Date > dtePeriodoFimD.Date then
    begin
      Application.MessageBox
        ('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso',
        MB_OK + MB_ICONWARNING);

      dtePeriodoIniD.Clear;

      if dtePeriodoIniD.CanFocus then
        dtePeriodoIniD.SetFocus;
    end
    else
      btnFiltrarDepositos.Click;

    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFLancamentosVincularDepositos.dtePeriodoIniLKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then // TAB
  begin
    if dtePeriodoIniL.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso',
        MB_OK + MB_ICONWARNING);

      if dtePeriodoIniL.CanFocus then
        dtePeriodoIniL.SetFocus;
    end
    else if dtePeriodoIniL.Date > dtePeriodoFimL.Date then
    begin
      Application.MessageBox
        ('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso',
        MB_OK + MB_ICONWARNING);

      dtePeriodoIniL.Clear;

      if dtePeriodoIniL.CanFocus then
        dtePeriodoIniL.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    if dtePeriodoIniL.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso',
        MB_OK + MB_ICONWARNING);

      if dtePeriodoIniL.CanFocus then
        dtePeriodoIniL.SetFocus;
    end
    else if dtePeriodoIniL.Date > dtePeriodoFimL.Date then
    begin
      Application.MessageBox
        ('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso',
        MB_OK + MB_ICONWARNING);

      dtePeriodoIniL.Clear;

      if dtePeriodoIniL.CanFocus then
        dtePeriodoIniL.SetFocus;
    end
    else
      btnFiltrarLancamentos.Click;

    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFLancamentosVincularDepositos.edtNumCodDepositoKeyPress(
  Sender: TObject; var Key: Char);
begin
  if Key = #9 then // TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    btnFiltrarDepositos.Click;
    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFLancamentosVincularDepositos.edtNumReciboLKeyPress(
  Sender: TObject; var Key: Char);
begin
  if Key = #9 then // TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    btnFiltrarLancamentos.Click;
    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFLancamentosVincularDepositos.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFLancamentosVincularDepositos.FormShow(Sender: TObject);
begin
  Self.Color := vgCorSistema;

  dtePeriodoIniL.Date := StartOfTheMonth(Date);
  dtePeriodoFimL.Date := Date;

  cedValorLancamento.Value := 0;

  edtNumReciboL.Clear;

  rgSituacaoL.ItemIndex := 1;

  btnFiltrarLancamentos.Click;

  dtePeriodoIniD.Date := StartOfTheMonth(Date);
  dtePeriodoFimD.Date := Date;

  cedValorDeposito.Value := 0;

  edtNumCodDeposito.Clear;

  rgSituacaoD.ItemIndex := 1;

  btnFiltrarDepositos.Click;

  btnDesfazerVinculo.Visible := False;

//  ShowScrollBar(dbgLancamentos.Handle, SB_HORZ, False);
//  ShowScrollBar(dbgDepositos.Handle, SB_HORZ, False);

  dteDataVinculo.Date := vgDataLancVigente;

  if dtePeriodoIniL.CanFocus then
    dtePeriodoIniL.SetFocus;
end;

procedure TFLancamentosVincularDepositos.dbgDepositosCellClick(
  Column: TColumn);
begin
  if dbgDepositos.SelectedField.DataType = ftBoolean then
    SalvarBoleano('D');
end;

procedure TFLancamentosVincularDepositos.dbgDepositosDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if gdSelected in State then
    dbgDepositos.Canvas.Brush.Color := clHighlight
  else
  begin
    if dmBaixa.cdsDepos.FieldByName('FLG_STATUS').AsString = 'B' then
      dbgDepositos.Canvas.Brush.Color := $00DFFFDF
    else
      dbgDepositos.Canvas.Brush.Color := $00A4C1FF;
  end;

  dbgDepositos.DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure TFLancamentosVincularDepositos.dbgDepositosTitleClick(
  Column: TColumn);
begin
  dmBaixa.cdsDepos.IndexFieldNames := Column.FieldName;
end;

procedure TFLancamentosVincularDepositos.dbgLancamentosCellClick(
  Column: TColumn);
begin
  if dbgLancamentos.SelectedField.DataType = ftBoolean then
    SalvarBoleano('L');
end;

procedure TFLancamentosVincularDepositos.dbgLancamentosDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if gdSelected in State then
    dbgLancamentos.Canvas.Brush.Color := clHighlight
  else
  begin
    if dmBaixa.cdsLanc.FieldByName('VINCULADO').AsString = 'S' then
      dbgLancamentos.Canvas.Brush.Color := $00DFFFDF
    else
      dbgLancamentos.Canvas.Brush.Color := $00A4C1FF;
  end;

  dbgLancamentos.DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure TFLancamentosVincularDepositos.dbgLancamentosTitleClick(
  Column: TColumn);
begin
  dmBaixa.cdsLanc.IndexFieldNames := Column.FieldName;
end;

procedure TFLancamentosVincularDepositos.btnEditarLancamentoClick(
  Sender: TObject);
var
  lEditado: Boolean;
begin
  lEditado := False;

  dmBaixa.cdsLanc.DisableControls;

  dmBaixa.cdsLanc.First;

  while not dmBaixa.cdsLanc.Eof do
  begin
    if dmBaixa.cdsLanc.FieldByName('SELECIONADO').AsBoolean then
    begin
      if Trim(dmBaixa.cdsLanc.FieldByName('VINCULADO').AsString) = 'S' then
        Application.MessageBox('Lan�amento j� Vinculado � Dep�sito n�o pode ser editado.',
                               'Aviso',
                               MB_OK + MB_ICONWARNING)
      else if (Trim(dmBaixa.cdsLanc.FieldByName('DESCR_NATUREZA').AsString) = 'SAL�RIO') and
        (Trim(dmBaixa.cdsLanc.FieldByName('DESCR_NATUREZA').AsString) = 'COMISS�O') and
        (Trim(dmBaixa.cdsLanc.FieldByName('DESCR_NATUREZA').AsString) = 'VALE') and
        (Trim(dmBaixa.cdsLanc.FieldByName('DESCR_NATUREZA').AsString) = 'OUTROS LAN�AMENTOS COM COLABORADOR') then
        Application.MessageBox('Esse Lan�amento somente pode ser alterado na Origem.',
                               'Aviso',
                               MB_OK + MB_ICONWARNING)
      else if (not dmBaixa.cdsLanc.FieldByName('DATA_PAGAMENTO').IsNull) and
       (dmBaixa.cdsLanc.FieldByName('DATA_PAGAMENTO').AsDateTime <= vgDataUltFech) then
        Application.MessageBox(PChar('O Caixa do dia ' +
                                     FormatDateTime('DD/MM/YYYY', dmBaixa.cdsLanc.FieldByName('DATA_LANCAMENTO_PARC').AsDateTime) +
                                     ' j� se encontra Fechado.'),
                               'Aviso',
                               MB_OK + MB_ICONWARNING)
      else if (dmBaixa.cdsLanc.FieldByName('FLG_STATUS_1').AsString = 'G') and
        (not vgPrm_EdLancPago) then
        Application.MessageBox('Seu usu�rio n�o possui permiss�o para editar Lan�amento Pago.',
                               'Aviso',
                               MB_OK + MB_ICONWARNING)
      else
      begin
        if Application.MessageBox(PChar('Deseja alterar os dados da Parcela N� ' + dmBaixa.cdsLanc.FieldByName('NUM_PARCELA').AsString +
                                        ' - Recibo ' + dmBaixa.cdsLanc.FieldByName('NUM_RECIBO').AsString +
                                        ' (' + FloatToStrF(dmBaixa.cdsLanc.FieldByName('VR_PARCELA_PREV').AsFloat, ffCurrency, 10, 2) + ')?'),
                                  'Altera��o de Lan�amento',
                                  MB_YESNO) = ID_YES then
        begin
          vgOperacao := E;

          vgOrigemFiltro := True;
          vgLanc_Codigo  := dmBaixa.cdsLanc.FieldByName('COD_LANCAMENTO').AsInteger;
          vgLanc_Ano     := dmBaixa.cdsLanc.FieldByName('ANO_LANCAMENTO').AsInteger;
          vgLanc_TpLanc  := dmBaixa.cdsLanc.FieldByName('TIPO_LANCAMENTO').AsString;

          try
            Application.CreateForm(TdmLancamento, dmLancamento);
            Application.CreateForm(TFCadastroLancamento, FCadastroLancamento);

            if dmBaixa.cdsLanc.FieldByName('TIPO_LANCAMENTO').AsString = 'D' then
              FCadastroLancamento.TipoLanc := DESP
            else
              FCadastroLancamento.TipoLanc := REC;

            FCadastroLancamento.lSomentePagamento := True;

            FCadastroLancamento.ShowModal;
          finally
            FreeAndNil(dmLancamento);
            FCadastroLancamento.Free;
          end;

          lEditado := True;
        end;
      end;
    end;

    dmBaixa.cdsLanc.Next;
  end;

  dmBaixa.cdsLanc.EnableControls;

  if lEditado then
    btnFiltrarLancamentos.Click;
end;

procedure TFLancamentosVincularDepositos.MarcarDesmarcarTodas(Tipo: String);
begin
  //Tipo: L = Lancamento / D = Deposito

  if Trim(Tipo) = 'L' then
  begin
    dmBaixa.cdsLanc.DisableControls;

    dmBaixa.cdsLanc.First;

    while not dmBaixa.cdsLanc.Eof do
    begin
      dmBaixa.cdsLanc.Edit;

      if dmBaixa.cdsLanc.FieldByName('VINCULADO').AsString = 'N' then
      begin
        if chbSelecionarTodosL.Checked then
          dmBaixa.cdsLanc.FieldByName('SELECIONADO').AsBoolean := True
        else
          dmBaixa.cdsLanc.FieldByName('SELECIONADO').AsBoolean := False;
      end
      else
        dmBaixa.cdsLanc.FieldByName('SELECIONADO').AsBoolean := False;

      dmBaixa.cdsLanc.Post;

      dmBaixa.cdsLanc.Next;
    end;

    if dmBaixa.cdsLanc.FieldByName('SELECIONADO').AsBoolean then
      chbSelecionarTodosL.Caption := 'Desmarcar Todos'
    else
      chbSelecionarTodosL.Caption := 'Desmarcar Todos';

    dmBaixa.cdsLanc.EnableControls;
  end
  else if Trim(Tipo) = 'D' then
  begin
    dmBaixa.cdsDepos.DisableControls;

    dmBaixa.cdsDepos.First;

    while not dmBaixa.cdsDepos.Eof do
    begin
      dmBaixa.cdsDepos.Edit;

      if dmBaixa.cdsDepos.FieldByName('FLG_STATUS').AsString = 'F' then
      begin
        if chbSelecionarTodosD.Checked then
          dmBaixa.cdsDepos.FieldByName('SELECIONADO').AsBoolean := True
        else
          dmBaixa.cdsDepos.FieldByName('SELECIONADO').AsBoolean := False;
      end
      else
        dmBaixa.cdsDepos.FieldByName('SELECIONADO').AsBoolean := False;

      dmBaixa.cdsDepos.Post;

      dmBaixa.cdsDepos.Next;
    end;

    if dmBaixa.cdsDepos.FieldByName('SELECIONADO').AsBoolean then
      chbSelecionarTodosD.Caption := 'Desmarcar Todos'
    else
      chbSelecionarTodosD.Caption := 'Desmarcar Todos';

    dmBaixa.cdsDepos.EnableControls;
  end;
end;

procedure TFLancamentosVincularDepositos.mmObservacoesKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then // TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then // ENTER
  begin
    if dteDataVinculo.Date = 0 then
    begin
      Application.MessageBox('Falta informar a DATA DE V�NCULO.', 'Aviso', MB_OK);

      if dteDataVinculo.CanFocus then
        dteDataVinculo.SetFocus;
    end
    else
      btnConfirmarVinculo.Click;

    Key := #0;
  end;

  if Key = #27 then // ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFLancamentosVincularDepositos.rgSituacaoDClick(Sender: TObject);
begin
  if rgSituacaoD.ItemIndex = 2 then
    btnDesfazerVinculo.Visible := True
  else
    btnDesfazerVinculo.Visible := False;

  btnFiltrarDepositos.Click;
end;

procedure TFLancamentosVincularDepositos.rgSituacaoDExit(Sender: TObject);
begin
  if rgSituacaoD.ItemIndex = 2 then
    btnDesfazerVinculo.Visible := True
  else
    btnDesfazerVinculo.Visible := False;

  btnFiltrarDepositos.Click;
end;

procedure TFLancamentosVincularDepositos.rgSituacaoLClick(Sender: TObject);
begin
  btnFiltrarLancamentos.Click;
end;

procedure TFLancamentosVincularDepositos.rgSituacaoLExit(Sender: TObject);
begin
  btnFiltrarLancamentos.Click;
end;

procedure TFLancamentosVincularDepositos.SalvarBoleano(Tipo: String);
begin
  //Tipo: L = Lancamento / D = Deposito

  if Trim(Tipo) = 'L' then
  begin
    dbgLancamentos.SelectedField.DataSet.Edit;

    if rgSituacaoD.ItemIndex = 2 then
      dbgLancamentos.SelectedField.AsBoolean := not dbgLancamentos.SelectedField.AsBoolean
    else
    begin
      if dmBaixa.cdsLanc.FieldByName('VINCULADO').AsString = 'N' then
        dbgLancamentos.SelectedField.AsBoolean := not dbgLancamentos.SelectedField.AsBoolean
      else
        dbgLancamentos.SelectedField.AsBoolean := False;
    end;

    dbgLancamentos.SelectedField.DataSet.Post;

    cedValorTotalLancamentos.Value := dmBaixa.cValorTotalLancamentos;
  end
  else if Trim(Tipo) = 'D' then
  begin
    dbgDepositos.SelectedField.DataSet.Edit;

    if rgSituacaoD.ItemIndex = 2 then
      dbgDepositos.SelectedField.AsBoolean := not dbgDepositos.SelectedField.AsBoolean
    else
    begin
      if dmBaixa.cdsDepos.FieldByName('FLG_STATUS').AsString = 'F' then
        dbgDepositos.SelectedField.AsBoolean := not dbgDepositos.SelectedField.AsBoolean
      else
        dbgDepositos.SelectedField.AsBoolean := False;
    end;

    dbgDepositos.SelectedField.DataSet.Post;

    cedValorTotalDepositos.Value := dmBaixa.cValorTotalDepositos;
  end;
end;

end.
