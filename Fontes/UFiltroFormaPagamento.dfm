inherited FFiltroFormaPagamento: TFFiltroFormaPagamento
  Caption = 'Filtro de Formas de Pagamento'
  ClientHeight = 277
  ClientWidth = 774
  OnCreate = FormCreate
  ExplicitWidth = 780
  ExplicitHeight = 306
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 768
    Height = 271
    ExplicitWidth = 768
    ExplicitHeight = 271
    inherited pnlGrid: TPanel
      Top = 52
      Width = 684
      Height = 216
      ExplicitTop = 52
      ExplicitWidth = 684
      ExplicitHeight = 216
      inherited pnlGridPai: TPanel
        Width = 684
        Height = 216
        ExplicitWidth = 684
        ExplicitHeight = 216
        inherited dbgGridPaiPadrao: TDBGrid
          Width = 684
          Height = 216
          Columns = <
            item
              Expanded = False
              FieldName = 'ID'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'DESCR_FORMAPAGAMENTO'
              Title.Caption = 'Descri'#231#227'o'
              Width = 340
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MAX_PARCELAS'
              Title.Caption = 'M'#225'x. Parc.'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MIN_PARCELAS'
              Title.Caption = 'M'#237'n. Parc.'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PRAZO'
              Title.Caption = 'Prazo'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PERC_TARIFA'
              Title.Caption = '%Tarifa'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TIPO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'FLG_ATIVA'
              Title.Caption = 'Ativa'
              Visible = True
            end>
        end
      end
    end
    inherited pnlFiltro: TPanel
      Width = 768
      Height = 49
      ExplicitWidth = 768
      ExplicitHeight = 49
      inherited btnFiltrar: TJvTransparentButton
        Left = 593
        Top = 20
        ExplicitLeft = 593
        ExplicitTop = 20
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 682
        Top = 20
        ExplicitLeft = 682
        ExplicitTop = 20
      end
      object lblDescricao: TLabel
        Left = 81
        Top = 8
        Width = 51
        Height = 14
        Caption = 'Descri'#231#227'o'
      end
      object edtDescricao: TEdit
        Left = 81
        Top = 24
        Width = 269
        Height = 22
        Color = 16114127
        MaxLength = 25
        TabOrder = 0
        OnKeyPress = edtDescricaoKeyPress
      end
      object rgFlgAtiva: TRadioGroup
        Left = 356
        Top = 8
        Width = 231
        Height = 38
        Caption = 'Situa'#231#227'o'
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Todas'
          'Ativa'
          'Inativa')
        TabOrder = 1
        OnExit = rgFlgAtivaExit
      end
    end
    inherited pnlMenu: TPanel
      Top = 52
      Height = 216
      ExplicitTop = 52
      ExplicitHeight = 216
      inherited btnImprimir: TJvTransparentButton
        Visible = False
      end
    end
  end
  inherited dsGridPaiPadrao: TDataSource
    Left = 700
    Top = 219
  end
  inherited qryGridPaiPadrao: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT ID_FORMAPAGAMENTO AS ID,'
      '       DESCR_FORMAPAGAMENTO,'
      '       MAX_PARCELAS,'
      '       MIN_PARCELAS,'
      '       PRAZO,'
      '       PERC_TARIFA,'
      '       TIPO,'
      '       FLG_ATIVA'
      '  FROM FORMAPAGAMENTO'
      'ORDER BY DESCR_FORMAPAGAMENTO')
    Left = 700
    Top = 167
    object qryGridPaiPadraoID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID_FORMAPAGAMENTO'
      Required = True
    end
    object qryGridPaiPadraoDESCR_FORMAPAGAMENTO: TStringField
      FieldName = 'DESCR_FORMAPAGAMENTO'
      Origin = 'DESCR_FORMAPAGAMENTO'
      Size = 250
    end
    object qryGridPaiPadraoMAX_PARCELAS: TIntegerField
      FieldName = 'MAX_PARCELAS'
      Origin = 'MAX_PARCELAS'
    end
    object qryGridPaiPadraoMIN_PARCELAS: TIntegerField
      FieldName = 'MIN_PARCELAS'
      Origin = 'MIN_PARCELAS'
    end
    object qryGridPaiPadraoPRAZO: TIntegerField
      FieldName = 'PRAZO'
      Origin = 'PRAZO'
    end
    object qryGridPaiPadraoPERC_TARIFA: TBCDField
      FieldName = 'PERC_TARIFA'
      Origin = 'PERC_TARIFA'
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoTIPO: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      FixedChar = True
      Size = 2
    end
    object qryGridPaiPadraoFLG_ATIVA: TStringField
      FieldName = 'FLG_ATIVA'
      Origin = 'FLG_ATIVA'
      FixedChar = True
      Size = 1
    end
  end
end
