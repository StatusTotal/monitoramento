{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UDMColaborador.pas
  Descricao:   Data Module de Colaborador
  Author   :   Cristina
  Date:        27-mai-2016
  Last Update: 27-jul-2017 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UDMColaborador;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Datasnap.DBClient,
  Datasnap.Provider, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TdmColaborador = class(TDataModule)
    qryCargo: TFDQuery;
    dsCargo: TDataSource;
    qryFuncionario_F: TFDQuery;
    dspFuncionario_F: TDataSetProvider;
    cdsFuncionario_F: TClientDataSet;
    dsFuncionario_F: TDataSource;
    qryFuncComissao_F: TFDQuery;
    dspFuncComissao_F: TDataSetProvider;
    cdsFuncComissao_F: TClientDataSet;
    dsFuncComissao_F: TDataSource;
    qryVale_F: TFDQuery;
    dsVale_F: TDataSource;
    cdsFuncComissao_FID_FUNCIONARIO_COMISSAO: TIntegerField;
    cdsFuncComissao_FID_FUNCIONARIO_FK: TIntegerField;
    cdsFuncComissao_FPERCENT_COMISSAO: TBCDField;
    cdsFuncComissao_FID_SISTEMA_FK: TIntegerField;
    cdsFuncComissao_FNOVO: TBooleanField;
    qrySistema: TFDQuery;
    qryTipoAto: TFDQuery;
    dsSistema: TDataSource;
    dsTipoAto: TDataSource;
    dsEscolaridade: TDataSource;
    dsUFCTPS: TDataSource;
    dsUFEnd: TDataSource;
    dsEstadoCivil: TDataSource;
    qryEscolaridade: TFDQuery;
    qryUFCTPS: TFDQuery;
    qryUFEnd: TFDQuery;
    qryEstadoCivil: TFDQuery;
    qryCidade: TFDQuery;
    dsCidade: TDataSource;
    cdsFuncionario_FID_FUNCIONARIO: TIntegerField;
    cdsFuncionario_FNOME_FUNCIONARIO: TStringField;
    cdsFuncionario_FDATA_NASCIMENTO: TDateField;
    cdsFuncionario_FCPF: TStringField;
    cdsFuncionario_FNUM_CTPS: TStringField;
    cdsFuncionario_FSERIE_CTPS: TStringField;
    cdsFuncionario_FDATA_CTPS: TDateField;
    cdsFuncionario_FSIGLA_ESTADO_CTPS: TStringField;
    cdsFuncionario_FNUM_TITULO_ELEITOR: TStringField;
    cdsFuncionario_FZONA_TE: TStringField;
    cdsFuncionario_FSECAO_TE: TStringField;
    cdsFuncionario_FCOD_MUNICIPIO_TE: TStringField;
    cdsFuncionario_FSIGLA_ESTADO_TE: TStringField;
    cdsFuncionario_FDATA_EMISSAO_TE: TDateField;
    cdsFuncionario_FTIPO_SANGUINEO: TStringField;
    cdsFuncionario_FFATOR_RH: TStringField;
    cdsFuncionario_FLOGRADOURO: TStringField;
    cdsFuncionario_FNUM_LOGRADOURO: TStringField;
    cdsFuncionario_FCOMPLEMENTO_LOGRADOURO: TStringField;
    cdsFuncionario_FBAIRRO_LOGRADOURO: TStringField;
    cdsFuncionario_FCEP_LOGR: TStringField;
    cdsFuncionario_FCOD_MUNICIPIO_LOGR: TStringField;
    cdsFuncionario_FSIGLA_ESTADO_LOGR: TStringField;
    cdsFuncionario_FTELEFONE: TStringField;
    cdsFuncionario_FCELULAR: TStringField;
    cdsFuncionario_FEMAIL: TStringField;
    cdsFuncionario_FID_CARGO_FK: TIntegerField;
    cdsFuncionario_FQTD_FILHOS: TIntegerField;
    cdsFuncionario_FQTD_DEPENDENTES: TIntegerField;
    cdsFuncionario_FHORA_ENT_TRAB: TTimeField;
    cdsFuncionario_FHORA_SAI_ALMOCO: TTimeField;
    cdsFuncionario_FHORA_ENT_ALMOCO: TTimeField;
    cdsFuncionario_FHORA_SAI_TRAB: TTimeField;
    cdsFuncionario_FVR_MAXIMO_VALE: TBCDField;
    cdsFuncionario_FDATA_CADASTRO: TDateField;
    cdsFuncionario_FDATA_DESLIGAMENTO: TDateField;
    cdsFuncionario_FFLG_ATIVO: TStringField;
    cdsFuncionario_FNUM_IDENTIDADE: TStringField;
    cdsFuncionario_FORGAO_EMISSOR: TStringField;
    cdsFuncionario_FDATA_EMISSAO_IDENTIDADE: TDateField;
    cdsFuncionario_FSEXO: TStringField;
    cdsFuncionario_FCOD_ESCOLARIDADE: TIntegerField;
    cdsFuncionario_FCOD_ESTADOCIVIL: TIntegerField;
    cdsFuncionario_FDATA_ADMISSAO: TDateField;
    cdsFuncionario_FNUM_PIS: TStringField;
    cdsFuncionario_FVR_SALARIO: TBCDField;
    dsUFTE: TDataSource;
    qryUFTE: TFDQuery;
    qryCidadeTE: TFDQuery;
    dsCidadeTE: TDataSource;
    qryComisRec_F: TFDQuery;
    dsComisRec_F: TDataSource;
    qrySistemaCom: TFDQuery;
    qryTipoAtoCom: TFDQuery;
    dsSistemaCom: TDataSource;
    dsTipoAtoCom: TDataSource;
    cdsFuncComissao_FCOD_ADICIONAL: TIntegerField;
    cdsVale_F: TClientDataSet;
    cdsComisRec_F: TClientDataSet;
    dspVale_F: TDataSetProvider;
    dspComisRec_F: TDataSetProvider;
    cdsVale_FID_VALE: TIntegerField;
    cdsVale_FID_FUNCIONARIO_FK: TIntegerField;
    cdsVale_FVR_VALE: TBCDField;
    cdsVale_FDESCR_VALE: TStringField;
    cdsVale_FCAD_ID_USUARIO: TIntegerField;
    cdsVale_FDATA_QUITACAO: TDateField;
    cdsVale_FQUIT_ID_USUARIO: TIntegerField;
    cdsFuncComissao_FNOMESISTEMA: TStringField;
    cdsFuncionario_FID_PESSOA: TIntegerField;
    qryODespF_F: TFDQuery;
    dspODespF_F: TDataSetProvider;
    cdsODespF_F: TClientDataSet;
    dsODespF_F: TDataSource;
    cdsODespF_FID_OUTRADESPESA_FUNC: TIntegerField;
    cdsODespF_FID_FUNCIONARIO_FK: TIntegerField;
    cdsODespF_FID_TIPO_OUTRADESP_FUNC_FK: TIntegerField;
    cdsODespF_FVR_OUTRADESPESA_FUNC: TBCDField;
    cdsODespF_FDESCR_OUTRADESPESA_FUNC: TStringField;
    cdsODespF_FCAD_ID_USUARIO: TIntegerField;
    cdsODespF_FDATA_QUITACAO: TDateField;
    cdsODespF_FQUIT_ID_USUARIO: TIntegerField;
    cdsODespF_FFLG_CANCELADO: TStringField;
    cdsODespF_FDATA_CANCELAMENTO: TDateField;
    cdsODespF_FCANCEL_ID_USUARIO: TIntegerField;
    cdsVale_FFLG_CANCELADO: TStringField;
    cdsVale_FDATA_CANCELAMENTO: TDateField;
    cdsVale_FCANCEL_ID_USUARIO: TIntegerField;
    cdsODespF_FTIPO_ODESPF: TStringField;
    cdsODespF_FDATA_OUTRADESPESA_FUNC: TDateField;
    cdsODespF_FID_FECHAMENTO_SALARIO_FK: TIntegerField;
    cdsODespF_FDATA_CADASTRO: TSQLTimeStampField;
    cdsComisRec_FID_COMISSAO: TIntegerField;
    cdsComisRec_FID_FUNCIONARIO_FK: TIntegerField;
    cdsComisRec_FID_SISTEMA_FK: TIntegerField;
    cdsComisRec_FCOD_ADICIONAL: TIntegerField;
    cdsComisRec_FID_ORIGEM: TIntegerField;
    cdsComisRec_FTIPO_ORIGEM: TStringField;
    cdsComisRec_FPERCENT_COMISSAO: TBCDField;
    cdsComisRec_FVR_COMISSAO: TBCDField;
    cdsComisRec_FID_FECHAMENTO_SALARIO_FK: TIntegerField;
    cdsComisRec_FCAD_ID_USUARIO: TIntegerField;
    cdsComisRec_FDATA_PAGAMENTO: TDateField;
    cdsComisRec_FPAG_ID_USUARIO: TIntegerField;
    cdsComisRec_FFLG_CANCELADO: TStringField;
    cdsComisRec_FDATA_CANCELAMENTO: TDateField;
    cdsComisRec_FCANCEL_ID_USUARIO: TIntegerField;
    cdsVale_FDATA_VALE: TDateField;
    cdsVale_FID_FECHAMENTO_SALARIO_FK: TIntegerField;
    cdsVale_FDATA_CADASTRO: TSQLTimeStampField;
    cdsComisRec_FDATA_COMISSAO: TDateField;
    cdsComisRec_FDATA_CADASTRO: TSQLTimeStampField;
    cdsComisRec_FID_ITEM_FK: TIntegerField;
    cdsFuncComissao_FDESCR_ITEM: TStringField;
    cdsComisRec_FDESCR_ITEM: TStringField;
    cdsFuncComissao_FID_ITEM_FK: TIntegerField;
    qryFunc2: TFDQuery;
    dspFunc2: TDataSetProvider;
    cdsFunc2: TClientDataSet;
    cdsFunc2ID_FUNCIONARIO: TIntegerField;
    cdsFunc2NOME_FUNCIONARIO: TStringField;
    cdsFunc2DATA_NASCIMENTO: TDateField;
    cdsFunc2NUM_IDENTIDADE: TStringField;
    cdsFunc2ORGAO_EMISSOR: TStringField;
    cdsFunc2DATA_EMISSAO_IDENTIDADE: TDateField;
    cdsFunc2CPF: TStringField;
    cdsFunc2NUM_CTPS: TStringField;
    cdsFunc2SERIE_CTPS: TStringField;
    cdsFunc2DATA_CTPS: TDateField;
    cdsFunc2NUM_PIS: TStringField;
    cdsFunc2SIGLA_ESTADO_CTPS: TStringField;
    cdsFunc2NUM_TITULO_ELEITOR: TStringField;
    cdsFunc2ZONA_TE: TStringField;
    cdsFunc2SECAO_TE: TStringField;
    cdsFunc2COD_MUNICIPIO_TE: TStringField;
    cdsFunc2SIGLA_ESTADO_TE: TStringField;
    cdsFunc2DATA_EMISSAO_TE: TDateField;
    cdsFunc2TIPO_SANGUINEO: TStringField;
    cdsFunc2FATOR_RH: TStringField;
    cdsFunc2LOGRADOURO: TStringField;
    cdsFunc2NUM_LOGRADOURO: TStringField;
    cdsFunc2COMPLEMENTO_LOGRADOURO: TStringField;
    cdsFunc2BAIRRO_LOGRADOURO: TStringField;
    cdsFunc2CEP_LOGR: TStringField;
    cdsFunc2COD_MUNICIPIO_LOGR: TStringField;
    cdsFunc2SIGLA_ESTADO_LOGR: TStringField;
    cdsFunc2TELEFONE: TStringField;
    cdsFunc2CELULAR: TStringField;
    cdsFunc2EMAIL: TStringField;
    cdsFunc2ID_CARGO_FK: TIntegerField;
    cdsFunc2COD_ESCOLARIDADE: TIntegerField;
    cdsFunc2COD_ESTADOCIVIL: TIntegerField;
    cdsFunc2QTD_FILHOS: TIntegerField;
    cdsFunc2QTD_DEPENDENTES: TIntegerField;
    cdsFunc2HORA_ENT_TRAB: TTimeField;
    cdsFunc2HORA_SAI_ALMOCO: TTimeField;
    cdsFunc2HORA_ENT_ALMOCO: TTimeField;
    cdsFunc2HORA_SAI_TRAB: TTimeField;
    cdsFunc2VR_SALARIO: TBCDField;
    cdsFunc2VR_MAXIMO_VALE: TBCDField;
    cdsFunc2SEXO: TStringField;
    cdsFunc2DATA_ADMISSAO: TDateField;
    cdsFunc2DATA_CADASTRO: TDateField;
    cdsFunc2DATA_DESLIGAMENTO: TDateField;
    cdsFunc2FLG_ATIVO: TStringField;
    dsFunc2: TDataSource;
    qryFuncCom2: TFDQuery;
    dspFuncCom2: TDataSetProvider;
    cdsFuncCom2: TClientDataSet;
    cdsFuncCom2ID_FUNCIONARIO_COMISSAO: TIntegerField;
    cdsFuncCom2ID_FUNCIONARIO_FK: TIntegerField;
    cdsFuncCom2PERCENT_COMISSAO: TBCDField;
    cdsFuncCom2ID_SISTEMA_FK: TIntegerField;
    cdsFuncCom2COD_ADICIONAL: TIntegerField;
    cdsFuncCom2ID_ITEM_FK: TIntegerField;
    dsFuncCom2: TDataSource;
    cdsFunc2ID_PESSOA: TIntegerField;
    cdsFuncComissao_FVR_COMISSAO: TBCDField;
    cdsFuncComissao_FTIPO_COBRANCA: TStringField;
    cdsODespF_FFLG_MODALIDADE: TStringField;
    cdsODespF_FFLG_RECORRENTE: TStringField;
    cdsComisRec_FTIPO_COBRANCA: TStringField;
    cdsFuncCom2VR_COMISSAO: TBCDField;
    cdsFuncCom2TIPO_COBRANCA: TStringField;
    procedure cdsFuncComissao_FCalcFields(DataSet: TDataSet);
    procedure cdsFuncionarioSALARIOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsFuncionarioVR_MAXIMO_VALEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFuncComissao_FPERCENT_COMISSAOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsComisRecPERCENT_COMISSAOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsComisRecVR_COMISSAOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsComisRec_FCalcFields(DataSet: TDataSet);
    procedure cdsODespF_FVR_OUTRADESPESA_FUNCGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsVale_FVR_VALEGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsODespF_FCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmColaborador: TdmColaborador;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{$R *.dfm}

procedure TdmColaborador.cdsComisRecPERCENT_COMISSAOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmColaborador.cdsComisRecVR_COMISSAOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmColaborador.cdsComisRec_FCalcFields(DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with qryAux, SQL do
  begin
    //Sistema
    if cdsComisRec_F.FieldByName('ID_SISTEMA_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT * ' +
              '  FROM SISTEMA ' +
              ' WHERE ID_SISTEMA = :ID_SISTEMA';
      Params.ParamByName('ID_SISTEMA').AsInteger := cdsComisRec_F.FieldByName('ID_SISTEMA_FK').AsInteger;
      Open;

      cdsComisRec_F.FieldByName('NOMESISTEMA').AsString := qryAux.FieldByName('NOME_SISTEMA').AsString;
    end;

    //Tipo Ato
    if cdsComisRec_F.FieldByName('ID_ITEM_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT ID_ITEM, UPPER(DESCR_ITEM) AS DESCR_ITEM ' +
              '  FROM ITEM ' +
              ' WHERE ID_ITEM = :ID_ITEM';
      Params.ParamByName('ID_ITEM').AsInteger := cdsComisRec_F.FieldByName('ID_ITEM_FK').AsInteger;
      Open;

      cdsComisRec_F.FieldByName('DESCR_ITEM').AsString := qryAux.FieldByName('DESCR_ITEM').AsString;
    end;
  end;

  FreeAndNil(qryAux);
end;

procedure TdmColaborador.cdsFuncComissao_FCalcFields(DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with qryAux, SQL do
  begin
    //Sistema
    if cdsFuncComissao_F.FieldByName('ID_SISTEMA_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT * ' +
              '  FROM SISTEMA ' +
              ' WHERE ID_SISTEMA = :ID_SISTEMA';
      Params.ParamByName('ID_SISTEMA').AsInteger := cdsFuncComissao_F.FieldByName('ID_SISTEMA_FK').AsInteger;
      Open;

      cdsFuncComissao_F.FieldByName('NOMESISTEMA').AsString := qryAux.FieldByName('NOME_SISTEMA').AsString;
    end;

    //Tipo Ato
    if cdsFuncComissao_F.FieldByName('ID_ITEM_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT ID_ITEM, UPPER(DESCR_ITEM) AS DESCR_ITEM ' +
              '  FROM ITEM ' +
              ' WHERE ID_ITEM = :ID_ITEM';
      Params.ParamByName('ID_ITEM').AsInteger := cdsFuncComissao_F.FieldByName('ID_ITEM_FK').AsInteger;
      Open;

      cdsFuncComissao_F.FieldByName('DESCR_ITEM').AsString := qryAux.FieldByName('DESCR_ITEM').AsString;
    end;
  end;

  FreeAndNil(qryAux);
end;

procedure TdmColaborador.cdsFuncComissao_FPERCENT_COMISSAOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmColaborador.cdsFuncionarioSALARIOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmColaborador.cdsFuncionarioVR_MAXIMO_VALEGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmColaborador.cdsODespF_FCalcFields(DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with qryAux, SQL do
  begin
    //Tipo Despesa
    if cdsODespF_F.FieldByName('ID_TIPO_OUTRADESP_FUNC_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT ID_TIPO_OUTRADESP_FUNC, DESCR_TIPO_OUTRADESP_FUNC ' +
              '  FROM TIPO_OUTRADESP_FUNC ' +
              ' WHERE ID_TIPO_OUTRADESP_FUNC = :ID_TIPO_OUTRADESP_FUNC';
      Params.ParamByName('ID_TIPO_OUTRADESP_FUNC').AsInteger := cdsODespF_F.FieldByName('ID_TIPO_OUTRADESP_FUNC_FK').AsInteger;
      Open;

      cdsODespF_F.FieldByName('TIPO_ODESPF').AsString := qryAux.FieldByName('DESCR_TIPO_OUTRADESP_FUNC').AsString;
    end;
  end;

  FreeAndNil(qryAux);
end;

procedure TdmColaborador.cdsODespF_FVR_OUTRADESPESA_FUNCGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmColaborador.cdsVale_FVR_VALEGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

end.


