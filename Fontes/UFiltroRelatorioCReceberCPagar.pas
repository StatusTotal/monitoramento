{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroRelatorioCReceberCPagar.pas
  Descricao:   Filtro Relatorio de Contas a Receber x Contas a Pagar (R08 - R08DET)
  Author   :   Cristina
  Date:        30-nov-2016
  Last Update: 11-out-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroRelatorioCReceberCPagar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroRelatorioPadrao, Vcl.StdCtrls,
  JvExControls, JvButton, JvTransparentButton, Vcl.ExtCtrls, Vcl.Mask, JvExMask,
  JvToolEdit, FireDAC.Stan.Param;

type
  TFFiltroRelatorioCReceberCPagar = class(TFFiltroRelatorioPadrao)
    lblPeriodo: TLabel;
    lblPeriodoInicio: TLabel;
    dteDataInicio: TJvDateEdit;
    lblPeriodoFim: TLabel;
    dteDataFim: TJvDateEdit;
    rgTipoConta: TRadioGroup;
    chbSomenteVencidas: TCheckBox;
    procedure btnLimparClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    procedure GravarLog; override;
    procedure GerarRelatorio(ImpDet: Boolean); override;
    function VerificarFiltro: Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroRelatorioCReceberCPagar: TFFiltroRelatorioCReceberCPagar;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UDMRelatorios, UGDM, UVariaveisGlobais;

{ TFFiltroRelatorioCReceberCPagar }

procedure TFFiltroRelatorioCReceberCPagar.btnLimparClick(Sender: TObject);
begin
  inherited;

  dteDataInicio.Date := Date;
  dteDataFim.Date    := Date;

  rgTipoConta.ItemIndex := 0;

  chbTipoSintetico.Checked := True;
  chbTipoAnalitico.Checked := False;

  chbExportarPDF.Checked := True;

  chbExportarExcel.Enabled := vgPrm_ExpRelRecDesp;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioCReceberCPagar.btnVisualizarClick(Sender: TObject);
begin
  inherited;

  if not VerificarFiltro then
    Exit;

  dmRelatorios.qryRel08_Totais.Close;

  dmRelatorios.qryRel08_Totais.Params.ParamByName('DATA_INI1').Value := dteDataInicio.Date;
  dmRelatorios.qryRel08_Totais.Params.ParamByName('DATA_FIM1').Value := dteDataFim.Date;
  dmRelatorios.qryRel08_Totais.Params.ParamByName('DATA_INI2').Value := dteDataInicio.Date;
  dmRelatorios.qryRel08_Totais.Params.ParamByName('DATA_FIM2').Value := dteDataFim.Date;

  case rgTipoConta.ItemIndex of
    0:
    begin  //Totais
      dmRelatorios.qryRel08_Totais.Params.ParamByName('TP_LANC11').Value := 'R';
      dmRelatorios.qryRel08_Totais.Params.ParamByName('TP_LANC12').Value := 'D';
      dmRelatorios.qryRel08_Totais.Params.ParamByName('TP_LANC21').Value := 'R';
      dmRelatorios.qryRel08_Totais.Params.ParamByName('TP_LANC22').Value := 'D';
    end;
    1:
    begin  //Contas a Receber
      dmRelatorios.qryRel08_Totais.Params.ParamByName('TP_LANC11').Value := 'R';
      dmRelatorios.qryRel08_Totais.Params.ParamByName('TP_LANC12').Value := 'R';
      dmRelatorios.qryRel08_Totais.Params.ParamByName('TP_LANC21').Value := 'R';
      dmRelatorios.qryRel08_Totais.Params.ParamByName('TP_LANC22').Value := 'R';
    end;
    2:
    begin  //Contas a Pagar
      dmRelatorios.qryRel08_Totais.Params.ParamByName('TP_LANC11').Value := 'D';
      dmRelatorios.qryRel08_Totais.Params.ParamByName('TP_LANC12').Value := 'D';
      dmRelatorios.qryRel08_Totais.Params.ParamByName('TP_LANC21').Value := 'D';
      dmRelatorios.qryRel08_Totais.Params.ParamByName('TP_LANC22').Value := 'D';
    end;
  end;

  if chbSomenteVencidas.Checked then
  begin
    dmRelatorios.qryRel08_Totais.Params.ParamByName('VENC1').Value := 1;
    dmRelatorios.qryRel08_Totais.Params.ParamByName('VENC2').Value := 1;
  end
  else
  begin
    dmRelatorios.qryRel08_Totais.Params.ParamByName('VENC1').Value := 0;
    dmRelatorios.qryRel08_Totais.Params.ParamByName('VENC2').Value := 0;
  end;

  dmRelatorios.qryRel08_Totais.Open;

  if dmRelatorios.qryRel08_Totais.RecordCount = 0 then
  begin
    Application.MessageBox('N�o h� Lan�amentos registrados.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
  end
  else
  begin
    dmRelatorios.qryRel08_CRCP.Close;

    dmRelatorios.qryRel08_CRCP.Params.ParamByName('DATA_INI1').Value := dteDataInicio.Date;
    dmRelatorios.qryRel08_CRCP.Params.ParamByName('DATA_FIM1').Value := dteDataFim.Date;
    dmRelatorios.qryRel08_CRCP.Params.ParamByName('DATA_INI2').Value := dteDataInicio.Date;
    dmRelatorios.qryRel08_CRCP.Params.ParamByName('DATA_FIM2').Value := dteDataFim.Date;

    case rgTipoConta.ItemIndex of
      0:
      begin  //Totais
        dmRelatorios.qryRel08_CRCP.Params.ParamByName('TP_LANC11').Value := 'R';
        dmRelatorios.qryRel08_CRCP.Params.ParamByName('TP_LANC12').Value := 'D';
        dmRelatorios.qryRel08_CRCP.Params.ParamByName('TP_LANC21').Value := 'R';
        dmRelatorios.qryRel08_CRCP.Params.ParamByName('TP_LANC22').Value := 'D';
      end;
      1:
      begin  //Contas a Receber
        dmRelatorios.qryRel08_CRCP.Params.ParamByName('TP_LANC11').Value := 'R';
        dmRelatorios.qryRel08_CRCP.Params.ParamByName('TP_LANC12').Value := 'R';
        dmRelatorios.qryRel08_CRCP.Params.ParamByName('TP_LANC21').Value := 'R';
        dmRelatorios.qryRel08_CRCP.Params.ParamByName('TP_LANC22').Value := 'R';
      end;
      2:
      begin  //Contas a Pagar
        dmRelatorios.qryRel08_CRCP.Params.ParamByName('TP_LANC11').Value := 'D';
        dmRelatorios.qryRel08_CRCP.Params.ParamByName('TP_LANC12').Value := 'D';
        dmRelatorios.qryRel08_CRCP.Params.ParamByName('TP_LANC21').Value := 'D';
        dmRelatorios.qryRel08_CRCP.Params.ParamByName('TP_LANC22').Value := 'D';
      end;
    end;

    if chbSomenteVencidas.Checked then
    begin
      dmRelatorios.qryRel08_CRCP.Params.ParamByName('VENC1').Value := 1;
      dmRelatorios.qryRel08_CRCP.Params.ParamByName('VENC2').Value := 1;
    end
    else
    begin
      dmRelatorios.qryRel08_CRCP.Params.ParamByName('VENC1').Value := 0;
      dmRelatorios.qryRel08_CRCP.Params.ParamByName('VENC2').Value := 0;
    end;

    dmRelatorios.qryRel08_CRCP.Open;

    if chbTipoSintetico.Checked then
      GerarRelatorio(False);

    if chbTipoAnalitico.Checked then
      GerarRelatorio(True);
  end;
end;

procedure TFFiltroRelatorioCReceberCPagar.FormShow(Sender: TObject);
begin
  inherited;

  dteDataInicio.Date := Date;
  dteDataFim.Date    := Date;

  rgTipoConta.ItemIndex := 0;

  chbTipoSintetico.Checked := True;
  chbTipoAnalitico.Checked := False;

  chbExportarPDF.Checked := True;

  chbExportarExcel.Enabled := vgPrm_ExpRelRecDesp;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;

  chbExibirGrafico.Visible := False;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioCReceberCPagar.GerarRelatorio(ImpDet: Boolean);
begin
  inherited;

  try
    sTituloRelatorio    := 'CONTAS A RECEBER x CONTAS A PAGAR';
    sSubtituloRelatorio := 'Per�odo de ' + DateToStr(dteDataInicio.Date) + ' a ' + DateToStr(dteDataFim.Date);

    if ImpDet then
    begin
      lExibeDetalhe := True;

      dmRelatorios.DefinirRelatorio(R08DET);

      dmRelatorios.qryRel08_Det.Close;
      dmRelatorios.qryRel08_Det.Params.ParamByName('COD_LANCAMENTO').Value := dmRelatorios.qryRel08_CRCP.FieldByName('COD_LANCAMENTO_FK').AsInteger;
      dmRelatorios.qryRel08_Det.Params.ParamByName('ANO_LANCAMENTO').Value := dmRelatorios.qryRel08_CRCP.FieldByName('ANO_LANCAMENTO_FK').AsInteger;
      dmRelatorios.qryRel08_Det.Open;
    end
    else
    begin
      lExibeDetalhe := False;
      dmRelatorios.DefinirRelatorio(R08);
    end;

    dmRelatorios.ImprimirRelatorio(chbExportarPDF.Checked, chbExportarExcel.Checked);

    GravarLog;
  except
    Application.MessageBox('Erro ao gerar relat�rio de Contas a Receber x Contas a Pagar.',
                           'Erro',
                           MB_OK + MB_ICONERROR);
  end;
end;

procedure TFFiltroRelatorioCReceberCPagar.GravarLog;
begin
  inherited;

  BS.GravarUsuarioLog(50,
                      '',
                      'Impress�o de Contas a Receber X Contas a Pagar (' + DateToStr(dDataRelatorio) +
                                                                       ' ' +
                                                                       TimeToStr(tHoraRelatorio) + ')',
                      0,
                      '');
end;

function TFFiltroRelatorioCReceberCPagar.VerificarFiltro: Boolean;
var
  Msg: String;
begin
  Result := True;

  if dteDataInicio.Date = 0 then
  begin
    Msg := '- Informe a DATA DE IN�CIO do Per�odo.';
    Result := False;
  end;

  if dteDataFim.Date = 0 then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe a DATA DE FIM do Per�odo.'
    else
      Msg := #13#10 + '- Informe a DATA DE FIM do Per�odo.';

    Result := False;
  end;

  if dteDataFim.Date < dteDataInicio.Date then
  begin
    if Trim(Msg) = '' then
      Msg := '- A DATA DE FIM do Per�odo n�o pode inferior � DATA DE IN�CIO do mesmo.'
    else
      Msg := #13#10 + '- A DATA DE FIM do Per�odo n�o pode ser inferior � DATA DE IN�CIO do mesmo.';

    Result := False;
  end;

  if (not chbTipoAnalitico.Checked) and
    (not chbTipoSintetico.Checked) then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe o TIPO do Relat�rio de Contas a Receber X Contas a Pagar.'
    else
      Msg := #13#10 + '- Informe o TIPO do Relat�rio de Contas a Receber X Contas a Pagar.';

    Result := False;
  end;

  if not Result then
    Application.MessageBox(PChar(':: ATEN��O ::' + #13#10 + #13#10 + Msg),
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
end;

end.
