{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroFeriado.pas
  Descricao:   Formulario de cadastro de Feriados
  Author   :   Cristina
  Date:        14-jul-2017
  Last Update: 23-out-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroFeriado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.DBClient, Datasnap.Provider,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.DBCtrls, Vcl.Mask,
  System.DateUtils, JvExStdCtrls, JvCombobox, JvDBCombobox;

type
  TFCadastroFeriado = class(TFCadastroGeralPadrao)
    lblDescricao: TLabel;
    edtDescricao: TDBEdit;
    rgFlgAtivo: TDBRadioGroup;
    chbFlgFixo: TDBCheckBox;
    qryFeriado: TFDQuery;
    dspFeriado: TDataSetProvider;
    cdsFeriado: TClientDataSet;
    lblDia: TLabel;
    lblMes: TLabel;
    lblAno: TLabel;
    edtDia: TDBEdit;
    edtAno: TDBEdit;
    cdsFeriadoID_FERIADO: TIntegerField;
    cdsFeriadoDESCR_FERIADO: TStringField;
    cdsFeriadoDIA_FERIADO: TIntegerField;
    cdsFeriadoMES_FERIADO: TIntegerField;
    cdsFeriadoANO_FERIADO: TIntegerField;
    cdsFeriadoFLG_FIXO: TStringField;
    cdsFeriadoFLG_ATIVO: TStringField;
    cbMes: TJvDBComboBox;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure chbFlgFixoKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadastroFeriado: TFCadastroFeriado;

implementation

{$R *.dfm}

uses UDM, UGDM, UVariaveisGlobais, UBibliotecaSistema;

procedure TFCadastroFeriado.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroFeriado.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroFeriado.chbFlgFixoKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroFeriado.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtDescricao.MaxLength := 250;
  edtDia.MaxLength       := 2;
  edtAno.MaxLength       := 4;
end;

procedure TFCadastroFeriado.DesabilitarComponentes;
begin
  inherited;

  edtAno.ReadOnly     := True;
  rgFlgAtivo.ReadOnly := True;

  if vgOperacao = C then
    pnlDados.Enabled := False;
end;

procedure TFCadastroFeriado.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  if lPodeCancelar then
  begin
    cdsFeriado.Cancel;

    vgOperacao     := VAZIO;
    vgIdConsulta   := 0;
    vgOrigemFiltro := False;
  end
  else
    Action := caNone;
end;

procedure TFCadastroFeriado.FormCreate(Sender: TObject);
begin
  inherited;

  cdsFeriado.Close;
  cdsFeriado.Params.ParamByName('ID_FERIADO').AsInteger := vgIdConsulta;
  cdsFeriado.Open;

  if vgOperacao = I then
    cdsFeriado.Append;

  if vgOperacao = E then
    cdsFeriado.Edit;
end;

procedure TFCadastroFeriado.FormShow(Sender: TObject);
begin
  inherited;

  if vgOperacao = I then
  begin
    cdsFeriado.FieldByName('ANO_FERIADO').AsInteger := YearOf(Date);
    cdsFeriado.FieldByName('FLG_FIXO').AsString     := 'S';
    cdsFeriado.FieldByName('FLG_ATIVO').AsString    := 'S';
  end;

  if edtDescricao.CanFocus then
    edtDescricao.SetFocus;
end;

function TFCadastroFeriado.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroFeriado.GravarDadosGenerico(var Msg: String): Boolean;
begin
  Result := True;

  Msg := '';

  if vgOperacao = C then
    Exit;

  try
    if vgConGER.Connected then
      vgConGER.StartTransaction;

    if cdsFeriado.State in [dsInsert, dsEdit] then
    begin
      if vgOperacao = I then
        cdsFeriado.FieldByName('ID_FERIADO').AsInteger := BS.ProximoId('ID_FERIADO', 'FERIADO');

      cdsFeriado.Post;
      cdsFeriado.ApplyUpdates(0);
    end;

    if vgConGER.InTransaction then
      vgConGER.Commit;

    Msg := 'Feriado gravado com sucesso!';
  except
    if vgConGER.InTransaction then
      vgConGER.Rollback;

    Result := False;
    Msg := 'Erro na grava��o do Feriado.';
  end;
end;

procedure TFCadastroFeriado.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta de Feriado',
                          vgIdConsulta, 'FERIADO');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclus�o de Feriado',
                          vgIdConsulta, 'FERIADO');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO',
                                  'Por favor, indique o motivo da edi��o desse Feriado:',
                                  '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de Feriado';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          vgIdConsulta, 'FERIADO');
    end;
  end;
end;

procedure TFCadastroFeriado.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroFeriado.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

function TFCadastroFeriado.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if Trim(edtDescricao.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar a DESCRI��O DO FERIADO.';
    DadosMsgErro.Componente := edtDescricao;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtDia.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar o DIA DO FERIADO.';
    DadosMsgErro.Componente := edtDia;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(cbMes.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar o M�S DO FERIADO.';
    DadosMsgErro.Componente := cbMes;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroFeriado.VerificarLayoutAto(var QtdErros: Integer): Boolean;
begin
  //
end;

end.
