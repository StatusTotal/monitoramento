inherited FCadastroFeriado: TFCadastroFeriado
  Caption = 'Cadastro de Feriado'
  ClientHeight = 211
  ClientWidth = 476
  ExplicitWidth = 482
  ExplicitHeight = 240
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 470
    Height = 205
    ExplicitWidth = 470
    ExplicitHeight = 205
    inherited pnlDados: TPanel
      Width = 356
      Height = 199
      ExplicitWidth = 356
      ExplicitHeight = 199
      object lblDescricao: TLabel [0]
        Left = 5
        Top = 5
        Width = 51
        Height = 14
        Caption = 'Descri'#231#227'o'
      end
      object lblDia: TLabel [1]
        Left = 5
        Top = 49
        Width = 16
        Height = 14
        Caption = 'Dia'
      end
      object lblMes: TLabel [2]
        Left = 53
        Top = 49
        Width = 21
        Height = 14
        Caption = 'M'#234's'
      end
      object lblAno: TLabel [3]
        Left = 204
        Top = 49
        Width = 22
        Height = 14
        Caption = 'Ano'
      end
      inherited pnlMsgErro: TPanel
        Top = 138
        Width = 356
        TabOrder = 6
        ExplicitTop = 138
        ExplicitWidth = 356
        inherited lblMensagemErro: TLabel
          Width = 356
        end
        inherited lbMensagemErro: TListBox
          Width = 356
          OnDblClick = lbMensagemErroDblClick
          ExplicitWidth = 356
        end
      end
      object edtDescricao: TDBEdit
        Left = 5
        Top = 21
        Width = 348
        Height = 22
        CharCase = ecUpperCase
        Color = 16114127
        DataField = 'DESCR_FERIADO'
        DataSource = dsCadastro
        TabOrder = 0
        OnKeyPress = FormKeyPress
      end
      object rgFlgAtivo: TDBRadioGroup
        Left = 5
        Top = 93
        Width = 140
        Height = 41
        Caption = 'Situa'#231#227'o'
        Columns = 2
        DataField = 'FLG_ATIVO'
        DataSource = dsCadastro
        Items.Strings = (
          'Ativo'
          'Inativo')
        ReadOnly = True
        TabOrder = 5
        Values.Strings = (
          'S'
          'N')
      end
      object chbFlgFixo: TDBCheckBox
        Left = 270
        Top = 68
        Width = 40
        Height = 17
        Caption = 'Fixo'
        DataField = 'FLG_FIXO'
        DataSource = dsCadastro
        TabOrder = 4
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnKeyPress = chbFlgFixoKeyPress
      end
      object edtDia: TDBEdit
        Left = 5
        Top = 65
        Width = 42
        Height = 22
        Color = 16114127
        DataField = 'DIA_FERIADO'
        DataSource = dsCadastro
        TabOrder = 1
        OnKeyPress = FormKeyPress
      end
      object edtAno: TDBEdit
        Left = 204
        Top = 65
        Width = 60
        Height = 22
        Color = 16114127
        DataField = 'ANO_FERIADO'
        DataSource = dsCadastro
        ReadOnly = True
        TabOrder = 3
        OnKeyPress = FormKeyPress
      end
      object cbMes: TJvDBComboBox
        Left = 53
        Top = 65
        Width = 145
        Height = 22
        Style = csOwnerDrawFixed
        Color = 16114127
        DataField = 'MES_FERIADO'
        DataSource = dsCadastro
        Items.Strings = (
          'JANEIRO'
          'FEVEREIRO'
          'MAR'#199'O'
          'ABRIL'
          'MAIO'
          'JUNHO'
          'JULHO'
          'AGOSTO'
          'SETEMBRO'
          'OUTUBRO'
          'NOVEMBRO'
          'DEZEMBRO')
        TabOrder = 2
        Values.Strings = (
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9'
          '10'
          '11'
          '12')
        ListSettings.OutfilteredValueFont.Charset = DEFAULT_CHARSET
        ListSettings.OutfilteredValueFont.Color = clRed
        ListSettings.OutfilteredValueFont.Height = -11
        ListSettings.OutfilteredValueFont.Name = 'Tahoma'
        ListSettings.OutfilteredValueFont.Style = []
        OnKeyPress = FormKeyPress
      end
    end
    inherited pnlMenu: TPanel
      Height = 199
      ExplicitHeight = 199
      inherited btnOk: TJvTransparentButton
        Top = 109
        ExplicitTop = 109
      end
      inherited btnCancelar: TJvTransparentButton
        Top = 154
        ExplicitTop = 154
      end
    end
  end
  inherited dsCadastro: TDataSource
    DataSet = cdsFeriado
    Left = 422
    Top = 158
  end
  object qryFeriado: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT *'
      '  FROM FERIADO'
      ' WHERE ID_FERIADO = :ID_FERIADO')
    Left = 250
    Top = 158
    ParamData = <
      item
        Position = 1
        Name = 'ID_FERIADO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspFeriado: TDataSetProvider
    DataSet = qryFeriado
    Left = 306
    Top = 158
  end
  object cdsFeriado: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_FERIADO'
        ParamType = ptInput
      end>
    ProviderName = 'dspFeriado'
    Left = 362
    Top = 158
    object cdsFeriadoID_FERIADO: TIntegerField
      FieldName = 'ID_FERIADO'
      Required = True
    end
    object cdsFeriadoDESCR_FERIADO: TStringField
      FieldName = 'DESCR_FERIADO'
      Size = 250
    end
    object cdsFeriadoDIA_FERIADO: TIntegerField
      FieldName = 'DIA_FERIADO'
    end
    object cdsFeriadoMES_FERIADO: TIntegerField
      FieldName = 'MES_FERIADO'
    end
    object cdsFeriadoANO_FERIADO: TIntegerField
      FieldName = 'ANO_FERIADO'
    end
    object cdsFeriadoFLG_FIXO: TStringField
      FieldName = 'FLG_FIXO'
      FixedChar = True
      Size = 1
    end
    object cdsFeriadoFLG_ATIVO: TStringField
      FieldName = 'FLG_ATIVO'
      FixedChar = True
      Size = 1
    end
  end
end
