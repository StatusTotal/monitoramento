inherited FCadastroFechamentoComissao: TFCadastroFechamentoComissao
  Caption = 'Cadastro de Fechamento de Comiss'#227'o'
  ClientWidth = 861
  ExplicitWidth = 867
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 855
    ExplicitWidth = 855
    inherited pnlDados: TPanel
      Width = 741
      ExplicitWidth = 741
      object lblMes: TLabel [0]
        Left = 5
        Top = 5
        Width = 21
        Height = 14
        Caption = 'M'#234's'
      end
      object lblAno: TLabel [1]
        Left = 136
        Top = 5
        Width = 22
        Height = 14
        Caption = 'Ano'
      end
      object lblValorTotalItem: TLabel [2]
        Left = 447
        Top = 181
        Width = 80
        Height = 14
        Caption = 'Vlr. Total Item'
      end
      object lblPercentPrevista: TLabel [3]
        Left = 553
        Top = 181
        Width = 44
        Height = 14
        Caption = '% Prev.'
      end
      object lblValorPrevisto: TLabel [4]
        Left = 639
        Top = 181
        Width = 82
        Height = 14
        Caption = 'Vlr. Com. Prev.'
      end
      object lblPercentPaga: TLabel [5]
        Left = 447
        Top = 225
        Width = 58
        Height = 14
        Caption = '% A Pagar'
      end
      object lblValorPago: TLabel [6]
        Left = 533
        Top = 225
        Width = 96
        Height = 14
        Caption = 'Vlr. Com. A Pagar'
      end
      object btnConfirmarComissao: TJvTransparentButton [7]
        Left = 675
        Top = 285
        Width = 30
        Height = 22
        Hint = 'Confirmar inclus'#227'o/edi'#231#227'o da Comiss'#227'o selecionada'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        Transparent = False
        OnClick = btnConfirmarComissaoClick
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          1800000000000006000000000000000000000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF4F9EFDDE4ECD8E0ECD8E0EFDDE4FFF4F9FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFAE6E6E6E2
          E2E2E2E2E2E6E6E6FAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFF3FAA3B6AA329064008A47008D49008D49008A47329064A3B6AAFFF3
          FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBABABAB6161613A3A3A39
          39393939393A3A3A616161ABABABFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFECF62F8D5F009A5B34CDA539DDB23AE1B33AE1B239DDB234CDA5009A5A2F8D
          5FFFECF6FFFFFFFFFFFFFFFFFFFFFFFFF8F8F85E5E5E47474788888892929295
          95959595959292928888884747475E5E5EF8F8F8FFFFFFFFFFFFFFFFFFFFF5FE
          1A8B550DB5833EE0B600D79700D49600D49700D49700D49600D7973EE0B50DB5
          831A8B55FFF5FEFFFFFFFFFFFFFFFFFF53535367676797979770707070707070
          7070707070707070707070979797676767535353FFFFFFFFFFFFFFFFFF75A78C
          00A97233DFB200D29500D29600CB8700CE8F00D39900D39900D29800D29533DF
          B200A97275A78CFFFFFFFFFFFF8E8E8E5959599191916D6D6D70707061616168
          68687373737373737272726D6D6D9191915959598E8E8EFFFFFFFFF6FE008641
          45DDB800D19400CF9404CE93FFFFFFA1EDD700C78400D29A00D19900D19900D1
          9445DDB6008641FFF6FEFFFFFF3333339999996D6D6D6D6D6D717171FFFFFFCA
          CACA5C5C5C7272727272727272726D6D6D999999333333FFFFFFA7C6B605AA75
          19D8A700CF9605CC93FFFFFFFFFFFFFFFFFF9DECD600C68400D09A00D09900CF
          9819D8A606AB74A5C9B6B9B3B65E5E5E8181816E6E6E707070FFFFFFFFFFFFFF
          FFFFC8C8C85B5B5B7272727272727070708282825E5E5EB6B6B678B7962EC69C
          00D09800C789FFFFFFFFFFFFA0EAD3FFFFFFFFFFFF9DEBD600C48400CF9A00CE
          9900D0982BC49974BC979C93978080806F6F6F5F5F5FFFFFFFFFFFFFC7C7C7FF
          FFFFFFFFFFC7C7C75B5B5B7171717070706F6F6F7F7F7F9898987CB79738D0A8
          00CC9400C99270E2C4B5EFDF00C18004CA92FFFFFFFFFFFF9DEAD600C28400CD
          9900CE972CC49979BC9A9D95998B8B8B6C6C6C696969AFAFAFD4D4D45454546F
          6F6FFFFFFFFFFFFFC8C8C85959597171716F6F6F7F7F7F9A9A9A75B49250DDB9
          00C99300CB9900C89100C78F00CB9900C99404C893FFFFFFFFFFFFA8ECD900C7
          8F00CC982BC49874BC979990949E9E9E6969697070706767676464647070706A
          6A6A6E6E6EFFFFFFFFFFFFCDCDCD6464646E6E6E7F7F7F989898B1D2BF38C69B
          18D3A700C99700CA9900CA9900CA9900CA9900C79404C793FFFFFFEFFBFA00C2
          8A19D3A604AA73B3D8C4C3BEC18585857F7F7F6C6C6C6E6E6E6E6E6E6E6E6E6E
          6E6E6969696F6F6FFFFFFFF6F6F65E5E5E8080805C5C5CC5C5C5FFFFFF07985C
          64F5D700BF8D00C89900C89900C89900C89900C89900C69421CCA116CC9E00C5
          9245DBB800853FFFFFFFFFFFFF525252B5B5B56262626F6F6F6F6F6F6F6F6F6F
          6F6F6F6F6F6969697F7F7F7A7A7A666666989898313131FFFFFFFFFFFF81BB9C
          6FE1C12FE0B800BF8D00C79900C79900C79900C79900C79900C69700C39233D8
          B300A76D90C3A8FFFFFFFFFFFF9E9E9EACACAC9191916363636E6E6E6E6E6E6D
          6D6D6D6D6D6D6D6D6A6A6A6565658E8E8E545454AAAAAAFFFFFFFFFFFFFFFFFF
          319B66A9FFF043E8C300BA8900C09200C29400C29400C29400C2963ED9B708B2
          7E2B9C68FFFFFFFFFFFFFFFFFFFFFFFF666666DADADA9E9E9E5C5C5C66666668
          6868686868686868676767959595636363656565FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF41A2706ED5B49FFFF64DECCA43E2C042E1BF45E1C140D6B00094524AA8
          7AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF717171A5A5A5DADADAA6A6A69B
          9B9B9B9B9B9D9D9D9292923E3E3E797979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFCDE3D568B7913AAD7D33AC7B20A671109D6363B68FDCEBE1FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D7D790909076767672
          72726666665959598D8D8DE3E3E3FFFFFFFFFFFFFFFFFFFFFFFF}
        NumGlyphs = 2
      end
      object btnCancelarComissao: TJvTransparentButton [8]
        Left = 711
        Top = 285
        Width = 30
        Height = 22
        Hint = 'Cancelar inclus'#227'o/edi'#231#227'o da Comiss'#227'o selecionada'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        Transparent = False
        OnClick = btnCancelarComissaoClick
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          1800000000000006000000000000000000000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFF7F5EAE4E2D2E6E4D4E6E4D4E6E4D4E7E5D4E5E3D2F7F5EAFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1F1F1DCDCDCDEDEDEDF
          DFDFDFDFDFDFDFDFDDDDDDF1F1F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFDFAE4424EB71426BA1829B91627B71526B71223B70C1EB6424FB7FEFB
          E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F2F27474745A5A5A5C5C5C5A
          5A5A595959565656545454737373F3F3F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FDFAE4414EB44252D57184FF6D80FF6D80FF6D81FF6E81FF7387FF3142D0414D
          B4FEFBE4FFFFFFFFFFFFFFFFFFFFFFFFF2F2F2727272808080B3B3B3AEAEAEAE
          AEAEAFAFAFAFAFAFB4B4B4747474727272F3F3F3FFFFFFFFFFFFFFFFFFFCFAE4
          434FB54B5BD8667AFF5469FA5E72FB5E71FB5E71FB5E72FB5469FA697DFF3041
          D0414DB5FEFBE4FFFFFFFFFFFFF2F2F2737373878787A9A9A99B9B9BA1A1A1A0
          A0A0A0A0A0A1A1A19B9B9BACACAC737373737373F3F3F3FFFFFFFFFCEA434FB5
          5464D95D72FF3C54F8A5AFFC3F55F85B6FF95B6FF93D55F8A5AFFC3C54F86579
          FF2F41CF404DB4FFFFF1F5F5F57373738D8D8DA4A4A48C8C8CC9C9C98D8D8D9E
          9E9E9E9E9E8D8D8DC9C9C98C8C8CA9A9A9737373727272F6F6F68991D35564DA
          556BFF435BF6C2CAFCFFFFFFC8CEFC324BF6324BF6CCD2FCFFFFFFC3CBFC435A
          F65E74FF2639CC9AA2E8ABAAA28E8E8E9E9E9E8F8F8FDADADAFFFFFFDEDEDE85
          8585858585E1E1E1FFFFFFDBDBDB909090A4A4A46D6D6DADADAD8E95D45E70E7
          4E64F83B54F4E1E5FDFFFFFFFFFFFFA6B1FAA6B1FAFFFFFFFFFFFFDDE1FC3C55
          F45368F9384CDC9EA6E8AEADA49898989696968A8A8AEEEEEEFFFFFFFFFFFFCA
          CACACACACAFFFFFFFFFFFFEBEBEB8A8A8A9A9A9A7F7F7FAFAFAF8E95D26373E7
          495FF54C62F32540F1DADFFDFFFFFFFFFFFFFFFFFFFFFFFFDADFFD2641F14C62
          F34C63F73A4DDA9EA6E8ADACA49C9C9C9292929494947C7C7CE9E9E9FFFFFFFF
          FFFFFFFFFFFFFFFFE9E9E97C7C7C9494949595957F7F7FAFAFAF8D94D26878E7
          425BF1475EF04960F00324EBF2F3FEFFFFFFFFFFFFF2F3FE0324EB4960F0475E
          F0475FF43B4FD99EA6E8ACABA39D9D9D8D8D8D8F8F8F919191666666F7F7F7FF
          FFFFFFFFFFF7F7F76666669191918F8F8F919191808080AFAFAF8C93D26C7CE9
          3D57EF435CEE1C3AEBB2BCF7FFFFFFFFFFFFFFFFFFFFFFFFB2BCF71C3AEB435C
          EE425BF23C50DA9FA6E8ACABA3A2A2A28989898C8C8C757575D0D0D0FFFFFFFF
          FFFFFFFFFFFFFFFFD0D0D07575758C8C8C8E8E8E808080AFAFAF8991D16E7FEA
          3852ED2B46EBB9C2F8FFFFFFFFFFFFBFC7F9BFC7F9FFFFFFFFFFFFB9C2F82B46
          EB3D57EE3D51DB9CA4E8ABAAA1A3A3A38686867D7D7DD4D4D4FFFFFFFFFFFFD8
          D8D8D8D8D8FFFFFFFFFFFFD4D4D47D7D7D898989818181AEAEAE8991D78894E5
          2241EB203DE8E3E6FBFFFFFFD5DAFA0F2EE60F2EE6D5DAFAFFFFFFE3E6FB203D
          E83350F03D4DD0959EE6ADACA4B0B0B0787878767676EDEDEDFFFFFFE5E5E56B
          6B6B6B6B6BE5E5E5FFFFFFEDEDED7676768585857C7C7CB1B1B1FFFFFF5C66CB
          97A1E91A3AE90F2EE4ABB6F50E2DE4354FE8354FE80E2DE4ABB6F50E2EE42B49
          ED4D5DD35963C9FFFFFFFFFFFF8A8A8ABABABA7474746A6A6ACBCBCB6A6A6A83
          83838383836A6A6ACBCBCB6A6A6A7F7F7F868686878787FFFFFFFFFFFFFFFFFF
          5E69CBA4AEEC1333E52240E5314CE6304BE6304BE6314CE6223FE42341EA5767
          D65964C9FFFFFFFFFFFFFFFFFFFFFFFF8C8C8CC3C3C36D6D6D7777777F7F7F7E
          7E7E7E7E7E7F7F7F7676767878788D8D8D888888FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF606BCCABB2EE0E30E51C3CE61D3DE61E3DE7203FE71A3BE9606FD95B66
          CAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EC7C7C76A6A6A73737374
          7474757575767676747474949494898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF737CD3727CD56974D26470D2616CD05A67CF5560CC6D78D1FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9B9B9B9C9C9C95959593
          93938F8F8F8B8B8B878787979797FFFFFFFFFFFFFFFFFFFFFFFF}
        NumGlyphs = 2
      end
      object btnAbrirComissoes: TJvTransparentButton [9]
        Left = 202
        Top = 21
        Width = 30
        Height = 22
        Hint = 'Abre a lista de Comiss'#245'es'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        Transparent = False
        OnClick = btnAbrirComissoesClick
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          1800000000000006000000000000000000000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCCCC
          CCCCCCCCF4F4F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFCCCCCCCCCCCCCCCCCCF4F4F4FFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC4C80A74B7F
          A84981ACB0BDC6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFCCCCCC7B7B7B7B7B7B7C7C7CBCBCBCFFFFFFFFFFFFFFFFFF
          FFFFFFE5E5E5CECECECCCCCCCCCCCCCCCCCCCFCFCFCCCCCC4E81A71A9FFF29AD
          FF94CEEA4591C6FFFFFFFFFFFFFFFFFFFFFFFFE5E5E5CECECECCCCCCCCCCCCCC
          CCCCCFCFCFCCCCCC7C7C7C909090999999C2C2C2888888FFFFFFFFFFFFFFFFFF
          D1D1D188A4BB477FAA427BA9427BA8427BA84D82AB487FA7139BFF1DA0FF5BBF
          FFB9ECFF4191C5FFFFFFFFFFFFFFFFFFD1D1D1A2A2A27A7A7A77777776767676
          76767D7D7D7979798D8D8D929292B1B1B1DFDFDF868686FFFFFFFFFFFFD1D1D1
          5288B1589BC87BCCF687DDFF85DCFF86DCFF7BCAF45998C43D81B759BCFFB1E5
          FF6ED6FF4392C8FFFFFFFFFFFFD1D1D1838383929292BCBCBCC8C8C8C7C7C7C8
          C8C8BBBBBB9090907B7B7BAFAFAFDBDBDBBDBDBD888888FFFFFFE5E5E55289B4
          68B0DC99DBF7D4CBA0FACB7AFDD485FFDB89DEDDB29ADEF767ABD6548DB96BD4
          FF4190C4FFFFFFFFFFFFE5E5E5848484A5A5A5CCCCCCBCBCBCB4B4B4BDBDBDC1
          C1C1CCCCCCCDCDCDA1A1A1888888BBBBBB858585FFFFFFFFFFFF87A8C15B9FCC
          A0DEF5ECBA72F1C47AF3D089F6DB93FBE39BFFE89CFFE499A0DFF6599BC74288
          BCFFFFFFFFFFFFFFFFFFA5A5A5969696CECECEA7A7A7AFAFAFBBBBBBC4C4C4CB
          CBCBCECECECBCBCBCFCFCF929292818181FFFFFFFFFFFFFFFFFF4586B78DD5F9
          D2BF93EFC589EFD097F1D28EF5DD98FBEAA2FFF1A9FFE89CE1DFB28BD2F64A86
          B5FFFFFFFFFFFFFFFFFF808080C7C7C7B1B1B1B5B5B5BFBFBFBDBDBDC6C6C6D1
          D1D1D7D7D7CECECECDCDCDC4C4C4818181FFFFFFFFFFFFFFFFFF4084B8A4EBFF
          EAA95AF5DBB6F1D9ACF2D9A0F5DC96F8E59EFBEAA2FBE39BFFDA89A2E8FF3F84
          B7FFFFFFFFFFFFFFFFFF7E7E7ED7D7D7969696D1D1D1CCCCCCC7C7C7C5C5C5CC
          CCCCD1D1D1CBCBCBC1C1C1D5D5D57D7D7DFFFFFFFFFFFFFFFFFF3E86BAA8EFFF
          E6A556F7E8D1F3DFBBF3DCADF4DCA3F5DC97F5DD98F6DA93FBD284A7ECFF3E86
          B9FFFFFFFFFFFFFFFFFF7E7E7ED9D9D9929292E2E2E2D5D5D5CECECECACACAC5
          C5C5C6C6C6C3C3C3BBBBBBD8D8D87E7E7EFFFFFFFFFFFFFFFFFF3D88BEB1F3FF
          E49F51F8E8D3F6E6CDF4DFBAF3DCADF2D99FF1D28DF2CF88F7C979B0F0FF3D88
          BDFFFFFFFFFFFFFFFFFF808080DDDDDD8E8E8EE3E3E3DFDFDFD4D4D4CECECEC7
          C7C7BDBDBDBABABAB2B2B2DCDCDC7F7F7FFFFFFFFFFFFFFFFFFF448DC1A3E1FD
          D5B687F0CDA3FAF1E4F6E6CCF3DFBBF1D9ACF0D097EFC37AE0CFA0A2DFFC448D
          C1FFFFFFFFFFFFFFFFFF848484D3D3D3A9A9A9C3C3C3EEEEEEDFDFDFD5D5D5CC
          CCCCC0C0C0AFAFAFC0C0C0D2D2D2848484FFFFFFFFFFFFFFFFFF9EC5E165ADD9
          C3EDF3DFA057F0CCA2FAEDDCF7E7D0F4DDBBF0CA94EBBA70C3EFF764ACD99EC5
          E1FFFFFFFFFFFFFFFFFFC1C1C1A2A2A2DFDFDF8F8F8FC2C2C2E9E9E9E1E1E1D4
          D4D4BCBCBCA6A6A6E1E1E1A1A1A1C1C1C1FFFFFFFFFFFFFFFFFFFFFFFF539ACD
          81C4E8C9EFF4D8B787E29E50E3A355E6A859DDC393CAF1F680C3E7539ACDFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF919191B8B8B8E2E2E2AAAAAA8C8C8C91919195
          9595B5B5B5E3E3E3B7B7B7919191FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          529BCF68B1DDB2EAFECDFFFFCCFEFFCDFFFFB2E9FE68B1DD529BCFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF929292A5A5A5DCDCDCEBEBEBEAEAEAEB
          EBEBDBDBDBA5A5A5929292FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF9DC9E54395CE3A91CC3A91CC3A91CC4395CE9DC9E5FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C3C38B8B8B86868686868686
          86868B8B8BC3C3C3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        NumGlyphs = 2
      end
      object lblNomeFuncionario: TLabel [10]
        Left = 447
        Top = 49
        Width = 64
        Height = 14
        Caption = 'Colaborador'
      end
      object lblNomeSistema: TLabel [11]
        Left = 447
        Top = 93
        Width = 42
        Height = 14
        Caption = 'Sistema'
      end
      object lblDescricaoItem: TLabel [12]
        Left = 447
        Top = 137
        Width = 26
        Height = 14
        Caption = 'Item'
      end
      object lblDataComissao: TLabel [13]
        Left = 447
        Top = 269
        Width = 70
        Height = 14
        Caption = 'Dt. Comiss'#227'o'
      end
      object lblDataPagamento: TLabel [14]
        Left = 639
        Top = 226
        Width = 83
        Height = 14
        Caption = 'Dt. Pagamento'
      end
      object lblCancelada: TLabel [15]
        Left = 548
        Top = 333
        Width = 91
        Height = 18
        Caption = 'CANCELADA'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
      inherited pnlMsgErro: TPanel
        Width = 741
        TabOrder = 13
        ExplicitWidth = 741
        inherited lblMensagemErro: TLabel
          Width = 741
        end
        inherited lbMensagemErro: TListBox
          Width = 741
          OnDblClick = lbMensagemErroDblClick
          ExplicitWidth = 741
        end
      end
      object tvwComissoes: TTreeView
        Left = 5
        Top = 49
        Width = 436
        Height = 433
        Indent = 19
        ReadOnly = True
        TabOrder = 2
        OnClick = tvwComissoesClick
        OnCustomDrawItem = tvwComissoesCustomDrawItem
        OnKeyPress = FormKeyPress
      end
      object cedValorTotalItem: TJvDBCalcEdit
        Left = 447
        Top = 197
        Width = 100
        Height = 22
        Color = 16114127
        DisplayFormat = ',0.00'
        ReadOnly = True
        TabOrder = 6
        DecimalPlacesAlwaysShown = False
        OnKeyPress = FormKeyPress
        DataField = 'VR_TOTAL_ITEM'
        DataSource = dsComissoes
      end
      object cedPercentPrevista: TJvDBCalcEdit
        Left = 553
        Top = 197
        Width = 80
        Height = 22
        Color = 16114127
        DisplayFormat = ',0.00'
        ReadOnly = True
        TabOrder = 7
        DecimalPlacesAlwaysShown = False
        OnKeyPress = FormKeyPress
        DataField = 'PERCENT_COMISSAO_PREV'
        DataSource = dsComissoes
      end
      object cedValorPrevisto: TJvDBCalcEdit
        Left = 639
        Top = 197
        Width = 100
        Height = 22
        Color = 16114127
        DisplayFormat = ',0.00'
        ReadOnly = True
        TabOrder = 8
        DecimalPlacesAlwaysShown = False
        OnKeyPress = FormKeyPress
        DataField = 'VR_COMISSAO_PREV'
        DataSource = dsComissoes
      end
      object cedPercentPaga: TJvDBCalcEdit
        Left = 447
        Top = 241
        Width = 80
        Height = 22
        Color = 16114127
        DisplayFormat = ',0.00'
        TabOrder = 9
        DecimalPlacesAlwaysShown = False
        OnExit = cedPercentPagaExit
        OnKeyPress = FormKeyPress
        DataField = 'PERCENT_COMISSAO_PAGO'
        DataSource = dsComissoes
      end
      object cedValorPago: TJvDBCalcEdit
        Left = 533
        Top = 241
        Width = 100
        Height = 22
        Color = 16114127
        DisplayFormat = ',0.00'
        TabOrder = 10
        DecimalPlacesAlwaysShown = False
        OnExit = cedValorPagoExit
        OnKeyPress = FormKeyPress
        DataField = 'VR_COMISSAO_PAGO'
        DataSource = dsComissoes
      end
      object edtAno: TEdit
        Left = 136
        Top = 21
        Width = 60
        Height = 22
        Color = 16114127
        TabOrder = 1
        OnKeyPress = FormKeyPress
      end
      object cbMes: TComboBox
        Left = 5
        Top = 21
        Width = 125
        Height = 22
        Style = csOwnerDrawFixed
        Color = 16114127
        TabOrder = 0
        OnKeyPress = FormKeyPress
        Items.Strings = (
          'JANEIRO'
          'FEVEREIRO'
          'MAR'#199'O'
          'ABRIL'
          'MAIO'
          'JUNHO'
          'JULHO'
          'AGOSTO'
          'SETEMBRO'
          'OUTUBRO'
          'NOVEMBRO'
          'DEZEMBRO')
      end
      object edtNomeFuncionario: TDBEdit
        Left = 447
        Top = 65
        Width = 292
        Height = 22
        CharCase = ecUpperCase
        Color = 16114127
        DataField = 'NOME_FUNCIONARIO'
        DataSource = dsComissoes
        ReadOnly = True
        TabOrder = 3
      end
      object edtNomeSistema: TDBEdit
        Left = 447
        Top = 109
        Width = 292
        Height = 22
        CharCase = ecUpperCase
        Color = 16114127
        DataField = 'NOME_SISTEMA'
        DataSource = dsComissoes
        ReadOnly = True
        TabOrder = 4
      end
      object edtDescricaoItem: TDBEdit
        Left = 447
        Top = 153
        Width = 292
        Height = 22
        CharCase = ecUpperCase
        Color = 16114127
        DataField = 'DESCR_ITEM'
        DataSource = dsComissoes
        ReadOnly = True
        TabOrder = 5
      end
      object dteDataComissao: TJvDBDateEdit
        Left = 447
        Top = 285
        Width = 100
        Height = 22
        DataField = 'DATA_COMISSAO'
        DataSource = dsComissoes
        Color = 16114127
        ShowNullDate = False
        TabOrder = 12
        OnExit = dteDataComissaoExit
        OnKeyPress = dteDataComissaoKeyPress
      end
      object dteDataPagamento: TJvDBDateEdit
        Left = 639
        Top = 241
        Width = 100
        Height = 22
        DataField = 'DATA_PAGAMENTO'
        DataSource = dsComissoes
        Color = 16114127
        ShowNullDate = False
        TabOrder = 11
        OnKeyPress = FormKeyPress
      end
    end
    inherited pnlMenu: TPanel
      inherited btnDadosPrincipais: TJvTransparentButton
        Caption = 'Fechamento de Comiss'#245'es'
      end
      inherited btnCancelar: TJvTransparentButton
        Caption = '&Fechar'
        Spacing = 15
        ExplicitTop = 518
      end
    end
  end
  inherited dsCadastro: TDataSource
    DataSet = cdsComissao
    Left = 806
    Top = 502
  end
  object qryComissoes: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT C.*,'
      '       (CASE WHEN (PERCENT_COMISSAO_PREV = 0)'
      '             THEN 0'
      '             ELSE ((VR_TOTAL_ITEM * PERCENT_COMISSAO_PREV)/100)'
      '        END) AS VR_COMISSAO_PREV,'
      '       (CASE WHEN (COM.PERCENT_COMISSAO IS NULL)'
      '             THEN 0'
      '             ELSE COM.PERCENT_COMISSAO'
      '        END) AS PERCENT_COMISSAO_PAGO,'
      '       (CASE WHEN (COM.VR_COMISSAO IS NULL)'
      '             THEN 0'
      '             ELSE COM.VR_COMISSAO'
      '        END) AS VR_COMISSAO_PAGO,'
      '       COM.DATA_COMISSAO,'
      '       COM.FLG_CANCELADO,'
      '       COM.DATA_PAGAMENTO,'
      '       COM.PAG_ID_USUARIO'
      '  FROM (SELECT F.NOME_FUNCIONARIO,'
      '               F.ID_FUNCIONARIO,'
      '               F.CPF,'
      '               (CASE WHEN (L.ID_SISTEMA_FK IS NULL)'
      '                     THEN '#39'N'#195'O IDENTIFICADO'#39
      '                     ELSE UPPER(S.NOME_SISTEMA)'
      '                END) AS NOME_SISTEMA,'
      '               L.ID_SISTEMA_FK,'
      '               UPPER(I.DESCR_ITEM) AS DESCR_ITEM,'
      '               LD.ID_ITEM_FK,'
      '               SUM(LD.VR_TOTAL) VR_TOTAL_ITEM,'
      '               (CASE WHEN (FC.PERCENT_COMISSAO IS NULL)'
      '                     THEN 0'
      '                     ELSE FC.PERCENT_COMISSAO'
      '                END) AS PERCENT_COMISSAO_PREV,'
      '               L.ID_ORIGEM,'
      '               (CASE L.ID_NATUREZA_FK'
      '                     WHEN 4 THEN '#39'F'#39
      '                     WHEN 5 THEN '#39'A'#39
      '                     WHEN 6 THEN '#39'R'#39
      '                END) AS TIPO_ORIGEM,'
      '               LD.COD_ADICIONAL'
      '          FROM LANCAMENTO L'
      '         INNER JOIN LANCAMENTO_DET LD'
      '            ON LD.COD_LANCAMENTO_FK = L.COD_LANCAMENTO'
      '           AND LD.ANO_LANCAMENTO_FK = L.ANO_LANCAMENTO'
      '          LEFT JOIN USUARIO U'
      '            ON L.CAD_ID_USUARIO = U.ID_USUARIO'
      '          LEFT JOIN FUNCIONARIO F'
      '            ON U.ID_FUNCIONARIO_FK = F.ID_FUNCIONARIO'
      '          LEFT JOIN SISTEMA S'
      '            ON L.ID_SISTEMA_FK = S.ID_SISTEMA'
      '          LEFT JOIN ITEM I'
      '            ON LD.ID_ITEM_FK = I.ID_ITEM'
      '          LEFT JOIN FUNCIONARIO_COMISSAO FC'
      '            ON F.ID_FUNCIONARIO = FC.ID_FUNCIONARIO_FK'
      '           AND S.ID_SISTEMA = FC.ID_SISTEMA_FK'
      '           AND I.ID_ITEM = FC.ID_ITEM_FK'
      '         WHERE EXTRACT(MONTH FROM L.DATA_LANCAMENTO) = :MES'
      '           AND EXTRACT(YEAR FROM L.DATA_LANCAMENTO) = :ANO'
      '           AND L.CAD_ID_USUARIO IS NOT NULL'
      '           AND L.TIPO_LANCAMENTO = '#39'R'#39
      '           AND L.FLG_CANCELADO = '#39'N'#39
      '           AND L.FLG_FLUTUANTE = '#39'N'#39
      '           AND LD.FLG_CANCELADO = '#39'N'#39
      '           AND LD.VR_TOTAL > 0'
      '           AND F.NOME_FUNCIONARIO <> '#39'FUNCION'#193'RIO'#39
      '        GROUP BY NOME_FUNCIONARIO,'
      '                 NOME_SISTEMA,'
      '                 I.DESCR_ITEM,  '
      '                 F.ID_FUNCIONARIO,'
      '                 F.CPF,           '
      '                 L.ID_SISTEMA_FK,'
      '                 LD.ID_ITEM_FK,'
      '                 PERCENT_COMISSAO_PREV,'
      '                 L.ID_ORIGEM,'
      '                 TIPO_ORIGEM,'
      '                 LD.COD_ADICIONAL) AS C'
      '    LEFT JOIN COMISSAO COM'
      '      ON COM.ID_FUNCIONARIO_FK = C.ID_FUNCIONARIO'
      '     AND COM.ID_SISTEMA_FK = C.ID_SISTEMA_FK'
      '     AND COM.ID_ITEM_FK = C.ID_ITEM_FK'
      '     AND COM.COD_ADICIONAL = C.COD_ADICIONAL'
      '     AND COM.ID_ORIGEM = C.ID_ORIGEM'
      '     AND COM.TIPO_ORIGEM = C.TIPO_ORIGEM'
      'ORDER BY C.NOME_FUNCIONARIO,'
      '         C.NOME_SISTEMA,'
      '         C.DESCR_ITEM')
    Left = 40
    Top = 248
    ParamData = <
      item
        Position = 1
        Name = 'MES'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'ANO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspComissoes: TDataSetProvider
    DataSet = qryComissoes
    Left = 40
    Top = 296
  end
  object cdsComissoes: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'MES'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ANO'
        ParamType = ptInput
      end>
    ProviderName = 'dspComissoes'
    Left = 40
    Top = 344
    object cdsComissoesNOME_FUNCIONARIO: TStringField
      FieldName = 'NOME_FUNCIONARIO'
      Size = 250
    end
    object cdsComissoesID_FUNCIONARIO: TIntegerField
      FieldName = 'ID_FUNCIONARIO'
    end
    object cdsComissoesCPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
    object cdsComissoesNOME_SISTEMA: TStringField
      FieldName = 'NOME_SISTEMA'
      Size = 150
    end
    object cdsComissoesID_SISTEMA_FK: TIntegerField
      FieldName = 'ID_SISTEMA_FK'
    end
    object cdsComissoesDESCR_ITEM: TStringField
      FieldName = 'DESCR_ITEM'
      Size = 250
    end
    object cdsComissoesID_ITEM_FK: TIntegerField
      FieldName = 'ID_ITEM_FK'
    end
    object cdsComissoesVR_TOTAL_ITEM: TBCDField
      FieldName = 'VR_TOTAL_ITEM'
      Precision = 18
      Size = 2
    end
    object cdsComissoesPERCENT_COMISSAO_PREV: TBCDField
      FieldName = 'PERCENT_COMISSAO_PREV'
      Precision = 18
      Size = 2
    end
    object cdsComissoesID_ORIGEM: TIntegerField
      FieldName = 'ID_ORIGEM'
    end
    object cdsComissoesTIPO_ORIGEM: TStringField
      FieldName = 'TIPO_ORIGEM'
      FixedChar = True
      Size = 1
    end
    object cdsComissoesCOD_ADICIONAL: TIntegerField
      FieldName = 'COD_ADICIONAL'
    end
    object cdsComissoesVR_COMISSAO_PREV: TBCDField
      FieldName = 'VR_COMISSAO_PREV'
      Precision = 18
    end
    object cdsComissoesDATA_COMISSAO: TDateField
      FieldName = 'DATA_COMISSAO'
    end
    object cdsComissoesFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsComissoesDATA_PAGAMENTO: TDateField
      FieldName = 'DATA_PAGAMENTO'
    end
    object cdsComissoesPAG_ID_USUARIO: TIntegerField
      FieldName = 'PAG_ID_USUARIO'
    end
    object cdsComissoesPERCENT_COMISSAO_PAGO: TBCDField
      FieldName = 'PERCENT_COMISSAO_PAGO'
      Precision = 18
      Size = 2
    end
    object cdsComissoesVR_COMISSAO_PAGO: TBCDField
      FieldName = 'VR_COMISSAO_PAGO'
      Precision = 18
      Size = 2
    end
  end
  object dsComissoes: TDataSource
    DataSet = cdsComissoes
    Left = 40
    Top = 392
  end
  object qryComissao: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM COMISSAO'
      ' WHERE ID_FUNCIONARIO_FK = :ID_FUNCIONARIO'
      '   AND ID_SISTEMA_FK = :ID_SISTEMA'
      '   AND ID_ITEM_FK = :ID_ITEM'
      '   AND COD_ADICIONAL = :COD_ADICIONAL'
      '   AND ID_ORIGEM = :ID_ORIGEM'
      '   AND TIPO_ORIGEM = :TIPO_ORIGEM'
      '   AND EXTRACT(MONTH FROM DATA_COMISSAO) = :MES'
      '   AND EXTRACT(YEAR FROM DATA_COMISSAO) = :ANO')
    Left = 618
    Top = 502
    ParamData = <
      item
        Position = 1
        Name = 'ID_FUNCIONARIO'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'ID_SISTEMA'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 3
        Name = 'ID_ITEM'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 4
        Name = 'COD_ADICIONAL'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'ID_ORIGEM'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 6
        Name = 'TIPO_ORIGEM'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Position = 7
        Name = 'MES'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 8
        Name = 'ANO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspComissao: TDataSetProvider
    DataSet = qryComissao
    Left = 682
    Top = 502
  end
  object cdsComissao: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_FUNCIONARIO'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID_SISTEMA'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID_ITEM'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'COD_ADICIONAL'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID_ORIGEM'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'TIPO_ORIGEM'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'MES'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ANO'
        ParamType = ptInput
      end>
    ProviderName = 'dspComissao'
    Left = 746
    Top = 502
    object cdsComissaoID_COMISSAO: TIntegerField
      FieldName = 'ID_COMISSAO'
      Required = True
    end
    object cdsComissaoID_FUNCIONARIO_FK: TIntegerField
      FieldName = 'ID_FUNCIONARIO_FK'
    end
    object cdsComissaoID_SISTEMA_FK: TIntegerField
      FieldName = 'ID_SISTEMA_FK'
    end
    object cdsComissaoCOD_ADICIONAL: TIntegerField
      FieldName = 'COD_ADICIONAL'
    end
    object cdsComissaoID_ORIGEM: TIntegerField
      FieldName = 'ID_ORIGEM'
    end
    object cdsComissaoTIPO_ORIGEM: TStringField
      FieldName = 'TIPO_ORIGEM'
      FixedChar = True
      Size = 1
    end
    object cdsComissaoPERCENT_COMISSAO: TBCDField
      FieldName = 'PERCENT_COMISSAO'
      Precision = 18
      Size = 2
    end
    object cdsComissaoVR_COMISSAO: TBCDField
      FieldName = 'VR_COMISSAO'
      Precision = 18
      Size = 2
    end
    object cdsComissaoID_FECHAMENTO_SALARIO_FK: TIntegerField
      FieldName = 'ID_FECHAMENTO_SALARIO_FK'
    end
    object cdsComissaoDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsComissaoCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsComissaoDATA_PAGAMENTO: TDateField
      FieldName = 'DATA_PAGAMENTO'
    end
    object cdsComissaoPAG_ID_USUARIO: TIntegerField
      FieldName = 'PAG_ID_USUARIO'
    end
    object cdsComissaoFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsComissaoDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsComissaoCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsComissaoDATA_COMISSAO: TDateField
      FieldName = 'DATA_COMISSAO'
    end
    object cdsComissaoID_ITEM_FK: TIntegerField
      FieldName = 'ID_ITEM_FK'
    end
  end
end
