{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroFeriado.pas
  Descricao:   Filtro de Feriados
  Author   :   Cristina
  Date:        14-jul-2017
  Last Update: 08-nov-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroFeriado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.StdCtrls, System.DateUtils;

type
  TFFiltroFeriado = class(TFFiltroSimplesPadrao)
    edtDescrFeriado: TEdit;
    lblDescrFeriado: TLabel;
    qryGridPaiPadraoID: TIntegerField;
    qryGridPaiPadraoDESCR_FERIADO: TStringField;
    qryGridPaiPadraoDIA_FERIADO: TIntegerField;
    qryGridPaiPadraoMES_FERIADO: TIntegerField;
    qryGridPaiPadraoANO_FERIADO: TIntegerField;
    qryGridPaiPadraoFLG_FIXO: TStringField;
    qryGridPaiPadraoFLG_ATIVO: TStringField;
    lblAnoFeriado: TLabel;
    edtAnoFeriado: TEdit;
    qryGridPaiPadraoDATA_FERIADO: TStringField;
    procedure edtDescrFeriadoKeyPress(Sender: TObject; var Key: Char);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtAnoFeriadoKeyPress(Sender: TObject; var Key: Char);
    procedure qryGridPaiPadraoCalcFields(DataSet: TDataSet);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroFeriado: TFFiltroFeriado;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UCadastroFeriado;

{ TFFiltroFeriado }

procedure TFFiltroFeriado.btnEditarClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao     := E;
  vgIdConsulta   := qryGridPaiPadrao.FieldByName('ID').AsInteger;
  vgOrigemFiltro := True;

  dmGerencial.CriarForm(TFCadastroFeriado, FCadastroFeriado);

  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroFeriado.btnExcluirClick(Sender: TObject);
var
  qryExclusao: TFDQuery;
begin
  inherited;

  if lPodeExcluir then
  begin
    qryExclusao := dmGerencial.CriarFDQuery(nil, vgConGER);

    try
      if vgConGER.Connected then
        vgConGER.StartTransaction;

      with qryExclusao, SQL do
      begin
        Close;
        Clear;
        Text := 'UPDATE FERIADO ' +
                '   SET FLG_ATIVO = :FLG_ATIVO ' +
                ' WHERE ID_FERIADO = :ID_FERIADO';
        Params.ParamByName('FLG_ATIVO').Value  := 'N';
        Params.ParamByName('ID_FERIADO').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;
      end;

      if vgConGER.InTransaction then
        vgConGER.Commit;

      GravarLog;
      Application.MessageBox('Feriado exclu�do com sucesso!', 'Aviso', MB_OK)
    except
      if vgConGER.InTransaction then
        vgConGER.Rollback;

      Application.MessageBox('Erro na exclus�o do Feriado.', 'Erro', MB_OK + MB_ICONERROR)
    end;

    FreeAndNil(qryExclusao);

    btnFiltrar.Click;
  end;
end;

procedure TFFiltroFeriado.btnFiltrarClick(Sender: TObject);
begin
  with qryGridPaiPadrao, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_FERIADO AS ID, ' +
            '       DESCR_FERIADO, ' +
            '       DIA_FERIADO, ' +
            '       MES_FERIADO, ' +
            '       ANO_FERIADO, ' +
            '       FLG_FIXO, ' +
            '       FLG_ATIVO ' +
            '  FROM FERIADO ' +
            ' WHERE FLG_ATIVO = ' + QuotedStr('S') +
            '   AND ANO_FERIADO = (CASE WHEN (:ANO_FER1 = 0) THEN ANO_FERIADO ELSE :ANO_FER2 END)';

    if Trim(edtDescrFeriado.Text) <> '' then
    begin
      Add('   AND CAST(UPPER(DESCR_FERIADO) AS VARCHAR(250)) LIKE CAST(:DESCR_FER AS VARCHAR(250))');
      Params.ParamByName('DESCR_FER').Value := '%' + Trim(UpperCase(edtDescrFeriado.Text)) + '%';
    end;

    if Trim(edtAnoFeriado.Text) = '' then
    begin
      Params.ParamByName('ANO_FER1').Value := 0;
      Params.ParamByName('ANO_FER2').Value := 0;
    end
    else
    begin
      Params.ParamByName('ANO_FER1').Value := StrToInt(edtAnoFeriado.Text);
      Params.ParamByName('ANO_FER2').Value := StrToInt(edtAnoFeriado.Text);
    end;

    Add('ORDER BY ANO_FERIADO, ' +
        '         MES_FERIADO, ' +
        '         DIA_FERIADO');

    Open;
  end;

  inherited;
end;

procedure TFFiltroFeriado.btnIncluirClick(Sender: TObject);
begin
  inherited;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := True;

  dmGerencial.CriarForm(TFCadastroFeriado, FCadastroFeriado);

  btnFiltrar.Click;
end;

procedure TFFiltroFeriado.btnLimparClick(Sender: TObject);
begin
  edtDescrFeriado.Clear;

  inherited;

  if edtDescrFeriado.CanFocus then
    edtDescrFeriado.SetFocus;
end;

procedure TFFiltroFeriado.dbgGridPaiPadraoDblClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao     := C;
  vgIdConsulta   := qryGridPaiPadrao.FieldByName('ID').AsInteger;
  vgOrigemFiltro := True;

  dmGerencial.CriarForm(TFCadastroFeriado, FCadastroFeriado);

  DefinirPosicaoGrid;
end;

procedure TFFiltroFeriado.edtAnoFeriadoKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroFeriado.edtDescrFeriadoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroFeriado.FormShow(Sender: TObject);
begin
  btnImprimir.Visible := False;

  edtAnoFeriado.Text := IntToStr(YearOf(Date));

  with qryGridPaiPadrao, SQL do
  begin
    Close;
    Clear;

    Text := 'SELECT ID_FERIADO AS ID, ' +
            '       DESCR_FERIADO, ' +
            '       DIA_FERIADO, ' +
            '       MES_FERIADO, ' +
            '       ANO_FERIADO, ' +
            '       FLG_FIXO, ' +
            '       FLG_ATIVO ' +
            '  FROM FERIADO ' +
            ' WHERE FLG_ATIVO = ' + QuotedStr('S') +
            '   AND ANO_FERIADO = (CASE WHEN (:ANO_FER1 = 0) THEN ANO_FERIADO ELSE :ANO_FER2 END)' +
            ' ORDER BY ANO_FERIADO, ' +
            '          MES_FERIADO, ' +
            '          DIA_FERIADO';

    if Trim(edtAnoFeriado.Text) = '' then
    begin
      Params.ParamByName('ANO_FER1').Value := 0;
      Params.ParamByName('ANO_FER2').Value := 0;
    end
    else
    begin
      Params.ParamByName('ANO_FER1').Value := StrToInt(edtAnoFeriado.Text);
      Params.ParamByName('ANO_FER2').Value := StrToInt(edtAnoFeriado.Text);
    end;

    Open;
  end;

  ShowScrollBar(dbgGridPaiPadrao.Handle, SB_HORZ, False);

  inherited;

  if edtDescrFeriado.CanFocus then
    edtDescrFeriado.SetFocus;
end;

procedure TFFiltroFeriado.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  if vgOperacao = X then  //EXCLUSAO
  begin
    repeat
      sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da exclus�o desse Feriado:', '');
    until sObservacao <> '';

    BS.GravarUsuarioLog(2, '', sObservacao,
                        qryGridPaiPadrao.FieldByName('ID').AsInteger, 'FERIADO');
  end;
end;

function TFFiltroFeriado.PodeExcluir(var Msg: String): Boolean;
begin
  inherited;

  Result := True;
end;

procedure TFFiltroFeriado.qryGridPaiPadraoCalcFields(DataSet: TDataSet);
begin
  inherited;

  qryGridPaiPadrao.FieldByName('DATA_FERIADO').AsString := dmGerencial.CompletaString(qryGridPaiPadrao.FieldByName('DIA_FERIADO').AsString, '0', 'E', 2) +
                                                           '/' +
                                                           dmGerencial.CompletaString(qryGridPaiPadrao.FieldByName('MES_FERIADO').AsString, '0', 'E', 2) +
                                                           '/' +
                                                           qryGridPaiPadrao.FieldByName('ANO_FERIADO').AsString;
end;

procedure TFFiltroFeriado.VerificarPermissoes;
begin
  inherited;

  btnIncluir.Enabled  := vgPrm_IncFer;
  btnEditar.Enabled   := vgPrm_EdFer and (qryGridPaiPadrao.RecordCount > 0);
  btnExcluir.Enabled  := vgPrm_ExcFer and (qryGridPaiPadrao.RecordCount > 0);
  btnImprimir.Enabled := vgPrm_ImpFer and (qryGridPaiPadrao.RecordCount > 0);
end;

end.
