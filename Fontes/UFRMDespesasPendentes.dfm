object FFRMLancamentosPendentes: TFFRMLancamentosPendentes
  Left = 0
  Top = 0
  Width = 1023
  Height = 306
  Color = 1006847
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentBackground = False
  ParentColor = False
  ParentFont = False
  TabOrder = 0
  OnResize = FrameResize
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 1017
    Height = 300
    Align = alClient
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 0
    OnResize = Panel1Resize
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 1017
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object lblPeriodoLanc: TLabel
        Left = 5
        Top = 19
        Width = 45
        Height = 14
        Caption = 'Per'#237'odo:'
      end
      object Label2: TLabel
        Left = 160
        Top = 19
        Width = 6
        Height = 14
        Caption = 'a'
      end
      object rgTipoLanc: TRadioGroup
        Left = 276
        Top = 0
        Width = 229
        Height = 38
        Caption = 'Tipo'
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Todos'
          'Receita'
          'Despesa')
        TabOrder = 0
        OnClick = rgTipoLancClick
      end
      object dtePeriodoIni: TJvDateEdit
        Left = 56
        Top = 16
        Width = 100
        Height = 22
        Color = clInfoBk
        ShowNullDate = False
        TabOrder = 1
        OnKeyPress = dtePeriodoIniKeyPress
      end
      object dtePeriodoFim: TJvDateEdit
        Left = 170
        Top = 16
        Width = 100
        Height = 22
        Color = clInfoBk
        ShowNullDate = False
        TabOrder = 2
        OnKeyPress = dtePeriodoFimKeyPress
      end
      object chbSelecionarTodos: TCheckBox
        Left = 511
        Top = 18
        Width = 156
        Height = 17
        BiDiMode = bdLeftToRight
        Caption = 'Marcar Todos'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBiDiMode = False
        ParentFont = False
        TabOrder = 3
        OnClick = chbSelecionarTodosClick
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 262
      Width = 1017
      Height = 38
      Align = alBottom
      BevelOuter = bvNone
      Color = clWhite
      ParentBackground = False
      TabOrder = 1
      object btnFechar: TJvTransparentButton
        Left = 928
        Top = 6
        Width = 83
        Height = 26
        Hint = 'Fechar tela de aviso'
        Caption = 'Fechar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGray
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        Transparent = False
        OnClick = btnFecharClick
      end
      object lblLegendaSituacaoLanc: TLabel
        Left = 7
        Top = 13
        Width = 139
        Height = 14
        Caption = 'Situa'#231#227'o do Lan'#231'amento:'
      end
      object spLegendaVenceHoje: TShape
        Left = 162
        Top = 16
        Width = 10
        Height = 10
        Brush.Color = clGreen
      end
      object lblLegendaVenceHoje: TLabel
        Left = 175
        Top = 13
        Width = 64
        Height = 14
        Caption = 'Vence Hoje'
      end
      object spLegendaVencido: TShape
        Left = 255
        Top = 16
        Width = 10
        Height = 10
        Brush.Color = clMaroon
      end
      object lblLegendaVencido: TLabel
        Left = 268
        Top = 13
        Width = 44
        Height = 14
        Caption = 'Vencido'
      end
      object btnEditarLanc: TJvTransparentButton
        Left = 673
        Top = 6
        Width = 83
        Height = 26
        Hint = 
          'Abre tela de cadastro de Lan'#231'amento para que o mesmo possa ser a' +
          'lterado'
        Caption = 'Editar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGray
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        Spacing = 14
        TextAlign = ttaRight
        Transparent = False
        OnClick = btnEditarLancClick
        Glyph.Data = {
          96090000424D9609000000000000360000002800000028000000140000000100
          1800000000006009000000000000000000000000000000000000F5F5F4EFEDEC
          FDFCFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F3F3EDEDEDFCFCFCFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCDD2D47F939FC2C4C6E6E3E0FBF8
          F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFCBCBCB8F8F8FC4C4C4E3E3E3F9F9F9FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF7FCFF6066685386A56B9BBB8C99A5F1E9E2FFFFFEFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFF9F9F96363637E7E7E949494999999EAEAEAFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFA1C8DD85DAFF7DCAFA59A4DE5582A9E3DCD7FFFFFEFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFC1C1C1C7C7C7BFBFBF9D9D9D7F7F7FDDDDDDFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E8EC
          5CADD8DCF5FF39ABFF038EFB6183A2F4E8DEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E7E79E9E9EF0
          F0F0A0A0A0828282828282E9E9E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE76AAD07DD9FFBDE5
          FF1196FF0695FF537DA2E6DED7FFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA4A4A4C4C4C4DFDFDF8B8B8B
          8888887B7B7BDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F5F93999DA72D9FFB3DDFF1A9BFF08
          92FD6488A8E7DFD8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFF7F7F78D8D8DBFBFBFDADADA9090908686868686
          86DFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFDF7F73992D57CE2FFB8E1FF1397FF0891FD537EA2
          F3E7DDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFF9F9F9898989C7C7C7DCDCDC8E8E8E8585857B7B7BE8E8E8FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFB3593D671D7FFB8E1FF1A9BFF0694FF6184A2E3DCD7FFFF
          FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFF898989BFBFBFDCDCDC919191898989818181DDDDDDFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFF8F3F63593D77CE1FFB3DEFF1196FF0A91FA6083A2F4E8DEFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F6
          898989C7C7C7DADADA8C8C8C858585828282E9E9E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB39
          93D66FD3FFC7EAFF1096FF0695FF537DA2E6DED7FFFFFEFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8A8A8ABCBC
          BCE5E5E58B8B8B8888887B7B7BDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDF7F73990D570D1FD
          B4DEFF1A9BFF0892FD6488A8E7DFD8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFA898989BCBCBCDADADA90
          9090868686868686DFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDF8F8358AD07DE3FFB8E1FF1397
          FF0891FD537EA2F3E7DDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFA848484C9C9C9DCDCDC8E8E8E858585
          7B7B7BE8E8E8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB3593D671D7FFB8E1FF1A9BFF0694FF61
          84A3E3DCD7FFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF898989BFBFBFDCDCDC919191898989818181DDDD
          DDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFF8F3F63593D77CE1FFB2DDFF1196FF0692FD4E86B7EFE9E4
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFF6F6F6898989C7C7C7D9D9D98C8C8C858585838383E9E9E9FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFB3992D572DAFFC4E9FF0F96F87878777F7C74DCE0DAFDFEFDFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF8A8A8AC0C0C0E4E4E48686867575757A7A7ADCDCDCFEFEFEFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDF6
          F73B99DB66D4FFE8DCD6A59F986D7365A485C1E4E6E3FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F98E8E8E
          B9B9B9DDDDDD9E9E9E6B6B6BAAAAAAE7E7E7FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F7FE8E8B91FE
          F9F3E3EBE2BB89B8B36EB9C3B2DCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F98D8D8DF7F7F7E5E5
          E5B0B0B0A7A7A7CFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9B9BDCBADBF1D4ED
          B47DC0DDD2ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9C9C9CD4D4D4E9E9E9ADADADE5
          E5E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDF8FFCE9BDABC87CCE4D8EEFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFDFDFDC8C8C8B7B7B7E5E5E5FFFFFF}
        NumGlyphs = 2
      end
      object btnRealizarPagto: TJvTransparentButton
        Left = 762
        Top = 6
        Width = 160
        Height = 26
        Hint = 
          'Abre tela de cadastro de Lan'#231'amento para que seja realizado o pa' +
          'gamento da Parcela'
        Caption = 'Realizar Pagamento'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGray
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        Spacing = 16
        TextAlign = ttaRight
        Transparent = False
        OnClick = btnRealizarPagtoClick
        Glyph.Data = {
          96090000424D9609000000000000360000002800000028000000140000000100
          1800000000006009000000000000000000000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
          FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFFFFFFFFFF
          FFFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
          FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFFFFFFF4F1F2D5CED2
          CEC7CACFC7CBCFC7CBCFC7CBCFC7CBCFC7CBCFC7CBCFC7CBCFC7CBCFC7CBCFC7
          CBCFC7CBCFC7CBCFC7CBCFC7CBCEC7CAD4CDD1FAF8F9F2F2F2D1D1D1CACACACB
          CBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCB
          CBCBCBCBCBCBCBCBCBCACACAD1D1D1F2F2F2C3D5CC77BB9887C9A786C8A685C7
          A485C7A585C7A585C7A585C7A586C8A686C8A685C7A585C7A585C7A585C7A585
          C7A486C8A687C9A775BA96D4E8DDCBC9CA999999A8A8A8A7A7A7A6A6A6A6A6A6
          A6A6A6A6A6A6A7A7A7A7A7A7A7A7A7A7A7A7A6A6A6A6A6A6A6A6A6A6A6A6A7A7
          A7A8A8A8999999CCCCCCBAD0C5AEE1C478B8967FBC9C83C0A080BF9D80BF9D81
          BF9F84C1A180BF9D80BF9D84C1A181BF9F80BF9D80BF9D83C0A07FBC9C78B896
          ACE0C3CAE4D7C5C2C3C7C7C79797979D9D9DA1A1A19F9F9F9F9F9F9F9F9FA3A3
          A39F9F9F9F9F9FA3A3A39F9F9F9F9F9F9F9F9FA1A1A19D9D9D979797C7C7C7C5
          C5C5BBD0C5A9DDC23F875C66A681C4F3DBBAEAD1B8E9D0C2F0D88BC7A881BE9E
          81BE9E8BC7A8C2F0D8B8E9D0BAEAD1C4F3DB66A6813F875CA7DBC0C9E3D5C5C2
          C3C3C3C3626262848484DBDBDBD2D2D2D1D1D1D9D9D9A9A9A99F9F9F9F9F9FA9
          A9A9D9D9D9D1D1D1D2D2D2DBDBDB848484626262C3C3C3C5C5C5B9CFC3A6D9BF
          7EBC9CB2E5CCA8DDC2A0D7BBAEE1C893CDAF7DBB9A99D0B499D0B47DBB9A93CD
          AFAEE1C8A0D7BBA8DDC2B2E5CC7EBC9CA5D7BDC8E2D4C4C1C2C0C0C09D9D9DCB
          CBCBC2C2C2BCBCBCC8C8C8B1B1B19B9B9BB5B5B5B5B5B59B9B9BB1B1B1C8C8C8
          BCBCBCC2C2C2CBCBCB9D9D9DC0C0C0C4C4C4B7CEC2A8D7BF78B997B2E8CF76B4
          944E936D9FD8BC7FBE9E89C5A68EC8AB8EC8AB89C5A67FBE9E9FD8BC4E936D76
          B494B2E8CF78B997A6D5BDC7E1D2C2BFC0BFBFBF989898CECECE959595707070
          BCBCBC9E9E9EA7A7A7ABABABABABABA7A7A79E9E9EBCBCBC707070959595CECE
          CE989898BFBFBFC2C2C2B6CDBFAAD7C271B490A9E3C986C2A368A782A0DABF7A
          B99680BE9F86C3A486C3A480BE9F7AB996A0DABF68A78286C2A3A9E3C971B490
          A8D5C0C6DFD1C1BEBFC1C1C1929292C6C6C6A4A4A4868686BEBEBE989898A0A0
          A0A4A4A4A4A4A4A0A0A0989898BEBEBE868686A4A4A4C6C6C6929292C1C1C1C1
          C1C1B4CCBEAFD8C46EB18EA1DCC39FDAC1A2DDC49FDAC186C2A56BAC8882C2A5
          82C2A56BAC8886C2A59FDAC1A2DDC49FDAC1A1DCC36EB18EADD6C2C4DFCFC0BD
          BEC3C3C38F8F8FBFBFBFBEBEBEC1C1C1BEBEBEA4A4A48A8A8AA2A2A2A2A2A28A
          8A8AA4A4A4BEBEBEC1C1C1BEBEBEBFBFBF8F8F8FC3C3C3C0C0C0B2CABCB7DDCC
          4F99716CAF8CA2E0C99BDAC19BDAC2A5E2CC70B29061A47E61A47E70B290A5E2
          CC9BDAC29BDAC1A2E0C96CAF8C4F9971B5DBCAC1DECCBDBABCCBCBCB7373738D
          8D8DC3C3C3BCBCBCBCBCBCC5C5C5929292828282828282929292C5C5C5BCBCBC
          BCBCBCC3C3C38D8D8D737373CBCBCBBDBDBDB0C9BABEDED0649F7A6EA4836CA4
          816CA3806BA3806DA4816FA7856CA3806CA3806FA7856DA4816BA3806CA3806C
          A4816EA483649F7ABDDDCFBEDAC9BCB9BBCFCFCF808080878787878787868686
          8686868787878A8A8A8686868686868A8A8A8787878686868686868787878787
          87808080CFCFCFBCBCBCC6D8CD8BC4A8C4F0E1BEEADABDEADABDEADABDEADABD
          EADABEEADABEEBDBBEEBDBBEEADABDEADABDEADABDEADABDEADABEEADAC4F0E1
          8AC4A7CBDFD2CFCDCEA7A7A7DBDBDBD5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5
          D5D6D6D6D6D6D6D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5DBDBDBA7A7A7CF
          CFCFFFFFFFBBD2C47EBC9786C4A086C39F86C39F86C39F86C39F86C39F86C39F
          86C39F86C39F86C39F86C39F86C39F86C39F86C4A07EBC97BBD2C4FFFFFFFFFF
          FFC6C6C69C9C9CA3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3
          A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A3A39C9C9CC6C6C6FFFFFFFFFFFFFFFFFF
          DBE7DFD4E2D9D5E3DAD5E3DAD5E3DAD5E3DAD5E3DAD5E3DAD5E3DAD5E3DAD5E3
          DAD5E3DAD5E3DAD5E3DAD4E2D9DBE7DFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E1DB
          DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
          DBDBDBDBDBDBDBDBDBE1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        NumGlyphs = 2
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 41
      Width = 1017
      Height = 221
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      object dbgLancPend: TJvDBUltimGrid
        Left = 0
        Top = 0
        Width = 1017
        Height = 221
        Align = alClient
        DataSource = dmPrincipal.dsDespesas
        Options = [dgTitles, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnCellClick = dbgLancPendCellClick
        OnDrawColumnCell = dbgLancPendDrawColumnCell
        OnTitleClick = dbgLancPendTitleClick
        MinColumnWidth = 26
        AutoSizeColumnIndex = 0
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 18
        TitleRowHeight = 18
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'SELECIONADO'
            Title.Alignment = taCenter
            Title.Caption = 'Sel.'
            Width = 26
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA_VENCIMENTO'
            Title.Caption = 'Dt. Venc.'
            Width = 84
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'NUM_RECIBO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Title.Alignment = taCenter
            Title.Caption = 'Recibo'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = [fsBold]
            Width = 45
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'LANC'
            Title.Alignment = taCenter
            Title.Caption = 'Lan'#231'amento'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCR_NATUREZA'
            Title.Caption = 'Natureza'
            Width = 160
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCR_CATEGORIA_DESPREC'
            Title.Caption = 'Categoria'
            Width = 160
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCR_SUBCATEGORIA_DESPREC'
            Title.Caption = 'Subcategoria'
            Width = 160
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'NUM_PARCELA'
            Title.Alignment = taCenter
            Title.Caption = 'Parc.'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VR_PARCELA_PREV'
            Title.Alignment = taRightJustify
            Title.Caption = 'Valor'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCR_FORMAPAGAMENTO'
            Title.Caption = 'Forma Pagto.'
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TIPO_LANCAMENTO'
            Title.Alignment = taCenter
            Title.Caption = 'Tipo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COD_LANCAMENTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ANO_LANCAMENTO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_STATUS'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_NATUREZA_FK'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_LANCAMENTO_PARC'
            Visible = False
          end>
      end
    end
  end
end
