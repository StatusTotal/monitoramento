{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UListaFuncionarios.pas
  Descricao:   Lista Simples de Funcionarios Ativos
  Author   :   Cristina
  Date:        17-jun-2016
  Last Update: 30-nov-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UListaFuncionarios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UListaSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.StdCtrls, Datasnap.DBClient, Datasnap.Provider, Vcl.DBCtrls,
  JvExDBGrids, JvDBGrid;

type
  TFListaFuncionarios = class(TFListaSimplesPadrao)
    lblNomeFuncionario: TLabel;
    edtNomeFuncionario: TEdit;
    dspGridListaPadrao: TDataSetProvider;
    cdsGridListaPadrao: TClientDataSet;
    cdsGridListaPadraoID_FUNCIONARIO: TIntegerField;
    cdsGridListaPadraoNOME_FUNCIONARIO: TStringField;
    cdsGridListaPadraoSELECIONADO: TBooleanField;
    cdsGridListaPadraoVR_SALARIO: TBCDField;
    btnConfirmar: TJvTransparentButton;
    chbSelecionarTodos: TCheckBox;
    pnlBotaoAlerta: TPanel;
    btnAlerta: TJvTransparentButton;
    procedure FormShow(Sender: TObject);
    procedure edtNomeFuncionarioKeyPress(Sender: TObject; var Key: Char);
    procedure btnLimparClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure dbgGridListaPadraoCellClick(Column: TColumn);
    procedure dbgGridListaPadraoDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure dbgGridListaPadraoColEnter(Sender: TObject);
    procedure dbgGridListaPadraoColExit(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
    procedure chbSelecionarTodosClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure dbgGridListaPadraoKeyPress(Sender: TObject; var Key: Char);
    procedure chbSelecionarTodosKeyPress(Sender: TObject; var Key: Char);
    procedure cdsGridListaPadraoVR_SALARIOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure pnlBotaoAlertaClick(Sender: TObject);
    procedure btnAlertaClick(Sender: TObject);
  private
    { Private declarations }

    FOriginalOptions: TDBGridOptions;

    procedure KeyPressFormulario(Sender: TObject; var Key: Char);

    procedure MarcarDesmarcarTodos;
    procedure SalvarBoleano;
  public
    { Public declarations }
  end;

var
  FListaFuncionarios: TFListaFuncionarios;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais,
  UCadastroFechamentoSalario, UDMFechamentoSalario;

procedure TFListaFuncionarios.btnAlertaClick(Sender: TObject);
begin
  inherited;

  pnlBotaoAlerta.OnClick(Sender);
end;

procedure TFListaFuncionarios.btnConfirmarClick(Sender: TObject);
var
  sMsg: String;
  iCont: Integer;
  sLstFunc: String;
begin
  inherited;

  iCont := 0;
  sMsg  := '';

  FCadastroFechamentoSalario.FinalizarComponenteFuncionario;
  FCadastroFechamentoSalario.InicializarComponenteFuncionario;

  cdsGridListaPadrao.First;

  while not cdsGridListaPadrao.Eof do
  begin

    if cdsGridListaPadrao.FieldByName('SELECIONADO').AsBoolean then
    begin
      if Trim(sLstFunc) = '' then
      begin
        sLstFunc := cdsGridListaPadrao.FieldByName('NOME_FUNCIONARIO').AsString;

        DadosFuncionario := Funcionario.Create;
        DadosFuncionario.IdFuncionario   := cdsGridListaPadrao.FieldByName('ID_FUNCIONARIO').AsInteger;
        DadosFuncionario.NomeFuncionario := cdsGridListaPadrao.FieldByName('NOME_FUNCIONARIO').AsString;
        DadosFuncionario.VrSalario       := cdsGridListaPadrao.FieldByName('VR_SALARIO').AsCurrency;
        ListaFuncionarios.Add(DadosFuncionario);
      end
      else
      begin
        sLstFunc := (sLstFunc + #13#10 + cdsGridListaPadrao.FieldByName('NOME_FUNCIONARIO').AsString);

        DadosFuncionario := Funcionario.Create;
        DadosFuncionario.IdFuncionario   := cdsGridListaPadrao.FieldByName('ID_FUNCIONARIO').AsInteger;
        DadosFuncionario.NomeFuncionario := cdsGridListaPadrao.FieldByName('NOME_FUNCIONARIO').AsString;
        DadosFuncionario.VrSalario       := cdsGridListaPadrao.FieldByName('VR_SALARIO').AsCurrency;
        ListaFuncionarios.Add(DadosFuncionario);
      end;

      Inc(iCont);
    end;

    cdsGridListaPadrao.Next;
  end;

  if iCont = 0 then
    Application.MessageBox('Nenhum Colaborador selecionado.', 'Aviso', MB_OK + MB_ICONWARNING)
  else
  begin
    if iCont = 1 then
      sMsg := 'Confirma a sele��o do Colaborador abaixo? ' + #13#10 + sLstFunc
    else
      sMsg := 'Confirma a sele��o dos Colaboradores abaixo? ' + #13#10 + sLstFunc;

    if Application.MessageBox(PChar(sMsg), 'Aviso', MB_YESNO + MB_ICONQUESTION) = ID_YES then
      Self.Close;
  end;
end;

procedure TFListaFuncionarios.btnFecharClick(Sender: TObject);
begin
  FCadastroFechamentoSalario.FinalizarComponenteFuncionario;
  FCadastroFechamentoSalario.InicializarComponenteFuncionario;

  inherited;
end;

procedure TFListaFuncionarios.btnFiltrarClick(Sender: TObject);
begin
  inherited;

  cdsGridListaPadrao.Filtered := False;

  if Trim(edtNomeFuncionario.Text) <> '' then
  begin
    cdsGridListaPadrao.Filter   := 'UPPER(NOME_FUNCIONARIO) LIKE ' + QuotedStr('%' + Trim(UpperCase(edtNomeFuncionario.Text)) + '%');
    cdsGridListaPadrao.Filtered := True;
  end;
end;

procedure TFListaFuncionarios.btnLimparClick(Sender: TObject);
begin
  inherited;

  edtNomeFuncionario.Clear;

  if edtNomeFuncionario.CanFocus then
    edtNomeFuncionario.SetFocus;
end;

procedure TFListaFuncionarios.cdsGridListaPadraoVR_SALARIOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFListaFuncionarios.chbSelecionarTodosClick(Sender: TObject);
begin
  inherited;

  MarcarDesmarcarTodos;
end;

procedure TFListaFuncionarios.chbSelecionarTodosKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  KeyPressFormulario(Sender, Key);
end;

procedure TFListaFuncionarios.dbgGridListaPadraoCellClick(Column: TColumn);
begin
  inherited;

  if dbgGridListaPadrao.SelectedField.DataType = ftBoolean then
    SalvarBoleano;
end;

procedure TFListaFuncionarios.dbgGridListaPadraoColEnter(Sender: TObject);
begin
  inherited;

  if dbgGridListaPadrao.SelectedField.DataType = ftBoolean then
  begin
    FOriginalOptions           := dbgGridListaPadrao.Options;
    dbgGridListaPadrao.Options := dbgGridListaPadrao.Options - [dgEditing];
  end;
end;

procedure TFListaFuncionarios.dbgGridListaPadraoColExit(Sender: TObject);
begin
  inherited;

  if dbgGridListaPadrao.SelectedField.DataType = ftBoolean then
    dbgGridListaPadrao.Options := FOriginalOptions;
end;

procedure TFListaFuncionarios.dbgGridListaPadraoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
const
  CtrlState: array[Boolean] of Integer = (DFCS_BUTTONCHECK,
                                          DFCS_BUTTONCHECK or DFCS_CHECKED);
var
  CheckBoxRectangle: TRect;
begin
  inherited;

  if (Column.Field.FieldName = 'SELECIONADO') then
  begin
    dbgGridListaPadrao.Canvas.FillRect(Rect);

    CheckBoxRectangle.Left   := (Rect.Left + 2);
    CheckBoxRectangle.Right  := (Rect.Right - 2);
    CheckBoxRectangle.Top    := (Rect.Top + 2);
    CheckBoxRectangle.Bottom := (Rect.Bottom - 2);

    DrawFrameControl(dbgGridListaPadrao.Canvas.Handle,
                     CheckBoxRectangle,
                     DFC_BUTTON,
                     CtrlState[Column.Field.AsBoolean]);
  end;
end;

procedure TFListaFuncionarios.dbgGridListaPadraoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  KeyPressFormulario(Sender, Key);
end;

procedure TFListaFuncionarios.edtNomeFuncionarioKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnFiltrar.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    FCadastroFechamentoSalario.FinalizarComponenteFuncionario;
    FCadastroFechamentoSalario.InicializarComponenteFuncionario;
  end;
end;

procedure TFListaFuncionarios.FormKeyPress(Sender: TObject; var Key: Char);
begin
  KeyPressFormulario(Sender, Key);
end;

procedure TFListaFuncionarios.FormShow(Sender: TObject);
var
  sLista: String;
  j: Integer;
  QryAux: TFDQuery;
begin
  inherited;

  QryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  QryAux.Close;
  QryAux.SQL.Clear;
  QryAux.SQL.Text := 'SELECT ID_FUNCIONARIO '  +
                     '  FROM FUNCIONARIO ' +
                     ' WHERE FLG_ATIVO = ' + QuotedStr('S') +
                     '   AND DATA_DESLIGAMENTO IS NULL ' +
                     '   AND (NOME_FUNCIONARIO <> ' + QuotedStr('N�O INFORMADO') +
                     '    OR  NOME_FUNCIONARIO <> ' + QuotedStr('FUNCION�RIO') + ')' +
                     '   AND (VR_SALARIO IS NULL ' +
                     '    OR  VR_SALARIO = 0)';
  QryAux.Open;

  btnAlerta.Visible := (QryAux.RecordCount > 0);

  sLista := '';

  with QryAux, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT S.ID_FUNCIONARIO_FK AS ID_FUNCIONARIO ' +
            '  FROM SALARIO S ' +
            ' INNER JOIN FECHAMENTO_SALARIO F ' +
            '    ON S.ID_FECHAMENTO_SALARIO_FK = F.ID_FECHAMENTO_SALARIO ' +
            ' WHERE F.MES_REFERENCIA = :MES_REFERENCIA ' +
            '   AND F.ANO_REFERENCIA = :ANO_REFERENCIA ' +
            '   AND F.DATA_REAL_PAGAMENTO IS NOT NULL ' +
            'ORDER BY S.ID_FUNCIONARIO_FK';
    Params.ParamByName('MES_REFERENCIA').Value := iMesRef;
    Params.ParamByName('ANO_REFERENCIA').Value := iAnoRef;
    Open;

    if QryAux.RecordCount > 0 then
    begin
      QryAux.First;

      while not QryAux.Eof do
      begin
        if Trim(sLista) = '' then
          sLista := QryAux.FieldByName('ID_FUNCIONARIO').AsString
        else
          sLista := (sLista + ', ' + QryAux.FieldByName('ID_FUNCIONARIO').AsString);

        QryAux.Next;
      end;
    end;
  end;

  if ListaFuncionarios.Count > 0 then
  begin
    j := 0;

    for j := 0 to Pred(ListaFuncionarios.Count) do
    begin
      if Trim(sLista) = '' then
        sLista := IntToStr(ListaFuncionarios[j].IdFuncionario)
      else
        sLista := (sLista + ', ' + IntToStr(ListaFuncionarios[j].IdFuncionario));
    end;
  end;

  qryGridListaPadrao.Close;
  qryGridListaPadrao.SQL.Clear;

  if Trim(sLista) = '' then
  begin
    qryGridListaPadrao.SQL.Text := 'SELECT ID_FUNCIONARIO, NOME_FUNCIONARIO, VR_SALARIO ' +
                                   '  FROM FUNCIONARIO ' +
                                   ' WHERE FLG_ATIVO = ' + QuotedStr('S') +
                                   '   AND (NOME_FUNCIONARIO <> ' + QuotedStr('N�O INFORMADO') +
                                   '    OR  NOME_FUNCIONARIO <> ' + QuotedStr('FUNCION�RIO') + ')' +
                                   '   AND VR_SALARIO IS NOT NULL ' +
                                   '   AND VR_SALARIO > 0 ' +
                                   'ORDER BY NOME_FUNCIONARIO';
  end
  else
  begin
    qryGridListaPadrao.SQL.Text := 'SELECT ID_FUNCIONARIO, NOME_FUNCIONARIO, VR_SALARIO ' +
                                   '  FROM FUNCIONARIO ' +
                                   ' WHERE FLG_ATIVO = ' + QuotedStr('S') +
                                   '   AND ID_FUNCIONARIO NOT IN (' + sLista + ') ' +
                                   '   AND (NOME_FUNCIONARIO <> ' + QuotedStr('N�O INFORMADO') +
                                   '    OR  NOME_FUNCIONARIO <> ' + QuotedStr('FUNCION�RIO') + ')' +
                                   '   AND VR_SALARIO IS NOT NULL ' +
                                   '   AND VR_SALARIO > 0 ' +
                                   'ORDER BY NOME_FUNCIONARIO';
  end;

  cdsGridListaPadrao.Close;
  cdsGridListaPadrao.Open;

  chbSelecionarTodos.Checked := True;
  MarcarDesmarcarTodos;

  ShowScrollBar(dbgGridListaPadrao.Handle, SB_HORZ, False);

  FreeAndNil(QryAux);

  if edtNomeFuncionario.CanFocus then
    edtNomeFuncionario.SetFocus;
end;

procedure TFListaFuncionarios.KeyPressFormulario(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    FCadastroFechamentoSalario.FinalizarComponenteFuncionario;
    FCadastroFechamentoSalario.InicializarComponenteFuncionario;
  end;
end;

procedure TFListaFuncionarios.SalvarBoleano;
begin
  dbgGridListaPadrao.SelectedField.Dataset.Edit;
  dbgGridListaPadrao.SelectedField.AsBoolean := not dbgGridListaPadrao.SelectedField.AsBoolean;
  dbgGridListaPadrao.SelectedField.Dataset.Post;
end;

procedure TFListaFuncionarios.MarcarDesmarcarTodos;
begin
  cdsGridListaPadrao.First;

  while not cdsGridListaPadrao.Eof do
  begin
    cdsGridListaPadrao.Edit;

    if chbSelecionarTodos.Checked then
      cdsGridListaPadrao.FieldByName('SELECIONADO').AsBoolean := True
    else
      cdsGridListaPadrao.FieldByName('SELECIONADO').AsBoolean := False;

    cdsGridListaPadrao.Post;

    cdsGridListaPadrao.Next;
  end;

  if cdsGridListaPadrao.FieldByName('SELECIONADO').AsBoolean then
    chbSelecionarTodos.Caption := 'Desmarcar Todos'
  else
    chbSelecionarTodos.Caption := 'Marcar Todos';
end;

procedure TFListaFuncionarios.pnlBotaoAlertaClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(pnlBotaoAlerta, 1, 'Cadastro Incompleto',
  'H� Colcaboradores com o cadastro incompleto que n�o aparecer�o nessa lista.', clBlue, clNavy);
end;

end.
