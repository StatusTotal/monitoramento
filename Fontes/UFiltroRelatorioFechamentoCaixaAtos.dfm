inherited FFiltroRelatorioFechamentoCaixaAtos: TFFiltroRelatorioFechamentoCaixaAtos
  Caption = 'Relat'#243'rio de Fechamento de Caixa por Ato'
  ClientHeight = 254
  ClientWidth = 565
  ExplicitWidth = 571
  ExplicitHeight = 283
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlFiltro: TPanel
    Width = 559
    Height = 248
    ExplicitWidth = 559
    ExplicitHeight = 277
    object lblPeriodo: TLabel [0]
      Left = 5
      Top = 8
      Width = 125
      Height = 14
      Caption = 'Per'#237'odo de Pagamento'
    end
    object lblPeriodoInicio: TLabel [1]
      Left = 5
      Top = 31
      Width = 32
      Height = 14
      Caption = 'In'#237'cio:'
    end
    object lblPeriodoFim: TLabel [2]
      Left = 149
      Top = 31
      Width = 22
      Height = 14
      Caption = 'Fim:'
    end
    inherited pnlBotoes: TPanel
      Top = 201
      Width = 559
      TabOrder = 9
      ExplicitTop = 230
      ExplicitWidth = 559
      inherited btnVisualizar: TJvTransparentButton
        Left = 225
        ExplicitLeft = 225
      end
      inherited btnCancelar: TJvTransparentButton
        Left = 333
        ExplicitLeft = 333
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 117
        OnClick = btnLimparClick
        ExplicitLeft = 117
      end
    end
    inherited gbTipo: TGroupBox
      Top = 154
      Width = 149
      Height = 43
      TabOrder = 5
      ExplicitTop = 154
      ExplicitWidth = 149
      ExplicitHeight = 43
      inherited chbTipoAnalitico: TCheckBox
        Left = 81
        Top = 19
        ExplicitLeft = 81
        ExplicitTop = 19
      end
    end
    inherited chbExibirGrafico: TCheckBox
      Left = 168
      Top = 154
      TabOrder = 6
      ExplicitLeft = 168
      ExplicitTop = 154
    end
    inherited chbExportarPDF: TCheckBox
      Left = 168
      Top = 173
      TabOrder = 7
      ExplicitLeft = 168
      ExplicitTop = 173
    end
    inherited chbExportarExcel: TCheckBox
      Left = 299
      Top = 173
      TabOrder = 8
      ExplicitLeft = 299
      ExplicitTop = 173
    end
    object dteDataInicio: TJvDateEdit
      Left = 43
      Top = 28
      Width = 100
      Height = 22
      Color = 16114127
      ShowNullDate = False
      TabOrder = 0
      OnKeyPress = dteDataInicioKeyPress
    end
    object dteDataFim: TJvDateEdit
      Left = 177
      Top = 28
      Width = 100
      Height = 22
      Color = 16114127
      ShowNullDate = False
      TabOrder = 1
      OnKeyPress = dteDataFimKeyPress
    end
    object rgTipoAto: TRadioGroup
      Left = 5
      Top = 56
      Width = 550
      Height = 69
      Caption = 'Tipo de Ato'
      ItemIndex = 0
      Items.Strings = (
        'Todos'
        'Espec'#237'fico:')
      TabOrder = 2
      OnClick = rgTipoAtoClick
      OnExit = rgTipoAtoExit
    end
    object lcbTipoAto: TDBLookupComboBox
      Left = 91
      Top = 96
      Width = 458
      Height = 22
      Color = 16114127
      Enabled = False
      KeyField = 'ID_ITEM'
      ListField = 'DESCR_ITEM'
      ListSource = dsTipoAto
      TabOrder = 3
      OnKeyPress = FormKeyPress
    end
    object chbExibirDemaisLanc: TCheckBox
      Left = 5
      Top = 131
      Width = 166
      Height = 17
      Caption = 'Exibir Demais Lan'#231'amentos'
      TabOrder = 4
      OnClick = chbExibirDemaisLancClick
      OnKeyPress = FormKeyPress
    end
  end
  object qryTipoAto: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT I.ID_ITEM, I.DESCR_ITEM'
      '  FROM ITEM I'
      ' WHERE EXISTS(SELECT LD.ID_ITEM_FK'
      '                FROM LANCAMENTO_DET LD'
      '               INNER JOIN LANCAMENTO L'
      '                  ON LD.COD_LANCAMENTO_FK = L.COD_LANCAMENTO'
      '                 AND LD.ANO_LANCAMENTO_FK = L.ANO_LANCAMENTO'
      '               WHERE L.TIPO_LANCAMENTO = '#39'R'#39
      '                 AND L.FLG_CANCELADO = '#39'N'#39
      '                 AND L.FLG_FLUTUANTE = '#39'N'#39
      '                 AND LD.COD_ADICIONAL IS NOT NULL'
      '                 AND LD.FLG_CANCELADO = '#39'N'#39
      '                 AND LD.ID_ITEM_FK = I.ID_ITEM'
      
        '                 AND L.DATA_LANCAMENTO BETWEEN :DATA_INI AND :DA' +
        'TA_FIM)'
      'ORDER BY I.DESCR_ITEM')
    Left = 451
    Top = 139
    ParamData = <
      item
        Name = 'DATA_INI'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'DATA_FIM'
        DataType = ftDate
        ParamType = ptInput
      end>
  end
  object dsTipoAto: TDataSource
    DataSet = qryTipoAto
    Left = 507
    Top = 139
  end
end
