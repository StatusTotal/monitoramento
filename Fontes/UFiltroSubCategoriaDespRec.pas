{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltrooSubCategoriaDespRec.pas
  Descricao:   Filtro de Subcategorias de Despesas ou Receitas
  Author   :   Cristina
  Date:        04-mar-2016
  Last Update: 09-nov-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroSubCategoriaDespRec;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.StdCtrls, frxClass, frxDBSet;

type
  TFFiltroSubCategoriaDespRec = class(TFFiltroSimplesPadrao)
    edtSubCategoria: TEdit;
    lblSubCategoria: TLabel;
    lblCategoria: TLabel;
    cbCategoria: TComboBox;
    qryCategoria: TFDQuery;
    qryGridPaiPadraoID: TIntegerField;
    qryGridPaiPadraoID_CATEGORIA_DESPREC_FK: TIntegerField;
    qryGridPaiPadraoDESCR_CATEGORIA_DESPREC: TStringField;
    qryGridPaiPadraoTIPO: TStringField;
    qryGridPaiPadraoDESCR_SUBCATEGORIA_DESPREC: TStringField;
    qryGridPaiPadraoDESCR_NATUREZA: TStringField;
    procedure edtSubCategoriaKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure cbCategoriaKeyPress(Sender: TObject; var Key: Char);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroSubCategoriaDespRec: TFFiltroSubCategoriaDespRec;

  TipoSubCategoria: String;
  IdSubCategoria: Integer;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais,
  UCadastroSubCategoriaDespRec;

procedure TFFiltroSubCategoriaDespRec.btnEditarClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao       := E;
  vgIdConsulta     := qryGridPaiPadrao.FieldByName('ID').AsInteger;
  vgOrigemFiltro   := True;

  try
    Application.CreateForm(TFCadastroSubCategoriaDespRec, FCadastroSubCategoriaDespRec);

    TipoSubCat := TipoSubCategoria;

    FCadastroSubCategoriaDespRec.ShowModal;
  finally
    FCadastroSubCategoriaDespRec.Free;
  end;

  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroSubCategoriaDespRec.btnExcluirClick(Sender: TObject);
var
  qryExclusao: TFDQuery;
begin
  inherited;

  if lPodeExcluir then
  begin
    qryExclusao := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      with qryExclusao, SQL do
      begin
        Close;
        Clear;
        Text := 'DELETE FROM SUBCATEGORIA_DESPREC WHERE ID_SUBCATEGORIA_DESPREC = :iIdSubCatDespRec';
        Params.ParamByName('iIdSubCatDespRec').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      GravarLog;
      Application.MessageBox('Subcategoria exclu�do com sucesso!', 'Aviso', MB_OK)
    except
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Application.MessageBox('Erro na exclus�o da Subcategoria.', 'Erro', MB_OK + MB_ICONERROR)
    end;

    FreeAndNil(qryExclusao);

    btnFiltrar.Click;
  end;
end;

procedure TFFiltroSubCategoriaDespRec.btnFiltrarClick(Sender: TObject);
begin
  with qryGridPaiPadrao, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT S.ID_SUBCATEGORIA_DESPREC AS ID, ' +
            '       S.ID_CATEGORIA_DESPREC_FK, ' +
            '       C.DESCR_CATEGORIA_DESPREC, ' +
            '       S.TIPO, ' +
            '       S.DESCR_SUBCATEGORIA_DESPREC, ' +
            '       N.DESCR_NATUREZA ' +
            '  FROM SUBCATEGORIA_DESPREC S ' +
            ' INNER JOIN CATEGORIA_DESPREC C ' +
            '    ON S.ID_CATEGORIA_DESPREC_FK = C.ID_CATEGORIA_DESPREC ' +
            '  LEFT JOIN NATUREZA N ' +
            '    ON C.ID_NATUREZA_FK = N.ID_NATUREZA ' +
            ' WHERE S.TIPO = ' + QuotedStr(TipoSubCategoria);

    if Trim(cbCategoria.Text) <> '' then
    begin
      qryCategoria.Close;
      qryCategoria.Params.ParamByName('DESCR_CATEGORIA_DESPREC').Value := Trim(cbCategoria.Text);
      qryCategoria.Params.ParamByName('TIPO').Value                    := Trim(TipoSubCategoria);
      qryCategoria.Open;

      Add(' AND S.ID_CATEGORIA_DESPREC_FK = ' + qryCategoria.FieldByName('ID_CATEGORIA_DESPREC').AsString);
    end;

    if Trim(edtSubCategoria.Text) <> '' then
    begin
      Add(' AND CAST(UPPER(S.DESCR_SUBCATEGORIA_DESPREC) AS VARCHAR(250)) LIKE CAST(:DESCR AS VARCHAR(250))');
      Params.ParamByName('DESCR').Value := '%' + Trim(UpperCase(edtSubCategoria.Text)) + '%';
    end;

    Add('ORDER BY C.DESCR_CATEGORIA_DESPREC, S.DESCR_SUBCATEGORIA_DESPREC');
    Open;
  end;

  inherited;
end;

procedure TFFiltroSubCategoriaDespRec.btnIncluirClick(Sender: TObject);
begin
  inherited;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := True;

  try
    Application.CreateForm(TFCadastroSubCategoriaDespRec, FCadastroSubCategoriaDespRec);

    TipoSubCat := TipoSubCategoria;

    FCadastroSubCategoriaDespRec.ShowModal;
  finally
    FCadastroSubCategoriaDespRec.Free;
  end;

  btnFiltrar.Click;
end;

procedure TFFiltroSubCategoriaDespRec.btnLimparClick(Sender: TObject);
begin
  cbCategoria.ItemIndex := -1;
  edtSubCategoria.Clear;

  inherited;

  if cbCategoria.CanFocus then
    cbCategoria.SetFocus;
end;

procedure TFFiltroSubCategoriaDespRec.cbCategoriaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroSubCategoriaDespRec.dbgGridPaiPadraoDblClick(Sender: TObject);
var
  sTexto: String;
begin
  inherited;

  if vgOrigemCadastro then
  begin
    if qryGridPaiPadrao.RecordCount = 0 then
      IdSubCategoria := 0
    else
    begin
      IdSubCategoria := qryGridPaiPadrao.FieldByName('ID').AsInteger;

      sTexto := 'Confirma a sele��o de ' + qryGridPaiPadrao.FieldByName('DESCR_SUBCATEGORIA_DESPREC').AsString + '?';

      if Application.MessageBox(PChar(sTexto), 'Confirma��o', MB_YESNO + MB_ICONQUESTION) = ID_YES then
        Self.Close
      else
        IdSubCategoria := 0;
    end;
  end
  else
  begin
    PegarPosicaoGrid;

    vgOperacao     := C;
    vgIdConsulta   := qryGridPaiPadrao.FieldByName('ID').AsInteger;
    vgOrigemFiltro := True;

    try
      Application.CreateForm(TFCadastroSubCategoriaDespRec, FCadastroSubCategoriaDespRec);

      TipoSubCat := TipoSubCategoria;

      FCadastroSubCategoriaDespRec.ShowModal;
    finally
      FCadastroSubCategoriaDespRec.Free;
    end;

    DefinirPosicaoGrid;
  end;
end;

procedure TFFiltroSubCategoriaDespRec.edtSubCategoriaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroSubCategoriaDespRec.FormCreate(Sender: TObject);
begin
  inherited;

  IdSubCategoria := 0;
end;

procedure TFFiltroSubCategoriaDespRec.FormShow(Sender: TObject);
begin
  btnImprimir.Visible := False;

  BS.MontarComboBox(cbCategoria,
                    'DESCR_CATEGORIA_DESPREC',
                    'CATEGORIA_DESPREC',
                    'TIPO = :TIPO',
                    TipoSubCategoria,
                    'ID_CATEGORIA_DESPREC');

  with qryGridPaiPadrao, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT S.ID_SUBCATEGORIA_DESPREC AS ID, ' +
            '       S.ID_CATEGORIA_DESPREC_FK, ' +
            '       C.DESCR_CATEGORIA_DESPREC, ' +
            '       S.TIPO, ' +
            '       S.DESCR_SUBCATEGORIA_DESPREC, ' +
            '       N.DESCR_NATUREZA ' +
            '  FROM SUBCATEGORIA_DESPREC S ' +
            ' INNER JOIN CATEGORIA_DESPREC C ' +
            '    ON S.ID_CATEGORIA_DESPREC_FK = C.ID_CATEGORIA_DESPREC ' +
            '  LEFT JOIN NATUREZA N ' +
            '    ON C.ID_NATUREZA_FK = N.ID_NATUREZA ' +
            ' WHERE S.TIPO = ' + QuotedStr(TipoSubCategoria) +
            ' ORDER BY C.DESCR_CATEGORIA_DESPREC, S.DESCR_SUBCATEGORIA_DESPREC';
    Open;
  end;

  ShowScrollBar(dbgGridPaiPadrao.Handle, SB_HORZ, False);

  if TipoSubCategoria = 'D' then
    lblCategoria.Caption := 'Subcategoria de Despesa'
  else if TipoSubCategoria = 'R' then
    lblCategoria.Caption := 'Subcategoria de Receita';

  inherited;

  if cbCategoria.CanFocus then
    cbCategoria.SetFocus;
end;

procedure TFFiltroSubCategoriaDespRec.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  if vgOperacao = X then  //EXCLUSAO
  begin
    repeat
      sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da exclus�o dessa Subcategoria:', '');
    until sObservacao <> '';

    BS.GravarUsuarioLog(2, '', sObservacao,
                        qryGridPaiPadrao.FieldByName('ID').AsInteger, 'SUBCATEGORIA_DESPREC');
  end;
end;

function TFFiltroSubCategoriaDespRec.PodeExcluir(var Msg: String): Boolean;
var
  QryVerif: TFDQuery;
begin
  inherited;

  Result := True;

  QryVerif := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryVerif, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT * FROM LANCAMENTO WHERE ID_SUBCATEGORIA_DESPREC_FK = :ID_SUBCATEGORIA_DESPREC';
    Params.ParamByName('ID_SUBCATEGORIA_DESPREC').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
    Open;
  end;

  if QryVerif.RecordCount > 0 then
    msg := 'N�o � poss�vel excluir a Subcategoria informada, pois a mesma j� possui registro no Sistema.';

  Result := (QryVerif.RecordCount = 0) and (qryGridPaiPadrao.RecordCount > 0);

  FreeAndNil(QryVerif);
end;

procedure TFFiltroSubCategoriaDespRec.VerificarPermissoes;
begin
  inherited;

  if TipoSubCategoria = 'R' then
  begin
    btnIncluir.Enabled  := vgPrm_IncSubcatRec;
    btnEditar.Enabled   := vgPrm_EdSubcatRec and (qryGridPaiPadrao.RecordCount > 0);
    btnExcluir.Enabled  := vgPrm_ExcSubcatRec and (qryGridPaiPadrao.RecordCount > 0);
  end;

  if TipoSubCategoria = 'D' then
  begin
    btnIncluir.Enabled  := vgPrm_IncSubcatRec;
    btnEditar.Enabled   := vgPrm_EdSubcatDesp and (qryGridPaiPadrao.RecordCount > 0);
    btnExcluir.Enabled  := vgPrm_ExcSubcatDesp and (qryGridPaiPadrao.RecordCount > 0);
  end;

  btnImprimir.Enabled := False;
end;

end.
