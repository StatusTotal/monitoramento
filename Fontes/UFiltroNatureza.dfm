inherited FFiltroNatureza: TFFiltroNatureza
  Caption = 'Filtro de Natureza'
  ClientHeight = 277
  ClientWidth = 589
  OnCreate = FormCreate
  ExplicitWidth = 595
  ExplicitHeight = 306
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 583
    Height = 271
    ExplicitWidth = 583
    ExplicitHeight = 271
    inherited pnlGrid: TPanel
      Top = 52
      Width = 499
      Height = 216
      ExplicitTop = 52
      ExplicitWidth = 499
      ExplicitHeight = 216
      inherited pnlGridPai: TPanel
        Width = 499
        Height = 216
        ExplicitWidth = 499
        ExplicitHeight = 216
        inherited dbgGridPaiPadrao: TDBGrid
          Width = 499
          Height = 216
          Columns = <
            item
              Expanded = False
              FieldName = 'ID'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'DESCR_NATUREZA'
              Title.Caption = 'Descri'#231#227'o'
              Width = 250
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'TIPO_NATUREZA'
              Title.Alignment = taCenter
              Title.Caption = 'Tipo'
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'FLG_EDITAVEL'
              Title.Alignment = taCenter
              Title.Caption = 'Edit'#225'vel'
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'FLG_IMPOSTORENDA'
              Title.Alignment = taCenter
              Title.Caption = 'IR'
              Width = 45
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'FLG_AUXILIAR'
              Title.Alignment = taCenter
              Title.Caption = 'L. Aux.'
              Width = 45
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'FLG_PESSOAL'
              Title.Alignment = taCenter
              Title.Caption = 'C. Pes.'
              Width = 45
              Visible = True
            end>
        end
      end
    end
    inherited pnlFiltro: TPanel
      Width = 583
      Height = 49
      ExplicitWidth = 583
      ExplicitHeight = 49
      inherited btnFiltrar: TJvTransparentButton
        Left = 408
        Top = 20
        ExplicitLeft = 408
        ExplicitTop = 20
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 497
        Top = 20
        ExplicitLeft = 497
        ExplicitTop = 20
      end
      object lblDescrNatureza: TLabel
        Left = 81
        Top = 8
        Width = 48
        Height = 14
        Caption = 'Natureza'
      end
      object edtDescrNatureza: TEdit
        Left = 81
        Top = 24
        Width = 321
        Height = 22
        Color = 16114127
        TabOrder = 0
        OnKeyPress = edtDescrNaturezaKeyPress
      end
    end
    inherited pnlMenu: TPanel
      Top = 52
      Height = 216
      ExplicitTop = 52
      ExplicitHeight = 216
    end
  end
  inherited dsGridPaiPadrao: TDataSource
    Left = 508
    Top = 195
  end
  inherited qryGridPaiPadrao: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT ID_NATUREZA AS ID,'
      '       DESCR_NATUREZA,'
      '       TIPO_NATUREZA,'
      '       FLG_EDITAVEL,'
      '       FLG_IMPOSTORENDA,'
      '       FLG_AUXILIAR,'
      '       FLG_PESSOAL'
      '  FROM NATUREZA'
      'ORDER BY DESCR_NATUREZA')
    Left = 508
    Top = 143
    object qryGridPaiPadraoID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID_NATUREZA'
      Required = True
    end
    object qryGridPaiPadraoDESCR_NATUREZA: TStringField
      FieldName = 'DESCR_NATUREZA'
      Origin = 'DESCR_NATUREZA'
      Size = 250
    end
    object qryGridPaiPadraoTIPO_NATUREZA: TStringField
      FieldName = 'TIPO_NATUREZA'
      Origin = 'TIPO_NATUREZA'
      FixedChar = True
      Size = 1
    end
    object qryGridPaiPadraoFLG_EDITAVEL: TStringField
      FieldName = 'FLG_EDITAVEL'
      Origin = 'FLG_EDITAVEL'
      FixedChar = True
      Size = 1
    end
    object qryGridPaiPadraoFLG_IMPOSTORENDA: TStringField
      FieldName = 'FLG_IMPOSTORENDA'
      Origin = 'FLG_IMPOSTORENDA'
      FixedChar = True
      Size = 1
    end
    object qryGridPaiPadraoFLG_AUXILIAR: TStringField
      FieldName = 'FLG_AUXILIAR'
      Origin = 'FLG_AUXILIAR'
      FixedChar = True
      Size = 1
    end
    object qryGridPaiPadraoFLG_PESSOAL: TStringField
      FieldName = 'FLG_PESSOAL'
      Origin = 'FLG_PESSOAL'
      FixedChar = True
      Size = 1
    end
  end
end
