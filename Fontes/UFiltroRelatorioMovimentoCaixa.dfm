inherited FFiltroRelatorioMovimentoCaixa: TFFiltroRelatorioMovimentoCaixa
  Caption = 'Relat'#243'rio de Movimento de Caixa'
  ClientHeight = 182
  ExplicitHeight = 211
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlFiltro: TPanel
    Height = 176
    ExplicitWidth = 328
    ExplicitHeight = 176
    object lblData: TLabel [0]
      Left = 5
      Top = 8
      Width = 115
      Height = 14
      Caption = 'Data do Fechamento'
    end
    object lblVlrRealTContFinal: TLabel [1]
      Left = 3
      Top = 59
      Width = 99
      Height = 14
      Caption = 'Total Saldo do Dia'
    end
    object lblContaCorrente: TLabel [2]
      Left = 206
      Top = 54
      Width = 101
      Height = 26
      Caption = '(O Caixa desse Dia ainda n'#227'o foi aberto)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    inherited pnlBotoes: TPanel
      Top = 129
      TabOrder = 6
      ExplicitTop = 129
      ExplicitWidth = 328
      inherited btnLimpar: TJvTransparentButton
        OnClick = btnLimparClick
      end
    end
    inherited gbTipo: TGroupBox
      Left = 160
      Top = 95
      Width = 55
      Height = 29
      TabOrder = 2
      Visible = False
      ExplicitLeft = 160
      ExplicitTop = 95
      ExplicitWidth = 55
      ExplicitHeight = 29
      inherited chbTipoAnalitico: TCheckBox
        Left = 191
        Top = 56
        Width = 124
        Caption = 'Anal'#237'tico (Receitas)'
        Visible = False
        ExplicitLeft = 191
        ExplicitTop = 56
        ExplicitWidth = 124
      end
      inherited chbTipoSintetico: TCheckBox
        Left = 191
        Top = 33
        Width = 131
        Caption = 'Sint'#233'tico (Receitas)'
        Visible = False
        ExplicitLeft = 191
        ExplicitTop = 33
        ExplicitWidth = 131
      end
    end
    inherited chbExibirGrafico: TCheckBox
      Left = 236
      TabOrder = 3
      Visible = False
      ExplicitLeft = 236
    end
    inherited chbExportarPDF: TCheckBox
      Left = 5
      Top = 84
      TabOrder = 4
      ExplicitLeft = 5
      ExplicitTop = 84
    end
    inherited chbExportarExcel: TCheckBox
      Left = 5
      Top = 107
      TabOrder = 5
      ExplicitLeft = 5
      ExplicitTop = 107
    end
    object dteData: TJvDateEdit
      Left = 5
      Top = 24
      Width = 100
      Height = 22
      Color = 16114127
      ShowNullDate = False
      TabOrder = 0
      OnExit = dteDataExit
      OnKeyPress = dteDataKeyPress
    end
    object cedVlrRealTContFinal: TJvCalcEdit
      Left = 108
      Top = 56
      Width = 90
      Height = 22
      Color = 16114127
      DisplayFormat = ',0.00'
      ReadOnly = True
      TabOrder = 1
      ZeroEmpty = False
      DecimalPlacesAlwaysShown = False
      OnKeyPress = FormKeyPress
    end
  end
end
