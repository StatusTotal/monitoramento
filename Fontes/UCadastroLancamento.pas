{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroLancamento.pas
  Descricao:   Formulario de cadastro de Lancamentos de Despesas e Receitas
  Author   :   Cristina
  Date:        11-abr-2016
  Last Update: 23-out-2018 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroLancamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Vcl.ExtCtrls,
  Data.DB, JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls,
  JvToolEdit, Vcl.Mask, JvExMask, JvMaskEdit, Vcl.DBCtrls, JvSpin, Vcl.Grids,
  Vcl.DBGrids, JvBaseEdits, JvDBControls, Vcl.Graphics, Vcl.Imaging.jpeg,
  Vcl.Imaging.pngimage, JvExForms, JvCustomItemViewer, JvImageListViewer,
  Vcl.ComCtrls, System.ImageList, Vcl.ImgList, FireDAC.Stan.Param, System.StrUtils,
  FireDAC.Comp.Client, System.Generics.Collections, System.DateUtils,
  JvExStdCtrls, JvListComb, Clipbrd, Vcl.ExtDlgs, JvZoom, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxSkinsCore, dxSkinMetropolis, cxTrackBar, dxZoomTrackBar, JvExExtCtrls,
  JvImageRotate, JvAVICapture, cxImage, dxGDIPlusClasses, JvImage, cxClasses,
  Datasnap.DBClient, Commctrl, JvExtComponent, JvPanel, UDM,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, Vcl.Menus, JvCombobox, JvDBCombobox,
  JvAutoComplete, gtPDFClasses, gtCstPDFDoc, gtExPDFDoc, gtExProPDFDoc, gtPDFDoc,
  gtScrollingPanel, gtPDFViewer;

type
  TFCadastroLancamento = class(TFCadastroGeralPadrao)
    ntbLancamento: TNotebook;
    lblCliFor: TLabel;
    lblCodigo: TLabel;
    Label3: TLabel;
    lblAno: TLabel;
    edtCodigo: TDBEdit;
    edtAno: TDBEdit;
    lcbCliFor: TDBLookupComboBox;
    lblCategoria: TLabel;
    lblSubCategoria: TLabel;
    lblNumParcelas: TLabel;
    lblValorTotalPrev: TLabel;
    lblJuros: TLabel;
    lblDesconto: TLabel;
    lblValorTotalPago: TLabel;
    lblDataLancamento: TLabel;
    lblObsLancamento: TLabel;
    dteDataLancamento: TJvDBDateEdit;
    lcbCategoria: TDBLookupComboBox;
    lcbSubCategoria: TDBLookupComboBox;
    edtCliFor: TDBEdit;
    edtCategoria: TDBEdit;
    edtSubCategoria: TDBEdit;
    cedValorTotalPrev: TJvDBCalcEdit;
    cedValorTotalJuros: TJvDBCalcEdit;
    cedValorTotalDesconto: TJvDBCalcEdit;
    cedValorTotalPago: TJvDBCalcEdit;
    rgSituacaoLanc: TDBRadioGroup;
    mmObsLancamento: TDBMemo;
    chbRecorrente: TDBCheckBox;
    chbCanceladaLanc: TDBCheckBox;
    gbClassificacao: TGroupBox;
    chbImpostoRenda: TDBCheckBox;
    chbAuxiliar: TDBCheckBox;
    chbPessoal: TDBCheckBox;
    btnAbaDigitalizacao: TJvTransparentButton;
    lblNumeroRecibo: TLabel;
    edtQtdParcelasLanc: TDBEdit;
    btnAcionarScanner: TJvTransparentButton;
    btnAbrirPastaImagem: TJvTransparentButton;
    btnColarImagem: TJvTransparentButton;
    btnConfirmarAcao: TJvTransparentButton;
    btnCancelarAcao: TJvTransparentButton;
    btnEditarImagem: TJvTransparentButton;
    btnExcluirImagem: TJvTransparentButton;
    dbgImagens: TDBGrid;
    edtNomeArquivo: TEdit;
    gbParcelas: TGroupBox;
    lblNumParcela: TLabel;
    lblDataVencimento: TLabel;
    lblValorParcelaPrev: TLabel;
    lblValorParcelaJuros: TLabel;
    lblValorParcelaDesconto: TLabel;
    lblValorParcelaPago: TLabel;
    lblDataPagamentoParc: TLabel;
    lblDataCancelamentoParc: TLabel;
    lblAgenciaParc: TLabel;
    lblContaParc: TLabel;
    lblBancoParc: TLabel;
    lblObsParcela: TLabel;
    edtNumParcela: TDBEdit;
    dteDataVencimento: TJvDBDateEdit;
    cedValorParcelaPrev: TJvDBCalcEdit;
    cedValorParcelaJuros: TJvDBCalcEdit;
    cedValorParcelaDesconto: TJvDBCalcEdit;
    cedValorParcelaPago: TJvDBCalcEdit;
    dteDataPagamentoParc: TJvDBDateEdit;
    dteDataCancelamentoParc: TJvDBDateEdit;
    edtAgenciaParc: TDBEdit;
    edtContaParc: TDBEdit;
    lcbBancoParc: TDBLookupComboBox;
    mmObsParcela: TDBMemo;
    dbgParcelas: TDBGrid;
    btnIncluirParcela: TJvTransparentButton;
    btnEditarParcela: TJvTransparentButton;
    btnConfirmarParcela: TJvTransparentButton;
    btnExcluirParcela: TJvTransparentButton;
    btnCancelarParcela: TJvTransparentButton;
    lblAvisoValorDivergente: TLabel;
    btnIncluirCategoria: TJvTransparentButton;
    btnIncluirSubCategoria: TJvTransparentButton;
    tdlgMensagem: TTaskDialog;
    btnIncluirFornecedor: TJvTransparentButton;
    btnAbaDetalhe: TJvTransparentButton;
    dbgDetalhes: TDBGrid;
    btnIncluirDet: TJvTransparentButton;
    btnEditarDet: TJvTransparentButton;
    btnExcluirDet: TJvTransparentButton;
    btnConfirmarDet: TJvTransparentButton;
    btnCancelarDet: TJvTransparentButton;
    lblItemDet: TLabel;
    lblVlrUnitarioDet: TLabel;
    lblQuantidadeDet: TLabel;
    lblVlrTotalDet: TLabel;
    cedVlrUnitarioDet: TJvDBCalcEdit;
    cedQuantidadeDet: TJvDBCalcEdit;
    cedVlrTotalDet: TJvDBCalcEdit;
    lblSeloDet: TLabel;
    lblAleatorioDet: TLabel;
    lblTipoCobrancaDet: TLabel;
    lblCodAdicionalDet: TLabel;
    lblEmolumentosDet: TLabel;
    edtCodAdicionalDet: TDBEdit;
    chbCanceladoDet: TDBCheckBox;
    cedEmolumentosDet: TJvDBCalcEdit;
    cedFETJDet: TJvDBCalcEdit;
    cedFUNDPERJDet: TJvDBCalcEdit;
    cedFUNPERJDet: TJvDBCalcEdit;
    cedFUNARPENDet: TJvDBCalcEdit;
    cedPMCMVDet: TJvDBCalcEdit;
    cedISSDet: TJvDBCalcEdit;
    lblFETJDet: TLabel;
    lblFUNDPERJDet: TLabel;
    lblFUNPERJDet: TLabel;
    lblFUNARPENDet: TLabel;
    lblPMCMVDet: TLabel;
    lblISSDet: TLabel;
    medSeloDet: TJvDBMaskEdit;
    medAleatorioDet: TJvDBMaskEdit;
    medTipoCobrancaDet: TJvDBMaskEdit;
    lblNatureza: TLabel;
    edtNatureza: TDBEdit;
    lcbNatureza: TDBLookupComboBox;
    btnIncluirNatureza: TJvTransparentButton;
    lcbItemDet: TDBLookupComboBox;
    btnIncluirItem: TJvTransparentButton;
    lblTipoItem: TLabel;
    lblAvisoAltOrigem: TLabel;
    edtNumeroCheque: TDBEdit;
    lblNumeroCheque: TLabel;
    chbFlutuante: TDBCheckBox;
    edtNumCodDeposito: TDBEdit;
    lblNumCodDeposito: TLabel;
    lcbFormaPagamento: TDBLookupComboBox;
    edtFormaPagamento: TDBEdit;
    lblFormaPagamento: TLabel;
    lblNumProtocolo: TLabel;
    edtNumProtocolo: TDBEdit;
    lblMutuaDet: TLabel;
    lblAcoterjDet: TLabel;
    cedMutuaDet: TJvDBCalcEdit;
    cedAcoterjDet: TJvDBCalcEdit;
    lblCarneLeao: TLabel;
    lcbCarneLeao: TDBLookupComboBox;
    odAbrirPastaImagem: TOpenPictureDialog;
    chbForaFechCaixa: TDBCheckBox;
    ppmImagem: TPopupMenu;
    btnExportar: TMenuItem;
    cbTipoItem: TJvDBComboBox;
    btnProcurarItem: TJvTransparentButton;
    btnAtualizarPagamento: TJvTransparentButton;
    cbSituacaoParcela: TJvDBComboBox;
    lblSituacaoParcela: TLabel;
    lblDataLancamentoParc: TLabel;
    dteDataLancamentoParc: TJvDBDateEdit;
    dteDataCancelamentoLanc: TJvDBDateEdit;
    Label1: TLabel;
    odSalvarPDF: TOpenDialog;
    odAbrirPDF: TOpenDialog;
    docPDF: TgtPDFDocument;
    ntbArquivo: TNotebook;
    pnlImagem: TPanel;
    imgImagemLanc: TcxImage;
    vwPDF: TgtPDFViewer;
    btnAssociarPDF: TJvTransparentButton;
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnDadosPrincipaisClick(Sender: TObject);
    procedure lcbCliForExit(Sender: TObject);
    procedure lcbFormaPagamentoExit(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure edtFormaPagamentoExit(Sender: TObject);
    procedure edtCliForExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtQtdParcelasLancExit(Sender: TObject);
    procedure dbgImagensColEnter(Sender: TObject);
    procedure dbgImagensDblClick(Sender: TObject);
    procedure dbgImagensDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure edtNomeArquivoKeyPress(Sender: TObject; var Key: Char);
    procedure btnEditarImagemClick(Sender: TObject);
    procedure btnExcluirImagemClick(Sender: TObject);
    procedure btnConfirmarAcaoClick(Sender: TObject);
    procedure btnCancelarAcaoClick(Sender: TObject);
    procedure btnColarImagemClick(Sender: TObject);
    procedure btnAbrirPastaImagemClick(Sender: TObject);
    procedure btnAcionarScannerClick(Sender: TObject);
    procedure btnAbaDigitalizacaoClick(Sender: TObject);
    procedure chbRecorrenteClick(Sender: TObject);
    procedure dbgParcelasDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnIncluirParcelaClick(Sender: TObject);
    procedure btnEditarParcelaClick(Sender: TObject);
    procedure btnExcluirParcelaClick(Sender: TObject);
    procedure btnConfirmarParcelaClick(Sender: TObject);
    procedure btnCancelarParcelaClick(Sender: TObject);
    procedure dteDataPagamentoParcExit(Sender: TObject);
    procedure mmObsParcelaKeyPress(Sender: TObject; var Key: Char);
    procedure mmObsLancamentoKeyPress(Sender: TObject; var Key: Char);
    procedure chbImpostoRendaClick(Sender: TObject);
    procedure chbAuxiliarClick(Sender: TObject);
    procedure chbPessoalClick(Sender: TObject);
    procedure dbgParcelasDblClick(Sender: TObject);
    procedure btnIncluirCategoriaClick(Sender: TObject);
    procedure btnIncluirSubCategoriaClick(Sender: TObject);
    procedure btnIncluirItemClick(Sender: TObject);
    procedure btnIncluirFornecedorClick(Sender: TObject);
    procedure btnAbaDetalheClick(Sender: TObject);
    procedure btnIncluirDetClick(Sender: TObject);
    procedure btnEditarDetClick(Sender: TObject);
    procedure btnExcluirDetClick(Sender: TObject);
    procedure btnConfirmarDetClick(Sender: TObject);
    procedure btnCancelarDetClick(Sender: TObject);
    procedure dbgDetalhesDblClick(Sender: TObject);
    procedure dbgDetalhesDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnIncluirNaturezaClick(Sender: TObject);
    procedure edtNaturezaExit(Sender: TObject);
    procedure lcbNaturezaExit(Sender: TObject);
    procedure lcbItemDetExit(Sender: TObject);
    procedure cedVlrUnitarioDetExit(Sender: TObject);
    procedure cedQuantidadeDetExit(Sender: TObject);
    procedure chbCanceladoDetKeyPress(Sender: TObject; var Key: Char);
    procedure chbCanceladaLancKeyPress(Sender: TObject; var Key: Char);
    procedure chbFlutuanteClick(Sender: TObject);
    procedure cedValorTotalPrevExit(Sender: TObject);
    procedure cedAcoterjDetKeyPress(Sender: TObject; var Key: Char);
    procedure chbForaFechCaixaClick(Sender: TObject);
    procedure btnExportarClick(Sender: TObject);
    procedure lcbCategoriaClick(Sender: TObject);
    procedure lcbSubCategoriaClick(Sender: TObject);
    procedure edtCategoriaExit(Sender: TObject);
    procedure lcbCategoriaExit(Sender: TObject);
    procedure edtSubCategoriaExit(Sender: TObject);
    procedure lcbSubCategoriaExit(Sender: TObject);
    procedure lcbNaturezaClick(Sender: TObject);
    procedure lcbCliForClick(Sender: TObject);
    procedure lcbFormaPagamentoClick(Sender: TObject);
    procedure btnProcurarItemClick(Sender: TObject);
    procedure btnAtualizarPagamentoClick(Sender: TObject);
    procedure cbSituacaoParcelaClick(Sender: TObject);
    procedure cbSituacaoParcelaExit(Sender: TObject);
    procedure dteDataLancamentoExit(Sender: TObject);
    procedure dteDataLancamentoKeyPress(Sender: TObject; var Key: Char);
    procedure dteDataLancamentoParcExit(Sender: TObject);
    procedure dteDataLancamentoParcKeyPress(Sender: TObject; var Key: Char);
    procedure dteDataPagamentoParcKeyPress(Sender: TObject; var Key: Char);
    procedure rgSituacaoLancClick(Sender: TObject);
    procedure rgSituacaoLancExit(Sender: TObject);
    procedure rgSituacaoLancEnter(Sender: TObject);
    procedure btnAssociarPDFClick(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }

    cTotalLanc : Currency;

    lTabAuxDet,
    lTabAuxDig: Boolean;

    iIdIncParc,
    iIdIncDet,
    iIdIncImg: Integer;

    sNomeOriginal: String;
	
	  procedure HabDesabBotoesCRUD(Tipo: String; Inc, Ed, Exc: Boolean);
    procedure DesabilitaHabilitaCampos(Tipo: String; Hab: Boolean);
    procedure CalcularLancamento;
    procedure GerarParcelas;
    procedure CalcularPagamentoParcela;
    procedure AcertarParcelas(NumParc: Integer);
    procedure AbrirListaItem;
    procedure AbrirListaNatureza;
    procedure AtualizaSituacaoLancamento;

    function VerificarDadosParcela(var Msg: String; var QtdErros: Integer): Boolean;
    function VerificarDadosDetalhe(var Msg: String; var QtdErros: Integer): Boolean;
  public
    { Public declarations }

    TipoLanc: TTipoLancamento;

    iSituacaoLanc: Integer;

    lGerarNovaParcela,
    lSomentePagamento: Boolean;
  end;

var
  FCadastroLancamento: TFCadastroLancamento;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UGDM, UVariaveisGlobais, UDMLancamento,
  UCadastroCategoriaDespRec, UCadastroSubCategoriaDespRec,
  UCadastroProdutoServico, UCadastroClienteFornecedor, UCadastroNatureza,
  UListaDepositosFlutuantes, UCapturaImagem;

{ TFCadastroLancamento }

procedure TFCadastroLancamento.AbrirListaItem;
begin
  dmLancamento.qryItem.Close;

  lcbItemDet.Enabled := False;

  if not dmLancamento.cdsDetalhe_F.Active then
    Exit;

  dmLancamento.qryItem.Params.ParamByName('FLG_ATIVO_I1').Value := IfThen((vgOperacao = I), 'S', '');
  dmLancamento.qryItem.Params.ParamByName('FLG_ATIVO_I2').Value := IfThen((vgOperacao = I), 'S', '');
  dmLancamento.qryItem.Params.ParamByName('TIPO_ITEM1').Value   := IfThen((cbTipoItem.ItemIndex = 0),
                                                                          'P',
                                                                          'S');
  dmLancamento.qryItem.Params.ParamByName('TIPO_ITEM2').Value   := IfThen((cbTipoItem.ItemIndex = 0),
                                                                          'P',
                                                                          'S');
  dmLancamento.qryItem.Open;

  if dmLancamento.qryItem.RecordCount > 0 then
  begin
    if vgOperacao <> C then
      lcbItemDet.Enabled := True;
  end;

  if (vgOperacao <> I) and
    (dmLancamento.cdsDetalhe_F.RecordCount > 0) then
  begin
    if dmLancamento.qryItem.RecordCount > 0 then
    begin
      if dmLancamento.qryItem.FieldByName('TIPO_ITEM').AsString = 'P' then
        cbTipoItem.ItemIndex := 0
      else if dmLancamento.qryItem.FieldByName('TIPO_ITEM').AsString = 'S' then
        cbTipoItem.ItemIndex := 1;
    end;
  end;
end;

procedure TFCadastroLancamento.AbrirListaNatureza;
begin
  dmLancamento.qryNatureza.Close;

  if vgOperacao in [I, E] then
  begin
    dmLancamento.qryNatureza.SQL.Clear;
    dmLancamento.qryNatureza.SQL.Text := 'SELECT * ' +
                                         '  FROM NATUREZA ' +
                                         ' WHERE TIPO_NATUREZA = :TIPO_NATUREZA ';

    if vgOperacao = I then
      dmLancamento.qryNatureza.SQL.Add(' AND ID_NATUREZA NOT IN (1, 2, 3, 4, 5, 6, 7) ')
    else
    begin
      if vgUsaTotalFirmas then
      begin
        if vgUsaTotalRecibo then
          dmLancamento.qryNatureza.SQL.Add(' AND ID_NATUREZA NOT IN (1, 2, 3, 7) ')
        else
          dmLancamento.qryNatureza.SQL.Add(' AND ID_NATUREZA NOT IN (1, 2, 3, 6, 7) ');
      end
      else if vgUsaTotalRecibo then
        dmLancamento.qryNatureza.SQL.Add(' AND ID_NATUREZA NOT IN (1, 2, 3, 4, 5, 7) ')
      else
        dmLancamento.qryNatureza.SQL.Add(' AND ID_NATUREZA NOT IN (1, 2, 3, 4, 5, 6, 7) ');
    end;
  end
  else
  begin
    dmLancamento.qryNatureza.SQL.Clear;
    dmLancamento.qryNatureza.SQL.Text := 'SELECT * ' +
                                         '  FROM NATUREZA ' +
                                         ' WHERE TIPO_NATUREZA = :TIPO_NATUREZA ';
  end;

  dmLancamento.qryNatureza.SQL.Add('ORDER BY DESCR_NATUREZA');

  dmLancamento.qryNatureza.Params.ParamByName('TIPO_NATUREZA').Value := IfThen((TipoLanc = DESP), 'D', 'R');
  dmLancamento.qryNatureza.Open;
end;

procedure TFCadastroLancamento.AcertarParcelas(NumParc: Integer);
var
  cTotalParcs: Currency;
  iProxParc: Integer;
begin
  cTotalParcs := 0;
  iProxParc   := 0;

  cTotalLanc := dmLancamento.cdsLancamento_F.FieldByName('VR_TOTAL_PREV').AsCurrency;

  dmLancamento.cdsParcela_F.First;

  while not dmLancamento.cdsParcela_F.Eof do
  begin
    cTotalParcs := (cTotalParcs + dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency);

    if iProxParc = 0 then
    begin
      if (dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString = 'P') and
        (NumParc <> dmLancamento.cdsParcela_F.FieldByName('NUM_PARCELA').AsInteger) then
        iProxParc := dmLancamento.cdsParcela_F.FieldByName('NUM_PARCELA').AsInteger;
    end;

    dmLancamento.cdsParcela_F.Next;
  end;

  if cTotalLanc > cTotalParcs then  //Houve reducao na parcela
  begin
    if iProxParc > 0 then
    begin
      if dmLancamento.cdsParcela_F.Locate('NUM_PARCELA', iProxParc, []) then
      begin
        dmLancamento.cdsParcela_F.Edit;
        dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency := (dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency +
                                                                              (cTotalLanc - cTotalParcs));
        dmLancamento.cdsParcela_F.Post;
      end;
    end
    else
    begin
      Inc(iIdIncParc);

      dmLancamento.cdsParcela_F.Append;
      dmLancamento.cdsParcela_F.FieldByName('NOVO').AsBoolean                  := True;
      dmLancamento.cdsParcela_F.FieldByName('ID_LANCAMENTO_PARC').AsInteger    := (iIdIncParc + 900000);
      dmLancamento.cdsParcela_F.FieldByName('NUM_PARCELA').AsInteger           := (dmLancamento.cdsLancamento_F.FieldByName('QTD_PARCELAS').AsInteger + 1);
      dmLancamento.cdsParcela_F.FieldByName('DATA_LANCAMENTO_PARC').AsDateTime := dteDataLancamento.Date;
      dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger  := 6;  //DINHEIRO - default
      dmLancamento.cdsParcela_F.FieldByName('DATA_VENCIMENTO').AsDateTime      := dteDataLancamento.Date;  //IncDay(Date);
      dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString             := 'P';
      dmLancamento.cdsParcela_F.FieldByName('CAD_ID_USUARIO').AsInteger        := vgUsu_Id;
      dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').Value            := Null;
      dmLancamento.cdsParcela_F.FieldByName('PAG_ID_USUARIO').Value            := Null;
      dmLancamento.cdsParcela_F.FieldByName('DATA_CANCELAMENTO').Value         := Null;
      dmLancamento.cdsParcela_F.FieldByName('CANCEL_ID_USUARIO').Value         := Null;
      dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency      := (cTotalLanc - cTotalParcs);
      dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_JUROS').AsCurrency     := 0;
      dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_DESCONTO').AsCurrency  := 0;
      dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency      := 0;
      dmLancamento.cdsParcela_F.Post;

      if not (dmLancamento.cdsLancamento_F.State in [dsInsert, dsEdit]) then
        dmLancamento.cdsLancamento_F.Edit;

      dmLancamento.cdsLancamento_F.FieldByName('QTD_PARCELAS').AsInteger := (dmLancamento.cdsLancamento_F.FieldByName('QTD_PARCELAS').AsInteger + 1);
    end;
  end
  else if cTotalLanc < cTotalParcs then  //Houve aumento na parcela
  begin
    if iProxParc > 0 then
    begin
      if dmLancamento.cdsParcela_F.Locate('NUM_PARCELA', iProxParc, []) then
      begin
        dmLancamento.cdsParcela_F.Edit;
        dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency := (dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency -
                                                                              (cTotalParcs - cTotalLanc));
        dmLancamento.cdsParcela_F.Post;
      end;
    end
    else
    begin
      if not (dmLancamento.cdsLancamento_F.State in [dsInsert, dsEdit]) then
        dmLancamento.cdsLancamento_F.Edit;

      dmLancamento.cdsLancamento_F.FieldByName('VR_TOTAL_PREV').AsCurrency := (dmLancamento.cdsLancamento_F.FieldByName('VR_TOTAL_PREV').AsCurrency +
                                                                               (cTotalParcs - cTotalLanc));
    end;
  end;
end;

procedure TFCadastroLancamento.AtualizaSituacaoLancamento;
var
  cTotalPago: Currency;
begin
  if vgOperacao <> C then
  begin
    cTotalPago := 0;

    if rgSituacaoLanc.ItemIndex = 0 then  //Lancamento a Pagar
    begin
      //PARCELAS
      dmLancamento.cdsParcela_F.DisableControls;

      dmLancamento.cdsParcela_F.First;

      while not dmLancamento.cdsParcela_F.Eof do
      begin
        if dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString <> 'C' then
        begin
          dmLancamento.cdsParcela_F.Edit;
          dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency := 0;
          dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString        := 'P';
          dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').Value       := Null;
          dmLancamento.cdsParcela_F.Post;
        end;

        dmLancamento.cdsParcela_F.Next;
      end;

      dmLancamento.cdsParcela_F.EnableControls;

      //LANCAMENTO
      dmLancamento.cdsLancamento_F.FieldByName('FLG_STATUS').AsString      := 'P';
      dmLancamento.cdsLancamento_F.FieldByName('VR_TOTAL_PAGO').AsCurrency := 0;
    end
    else
    begin  //Lancamento Pago
      GerarParcelas;

      //PARCELAS
      dmLancamento.cdsParcela_F.DisableControls;

      dmLancamento.cdsParcela_F.First;

      while not dmLancamento.cdsParcela_F.Eof do
      begin
        if dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString <> 'C' then
        begin
          dmLancamento.cdsParcela_F.Edit;
          dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency := ((dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency +
                                                                                   dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_JUROS').AsCurrency) -
                                                                                  dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_DESCONTO').AsCurrency);
          dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString        := 'G';

          if dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').IsNull then
            dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := dteDataLancamento.Date;

          dmLancamento.cdsParcela_F.Post;

          cTotalPago := (cTotalPago + dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency);
        end;

        dmLancamento.cdsParcela_F.Next;
      end;

      dmLancamento.cdsParcela_F.EnableControls;

      //LANCAMENTO
      dmLancamento.cdsLancamento_F.FieldByName('FLG_STATUS').AsString      := 'G';
      dmLancamento.cdsLancamento_F.FieldByName('VR_TOTAL_PAGO').AsCurrency := cTotalPago;
    end;
  end;
end;

procedure TFCadastroLancamento.btnAbaDetalheClick(Sender: TObject);
begin
  inherited;

  //DETALHE
  if not lTabAuxDet then
  begin
    with dmLancamento do
    begin
      if vgOperacao = I then
      begin
        cdsDetalhe_F.Append;
        cdsDetalhe_F.FieldByName('NOVO').AsBoolean         := True;
        cdsDetalhe_F.FieldByName('FLG_CANCELADO').AsString := 'N';
        chbCanceladoDet.Checked := False;

        HabDesabBotoesCRUD('D', False, False, False);
        DesabilitaHabilitaCampos('D', True);
      end
      else
      begin
        if vgOperacao <> C then
        begin
          if cdsDetalhe_F.RecordCount = 0 then
            HabDesabBotoesCRUD('D', True, False, False)
          else
            HabDesabBotoesCRUD('D', True, True, True);

          DesabilitaHabilitaCampos('D', False);
        end;
      end;

      case cdsLancamento_F.FieldByName('ID_NATUREZA_FK').AsInteger of
        1, 3:
        begin
          btnIncluirDet.Enabled   := False;
          btnEditarDet.Enabled    := False;
          btnExcluirDet.Enabled   := False;
          btnConfirmarDet.Enabled := False;
          btnCancelarDet.Enabled  := False;

          lblAvisoAltOrigem.Visible := True;

          lblItemDet.Caption        := 'Colaborador';
          lblVlrUnitarioDet.Caption := 'Sal�rio';

          lblQuantidadeDet.Visible   := False;
          cedQuantidadeDet.Visible   := False;
          lblVlrTotalDet.Visible     := False;
          cedVlrTotalDet.Visible     := False;
          lblNumProtocolo.Visible    := False;
          edtNumProtocolo.Visible    := False;
          chbCanceladoDet.Visible    := False;

          lblSeloDet.Visible         := False;
          medSeloDet.Visible         := False;
          lblAleatorioDet.Visible    := False;
          medAleatorioDet.Visible    := False;
          lblTipoCobrancaDet.Visible := False;
          medTipoCobrancaDet.Visible := False;
          lblCodAdicionalDet.Visible := False;
          edtCodAdicionalDet.Visible := False;
          lblEmolumentosDet.Visible  := False;
          cedEmolumentosDet.Visible  := False;
          lblFETJDet.Visible         := False;
          cedFETJDet.Visible         := False;
          lblFUNDPERJDet.Visible     := False;
          cedFUNDPERJDet.Visible     := False;
          lblFUNPERJDet.Visible      := False;
          cedFUNPERJDet.Visible      := False;
          lblFUNARPENDet.Visible     := False;
          cedFUNARPENDet.Visible     := False;
          lblPMCMVDet.Visible        := False;
          cedPMCMVDet.Visible        := False;
          lblISSDet.Visible          := False;
          cedISSDet.Visible          := False;
          lblMutuaDet.Visible        := False;
          cedMutuaDet.Visible        := False;
          lblAcoterjDet.Visible      := False;
          cedAcoterjDet.Visible      := False;

          dbgDetalhes.Top    := lblQuantidadeDet.Top;
          dbgDetalhes.Height := ((btnIncluirDet.Top - 4) - dbgDetalhes.Top);
        end;
        2, 4, 5, 6:
        begin
          btnIncluirDet.Enabled   := False;
          btnEditarDet.Enabled    := False;
          btnExcluirDet.Enabled   := False;
          btnConfirmarDet.Enabled := False;
          btnCancelarDet.Enabled  := False;

          lblAvisoAltOrigem.Visible := True;

          lblItemDet.Caption        := 'Item';
          lblVlrUnitarioDet.Caption := 'Vlr. Unit�rio';

          lblQuantidadeDet.Visible   := True;
          cedQuantidadeDet.Visible   := True;
          lblVlrTotalDet.Visible     := True;
          cedVlrTotalDet.Visible     := True;
          lblNumProtocolo.Visible    := True;
          edtNumProtocolo.Visible    := True;
          chbCanceladoDet.Visible    := True;

          lblSeloDet.Visible         := True;
          medSeloDet.Visible         := True;
          lblAleatorioDet.Visible    := True;
          medAleatorioDet.Visible    := True;
          lblTipoCobrancaDet.Visible := True;
          medTipoCobrancaDet.Visible := True;
          lblCodAdicionalDet.Visible := True;
          edtCodAdicionalDet.Visible := True;
          lblEmolumentosDet.Visible  := True;
          cedEmolumentosDet.Visible  := True;
          lblFETJDet.Visible         := True;
          cedFETJDet.Visible         := True;
          lblFUNDPERJDet.Visible     := True;
          cedFUNDPERJDet.Visible     := True;
          lblFUNPERJDet.Visible      := True;
          cedFUNPERJDet.Visible      := True;
          lblFUNARPENDet.Visible     := True;
          cedFUNARPENDet.Visible     := True;
          lblPMCMVDet.Visible        := True;
          cedPMCMVDet.Visible        := True;
          lblISSDet.Visible          := True;
          cedISSDet.Visible          := True;
          lblMutuaDet.Visible        := True;
          cedMutuaDet.Visible        := True;
          lblAcoterjDet.Visible      := True;
          cedAcoterjDet.Visible      := True;

          dbgDetalhes.Top    := (medSeloDet.Top + medSeloDet.Height + 7);
          dbgDetalhes.Height := ((btnIncluirDet.Top - 4) - dbgDetalhes.Top);
        end;
        7:
        begin
          btnIncluirDet.Enabled   := False;
          btnEditarDet.Enabled    := False;
          btnExcluirDet.Enabled   := False;
          btnConfirmarDet.Enabled := False;
          btnCancelarDet.Enabled  := False;

          lblAvisoAltOrigem.Visible := False;

          lblItemDet.Caption        := 'Item';
          lblVlrUnitarioDet.Caption := 'Vlr. Unit�rio';

          lblQuantidadeDet.Visible   := True;
          cedQuantidadeDet.Visible   := True;
          lblVlrTotalDet.Visible     := True;
          cedVlrTotalDet.Visible     := True;
          lblNumProtocolo.Visible    := True;
          edtNumProtocolo.Visible    := True;
          chbCanceladoDet.Visible    := True;

          lblSeloDet.Visible         := False;
          medSeloDet.Visible         := False;
          lblAleatorioDet.Visible    := False;
          medAleatorioDet.Visible    := False;
          lblTipoCobrancaDet.Visible := False;
          medTipoCobrancaDet.Visible := False;
          lblCodAdicionalDet.Visible := False;
          edtCodAdicionalDet.Visible := False;
          lblEmolumentosDet.Visible  := False;
          cedEmolumentosDet.Visible  := False;
          lblFETJDet.Visible         := False;
          cedFETJDet.Visible         := False;
          lblFUNDPERJDet.Visible     := False;
          cedFUNDPERJDet.Visible     := False;
          lblFUNPERJDet.Visible      := False;
          cedFUNPERJDet.Visible      := False;
          lblFUNARPENDet.Visible     := False;
          cedFUNARPENDet.Visible     := False;
          lblPMCMVDet.Visible        := False;
          cedPMCMVDet.Visible        := False;
          lblISSDet.Visible          := False;
          cedISSDet.Visible          := False;
          lblMutuaDet.Visible        := False;
          cedMutuaDet.Visible        := False;
          lblAcoterjDet.Visible      := False;
          cedAcoterjDet.Visible      := False;

          dbgDetalhes.Top    := lblSeloDet.Top;
          dbgDetalhes.Height := ((btnIncluirDet.Top - 4) - dbgDetalhes.Top);
        end;
        else
        begin
          btnIncluirDet.Enabled   := (vgOperacao <> C);
          btnEditarDet.Enabled    := (vgOperacao <> C) and
                                     (dmLancamento.cdsDetalhe_F.RecordCount > 0);
          btnExcluirDet.Enabled   := (vgOperacao <> C) and
                                     (dmLancamento.cdsDetalhe_F.RecordCount > 0);
          btnConfirmarDet.Enabled := False;
          btnCancelarDet.Enabled  := False;

          lblAvisoAltOrigem.Visible := False;

          lblItemDet.Caption        := 'Item';
          lblVlrUnitarioDet.Caption := 'Vlr. Unit�rio';

          lblQuantidadeDet.Visible   := True;
          cedQuantidadeDet.Visible   := True;
          lblVlrTotalDet.Visible     := True;
          cedVlrTotalDet.Visible     := True;
          edtNumProtocolo.Visible    := True;
          chbCanceladoDet.Visible    := True;

          lblSeloDet.Visible         := False;
          medSeloDet.Visible         := False;
          lblAleatorioDet.Visible    := False;
          medAleatorioDet.Visible    := False;
          lblTipoCobrancaDet.Visible := False;
          medTipoCobrancaDet.Visible := False;
          lblCodAdicionalDet.Visible := False;
          edtCodAdicionalDet.Visible := False;
          lblEmolumentosDet.Visible  := False;
          cedEmolumentosDet.Visible  := False;
          lblFETJDet.Visible         := False;
          cedFETJDet.Visible         := False;
          lblFUNDPERJDet.Visible     := False;
          cedFUNDPERJDet.Visible     := False;
          lblFUNPERJDet.Visible      := False;
          cedFUNPERJDet.Visible      := False;
          lblFUNARPENDet.Visible     := False;
          cedFUNARPENDet.Visible     := False;
          lblPMCMVDet.Visible        := False;
          cedPMCMVDet.Visible        := False;
          lblISSDet.Visible          := False;
          cedISSDet.Visible          := False;
          lblMutuaDet.Visible        := False;
          cedMutuaDet.Visible        := False;
          lblAcoterjDet.Visible      := False;
          cedAcoterjDet.Visible      := False;

          dbgDetalhes.Top    := lblSeloDet.Top;
          dbgDetalhes.Height := ((btnIncluirDet.Top - 4) - dbgDetalhes.Top);
        end;
      end;
    end;

    lTabAuxDet := True;
  end;

  MarcarDesmarcarAbas(DET);
end;

procedure TFCadastroLancamento.btnAbaDigitalizacaoClick(Sender: TObject);
begin
  inherited;

  //DIGITALIZACAO
  if not lTabAuxDig then
  begin
    with dmLancamento do
    begin
      if not (vgOperacao in [C, I]) then
      begin
        if cdsImagem_F.RecordCount > 0 then
          HabDesabBotoesCRUD('M', True, True, True)
        else
          HabDesabBotoesCRUD('M', True, False, False);
      end;

      if (vgOperacao <> I) and
        (dmLancamento.cdsImagem_F.FieldByName('CAMINHO_COMPLETO').AsString <> '') then
      begin
        if dmLancamento.cdsImagem_F.FieldByName('EXTENSAO').AsString = 'jpg' then
          imgImagemLanc.Picture.LoadFromFile(dmLancamento.cdsImagem_F.FieldByName('CAMINHO_COMPLETO').AsString)
        else if dmLancamento.cdsImagem_F.FieldByName('EXTENSAO').AsString = 'pdf' then
        begin
          docPDF.Reset;
          docPDF.LoadFromFile(dmLancamento.cdsImagem_F.FieldByName('CAMINHO_COMPLETO').AsString);
          vwPDF.Align := alClient;
          vwPDF.PDFDocument := docPDF;
          vwPDF.Active := True;
        end;
      end;

      DesabilitaHabilitaCampos('M', False);
    end;

    lTabAuxDig := True;

    if dmLancamento.cdsImagem_F.RecordCount > 0 then
    begin
      imgImagemLanc.PopupMenu := ppmImagem;
      vwPDF.PopupMenu         := ppmImagem;
    end
    else
    begin
      imgImagemLanc.PopupMenu := nil;
      vwPDF.PopupMenu         := nil;
    end;

    ntbArquivo.PageIndex := 0;
  end;

  MarcarDesmarcarAbas(DIG);
end;

procedure TFCadastroLancamento.btnAbrirPastaImagemClick(Sender: TObject);
var
  sCEscolhido, sCaminho: String;
  j, iPos: Integer;
begin
  inherited;

  imgImagemLanc.PopupMenu := nil;

  ntbArquivo.PageIndex := 0;

  odAbrirPastaImagem.InitialDir := vgConf_DiretorioImagens +
                                   IntToStr(vgLanc_Ano) + '\';
  iPos := 0;

  if odAbrirPastaImagem.Execute then
  begin
    if odAbrirPastaImagem.FileName <> '' then
    begin
      sCEscolhido := odAbrirPastaImagem.FileName;

      for j := 0 to Length(sCEscolhido) - 1 do
      begin
        if sCEscolhido[j] = '\' then
          iPos := j;
      end;

      sCaminho  := Copy(sCEscolhido, 1, iPos);

      try
        //Tela
        imgImagemLanc.Picture.LoadFromFile(odAbrirPastaImagem.FileName);

        //Banco
        Inc(iIdIncImg);
        dmLancamento.cdsImagem_F.Append;
        dmLancamento.cdsImagem_F.FieldByName('NOVO').AsBoolean              := True;
        dmLancamento.cdsImagem_F.FieldByName('ID_LANCAMENTO_IMG').AsInteger := iIdIncImg;

        if UpperCase(sCaminho) <> UpperCase(vgConf_DiretorioImagens +
                                            IntToStr(vgLanc_Ano) + '\') then
          dmLancamento.cdsImagem_F.FieldByName('ORIGEM_ANTIGA').AsString := sCEscolhido;

        HabDesabBotoesCRUD('M', False, False, False);
        DesabilitaHabilitaCampos('M', True);

        if edtNomeArquivo.CanFocus then
          edtNomeArquivo.SetFocus;
      except
        //Nao exibir erro caso nao consiga carregar a Imagem
        if dmLancamento.cdsImagem_F.State = dsInsert then
          dmLancamento.cdsImagem_F.Cancel;

        HabDesabBotoesCRUD('M', True, True, True);
        DesabilitaHabilitaCampos('M', False);

        if dmLancamento.cdsImagem_F.RecordCount > 0 then
        begin
          imgImagemLanc.PopupMenu := ppmImagem;
          vwPDF.PopupMenu         := ppmImagem;
        end;
      end;
    end;
  end;
end;

procedure TFCadastroLancamento.btnAcionarScannerClick(Sender: TObject);
var
  Imagem: TBitmap;
begin
  inherited;

  imgImagemLanc.PopupMenu := nil;

  Imagem := nil;

  try
    Application.CreateForm(TFCapturaImagem, FCapturaImagem);
    FCapturaImagem.ShowModal;
  finally
    Imagem := FCapturaImagem.ImagemCapturada;
    FreeAndNil(FCapturaImagem);
  end;

  if Imagem <> nil then
  begin
    try
      //Tela
      imgImagemLanc.Picture.Assign(Imagem);

      //Banco
      Inc(iIdIncImg);
      dmLancamento.cdsImagem_F.Append;
      dmLancamento.cdsImagem_F.FieldByName('NOVO').AsBoolean              := True;
      dmLancamento.cdsImagem_F.FieldByName('ID_LANCAMENTO_IMG').AsInteger := iIdIncImg;

      HabDesabBotoesCRUD('M', False, False, False);
      DesabilitaHabilitaCampos('M', True);

      if edtNomeArquivo.CanUndo then
        edtNomeArquivo.SetFocus;
    except
      HabDesabBotoesCRUD('M', True, True, True);
      DesabilitaHabilitaCampos('M', False);

      if dmLancamento.cdsImagem_F.RecordCount > 0 then
        imgImagemLanc.PopupMenu := ppmImagem;
    end;
  end;
end;

procedure TFCadastroLancamento.btnAssociarPDFClick(Sender: TObject);
var
  sCEscolhido, sCaminho: String;
  j, iPos: Integer;
begin
  inherited;

  sNomeOriginal := '';

  vwPDF.PopupMenu := nil;

  ntbArquivo.PageIndex := 1;

  odAbrirPDF.InitialDir := vgConf_DiretorioImagens +
                           IntToStr(vgLanc_Ano) + '\';
  iPos := 0;

  if odAbrirPDF.Execute then
  begin
    if odAbrirPDF.FileName <> '' then
    begin
      sNomeOriginal := odAbrirPDF.FileName;
      sCEscolhido   := odAbrirPDF.FileName;

      for j := 0 to Length(sCEscolhido) - 1 do
      begin
        if sCEscolhido[j] = '\' then
          iPos := j;
      end;

      sCaminho := Copy(sCEscolhido, 1, iPos);

      try
        //Tela
        docPDF.Reset;
        docPDF.LoadFromFile(odAbrirPDF.FileName);
        vwPDF.Align := alClient;
        vwPDF.PDFDocument := docPDF;
        vwPDF.Active := True;

        //Banco
        Inc(iIdIncImg);
        dmLancamento.cdsImagem_F.Append;
        dmLancamento.cdsImagem_F.FieldByName('NOVO').AsBoolean          := True;
        dmLancamento.cdsImagem_F.FieldByName('ID_LANCAMENTO_IMG').AsInteger := iIdIncImg;

        if UpperCase(sCaminho) <> UpperCase(vgConf_DiretorioImagens +
                                            IntToStr(vgLanc_Ano) + '\') then
          dmLancamento.cdsImagem_F.FieldByName('ORIGEM_ANTIGA').AsString := sCEscolhido;

        HabDesabBotoesCRUD('M', False, False, False);
        DesabilitaHabilitaCampos('M', True);

        if edtNomeArquivo.CanFocus then
          edtNomeArquivo.SetFocus;
      except
        //Nao exibir erro caso nao consiga carregar o Arquivo
        if dmLancamento.cdsImagem_F.State = dsInsert then
          dmLancamento.cdsImagem_F.Cancel;

        HabDesabBotoesCRUD('M', True, True, True);
        DesabilitaHabilitaCampos('M', False);

        if dmLancamento.cdsImagem_F.RecordCount > 0 then
        begin
          imgImagemLanc.PopupMenu := ppmImagem;
          vwPDF.PopupMenu         := ppmImagem;
        end;
      end;
    end;
  end;
end;

procedure TFCadastroLancamento.btnCancelarAcaoClick(Sender: TObject);
begin
  inherited;

  dmLancamento.cdsImagem_F.Cancel;

  edtNomeArquivo.Clear;

  if dmLancamento.cdsImagem_F.RecordCount = 0 then
    HabDesabBotoesCRUD('M', True, False, False)
  else
    HabDesabBotoesCRUD('M', True, True, True);

  DesabilitaHabilitaCampos('M', False);

  if dmLancamento.cdsImagem_F.RecordCount > 0 then
  begin
    imgImagemLanc.PopupMenu := ppmImagem;
    vwPDF.PopupMenu         := ppmImagem;
  end;
end;

procedure TFCadastroLancamento.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
  begin
    { IMAGENS }
    if dmLancamento.cdsImagem_F.RecordCount > 0 then
    begin
      dmLancamento.cdsImagem_F.First;

      while not dmLancamento.cdsImagem_F.Eof do
      begin
        if not dmLancamento.cdsImagem_F.FieldByName('NOME_V_ANTIGO').IsNull then
        begin
          if FileExists(vgConf_DiretorioImagens +
                        IntToStr(vgLanc_Ano) +
                        '\' +
                        dmLancamento.cdsImagem_F.FieldByName('NOME_FIXO').AsString +
                        dmLancamento.cdsImagem_F.FieldByName('NOME_V_ANTIGO').AsString + '.' +
                        dmLancamento.cdsImagem_F.FieldByName('EXTENSAO').AsString) then
          begin
            DeleteFile(vgConf_DiretorioImagens +
                        IntToStr(vgLanc_Ano) +
                       '\' +
                       dmLancamento.cdsImagem_F.FieldByName('NOME_FIXO').AsString +
                       dmLancamento.cdsImagem_F.FieldByName('NOME_V_ANTIGO').AsString + '.' +
                       dmLancamento.cdsImagem_F.FieldByName('EXTENSAO').AsString);
          end;
        end;

        if dmLancamento.cdsImagem_F.FieldByName('NOVO').AsBoolean and
          dmLancamento.cdsImagem_F.FieldByName('COD_LANCAMENTO_FK').IsNull then
        begin
          if FileExists(vgConf_DiretorioImagens +
                          IntToStr(vgLanc_Ano) +
                        '\' +
                        dmLancamento.cdsImagem_F.FieldByName('NOME_FIXO').AsString +
                        dmLancamento.cdsImagem_F.FieldByName('NOME_VARIAVEL').AsString + '.' +
                        dmLancamento.cdsImagem_F.FieldByName('EXTENSAO').AsString) then
          begin
            DeleteFile(vgConf_DiretorioImagens +
                          IntToStr(vgLanc_Ano) +
                       '\' +
                       dmLancamento.cdsImagem_F.FieldByName('NOME_FIXO').AsString +
                       dmLancamento.cdsImagem_F.FieldByName('NOME_VARIAVEL').AsString + '.' +
                       dmLancamento.cdsImagem_F.FieldByName('EXTENSAO').AsString);
          end;
        end;

        dmLancamento.cdsImagem_F.Next;
      end;
    end;

    Self.Close;
  end;
end;

procedure TFCadastroLancamento.btnCancelarDetClick(Sender: TObject);
begin
  inherited;

  dmLancamento.cdsDetalhe_F.Cancel;

  cbTipoItem.ItemIndex := -1;

  if dmLancamento.cdsDetalhe_F.RecordCount = 0 then
    HabDesabBotoesCRUD('D', True, False, False)
  else
    HabDesabBotoesCRUD('D', True, True, True);

  DesabilitaHabilitaCampos('D', False);
end;

procedure TFCadastroLancamento.btnCancelarParcelaClick(Sender: TObject);
begin
  inherited;

  dmLancamento.cdsParcela_F.Cancel;

  if dmLancamento.cdsParcela_F.RecordCount = 0 then
    HabDesabBotoesCRUD('P', True, False, False)
  else
    HabDesabBotoesCRUD('P', True, True, True);

  DesabilitaHabilitaCampos('P', False);
end;

procedure TFCadastroLancamento.btnColarImagemClick(Sender: TObject);
var
  imgBitmap: TBitmap;
begin
  inherited;

  imgImagemLanc.PopupMenu := nil;

  ntbArquivo.PageIndex := 0;

  try
    Clipboard.Open;

    if Clipboard.HasFormat(CF_PICTURE) or
      Clipboard.HasFormat(CF_BITMAP) then
    begin
      //Tela
      imgBitmap := TBitmap.Create;
      imgBitmap.Assign(Clipboard);
      imgImagemLanc.Picture.Bitmap.Assign(imgBitmap);

      //Banco
      Inc(iIdIncImg);
      dmLancamento.cdsImagem_F.Append;
      dmLancamento.cdsImagem_F.FieldByName('NOVO').AsBoolean              := True;
      dmLancamento.cdsImagem_F.FieldByName('ID_LANCAMENTO_IMG').AsInteger := iIdIncImg;

      HabDesabBotoesCRUD('M', False, False, False);
      DesabilitaHabilitaCampos('M', True);

      if edtNomeArquivo.CanFocus then
        edtNomeArquivo.SetFocus;
    end;

    Clipboard.Close;

    FreeAndNil(imgBitmap);
  except
    FreeAndNil(imgBitmap);

    if dmLancamento.cdsImagem_F.RecordCount > 0 then
    begin
      imgImagemLanc.PopupMenu := ppmImagem;
      vwPDF.PopupMenu         := ppmImagem;
    end;
  end;
end;

procedure TFCadastroLancamento.btnConfirmarAcaoClick(Sender: TObject);
var
  sNomeFoto, MsgErro: String;
  imgJpg: TJPEGImage;
  arqPDF: TgtPDFDocument;
begin
  inherited;

  MsgErro := '';

  if Trim(edtNomeArquivo.Text) = '' then
  begin
    MsgErro := 'Nenhum nome foi escolhido para o arquivo.' + #13 +
               'Por favor, digite um nome para o mesmo.';

    Application.MessageBox(PChar(MsgErro), 'Erro', MB_OK + MB_ICONERROR);

    if edtNomeArquivo.CanFocus then
      edtNomeArquivo.SetFocus;
  end
  else
  begin
    sNomeFoto := dmLancamento.cdsLancamento_F.FieldByName('COD_LANCAMENTO').AsString + '_' +
                 vgLanc_TpLanc + '_' +
                 Trim(edtNomeArquivo.Text) +
                 IfThen(ntbArquivo.PageIndex = 0, '.jpg', '.pdf');

    if FileExists(vgConf_DiretorioImagens +
                  IntToStr(vgLanc_Ano) +
                  '\' + sNomeFoto) then
    begin
      MsgErro := 'J� existe um arquivo com o nome escolhido.' + #13 +
                 'Por favor, digite outro nome para o arquivo.';

      Application.MessageBox(PChar(MsgErro), 'Aviso', MB_OK + MB_ICONEXCLAMATION);

      if edtNomeArquivo.CanFocus then
        edtNomeArquivo.SetFocus;
    end
    else
    begin
      if dmLancamento.cdsImagem_F.State = dsInsert then
      begin
        //Arquivo
        if ntbArquivo.PageIndex = 0 then
        begin
          dmGerencial.Processando(True, 'Salvando Imagem', True, 'Por favor, aguarde...');
          imgJpg := TJPEGImage.Create;
          imgJpg.Assign(imgImagemLanc.Picture.Graphic);
          imgJpg.SaveToFile(vgConf_DiretorioImagens +
                            IntToStr(vgLanc_Ano) +
                            '\' + sNomeFoto);
          dmGerencial.Processando(False);
        end
        else if ntbArquivo.PageIndex = 1 then
        begin
          if Trim(sNomeOriginal) <> '' then
          begin
            dmGerencial.Processando(True, 'Salvando arquivo PDF', True, 'Por favor, aguarde...');
            arqPDF := TgtPDFDocument.Create(nil);
            arqPDF.LoadFromFile(sNomeOriginal);
            arqPDF.SaveToFile(vgConf_DiretorioImagens +
                              IntToStr(vgLanc_Ano) +
                              '\' +
                              sNomeFoto);
            arqPDF.OpenAfterSave := True;
            dmGerencial.Processando(False);
          end;
        end;
      end;

      if not dmLancamento.cdsImagem_F.FieldByName('NOME_V_ANTIGO').IsNull then
      begin
        //Arquivo
        if ntbArquivo.PageIndex = 0 then
        begin
          dmGerencial.Processando(True, 'Salvando Imagem', True, 'Por favor, aguarde...');
          imgJpg := TJPEGImage.Create;
          imgJpg.Assign(imgImagemLanc.Picture.Graphic);
          imgJpg.SaveToFile(vgConf_DiretorioImagens +
                            IntToStr(vgLanc_Ano) +
                            '\' + sNomeFoto);
          dmGerencial.Processando(False);
        end
        else if ntbArquivo.PageIndex = 1 then
        begin
          if Trim(sNomeOriginal) <> '' then
          begin
            dmGerencial.Processando(True, 'Salvando arquivo PDF', True, 'Por favor, aguarde...');
            arqPDF := TgtPDFDocument.Create(nil);
            arqPDF.LoadFromFile(sNomeOriginal);
            arqPDF.SaveToFile(vgConf_DiretorioImagens +
                              IntToStr(vgLanc_Ano) +
                              '\' +
                              sNomeFoto);
            arqPDF.OpenAfterSave := True;
            dmGerencial.Processando(False);
          end;
        end;
      end;

      dmLancamento.cdsImagem_F.FieldByName('NOME_VARIAVEL').AsString := Trim(edtNomeArquivo.Text);
      dmLancamento.cdsImagem_F.FieldByName('NOME_FIXO').AsString     := dmLancamento.cdsLancamento_F.FieldByName('COD_LANCAMENTO').AsString + '_' +
                                                                        vgLanc_TpLanc + '_';
      dmLancamento.cdsImagem_F.FieldByName('EXTENSAO').AsString      := IfThen(ntbArquivo.PageIndex = 0, 'jpg', 'pdf');

      dmLancamento.cdsImagem_F.Post;

      HabDesabBotoesCRUD('M', True, True, True);
      DesabilitaHabilitaCampos('M', False);

      FreeAndNil(imgJpg);
      FreeAndNil(arqPDF);
    end;
  end;

  imgImagemLanc.PopupMenu := ppmImagem;

  if (ntbArquivo.PageIndex = 1) and
    (Trim(sNomeOriginal) <> '') then
  begin
    docPDF.Reset;
    docPDF.LoadFromFile(vgConf_DiretorioImagens +
                        IntToStr(vgLanc_Ano) +
                        '\' +
                        sNomeFoto);
    vwPDF.Align := alClient;
    vwPDF.PDFDocument := docPDF;
    vwPDF.Active := True;
  end;

  vwPDF.PopupMenu := ppmImagem;

  edtNomeArquivo.Clear;
end;

procedure TFCadastroLancamento.btnConfirmarDetClick(Sender: TObject);
var
  Msg: String;
  iCount, Qtd: Integer;
  lInserindo:  Boolean;
begin
  inherited;

  lblMensagemErro.Caption := '  Mensagem:';
  lbMensagemErro.Color    := clWindow;
  lbMensagemErro.Clear;

  Msg    := '';
  iCount := 0;
  Qtd    := 0;
  lInserindo := False;

  if not VerificarDadosDetalhe(Msg, Qtd) then
  begin
    case Qtd of
      0:
      begin
        lblMensagemErro.Caption := '  Mensagem:';
        lbMensagemErro.Color    := clWindow;
      end;
      1:
      begin
        lblMensagemErro.Caption := '  Durante a verifica��o dos dados, o seguinte erro foi encontrado:';
        lbMensagemErro.Color    := clInfoBk;
      end
      else
      begin
        lblMensagemErro.Caption := '  Durante a verifica��o dos dados, os seguintes erros foram encontrados:';
        lbMensagemErro.Color    := clInfoBk;
      end;
    end;

    for iCount := 0 to Pred(ListaMsgErro.Count) do
      lbMensagemErro.Items.Add(ListaMsgErro[iCount].Mensagem);
  end
  else
  begin
    if dmLancamento.cdsDetalhe_F.State = dsInsert then
    begin
      Inc(iIdIncDet);
      dmLancamento.cdsDetalhe_F.FieldByName('ID_LANCAMENTO_DET').AsInteger := (iIdIncDet + 900000);
      lInserindo := True;
    end;

    dmLancamento.cdsDetalhe_F.Post;

    HabDesabBotoesCRUD('D', True, True, True);

    if lInserindo then
    begin
      if vgOperacao = E then
        DesabilitaHabilitaCampos('D', False)
      else
      begin
        if Application.MessageBox('Deseja incluir outro Detalhe?', 'Confirma��o', MB_YESNO + MB_ICONQUESTION) = ID_YES then
        begin
          DesabilitaHabilitaCampos('D', True);
          btnIncluirDet.Click;
        end
        else  if Application.MessageBox('Deseja incluir Imagem?', 'Confirma��o', MB_YESNO + MB_ICONQUESTION) = ID_YES then
        begin
          DesabilitaHabilitaCampos('D', True);
          btnAbaDigitalizacao.Click;
        end
        else
        begin
          DesabilitaHabilitaCampos('D', False);
          btnOk.Click;
        end;
      end;
    end
    else
      DesabilitaHabilitaCampos('D', False);
  end;
end;

procedure TFCadastroLancamento.btnConfirmarParcelaClick(Sender: TObject);
var
  Msg: String;
  iCount, Qtd: Integer;
  lInserindo:  Boolean;
begin
  inherited;

  lblMensagemErro.Caption := '  Mensagem:';
  lbMensagemErro.Color    := clWindow;
  lbMensagemErro.Clear;

  Msg    := '';
  iCount := 0;
  Qtd    := 0;
  lInserindo := False;

  if not VerificarDadosParcela(Msg, Qtd) then
  begin
    case Qtd of
      0:
      begin
        lblMensagemErro.Caption := '  Mensagem:';
        lbMensagemErro.Color    := clWindow;
      end;
      1:
      begin
        lblMensagemErro.Caption := '  Durante a verifica��o dos dados, o seguinte erro foi encontrado:';
        lbMensagemErro.Color    := clInfoBk;
      end
      else
      begin
        lblMensagemErro.Caption := '  Durante a verifica��o dos dados, os seguintes erros foram encontrados:';
        lbMensagemErro.Color    := clInfoBk;
      end;
    end;

    for iCount := 0 to Pred(ListaMsgErro.Count) do
      lbMensagemErro.Items.Add(ListaMsgErro[iCount].Mensagem);
  end
  else
  begin
    if dmLancamento.cdsParcela_F.State = dsInsert then
    begin
      Inc(iIdIncParc);
      dmLancamento.cdsParcela_F.FieldByName('ID_LANCAMENTO_PARC').AsInteger := (iIdIncParc + 900000);
      lInserindo := True;
    end;

    dmLancamento.cdsParcela_F.Post;

//    CalcularLancamento;
    AcertarParcelas(dmLancamento.cdsParcela_F.FieldByName('NUM_PARCELA').AsInteger);

    HabDesabBotoesCRUD('P', True, True, True);

    if lInserindo then
    begin
      if vgOperacao = E then
        DesabilitaHabilitaCampos('P', False)
      else
      begin
        if Application.MessageBox('Deseja incluir outra Parcela?', 'Confirma��o', MB_YESNO + MB_ICONQUESTION) = ID_YES then
        begin
          DesabilitaHabilitaCampos('P', True);
          btnIncluirParcela.Click;
        end
        else
        begin
          DesabilitaHabilitaCampos('P', False);
          btnAbaDigitalizacao.Click;
        end;
      end;
    end
    else
      DesabilitaHabilitaCampos('P', False);
  end;
end;

procedure TFCadastroLancamento.btnDadosPrincipaisClick(Sender: TObject);
begin
  inherited;

  MarcarDesmarcarAbas(PRINC);
end;

procedure TFCadastroLancamento.btnEditarDetClick(Sender: TObject);
begin
  inherited;

  if dmLancamento.cdsDetalhe_F.RecordCount > 0 then
  begin
    dmLancamento.cdsDetalhe_F.Edit;
    HabDesabBotoesCRUD('D', False, False, False);
    DesabilitaHabilitaCampos('D', True);
  end;
end;

procedure TFCadastroLancamento.btnEditarImagemClick(Sender: TObject);
begin
  inherited;

  if dmLancamento.cdsImagem_F.RecordCount > 0 then
  begin
    dmLancamento.cdsImagem_F.Edit;
    dmLancamento.cdsImagem_F.FieldByName('NOME_V_ANTIGO').AsString := dmLancamento.cdsImagem_F.FieldByName('NOME_VARIAVEL').AsString;
    edtNomeArquivo.Text := dmLancamento.cdsImagem_F.FieldByName('NOME_VARIAVEL').AsString;
    HabDesabBotoesCRUD('M', False, False, False);
    DesabilitaHabilitaCampos('M', True);
  end;
end;

procedure TFCadastroLancamento.btnEditarParcelaClick(Sender: TObject);
begin
  inherited;

  if dmLancamento.cdsParcela_F.RecordCount > 0 then
  begin
    dmLancamento.cdsParcela_F.Edit;
    HabDesabBotoesCRUD('P', False, False, False);
    DesabilitaHabilitaCampos('P', True);
  end;
end;

procedure TFCadastroLancamento.btnExcluirDetClick(Sender: TObject);
begin
  inherited;

  if dmLancamento.cdsDetalhe_F.RecordCount > 0 then
    dmLancamento.cdsDetalhe_F.Delete;
end;

procedure TFCadastroLancamento.btnExcluirImagemClick(Sender: TObject);
begin
  inherited;

  if dmLancamento.cdsImagem_F.RecordCount > 0 then
    dmLancamento.cdsImagem_F.Delete;
end;

procedure TFCadastroLancamento.btnExcluirParcelaClick(Sender: TObject);
var
  cPrevParc: Currency;
begin
  inherited;

  cPrevParc := 0;

  if dmLancamento.cdsParcela_F.RecordCount > 0 then
  begin
    cPrevParc := dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency;

    dmLancamento.cdsParcela_F.Delete;
  end;

  if not (dmLancamento.cdsLancamento_F.State in [dsInsert, dsEdit]) then
    dmLancamento.cdsLancamento_F.Edit;

  dmLancamento.cdsLancamento_F.FieldByName('QTD_PARCELAS').AsInteger := (dmLancamento.cdsLancamento_F.FieldByName('QTD_PARCELAS').AsInteger - 1);

  if dmLancamento.cdsLancamento_F.FieldByName('QTD_PARCELAS').AsInteger > 0 then
  begin
    dmLancamento.cdsParcela_F.First;

    while not dmLancamento.cdsParcela_F.Eof do
    begin
      if (dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString = 'P') and
        (cPrevParc > 0) then
      begin
        dmLancamento.cdsParcela_F.Edit;
        dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency := (dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency +
                                                                                cPrevParc);
        dmLancamento.cdsParcela_F.Post;

        cPrevParc := 0;
      end;

      dmLancamento.cdsParcela_F.Next;
    end;
  end;
end;

procedure TFCadastroLancamento.btnIncluirCategoriaClick(Sender: TObject);
var
  Op: TOperacao;
  IdCons: Integer;
  OrigF, OrigC: Boolean;
begin
  inherited;

  Op     := vgOperacao;
  IdCons := vgIdConsulta;
  OrigF  := vgOrigemFiltro;
  OrigC  := vgOrigemCadastro;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := False;
  vgOrigemCadastro := True;

  try
    Application.CreateForm(TFCadastroCategoriaDespRec, FCadastroCategoriaDespRec);

    TipoCat := IfThen((TipoLanc = DESP), 'D', 'R');

    FCadastroCategoriaDespRec.ShowModal;
  finally
    dmLancamento.qryCategoria.Close;
    dmLancamento.qryCategoria.Params.ParamByName('ID_NAT').Value   := dmLancamento.qryNatureza.FieldByName('ID_NATUREZA').AsInteger;
    dmLancamento.qryCategoria.Params.ParamByName('TIPO_CAT').Value := IfThen((TipoLanc = DESP), 'D', 'R');
    dmLancamento.qryCategoria.Open;

    if FCadastroCategoriaDespRec.IdNovaCategoria <> 0 then
      edtCategoria.Text := IntToStr(FCadastroCategoriaDespRec.IdNovaCategoria);
    
    FCadastroCategoriaDespRec.Free;
  end;

  vgOperacao       := Op;
  vgIdConsulta     := IdCons;
  vgOrigemFiltro   := OrigF;
  vgOrigemCadastro := OrigC;

  if Trim(edtCategoria.Text) <> '' then
  begin
    dmLancamento.cdsLancamento_F.FieldByName('ID_CATEGORIA_DESPREC_FK').AsInteger := StrToInt(edtCategoria.Text);

    edtSubCategoria.Enabled        := True;
    lcbSubCategoria.Enabled        := True;
    btnIncluirSubCategoria.Enabled := True;

    dmLancamento.qrySubCategoria.Close;
    dmLancamento.qrySubCategoria.Params.ParamByName('ID_CAT').Value      := dmLancamento.qryCategoria.FieldByName('ID_CATEGORIA_DESPREC').AsInteger;
    dmLancamento.qrySubCategoria.Params.ParamByName('TIPO_SUBCAT').Value := IfThen((TipoLanc = DESP), 'D', 'R');
    dmLancamento.qrySubCategoria.Open;

    if edtSubCategoria.CanFocus then
      edtSubCategoria.SetFocus;
  end
  else
  begin
    dmLancamento.qrySubCategoria.Close;

    edtSubCategoria.Enabled        := False;
    lcbSubCategoria.Enabled        := False;
    btnIncluirSubCategoria.Enabled := False;
  end;
end;

procedure TFCadastroLancamento.btnIncluirDetClick(Sender: TObject);
begin
  inherited;

  dmLancamento.cdsDetalhe_F.Append;
  dmLancamento.cdsDetalhe_F.FieldByName('NOVO').AsBoolean         := True;
  dmLancamento.cdsDetalhe_F.FieldByName('FLG_CANCELADO').AsString := 'N';

  HabDesabBotoesCRUD('D', False, False, False);
  DesabilitaHabilitaCampos('D', True);

  if cbTipoItem.CanFocus then
    cbTipoItem.SetFocus;
end;

procedure TFCadastroLancamento.btnIncluirFornecedorClick(Sender: TObject);
var
  Op: TOperacao;
  IdCons: Integer;
  OrigF, OrigC: Boolean;
begin
  inherited;

  Op     := vgOperacao;
  IdCons := vgIdConsulta;
  OrigF  := vgOrigemFiltro;
  OrigC  := vgOrigemCadastro;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := False;
  vgOrigemCadastro := True;

  try
    Application.CreateForm(TFCadastroClienteFornecedor, FCadastroClienteFornecedor);

    if TipoLanc = DESP then
      FCadastroClienteFornecedor.TipoCliFor := FORN
    else
      FCadastroClienteFornecedor.TipoCliFor := CLIE;

    FCadastroClienteFornecedor.ShowModal;
  finally
    dmLancamento.qryCliFor.Close;
    dmLancamento.qryCliFor.Params.ParamByName('TIPO_CLIFOR').Value   := IfThen((TipoLanc = DESP), 'F', 'C');  //Despesa => Fornecedor (F) / Receita => Cliente (C)
    dmLancamento.qryCliFor.Params.ParamByName('FLG_ATIVO_CF1').Value := IfThen((vgOperacao = I), 'S', '');
    dmLancamento.qryCliFor.Params.ParamByName('FLG_ATIVO_CF2').Value := IfThen((vgOperacao = I), 'S', '');
    dmLancamento.qryCliFor.Open;

    if FCadastroClienteFornecedor.IdNovoCliFor <> 0 then
      edtCliFor.Text := IntToStr(FCadastroClienteFornecedor.IdNovoCliFor);

    FCadastroClienteFornecedor.Free;
  end;

  vgOperacao       := Op;
  vgIdConsulta     := IdCons;
  vgOrigemFiltro   := OrigF;
  vgOrigemCadastro := OrigC;

  if Trim(edtCliFor.Text) <> '' then
  begin
    dmLancamento.cdsLancamento_F.FieldByName('ID_CLIENTE_FORNECEDOR_FK').AsInteger := StrToInt(edtCliFor.Text);

    if edtQtdParcelasLanc.CanFocus then
      edtQtdParcelasLanc.SetFocus;
  end;
end;

procedure TFCadastroLancamento.btnIncluirItemClick(Sender: TObject);
var
  Op: TOperacao;
  IdCons, IdItem: Integer;
  OrigF, OrigC: Boolean;
begin
  inherited;

  IdItem := 0;

  Op     := vgOperacao;
  IdCons := vgIdConsulta;
  OrigF  := vgOrigemFiltro;
  OrigC  := vgOrigemCadastro;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := False;
  vgOrigemCadastro := True;

  try
    Application.CreateForm(TFCadastroProdutoServico, FCadastroProdutoServico);

{    tdlgMensagem.Execute;

    if tdlgMensagem.ModalResult = 100 then
      FCadastroProdutoServico.TipoProdserv := PROD
    else
      FCadastroProdutoServico.TipoProdserv := SERV;  }

    if cbTipoItem.ItemIndex = 0 then
      FCadastroProdutoServico.TipoProdserv := PROD
    else if cbTipoItem.ItemIndex = 1 then
      FCadastroProdutoServico.TipoProdserv := SERV;

    FCadastroProdutoServico.ShowModal;
  finally
    AbrirListaItem;

    if FCadastroProdutoServico.IdNovoItem <> 0 then
      IdItem := FCadastroProdutoServico.IdNovoItem;

    FCadastroProdutoServico.Free;
  end;

  vgOperacao       := Op;
  vgIdConsulta     := IdCons;
  vgOrigemFiltro   := OrigF;
  vgOrigemCadastro := OrigC;

  if IdItem > 0 then
  begin
    dmLancamento.cdsDetalhe_F.FieldByName('ID_ITEM_FK').AsInteger := IdItem;
    dmLancamento.cdsDetalhe_F.FieldByName('VR_UNITARIO').AsCurrency := dmLancamento.qryItem.FieldByName('ULT_VALOR_CUSTO').AsCurrency;

    if cedVlrUnitarioDet.CanFocus then
      cedVlrUnitarioDet.SetFocus;
  end;
end;

procedure TFCadastroLancamento.btnIncluirNaturezaClick(Sender: TObject);
var
  Op: TOperacao;
  IdCons: Integer;
  OrigF, OrigC: Boolean;
begin
  inherited;

  Op     := vgOperacao;
  IdCons := vgIdConsulta;
  OrigF  := vgOrigemFiltro;
  OrigC  := vgOrigemCadastro;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := False;
  vgOrigemCadastro := True;

  try
    Application.CreateForm(TFCadastroNatureza, FCadastroNatureza);

    FCadastroNatureza.TipoNat := TipoLanc;

    FCadastroNatureza.ShowModal;
  finally
    AbrirListaNatureza;

    if FCadastroNatureza.IdNovaNatureza <> 0 then
      edtNatureza.Text := IntToStr(FCadastroNatureza.IdNovaNatureza);

    FCadastroNatureza.Free;
  end;

  vgOperacao       := Op;
  vgIdConsulta     := IdCons;
  vgOrigemFiltro   := OrigF;
  vgOrigemCadastro := OrigC;

  if Trim(edtNatureza.Text) <> '' then
  begin
    dmLancamento.cdsLancamento_F.FieldByName('ID_NATUREZA_FK').AsInteger := StrToInt(edtNatureza.Text);
    cedValorTotalPrev.Enabled := (dmLancamento.cdsLancamento_F.FieldByName('ID_NATUREZA_FK').AsInteger > 7);

    edtCategoria.Enabled        := True;
    lcbCategoria.Enabled        := True;
    btnIncluirCategoria.Enabled := True;

    dmLancamento.qryCategoria.Close;
    dmLancamento.qryCategoria.Params.ParamByName('ID_NAT').Value   := dmLancamento.qryNatureza.FieldByName('ID_NATUREZA').AsInteger;
    dmLancamento.qryCategoria.Params.ParamByName('TIPO_CAT').Value := IfThen((TipoLanc = DESP), 'D', 'R');
    dmLancamento.qryCategoria.Open;

    if edtCategoria.CanFocus then
      edtCategoria.SetFocus;
  end
  else
  begin
    cedValorTotalPrev.Enabled := False;

    dmLancamento.qryCategoria.Close;

    edtCategoria.Enabled        := False;
    lcbCategoria.Enabled        := False;
    btnIncluirCategoria.Enabled := False;
  end;
end;

procedure TFCadastroLancamento.btnIncluirParcelaClick(Sender: TObject);
begin
  inherited;

  dmLancamento.cdsParcela_F.Append;
  dmLancamento.cdsParcela_F.FieldByName('NOVO').AsBoolean                  := True;
  dmLancamento.cdsParcela_F.FieldByName('DATA_LANCAMENTO_PARC').AsDateTime := dteDataLancamento.Date;
  dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger  := 6;  //DINHEIRO - default
  dmLancamento.cdsParcela_F.FieldByName('DATA_VENCIMENTO').AsDateTime      := dteDataLancamento.Date;
  dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString             := 'P';
  dmLancamento.cdsParcela_F.FieldByName('CAD_ID_USUARIO').AsInteger        := vgUsu_Id;
  dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').Value            := Null;
  dmLancamento.cdsParcela_F.FieldByName('PAG_ID_USUARIO').Value            := Null;
  dmLancamento.cdsParcela_F.FieldByName('DATA_CANCELAMENTO').Value         := Null;
  dmLancamento.cdsParcela_F.FieldByName('CANCEL_ID_USUARIO').Value         := Null;
  dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency      := 0;
  dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_JUROS').AsCurrency     := 0;
  dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_DESCONTO').AsCurrency  := 0;
  dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency      := 0;

  HabDesabBotoesCRUD('P', False, False, False);
  DesabilitaHabilitaCampos('P', True);

  if dteDataLancamentoParc.CanFocus then
    dteDataLancamentoParc.SetFocus;
end;

procedure TFCadastroLancamento.btnIncluirSubCategoriaClick(Sender: TObject);
var
  Op: TOperacao;
  IdCons: Integer;
  OrigF, OrigC: Boolean;
begin
  inherited;

  Op     := vgOperacao;
  IdCons := vgIdConsulta;
  OrigF  := vgOrigemFiltro;
  OrigC  := vgOrigemCadastro;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := False;
  vgOrigemCadastro := True;

  try
    Application.CreateForm(TFCadastroSubCategoriaDespRec, FCadastroSubCategoriaDespRec);

    TipoSubCat := IfThen((TipoLanc = DESP), 'D', 'R');

    FCadastroSubCategoriaDespRec.ShowModal;
  finally
    dmLancamento.qrySubCategoria.Close;
    dmLancamento.qrySubCategoria.Params.ParamByName('ID_CAT').Value      := dmLancamento.qryCategoria.FieldByName('ID_CATEGORIA_DESPREC').AsInteger;
    dmLancamento.qrySubCategoria.Params.ParamByName('TIPO_SUBCAT').Value := IfThen((TipoLanc = DESP), 'D', 'R');
    dmLancamento.qrySubCategoria.Open;

    if FCadastroSubCategoriaDespRec.IdNovaSubCategoria <> 0 then    
      edtSubCategoria.Text := IntToStr(FCadastroSubCategoriaDespRec.IdNovaSubCategoria);
      
    FCadastroSubCategoriaDespRec.Free;
  end;

  vgOperacao       := Op;
  vgIdConsulta     := IdCons;
  vgOrigemFiltro   := OrigF;
  vgOrigemCadastro := OrigC;

  if Trim(edtSubCategoria.Text) <> '' then
  begin
    dmLancamento.cdsLancamento_F.FieldByName('ID_SUBCATEGORIA_DESPREC_FK').AsInteger := StrToInt(edtSubCategoria.Text);

    if edtCliFor.CanFocus then
      edtCliFor.SetFocus;
  end;
end;

procedure TFCadastroLancamento.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroLancamento.btnProcurarItemClick(Sender: TObject);
begin
  inherited;

  AbrirListaItem;
end;

procedure TFCadastroLancamento.CalcularLancamento;
var
  cTotalPrev, cTotalJuros, cTotalDesc, cTotalLancamento: Currency;
  iQtdParc: Integer;
begin
  cTotalPrev  := 0;
  cTotalJuros := 0;
  cTotalDesc  := 0;
  cTotalLancamento := 0;
  iQtdParc := 0;

  with dmLancamento do
  begin
    cdsParcela_F.First;

    while not cdsParcela_F.Eof do
    begin
      cTotalPrev       := (cTotalPrev + cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency);
      cTotalJuros      := (cTotalJuros + cdsParcela_F.FieldByName('VR_PARCELA_JUROS').AsCurrency);
      cTotalDesc       := (cTotalDesc + cdsParcela_F.FieldByName('VR_PARCELA_DESCONTO').AsCurrency);
      cTotalLancamento := (cTotalLancamento + cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency);

      Inc(iQtdParc);

      cdsParcela_F.Next;
    end;

    cdsLancamento_F.FieldByName('VR_TOTAL_PREV').AsCurrency     := cTotalPrev;
    cdsLancamento_F.FieldByName('VR_TOTAL_JUROS').AsCurrency    := cTotalJuros;
    cdsLancamento_F.FieldByName('VR_TOTAL_DESCONTO').AsCurrency := cTotalDesc;
    cdsLancamento_F.FieldByName('VR_TOTAL_PAGO').AsCurrency     := cTotalLancamento;

    cdsLancamento_F.FieldByName('QTD_PARCELAS').AsInteger := iQtdParc;
  end;
end;

procedure TFCadastroLancamento.CalcularPagamentoParcela;
begin
  
end;

procedure TFCadastroLancamento.cbSituacaoParcelaClick(Sender: TObject);
begin
  inherited;

  cbSituacaoParcelaExit(Sender);
end;

procedure TFCadastroLancamento.cbSituacaoParcelaExit(Sender: TObject);
var
  sTipo, sNomeUsu, sSenhaUsu: String;
  iIdUsu: Integer;
  lCancelado: Boolean;
  cVlrPago: Currency;
begin
  inherited;

  cVlrPago := 0;

  if not (dmLancamento.cdsParcela_F.State in [dsInsert, dsEdit]) then
    dmLancamento.cdsParcela_F.Edit;

  if vgOperacao = I then
  begin
    case cbSituacaoParcela.ItemIndex of
      0:
        begin
          dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString     := 'P';
          dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').Value    := Null;
          dmLancamento.cdsParcela_F.FieldByName('PAG_ID_USUARIO').Value    := Null;
          dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency := 0;
          dmLancamento.cdsParcela_F.FieldByName('DATA_CANCELAMENTO').Value := Null;
          dmLancamento.cdsParcela_F.FieldByName('CANCEL_ID_USUARIO').Value := Null;
        end;
      1:
        begin
          if cedValorParcelaPago.Value < 0 then
          begin
            Application.MessageBox('Por favor, preencha o VALOR PAGO corretamente.', 'Aviso', MB_OK + MB_ICONWARNING);
            dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString     := 'P';
            dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').Value    := Null;
            dmLancamento.cdsParcela_F.FieldByName('PAG_ID_USUARIO').Value    := Null;
            dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency := 0;
            dmLancamento.cdsParcela_F.FieldByName('DATA_CANCELAMENTO').Value := Null;
            dmLancamento.cdsParcela_F.FieldByName('CANCEL_ID_USUARIO').Value := Null;
          end
          else
          begin
            if cedValorParcelaPago.Value = 0 then
              cVlrPago := 0
            else
              cVlrPago := StrToCurr(ReplaceStr(cedValorParcelaPago.Text, '.', ''));

            if dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency <> ((cVlrPago +
                                                                                        dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_DESCONTO').AsCurrency) -
                                                                                       dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_JUROS').AsCurrency) then
            begin
              dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString     := 'P';
              dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').Value    := Null;
              dmLancamento.cdsParcela_F.FieldByName('PAG_ID_USUARIO').Value    := Null;
              dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency := 0;
              dmLancamento.cdsParcela_F.FieldByName('DATA_CANCELAMENTO').Value := Null;
              dmLancamento.cdsParcela_F.FieldByName('CANCEL_ID_USUARIO').Value := Null;
            end
            else
            begin
              dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString       := 'G';

              if (dteDataPagamentoParc.Date = 0) or
                (dteDataPagamentoParc.Date = Null) then
              begin
                if not chbForaFechCaixa.Checked then  //if chbForaFechCaixa.Checked then
//                  dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := IncDay(Now, 1)
//                else
                begin
                  if (Date < vgDataLancVigente) or
                   (Date < dteDataLancamento.Date) then
                  begin
                    if vgDataLancVigente < dteDataLancamento.Date then
                      dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := dteDataLancamento.Date
                    else
                      dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := vgDataLancVigente;
                  end;
//                  else
//                    dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := Now;
                end;
              end;

              dmLancamento.cdsParcela_F.FieldByName('PAG_ID_USUARIO').AsInteger  := vgUsu_Id;
              dmLancamento.cdsParcela_F.FieldByName('DATA_CANCELAMENTO').Value   := Null;
              dmLancamento.cdsParcela_F.FieldByName('CANCEL_ID_USUARIO').Value   := Null;
            end;
          end;
        end;
      2:
        begin
          dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString          := 'C';
          dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').Value         := Null;
          dmLancamento.cdsParcela_F.FieldByName('PAG_ID_USUARIO').Value         := Null;
          dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency   := 0;
          dmLancamento.cdsParcela_F.FieldByName('DATA_CANCELAMENTO').AsDateTime := Now;
          dmLancamento.cdsParcela_F.FieldByName('CANCEL_ID_USUARIO').Value      := vgUsu_Id;
        end;
    end;
  end
  else
  begin
    lCancelado := False;

    case cbSituacaoParcela.ItemIndex of
      0:
        begin
          if (not dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO_ANT').IsNull) and
            (dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime <>
             dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO_ANT').AsDateTime) then
          begin
            iIdUsu    := 0;
            sNomeUsu  := '';
            sSenhaUsu := '';

            sTipo := IfThen((TipoLanc = DESP), 'Despesa', 'Receita');

            if Application.MessageBox(PChar('Confirma a altera��o da Data de Pagamento dessa Parcela da ' + sTipo + '?'), 'Aviso', MB_YESNO + MB_ICONQUESTION) = ID_YES then
            begin
              iIdUsu    := 0;
              sNomeUsu  := '';
              sSenhaUsu := '';

              repeat
                repeat
                  if not InputQuery('LOGIN', 'Por favor, informe o LOGIN do Autorizador:', sNomeUsu) then
                    lCancelado := True;
                until (Trim(sNomeUsu) <> '') or lCancelado;

                if not lCancelado then
                begin
                  repeat
                    if not InputQuery('SENHA', #31'Por favor, informe a SENHA do Autorizador:', sSenhaUsu) then
                      lCancelado := True;
                  until (Trim(sSenhaUsu) <> '') or lCancelado;
                end;
              until (dmGerencial.VerificarAutorizacao(iIdUsu, sNomeUsu, sSenhaUsu) = True) or lCancelado;

              if not lCancelado then
                BS.GravarUsuarioLog(2, '', ('O usu�rio mudou o status de PAGO da Parcela ' +
                                            Trim(edtNumParcela.Text) +
                                            ' para A PAGAR.'));
            end
            else
              lCancelado := True;
          end;

          if not lCancelado then  //Operacao nao cancelada
          begin
            dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString        := 'P';
            dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').Value       := Null;
            dmLancamento.cdsParcela_F.FieldByName('PAG_ID_USUARIO').Value       := Null;
            dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency := 0;
            dmLancamento.cdsParcela_F.FieldByName('DATA_CANCELAMENTO').Value    := Null;
            dmLancamento.cdsParcela_F.FieldByName('CANCEL_ID_USUARIO').Value    := Null;
          end;
        end;
      1:
        begin
          if cedValorParcelaPago.Value < 0 then
          begin
            Application.MessageBox('Por favor, preencha o VALOR PAGO corretamente.', 'Aviso', MB_OK + MB_ICONWARNING);
            dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString        := 'P';
            dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').Value       := Null;
            dmLancamento.cdsParcela_F.FieldByName('PAG_ID_USUARIO').Value       := Null;
            dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency := 0;
            dmLancamento.cdsParcela_F.FieldByName('DATA_CANCELAMENTO').Value    := Null;
            dmLancamento.cdsParcela_F.FieldByName('CANCEL_ID_USUARIO').Value    := Null;
          end
          else
          begin
            if cedValorParcelaPago.Value = 0 then
              cVlrPago := 0
            else
              cVlrPago := StrToCurr(ReplaceStr(cedValorParcelaPago.Text, '.', ''));

            if dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency <> ((cVlrPago +
                                                                                        dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_DESCONTO').AsCurrency) -
                                                                                       dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_JUROS').AsCurrency) then
            begin
              dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString        := 'P';
              dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').Value       := Null;
              dmLancamento.cdsParcela_F.FieldByName('PAG_ID_USUARIO').Value       := Null;
              dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency := 0;
              dmLancamento.cdsParcela_F.FieldByName('DATA_CANCELAMENTO').Value    := Null;
              dmLancamento.cdsParcela_F.FieldByName('CANCEL_ID_USUARIO').Value    := Null;
            end
            else
            begin
              dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString       := 'G';

              if (dteDataPagamentoParc.Date = 0) or
                (dteDataPagamentoParc.Date = Null) then
              begin
                if not chbForaFechCaixa.Checked then  //if chbForaFechCaixa.Checked then
//                  dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := IncDay(Now, 1)
//                else
                begin
                  if (Date < vgDataLancVigente) or
                    (Date < dteDataLancamento.Date) then
                  begin
                    if vgDataLancVigente < dteDataLancamento.Date then
                      dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := dteDataLancamento.Date
                    else
                      dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := vgDataLancVigente;
                  end;
//                  else
//                    dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := Now;
                end;
              end;

              dmLancamento.cdsParcela_F.FieldByName('PAG_ID_USUARIO').AsInteger  := vgUsu_Id;
              dmLancamento.cdsParcela_F.FieldByName('DATA_CANCELAMENTO').Value   := Null;
              dmLancamento.cdsParcela_F.FieldByName('CANCEL_ID_USUARIO').Value   := Null;
            end;
          end;
        end;
      2:
        begin
          if (not dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO_ANT').IsNull) and
            (dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime <>
             dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO_ANT').AsDateTime) then
          begin
            iIdUsu    := 0;
            sNomeUsu  := '';
            sSenhaUsu := '';

            sTipo := IfThen((TipoLanc = DESP), 'Despesa', 'Receita');

            if Application.MessageBox(PChar('Essa ' + sTipo + ' foi gerada automaticamente e ser� preciso informar LOGIN ' +
                                            'e SENHA de Autorizador para exclu�-la. ' + #13#10 + 'Deseja prosseguir?'), 'Aviso', MB_YESNO + MB_ICONQUESTION) = ID_YES then
            begin
              iIdUsu    := 0;
              sNomeUsu  := '';
              sSenhaUsu := '';

              repeat
                repeat
                  if not InputQuery('LOGIN', 'Por favor, informe o LOGIN do Autorizador:', sNomeUsu) then
                    lCancelado := True;
                until (Trim(sNomeUsu) <> '') or lCancelado;

                if not lCancelado then
                begin
                  repeat
                    if not InputQuery('SENHA', #31'Por favor, informe a SENHA do Autorizador:', sSenhaUsu) then
                      lCancelado := True;
                  until (Trim(sSenhaUsu) <> '') or lCancelado;
                end;
              until (dmGerencial.VerificarAutorizacao(iIdUsu, sNomeUsu, sSenhaUsu) = True) or lCancelado;

              if not lCancelado then
                BS.GravarUsuarioLog(2, '', ('O usu�rio mudou o status de PAGO da Parcela ' +
                                            Trim(edtNumParcela.Text) +
                                            ' para CANCELADO.'));
            end
            else
              lCancelado := True;
          end;

          if not lCancelado then  //Operacao nao cancelada
          begin
            dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString          := 'C';
            dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').Value         := Null;
            dmLancamento.cdsParcela_F.FieldByName('PAG_ID_USUARIO').Value         := Null;
            dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency   := 0;
            dmLancamento.cdsParcela_F.FieldByName('DATA_CANCELAMENTO').AsDateTime := Now;
            dmLancamento.cdsParcela_F.FieldByName('CANCEL_ID_USUARIO').AsInteger  := vgUsu_Id;
          end;
        end;
    end;
  end;

  if dteDataPagamentoParc.CanFocus then
    dteDataPagamentoParc.SetFocus;
end;

procedure TFCadastroLancamento.cedAcoterjDetKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if Application.MessageBox('Deseja incluir alguma Imagem?', 'Confirma��o', MB_YESNO + MB_ICONQUESTION) = ID_YES then
      btnAbaDigitalizacao.Click
    else
      btnOk.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroLancamento.cedQuantidadeDetExit(Sender: TObject);
begin
  inherited;

  if (cedVlrUnitarioDet.Value > 0) and
    (cedQuantidadeDet.Value > 0) then
  begin
    dmLancamento.cdsDetalhe_F.FieldByName('VR_TOTAL').AsCurrency := (cedVlrUnitarioDet.Value * cedQuantidadeDet.Value);
  end
  else
    dmLancamento.cdsDetalhe_F.FieldByName('VR_TOTAL').AsCurrency := 0;
end;

procedure TFCadastroLancamento.cedValorTotalPrevExit(Sender: TObject);
begin
  inherited;

  if (Trim(edtNatureza.Text) <> '') and
    (Trim(lcbNatureza.Text) <> '') then
  begin
    if StrToInt(edtQtdParcelasLanc.Text) <= 0 then
      Application.MessageBox('A Quantidade de Parcelas n�o pode ser menor ou igual a 0 (zero).', 'Aviso', MB_OK + MB_ICONWARNING);
  end;
end;

procedure TFCadastroLancamento.cedVlrUnitarioDetExit(Sender: TObject);
begin
  inherited;

  if dmLancamento.cdsDetalhe_F.State in [dsInsert, dsEdit] then
  begin
    if (cedVlrUnitarioDet.Value > 0) and
      (cedQuantidadeDet.Value > 0) then
    begin
      dmLancamento.cdsDetalhe_F.FieldByName('VR_TOTAL').AsCurrency := (cedVlrUnitarioDet.Value * cedQuantidadeDet.Value);
    end
    else
      dmLancamento.cdsDetalhe_F.FieldByName('VR_TOTAL').AsCurrency := 0;
  end;
end;

procedure TFCadastroLancamento.chbAuxiliarClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(chbAuxiliar, 1, 'Livro Auxiliar',
  'Marque essa op��o para incluir o Lan�amento no relat�rio do Livro Auxiliar.', clBlue, clNavy);
end;

procedure TFCadastroLancamento.chbCanceladaLancKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    if chbCanceladaLanc.Checked then
      dmLancamento.cdsLancamento_F.FieldByName('DATA_CANCELAMENTO').AsDateTime := Now
    else
      dmLancamento.cdsLancamento_F.FieldByName('DATA_CANCELAMENTO').Value := Null;

    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if chbCanceladaLanc.Checked then
      dmLancamento.cdsLancamento_F.FieldByName('DATA_CANCELAMENTO').AsDateTime := Now
    else
      dmLancamento.cdsLancamento_F.FieldByName('DATA_CANCELAMENTO').Value := Null;

    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroLancamento.chbCanceladoDetKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnConfirmarDet.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroLancamento.chbFlutuanteClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(chbFlutuante, 1, 'Flutuante',
  'Marque essa op��o para incluir esse Lan�amento no relat�rio de Flutuantes', clBlue, clNavy);
end;

procedure TFCadastroLancamento.chbForaFechCaixaClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(chbAuxiliar, 1, 'Fora Fech. Caixa (Fora do Movimento de Caixa)',
  'Marque essa op��o para excluir esse Lan�amento do Fechamento de Caixa.', clBlue, clNavy);
end;

procedure TFCadastroLancamento.chbImpostoRendaClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(chbImpostoRenda, 1, 'Imposto de Renda',
  'Marque essa op��o para incluir esse Lan�amento no relat�rio do Imposto de Renda.', clBlue, clNavy);
end;

procedure TFCadastroLancamento.chbPessoalClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(chbPessoal, 1, 'Contas Pessoais',
  'Marque essa op��o para incluir esse Lan�amento no relat�rio das Contas Pessoais.', clBlue, clNavy);
end;

procedure TFCadastroLancamento.chbRecorrenteClick(Sender: TObject);
begin
  inherited;

  dmGerencial.MostrarBalaoInformativo(chbRecorrente, 1, 'Despesas Recorrentes',
  'Marque essa op��o para repetir essa despesa nos pr�ximos meses.', clBlue, clNavy);
end;

procedure TFCadastroLancamento.dbgDetalhesDblClick(Sender: TObject);
begin
  inherited;

  if vgOperacao in [I, E] then
  begin
    if not (dmLancamento.cdsLancamento_F.FieldByName('ID_NATUREZA_FK').AsInteger in [1, 2, 3, 4, 5, 6, 7]) then
      btnEditarDet.Click;
  end;
end;

procedure TFCadastroLancamento.dbgDetalhesDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;

  AlternarCorGrid(Sender, Rect, DataCol, Column, State);
end;

procedure TFCadastroLancamento.dbgImagensColEnter(Sender: TObject);
begin
  inherited;

  if dmLancamento.cdsImagem_F.FieldByName('CAMINHO_COMPLETO').AsString <> '' then
  begin
    if dmLancamento.cdsImagem_F.FieldByName('EXTENSAO').AsString = 'jpg' then
    begin
      ntbArquivo.PageIndex := 0;
      imgImagemLanc.Picture.LoadFromFile(dmLancamento.cdsImagem_F.FieldByName('CAMINHO_COMPLETO').AsString);
    end
    else if dmLancamento.cdsImagem_F.FieldByName('EXTENSAO').AsString = 'pdf' then
    begin
      ntbArquivo.PageIndex := 1;
      docPDF.Reset;
      docPDF.LoadFromFile(dmLancamento.cdsImagem_F.FieldByName('CAMINHO_COMPLETO').AsString);
      vwPDF.Align := alClient;
      vwPDF.PDFDocument := docPDF;
      vwPDF.Active := True;
    end;
  end;
end;

procedure TFCadastroLancamento.dbgImagensDblClick(Sender: TObject);
begin
  inherited;

  if vgOperacao in [I, E] then
    btnEditarImagem.Click;
end;

procedure TFCadastroLancamento.dbgImagensDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;

  AlternarCorGrid(Sender, Rect, DataCol, Column, State);
end;

procedure TFCadastroLancamento.dbgParcelasDblClick(Sender: TObject);
begin
  inherited;

  if vgOperacao in [I, E] then
  begin
    if (dmLancamento.cdsLancamento_F.FieldByName('FLG_STATUS').AsString = 'P') and
      (dmLancamento.cdsLancamento_F.FieldByName('FLG_CANCELADO').AsString = 'N') then
      btnEditarParcela.Click;
  end;
end;

procedure TFCadastroLancamento.dbgParcelasDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Grid: TDBGrid;
  Row: Integer;
begin
  inherited;

  Grid := Sender as TDBGrid;
  Row  := Grid.DataSource.DataSet.RecNo;

  if gdSelected in State then
    Grid.Canvas.Brush.Color := clSkyBlue
  else
  begin
    if Odd(Row) then
      Grid.Canvas.Brush.Color := $FFFFF0
    else
      Grid.Canvas.Brush.Color := $D3D3D3;
  end;

  Grid.DefaultDrawColumnCell(Rect, DataCol, Column, State);

  //Legenda
  if dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString = 'C' then  //Cancelada
  begin
    dbgParcelas.Canvas.Font.Color := clGray;
    dbgParcelas.Canvas.FillRect(Rect);
    dbgParcelas.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end
  else if dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString = 'P' then  //Pendente (A Pagar)
  begin
    if dmLancamento.cdsParcela_F.FieldByName('DATA_VENCIMENTO').AsDateTime > Date then  //A Vencer
    begin
      dbgParcelas.Canvas.Font.Color := clBlack;
      dbgParcelas.Canvas.FillRect(Rect);
      dbgParcelas.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end
    else if dmLancamento.cdsParcela_F.FieldByName('DATA_VENCIMENTO').AsDateTime = Date then  //Vence Hoje
    begin
      dbgParcelas.Canvas.Font.Color := clGreen;
      dbgParcelas.Canvas.Font.Style := [fsBold];
      dbgParcelas.Canvas.FillRect(Rect);
      dbgParcelas.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end
    else if dmLancamento.cdsParcela_F.FieldByName('DATA_VENCIMENTO').AsDateTime < Date then  //Vencido
    begin
      dbgParcelas.Canvas.Font.Color := clMaroon;
      dbgParcelas.Canvas.Font.Style := [fsBold];
      dbgParcelas.Canvas.FillRect(Rect);
      dbgParcelas.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;
  end
  else if dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString = 'G' then  //Paga
  begin
    dbgParcelas.Canvas.Font.Color := clNavy;
    dbgParcelas.Canvas.FillRect(Rect);
    dbgParcelas.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TFCadastroLancamento.DefinirTamanhoMaxCampos;
begin
  inherited;
  //
end;

procedure TFCadastroLancamento.DesabilitaHabilitaCampos(Tipo: String;
  Hab: Boolean);
begin
  case AnsiIndexStr(UpperCase(Tipo), ['P', 'D', 'M']) of
    0:  //PARCELA
      begin
        dteDataLancamentoParc.Enabled   := Hab and (vgOperacao = I);
        edtNumParcela.Enabled           := Hab;
        dteDataVencimento.Enabled       := Hab;
        cedValorParcelaPrev.Enabled     := Hab;
        cedValorParcelaJuros.Enabled    := Hab;
        cedValorParcelaDesconto.Enabled := Hab;
        cedValorParcelaPago.Enabled     := Hab;
        btnAtualizarPagamento.Enabled   := Hab;
        cbSituacaoParcela.Enabled       := False;
        dteDataPagamentoParc.Enabled    := Hab;
        edtAgenciaParc.Enabled          := Hab;
        edtContaParc.Enabled            := Hab;
        lcbBancoParc.Enabled            := Hab;
        edtNumeroCheque.Enabled         := Hab;
        edtNumCodDeposito.Enabled       := False;
        dteDataCancelamentoParc.Enabled := False;
        edtFormaPagamento.Enabled       := Hab;
        lcbFormaPagamento.Enabled       := Hab;
        mmObsParcela.Enabled            := Hab;

        dbgParcelas.Enabled := not Hab;
      end;
    1:  //DETALHE
      begin
        cbTipoItem.Enabled         := Hab;
        btnProcurarItem.Enabled    := Hab;
        lcbItemDet.Enabled         := False;
        btnIncluirItem.Enabled     := Hab;
        cedVlrUnitarioDet.Enabled  := Hab;
        cedQuantidadeDet.Enabled   := Hab;
        cedVlrTotalDet.Enabled     := Hab;
        edtNumProtocolo.Enabled    := False;
        chbCanceladoDet.Enabled    := Hab;
        medSeloDet.Enabled         := Hab;
        medAleatorioDet.Enabled    := Hab;
        medTipocobrancaDet.Enabled := Hab;
        edtCodAdicionalDet.Enabled := Hab;
        cedEmolumentosDet.Enabled  := Hab;
        cedFETJDet.Enabled         := Hab;
        cedFUNDPERJDet.Enabled     := Hab;
        cedFUNPERJDet.Enabled      := Hab;
        cedFUNARPENDet.Enabled     := Hab;
        cedPMCMVDet.Enabled        := Hab;
        cedISSDet.Enabled          := Hab;
        cedMutuaDet.Enabled        := Hab;
        cedAcoterjDet.Enabled      := Hab;

        dbgDetalhes.Enabled := not Hab;
      end;
    2:  //IMAGEM
      begin
        edtNomeArquivo.Enabled := Hab;

        dbgImagens.Enabled := not Hab;
      end;
  end;
end;

procedure TFCadastroLancamento.DesabilitarComponentes;
begin
  inherited;

  if TipoLanc = DESP then
  begin
    btnOk.Enabled       := vgPrm_IncDesp and vgPrm_EdDesp and
                           (vgOperacao in [I, E, C]);
    btnCancelar.Enabled := vgPrm_IncDesp and vgPrm_EdDesp and
                           (vgOperacao in [I, E, C]);
  end;

  if TipoLanc = REC then
  begin
    btnOk.Enabled       := vgPrm_IncRec and vgPrm_EdRec and
                           (vgOperacao in [I, E, C]);
    btnCancelar.Enabled := vgPrm_IncRec and vgPrm_EdRec and
                           (vgOperacao in [I, E, C]);
  end;

  { LANCAMENTO }
  edtCodigo.Enabled               := False;
  edtAno.Enabled                  := False;
  dteDataLancamento.Enabled       := (vgOperacao = I);
  cedValorTotalJuros.Enabled      := False;
  cedValorTotalDesconto.Enabled   := False;
  cedValorTotalPago.Enabled       := False;
  chbImpostoRenda.Enabled         := False;
  chbAuxiliar.Enabled             := False;
  chbPessoal.Enabled              := False;
  lcbCarneLeao.Enabled            := False;
  lblCarneLeao.Visible            := not dmLancamento.cdsLancamento_F.FieldByName('ID_CARNELEAO_EQUIVALENCIA_FK').IsNull;
  lcbCarneLeao.Visible            := not dmLancamento.cdsLancamento_F.FieldByName('ID_CARNELEAO_EQUIVALENCIA_FK').IsNull;
  dteDataCancelamentoLanc.Enabled := False;

  lblCodigo.Visible := (vgOperacao <> I);
  edtCodigo.Visible := (vgOperacao <> I);
  Label3.Visible    := (vgOperacao <> I);
  lblAno.Visible    := (vgOperacao <> I);
  edtAno.Visible    := (vgOperacao <> I);

  { PARCELA }
  cbSituacaoParcela.Enabled       := False;
  dteDataCancelamentoParc.Enabled := False;
  edtNumCodDeposito.Enabled       := False;
  
  if vgOperacao = C then
  begin
    { LANCAMENTO }
    dteDataLancamento.Enabled      := False;
    edtCliFor.Enabled              := False;
    lcbCliFor.Enabled              := False;
    btnIncluirFornecedor.Enabled   := False;
    edtCategoria.Enabled           := False;
    lcbCategoria.Enabled           := False;
    btnIncluirCategoria.Enabled    := False;
    edtSubCategoria.Enabled        := False;
    lcbSubCategoria.Enabled        := False;
    btnIncluirSubCategoria.Enabled := False;
    edtNatureza.Enabled            := False;
    lcbNatureza.Enabled            := False;
    btnIncluirNatureza.Enabled     := False;
    cedValorTotalPrev.Enabled      := False;
    chbFlutuante.Enabled           := False;
    chbForaFechCaixa.Enabled       := False;
    edtQtdParcelasLanc.Enabled     := False;
    chbRecorrente.Enabled          := False;
    chbCanceladaLanc.Enabled       := False;
    rgSituacaoLanc.Enabled         := False;

    mmObsLancamento.ReadOnly := True;

    { DETALHE }
    cbTipoItem.Enabled        := False;
    btnProcurarItem.Enabled   := False;
    lcbItemDet.Enabled        := False;
    btnIncluirItem.Enabled    := False;
    cedVlrUnitarioDet.Enabled := False;
    cedQuantidadeDet.Enabled  := False;
    cedVlrTotalDet.Enabled    := False;
    edtNumProtocolo.Enabled   := False;
    chbCanceladoDet.Enabled   := False;

    { PARCELA }
    dteDataLancamentoParc.Enabled   := False;
    edtNumParcela.Enabled           := False;
    dteDataVencimento.Enabled       := False;
    cedValorParcelaPrev.Enabled     := False;
    cedValorParcelaJuros.Enabled    := False;
    cedValorParcelaDesconto.Enabled := False;
    cedValorParcelaPago.Enabled     := False;
    btnAtualizarPagamento.Enabled   := False;
    cbSituacaoParcela.Enabled       := False;
    dteDataPagamentoParc.Enabled    := False;
    edtAgenciaParc.Enabled          := False;
    edtContaParc.Enabled            := False;
    lcbBancoParc.Enabled            := False;
    edtNumeroCheque.Enabled         := False;
    edtFormaPagamento.Enabled       := False;
    lcbFormaPagamento.Enabled       := False;

    mmObsParcela.ReadOnly := True;

    btnIncluirParcela.Enabled    := False;
    btnEditarParcela.Enabled     := False;
    btnExcluirParcela.Enabled    := False;
    btnConfirmarParcela.Enabled  := False;
    btnCancelarParcela.Enabled   := False;

    dbgParcelas.ReadOnly := True;

    { IMAGEM }
    btnAcionarScanner.Enabled   := False;
    btnAbrirPastaImagem.Enabled := False;
    btnColarImagem.Enabled      := False;
    btnAssociarPDF.Enabled      := False;
    btnConfirmarAcao.Enabled    := False;
    btnCancelarAcao.Enabled     := False;
    edtNomeArquivo.Enabled      := False;
    btnEditarImagem.Enabled     := False;
    btnExcluirImagem.Enabled    := False;

    dbgImagens.ReadOnly := True;
  end  
  else
  begin
    btnAbaDetalhe.Visible       := not lSomentePagamento;
    btnAbaDigitalizacao.Visible := not lSomentePagamento;

    { LANCAMENTO }
    //CLIENTE - FORNECEDOR
    edtCliFor.Enabled              := ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'M') or
                                       ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'A') and
                                        (TipoLanc = DESP))) and
                                      (not lSomentePagamento);
    lcbCliFor.Enabled              := ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'M') or
                                       ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'A') and
                                        (TipoLanc = DESP))) and
                                      (not lSomentePagamento);
    btnIncluirFornecedor.Enabled   := ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'M') or
                                       ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'A') and
                                        (TipoLanc = DESP))) and
                                      (not lSomentePagamento);

    //CATEGORIA
    edtCategoria.Enabled           := ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'M') or
                                       ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'A') and
                                        (TipoLanc = DESP))) and
                                      (not lSomentePagamento) and
                                      (vgOperacao = E);
    lcbCategoria.Enabled           := ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'M') or
                                       ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'A') and
                                        (TipoLanc = DESP))) and
                                      (not lSomentePagamento) and
                                      (vgOperacao = E);
    btnIncluirCategoria.Enabled    := ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'M') or
                                       ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'A') and
                                        (TipoLanc = DESP))) and
                                      (not lSomentePagamento) and
                                      (vgOperacao = E);

    //SUBCATEGORIA
    edtSubCategoria.Enabled        := (vgOperacao = E) and
                                      (not lSomentePagamento) and
                                      ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'M') or
                                       ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'A') and
                                        (TipoLanc = DESP)));
    lcbSubCategoria.Enabled        := (vgOperacao = E) and
                                      (not lSomentePagamento) and
                                      ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'M') or
                                       ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'A') and
                                        (TipoLanc = DESP)));
    btnIncluirSubCategoria.Enabled := (vgOperacao = E) and
                                      (not lSomentePagamento) and
                                      ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'M') or
                                       ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'A') and
                                        (TipoLanc = DESP)));

    //NATUREZA
    edtNatureza.Enabled            := ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'M') or
                                       ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'A') and
                                        (TipoLanc = DESP))) and
                                      (not lSomentePagamento);
    lcbNatureza.Enabled            := ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'M') or
                                       ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'A') and
                                        (TipoLanc = DESP))) and
                                      (not lSomentePagamento);
    btnIncluirNatureza.Enabled     := ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'M') or
                                       ((dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'A') and
                                        (TipoLanc = DESP))) and
                                      (not lSomentePagamento);

    cedValorTotalPrev.Enabled  := (dmLancamento.cdsLancamento_F.FieldByName('ID_NATUREZA_FK').AsInteger > 7) and
                                  (not lSomentePagamento);

    chbFlutuante.Enabled       := (not lSomentePagamento) and
                                  (TipoLanc = REC);

    chbForaFechCaixa.Enabled   := (not lSomentePagamento);
    edtQtdParcelasLanc.Enabled := (not lSomentePagamento);
    
    chbRecorrente.Enabled := (TipoLanc = DESP) and  //Despesa
                             (dmLancamento.cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString = 'M') and
                             (dmLancamento.cdsLancamento_F.FieldByName('ID_NATUREZA_FK').AsInteger <> 7) and
                             (not lSomentePagamento);  //Manual
                  
    chbCanceladaLanc.Enabled := (rgSituacaoLanc.ItemIndex = 0) and
                                (not lSomentePagamento);
    mmObsLancamento.Enabled  := (not lSomentePagamento);

    { PARCELA }
    dteDataLancamentoParc.Enabled   := (vgOperacao = I);
    edtNumParcela.Enabled           := True;
    dteDataVencimento.Enabled       := True;
    cedValorParcelaPrev.Enabled     := True;
    cedValorParcelaJuros.Enabled    := True;
    cedValorParcelaDesconto.Enabled := True;
    cedValorParcelaPago.Enabled     := True;
    btnAtualizarPagamento.Enabled   := True;
    dteDataPagamentoParc.Enabled    := True;
    edtAgenciaParc.Enabled          := (dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger in [2, 3, 5]);
    edtContaParc.Enabled            := (dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger in [2, 3, 5]);
    lcbBancoParc.Enabled            := (dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger in [2, 3, 5]);
    edtNumeroCheque.Enabled         := (dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger = 3);
    edtFormaPagamento.Enabled       := True;
    lcbFormaPagamento.Enabled       := True;
    mmObsParcela.Enabled            := True;

    if dmLancamento.cdsParcela_F.RecordCount = 0 then
      HabDesabBotoesCRUD('P', True, False, False)
    else
      HabDesabBotoesCRUD('P', True, True, True);

    DesabilitaHabilitaCampos('P', False);
  end;
end;

procedure TFCadastroLancamento.dteDataLancamentoExit(Sender: TObject);
begin
  inherited;

  if (vgOperacao = I) and
    (dmLancamento.cdsLancamento_F.State in [dsInsert, dsEdit]) then
  begin
    if not chbForaFechCaixa.Checked then
    begin
      if dteDataLancamento.Date < vgDataLancVigente then
        dteDataLancamento.Date := vgDataLancVigente;
    end;

    if (Trim(edtNatureza.Text) <> '') and
      (Trim(lcbNatureza.Text) <> '') and
      (Trim(edtQtdParcelasLanc.Text) <> '') then
    begin
      if StrToInt(edtQtdParcelasLanc.Text) > 0 then
        GerarParcelas;
    end;
  end;
end;

procedure TFCadastroLancamento.dteDataLancamentoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if (vgOperacao = I) and
      (dmLancamento.cdsLancamento_F.State in [dsInsert, dsEdit]) then
    begin
      if not chbForaFechCaixa.Checked then
      begin
        if dteDataLancamento.Date < vgDataLancVigente then
          dteDataLancamento.Date := vgDataLancVigente;
      end;

      if (Trim(edtNatureza.Text) <> '') and
        (Trim(lcbNatureza.Text) <> '') and
        (Trim(edtQtdParcelasLanc.Text) <> '') then
      begin
        if StrToInt(edtQtdParcelasLanc.Text) > 0 then
          GerarParcelas;
      end;
    end;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroLancamento.dteDataLancamentoParcExit(Sender: TObject);
begin
  inherited;

  if (vgOperacao in [I, E]) and
    (dmLancamento.cdsParcela_F.State in [dsInsert, dsEdit]) then
  begin
    if not chbForaFechCaixa.Checked then
    begin
      if (dteDataLancamentoParc.Date < vgDataLancVigente) or
        (dteDataLancamentoParc.Date < dteDataLancamento.Date) then
      begin
        if vgDataLancVigente < dteDataLancamento.Date then
          dteDataLancamentoParc.Date := dteDataLancamento.Date
        else
          dteDataLancamentoParc.Date := vgDataLancVigente;
      end;
    end;
  end;
end;

procedure TFCadastroLancamento.dteDataLancamentoParcKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if (vgOperacao in [I, E]) and
      (dmLancamento.cdsParcela_F.State in [dsInsert, dsEdit]) then
    begin
      if not chbForaFechCaixa.Checked then
      begin
        if (dteDataLancamentoParc.Date < vgDataLancVigente) or
          (dteDataLancamentoParc.Date < dteDataLancamento.Date) then
        begin
          if vgDataLancVigente < dteDataLancamento.Date then
            dteDataLancamentoParc.Date := dteDataLancamento.Date
          else
            dteDataLancamentoParc.Date := vgDataLancVigente;
        end;
      end;
    end;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroLancamento.dteDataPagamentoParcExit(Sender: TObject);
var
  iIdUsu: Integer;
  sNomeUsu, sSenhaUsu: String;
  lCancelado: Boolean;
  cVlrPago: Currency;
begin
  inherited;     

  if (vgOperacao in [I, E]) and
      (dmLancamento.cdsParcela_F.State in [dsInsert, dsEdit]) then
  begin
    if not chbForaFechCaixa.Checked then
    begin
      if (dteDataPagamentoParc.Date > 0) and
        (dteDataPagamentoParc.Date <> Null) and
        (Trim(dteDataPagamentoParc.Text) <> '/  /') then
      begin
        if (dteDataPagamentoParc.Date < vgDataLancVigente) or
          (dteDataPagamentoParc.Date < dteDataLancamento.Date) then
        begin
          if vgDataLancVigente < dteDataLancamento.Date then
            dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := dteDataLancamento.Date
          else
            dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := vgDataLancVigente;
        end;
      end
      else
      begin
        dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString  := 'P';
        dmLancamento.cdsParcela_F.FieldByName('PAG_ID_USUARIO').Value := Null;
      end;
    end;
  end;

(*  cVlrPago := 0;

  if not (dmLancamento.cdsParcela_F.State in [dsInsert, dsEdit]) then
    dmLancamento.cdsParcela_F.Edit;

  dmLancamento.cdsParcela_F.FieldByName('DATA_CANCELAMENTO').Value    := Null;
  dmLancamento.cdsParcela_F.FieldByName('CANCEL_ID_USUARIO').Value    := Null;

  if cedValorParcelaPago.Value < 0 then
  begin
    Application.MessageBox('Por favor, preencha o VALOR PAGO corretamente.', 'Aviso', MB_OK + MB_ICONWARNING);
    cbSituacaoParcela.ItemIndex := 0;
    dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency := 0;
    dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString        := 'P';
    dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').Value       := Null;
  end
  else
  begin
    if cedValorParcelaPago.Value = 0 then
      cVlrPago := 0
    else
      cVlrPago := StrToCurr(ReplaceStr(cedValorParcelaPago.Text, '.', ''));

    if dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency <> ((cVlrPago +
                                                                                dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_DESCONTO').AsCurrency) -
                                                                               dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_JUROS').AsCurrency) then
    begin
      cbSituacaoParcela.ItemIndex := 0;
      dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency := 0;
      dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString        := 'P';
      dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').Value       := Null;
    end
    else
    begin
      cbSituacaoParcela.ItemIndex := 1;

      if (dteDataPagamentoParc.Date = 0) or
        (dteDataPagamentoParc.Date = Null) then
      begin
        if chbForaFechCaixa.Checked then
          dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := IncDay(Now, 1)
        else
        begin
          if (Date < vgDataLancVigente) or
            (Date < dteDataLancamento.Date) then
          begin
            if vgDataLancVigente < dteDataLancamento.Date then
              dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := dteDataLancamento.Date
            else
              dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := vgDataLancVigente;
          end
          else
            dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := Now;
        end;
      end;

      dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString        := 'G';
      dmLancamento.cdsParcela_F.FieldByName('PAG_ID_USUARIO').AsInteger  := vgUsu_Id;
    end;
  end;

  if (vgOperacao = E) and vgPrm_EdLancPago then
  begin
    if (dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime <>
      dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO_ANT').AsDateTime) then
    begin
      if vgUsu_FlgMaster = 'N' then
      begin
        if not (dmLancamento.cdsParcela_F.State in [dsInsert, dsEdit]) then
          dmLancamento.cdsParcela_F.Edit;

        dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO_ANT').AsDateTime;
      end;
    end;
  end;  *)
end;

procedure TFCadastroLancamento.dteDataPagamentoParcKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if (vgOperacao in [I, E]) and
      (dmLancamento.cdsParcela_F.State in [dsInsert, dsEdit]) then
    begin
      if not chbForaFechCaixa.Checked then
      begin
        if (dteDataPagamentoParc.Date > 0) and
          (dteDataPagamentoParc.Date <> Null) and
          (Trim(dteDataPagamentoParc.Text) <> '/  /') then
        begin
          if (dteDataPagamentoParc.Date < vgDataLancVigente) or
            (dteDataPagamentoParc.Date < dteDataLancamento.Date) then
          begin
            if vgDataLancVigente < dteDataLancamento.Date then
              dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := dteDataLancamento.Date
            else
              dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := vgDataLancVigente;
          end;
        end
        else
        begin
          dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString  := 'P';
          dmLancamento.cdsParcela_F.FieldByName('PAG_ID_USUARIO').Value := Null;
        end;
      end;
    end;

    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroLancamento.edtCategoriaExit(Sender: TObject);
begin
  inherited;

  if Trim(edtCategoria.Text) <> '' then
  begin
    dmLancamento.cdsLancamento_F.FieldByName('ID_CATEGORIA_DESPREC_FK').AsInteger := StrToInt(edtCategoria.Text);

    edtSubCategoria.Enabled        := True;
    lcbSubCategoria.Enabled        := True;
    btnIncluirSubCategoria.Enabled := True;

    dmLancamento.qrySubCategoria.Close;
    dmLancamento.qrySubCategoria.Params.ParamByName('ID_CAT').Value      := dmLancamento.qryCategoria.FieldByName('ID_CATEGORIA_DESPREC').AsInteger;
    dmLancamento.qrySubCategoria.Params.ParamByName('TIPO_SUBCAT').Value := IfThen((TipoLanc = DESP), 'D', 'R');
    dmLancamento.qrySubCategoria.Open;

    if edtSubCategoria.CanFocus then
      edtSubCategoria.SetFocus;
  end
  else
  begin
    edtSubCategoria.Clear;
    lcbSubCategoria.KeyValue := -1;

    edtSubCategoria.Enabled        := False;
    lcbSubCategoria.Enabled        := False;
    btnIncluirSubCategoria.Enabled := False;
  end;
end;

procedure TFCadastroLancamento.edtCliForExit(Sender: TObject);
begin
  inherited;

  if Trim(edtCliFor.Text) <> '' then
    dmLancamento.cdsLancamento_F.FieldByName('ID_CLIENTE_FORNECEDOR_FK').AsInteger := StrToInt(edtCliFor.Text);
end;

procedure TFCadastroLancamento.edtFormaPagamentoExit(Sender: TObject);
begin
  inherited;

  if Trim(edtFormaPagamento.Text) <> '' then
  begin
    if dmLancamento.cdsParcela_F.State in [dsInsert, dsEdit] then
      dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger := StrToInt(edtFormaPagamento.Text);

    edtAgenciaParc.Enabled    := (dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger in [2, 3, 5]);
    edtContaParc.Enabled      := (dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger in [2, 3, 5]);
    lcbBancoParc.Enabled      := (dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger in [2, 3, 5]);
    edtNumeroCheque.Enabled   := (dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger = 3);

    if dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger <> 5 then
    begin
      if dmLancamento.cdsParcela_F.State in [dsInsert, dsEdit] then
        dmLancamento.cdsParcela_F.FieldByName('NUM_COD_DEPOSITO').Value := Null;
    end;
  end
  else
  begin
    edtAgenciaParc.Enabled    := False;
    edtContaParc.Enabled      := False;
    lcbBancoParc.Enabled      := False;
    edtNumeroCheque.Enabled   := False;

    if dmLancamento.cdsParcela_F.State in [dsInsert, dsEdit] then
      dmLancamento.cdsParcela_F.FieldByName('NUM_COD_DEPOSITO').Value := Null;
  end;
end;

procedure TFCadastroLancamento.edtNaturezaExit(Sender: TObject);
begin
  inherited;

  if Trim(edtNatureza.Text) <> '' then
  begin
    cedValorTotalPrev.Enabled := (StrToInt(edtNatureza.Text) > 7);
    dmLancamento.cdsLancamento_F.FieldByName('ID_NATUREZA_FK').AsInteger  := StrToInt(edtNatureza.Text);
    dmLancamento.cdsLancamento_F.FieldByName('FLG_IMPOSTORENDA').AsString := dmLancamento.qryNatureza.FieldByName('FLG_IMPOSTORENDA').AsString;
    dmLancamento.cdsLancamento_F.FieldByName('FLG_AUXILIAR').AsString     := dmLancamento.qryNatureza.FieldByName('FLG_AUXILIAR').AsString;
    dmLancamento.cdsLancamento_F.FieldByName('FLG_PESSOAL').AsString      := dmLancamento.qryNatureza.FieldByName('FLG_PESSOAL').AsString;

    edtCategoria.Enabled        := True;
    lcbCategoria.Enabled        := True;
    btnIncluirCategoria.Enabled := True;

    dmLancamento.qryCategoria.Close;
    dmLancamento.qryCategoria.Params.ParamByName('ID_NAT').Value   := dmLancamento.qryNatureza.FieldByName('ID_NATUREZA').AsInteger;
    dmLancamento.qryCategoria.Params.ParamByName('TIPO_CAT').Value := IfThen((TipoLanc = DESP), 'D', 'R');
    dmLancamento.qryCategoria.Open;

    if edtCategoria.CanFocus then
      edtCategoria.SetFocus;
  end
  else
  begin
    dmLancamento.cdsLancamento_F.FieldByName('FLG_IMPOSTORENDA').AsString := 'N';
    dmLancamento.cdsLancamento_F.FieldByName('FLG_AUXILIAR').AsString     := 'N';
    dmLancamento.cdsLancamento_F.FieldByName('FLG_PESSOAL').AsString      := 'N';

    cedValorTotalPrev.Enabled := False;

    edtCategoria.Clear;
    lcbCategoria.KeyValue := -1;

    edtCategoria.Enabled        := False;
    lcbCategoria.Enabled        := False;
    btnIncluirCategoria.Enabled := False;
  end;
end;

procedure TFCadastroLancamento.edtNomeArquivoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
     SelectNext(ActiveControl, True, True);
     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnConfirmarAcao.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroLancamento.edtQtdParcelasLancExit(Sender: TObject);
begin
  inherited;

  if (Trim(edtNatureza.Text) <> '') and
    (Trim(lcbNatureza.Text) <> '') then
  begin
    if StrToInt(edtQtdParcelasLanc.Text) <= 0 then
      Application.MessageBox('A Quantidade de Parcelas n�o pode ser menor ou igual a 0 (zero).', 'Aviso', MB_OK + MB_ICONWARNING)
    else
    begin
      if vgOperacao = I then
        rgSituacaoLanc.ItemIndex := 1;
    end;
  end;
end;

procedure TFCadastroLancamento.edtSubCategoriaExit(Sender: TObject);
begin
  inherited;

  if Trim(edtSubCategoria.Text) <> '' then
    dmLancamento.cdsLancamento_F.FieldByName('ID_SUBCATEGORIA_DESPREC_FK').AsInteger := StrToInt(edtSubCategoria.Text);
end;

procedure TFCadastroLancamento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  if lPodeCancelar then
  begin
    dmLancamento.cdsParcela_F.Cancel;
    dmLancamento.cdsLancamento_F.Cancel;

    dmLancamento.cdsDetalhe_F.Close;
    dmLancamento.cdsDetalhe_F.Close;

    dmLancamento.cdsParcela_F.Close;
    dmLancamento.cdsLancamento_F.Close;

    if vgLanc_VlrTotPrev = 0 then
    begin
      vgLanc_Codigo := 0;
      vgLanc_Ano    := 0;
    end;

    vgLanc_TpLanc := '';
  end
  else
    Action := caNone;
end;

procedure TFCadastroLancamento.FormCreate(Sender: TObject);
begin
  inherited;

  BS.VerificarExistenciaPastasSistema;

  lTabAuxDig  := False;

  iIdIncParc := 0;
  iIdIncDet  := 0;
  iIdIncImg  := 0;

  lGerarNovaParcela := False;

  //No caso de Baixa por Grupo de Flutuantes
  vgLanc_VlrTotPrev := 0;
  vgLanc_VlrTotPago := 0;
end;

procedure TFCadastroLancamento.FormShow(Sender: TObject);
var
  cTotalPrevItem, cTotalPrevParc: Currency;
begin
  cTotalPrevItem := 0;
  cTotalPrevParc := 0;

  with dmLancamento do
  begin
    //TABELAS AUXILIARES
    qryCliFor.Close;
    qryCliFor.Params.ParamByName('TIPO_CLIFOR').Value   := IfThen((TipoLanc = DESP), 'F', 'C');  //Despesa => Fornecedor (F) / Receita => Cliente (C)
    qryCliFor.Params.ParamByName('FLG_ATIVO_CF1').Value := IfThen((vgOperacao = I), 'S', '');
    qryCliFor.Params.ParamByName('FLG_ATIVO_CF2').Value := IfThen((vgOperacao = I), 'S', '');
    qryCliFor.Open;

    qryCategoria.Close;
    qryCategoria.Params.ParamByName('TIPO_CAT').Value := IfThen((TipoLanc = DESP), 'D', 'R');
    qryCategoria.Open;

    qryFormaPagto.Close;
    qryFormaPagto.Params.ParamByName('FLG_ATIVA_FP1').Value := IfThen((vgOperacao = I), 'S', '');
    qryFormaPagto.Params.ParamByName('FLG_ATIVA_FP2').Value := IfThen((vgOperacao = I), 'S', '');
    qryFormaPagto.Open;

    qryCarneLeao.Close;
    qryCarneLeao.Open;

    AbrirListaItem;

    qryBanco.Close;
    qryBanco.Open;

    AbrirListaNatureza;

    //LANCAMENTO
    cdsLancamento_F.Close;
    cdsLancamento_F.Params.ParamByName('COD_LANCAMENTO').Value := vgLanc_Codigo;
    cdsLancamento_F.Params.ParamByName('ANO_LANCAMENTO').Value := vgLanc_Ano;
    cdsLancamento_F.Open;

    vgLanc_TpLanc := IfThen((TipoLanc = DESP), 'D', 'R');

    cTotalPrevItem := cdsLancamento_F.Fields.FieldByName('VR_TOTAL_PREV').AsCurrency;

    //DETALHE
    cdsDetalhe_F.Close;
    cdsDetalhe_F.Params.ParamByName('COD_LANCAMENTO').Value := vgLanc_Codigo;
    cdsDetalhe_F.Params.ParamByName('ANO_LANCAMENTO').Value := vgLanc_Ano;
    cdsDetalhe_F.Open;

    iIdIncDet := cdsDetalhe_F.RecordCount;

    AbrirListaItem;

    //PARCELA
    cdsParcela_F.Close;
    cdsParcela_F.Params.ParamByName('COD_LANCAMENTO').Value := vgLanc_Codigo;
    cdsParcela_F.Params.ParamByName('ANO_LANCAMENTO').Value := vgLanc_Ano;
    cdsParcela_F.Open;

    iIdIncParc := cdsParcela_F.RecordCount;

    //IMAGENS
    cdsImagem_F.Close;
    cdsImagem_F.Params.ParamByName('COD_LANCAMENTO').Value := vgLanc_Codigo;
    cdsImagem_F.Params.ParamByName('ANO_LANCAMENTO').Value := vgLanc_Ano;
    cdsImagem_F.Open;

    iIdIncImg := cdsImagem_F.RecordCount;

    if vgOperacao = I then
    begin
      vgLanc_Ano := YearOf(Date);

//      dmPrincipal.qryFechCx.Close;
//      dmPrincipal.qryFechCx.Params.ParamByName('DATA').Value := Date;
//      dmPrincipal.qryFechCx.Open;

      cdsLancamento_F.Append;

      cdsLancamento_F.FieldByName('DATA_LANCAMENTO').AsDateTime := vgDataLancVigente;
      cdsLancamento_F.FieldByName('TIPO_CADASTRO').AsString     := 'M';
      cdsLancamento_F.FieldByName('ANO_LANCAMENTO').AsInteger   := YearOf(Date);
      cdsLancamento_F.FieldByName('QTD_PARCELAS').AsInteger     := 1;
      cdsLancamento_F.FieldByName('CAD_ID_USUARIO').AsInteger   := vgUsu_Id;
      cdsLancamento_F.FieldByName('FLG_STATUS').AsString        := 'P';
      cdsLancamento_F.FieldByName('FLG_RECORRENTE').AsString    := 'N';
      cdsLancamento_F.FieldByName('FLG_REPLICADO').AsString     := 'N';
      cdsLancamento_F.FieldByName('FLG_CANCELADO').AsString     := 'N';
      cdsLancamento_F.FieldByName('FLG_IMPOSTORENDA').AsString  := 'N';
      cdsLancamento_F.FieldByName('FLG_PESSOAL').AsString       := 'N';
      cdsLancamento_F.FieldByName('FLG_AUXILIAR').AsString      := 'N';
      cdsLancamento_F.FieldByName('FLG_FORAFECHCAIXA').AsString := 'N';
      cdsLancamento_F.FieldByName('FLG_FLUTUANTE').AsString     := 'N';
      cdsLancamento_F.FieldByName('FLG_REAL').AsString          := 'S';

      //No caso de Baixa por Grupo de Flutuantes
      if vgLanc_VlrTotPrev > 0 then
        cdsLancamento_F.FieldByName('VR_TOTAL_PREV').AsCurrency := vgLanc_VlrTotPrev;

      cdsLancamento_F.FieldByName('VR_TOTAL_JUROS').AsCurrency    := 0;
      cdsLancamento_F.FieldByName('VR_TOTAL_DESCONTO').AsCurrency := 0;

      edtQtdParcelasLanc.Text := '1';

      lblAvisoValorDivergente.Visible := False;
    end
    else
    begin
      //Tabela Auxiliar
      dmLancamento.qrySubCategoria.Close;
      dmLancamento.qrySubCategoria.Params.ParamByName('ID_CAT').Value      := dmLancamento.qryCategoria.FieldByName('ID_CATEGORIA_DESPREC').AsInteger;
      dmLancamento.qrySubCategoria.Params.ParamByName('TIPO_SUBCAT').Value := IfThen((TipoLanc = DESP), 'D', 'R');
      dmLancamento.qrySubCategoria.Open;

      //LANCAMENTO
      cdsLancamento_F.Edit;

      //DETALHE
      cdsDetalhe_F.First;

      while not cdsDetalhe_F.Eof do
      begin
        cdsDetalhe_F.Edit;
        cdsDetalhe_F.FieldByName('PRE_CADASTRADO').AsBoolean := True;
        cdsDetalhe_F.FieldByName('QTD_ANTERIOR').AsInteger   := cdsDetalhe_F.FieldByName('QUANTIDADE').AsInteger;
        cdsDetalhe_F.Post;

        cdsDetalhe_F.Next;
      end;

      //PARCELA
      cdsParcela_F.First;

      while not cdsParcela_F.Eof do
      begin
        cdsParcela_F.Edit;

        cdsParcela_F.FieldByName('PRE_CADASTRADO').AsBoolean := True;

        if not cdsParcela_F.FieldByName('DATA_PAGAMENTO').IsNull then
          cdsParcela_F.FieldByName('DATA_PAGAMENTO_ANT').AsDateTime := cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime;

        cdsParcela_F.Post;

        cTotalPrevParc := (cTotalPrevParc + cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency);

        cdsParcela_F.Next;
      end;

      if cTotalPrevItem <> cTotalPrevParc then
        lblAvisoValorDivergente.Visible := True
      else
        lblAvisoValorDivergente.Visible := False;
    end;

    if vgOperacao <> C then
    begin
      if cdsParcela_F.RecordCount = 0 then
        HabDesabBotoesCRUD('P', True, False, False)
      else
        HabDesabBotoesCRUD('P', True, True, True);

      DesabilitaHabilitaCampos('P', False);
    end;
  end;

  inherited;

  if TipoLanc = DESP then
  begin
    Self.Caption := 'Cadastro de Lan�amento - Despesa';

    lblNumeroRecibo.Visible := False;

    lblCliFor.Caption := 'Fornecedor';

    gbParcelas.Color := $00DCD9FF;
  end
  else
  begin
    Self.Caption := 'Cadastro de Lan�amento - Receita';

    if not dmLancamento.cdsLancamento_F.FieldByName('NUM_RECIBO').IsNull then
    begin
      lblNumeroRecibo.Visible := True;
      lblNumeroRecibo.Caption := 'N� do Recibo: ' + dmLancamento.cdsLancamento_F.FieldByName('NUM_RECIBO').AsString;
    end;

    lblCliFor.Caption := 'Cliente';

    gbParcelas.Color := $00DFFFDF;
  end;

  if (vgOperacao = I) and
    vgOrigemCadastro then  //Origem na UListaBaixaFLutuante - Repasse de Receita com cria��o de Despesa
  begin
    GerarParcelas;

    dmLancamento.cdsParcela_F.Edit;
    dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency := ((dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency +
                                                                             dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_JUROS').AsCurrency) -
                                                                            dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_DESCONTO').AsCurrency);
    dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString        := 'G';
    dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime  := dmLancamento.cdsParcela_F.FieldByName('DATA_LANCAMENTO_PARC').AsDateTime;
    dmLancamento.cdsParcela_F.Post;

    dmLancamento.cdsLancamento_F.FieldByName('FLG_STATUS').AsString := 'G';

    if lcbNatureza.CanFocus then
      lcbNatureza.SetFocus;
  end;

  ShowScrollBar(dbgParcelas.Handle, SB_HORZ, False);

  MarcarDesmarcarAbas(PRINC);
end;

procedure TFCadastroLancamento.GerarParcelas;
var
  j, k, p, iPaga, iBaixa, iIdParc, iNumParc: Integer;
  lApagar, lGerar, lAdicionar, lDizimaPeriodica: Boolean;
  cVlrParcela: Currency;
  dData: TDate;
begin
  j := 0;
  k := 0;
  p := 0;

  iPaga    := 0;
  iBaixa   := 0;
  iIdParc  := 0;
  iNumParc := 0;

  lGerar           := True;
  lAdicionar       := False;
  lApagar          := False;
  lDizimaPeriodica := False;

  dData := Date;

  with dmLancamento do
  begin
    cTotalLanc := cdsLancamento_F.FieldByName('VR_TOTAL_PREV').AsCurrency;

    if cdsParcela_F.RecordCount > 0 then
    begin
      cdsParcela_F.First;

      while not cdsParcela_F.Eof do
      begin
        if cdsParcela_F.FieldByName('FLG_STATUS').AsString = 'G' then
          Inc(iPaga);

        cdsParcela_F.Next;
      end;

      if iPaga > 0 then
      begin
        if vgOperacao = I then
        begin
          if Application.MessageBox('Essa a��o ir� recalcular as Parcelas. Confirma a altera��o?', 'Aviso', MB_YESNO + MB_ICONQUESTION) = ID_YES then
            lApagar := True
          else
            lGerar := False;
        end;

        if vgOperacao = E then
        begin
{          if iBaixa > 0 then
          begin
            Application.MessageBox('N�o � poss�vel recalcular Parcelas j� PAGAS.', 'Aviso', MB_OK + MB_ICONWARNING);
            lGerar := False;
          end
          else
          begin  }
            if StrToInt(edtQtdParcelasLanc.Text) > iPaga then
            begin
              if Application.MessageBox('Essa a��o pode recalcular o Valor Previsto das Parcelas. Deseja fazer o rec�lculo?', 'Aviso', MB_YESNO + MB_ICONQUESTION) = ID_YES then
                lAdicionar := True
              else
                lGerar := False;
            end
            else if StrToInt(edtQtdParcelasLanc.Text) < iPaga then
            begin
              Application.MessageBox('N�o � poss�vel reduzir as Parcelas a uma quantidade menor que a de Parcelas j� PAGAS.', 'Aviso', MB_OK + MB_ICONWARNING);
              lGerar := False;
            end
            else
              lGerar := False;
//          end;
        end;
      end
      else
      begin
        if vgOperacao = I then
          lApagar := True
        else
        begin
          if Application.MessageBox('Essa a��o ir� recalcular as Parcelas. Confirma a altera��o?', 'Aviso', MB_YESNO + MB_ICONQUESTION) = ID_YES then
            lApagar := True
          else
            lGerar := False;
        end;
      end;
    end;

    if lGerar then
    begin
      if lApagar then
      begin
        cdsParcela_F.First;

        while not (cdsParcela_F.RecordCount = 0) do
        begin
          cdsParcela_F.Delete;
          cdsParcela_F.Next;
        end;
      end;

      p := 1;  //dmLancamento.qryFormaPagto.FieldByName('PRAZO').AsInteger;
      k := StrToInt(edtQtdParcelasLanc.Text);
      cVlrParcela := (cTotalLanc / k);

      //Verificar se se trata de dizima periodica
      if ((Frac(cVlrParcela) * k) < 1) and
        (((cVlrParcela * k) + 0.0001) = cTotalLanc) then
        lDizimaPeriodica := True;

      if lAdicionar then
      begin
        cdsParcela_F.First;

        while not cdsParcela_F.Eof do
        begin
          cdsParcela_F.Edit;

          if lDizimaPeriodica then
          begin
            cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency := (cVlrParcela + 0.01);
            cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency := (cVlrParcela + 0.01);
            lDizimaPeriodica := False;
          end
          else
          begin
            cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency := cVlrParcela;
            cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency := cVlrParcela;
          end;

          cdsParcela_F.Post;

          cdsParcela_F.Next;
        end;

        k := (k - iPaga);
      end;

      iNumParc := (cdsParcela_F.RecordCount + 1);
      iIdParc  := (900000 + iNumParc);

      for j := 1 to k do
      begin
        cdsParcela_F.Append;
        cdsParcela_F.FieldByName('NOVO').AsBoolean               := True;
        cdsParcela_F.FieldByName('ID_LANCAMENTO_PARC').AsInteger := iIdParc;
        cdsParcela_F.FieldByName('NUM_PARCELA').AsInteger        := iNumParc;

        if p > 1 then
          dData := IncDay(dData, p);

        cdsParcela_F.FieldByName('DATA_VENCIMENTO').AsDateTime   := dteDataLancamento.Date;  //dData;

        if lDizimaPeriodica then
        begin
          cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency := (cVlrParcela + 0.01);
          lDizimaPeriodica := False;
        end
        else
          cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency   := cVlrParcela;

        cdsParcela_F.FieldByName('DATA_LANCAMENTO_PARC').AsDateTime := dteDataLancamento.Date;
        cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger  := 6;  //DINHEIRO - default
        cdsParcela_F.FieldByName('FLG_STATUS').AsString             := 'P';
        cdsParcela_F.FieldByName('CAD_ID_USUARIO').AsInteger        := vgUsu_Id;
        cdsParcela_F.FieldByName('DATA_CADASTRO').AsDateTime        := Now;
        cdsParcela_F.FieldByName('DATA_PAGAMENTO').Value            := Null;
        cdsParcela_F.FieldByName('PAG_ID_USUARIO').Value            := Null;
        cdsParcela_F.FieldByName('DATA_CANCELAMENTO').Value         := Null;
        cdsParcela_F.FieldByName('CANCEL_ID_USUARIO').Value         := Null;
        cdsParcela_F.FieldByName('VR_PARCELA_JUROS').AsCurrency     := 0;
        cdsParcela_F.FieldByName('VR_PARCELA_DESCONTO').AsCurrency  := 0;
        cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency      := 0;

        cdsParcela_F.Post;

        Inc(iIdParc);
        Inc(iNumParc);
      end;
    end;
  end;

  if dmLancamento.cdsParcela_F.RecordCount = 0 then
    HabDesabBotoesCRUD('P', True, False, False)
  else
    HabDesabBotoesCRUD('P', True, True, True);

  DesabilitaHabilitaCampos('P', False);
end;

function TFCadastroLancamento.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroLancamento.GravarDadosGenerico(var Msg: String): Boolean;
var
  j, IdDetalhe, IdHistorico, IdParcela,
  IdImagem, iTudoPago, iNatureza,
  IdEquiv: Integer;
  QryAux: TFDQuery;
  lPodeQuitar, lOk: Boolean;
  cTotalPrevItem, cTotalParcPrev, cTotalParcPago,
  cTotalParcJuros, cTotalParcDesc: Currency;
  dDtDepAnt: TDatetime;
  IdNat, Cod: Integer;
  Descr, Obs, sIR, sCP, sLA: String;
begin
  Result := True;
  lOk    := True;

  Msg         := '';
  IdDetalhe   := 0;
  IdHistorico := 0;
  IdParcela   := 0;
  IdImagem    := 0;
  iTudoPago   := 0;
  iNatureza   := 0;
  IdEquiv     := 0;

  cTotalPrevItem  := 0;
  cTotalParcPrev  := 0;
  cTotalParcPago  := 0;
  cTotalParcJuros := 0;
  cTotalParcDesc  := 0;

  dDtDepAnt := 0;

  lPodeQuitar  := False;
  
  j := 0;

  if vgOperacao = C then
    Exit;

  QryAux    := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  dmPrincipal.cdsHistoricoItem.Close;
  dmPrincipal.cdsHistoricoItem.Open;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    with dmLancamento do
    begin
      { LANCAMENTO }
      if vgOperacao = I then
      begin
        vgLanc_Codigo := dmGerencial.ProximoCodigoLancamento;
        vgLanc_Ano    := YearOf(Date);

        if not (cdsLancamento_F.State = dsInsert) then
          cdsLancamento_F.Edit;

        cdsLancamento_F.FieldByName('COD_LANCAMENTO').AsInteger   := vgLanc_Codigo;
        cdsLancamento_F.FieldByName('ANO_LANCAMENTO').AsInteger   := vgLanc_Ano;
        cdsLancamento_F.FieldByName('TIPO_LANCAMENTO').AsString   := vgLanc_TpLanc;
        cdsLancamento_F.FieldByName('FLG_REAL').AsString          := 'S';

{        if Trim(cdsLancamento_F.FieldByName('FLG_FORAFECHCAIXA').AsString) = 'S' then
          dmLancamento.cdsLancamento_F.FieldByName('DATA_LANCAMENTO').AsDateTime := IncDay(Now, 1)
        else
        begin
          if (Date < vgDataLancVigente) or
            (Date < dteDataLancamento.Date) then
          begin
            if vgDataLancVigente < dteDataLancamento.Date then
              dmLancamento.cdsLancamento_F.FieldByName('DATA_LANCAMENTO').AsDateTime := dteDataLancamento.Date
            else
              dmLancamento.cdsLancamento_F.FieldByName('DATA_LANCAMENTO').AsDateTime := vgDataLancVigente;
          end
          else
            dmLancamento.cdsLancamento_F.FieldByName('DATA_LANCAMENTO').AsDateTime := Now;
        end;  }
      end
      else
      begin
        if not (cdsLancamento_F.State = dsEdit) then
          cdsLancamento_F.Edit;
      end;

      //Carne-Leao
      IdEquiv := dmPrincipal.RetornaEquivalenciaCarneLeao(cdsLancamento_F.FieldByName('ID_CATEGORIA_DESPREC_FK').AsInteger,
                                                          cdsLancamento_F.FieldByName('ID_SUBCATEGORIA_DESPREC_FK').AsInteger,
                                                          cdsLancamento_F.FieldByName('ID_NATUREZA_FK').AsInteger);

      if IdEquiv > 0 then
        cdsLancamento_F.FieldByName('ID_CARNELEAO_EQUIVALENCIA_FK').AsInteger := IdEquiv
      else
        cdsLancamento_F.FieldByName('ID_CARNELEAO_EQUIVALENCIA_FK').Value     := Null;

      cdsLancamento_F.Post;

      iNatureza      := cdsLancamento_F.FieldByName('ID_NATUREZA_FK').AsInteger;
      cTotalPrevItem := cdsLancamento_F.FieldByName('VR_TOTAL_PREV').AsCurrency;

      cdsLancamento_F.ApplyUpdates(0);

      { DETALHE }
      if cdsDetalhe_F.RecordCount > 0 then
      begin
        IdDetalhe := BS.ProximoId('ID_LANCAMENTO_DET', 'LANCAMENTO_DET');
        cdsDetalhe_F.First;

        while not cdsDetalhe_F.Eof do
        begin
          if cdsDetalhe_F.FieldByName('NOVO').AsBoolean then
          begin
            //Detalhe
            cdsDetalhe_F.Edit;
            cdsDetalhe_F.FieldByName('ID_LANCAMENTO_DET').AsInteger := IdDetalhe;
            cdsDetalhe_F.FieldByName('COD_LANCAMENTO_FK').AsInteger := vgLanc_Codigo;
            cdsDetalhe_F.FieldByName('ANO_LANCAMENTO_FK').AsInteger := vgLanc_Ano;
            cdsDetalhe_F.Post;

            if qryItem.FieldByName('TIPO_ITEM').AsString = 'P' then
            begin
              //Estoque
              dmPrincipal.cdsHistoricoItem.Append;
              dmPrincipal.cdsHistoricoItem.FieldByName('ID_HISTORICO_ITEM').AsInteger := IdHistorico + 900000;
              dmPrincipal.cdsHistoricoItem.FieldByName('ID_ITEM_FK').AsInteger        := cdsDetalhe_F.FieldByName('ID_ITEM_FK').AsInteger;
              dmPrincipal.cdsHistoricoItem.FieldByName('DATA_HISTORICO').AsDateTime   := Now;
              dmPrincipal.cdsHistoricoItem.FieldByName('ID_USUARIO').AsInteger        := vgUsu_Id;
              dmPrincipal.cdsHistoricoItem.FieldByName('QUANTIDADE').AsInteger        := cdsDetalhe_F.FieldByName('QUANTIDADE').AsInteger;
              dmPrincipal.cdsHistoricoItem.FieldByName('VR_UNITARIO').AsCurrency      := cdsDetalhe_F.FieldByName('VR_UNITARIO').AsCurrency;

              if TipoLanc = DESP then
              begin
                dmPrincipal.cdsHistoricoItem.FieldByName('ID_TIPO_HISTORICO_FK').AsInteger := 1;  //LANCAMENTO (ENTRADA)
                dmPrincipal.cdsHistoricoItem.FieldByName('FLG_TIPO_HISTORICO').AsString    := 'E';
              end
              else
              begin
                dmPrincipal.cdsHistoricoItem.FieldByName('ID_TIPO_HISTORICO_FK').AsInteger := 2;  //LANCAMENTO (SAIDA)
                dmPrincipal.cdsHistoricoItem.FieldByName('FLG_TIPO_HISTORICO').AsString    := 'S';
              end;

              dmPrincipal.cdsHistoricoItem.Post;

              Inc(IdHistorico);
            end;

            Inc(IdDetalhe);
          end
          else
          begin
            if (qryItem.FieldByName('TIPO_ITEM').AsString = 'P') and
              (cdsDetalhe_F.FieldByName('QTD_ANTERIOR').AsInteger <> cdsDetalhe_F.FieldByName('QUANTIDADE').AsInteger) then
            begin
              //Estoque
              dmPrincipal.cdsHistoricoItem.Append;
              dmPrincipal.cdsHistoricoItem.FieldByName('ID_HISTORICO_ITEM').AsInteger := IdHistorico + 900000;
              dmPrincipal.cdsHistoricoItem.FieldByName('ID_ITEM_FK').AsInteger        := cdsDetalhe_F.FieldByName('ID_ITEM_FK').AsInteger;
              dmPrincipal.cdsHistoricoItem.FieldByName('DATA_HISTORICO').AsDateTime   := Now;
              dmPrincipal.cdsHistoricoItem.FieldByName('ID_USUARIO').AsInteger        := vgUsu_Id;
              dmPrincipal.cdsHistoricoItem.FieldByName('VR_UNITARIO').AsCurrency      := cdsDetalhe_F.FieldByName('VR_UNITARIO').AsCurrency;

              if cdsDetalhe_F.FieldByName('QTD_ANTERIOR').AsInteger > cdsDetalhe_F.FieldByName('QUANTIDADE').AsInteger then
              begin
                dmPrincipal.cdsHistoricoItem.FieldByName('QUANTIDADE').AsInteger      := (cdsDetalhe_F.FieldByName('QTD_ANTERIOR').AsInteger -
                                                                                          cdsDetalhe_F.FieldByName('QUANTIDADE').AsInteger);

                if TipoLanc = DESP then
                begin
                  dmPrincipal.cdsHistoricoItem.FieldByName('ID_TIPO_HISTORICO_FK').AsInteger := 4;  //ACERTO DE ESTOQUE (SAIDA)
                  dmPrincipal.cdsHistoricoItem.FieldByName('FLG_TIPO_HISTORICO').AsString    := 'S';
                end
                else
                begin
                  dmPrincipal.cdsHistoricoItem.FieldByName('ID_TIPO_HISTORICO_FK').AsInteger := 3;  //ACERTO DE ESTOQUE (ENTRADA)
                  dmPrincipal.cdsHistoricoItem.FieldByName('FLG_TIPO_HISTORICO').AsString    := 'E';
                end;
              end;

              if cdsDetalhe_F.FieldByName('QTD_ANTERIOR').AsInteger < cdsDetalhe_F.FieldByName('QUANTIDADE').AsInteger then
              begin
                dmPrincipal.cdsHistoricoItem.FieldByName('QUANTIDADE').AsInteger      := (cdsDetalhe_F.FieldByName('QUANTIDADE').AsInteger -
                                                                                          cdsDetalhe_F.FieldByName('QTD_ANTERIOR').AsInteger);

                if TipoLanc = DESP then
                begin
                  dmPrincipal.cdsHistoricoItem.FieldByName('ID_TIPO_HISTORICO_FK').AsInteger := 1;  //LANCAMENTO (ENTRADA)
                  dmPrincipal.cdsHistoricoItem.FieldByName('FLG_TIPO_HISTORICO').AsString    := 'E';
                end
                else
                begin
                  dmPrincipal.cdsHistoricoItem.FieldByName('ID_TIPO_HISTORICO_FK').AsInteger := 2;  //LANCAMENTO (SAIDA)
                  dmPrincipal.cdsHistoricoItem.FieldByName('FLG_TIPO_HISTORICO').AsString    := 'S';
                end;
              end;

              dmPrincipal.cdsHistoricoItem.Post;

              Inc(IdHistorico);
            end;
          end;

          cdsDetalhe_F.Next;
        end;

        cdsDetalhe_F.ApplyUpdates(0);
      end;

      { PARCELA }
      if cdsParcela_F.RecordCount > 0 then
      begin
        IdParcela := BS.ProximoId('ID_LANCAMENTO_PARC', 'LANCAMENTO_PARC');
        cdsParcela_F.First;

        while not cdsParcela_F.Eof do
        begin
          if cdsParcela_F.FieldByName('NOVO').AsBoolean then
          begin
            cdsParcela_F.Edit;
            cdsParcela_F.FieldByName('ID_LANCAMENTO_PARC').AsInteger := IdParcela;
            cdsParcela_F.FieldByName('COD_LANCAMENTO_FK').AsInteger  := vgLanc_Codigo;
            cdsParcela_F.FieldByName('ANO_LANCAMENTO_FK').AsInteger  := vgLanc_Ano;
            cdsParcela_F.FieldByName('DATA_CADASTRO').AsDateTime     := Now;

            cdsParcela_F.Post;

            Inc(IdParcela);
          end;

          cTotalParcPrev  := (cTotalParcPrev + cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency);
          cTotalParcJuros := (cTotalParcJuros + cdsParcela_F.FieldByName('VR_PARCELA_JUROS').AsCurrency);
          cTotalParcDesc  := (cTotalParcDesc + cdsParcela_F.FieldByName('VR_PARCELA_DESCONTO').AsCurrency);
          cTotalParcPago  := (cTotalParcPago + cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency);

          //No caso de Baixa por Grupo de Flutuantes
          if vgLanc_VlrTotPrev > 0 then
            vgLanc_VlrTotPago := (vgLanc_VlrTotPago + cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency);

          if cdsParcela_F.FieldByName('FLG_STATUS').AsString = 'G' then
            Inc(iTudoPago);

          if dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').IsNull then
          begin
            cdsParcela_F.Edit;
            cdsParcela_F.FieldByName('NUM_COD_DEPOSITO').Value := Null;
            cdsParcela_F.Post;
          end;

          cdsParcela_F.Next;
        end;

        cdsParcela_F.ApplyUpdates(0);
      end;

      { IMAGENS }
      if cdsImagem_F.RecordCount > 0 then
      begin
        IdImagem := BS.ProximoId('ID_LANCAMENTO_IMG', 'LANCAMENTO_IMG');

        cdsImagem_F.First;

        while not cdsImagem_F.Eof do
        begin
          cdsImagem_F.Edit;

          if cdsImagem_F.FieldByName('NOVO').AsBoolean then
          begin
            cdsImagem_F.FieldByName('ID_LANCAMENTO_IMG').AsInteger := IdImagem;
            Inc(IdImagem);
          end;

          if Copy(cdsImagem_F.FieldByName('NOME_FIXO').AsString, 1, 1) = '0' then
            cdsImagem_F.FieldByName('NOME_FIXO').AsString  := IntToStr(vgLanc_Codigo) +
                                                              Copy(cdsImagem_F.FieldByName('NOME_FIXO').AsString,
                                                                   2,
                                                                   (Length(cdsImagem_F.FieldByName('NOME_FIXO').AsString) - 1));

          cdsImagem_F.FieldByName('COD_LANCAMENTO_FK').AsInteger  := vgLanc_Codigo;
          cdsImagem_F.FieldByName('ANO_LANCAMENTO_FK').AsInteger  := vgLanc_Ano;
          cdsImagem_F.Post;

          cdsImagem_F.Next;
        end;
      end;

      if cdsImagem_F.RecordCount > 0 then
      begin
        cdsImagem_F.First;

        while not cdsImagem_F.Eof do
        begin
          if not cdsImagem_F.FieldByName('NOME_V_ANTIGO').IsNull then
          begin
            if FileExists(vgConf_DiretorioImagens +
                          IntToStr(vgLanc_Ano) +
                          '\' + cdsImagem_F.FieldByName('NOME_FIXO').AsString +
                          cdsImagem_F.FieldByName('NOME_V_ANTIGO').AsString + '.' +
                          cdsImagem_F.FieldByName('EXTENSAO').AsString) then
            begin
              DeleteFile(vgConf_DiretorioImagens +
                         IntToStr(vgLanc_Ano) +
                         '\' + cdsImagem_F.FieldByName('NOME_FIXO').AsString +
                         cdsImagem_F.FieldByName('NOME_V_ANTIGO').AsString + '.' +
                         cdsImagem_F.FieldByName('EXTENSAO').AsString);
            end;
          end;

          if not cdsImagem_F.FieldByName('ORIGEM_ANTIGA').IsNull then
          begin
            if FileExists(cdsImagem_F.FieldByName('ORIGEM_ANTIGA').AsString) then
              DeleteFile(cdsImagem_F.FieldByName('ORIGEM_ANTIGA').AsString);
          end;

          cdsImagem_F.Next;
        end;

        cdsImagem_F.ApplyUpdates(0);
      end;

{      //LANCAMENTO
      if (iTudoPago = cdsParcela_F.RecordCount) and
        (cdsLancamento_F.FieldByName('VR_TOTAL_PREV').AsCurrency = ((cTotalParcPago - cTotalParcJuros) +
                                                                    cTotalParcDesc)) then
        QuitarLancamento(cTotalParcPago);   }
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    //LANCAMENTO
    dmLancamento.QuitarLancamento;

{    if (iTudoPago = dmLancamento.cdsParcela_F.RecordCount) and
      (dmLancamento.cdsLancamento_F.FieldByName('VR_TOTAL_PREV').AsCurrency = ((cTotalParcPago - cTotalParcJuros) +
                                                                               cTotalParcDesc)) then
      dmLancamento.QuitarLancamento;  }

    lOk := True;

    vgLanc_CriarNovo := True;
  except
    on E: Exception do
    begin
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      if vgOperacao = I then
      begin
        vgLanc_Codigo := 0;
        vgLanc_Ano    := 0;

        dmGerencial.FinalizarComponentePessoa;
        dmGerencial.InicializarComponentePessoa;
      end;

      Result := False;

      Msg := 'Erro na grava��o do Lan�amento.';
    end;
  end;

  if lOk then
  begin
    if dmPrincipal.cdsHistoricoItem.RecordCount > 0 then
    begin
      //Atualiza Historico de Item
      IdHistorico := 0;

      dmPrincipal.cdsHistoricoItem.First;
      IdHistorico := BS.ProximoId('ID_HISTORICO_ITEM', 'HISTORICO_ITEM');

      while not dmPrincipal.cdsHistoricoItem.Eof do
      begin
        dmPrincipal.cdsHistoricoItem.Edit;
        dmPrincipal.cdsHistoricoItem.FieldByName('ID_HISTORICO_ITEM').AsInteger := IdHistorico;
        dmPrincipal.cdsHistoricoItem.Post;

        dmPrincipal.cdsHistoricoItem.Next;

        Inc(IdHistorico);
      end;

      dmPrincipal.cdsHistoricoItem.ApplyUpdates(0);

      //Atualiza Item
      dmPrincipal.cdsHistoricoItem.First;

      while not dmPrincipal.cdsHistoricoItem.Eof do
      begin
        Msg := dmPrincipal.AtualizarEstoqueProduto(dmPrincipal.cdsHistoricoItem.FieldByName('ID_ITEM_FK').AsInteger,
                                                   dmPrincipal.cdsHistoricoItem.FieldByName('VR_UNITARIO').AsCurrency);

        if Trim(Msg) <> '' then
        begin
          Msg := 'Erro na Atualiza��o de Estoque do Item';
          Result := False;
        end;

        dmPrincipal.cdsHistoricoItem.Next;
      end;
    end;
  end;

  if Trim(Msg) = '' then
    Msg := IfThen((TipoLanc = DESP), 'Despesa gravada com sucesso!', 'Receita gravada com sucesso!');

  dmPrincipal.cdsHistoricoItem.Close;

  FreeAndNil(QryAux);
end;

procedure TFCadastroLancamento.GravarLog;
var
  sObservacao: String;
  sTipo: String;
begin
  inherited;

  sObservacao := '';

  sTipo := IfThen((TipoLanc = DESP), 'Despesa', 'Receita');

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta de ' + sTipo);
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclus�o de ' + sTipo);
    end;
    E:  //EDICAO
    begin
      if lSomentePagamento then
        sObservacao := 'Pagamento de Parcela de Lan�amento de ' + sTipo
      else
      begin
        if vgUsu_FlgJustifEdicao = 'S' then
        begin
          repeat
            sObservacao := InputBox('MOTIVO',
                                    'Por favor, indique o motivo da edi��o dessa ' + sTipo + ':',
                                    '');
          until sObservacao <> '';
        end
        else
          sObservacao := 'Edi��o de Dados de Lan�amento de ' + sTipo;
      end;

      BS.GravarUsuarioLog(2, '', sObservacao);
    end;
  end;
end;

procedure TFCadastroLancamento.HabDesabBotoesCRUD(Tipo: String; Inc, Ed,
  Exc: Boolean);
begin
  case AnsiIndexStr(UpperCase(Tipo), ['P', 'D', 'M']) of
    0:  //PARCELA
      begin
        btnIncluirParcela.Enabled   := Inc;
        btnEditarParcela.Enabled    := Ed;
        btnExcluirParcela.Enabled   := Exc;
        btnConfirmarParcela.Enabled := (not (Inc or Ed or Exc));
        btnCancelarParcela.Enabled  := (not (Inc or Ed or Exc));
      end;
    1:  //DETALHE
      begin
        btnIncluirDet.Enabled   := Inc;
        btnEditarDet.Enabled    := Ed;
        btnExcluirDet.Enabled   := Exc;
        btnConfirmarDet.Enabled := not (Inc or Ed or Exc);
        btnCancelarDet.Enabled  := not (Inc or Ed or Exc);
      end;
    2:  //IMAGEM
      begin
        btnAcionarScanner.Enabled   := Inc;
        btnAbrirPastaImagem.Enabled := Inc;
        btnColarImagem.Enabled      := Inc;
        btnAssociarPDF.Enabled      := Inc;
        btnEditarImagem.Enabled     := Ed;
        btnExcluirImagem.Enabled    := Exc;
        edtNomeArquivo.Enabled      := not (Inc or Ed or Exc);
        btnConfirmarAcao.Enabled    := not (Inc or Ed or Exc);
        btnCancelarAcao.Enabled     := not (Inc or Ed or Exc);
      end;
  end;
end;

procedure TFCadastroLancamento.btnAtualizarPagamentoClick(Sender: TObject);
var
  cVlrPago: Currency;
begin
  inherited;

  cVlrPago := 0;

  if not (dmLancamento.cdsParcela_F.State in [dsInsert, dsEdit]) then
    dmLancamento.cdsParcela_F.Edit;

  dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_JUROS').AsCurrency    := cedValorParcelaJuros.Value;
  dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_DESCONTO').AsCurrency := cedValorParcelaDesconto.Value;
  dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency     := cedValorParcelaPago.Value;
  dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency     := ((dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency +
                                                                               dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_JUROS').AsCurrency) -
                                                                              dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_DESCONTO').AsCurrency);
  dmLancamento.cdsParcela_F.FieldByName('DATA_CANCELAMENTO').Value        := Null;
  dmLancamento.cdsParcela_F.FieldByName('CANCEL_ID_USUARIO').Value        := Null;

  if cedValorParcelaPago.Value < 0 then
  begin
    Application.MessageBox('Por favor, preencha o VALOR PAGO corretamente.', 'Aviso', MB_OK + MB_ICONWARNING);
    cbSituacaoParcela.ItemIndex := 0;
    dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency := 0;
    dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString        := 'P';
    dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').Value       := Null;
  end
  else
  begin
    if cedValorParcelaPago.Value = 0 then
      cVlrPago := 0
    else
      cVlrPago := StrToCurr(ReplaceStr(cedValorParcelaPago.Text, '.', ''));

    if dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency <> ((cVlrPago +
                                                                                dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_DESCONTO').AsCurrency) -
                                                                               dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_JUROS').AsCurrency) then
    begin
      cbSituacaoParcela.ItemIndex := 0;
      dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency := 0;
      dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString        := 'P';
      dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').Value       := Null;
    end
    else
    begin
      cbSituacaoParcela.ItemIndex := 1;

      if (dteDataPagamentoParc.Date = 0) or
        (dteDataPagamentoParc.Date = Null) then
      begin
        if not chbForaFechCaixa.Checked then  //if chbForaFechCaixa.Checked then
//          dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := IncDay(Now, 1)
//        else
        begin
          if (Date < vgDataLancVigente) or
            (Date < dteDataLancamento.Date) then
          begin
            if vgDataLancVigente < dteDataLancamento.Date then
              dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := dteDataLancamento.Date
            else
              dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := vgDataLancVigente;
          end;
//          else
//            dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := Now;
        end;
      end;

      dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString := 'G';
      dmLancamento.cdsParcela_F.FieldByName('PAG_ID_USUARIO').AsInteger  := vgUsu_Id;
    end;
  end;
end;

procedure TFCadastroLancamento.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(ntbLancamento);
end;

procedure TFCadastroLancamento.lcbCategoriaClick(Sender: TObject);
begin
  inherited;

  if Trim(lcbCategoria.Text) <> '' then
  begin
    edtCategoria.Text := dmLancamento.qryCategoria.FieldByName('ID_CATEGORIA_DESPREC').AsString;

    edtSubCategoria.Enabled        := True;
    lcbSubCategoria.Enabled        := True;
    btnIncluirSubCategoria.Enabled := True;

    dmLancamento.qrySubCategoria.Close;
    dmLancamento.qrySubCategoria.Params.ParamByName('ID_CAT').Value      := dmLancamento.qryCategoria.FieldByName('ID_CATEGORIA_DESPREC').AsInteger;
    dmLancamento.qrySubCategoria.Params.ParamByName('TIPO_SUBCAT').Value := IfThen((TipoLanc = DESP), 'D', 'R');
    dmLancamento.qrySubCategoria.Open;

    if edtSubCategoria.CanFocus then
      edtSubCategoria.SetFocus;
  end
  else
  begin
    edtSubCategoria.Clear;
    lcbSubCategoria.KeyValue := -1;

    edtSubCategoria.Enabled        := False;
    lcbSubCategoria.Enabled        := False;
    btnIncluirSubCategoria.Enabled := False;
  end;
end;

procedure TFCadastroLancamento.lcbCategoriaExit(Sender: TObject);
begin
  inherited;

  if Trim(lcbCategoria.Text) <> '' then
  begin
    edtCategoria.Text := dmLancamento.qryCategoria.FieldByName('ID_CATEGORIA_DESPREC').AsString;

    edtSubCategoria.Enabled        := True;
    lcbSubCategoria.Enabled        := True;
    btnIncluirSubCategoria.Enabled := True;

    dmLancamento.qrySubCategoria.Close;
    dmLancamento.qrySubCategoria.Params.ParamByName('ID_CAT').Value      := dmLancamento.qryCategoria.FieldByName('ID_CATEGORIA_DESPREC').AsInteger;
    dmLancamento.qrySubCategoria.Params.ParamByName('TIPO_SUBCAT').Value := IfThen((TipoLanc = DESP), 'D', 'R');
    dmLancamento.qrySubCategoria.Open;

    if edtSubCategoria.CanFocus then
      edtSubCategoria.SetFocus;
  end
  else
  begin
    edtSubCategoria.Clear;
    lcbSubCategoria.KeyValue := -1;

    edtSubCategoria.Enabled        := False;
    lcbSubCategoria.Enabled        := False;
    btnIncluirSubCategoria.Enabled := False;
  end;
end;

procedure TFCadastroLancamento.lcbCliForClick(Sender: TObject);
begin
  inherited;

  if Trim(lcbCliFor.Text) <> '' then
    edtCliFor.Text := dmLancamento.qryCliFor.FieldByName('ID_CLIENTE_FORNECEDOR').AsString;
end;

procedure TFCadastroLancamento.lcbCliForExit(Sender: TObject);
begin
  inherited;

  if Trim(lcbCliFor.Text) <> '' then
    edtCliFor.Text := dmLancamento.qryCliFor.FieldByName('ID_CLIENTE_FORNECEDOR').AsString;
end;

procedure TFCadastroLancamento.lcbFormaPagamentoClick(Sender: TObject);
begin
  inherited;

  if not (dmLancamento.cdsParcela_F.State in [dsInsert, dsEdit]) then
    dmLancamento.cdsParcela_F.Edit;

  if Trim(lcbFormaPagamento.Text) <> '' then
  begin
    edtFormaPagamento.Text := dmLancamento.qryFormaPagto.FieldByName('ID_FORMAPAGAMENTO').AsString;

    edtAgenciaParc.Enabled    := (dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger in [2, 3, 5]);
    edtContaParc.Enabled      := (dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger in [2, 3, 5]);
    lcbBancoParc.Enabled      := (dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger in [2, 3, 5]);
    edtNumeroCheque.Enabled   := (dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger = 3);

    if dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger <> 5 then
      dmLancamento.cdsParcela_F.FieldByName('NUM_COD_DEPOSITO').Value := Null;
  end
  else
  begin
    edtAgenciaParc.Enabled    := False;
    edtContaParc.Enabled      := False;
    lcbBancoParc.Enabled      := False;
    edtNumeroCheque.Enabled   := False;

    dmLancamento.cdsParcela_F.FieldByName('NUM_COD_DEPOSITO').Value := Null;
  end;
end;

procedure TFCadastroLancamento.lcbFormaPagamentoExit(Sender: TObject);
begin
  inherited;

  if not (dmLancamento.cdsParcela_F.State in [dsInsert, dsEdit]) then
    dmLancamento.cdsParcela_F.Edit;

  if Trim(lcbFormaPagamento.Text) <> '' then
  begin
    edtFormaPagamento.Text := dmLancamento.qryFormaPagto.FieldByName('ID_FORMAPAGAMENTO').AsString;

    edtAgenciaParc.Enabled    := (dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger in [2, 3, 5]);
    edtContaParc.Enabled      := (dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger in [2, 3, 5]);
    lcbBancoParc.Enabled      := (dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger in [2, 3, 5]);
    edtNumeroCheque.Enabled   := (dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger = 3);

    if dmLancamento.cdsParcela_F.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger <> 5 then
      dmLancamento.cdsParcela_F.FieldByName('NUM_COD_DEPOSITO').Value := Null;
  end
  else
  begin
    edtAgenciaParc.Enabled    := False;
    edtContaParc.Enabled      := False;
    lcbBancoParc.Enabled      := False;
    edtNumeroCheque.Enabled   := False;

    dmLancamento.cdsParcela_F.FieldByName('NUM_COD_DEPOSITO').Value := Null;
  end;
end;

procedure TFCadastroLancamento.lcbItemDetExit(Sender: TObject);
begin
  inherited;

  if dmLancamento.cdsDetalhe_F.State in [dsInsert, dsEdit] then
  begin
    if Trim(lcbItemDet.Text) <> '' then
      dmLancamento.cdsDetalhe_F.FieldByName('VR_UNITARIO').AsCurrency := dmLancamento.qryItem.FieldByName('ULT_VALOR_CUSTO').AsCurrency;
  end;
end;

procedure TFCadastroLancamento.lcbNaturezaClick(Sender: TObject);
begin
  inherited;

  if Trim(lcbNatureza.Text) <> '' then
  begin
    cedValorTotalPrev.Enabled := (StrToInt(edtNatureza.Text) > 7);
    edtNatureza.Text := dmLancamento.qryNatureza.FieldByName('ID_NATUREZA').AsString;
    dmLancamento.cdsLancamento_F.FieldByName('FLG_IMPOSTORENDA').AsString := dmLancamento.qryNatureza.FieldByName('FLG_IMPOSTORENDA').AsString;
    dmLancamento.cdsLancamento_F.FieldByName('FLG_AUXILIAR').AsString     := dmLancamento.qryNatureza.FieldByName('FLG_AUXILIAR').AsString;
    dmLancamento.cdsLancamento_F.FieldByName('FLG_PESSOAL').AsString      := dmLancamento.qryNatureza.FieldByName('FLG_PESSOAL').AsString;

    edtCategoria.Enabled        := True;
    lcbCategoria.Enabled        := True;
    btnIncluirCategoria.Enabled := True;

    dmLancamento.qryCategoria.Close;
    dmLancamento.qryCategoria.Params.ParamByName('ID_NAT').Value   := dmLancamento.qryNatureza.FieldByName('ID_NATUREZA').AsInteger;
    dmLancamento.qryCategoria.Params.ParamByName('TIPO_CAT').Value := IfThen((TipoLanc = DESP), 'D', 'R');
    dmLancamento.qryCategoria.Open;

    if edtCategoria.CanFocus then
      edtCategoria.SetFocus;
  end
  else
  begin
    dmLancamento.cdsLancamento_F.FieldByName('FLG_IMPOSTORENDA').AsString := 'N';
    dmLancamento.cdsLancamento_F.FieldByName('FLG_AUXILIAR').AsString     := 'N';
    dmLancamento.cdsLancamento_F.FieldByName('FLG_PESSOAL').AsString      := 'N';

    cedValorTotalPrev.Enabled := False;

    edtCategoria.Clear;
    lcbCategoria.KeyValue := -1;

    edtCategoria.Enabled        := False;
    lcbCategoria.Enabled        := False;
    btnIncluirCategoria.Enabled := False;
  end;
end;

procedure TFCadastroLancamento.lcbNaturezaExit(Sender: TObject);
begin
  inherited;

  if Trim(lcbNatureza.Text) <> '' then
  begin
    cedValorTotalPrev.Enabled := (StrToInt(edtNatureza.Text) > 7);
    edtNatureza.Text := dmLancamento.qryNatureza.FieldByName('ID_NATUREZA').AsString;
    dmLancamento.cdsLancamento_F.FieldByName('FLG_IMPOSTORENDA').AsString := dmLancamento.qryNatureza.FieldByName('FLG_IMPOSTORENDA').AsString;
    dmLancamento.cdsLancamento_F.FieldByName('FLG_AUXILIAR').AsString     := dmLancamento.qryNatureza.FieldByName('FLG_AUXILIAR').AsString;
    dmLancamento.cdsLancamento_F.FieldByName('FLG_PESSOAL').AsString      := dmLancamento.qryNatureza.FieldByName('FLG_PESSOAL').AsString;

    edtCategoria.Enabled        := True;
    lcbCategoria.Enabled        := True;
    btnIncluirCategoria.Enabled := True;

    dmLancamento.qryCategoria.Close;
    dmLancamento.qryCategoria.Params.ParamByName('ID_NAT').Value   := dmLancamento.qryNatureza.FieldByName('ID_NATUREZA').AsInteger;
    dmLancamento.qryCategoria.Params.ParamByName('TIPO_CAT').Value := IfThen((TipoLanc = DESP), 'D', 'R');
    dmLancamento.qryCategoria.Open;

    if cedValorTotalPrev.CanFocus then
      cedValorTotalPrev.SetFocus;
  end
  else
  begin
    dmLancamento.cdsLancamento_F.FieldByName('FLG_IMPOSTORENDA').AsString := 'N';
    dmLancamento.cdsLancamento_F.FieldByName('FLG_AUXILIAR').AsString     := 'N';
    dmLancamento.cdsLancamento_F.FieldByName('FLG_PESSOAL').AsString      := 'N';

    cedValorTotalPrev.Enabled := False;

    edtCategoria.Clear;
    lcbCategoria.KeyValue := -1;

    edtCategoria.Enabled        := False;
    lcbCategoria.Enabled        := False;
    btnIncluirCategoria.Enabled := False;
  end;
end;

procedure TFCadastroLancamento.lcbSubCategoriaClick(Sender: TObject);
begin
  inherited;

  if Trim(lcbSubCategoria.Text) <> '' then
    edtSubCategoria.Text := dmLancamento.qrySubCategoria.FieldByName('ID_SUBCATEGORIA_DESPREC').AsString;
end;

procedure TFCadastroLancamento.lcbSubCategoriaExit(Sender: TObject);
begin
  inherited;

  if Trim(lcbSubCategoria.Text) <> '' then
    edtSubCategoria.Text := dmLancamento.qrySubCategoria.FieldByName('ID_SUBCATEGORIA_DESPREC').AsString;
end;

procedure TFCadastroLancamento.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;

  case Aba of
    PRINC:
    begin
      ntbLancamento.PageIndex := 0;

      if edtNatureza.CanFocus then
        edtNatureza.SetFocus;
    end;
    DET:
    begin
      ntbLancamento.PageIndex := 1;

      if cbTipoItem.CanFocus then
        cbTipoItem.SetFocus;
    end;
    DIG:
    begin
      ntbLancamento.PageIndex := 2;
    end;
  end;

  btnDadosPrincipais.Transparent  := not (Aba = PRINC);
  btnAbaDetalhe.Transparent       := not (Aba = DET);
  btnAbaDigitalizacao.Transparent := not (Aba = DIG);
end;

procedure TFCadastroLancamento.mmObsLancamentoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if Application.MessageBox('Deseja incluir algum Detalhe?', 'Confirma��o', MB_YESNO + MB_ICONQUESTION) = ID_YES then
      btnAbaDetalhe.Click
    else if Application.MessageBox('Deseja incluir alguma Imagem?', 'Confirma��o', MB_YESNO + MB_ICONQUESTION) = ID_YES then
      btnAbaDigitalizacao.Click
    else
      btnOk.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroLancamento.mmObsParcelaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnConfirmarParcela.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroLancamento.rgSituacaoLancClick(Sender: TObject);
begin
  inherited;

  AtualizaSituacaoLancamento;
end;

procedure TFCadastroLancamento.rgSituacaoLancEnter(Sender: TObject);
begin
  inherited;

  iSituacaoLanc := rgSituacaoLanc.ItemIndex;
end;

procedure TFCadastroLancamento.rgSituacaoLancExit(Sender: TObject);
begin
  inherited;

  if iSituacaoLanc = rgSituacaoLanc.ItemIndex then
    Exit;

  AtualizaSituacaoLancamento;
end;

procedure TFCadastroLancamento.btnExportarClick(Sender: TObject);
var
  sCEscolhido: String;
  j, iPos: Integer;
  imgJpg: TJPEGImage;
  arqPDF: TgtPDFDocument;
begin
  inherited;

  //Exporta para outro diretorio, a escolha do usuario
  if Trim(dmLancamento.cdsImagem_F.FieldByName('EXTENSAO').AsString) = 'jpg' then
  begin
    odAbrirPastaImagem.InitialDir := 'C:\';
    iPos := 0;

    if odAbrirPastaImagem.Execute then
    begin
      if odAbrirPastaImagem.FileName <> '' then
      begin
        sCEscolhido := odAbrirPastaImagem.FileName;

        for j := 0 to Length(sCEscolhido) - 1 do
        begin
          if sCEscolhido[j] = '\' then
            iPos := j;
        end;

        imgJpg := TJPEGImage.Create;
        imgJpg.Assign(imgImagemLanc.Picture.Graphic);
        imgJpg.SaveToFile(sCEscolhido);
      end;
    end;
  end
  else if Trim(dmLancamento.cdsImagem_F.FieldByName('EXTENSAO').AsString) = 'pdf' then
  begin
    odSalvarPDF.InitialDir := 'C:\';
    iPos := 0;

    if odSalvarPDF.Execute then
    begin
      if odSalvarPDF.FileName <> '' then
      begin
        sCEscolhido := odSalvarPDF.FileName;

        for j := 0 to Length(sCEscolhido) - 1 do
        begin
          if sCEscolhido[j] = '\' then
            iPos := j;
        end;

        arqPDF := TgtPDFDocument.Create(nil);
        arqPDF.LoadFromFile(dmLancamento.cdsImagem_F.FieldByName('CAMINHO_COMPLETO').AsString);
        arqPDF.SaveToFile(sCEscolhido);
        arqPDF.OpenAfterSave := True;
      end;
    end;
  end;
end;

function TFCadastroLancamento.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
var
  sTipo, sTipoCliFor: String;
  cTotalPrevParcs, cTotalPagoParcs, cTotalDif: Currency;
  iTotalParcs: Integer;
begin
  Result          := True;
  QtdErros        := 0;
  cTotalPrevParcs := 0;
  cTotalPagoParcs := 0;
  cTotalDif       := 0;
  iTotalParcs     := 0;

  sTipo := IfThen((TipoLanc = DESP), 'DESPESA', 'RECEITA');

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  { LANCAMENTO }

  if dteDataLancamento.Date = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a DATA DO LAN�AMENTO. (Aba: Lan�amento)';
    DadosMsgErro.Componente := dteDataLancamento;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(lcbNatureza.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a NATUREZA DO LAN�AMENTO. (Aba: Lan�amento)';
    DadosMsgErro.Componente := lcbNatureza;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if (not chbForaFechCaixa.Checked) and
    (dteDataLancamento.Date < vgDataLancVigente) and
    (vgOperacao = I)  then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') A DATA DO LAN�AMENTO n�o pode ser inferior a ' +
                                                        FormatDateTime('DD/MM/YYYY', vgDataLancVigente) + ', pois os Caixas anteriores a essa data j� se encontram FECHADOS.';
    DadosMsgErro.Componente := dteDataLancamento;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if cedValorTotalPrev.Value < 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') O VALOR TOTAL PREVISTO DA ' + sTipo + ' n�o pode ser menor que 0 (zero). (Aba: Lan�amento)';
    DadosMsgErro.Componente := cedValorTotalPrev;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtQtdParcelasLanc.Text) <> '' then
  begin
    if StrToInt(edtQtdParcelasLanc.Text) < 0 then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') A QUANTIDADE DE PARCELAS DA ' + sTipo + ' n�o pode ser menor que 0 (zero). (Aba: Lan�amento)';
      DadosMsgErro.Componente := edtQtdParcelasLanc;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;
  end
  else
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a QUANTIDADE DE PARCELAS DA ' + sTipo + '. (Aba: Lan�amento)';
    DadosMsgErro.Componente := edtQtdParcelasLanc;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  { PARCELA }  
  if dmLancamento.cdsParcela_F.RecordCount = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar PARCELA DA ' + sTipo + '. (Aba: Lan�amento)';
    DadosMsgErro.Componente := edtNumParcela;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end
  else
  begin
    dmLancamento.cdsParcela_F.First;

    while not dmLancamento.cdsParcela_F.Eof do
    begin
      cTotalPrevParcs := (cTotalPrevParcs + dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PREV').AsCurrency);
      cTotalPagoParcs := (cTotalPagoParcs + dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency);

      if dmLancamento.cdsParcela_F.FieldByName('VR_DIFERENCA').AsCurrency = 0 then
        cTotalDif   := (cTotalDif + dmLancamento.cdsParcela_F.FieldByName('VR_DIFERENCA').AsCurrency);

      if vgLanc_VlrTotPrev > 0 then
      begin
        if dmLancamento.cdsParcela_F.FieldByName('VR_PARCELA_PAGO').AsCurrency >= 0 then
          Inc(iTotalParcs);
      end
      else
      begin
        if dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString = 'G' then
          Inc(iTotalParcs);
      end;

      dmLancamento.cdsParcela_F.Next;
    end;

    if dmLancamento.cdsLancamento_F.FieldByName('VR_TOTAL_PREV').AsCurrency <> cTotalPrevParcs then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') O TOTAL PREVISTO para as PARCELAS est� divergindo do TOTAL PREVISTO para o LAN�AMENTO DE ' + sTipo + '. (Aba: Lan�amento)';
      DadosMsgErro.Componente := cedValorTotalPrev;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;

    if dmLancamento.cdsLancamento_F.FieldByName('QTD_PARCELAS').AsInteger = iTotalParcs then
    begin
      CalcularLancamento;

      if (((dmLancamento.cdsParcela_F.FieldByName('TP_DIFERENCA').IsNull) and
            (cTotalPagoParcs <> ((dmLancamento.cdsLancamento_F.FieldByName('VR_TOTAL_PREV').AsCurrency +
                                  dmLancamento.cdsLancamento_F.FieldByName('VR_TOTAL_JUROS').AsCurrency) -
                                 dmLancamento.cdsLancamento_F.FieldByName('VR_TOTAL_DESCONTO').AsCurrency))) or
          ((dmLancamento.cdsParcela_F.FieldByName('TP_DIFERENCA').AsString = 'F') and
           (cTotalPagoParcs <> (dmLancamento.cdsLancamento_F.FieldByName('VR_TOTAL_PREV').AsCurrency -
                                cTotalDif)))) then
      begin
        DadosMsgErro := MsgErro.Create;
        DadosMsgErro.IdMsg      := QtdErros;
        DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') O TOTAL PAGO est� divergindo do TOTAL PREVISTO para o LAN�AMENTO DE ' + sTipo + '. (Aba: Lan�amento)';
        DadosMsgErro.Componente := cedValorTotalPrev;
        DadosMsgErro.PageIndex  := 0;
        ListaMsgErro.Add(DadosMsgErro);

        Inc(QtdErros);
      end;
    end;
  end;

  if QtdErros > 0 then
    Result := False;
end;

function TFCadastroLancamento.VerificarDadosParcela(var Msg: String;
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  Msg      := '';
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if dteDataLancamentoParc.Date = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a DATA DE LAN�AMENTO DA PARCELA. (Aba: Lan�amento)';
    DadosMsgErro.Componente := dteDataLancamentoParc;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if dmLancamento.cdsParcela_F.FieldByName('NOVO').AsBoolean then
  begin
    if (not chbForaFechCaixa.Checked) and
      (dteDataLancamento.Date < vgDataLancVigente)  then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') A DATA DO LAN�AMENTO n�o pode ser inferior a ' +
                                                          FormatDateTime('DD/MM/YYYY', vgDataLancVigente) +
                                                          ', pois os Caixas anteriores a essa data j� se encontram FECHADOS.';
      DadosMsgErro.Componente := dteDataLancamento;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;
  end;

  if Trim(edtNumParcela.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o N�MERO DA PARCELA. (Aba: Lan�amento)';
    DadosMsgErro.Componente := edtNumParcela;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if dteDataVencimento.Date = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a DATA DE VENCIMENTO DA PARCELA. (Aba: Lan�amento)';
    DadosMsgErro.Componente := dteDataVencimento;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtFormaPagamento.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a FORMA DE PAGAMENTO DA PARCELA. (Aba: Lan�amento)';
    DadosMsgErro.Componente := edtFormaPagamento;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;
{  else
  begin
    if (Trim(edtFormaPagamento.Text) = '5') and
      (Trim(edtNumCodDeposito.Text) = '') and
      (cedValorParcelaPago.Value > 0) then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a IDENTIFICA��O DO DEP�SITO. (Aba: Lan�amento)';
      DadosMsgErro.Componente := edtNumCodDeposito;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end
  end; }

  if (Trim(cedValorParcelaPago.Text) <> '') and
    ((dteDataPagamentoParc.Date = 0) or
     (dteDataPagamentoParc.Date = Null)) then
  begin
    if not chbForaFechCaixa.Checked then  //if chbForaFechCaixa.Checked then
//      dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := IncDay(Now, 1)
//    else
    begin
      if (Date < vgDataLancVigente) or
        (Date < dteDataLancamento.Date) then
      begin
        if vgDataLancVigente < dteDataLancamento.Date then
          dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := dteDataLancamento.Date
        else
          dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := vgDataLancVigente;
      end;
//      else
//        dmLancamento.cdsParcela_F.FieldByName('DATA_PAGAMENTO').AsDateTime := Now;
    end;

    dmLancamento.cdsParcela_F.FieldByName('PAG_ID_USUARIO').AsInteger  := vgUsu_Id;
    dmLancamento.cdsParcela_F.FieldByName('FLG_STATUS').AsString       := 'G';
  end;
  
  if QtdErros > 0 then
    Result := False;
end;

function TFCadastroLancamento.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

function TFCadastroLancamento.VerificarDadosDetalhe(var Msg: String;
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  Msg      := '';
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if Trim(lcbItemDet.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o ITEM DO DETALHE. (Aba: Detalhe)';
    DadosMsgErro.Componente := lcbItemDet;
    DadosMsgErro.PageIndex  := 1;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if cedVlrUnitarioDet.Value < 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar o VALOR UNIT�RIO DO DETALHE. (Aba: Detalhe)';
    DadosMsgErro.Componente := cedVlrUnitarioDet;
    DadosMsgErro.PageIndex  := 1;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if cedQuantidadeDet.Value <= 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar a QUANTIDADE DO DETALHE. (Aba: Detalhe)';
    DadosMsgErro.Componente := cedQuantidadeDet;
    DadosMsgErro.PageIndex  := 1;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if cedVlrTotalDet.Value < 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar o VALOR TOTAL DO DETALHE. (Aba: Detalhe)';
    DadosMsgErro.Componente := cedVlrTotalDet;
    DadosMsgErro.PageIndex  := 1;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if QtdErros > 0 then
    Result := False;
end;

initialization
  TPicture.UnRegisterGraphicClass(TdxBMPImage);
  TPicture.UnRegisterGraphicClass(TdxPNGImage);
  TPicture.UnRegisterGraphicClass(TdxSmartImage);
  TPicture.RegisterFileFormat('BMP', 'Imagens BMP', TdxBMPImage);
  TPicture.RegisterFileFormat('PNG', 'Imagens PNG', TdxPNGImage);
  TPicture.RegisterFileFormat('JPG;*.JPEG;*.TIF;*.GIF;*.PNG;*.BMP', 'Todas as imagens', TdxSmartImage);

end.
