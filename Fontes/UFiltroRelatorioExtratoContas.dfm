inherited FFiltroRelatorioExtratoContas: TFFiltroRelatorioExtratoContas
  Caption = 'Relat'#243'rio de Extrato de Contas'
  ClientHeight = 205
  ExplicitHeight = 234
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlFiltro: TPanel
    Height = 199
    ExplicitHeight = 230
    object lblPeriodo: TLabel [0]
      Left = 5
      Top = 8
      Width = 125
      Height = 14
      Caption = 'Per'#237'odo de Pagamento'
    end
    object lblPeriodoInicio: TLabel [1]
      Left = 5
      Top = 31
      Width = 32
      Height = 14
      Caption = 'In'#237'cio:'
    end
    object lblPeriodoFim: TLabel [2]
      Left = 149
      Top = 31
      Width = 22
      Height = 14
      Caption = 'Fim:'
    end
    object lblSaldoInicial: TLabel [3]
      Left = 3
      Top = 59
      Width = 62
      Height = 14
      Caption = 'Saldo Inicial'
    end
    inherited pnlBotoes: TPanel
      Top = 152
      TabOrder = 7
      ExplicitTop = 183
      inherited btnLimpar: TJvTransparentButton
        OnClick = btnLimparClick
      end
    end
    inherited gbTipo: TGroupBox
      Left = 3
      Top = 84
      TabOrder = 3
      ExplicitLeft = 3
      ExplicitTop = 84
    end
    inherited chbExibirGrafico: TCheckBox
      Left = 97
      Top = 86
      TabOrder = 4
      Visible = False
      ExplicitLeft = 97
      ExplicitTop = 86
    end
    inherited chbExportarPDF: TCheckBox
      Left = 97
      Top = 109
      TabOrder = 5
      ExplicitLeft = 97
      ExplicitTop = 109
    end
    object dteDataInicio: TJvDateEdit [8]
      Left = 43
      Top = 28
      Width = 100
      Height = 22
      Color = 16114127
      ShowNullDate = False
      TabOrder = 0
      OnExit = dteDataInicioExit
      OnKeyPress = FormKeyPress
    end
    object dteDataFim: TJvDateEdit [9]
      Left = 177
      Top = 28
      Width = 100
      Height = 22
      Color = 16114127
      ShowNullDate = False
      TabOrder = 1
      OnKeyPress = FormKeyPress
    end
    inherited chbExportarExcel: TCheckBox
      Left = 97
      Top = 132
      TabOrder = 6
      ExplicitLeft = 97
      ExplicitTop = 132
    end
    object cedSaldoInicial: TJvCalcEdit
      Left = 71
      Top = 56
      Width = 100
      Height = 22
      Color = 16114127
      DisplayFormat = ',0.00'
      ReadOnly = True
      TabOrder = 2
      ZeroEmpty = False
      DecimalPlacesAlwaysShown = False
      OnKeyPress = FormKeyPress
    end
  end
end
