object dmEtiqueta: TdmEtiqueta
  OldCreateOrder = False
  Height = 229
  Width = 635
  object qryLoteEtq: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM LOTEETIQUETA'
      ' WHERE ID_LOTEETIQUETA = :ID_LOTEETIQUETA')
    Left = 32
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'ID_LOTEETIQUETA'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object qryEtiqueta: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM ETIQUETA'
      ' WHERE ID_ETIQUETA = :ID_ETIQUETA')
    Left = 104
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'ID_ETIQUETA'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object qrySistema: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM SISTEMA'
      'ORDER BY NOME_SISTEMA')
    Left = 240
    Top = 16
  end
  object qryNatAto: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM NATUREZAATO'
      
        ' WHERE ID_SISTEMA_FK = (CASE WHEN (:ID_SIST01 = 0) THEN ID_SISTE' +
        'MA_FK ELSE :ID_SIST02 END)'
      'ORDER BY DESCR_NATUREZAATO')
    Left = 312
    Top = 16
    ParamData = <
      item
        Name = 'ID_SIST01'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'ID_SIST02'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspLoteEtq: TDataSetProvider
    DataSet = qryLoteEtq
    Left = 32
    Top = 64
  end
  object dspEtiqueta: TDataSetProvider
    DataSet = qryEtiqueta
    Left = 104
    Top = 64
  end
  object cdsLoteEtq: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_LOTEETIQUETA'
        ParamType = ptInput
      end>
    ProviderName = 'dspLoteEtq'
    Left = 32
    Top = 112
    object cdsLoteEtqID_LOTEETIQUETA: TIntegerField
      FieldName = 'ID_LOTEETIQUETA'
      Required = True
    end
    object cdsLoteEtqNUM_LOTE: TStringField
      FieldName = 'NUM_LOTE'
      Size = 15
    end
    object cdsLoteEtqLETRA: TStringField
      FieldName = 'LETRA'
      Size = 10
    end
    object cdsLoteEtqNUM_INICIAL: TStringField
      FieldName = 'NUM_INICIAL'
      Size = 15
    end
    object cdsLoteEtqNUM_FINAL: TStringField
      FieldName = 'NUM_FINAL'
      Size = 15
    end
    object cdsLoteEtqQTD_ETIQUETAS: TIntegerField
      FieldName = 'QTD_ETIQUETAS'
    end
    object cdsLoteEtqMASCARA: TStringField
      FieldName = 'MASCARA'
      Size = 30
    end
    object cdsLoteEtqFLG_SITUACAO: TStringField
      FieldName = 'FLG_SITUACAO'
      FixedChar = True
      Size = 1
    end
    object cdsLoteEtqDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsLoteEtqCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsLoteEtqDATA_COMPRA: TDateField
      FieldName = 'DATA_COMPRA'
    end
    object cdsLoteEtqDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsLoteEtqCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
  end
  object cdsEtiqueta: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_ETIQUETA'
        ParamType = ptInput
      end>
    ProviderName = 'dspEtiqueta'
    Left = 104
    Top = 112
    object cdsEtiquetaID_ETIQUETA: TIntegerField
      FieldName = 'ID_ETIQUETA'
      Required = True
    end
    object cdsEtiquetaID_LOTEETIQUETA_FK: TIntegerField
      FieldName = 'ID_LOTEETIQUETA_FK'
    end
    object cdsEtiquetaORDEM: TIntegerField
      FieldName = 'ORDEM'
    end
    object cdsEtiquetaLETRA: TStringField
      FieldName = 'LETRA'
      Size = 4
    end
    object cdsEtiquetaNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object cdsEtiquetaALEATORIO: TStringField
      FieldName = 'ALEATORIO'
      FixedChar = True
      Size = 3
    end
    object cdsEtiquetaNUM_ETIQUETA: TStringField
      FieldName = 'NUM_ETIQUETA'
      Size = 50
    end
    object cdsEtiquetaDATA_UTILIZACAO: TDateField
      FieldName = 'DATA_UTILIZACAO'
    end
    object cdsEtiquetaID_SISTEMA_FK: TIntegerField
      FieldName = 'ID_SISTEMA_FK'
    end
    object cdsEtiquetaFLG_SITUACAO: TStringField
      FieldName = 'FLG_SITUACAO'
      FixedChar = True
      Size = 1
    end
    object cdsEtiquetaID_FUNCIONARIO_FK: TIntegerField
      FieldName = 'ID_FUNCIONARIO_FK'
    end
    object cdsEtiquetaID_NATUREZAATO_FK: TIntegerField
      FieldName = 'ID_NATUREZAATO_FK'
    end
    object cdsEtiquetaOBS_ETIQUETA: TStringField
      FieldName = 'OBS_ETIQUETA'
      Size = 600
    end
    object cdsEtiquetaDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsEtiquetaCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsEtiquetaDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsEtiquetaCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
  end
  object dsLoteEtq: TDataSource
    DataSet = cdsLoteEtq
    Left = 32
    Top = 160
  end
  object dsEtiqueta: TDataSource
    DataSet = cdsEtiqueta
    Left = 104
    Top = 160
  end
  object dsSistema: TDataSource
    DataSet = qrySistema
    Left = 240
    Top = 64
  end
  object dsNatAto: TDataSource
    DataSet = qryNatAto
    Left = 312
    Top = 64
  end
  object qryFuncionarios: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT ID_FUNCIONARIO, NOME_FUNCIONARIO'
      '  FROM FUNCIONARIO'
      'ORDER BY NOME_FUNCIONARIO')
    Left = 389
    Top = 16
  end
  object dsFuncionarios: TDataSource
    DataSet = qryFuncionarios
    Left = 389
    Top = 64
  end
  object qryEtqAux: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM ETIQUETA'
      ' WHERE ID_LOTEETIQUETA_FK = :ID_LOTEETIQUETA'
      '   AND ORDEM = :ORDEM')
    Left = 176
    Top = 112
    ParamData = <
      item
        Name = 'ID_LOTEETIQUETA'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'ORDEM'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryEtqAuxID_ETIQUETA: TIntegerField
      FieldName = 'ID_ETIQUETA'
      Origin = 'ID_ETIQUETA'
      Required = True
    end
    object qryEtqAuxID_LOTEETIQUETA_FK: TIntegerField
      FieldName = 'ID_LOTEETIQUETA_FK'
      Origin = 'ID_LOTEETIQUETA_FK'
    end
    object qryEtqAuxORDEM: TIntegerField
      FieldName = 'ORDEM'
      Origin = 'ORDEM'
    end
    object qryEtqAuxLETRA: TStringField
      FieldName = 'LETRA'
      Origin = 'LETRA'
      Size = 4
    end
    object qryEtqAuxNUMERO: TIntegerField
      FieldName = 'NUMERO'
      Origin = 'NUMERO'
    end
    object qryEtqAuxALEATORIO: TStringField
      FieldName = 'ALEATORIO'
      Origin = 'ALEATORIO'
      FixedChar = True
      Size = 3
    end
    object qryEtqAuxDATA_UTILIZACAO: TDateField
      FieldName = 'DATA_UTILIZACAO'
      Origin = 'DATA_UTILIZACAO'
    end
    object qryEtqAuxID_SISTEMA_FK: TIntegerField
      FieldName = 'ID_SISTEMA_FK'
      Origin = 'ID_SISTEMA_FK'
    end
    object qryEtqAuxFLG_SITUACAO: TStringField
      FieldName = 'FLG_SITUACAO'
      Origin = 'FLG_SITUACAO'
      FixedChar = True
      Size = 1
    end
    object qryEtqAuxID_FUNCIONARIO_FK: TIntegerField
      FieldName = 'ID_FUNCIONARIO_FK'
      Origin = 'ID_FUNCIONARIO_FK'
    end
    object qryEtqAuxID_NATUREZAATO_FK: TIntegerField
      FieldName = 'ID_NATUREZAATO_FK'
      Origin = 'ID_NATUREZAATO_FK'
    end
    object qryEtqAuxDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
      Origin = 'DATA_CADASTRO'
    end
    object qryEtqAuxCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
      Origin = 'CAD_ID_USUARIO'
    end
    object qryEtqAuxDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
      Origin = 'DATA_CANCELAMENTO'
    end
    object qryEtqAuxCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
      Origin = 'CANCEL_ID_USUARIO'
    end
    object qryEtqAuxNUM_ETIQUETA: TStringField
      FieldName = 'NUM_ETIQUETA'
      Origin = 'NUM_ETIQUETA'
      Size = 50
    end
    object qryEtqAuxOBS_ETIQUETA: TStringField
      FieldName = 'OBS_ETIQUETA'
      Origin = 'OBS_ETIQUETA'
      Size = 600
    end
  end
  object dsEtqAux: TDataSource
    DataSet = qryEtqAux
    Left = 176
    Top = 160
  end
  object qryNatureza: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM NATUREZAATO'
      ' WHERE ID_NATUREZAATO = :ID_NATUREZAATO')
    Left = 496
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'ID_NATUREZAATO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspNatureza: TDataSetProvider
    DataSet = qryNatureza
    Left = 496
    Top = 64
  end
  object cdsNatureza: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_NATUREZAATO'
        ParamType = ptInput
      end>
    ProviderName = 'dspNatureza'
    Left = 496
    Top = 112
    object cdsNaturezaID_NATUREZAATO: TIntegerField
      FieldName = 'ID_NATUREZAATO'
      Required = True
    end
    object cdsNaturezaDESCR_NATUREZAATO: TStringField
      FieldName = 'DESCR_NATUREZAATO'
      Size = 250
    end
    object cdsNaturezaID_SISTEMA_FK: TIntegerField
      FieldName = 'ID_SISTEMA_FK'
    end
  end
  object dsNatureza: TDataSource
    DataSet = cdsNatureza
    Left = 496
    Top = 160
  end
  object qrySistN: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM SISTEMA'
      'ORDER BY NOME_SISTEMA')
    Left = 576
    Top = 16
  end
  object dsSistN: TDataSource
    DataSet = qrySistN
    Left = 576
    Top = 64
  end
end
