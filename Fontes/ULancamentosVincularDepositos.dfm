object FLancamentosVincularDepositos: TFLancamentosVincularDepositos
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'V'#237'ncula'#231#227'o de Lan'#231'amentos e Dep'#243'sitos'
  ClientHeight = 466
  ClientWidth = 1089
  Color = 11578959
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  object pnlRepasse: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 1083
    Height = 460
    Align = alClient
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 0
    object lblObservacoes: TLabel
      Left = 559
      Top = 372
      Width = 69
      Height = 14
      Caption = 'Observa'#231#245'es'
    end
    object btnConfirmarVinculo: TJvTransparentButton
      Left = 990
      Top = 432
      Width = 86
      Height = 22
      Hint = 'Confirmar dados do V'#237'nculo'
      Caption = 'Vincular'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      FrameStyle = fsIndent
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      Spacing = 12
      TextAlign = ttaRight
      Transparent = False
      OnClick = btnConfirmarVinculoClick
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        1800000000000006000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFF4F9EFDDE4ECD8E0ECD8E0EFDDE4FFF4F9FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFAE6E6E6E2
        E2E2E2E2E2E6E6E6FAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFF3FAA3B6AA329064008A47008D49008D49008A47329064A3B6AAFFF3
        FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBABABAB6161613A3A3A39
        39393939393A3A3A616161ABABABFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFECF62F8D5F009A5B34CDA539DDB23AE1B33AE1B239DDB234CDA5009A5A2F8D
        5FFFECF6FFFFFFFFFFFFFFFFFFFFFFFFF8F8F85E5E5E47474788888892929295
        95959595959292928888884747475E5E5EF8F8F8FFFFFFFFFFFFFFFFFFFFF5FE
        1A8B550DB5833EE0B600D79700D49600D49700D49700D49600D7973EE0B50DB5
        831A8B55FFF5FEFFFFFFFFFFFFFFFFFF53535367676797979770707070707070
        7070707070707070707070979797676767535353FFFFFFFFFFFFFFFFFF75A78C
        00A97233DFB200D29500D29600CB8700CE8F00D39900D39900D29800D29533DF
        B200A97275A78CFFFFFFFFFFFF8E8E8E5959599191916D6D6D70707061616168
        68687373737373737272726D6D6D9191915959598E8E8EFFFFFFFFF6FE008641
        45DDB800D19400CF9404CE93FFFFFFA1EDD700C78400D29A00D19900D19900D1
        9445DDB6008641FFF6FEFFFFFF3333339999996D6D6D6D6D6D717171FFFFFFCA
        CACA5C5C5C7272727272727272726D6D6D999999333333FFFFFFA7C6B605AA75
        19D8A700CF9605CC93FFFFFFFFFFFFFFFFFF9DECD600C68400D09A00D09900CF
        9819D8A606AB74A5C9B6B9B3B65E5E5E8181816E6E6E707070FFFFFFFFFFFFFF
        FFFFC8C8C85B5B5B7272727272727070708282825E5E5EB6B6B678B7962EC69C
        00D09800C789FFFFFFFFFFFFA0EAD3FFFFFFFFFFFF9DEBD600C48400CF9A00CE
        9900D0982BC49974BC979C93978080806F6F6F5F5F5FFFFFFFFFFFFFC7C7C7FF
        FFFFFFFFFFC7C7C75B5B5B7171717070706F6F6F7F7F7F9898987CB79738D0A8
        00CC9400C99270E2C4B5EFDF00C18004CA92FFFFFFFFFFFF9DEAD600C28400CD
        9900CE972CC49979BC9A9D95998B8B8B6C6C6C696969AFAFAFD4D4D45454546F
        6F6FFFFFFFFFFFFFC8C8C85959597171716F6F6F7F7F7F9A9A9A75B49250DDB9
        00C99300CB9900C89100C78F00CB9900C99404C893FFFFFFFFFFFFA8ECD900C7
        8F00CC982BC49874BC979990949E9E9E6969697070706767676464647070706A
        6A6A6E6E6EFFFFFFFFFFFFCDCDCD6464646E6E6E7F7F7F989898B1D2BF38C69B
        18D3A700C99700CA9900CA9900CA9900CA9900C79404C793FFFFFFEFFBFA00C2
        8A19D3A604AA73B3D8C4C3BEC18585857F7F7F6C6C6C6E6E6E6E6E6E6E6E6E6E
        6E6E6969696F6F6FFFFFFFF6F6F65E5E5E8080805C5C5CC5C5C5FFFFFF07985C
        64F5D700BF8D00C89900C89900C89900C89900C89900C69421CCA116CC9E00C5
        9245DBB800853FFFFFFFFFFFFF525252B5B5B56262626F6F6F6F6F6F6F6F6F6F
        6F6F6F6F6F6969697F7F7F7A7A7A666666989898313131FFFFFFFFFFFF81BB9C
        6FE1C12FE0B800BF8D00C79900C79900C79900C79900C79900C69700C39233D8
        B300A76D90C3A8FFFFFFFFFFFF9E9E9EACACAC9191916363636E6E6E6E6E6E6D
        6D6D6D6D6D6D6D6D6A6A6A6565658E8E8E545454AAAAAAFFFFFFFFFFFFFFFFFF
        319B66A9FFF043E8C300BA8900C09200C29400C29400C29400C2963ED9B708B2
        7E2B9C68FFFFFFFFFFFFFFFFFFFFFFFF666666DADADA9E9E9E5C5C5C66666668
        6868686868686868676767959595636363656565FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF41A2706ED5B49FFFF64DECCA43E2C042E1BF45E1C140D6B00094524AA8
        7AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF717171A5A5A5DADADAA6A6A69B
        9B9B9B9B9B9D9D9D9292923E3E3E797979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFCDE3D568B7913AAD7D33AC7B20A671109D6363B68FDCEBE1FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D7D790909076767672
        72726666665959598D8D8DE3E3E3FFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
    end
    object btnCancelarVinculo: TJvTransparentButton
      Left = 990
      Top = 404
      Width = 86
      Height = 22
      Hint = 'Cancelar dados do V'#237'nculo'
      Caption = 'Cancelar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      FrameStyle = fsIndent
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      Spacing = 11
      TextAlign = ttaRight
      Transparent = False
      OnClick = btnCancelarVinculoClick
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        1800000000000006000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFF7F5EAE4E2D2E6E4D4E6E4D4E6E4D4E7E5D4E5E3D2F7F5EAFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1F1F1DCDCDCDEDEDEDF
        DFDFDFDFDFDFDFDFDDDDDDF1F1F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFDFAE4424EB71426BA1829B91627B71526B71223B70C1EB6424FB7FEFB
        E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F2F27474745A5A5A5C5C5C5A
        5A5A595959565656545454737373F3F3F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FDFAE4414EB44252D57184FF6D80FF6D80FF6D81FF6E81FF7387FF3142D0414D
        B4FEFBE4FFFFFFFFFFFFFFFFFFFFFFFFF2F2F2727272808080B3B3B3AEAEAEAE
        AEAEAFAFAFAFAFAFB4B4B4747474727272F3F3F3FFFFFFFFFFFFFFFFFFFCFAE4
        434FB54B5BD8667AFF5469FA5E72FB5E71FB5E71FB5E72FB5469FA697DFF3041
        D0414DB5FEFBE4FFFFFFFFFFFFF2F2F2737373878787A9A9A99B9B9BA1A1A1A0
        A0A0A0A0A0A1A1A19B9B9BACACAC737373737373F3F3F3FFFFFFFFFCEA434FB5
        5464D95D72FF3C54F8A5AFFC3F55F85B6FF95B6FF93D55F8A5AFFC3C54F86579
        FF2F41CF404DB4FFFFF1F5F5F57373738D8D8DA4A4A48C8C8CC9C9C98D8D8D9E
        9E9E9E9E9E8D8D8DC9C9C98C8C8CA9A9A9737373727272F6F6F68991D35564DA
        556BFF435BF6C2CAFCFFFFFFC8CEFC324BF6324BF6CCD2FCFFFFFFC3CBFC435A
        F65E74FF2639CC9AA2E8ABAAA28E8E8E9E9E9E8F8F8FDADADAFFFFFFDEDEDE85
        8585858585E1E1E1FFFFFFDBDBDB909090A4A4A46D6D6DADADAD8E95D45E70E7
        4E64F83B54F4E1E5FDFFFFFFFFFFFFA6B1FAA6B1FAFFFFFFFFFFFFDDE1FC3C55
        F45368F9384CDC9EA6E8AEADA49898989696968A8A8AEEEEEEFFFFFFFFFFFFCA
        CACACACACAFFFFFFFFFFFFEBEBEB8A8A8A9A9A9A7F7F7FAFAFAF8E95D26373E7
        495FF54C62F32540F1DADFFDFFFFFFFFFFFFFFFFFFFFFFFFDADFFD2641F14C62
        F34C63F73A4DDA9EA6E8ADACA49C9C9C9292929494947C7C7CE9E9E9FFFFFFFF
        FFFFFFFFFFFFFFFFE9E9E97C7C7C9494949595957F7F7FAFAFAF8D94D26878E7
        425BF1475EF04960F00324EBF2F3FEFFFFFFFFFFFFF2F3FE0324EB4960F0475E
        F0475FF43B4FD99EA6E8ACABA39D9D9D8D8D8D8F8F8F919191666666F7F7F7FF
        FFFFFFFFFFF7F7F76666669191918F8F8F919191808080AFAFAF8C93D26C7CE9
        3D57EF435CEE1C3AEBB2BCF7FFFFFFFFFFFFFFFFFFFFFFFFB2BCF71C3AEB435C
        EE425BF23C50DA9FA6E8ACABA3A2A2A28989898C8C8C757575D0D0D0FFFFFFFF
        FFFFFFFFFFFFFFFFD0D0D07575758C8C8C8E8E8E808080AFAFAF8991D16E7FEA
        3852ED2B46EBB9C2F8FFFFFFFFFFFFBFC7F9BFC7F9FFFFFFFFFFFFB9C2F82B46
        EB3D57EE3D51DB9CA4E8ABAAA1A3A3A38686867D7D7DD4D4D4FFFFFFFFFFFFD8
        D8D8D8D8D8FFFFFFFFFFFFD4D4D47D7D7D898989818181AEAEAE8991D78894E5
        2241EB203DE8E3E6FBFFFFFFD5DAFA0F2EE60F2EE6D5DAFAFFFFFFE3E6FB203D
        E83350F03D4DD0959EE6ADACA4B0B0B0787878767676EDEDEDFFFFFFE5E5E56B
        6B6B6B6B6BE5E5E5FFFFFFEDEDED7676768585857C7C7CB1B1B1FFFFFF5C66CB
        97A1E91A3AE90F2EE4ABB6F50E2DE4354FE8354FE80E2DE4ABB6F50E2EE42B49
        ED4D5DD35963C9FFFFFFFFFFFF8A8A8ABABABA7474746A6A6ACBCBCB6A6A6A83
        83838383836A6A6ACBCBCB6A6A6A7F7F7F868686878787FFFFFFFFFFFFFFFFFF
        5E69CBA4AEEC1333E52240E5314CE6304BE6304BE6314CE6223FE42341EA5767
        D65964C9FFFFFFFFFFFFFFFFFFFFFFFF8C8C8CC3C3C36D6D6D7777777F7F7F7E
        7E7E7E7E7E7F7F7F7676767878788D8D8D888888FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF606BCCABB2EE0E30E51C3CE61D3DE61E3DE7203FE71A3BE9606FD95B66
        CAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EC7C7C76A6A6A73737374
        7474757575767676747474949494898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF737CD3727CD56974D26470D2616CD05A67CF5560CC6D78D1FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9B9B9B9C9C9C95959593
        93938F8F8F8B8B8B878787979797FFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
    end
    object lblDataVinculo: TLabel
      Left = 5
      Top = 416
      Width = 60
      Height = 14
      Caption = 'Dt. V'#237'nculo'
    end
    object lblDescricaoVinculo: TLabel
      Left = 111
      Top = 416
      Width = 112
      Height = 14
      Caption = 'Descri'#231#227'o do V'#237'nculo'
    end
    object lblValorTotalLancamento: TLabel
      Left = 5
      Top = 372
      Width = 122
      Height = 14
      Caption = 'Vlr. Tot. Lan'#231'amentos'
    end
    object lblValorTotalDespesa: TLabel
      Left = 133
      Top = 372
      Width = 107
      Height = 14
      Caption = 'Vlr. Total Dep'#243'sitos'
    end
    object gbLancamentos: TGroupBox
      Left = 7
      Top = 8
      Width = 546
      Height = 358
      Caption = 'Lan'#231'amentos'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      object lblPeriodoL: TLabel
        Left = 6
        Top = 16
        Width = 130
        Height = 14
        Caption = 'Per'#237'odo de Lan'#231'amento'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 109
        Top = 35
        Width = 6
        Height = 14
        Caption = 'a'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblValorLancamento: TLabel
        Left = 224
        Top = 16
        Width = 27
        Height = 14
        Caption = 'Valor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblNumReciboL: TLabel
        Left = 330
        Top = 16
        Width = 54
        Height = 14
        Caption = 'N'#186' Recibo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object btnFiltrarLancamentos: TJvTransparentButton
        Left = 297
        Top = 77
        Width = 83
        Height = 26
        Hint = 
          'Clique para Filtrar a pesquisa de acordo com as informa'#231#245'es forn' +
          'ecidas'
        Caption = 'Filtr&ar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        Transparent = False
        OnClick = btnFiltrarLancamentosClick
      end
      object spLegendaSemVinculoL: TShape
        Left = 11
        Top = 340
        Width = 10
        Height = 10
        Brush.Color = 10797567
      end
      object lblLegendaSemVinculoL: TLabel
        Left = 24
        Top = 337
        Width = 67
        Height = 14
        Caption = 'Sem V'#237'nculo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object spLegendaVinculadoL: TShape
        Left = 99
        Top = 340
        Width = 10
        Height = 10
        Brush.Color = 14680031
      end
      object lblLegendaVinculadoL: TLabel
        Left = 112
        Top = 337
        Width = 52
        Height = 14
        Caption = 'Vinculado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object btnEditarLancamento: TJvTransparentButton
        Left = 256
        Top = 333
        Width = 142
        Height = 22
        Hint = 
          'Abre tela de cadastro de Lan'#231'amento para edi'#231#227'o do registro sele' +
          'cionado'
        Caption = 'Editar Lan'#231'amento'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        Spacing = 9
        TextAlign = ttaRight
        Transparent = False
        OnClick = btnEditarLancamentoClick
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          1800000000000006000000000000000000000000000000000000FFFFFFFFFFFF
          FFFFFFBD88D3C38AD9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBBBBBBC0C0C0FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          BE8BD3D5A6D2E8B9E887877FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBCBCBCCACACADFDFDF848484FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAE7DCE
          BB7FB7C990C9868980F2F0EB93867AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFB1B1B1ADADADBEBEBE858585EEEEEE848484FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF986ACA
          B672B28A8E84D3D0CB90857DB9E3F4327DC9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A3A6A6A6898989CFCFCF848484D9D9D97D
          7D7DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC
          787D6EB9B6AF958A8196BFDE32B9FF4ECEFF397DC6FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC767676B4B4B4898989BBBBBB9F9F9FAE
          AEAE7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          CCCCCC807A727FABD1129BFF29A8FF3DB9FF51CFFF397DC6FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC787878A9A9A98C8C8C989898A3
          A3A3B0B0B07F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFCCCCCC326CA2169CFF1E9CFF2DA8FF3EB9FF51CFFF397DC6FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC6A6A6A8E8E8E9191919A
          9A9AA4A4A4B0B0B07F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFCCCCCC3D6E9B1A9DFF1F9CFF2DA8FF3EB9FF51CFFF397DC6FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC6C6C6C90909092
          92929A9A9AA4A4A4B0B0B07F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFCCCCCC3E6E9A1A9DFF1F9CFF2DA8FF3EB9FF51CFFF397D
          C6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC6C6C6C90
          90909292929A9A9AA4A4A4B0B0B07F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFCCCCCC3E6E9A1A9DFF1F9CFF2DA8FF3EB9FF51CF
          FF397DC5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC6C
          6C6C9090909292929A9A9AA4A4A4B0B0B07F7F7FFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC3E6E9A1A9DFF1F9CFF2DA8FF3DB9
          FF4ECDFF3478C3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC
          CCCC6C6C6C9090909292929A9A9AA3A3A3AEAEAE7B7B7BFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC3E6E9A1A9DFF1E9BFF29A6
          FF43BCFF88E2FF4FB1EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFCCCCCC6C6C6C909090919191989898A6A6A6CACACAA1A1A1FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC3E6E9A179CFF259F
          FF99DBF99FE6FF4CAAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFCCCCCC6C6C6C8E8E8E959595CDCDCDD4D4D49E9E9EFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC3A6B9A5FBB
          FF90D5FD80A4B55F829DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFCCCCCC6A6A6AB1B1B1C9C9C99C9C9C7F7F7FFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC538C
          D14F94D94A6884596C7CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFCCCCCC9191919494946767676B6B6BFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCC
          CCCCCCCCCCCCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCCCCCCCCCCCCCCCCCC}
        NumGlyphs = 2
      end
      object btnVisualizarVincL: TJvTransparentButton
        Left = 400
        Top = 333
        Width = 142
        Height = 22
        Caption = 'Visualizar V'#237'nculo(s)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        ParentShowHint = False
        ShowHint = False
        Spacing = 9
        TextAlign = ttaRight
        Transparent = False
        OnClick = btnVisualizarVincLClick
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          1800000000000006000000000000000000000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFE5E5E5CECECECCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E5CECECECCCCCCCC
          CCCCCCCCCCCCCCCCDADADAF5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          F9F9F9CCCCCC67A1C30979BB0377BA0377BA0377BA0377B9398DBEA4BBC8E5E5
          E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9CCCCCC98989867676763636363
          63636363636363637F7F7FB7B7B7E5E5E5FFFFFFFFFFFFFFFFFFFFFFFFF9F9F9
          B5C2CA1587C9339FDD4FB7F256BEFA56BEFC55BDFA55BBF644ABE51F8CCC66A0
          C3DDDDDDFFFFFFFFFFFFFFFFFFF9F9F9C0C0C07474748D8D8DA5A5A5ACACACAD
          ADADACACACAAAAAA9999997A7A7A979797DDDDDDFFFFFFFFFFFFFDFDFDB8C5CC
          3FA2DBAFD5E3F9F5E9E1AF77B8711DB77423B8711DDEAC75F1F9FDA7DBF84AAC
          E2559FC9DEDEDEFFFFFFFDFDFDC3C3C3919191CBCBCBF1F1F1A3A3A35E5E5E61
          61615E5E5EA0A0A0F7F7F7D2D2D29A9A9A929292DEDEDEFFFFFFCCD1D44EADE3
          E4E2D0FFF2DAD2A774A87531988C61919576978D64A57632CA9B64FFFFFFFFFF
          F782C5E862A9D2FFFFFFD0D0D09D9D9DDBDBDBEBEBEB9B9B9B6464647D7D7D88
          88887F7F7F6464648E8E8EFFFFFFFCFCFCB8B8B89D9D9DDCDCDC6CBCE8EDE0C9
          FFE9CBFCF0DEA66B21938E661A1D2417191E171B1F86825FA1691DFFF9F3FFFC
          F3FFFCEDEAEDE8FFFFFFAEAEAED9D9D9E1E1E1EBEBEB5959597F7F7F1F1F1F1A
          1A1A1B1B1B757575565656F8F8F8F9F9F9F7F7F7EAEAEAB0B0B00479BC5C9EBC
          FFEDCBFFF2DEA670298C967A18191F1B1C1E5B5B5EA2A799BB9562F0DFCBFFFA
          EEFFFEE95FA4C7FFFFFF656565909090E3E3E3EDEDED5E5E5E8989891B1B1B1C
          1C1C5C5C5CA0A0A0888888DADADAF6F6F6F6F6F6969696636363CFE5F20072B9
          5A9EBFFFF7DAB270219A8E631C1D2014131699999AD0CBB8DBBA92FFFFF2FFF5
          E55CA2C60073B8FFFFFFE2E2E2616161909090EDEDED5E5E5E7F7F7F1E1E1E15
          1515999999C4C4C4B1B1B1FAFAFAF1F1F1949494616161EEEEEEFFFFFF67AEDC
          0074B91683C28B8568BE7B29AB8C519F8F5FEDD7BAFFE3C6E5DDCE94BFD1137D
          BA0278BDAFD5EDFFFFFFFFFFFFA4A4A46161617171717B7B7B6868687B7B7B7F
          7F7FD0D0D0DDDDDDD9D9D9B5B5B56B6B6B646464CFCFCFFFFFFFFFFFFFFFFFFF
          6CB4E01583CA047CC60077C20077C20076BF0072BB006FB70073BA057AC01986
          CBC3E0F2FFFFFFFFFFFFFFFFFFFFFFFFA9A9A97373736A6A6A65656565656564
          64646262625F5F5F616161676767767676DCDCDCFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFD4E9F662AFE12A92D72B92D72B92D72A91D62A91D63094D692C7EAFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE6E6E6A4A4A484848484848484
          8484838383838383878787C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        NumGlyphs = 2
      end
      object dtePeriodoIniL: TJvDateEdit
        Left = 5
        Top = 32
        Width = 100
        Height = 22
        Color = 16114127
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ShowNullDate = False
        TabOrder = 0
        OnKeyPress = dtePeriodoIniLKeyPress
      end
      object dtePeriodoFimL: TJvDateEdit
        Left = 118
        Top = 32
        Width = 100
        Height = 22
        Color = 16114127
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ShowNullDate = False
        TabOrder = 1
        OnKeyPress = dtePeriodoFimLKeyPress
      end
      object cedValorLancamento: TJvCalcEdit
        Left = 224
        Top = 32
        Width = 100
        Height = 22
        Color = 16114127
        DisplayFormat = ',0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        DecimalPlacesAlwaysShown = False
        OnKeyPress = cedValorLancamentoKeyPress
      end
      object edtNumReciboL: TEdit
        Left = 330
        Top = 32
        Width = 100
        Height = 22
        Color = 16114127
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        NumbersOnly = True
        ParentFont = False
        TabOrder = 3
        OnKeyPress = edtNumReciboLKeyPress
      end
      object rgSituacaoL: TRadioGroup
        Left = 5
        Top = 60
        Width = 286
        Height = 43
        Caption = 'Situa'#231#227'o'
        Columns = 3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemIndex = 1
        Items.Strings = (
          'Todos'
          'Sem V'#237'nculo'
          'Vinculados')
        ParentFont = False
        TabOrder = 4
        OnClick = rgSituacaoLClick
        OnExit = rgSituacaoLExit
      end
      object chbSelecionarTodosL: TCheckBox
        Left = 386
        Top = 86
        Width = 156
        Height = 17
        BiDiMode = bdLeftToRight
        Caption = 'Marcar Todos'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBiDiMode = False
        ParentFont = False
        TabOrder = 5
        OnClick = chbSelecionarTodosLClick
        OnKeyPress = FormKeyPress
      end
      object dbgLancamentos: TJvDBUltimGrid
        Left = 7
        Top = 107
        Width = 535
        Height = 223
        DataSource = dsLanc
        Options = [dgTitles, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 6
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        OnCellClick = dbgLancamentosCellClick
        OnDrawColumnCell = dbgLancamentosDrawColumnCell
        OnTitleClick = dbgLancamentosTitleClick
        MinColumnWidth = 26
        AutoSizeColumnIndex = 0
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 18
        TitleRowHeight = 18
        Columns = <
          item
            Expanded = False
            FieldName = 'SELECIONADO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Alignment = taCenter
            Title.Caption = 'Sel.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 27
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'NUM_RECIBO'
            Title.Alignment = taCenter
            Title.Caption = 'Recibo'
            Width = 74
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Caption = 'Descri'#231#227'o'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 229
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA_LANCAMENTO_PARC'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Caption = 'Dt. Entrada'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VR_PARCELA_PREV'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Caption = 'Vlr. Previsto'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 106
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VINCULOS'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Caption = 'V'#237'nculos'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_PARCELA_PAGO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'COD_LANCAMENTO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ANO_LANCAMENTO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_LANCAMENTO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'TIPO_LANCAMENTO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'TIPO_CADASTRO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_CATEGORIA_DESPREC_FK'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_SUBCATEGORIA_DESPREC_FK'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_NATUREZA_FK'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_CLIENTE_FORNECEDOR_FK'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_IMPOSTORENDA'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_PESSOAL'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_AUXILIAR'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_REAL'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_FLUTUANTE'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'QTD_PARCELAS'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_TOTAL_PREV'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_TOTAL_JUROS'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_TOTAL_DESCONTO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_TOTAL_PAGO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'CAD_ID_USUARIO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_CADASTRO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_STATUS'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_CANCELADO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_CANCELAMENTO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'CANCEL_ID_USUARIO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_RECORRENTE'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_REPLICADO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_ORIGEM'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_SISTEMA_FK'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'OBSERVACAO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_CARNELEAO_EQUIVALENCIA_FK'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_FORAFECHCAIXA'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_LANCAMENTO_PARC'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'COD_LANCAMENTO_FK'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ANO_LANCAMENTO_FK'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'NUM_PARCELA'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_VENCIMENTO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_PARCELA_JUROS'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VR_PARCELA_DESCONTO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_FORMAPAGAMENTO_FK'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'AGENCIA'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'CONTA'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_BANCO_FK'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'NUM_CHEQUE'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'NUM_COD_DEPOSITO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_STATUS_1'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_CANCELAMENTO_1'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'CANCEL_ID_USUARIO_1'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_PAGAMENTO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'PAG_ID_USUARIO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'OBS_LANCAMENTO_PARC'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'CAD_ID_USUARIO_1'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_CADASTRO_1'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_FORMAPAGAMENTO_ORIG_FK'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_STATUS_PARC'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'VINCULADO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DESCR_NATUREZA'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end>
      end
    end
    object gbDepositos: TGroupBox
      Left = 559
      Top = 8
      Width = 517
      Height = 358
      Caption = 'Dep'#243'sitos'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      object lblPeriodoD: TLabel
        Left = 5
        Top = 16
        Width = 111
        Height = 14
        Caption = 'Per'#237'odo do Dep'#243'sito'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 108
        Top = 35
        Width = 6
        Height = 14
        Caption = 'a'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblValorDeposito: TLabel
        Left = 223
        Top = 16
        Width = 27
        Height = 14
        Caption = 'Valor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object btnFiltrarDepositos: TJvTransparentButton
        Left = 297
        Top = 77
        Width = 83
        Height = 26
        Hint = 
          'Clique para Filtrar a pesquisa de acordo com as informa'#231#245'es forn' +
          'ecidas'
        Caption = 'Filtr&ar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        Transparent = False
        OnClick = btnFiltrarDepositosClick
      end
      object lblNumCodDeposito: TLabel
        Left = 329
        Top = 16
        Width = 64
        Height = 14
        Caption = 'ID Dep'#243'sito'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Shape1: TShape
        Left = 11
        Top = 340
        Width = 10
        Height = 10
        Brush.Color = 10797567
      end
      object Label1: TLabel
        Left = 24
        Top = 336
        Width = 67
        Height = 14
        Caption = 'Sem V'#237'nculo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Shape2: TShape
        Left = 99
        Top = 340
        Width = 10
        Height = 10
        Brush.Color = 14680031
      end
      object Label4: TLabel
        Left = 112
        Top = 336
        Width = 52
        Height = 14
        Caption = 'Vinculado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object btnDesfazerVinculo: TJvTransparentButton
        Left = 227
        Top = 333
        Width = 142
        Height = 22
        Hint = 
          'Desfazer V'#237'nculo(s) '#224' Parcela(s) de Lan'#231'amento atreladas ao Dep'#243 +
          'sito selecionado'
        Caption = 'Desfazer V'#237'nculo(s)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        Spacing = 9
        TextAlign = ttaRight
        Transparent = False
        OnClick = btnDesfazerVinculoClick
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          1800000000000006000000000000000000000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFDFDFDFEFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFDFDFDFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          E9E9E99090918D8D8D8F9091939393EAEAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E9E99090908D8D8D909090939393EA
          EAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE6E6E6
          8E8E8EF0F0F0F2F2F0CFCCCADDDCDA878787E8E8E8FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFE6E6E68E8E8EF0F0F0F2F2F2CCCCCCDBDBDB87
          8787E8E8E8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5F5F58B8B8A
          F9F8F7EDECEBEEEDECE4E5E4C4C2BFE5E4E2878787E8E8E8FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF2F2F28B8B8BF8F8F8ECECECEDEDEDE4E4E4C1C1C1E3
          E3E3878787E8E8E8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B3F1F1F0
          E3E4E2E3E3E1E3E3E1E7E7E5E5E5E3C3C2BFE6E4E3878787E8E8E8FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFABABABF1F1F1E2E2E2E2E2E2E2E2E2E6E6E6E4E4E4C1
          C1C1E4E4E4878787E8E8E8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFC0C0EAE8E6
          DCDBDADDDCDBDDDCDBDDDCDBE0DEDDE4E3E4C4C1BFE4E3E28B888DE7EAE6FFFF
          FFFFFFFFFFFFFFFFFFFFB9B9B9E8E8E8DBDBDBDCDCDCDCDCDCDCDCDCDFDFDFE4
          E4E4C0C0C0E3E3E38B8B8BE7E7E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA2A1A2
          E0DFDDD6D5D3D7D6D4D7D6D4D6D5D4D8D7D5E5E5E4C7C6C2E8E1E8907A9AE6EC
          E3FFFFFFFFFFFFFFFFFFFFFFFFA2A2A2DEDEDED4D4D4D5D5D5D5D5D5D5D5D5D7
          D7D7E4E4E4C4C4C4E7E7E7909090E6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          A6A5A6DAD8D5CFCDCCD0CECDD0CECDD0CECDCCCAC8E8ECDEBE9DEED3B2FD9777
          A2E5ECE2FFFFFFFFFFFFFFFFFFFFFFFFA6A6A6D7D7D7CDCDCDCECECECECECECE
          CECECACACAE4E4E4CBCBCBDDDDDD959595E5E5E5FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFA9A9A9D3D2D0C8C6C5CAC8C7C9C9C5CACAC3CFAAFAE4DAF6BA9CE8D7B7
          FF9B7BA8E5ECE2FFFFFFFFFFFFFFFFFFFFFFFFA9A9A9D1D1D1C6C6C6C8C8C8C7
          C7C7C7C7C7D9D9D9EAEAEAC7C7C7E1E1E19A9A9AE5E5E5FFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFADADADCDCCCAC0C0BAC4C1C2CFADFBCEB0F4CAA8F2E4DAF6BA9C
          E8D6B7FF9C7EABF0F5EEFFFFFFFFFFFFFFFFFFFFFFFFADADADCBCBCBBDBDBDC3
          C3C3DBDBDBD7D7D7D4D4D4E9E9E9C6C6C6E0E0E09C9C9CF3F3F3FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFB2B2B2CCC5CFC8A6F8C9ADF0C9ADF0C9ADF0C5A4F0E4D8
          F6BA9AE8DCC5FCBDAEC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B1B1CCCCCCD4
          D4D4D2D2D2D2D2D2D3D3D3CFCFCFE9E9E9C6C6C6E4E4E4C2C2C2FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFBBB0BED1B7FAC1A4ECC5A9EDC5A9EDC5A9EDBE9F
          ECE1D5F4CEB6F2CEBFD1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBABABADC
          DCDCCCCCCCCFCFCFCFCFCFCFCFCFCBCBCBE6E6E6D8D8D8D2D2D2FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC2AEC6D4BEF9BB9EE8C0A5EAC0A5EABDA0
          E9CAB0F5C2AEC7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0
          C0C0DEDEDEC7C7C7CCCCCCCCCCCCC9C9C9D5D5D5C0C0C0FFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB59ABAD5C5F9B597E5B699E5CAB4
          F4C6B3C8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFB1B1B1E0E0E0C2C2C2C3C3C3D6D6D6C3C3C3FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3AEC5D9C9F5D7C9F7C2AD
          C5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFBFBFBFE1E1E1E1E1E1BFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCEBDD3CFBED3FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFCDCDCDCDCDCDFFFFFFFFFFFFFFFFFFFFFFFF}
        NumGlyphs = 2
      end
      object btnVisualizarVincD: TJvTransparentButton
        Left = 371
        Top = 333
        Width = 142
        Height = 22
        Caption = 'Visualizar V'#237'nculo(s)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        ParentShowHint = False
        ShowHint = False
        Spacing = 9
        TextAlign = ttaRight
        Transparent = False
        OnClick = btnVisualizarVincDClick
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          1800000000000006000000000000000000000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFE5E5E5CECECECCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E5CECECECCCCCCCC
          CCCCCCCCCCCCCCCCDADADAF5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          F9F9F9CCCCCC67A1C30979BB0377BA0377BA0377BA0377B9398DBEA4BBC8E5E5
          E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9CCCCCC98989867676763636363
          63636363636363637F7F7FB7B7B7E5E5E5FFFFFFFFFFFFFFFFFFFFFFFFF9F9F9
          B5C2CA1587C9339FDD4FB7F256BEFA56BEFC55BDFA55BBF644ABE51F8CCC66A0
          C3DDDDDDFFFFFFFFFFFFFFFFFFF9F9F9C0C0C07474748D8D8DA5A5A5ACACACAD
          ADADACACACAAAAAA9999997A7A7A979797DDDDDDFFFFFFFFFFFFFDFDFDB8C5CC
          3FA2DBAFD5E3F9F5E9E1AF77B8711DB77423B8711DDEAC75F1F9FDA7DBF84AAC
          E2559FC9DEDEDEFFFFFFFDFDFDC3C3C3919191CBCBCBF1F1F1A3A3A35E5E5E61
          61615E5E5EA0A0A0F7F7F7D2D2D29A9A9A929292DEDEDEFFFFFFCCD1D44EADE3
          E4E2D0FFF2DAD2A774A87531988C61919576978D64A57632CA9B64FFFFFFFFFF
          F782C5E862A9D2FFFFFFD0D0D09D9D9DDBDBDBEBEBEB9B9B9B6464647D7D7D88
          88887F7F7F6464648E8E8EFFFFFFFCFCFCB8B8B89D9D9DDCDCDC6CBCE8EDE0C9
          FFE9CBFCF0DEA66B21938E661A1D2417191E171B1F86825FA1691DFFF9F3FFFC
          F3FFFCEDEAEDE8FFFFFFAEAEAED9D9D9E1E1E1EBEBEB5959597F7F7F1F1F1F1A
          1A1A1B1B1B757575565656F8F8F8F9F9F9F7F7F7EAEAEAB0B0B00479BC5C9EBC
          FFEDCBFFF2DEA670298C967A18191F1B1C1E5B5B5EA2A799BB9562F0DFCBFFFA
          EEFFFEE95FA4C7FFFFFF656565909090E3E3E3EDEDED5E5E5E8989891B1B1B1C
          1C1C5C5C5CA0A0A0888888DADADAF6F6F6F6F6F6969696636363CFE5F20072B9
          5A9EBFFFF7DAB270219A8E631C1D2014131699999AD0CBB8DBBA92FFFFF2FFF5
          E55CA2C60073B8FFFFFFE2E2E2616161909090EDEDED5E5E5E7F7F7F1E1E1E15
          1515999999C4C4C4B1B1B1FAFAFAF1F1F1949494616161EEEEEEFFFFFF67AEDC
          0074B91683C28B8568BE7B29AB8C519F8F5FEDD7BAFFE3C6E5DDCE94BFD1137D
          BA0278BDAFD5EDFFFFFFFFFFFFA4A4A46161617171717B7B7B6868687B7B7B7F
          7F7FD0D0D0DDDDDDD9D9D9B5B5B56B6B6B646464CFCFCFFFFFFFFFFFFFFFFFFF
          6CB4E01583CA047CC60077C20077C20076BF0072BB006FB70073BA057AC01986
          CBC3E0F2FFFFFFFFFFFFFFFFFFFFFFFFA9A9A97373736A6A6A65656565656564
          64646262625F5F5F616161676767767676DCDCDCFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFD4E9F662AFE12A92D72B92D72B92D72A91D62A91D63094D692C7EAFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE6E6E6A4A4A484848484848484
          8484838383838383878787C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        NumGlyphs = 2
      end
      object dtePeriodoIniD: TJvDateEdit
        Left = 5
        Top = 32
        Width = 100
        Height = 22
        Color = 16114127
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ShowNullDate = False
        TabOrder = 0
        OnKeyPress = dtePeriodoIniDKeyPress
      end
      object dtePeriodoFimD: TJvDateEdit
        Left = 117
        Top = 32
        Width = 100
        Height = 22
        Color = 16114127
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ShowNullDate = False
        TabOrder = 1
        OnKeyPress = dtePeriodoFimDKeyPress
      end
      object cedValorDeposito: TJvCalcEdit
        Left = 223
        Top = 32
        Width = 100
        Height = 22
        Color = 16114127
        DisplayFormat = ',0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        DecimalPlacesAlwaysShown = False
        OnKeyPress = cedValorDepositoKeyPress
      end
      object edtNumCodDeposito: TEdit
        Left = 329
        Top = 32
        Width = 100
        Height = 22
        Color = 16114127
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        OnKeyPress = edtNumCodDepositoKeyPress
      end
      object rgSituacaoD: TRadioGroup
        Left = 5
        Top = 60
        Width = 286
        Height = 43
        Caption = 'Situa'#231#227'o'
        Columns = 3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemIndex = 1
        Items.Strings = (
          'Todos'
          'Sem V'#237'nculo'
          'Vinculados')
        ParentFont = False
        TabOrder = 4
        OnClick = rgSituacaoDClick
        OnExit = rgSituacaoDExit
      end
      object chbSelecionarTodosD: TCheckBox
        Left = 386
        Top = 86
        Width = 127
        Height = 17
        Caption = 'Marcar Todos'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        OnClick = chbSelecionarTodosDClick
        OnKeyPress = FormKeyPress
      end
      object dbgDepositos: TJvDBUltimGrid
        Left = 6
        Top = 107
        Width = 508
        Height = 223
        DataSource = dsDepositos
        Options = [dgTitles, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 6
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        OnCellClick = dbgDepositosCellClick
        OnDrawColumnCell = dbgDepositosDrawColumnCell
        OnTitleClick = dbgDepositosTitleClick
        MinColumnWidth = 26
        AutoSizeColumnIndex = 0
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 18
        TitleRowHeight = 18
        Columns = <
          item
            Expanded = False
            FieldName = 'SELECIONADO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Alignment = taCenter
            Title.Caption = 'Sel.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 27
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'NUM_COD_DEPOSITO'
            Title.Alignment = taCenter
            Title.Caption = 'ID Dep'#243'sito'
            Width = 173
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCR_DEPOSITO_FLUTUANTE'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Caption = 'Descri'#231#227'o'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 116
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATA_DEPOSITO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Alignment = taCenter
            Title.Caption = 'Dt. Dep'#243'sito'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 88
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VR_DEPOSITO_FLUTUANTE'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Caption = 'Vlr. Dep'#243'sito'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 95
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VINCULOS'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Caption = 'V'#237'nculos'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'ID_DEPOSITO_FLUTUANTE'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_STATUS'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'CAD_ID_USUARIO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_CADASTRO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLUT_ID_USUARIO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'BAIXA_ID_USUARIO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'CANCEL_ID_USUARIO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_FLUTUANTE'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_BAIXADO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_CANCELAMENTO'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -12
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Visible = False
          end>
      end
    end
    object edtDescricaoVinculo: TEdit
      Left = 111
      Top = 432
      Width = 442
      Height = 22
      Color = 16114127
      TabOrder = 5
      OnKeyPress = FormKeyPress
    end
    object dteDataVinculo: TJvDateEdit
      Left = 5
      Top = 432
      Width = 100
      Height = 22
      Color = 16114127
      ShowNullDate = False
      TabOrder = 4
      OnExit = dteDataVinculoExit
      OnKeyPress = dteDataVinculoKeyPress
    end
    object cedValorTotalLancamentos: TJvCalcEdit
      Left = 5
      Top = 388
      Width = 122
      Height = 22
      Color = 16114127
      DisplayFormat = ',0.00'
      Enabled = False
      TabOrder = 2
      DecimalPlacesAlwaysShown = False
      OnKeyPress = FormKeyPress
    end
    object cedValorTotalDepositos: TJvCalcEdit
      Left = 133
      Top = 388
      Width = 122
      Height = 22
      Color = 16114127
      DisplayFormat = ',0.00'
      Enabled = False
      TabOrder = 3
      DecimalPlacesAlwaysShown = False
      OnKeyPress = FormKeyPress
    end
    object mmObservacoes: TMemo
      Left = 559
      Top = 388
      Width = 425
      Height = 66
      Color = 16114127
      TabOrder = 6
      OnKeyPress = FormKeyPress
    end
  end
  object dsLanc: TDataSource
    DataSet = dmBaixa.cdsLanc
    Left = 504
    Top = 282
  end
  object dsDepositos: TDataSource
    DataSet = dmBaixa.cdsDepos
    Left = 1010
    Top = 283
  end
end
