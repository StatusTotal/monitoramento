object dmBaixa: TdmBaixa
  OldCreateOrder = False
  Height = 247
  Width = 489
  object qryBaixaLancI: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT LP.COD_LANCAMENTO_FK,'
      '       LP.ANO_LANCAMENTO_FK,'
      '       LP.ID_LANCAMENTO_PARC,'
      '       LP.NUM_PARCELA,'
      '       LP.DATA_LANCAMENTO_PARC,'
      '       LP.VR_PARCELA_PREV,'
      '       LP.VR_PARCELA_JUROS,'
      '       LP.VR_PARCELA_DESCONTO,'
      '       LP.VR_PARCELA_PAGO,'
      '       LP.ID_FORMAPAGAMENTO_FK,'
      '       LP.DATA_PAGAMENTO,'
      '       LP.FLG_STATUS,'
      '       LP.PAG_ID_USUARIO'
      '  FROM LANCAMENTO_PARC LP'
      ' WHERE LP.ID_LANCAMENTO_PARC = :ID_LANCAMENTO_PARC')
    Left = 40
    Top = 24
    ParamData = <
      item
        Position = 1
        Name = 'ID_LANCAMENTO_PARC'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspBaixaLancI: TDataSetProvider
    DataSet = qryBaixaLancI
    Left = 40
    Top = 72
  end
  object cdsBaixaLancI: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_LANCAMENTO_PARC'
        ParamType = ptInput
      end>
    ProviderName = 'dspBaixaLancI'
    OnCalcFields = cdsBaixaLancICalcFields
    Left = 40
    Top = 120
    object cdsBaixaLancICOD_LANCAMENTO_FK: TIntegerField
      FieldName = 'COD_LANCAMENTO_FK'
    end
    object cdsBaixaLancIANO_LANCAMENTO_FK: TIntegerField
      FieldName = 'ANO_LANCAMENTO_FK'
    end
    object cdsBaixaLancIID_LANCAMENTO_PARC: TIntegerField
      FieldName = 'ID_LANCAMENTO_PARC'
      Required = True
    end
    object cdsBaixaLancINUM_PARCELA: TIntegerField
      FieldName = 'NUM_PARCELA'
    end
    object cdsBaixaLancIDATA_LANCAMENTO_PARC: TDateField
      FieldName = 'DATA_LANCAMENTO_PARC'
    end
    object cdsBaixaLancIVR_PARCELA_PREV: TBCDField
      FieldName = 'VR_PARCELA_PREV'
      OnGetText = cdsBaixaLancIVR_PARCELA_PREVGetText
      Precision = 18
      Size = 2
    end
    object cdsBaixaLancIVR_PARCELA_JUROS: TBCDField
      FieldName = 'VR_PARCELA_JUROS'
      OnGetText = cdsBaixaLancIVR_PARCELA_JUROSGetText
      Precision = 18
      Size = 2
    end
    object cdsBaixaLancIVR_PARCELA_DESCONTO: TBCDField
      FieldName = 'VR_PARCELA_DESCONTO'
      OnGetText = cdsBaixaLancIVR_PARCELA_DESCONTOGetText
      Precision = 18
      Size = 2
    end
    object cdsBaixaLancIVR_PARCELA_PAGO: TBCDField
      FieldName = 'VR_PARCELA_PAGO'
      OnGetText = cdsBaixaLancIVR_PARCELA_PAGOGetText
      Precision = 18
      Size = 2
    end
    object cdsBaixaLancIID_FORMAPAGAMENTO_FK: TIntegerField
      FieldName = 'ID_FORMAPAGAMENTO_FK'
    end
    object cdsBaixaLancIDATA_PAGAMENTO: TDateField
      FieldName = 'DATA_PAGAMENTO'
    end
    object cdsBaixaLancIFLG_STATUS: TStringField
      FieldName = 'FLG_STATUS'
      FixedChar = True
      Size = 1
    end
    object cdsBaixaLancIPAG_ID_USUARIO: TIntegerField
      FieldName = 'PAG_ID_USUARIO'
    end
    object cdsBaixaLancIDESCR_BAIXA_LANC_DERIVADO: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCR_BAIXA_LANC_DERIVADO'
      Size = 250
    end
    object cdsBaixaLancIOBS_BAIXA_LANC_DERIVADO: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'OBS_BAIXA_LANC_DERIVADO'
      Size = 1000
    end
    object cdsBaixaLancIID_SISTEMA_FK: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'ID_SISTEMA_FK'
    end
    object cdsBaixaLancINUM_RECIBO: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'NUM_RECIBO'
    end
    object cdsBaixaLancIVR_DIFERENCA: TCurrencyField
      FieldKind = fkInternalCalc
      FieldName = 'VR_DIFERENCA'
    end
    object cdsBaixaLancITP_DIFERENCA: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'TP_DIFERENCA'
      Size = 1
    end
  end
  object dsBaixaLancI: TDataSource
    DataSet = cdsBaixaLancI
    Left = 40
    Top = 168
  end
  object qryBaixaLancG: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT L.*,'
      '       LP.*,'
      '       LP.FLG_STATUS AS FLG_STATUS_PARC,'
      '       (CASE WHEN (BLD.DERIV_COD_LANCAMENTO_FK IS NOT NULL)'
      '             THEN '#39'S'#39
      '             ELSE '#39'N'#39
      '        END) AS BAIXADO'
      '  FROM LANCAMENTO L'
      ' INNER JOIN LANCAMENTO_PARC LP'
      '    ON L.COD_LANCAMENTO = LP.COD_LANCAMENTO_FK'
      '   AND L.ANO_LANCAMENTO = LP.ANO_LANCAMENTO_FK'
      '  LEFT JOIN BAIXA_LANC_DERIVADO BLD'
      '    ON L.COD_LANCAMENTO = BLD.ORIG_COD_LANCAMENTO_FK'
      '   AND L.ANO_LANCAMENTO = BLD.ORIG_ANO_LANCAMENTO_FK')
    Left = 128
    Top = 24
    ParamData = <
      item
        Position = 1
        Name = 'DATA_INI'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'DATA_FIM'
        DataType = ftDate
        ParamType = ptInput
      end>
  end
  object dspBaixaLancG: TDataSetProvider
    DataSet = qryBaixaLancG
    Left = 128
    Top = 72
  end
  object cdsBaixaLancG: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftDate
        Name = 'DATA_INI'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'DATA_FIM'
        ParamType = ptInput
      end>
    ProviderName = 'dspBaixaLancG'
    AfterPost = cdsBaixaLancGAfterPost
    Left = 128
    Top = 120
    object cdsBaixaLancGSELECIONADO: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'SELECIONADO'
    end
    object cdsBaixaLancGCOD_LANCAMENTO: TIntegerField
      FieldName = 'COD_LANCAMENTO'
      Required = True
    end
    object cdsBaixaLancGANO_LANCAMENTO: TIntegerField
      FieldName = 'ANO_LANCAMENTO'
      Required = True
    end
    object cdsBaixaLancGDATA_LANCAMENTO: TDateField
      FieldName = 'DATA_LANCAMENTO'
    end
    object cdsBaixaLancGTIPO_LANCAMENTO: TStringField
      FieldName = 'TIPO_LANCAMENTO'
      FixedChar = True
      Size = 1
    end
    object cdsBaixaLancGTIPO_CADASTRO: TStringField
      FieldName = 'TIPO_CADASTRO'
      FixedChar = True
      Size = 1
    end
    object cdsBaixaLancGID_CATEGORIA_DESPREC_FK: TIntegerField
      FieldName = 'ID_CATEGORIA_DESPREC_FK'
    end
    object cdsBaixaLancGID_SUBCATEGORIA_DESPREC_FK: TIntegerField
      FieldName = 'ID_SUBCATEGORIA_DESPREC_FK'
    end
    object cdsBaixaLancGID_NATUREZA_FK: TIntegerField
      FieldName = 'ID_NATUREZA_FK'
    end
    object cdsBaixaLancGID_CLIENTE_FORNECEDOR_FK: TIntegerField
      FieldName = 'ID_CLIENTE_FORNECEDOR_FK'
    end
    object cdsBaixaLancGFLG_IMPOSTORENDA: TStringField
      FieldName = 'FLG_IMPOSTORENDA'
      FixedChar = True
      Size = 1
    end
    object cdsBaixaLancGFLG_PESSOAL: TStringField
      FieldName = 'FLG_PESSOAL'
      FixedChar = True
      Size = 1
    end
    object cdsBaixaLancGFLG_AUXILIAR: TStringField
      FieldName = 'FLG_AUXILIAR'
      FixedChar = True
      Size = 1
    end
    object cdsBaixaLancGFLG_REAL: TStringField
      FieldName = 'FLG_REAL'
      FixedChar = True
      Size = 1
    end
    object cdsBaixaLancGFLG_FLUTUANTE: TStringField
      FieldName = 'FLG_FLUTUANTE'
      FixedChar = True
      Size = 1
    end
    object cdsBaixaLancGQTD_PARCELAS: TIntegerField
      FieldName = 'QTD_PARCELAS'
    end
    object cdsBaixaLancGVR_TOTAL_PREV: TBCDField
      FieldName = 'VR_TOTAL_PREV'
      OnGetText = cdsBaixaLancGVR_TOTAL_PREVGetText
      Precision = 18
      Size = 2
    end
    object cdsBaixaLancGVR_TOTAL_JUROS: TBCDField
      FieldName = 'VR_TOTAL_JUROS'
      OnGetText = cdsBaixaLancGVR_TOTAL_JUROSGetText
      Precision = 18
      Size = 2
    end
    object cdsBaixaLancGVR_TOTAL_DESCONTO: TBCDField
      FieldName = 'VR_TOTAL_DESCONTO'
      OnGetText = cdsBaixaLancGVR_TOTAL_DESCONTOGetText
      Precision = 18
      Size = 2
    end
    object cdsBaixaLancGVR_TOTAL_PAGO: TBCDField
      FieldName = 'VR_TOTAL_PAGO'
      OnGetText = cdsBaixaLancGVR_TOTAL_PAGOGetText
      Precision = 18
      Size = 2
    end
    object cdsBaixaLancGCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsBaixaLancGDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsBaixaLancGFLG_STATUS: TStringField
      FieldName = 'FLG_STATUS'
      FixedChar = True
      Size = 1
    end
    object cdsBaixaLancGFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsBaixaLancGDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsBaixaLancGCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsBaixaLancGFLG_RECORRENTE: TStringField
      FieldName = 'FLG_RECORRENTE'
      FixedChar = True
      Size = 1
    end
    object cdsBaixaLancGFLG_REPLICADO: TStringField
      FieldName = 'FLG_REPLICADO'
      FixedChar = True
      Size = 1
    end
    object cdsBaixaLancGNUM_RECIBO: TIntegerField
      FieldName = 'NUM_RECIBO'
    end
    object cdsBaixaLancGID_ORIGEM: TIntegerField
      FieldName = 'ID_ORIGEM'
    end
    object cdsBaixaLancGID_SISTEMA_FK: TIntegerField
      FieldName = 'ID_SISTEMA_FK'
    end
    object cdsBaixaLancGOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 3000
    end
    object cdsBaixaLancGID_CARNELEAO_EQUIVALENCIA_FK: TIntegerField
      FieldName = 'ID_CARNELEAO_EQUIVALENCIA_FK'
    end
    object cdsBaixaLancGID_LANCAMENTO_PARC: TIntegerField
      FieldName = 'ID_LANCAMENTO_PARC'
      ReadOnly = True
    end
    object cdsBaixaLancGCOD_LANCAMENTO_FK: TIntegerField
      FieldName = 'COD_LANCAMENTO_FK'
      ReadOnly = True
    end
    object cdsBaixaLancGANO_LANCAMENTO_FK: TIntegerField
      FieldName = 'ANO_LANCAMENTO_FK'
      ReadOnly = True
    end
    object cdsBaixaLancGDATA_LANCAMENTO_PARC: TDateField
      FieldName = 'DATA_LANCAMENTO_PARC'
      ReadOnly = True
    end
    object cdsBaixaLancGNUM_PARCELA: TIntegerField
      FieldName = 'NUM_PARCELA'
      ReadOnly = True
    end
    object cdsBaixaLancGDATA_VENCIMENTO: TDateField
      FieldName = 'DATA_VENCIMENTO'
      ReadOnly = True
    end
    object cdsBaixaLancGVR_PARCELA_PREV: TBCDField
      FieldName = 'VR_PARCELA_PREV'
      ReadOnly = True
      OnGetText = cdsBaixaLancGVR_PARCELA_PREVGetText
      Precision = 18
      Size = 2
    end
    object cdsBaixaLancGVR_PARCELA_JUROS: TBCDField
      FieldName = 'VR_PARCELA_JUROS'
      ReadOnly = True
      OnGetText = cdsBaixaLancGVR_PARCELA_JUROSGetText
      Precision = 18
      Size = 2
    end
    object cdsBaixaLancGVR_PARCELA_DESCONTO: TBCDField
      FieldName = 'VR_PARCELA_DESCONTO'
      ReadOnly = True
      OnGetText = cdsBaixaLancGVR_PARCELA_DESCONTOGetText
      Precision = 18
      Size = 2
    end
    object cdsBaixaLancGVR_PARCELA_PAGO: TBCDField
      FieldName = 'VR_PARCELA_PAGO'
      ReadOnly = True
      OnGetText = cdsBaixaLancGVR_PARCELA_PAGOGetText
      Precision = 18
      Size = 2
    end
    object cdsBaixaLancGID_FORMAPAGAMENTO_FK: TIntegerField
      FieldName = 'ID_FORMAPAGAMENTO_FK'
      ReadOnly = True
    end
    object cdsBaixaLancGAGENCIA: TStringField
      FieldName = 'AGENCIA'
      ReadOnly = True
      Size = 10
    end
    object cdsBaixaLancGCONTA: TStringField
      FieldName = 'CONTA'
      ReadOnly = True
      Size = 10
    end
    object cdsBaixaLancGID_BANCO_FK: TIntegerField
      FieldName = 'ID_BANCO_FK'
      ReadOnly = True
    end
    object cdsBaixaLancGNUM_CHEQUE: TStringField
      FieldName = 'NUM_CHEQUE'
      ReadOnly = True
      Size = 10
    end
    object cdsBaixaLancGNUM_COD_DEPOSITO: TStringField
      FieldName = 'NUM_COD_DEPOSITO'
      ReadOnly = True
      Size = 600
    end
    object cdsBaixaLancGFLG_STATUS_1: TStringField
      FieldName = 'FLG_STATUS_1'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object cdsBaixaLancGDATA_CANCELAMENTO_1: TDateField
      FieldName = 'DATA_CANCELAMENTO_1'
      ReadOnly = True
    end
    object cdsBaixaLancGCANCEL_ID_USUARIO_1: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO_1'
      ReadOnly = True
    end
    object cdsBaixaLancGDATA_PAGAMENTO: TDateField
      FieldName = 'DATA_PAGAMENTO'
      ReadOnly = True
    end
    object cdsBaixaLancGPAG_ID_USUARIO: TIntegerField
      FieldName = 'PAG_ID_USUARIO'
      ReadOnly = True
    end
    object cdsBaixaLancGOBS_LANCAMENTO_PARC: TStringField
      FieldName = 'OBS_LANCAMENTO_PARC'
      ReadOnly = True
      Size = 1000
    end
    object cdsBaixaLancGCAD_ID_USUARIO_1: TIntegerField
      FieldName = 'CAD_ID_USUARIO_1'
      ReadOnly = True
    end
    object cdsBaixaLancGDATA_CADASTRO_1: TDateField
      FieldName = 'DATA_CADASTRO_1'
      ReadOnly = True
    end
    object cdsBaixaLancGFLG_STATUS_PARC: TStringField
      FieldName = 'FLG_STATUS_PARC'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object cdsBaixaLancGFLG_FORAFECHCAIXA: TStringField
      FieldName = 'FLG_FORAFECHCAIXA'
      FixedChar = True
      Size = 1
    end
    object cdsBaixaLancGDESCRICAO: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCRICAO'
      Size = 1000
    end
    object cdsBaixaLancGID_FORMAPAGAMENTO_ORIG_FK: TIntegerField
      FieldName = 'ID_FORMAPAGAMENTO_ORIG_FK'
      ReadOnly = True
    end
    object cdsBaixaLancGBAIXADO: TStringField
      FieldName = 'BAIXADO'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
  end
  object dsBaixaLancG: TDataSource
    DataSet = cdsBaixaLancG
    Left = 128
    Top = 168
  end
  object qryDespPagas: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT LP.COD_LANCAMENTO_FK,'
      '       LP.ANO_LANCAMENTO_FK,'
      '       LP.ID_LANCAMENTO_PARC,'
      '       LP.DATA_LANCAMENTO_PARC,'
      '       LP.VR_PARCELA_PREV,'
      '       LP.VR_PARCELA_PAGO,'
      '       LP.DATA_PAGAMENTO,'
      '       L.TIPO_LANCAMENTO,'
      '       L.ID_NATUREZA_FK,'
      '       N.DESCR_NATUREZA,'
      '       L.ID_CATEGORIA_DESPREC_FK,'
      '       C.DESCR_CATEGORIA_DESPREC,'
      '       L.ID_SUBCATEGORIA_DESPREC_FK,'
      '       S.DESCR_SUBCATEGORIA_DESPREC,'
      '       L.OBSERVACAO,'
      '       (CASE WHEN (BLD.DERIV_COD_LANCAMENTO_FK IS NULL)'
      '             THEN '#39'N'#39
      '             ELSE '#39'S'#39
      '        END) AS BAIXADO'
      '  FROM LANCAMENTO L'
      '  LEFT JOIN NATUREZA N'
      '    ON L.ID_NATUREZA_FK = N.ID_NATUREZA'
      '  LEFT JOIN CATEGORIA_DESPREC C'
      '    ON L.ID_CATEGORIA_DESPREC_FK = C.ID_CATEGORIA_DESPREC'
      '  LEFT JOIN SUBCATEGORIA_DESPREC S'
      '    ON L.ID_SUBCATEGORIA_DESPREC_FK = S.ID_SUBCATEGORIA_DESPREC'
      ' INNER JOIN LANCAMENTO_PARC LP'
      '    ON L.COD_LANCAMENTO = LP.COD_LANCAMENTO_FK'
      '   AND L.ANO_LANCAMENTO = LP.ANO_LANCAMENTO_FK'
      '  LEFT JOIN BAIXA_LANC_DERIVADO BLD'
      '    ON L.COD_LANCAMENTO = BLD.DERIV_COD_LANCAMENTO_FK'
      '   AND L.ANO_LANCAMENTO = BLD.DERIV_ANO_LANCAMENTO_FK'
      ' WHERE L.TIPO_LANCAMENTO = '#39'D'#39
      '   AND LP.FLG_STATUS = '#39'G'#39
      '   AND L.NUM_RECIBO IS NULL'
      'ORDER BY LP.DATA_LANCAMENTO_PARC,'
      '         L.COD_LANCAMENTO,'
      '         L.ANO_LANCAMENTO')
    Left = 212
    Top = 23
    ParamData = <
      item
        Position = 1
        Name = 'DATA_INI'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'DATA_FIM'
        DataType = ftDate
        ParamType = ptInput
      end>
  end
  object dspDespPagas: TDataSetProvider
    DataSet = qryDespPagas
    Left = 212
    Top = 71
  end
  object cdsDespPagas: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftDate
        Name = 'DATA_INI'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'DATA_FIM'
        ParamType = ptInput
      end>
    ProviderName = 'dspDespPagas'
    Left = 212
    Top = 119
    object cdsDespPagasCOD_LANCAMENTO_FK: TIntegerField
      FieldName = 'COD_LANCAMENTO_FK'
    end
    object cdsDespPagasANO_LANCAMENTO_FK: TIntegerField
      FieldName = 'ANO_LANCAMENTO_FK'
    end
    object cdsDespPagasID_LANCAMENTO_PARC: TIntegerField
      FieldName = 'ID_LANCAMENTO_PARC'
      Required = True
    end
    object cdsDespPagasDATA_LANCAMENTO_PARC: TDateField
      FieldName = 'DATA_LANCAMENTO_PARC'
    end
    object cdsDespPagasVR_PARCELA_PREV: TBCDField
      FieldName = 'VR_PARCELA_PREV'
      OnGetText = cdsDespPagasVR_PARCELA_PREVGetText
      Precision = 18
      Size = 2
    end
    object cdsDespPagasVR_PARCELA_PAGO: TBCDField
      FieldName = 'VR_PARCELA_PAGO'
      OnGetText = cdsDespPagasVR_PARCELA_PAGOGetText
      Precision = 18
      Size = 2
    end
    object cdsDespPagasDATA_PAGAMENTO: TDateField
      FieldName = 'DATA_PAGAMENTO'
    end
    object cdsDespPagasTIPO_LANCAMENTO: TStringField
      FieldName = 'TIPO_LANCAMENTO'
      FixedChar = True
      Size = 1
    end
    object cdsDespPagasID_NATUREZA_FK: TIntegerField
      FieldName = 'ID_NATUREZA_FK'
    end
    object cdsDespPagasDESCR_NATUREZA: TStringField
      FieldName = 'DESCR_NATUREZA'
      Size = 250
    end
    object cdsDespPagasID_CATEGORIA_DESPREC_FK: TIntegerField
      FieldName = 'ID_CATEGORIA_DESPREC_FK'
    end
    object cdsDespPagasDESCR_CATEGORIA_DESPREC: TStringField
      FieldName = 'DESCR_CATEGORIA_DESPREC'
      Size = 250
    end
    object cdsDespPagasID_SUBCATEGORIA_DESPREC_FK: TIntegerField
      FieldName = 'ID_SUBCATEGORIA_DESPREC_FK'
    end
    object cdsDespPagasDESCR_SUBCATEGORIA_DESPREC: TStringField
      FieldName = 'DESCR_SUBCATEGORIA_DESPREC'
      Size = 250
    end
    object cdsDespPagasOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 3000
    end
    object cdsDespPagasBAIXADO: TStringField
      FieldName = 'BAIXADO'
      Required = True
      FixedChar = True
      Size = 1
    end
  end
  object dsDespPagas: TDataSource
    DataSet = cdsDespPagas
    Left = 212
    Top = 167
  end
  object qryDepos: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM DEPOSITO_FLUTUANTE'
      ' WHERE DATA_DEPOSITO BETWEEN :DATA_INI AND :DATA_FIM'
      'ORDER BY DATA_DEPOSITO DESC')
    Left = 424
    Top = 24
    ParamData = <
      item
        Position = 1
        Name = 'DATA_INI'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'DATA_FIM'
        DataType = ftDate
        ParamType = ptInput
      end>
  end
  object dspDepos: TDataSetProvider
    DataSet = qryDepos
    Left = 424
    Top = 72
  end
  object cdsDepos: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftDate
        Name = 'DATA_INI'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'DATA_FIM'
        ParamType = ptInput
      end>
    ProviderName = 'dspDepos'
    AfterPost = cdsDeposAfterPost
    Left = 424
    Top = 120
    object cdsDeposSELECIONADO: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'SELECIONADO'
    end
    object cdsDeposID_DEPOSITO_FLUTUANTE: TIntegerField
      FieldName = 'ID_DEPOSITO_FLUTUANTE'
      Required = True
    end
    object cdsDeposNUM_COD_DEPOSITO: TStringField
      FieldName = 'NUM_COD_DEPOSITO'
      Size = 25
    end
    object cdsDeposDESCR_DEPOSITO_FLUTUANTE: TStringField
      FieldName = 'DESCR_DEPOSITO_FLUTUANTE'
      Size = 250
    end
    object cdsDeposDATA_DEPOSITO: TDateField
      FieldName = 'DATA_DEPOSITO'
    end
    object cdsDeposVR_DEPOSITO_FLUTUANTE: TBCDField
      FieldName = 'VR_DEPOSITO_FLUTUANTE'
      OnGetText = cdsDeposVR_DEPOSITO_FLUTUANTEGetText
      Precision = 18
      Size = 2
    end
    object cdsDeposFLG_STATUS: TStringField
      FieldName = 'FLG_STATUS'
      FixedChar = True
      Size = 1
    end
    object cdsDeposCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsDeposDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsDeposFLUT_ID_USUARIO: TIntegerField
      FieldName = 'FLUT_ID_USUARIO'
    end
    object cdsDeposBAIXA_ID_USUARIO: TIntegerField
      FieldName = 'BAIXA_ID_USUARIO'
    end
    object cdsDeposCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsDeposDATA_FLUTUANTE: TDateField
      FieldName = 'DATA_FLUTUANTE'
    end
    object cdsDeposDATA_BAIXADO: TDateField
      FieldName = 'DATA_BAIXADO'
    end
    object cdsDeposDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsDeposVINCULOS: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'VINCULOS'
      Size = 1000
    end
  end
  object dsDepos: TDataSource
    DataSet = cdsDepos
    Left = 424
    Top = 168
  end
  object qryLanc: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT L.*,'
      '       N.DESCR_NATUREZA,'
      '       LP.*,'
      '       LP.FLG_STATUS AS FLG_STATUS_PARC'
      '  FROM LANCAMENTO L'
      ' INNER JOIN NATUREZA N'
      '    ON L.ID_NATUREZA_FK = N.ID_NATUREZA'
      ' INNER JOIN LANCAMENTO_PARC LP'
      '    ON L.COD_LANCAMENTO = LP.COD_LANCAMENTO_FK'
      '   AND L.ANO_LANCAMENTO = LP.ANO_LANCAMENTO_FK'
      ' WHERE LP.DATA_LANCAMENTO_PARC BETWEEN :DATA_INI AND :DATA_FIM'
      '   AND LP.ID_FORMAPAGAMENTO_FK = 5')
    Left = 360
    Top = 24
    ParamData = <
      item
        Position = 1
        Name = 'DATA_INI'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'DATA_FIM'
        DataType = ftDate
        ParamType = ptInput
      end>
  end
  object dspLanc: TDataSetProvider
    DataSet = qryLanc
    Left = 360
    Top = 72
  end
  object cdsLanc: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftUnknown
        Name = 'DATA_INI'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'DATA_FIM'
        ParamType = ptInput
      end>
    ProviderName = 'dspLanc'
    AfterPost = cdsLancAfterPost
    Left = 360
    Top = 120
    object cdsLancSELECIONADO: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'SELECIONADO'
    end
    object cdsLancCOD_LANCAMENTO: TIntegerField
      FieldName = 'COD_LANCAMENTO'
      Origin = 'COD_LANCAMENTO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsLancANO_LANCAMENTO: TIntegerField
      FieldName = 'ANO_LANCAMENTO'
      Origin = 'ANO_LANCAMENTO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsLancDATA_LANCAMENTO: TDateField
      FieldName = 'DATA_LANCAMENTO'
      Origin = 'DATA_LANCAMENTO'
    end
    object cdsLancTIPO_LANCAMENTO: TStringField
      FieldName = 'TIPO_LANCAMENTO'
      Origin = 'TIPO_LANCAMENTO'
      FixedChar = True
      Size = 1
    end
    object cdsLancTIPO_CADASTRO: TStringField
      FieldName = 'TIPO_CADASTRO'
      Origin = 'TIPO_CADASTRO'
      FixedChar = True
      Size = 1
    end
    object cdsLancID_CATEGORIA_DESPREC_FK: TIntegerField
      FieldName = 'ID_CATEGORIA_DESPREC_FK'
      Origin = 'ID_CATEGORIA_DESPREC_FK'
    end
    object cdsLancID_SUBCATEGORIA_DESPREC_FK: TIntegerField
      FieldName = 'ID_SUBCATEGORIA_DESPREC_FK'
      Origin = 'ID_SUBCATEGORIA_DESPREC_FK'
    end
    object cdsLancID_NATUREZA_FK: TIntegerField
      FieldName = 'ID_NATUREZA_FK'
      Origin = 'ID_NATUREZA_FK'
    end
    object cdsLancID_CLIENTE_FORNECEDOR_FK: TIntegerField
      FieldName = 'ID_CLIENTE_FORNECEDOR_FK'
      Origin = 'ID_CLIENTE_FORNECEDOR_FK'
    end
    object cdsLancFLG_IMPOSTORENDA: TStringField
      FieldName = 'FLG_IMPOSTORENDA'
      Origin = 'FLG_IMPOSTORENDA'
      FixedChar = True
      Size = 1
    end
    object cdsLancFLG_PESSOAL: TStringField
      FieldName = 'FLG_PESSOAL'
      Origin = 'FLG_PESSOAL'
      FixedChar = True
      Size = 1
    end
    object cdsLancFLG_AUXILIAR: TStringField
      FieldName = 'FLG_AUXILIAR'
      Origin = 'FLG_AUXILIAR'
      FixedChar = True
      Size = 1
    end
    object cdsLancFLG_REAL: TStringField
      FieldName = 'FLG_REAL'
      Origin = 'FLG_REAL'
      FixedChar = True
      Size = 1
    end
    object cdsLancFLG_FLUTUANTE: TStringField
      FieldName = 'FLG_FLUTUANTE'
      Origin = 'FLG_FLUTUANTE'
      FixedChar = True
      Size = 1
    end
    object cdsLancQTD_PARCELAS: TIntegerField
      FieldName = 'QTD_PARCELAS'
      Origin = 'QTD_PARCELAS'
    end
    object cdsLancVR_TOTAL_PREV: TBCDField
      FieldName = 'VR_TOTAL_PREV'
      Origin = 'VR_TOTAL_PREV'
      Precision = 18
      Size = 2
    end
    object cdsLancVR_TOTAL_JUROS: TBCDField
      FieldName = 'VR_TOTAL_JUROS'
      Origin = 'VR_TOTAL_JUROS'
      Precision = 18
      Size = 2
    end
    object cdsLancVR_TOTAL_DESCONTO: TBCDField
      FieldName = 'VR_TOTAL_DESCONTO'
      Origin = 'VR_TOTAL_DESCONTO'
      Precision = 18
      Size = 2
    end
    object cdsLancVR_TOTAL_PAGO: TBCDField
      FieldName = 'VR_TOTAL_PAGO'
      Origin = 'VR_TOTAL_PAGO'
      Precision = 18
      Size = 2
    end
    object cdsLancCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
      Origin = 'CAD_ID_USUARIO'
    end
    object cdsLancDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
      Origin = 'DATA_CADASTRO'
    end
    object cdsLancFLG_STATUS: TStringField
      FieldName = 'FLG_STATUS'
      Origin = 'FLG_STATUS'
      FixedChar = True
      Size = 1
    end
    object cdsLancFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      Origin = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsLancDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
      Origin = 'DATA_CANCELAMENTO'
    end
    object cdsLancCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
      Origin = 'CANCEL_ID_USUARIO'
    end
    object cdsLancFLG_RECORRENTE: TStringField
      FieldName = 'FLG_RECORRENTE'
      Origin = 'FLG_RECORRENTE'
      FixedChar = True
      Size = 1
    end
    object cdsLancFLG_REPLICADO: TStringField
      FieldName = 'FLG_REPLICADO'
      Origin = 'FLG_REPLICADO'
      FixedChar = True
      Size = 1
    end
    object cdsLancNUM_RECIBO: TIntegerField
      FieldName = 'NUM_RECIBO'
      Origin = 'NUM_RECIBO'
    end
    object cdsLancID_ORIGEM: TIntegerField
      FieldName = 'ID_ORIGEM'
      Origin = 'ID_ORIGEM'
    end
    object cdsLancID_SISTEMA_FK: TIntegerField
      FieldName = 'ID_SISTEMA_FK'
      Origin = 'ID_SISTEMA_FK'
    end
    object cdsLancOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      Size = 3000
    end
    object cdsLancID_CARNELEAO_EQUIVALENCIA_FK: TIntegerField
      FieldName = 'ID_CARNELEAO_EQUIVALENCIA_FK'
      Origin = 'ID_CARNELEAO_EQUIVALENCIA_FK'
    end
    object cdsLancFLG_FORAFECHCAIXA: TStringField
      FieldName = 'FLG_FORAFECHCAIXA'
      Origin = 'FLG_FORAFECHCAIXA'
      FixedChar = True
      Size = 1
    end
    object cdsLancID_LANCAMENTO_PARC: TIntegerField
      FieldName = 'ID_LANCAMENTO_PARC'
      Origin = 'ID_LANCAMENTO_PARC'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsLancCOD_LANCAMENTO_FK: TIntegerField
      FieldName = 'COD_LANCAMENTO_FK'
      Origin = 'COD_LANCAMENTO_FK'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsLancANO_LANCAMENTO_FK: TIntegerField
      FieldName = 'ANO_LANCAMENTO_FK'
      Origin = 'ANO_LANCAMENTO_FK'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsLancDATA_LANCAMENTO_PARC: TDateField
      FieldName = 'DATA_LANCAMENTO_PARC'
      Origin = 'DATA_LANCAMENTO_PARC'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsLancNUM_PARCELA: TIntegerField
      FieldName = 'NUM_PARCELA'
      Origin = 'NUM_PARCELA'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsLancDATA_VENCIMENTO: TDateField
      FieldName = 'DATA_VENCIMENTO'
      Origin = 'DATA_VENCIMENTO'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsLancVR_PARCELA_PREV: TBCDField
      FieldName = 'VR_PARCELA_PREV'
      Origin = 'VR_PARCELA_PREV'
      ProviderFlags = []
      ReadOnly = True
      OnGetText = cdsLancVR_PARCELA_PREVGetText
      Precision = 18
      Size = 2
    end
    object cdsLancVR_PARCELA_JUROS: TBCDField
      FieldName = 'VR_PARCELA_JUROS'
      Origin = 'VR_PARCELA_JUROS'
      ProviderFlags = []
      ReadOnly = True
      Precision = 18
      Size = 2
    end
    object cdsLancVR_PARCELA_DESCONTO: TBCDField
      FieldName = 'VR_PARCELA_DESCONTO'
      Origin = 'VR_PARCELA_DESCONTO'
      ProviderFlags = []
      ReadOnly = True
      Precision = 18
      Size = 2
    end
    object cdsLancVR_PARCELA_PAGO: TBCDField
      FieldName = 'VR_PARCELA_PAGO'
      Origin = 'VR_PARCELA_PAGO'
      ProviderFlags = []
      ReadOnly = True
      OnGetText = cdsLancVR_PARCELA_PAGOGetText
      Precision = 18
      Size = 2
    end
    object cdsLancID_FORMAPAGAMENTO_FK: TIntegerField
      FieldName = 'ID_FORMAPAGAMENTO_FK'
      Origin = 'ID_FORMAPAGAMENTO_FK'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsLancAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Origin = 'AGENCIA'
      ProviderFlags = []
      ReadOnly = True
      Size = 10
    end
    object cdsLancCONTA: TStringField
      FieldName = 'CONTA'
      Origin = 'CONTA'
      ProviderFlags = []
      ReadOnly = True
      Size = 10
    end
    object cdsLancID_BANCO_FK: TIntegerField
      FieldName = 'ID_BANCO_FK'
      Origin = 'ID_BANCO_FK'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsLancNUM_CHEQUE: TStringField
      FieldName = 'NUM_CHEQUE'
      Origin = 'NUM_CHEQUE'
      ProviderFlags = []
      ReadOnly = True
      Size = 10
    end
    object cdsLancNUM_COD_DEPOSITO: TStringField
      FieldName = 'NUM_COD_DEPOSITO'
      Origin = 'NUM_COD_DEPOSITO'
      ProviderFlags = []
      ReadOnly = True
      Size = 600
    end
    object cdsLancFLG_STATUS_1: TStringField
      FieldName = 'FLG_STATUS_1'
      Origin = 'FLG_STATUS'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object cdsLancDATA_CANCELAMENTO_1: TDateField
      FieldName = 'DATA_CANCELAMENTO_1'
      Origin = 'DATA_CANCELAMENTO'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsLancCANCEL_ID_USUARIO_1: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO_1'
      Origin = 'CANCEL_ID_USUARIO'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsLancDATA_PAGAMENTO: TDateField
      FieldName = 'DATA_PAGAMENTO'
      Origin = 'DATA_PAGAMENTO'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsLancPAG_ID_USUARIO: TIntegerField
      FieldName = 'PAG_ID_USUARIO'
      Origin = 'PAG_ID_USUARIO'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsLancOBS_LANCAMENTO_PARC: TStringField
      FieldName = 'OBS_LANCAMENTO_PARC'
      Origin = 'OBS_LANCAMENTO_PARC'
      ProviderFlags = []
      ReadOnly = True
      Size = 1000
    end
    object cdsLancCAD_ID_USUARIO_1: TIntegerField
      FieldName = 'CAD_ID_USUARIO_1'
      Origin = 'CAD_ID_USUARIO'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsLancDATA_CADASTRO_1: TDateField
      FieldName = 'DATA_CADASTRO_1'
      Origin = 'DATA_CADASTRO'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsLancID_FORMAPAGAMENTO_ORIG_FK: TIntegerField
      FieldName = 'ID_FORMAPAGAMENTO_ORIG_FK'
      Origin = 'ID_FORMAPAGAMENTO_ORIG_FK'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsLancFLG_STATUS_PARC: TStringField
      FieldName = 'FLG_STATUS_PARC'
      Origin = 'FLG_STATUS'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object cdsLancVINCULADO: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'VINCULADO'
      Size = 1
    end
    object cdsLancDESCRICAO: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCRICAO'
      Size = 1000
    end
    object cdsLancDESCR_NATUREZA: TStringField
      FieldName = 'DESCR_NATUREZA'
      Origin = 'DESCR_NATUREZA'
      ProviderFlags = []
      ReadOnly = True
      Size = 250
    end
    object cdsLancVINCULOS: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'VINCULOS'
      Size = 1000
    end
  end
  object dsLanc: TDataSource
    DataSet = cdsLanc
    Left = 360
    Top = 168
  end
end
