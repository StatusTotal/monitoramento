inherited FFiltroRelatorioLancamentosF: TFFiltroRelatorioLancamentosF
  Caption = 'Relat'#243'rio de Lan'#231'amentos'
  ClientHeight = 107
  ExplicitHeight = 136
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlFiltro: TPanel
    Height = 101
    ExplicitWidth = 328
    ExplicitHeight = 101
    inherited pnlBotoes: TPanel
      Top = 54
      TabOrder = 3
      ExplicitTop = 54
      ExplicitWidth = 328
      inherited btnLimpar: TJvTransparentButton
        OnClick = btnLimparClick
      end
    end
    inherited gbTipo: TGroupBox
      Left = 259
      Top = 8
      Width = 64
      Height = 41
      TabOrder = 4
      Visible = False
      ExplicitLeft = 259
      ExplicitTop = 8
      ExplicitWidth = 64
      ExplicitHeight = 41
      inherited chbTipoAnalitico: TCheckBox
        Left = 5
        Top = 25
        Visible = False
        ExplicitLeft = 5
        ExplicitTop = 25
      end
      inherited chbTipoSintetico: TCheckBox
        Left = 5
        Top = 14
        Visible = False
        ExplicitLeft = 5
        ExplicitTop = 14
      end
    end
    inherited chbExibirGrafico: TCheckBox
      Left = 127
      Top = 8
      TabOrder = 5
      Visible = False
      ExplicitLeft = 127
      ExplicitTop = 8
    end
    inherited chbExportarPDF: TCheckBox
      Left = 5
      Top = 31
      TabOrder = 1
      ExplicitLeft = 5
      ExplicitTop = 31
    end
    inherited chbExportarExcel: TCheckBox
      Left = 127
      Top = 31
      TabOrder = 2
      ExplicitLeft = 127
      ExplicitTop = 31
    end
    object chbExibirItens: TCheckBox
      Left = 5
      Top = 8
      Width = 84
      Height = 17
      Caption = 'Exibir Itens'
      TabOrder = 0
    end
  end
end
