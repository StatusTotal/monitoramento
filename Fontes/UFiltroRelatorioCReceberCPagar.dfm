inherited FFiltroRelatorioCReceberCPagar: TFFiltroRelatorioCReceberCPagar
  Caption = 'Relat'#243'rio de Contas a Receber e Contas a Pagar'
  ClientHeight = 249
  ExplicitHeight = 278
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlFiltro: TPanel
    Height = 243
    ExplicitHeight = 274
    object lblPeriodo: TLabel [0]
      Left = 5
      Top = 8
      Width = 129
      Height = 14
      Caption = 'Per'#237'odo de Vencimento'
    end
    object lblPeriodoInicio: TLabel [1]
      Left = 5
      Top = 31
      Width = 32
      Height = 14
      Caption = 'In'#237'cio:'
    end
    object lblPeriodoFim: TLabel [2]
      Left = 149
      Top = 31
      Width = 22
      Height = 14
      Caption = 'Fim:'
    end
    inherited pnlBotoes: TPanel
      Top = 196
      TabOrder = 8
      ExplicitTop = 227
      inherited btnLimpar: TJvTransparentButton
        OnClick = btnLimparClick
      end
    end
    inherited gbTipo: TGroupBox
      Top = 128
      TabOrder = 4
      ExplicitTop = 128
    end
    inherited chbExibirGrafico: TCheckBox
      Top = 128
      TabOrder = 5
      ExplicitTop = 128
    end
    inherited chbExportarPDF: TCheckBox
      Top = 151
      TabOrder = 6
      ExplicitTop = 151
    end
    inherited chbExportarExcel: TCheckBox
      Top = 174
      TabOrder = 7
      ExplicitTop = 174
    end
    object dteDataInicio: TJvDateEdit
      Left = 43
      Top = 28
      Width = 100
      Height = 22
      Color = 16114127
      ShowNullDate = False
      TabOrder = 0
      OnKeyPress = FormKeyPress
    end
    object dteDataFim: TJvDateEdit
      Left = 177
      Top = 28
      Width = 100
      Height = 22
      Color = 16114127
      ShowNullDate = False
      TabOrder = 1
      OnKeyPress = FormKeyPress
    end
    object rgTipoConta: TRadioGroup
      Left = 5
      Top = 56
      Width = 318
      Height = 43
      Caption = 'Tipo de Conta'
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Todas'
        'A Receber'
        'A Pagar')
      TabOrder = 2
    end
    object chbSomenteVencidas: TCheckBox
      Left = 5
      Top = 105
      Width = 126
      Height = 17
      Caption = 'Somente Vencidas'
      TabOrder = 3
      OnKeyPress = FormKeyPress
    end
  end
end
