inherited FFiltroRelatorioLote: TFFiltroRelatorioLote
  Caption = 'Relat'#243'rio de Lotes de Folhas de Seguran'#231'a'
  ClientHeight = 210
  ClientWidth = 430
  ExplicitWidth = 436
  ExplicitHeight = 239
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlFiltro: TPanel
    Width = 424
    Height = 204
    ExplicitWidth = 435
    ExplicitHeight = 232
    inherited pnlBotoes: TPanel
      Top = 157
      Width = 424
      TabOrder = 6
      ExplicitTop = 185
      ExplicitWidth = 435
      inherited btnVisualizar: TJvTransparentButton
        Left = 161
        ExplicitLeft = 161
      end
      inherited btnCancelar: TJvTransparentButton
        Left = 269
        ExplicitLeft = 269
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 53
        OnClick = btnLimparClick
        ExplicitLeft = 53
      end
    end
    inherited gbTipo: TGroupBox
      Left = 246
      Top = 61
      Width = 172
      Height = 41
      TabOrder = 3
      ExplicitLeft = 246
      ExplicitTop = 61
      ExplicitWidth = 172
      ExplicitHeight = 41
      inherited chbTipoAnalitico: TCheckBox
        Left = 93
        Top = 19
        ExplicitLeft = 93
        ExplicitTop = 19
      end
      inherited chbTipoSintetico: TCheckBox
        Left = 19
        ExplicitLeft = 19
      end
    end
    inherited chbExibirGrafico: TCheckBox
      Left = 137
      Top = 133
      TabOrder = 7
      Visible = False
      ExplicitLeft = 137
      ExplicitTop = 133
    end
    inherited chbExportarPDF: TCheckBox
      Left = 5
      Top = 110
      TabOrder = 4
      ExplicitLeft = 5
      ExplicitTop = 110
    end
    inherited chbExportarExcel: TCheckBox
      Left = 5
      Top = 133
      TabOrder = 5
      ExplicitLeft = 5
      ExplicitTop = 133
    end
    object rgstatus: TsRadioGroup
      Left = 5
      Top = 8
      Width = 413
      Height = 41
      Caption = 'Situa'#231#227'o do Lote'
      TabOrder = 0
      Columns = 4
      Items.Strings = (
        'Todos'
        'N'#227'o Utilizado'
        'Utilizado'
        'Cancelado')
    end
    object dtInicial: TsDateEdit
      Left = 5
      Top = 79
      Width = 105
      Height = 23
      AutoSize = False
      EditMask = '!99/99/9999;1; '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 1
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data Compra Inicial'
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
    end
    object dtFinal: TsDateEdit
      Left = 124
      Top = 79
      Width = 105
      Height = 23
      AutoSize = False
      EditMask = '!99/99/9999;1; '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 2
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data Compra Final'
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
    end
  end
end
