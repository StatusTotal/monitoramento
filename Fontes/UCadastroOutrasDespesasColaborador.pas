{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroOutrasDespesasColaborador.pas
  Descricao:   Tela de cadastro de Outras Despesas de Funcionario
  Author   :   Cristina
  Date:        14-jul-2016
  Last Update: 23-out-2018 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroOutrasDespesasColaborador;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  JvBaseEdits, JvDBControls, Vcl.Mask, JvExMask, JvToolEdit, Vcl.DBCtrls,
  FireDAC.Comp.Client, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Datasnap.DBClient,
  Datasnap.Provider, FireDAC.Comp.DataSet, System.StrUtils, System.DateUtils,
  JvExStdCtrls, JvCombobox, JvDBCombobox;

type
  TFCadastroOutrasDespesasColaborador = class(TFCadastroGeralPadrao)
    lblNomeFuncionario: TLabel;
    lblDescricao: TLabel;
    lblDataODesp: TLabel;
    lblValor: TLabel;
    lblDataQuitacao: TLabel;
    lcbNomeFuncionario: TDBLookupComboBox;
    mmDescricao: TDBMemo;
    dteDataODesp: TJvDBDateEdit;
    dteDataQuitacao: TJvDBDateEdit;
    cedValor: TJvDBCalcEdit;
    lcbTipoODespFunc: TDBLookupComboBox;
    lblTipoODespFunc: TLabel;
    btnIncluirTipoDespesa: TJvTransparentButton;
    lblModalidade: TLabel;
    cbModalidade: TJvDBComboBox;
    btnInformarDadosLancamento: TJvTransparentButton;
    chbRecorrente: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dteDataQuitacaoKeyPress(Sender: TObject; var Key: Char);
    procedure btnIncluirTipoDespesaClick(Sender: TObject);
    procedure btnInformarDadosLancamentoClick(Sender: TObject);
    procedure cbModalidadeClick(Sender: TObject);
    procedure cbModalidadeExit(Sender: TObject);
    procedure dteDataODespExit(Sender: TObject);
    procedure dteDataODespKeyPress(Sender: TObject; var Key: Char);
    procedure dteDataQuitacaoExit(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
    iIdCategoria,
    iIdSubCategoria: Integer;

  public
    { Public declarations }
  end;

var
  FCadastroOutrasDespesasColaborador: TFCadastroOutrasDespesasColaborador;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais,
  UDMOutrasDespesasColaborador, UCadastroTipoDespesaColaborador,
  USolicitaDadosLancamento;

{ TFCadastroOutrasDespesasColaborador }

procedure TFCadastroOutrasDespesasColaborador.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroOutrasDespesasColaborador.btnIncluirTipoDespesaClick(
  Sender: TObject);
var
  Op: TOperacao;
  IdCons: Integer;
  OrigF, OrigC: Boolean;
begin
  inherited;

  Op     := vgOperacao;
  IdCons := vgIdConsulta;
  OrigF  := vgOrigemFiltro;
  OrigC  := vgOrigemCadastro;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := False;
  vgOrigemCadastro := True;

  try
    Application.CreateForm(TFCadastroTipoDespesaColaborador, FCadastroTipoDespesaColaborador);
    FCadastroTipoDespesaColaborador.ShowModal;
  finally
    dmOutrasDespesasColaborador.qryTipoODespF.Close;
    dmOutrasDespesasColaborador.qryTipoODespF.Open;

    if FCadastroTipoDespesaColaborador.IdNovoTipoDespColab <> 0 then
      dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('ID_TIPO_OUTRADESP_FUNC_FK').AsInteger := FCadastroTipoDespesaColaborador.IdNovoTipoDespColab;

    FCadastroTipoDespesaColaborador.Free;
  end;

  vgOperacao       := Op;
  vgIdConsulta     := IdCons;
  vgOrigemFiltro   := OrigF;
  vgOrigemCadastro := OrigC;

  if Trim(lcbTipoODespFunc.Text) <> '' then
  begin
    if cedValor.CanFocus then
      cedValor.SetFocus;
  end;
end;

procedure TFCadastroOutrasDespesasColaborador.btnInformarDadosLancamentoClick(
  Sender: TObject);
begin
  inherited;

  try
    Application.CreateForm(TFSolicitaDadosLancamento, FSolicitaDadosLancamento);

    FSolicitaDadosLancamento.sCaptionLabel := 'Por favor, informe a Categoria e/ou a Subcategoria do Lan�amento:';

    FSolicitaDadosLancamento.ShowModal;
  finally
    iIdCategoria    := FSolicitaDadosLancamento.iIdCategoria;
    iIdSubCategoria := FSolicitaDadosLancamento.iIdSubCategoria;

    FSolicitaDadosLancamento.Free;
  end;
end;

procedure TFCadastroOutrasDespesasColaborador.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroOutrasDespesasColaborador.cbModalidadeClick(
  Sender: TObject);
begin
  inherited;

  btnInformarDadosLancamento.Visible := (cbModalidade.ItemIndex = 0);
end;

procedure TFCadastroOutrasDespesasColaborador.cbModalidadeExit(Sender: TObject);
begin
  inherited;

  btnInformarDadosLancamento.Visible := (cbModalidade.ItemIndex = 0);
end;

procedure TFCadastroOutrasDespesasColaborador.DefinirTamanhoMaxCampos;
begin
  inherited;

  mmDescricao.MaxLength := 1000;
end;

procedure TFCadastroOutrasDespesasColaborador.DesabilitarComponentes;
begin
  inherited;

  btnDadosPrincipais.Visible := False;

  btnInformarDadosLancamento.Visible := (cbModalidade.ItemIndex = 0);

  if vgOperacao = E then
  begin
    lcbNomeFuncionario.Enabled := False;
    cedValor.Enabled           := dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('DATA_QUITACAO').IsNull;
  end;

  if vgOperacao = C then
  begin
    dteDataODesp.Enabled       := False;
    lcbNomeFuncionario.Enabled := False;
    mmDescricao.Enabled        := False;
    lcbTipoODespFunc.Enabled   := False;
    cedValor.Enabled           := False;
    dteDataQuitacao.Enabled    := False;
  end;
end;

procedure TFCadastroOutrasDespesasColaborador.dteDataODespExit(Sender: TObject);
begin
  inherited;

  if dmOutrasDespesasColaborador.cdsOutraDespFunc.State in [dsInsert, dsEdit] then
  begin
    if dteDataODesp.Date < vgDataLancVigente then
      dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('DATA_OUTRADESPESA_FUNC').AsDateTime := vgDataLancVigente
    else
      dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('DATA_OUTRADESPESA_FUNC').AsDateTime := dteDataODesp.Date;
  end;
end;

procedure TFCadastroOutrasDespesasColaborador.dteDataODespKeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dmOutrasDespesasColaborador.cdsOutraDespFunc.State in [dsInsert, dsEdit] then
    begin
      if dteDataODesp.Date < vgDataLancVigente then
        dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('DATA_OUTRADESPESA_FUNC').AsDateTime := vgDataLancVigente
      else
        dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('DATA_OUTRADESPESA_FUNC').AsDateTime := dteDataODesp.Date;
    end;

    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    btnCancelar.Click;
end;

procedure TFCadastroOutrasDespesasColaborador.dteDataQuitacaoExit(
  Sender: TObject);
begin
  inherited;

  if dmOutrasDespesasColaborador.cdsOutraDespFunc.State in [dsInsert, dsEdit] then
  begin
    if dteDataQuitacao.Date < vgDataLancVigente then
      dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('DATA_QUITACAO').AsDateTime := vgDataLancVigente
    else
      dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('DATA_QUITACAO').AsDateTime := dteDataQuitacao.Date;
  end;
end;

procedure TFCadastroOutrasDespesasColaborador.dteDataQuitacaoKeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    if Trim(lcbNomeFuncionario.Text) = '' then
    begin
      Application.MessageBox('Por favor, informe o Nome do Colaborador.', 'Aviso', MB_OK + MB_ICONWARNING);

      if lcbNomeFuncionario.CanFocus then
        lcbNomeFuncionario.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if Trim(lcbNomeFuncionario.Text) = '' then
    begin
      Application.MessageBox('Por favor, informe o Nome do Colaborador.', 'Aviso', MB_OK + MB_ICONWARNING);

      if lcbNomeFuncionario.CanFocus then
        lcbNomeFuncionario.SetFocus;
    end
    else if dmOutrasDespesasColaborador.cdsOutraDespFunc.State in [dsInsert, dsEdit] then
    begin
      if dteDataQuitacao.Date < vgDataLancVigente then
        dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('DATA_QUITACAO').AsDateTime := vgDataLancVigente
      else
        dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('DATA_QUITACAO').AsDateTime := dteDataQuitacao.Date;

      btnOk.Click;
    end;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    btnCancelar.Click;
end;

procedure TFCadastroOutrasDespesasColaborador.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  if lPodeCancelar then
  begin
    dmOutrasDespesasColaborador.cdsOutraDespFunc.Cancel;

    FreeAndNil(dmOutrasDespesasColaborador);

    vgOperacao     := VAZIO;
    vgIdConsulta   := 0;
    vgOrigemFiltro := False;
  end
  else
    Action := caNone;
end;

procedure TFCadastroOutrasDespesasColaborador.FormCreate(Sender: TObject);
begin
  inherited;

  dmOutrasDespesasColaborador.qryFuncionarios.Close;
  dmOutrasDespesasColaborador.qryFuncionarios.Open;

  dmOutrasDespesasColaborador.qryTipoODespF.Close;
  dmOutrasDespesasColaborador.qryTipoODespF.Open;

  dmOutrasDespesasColaborador.cdsOutraDespFunc.Close;
  dmOutrasDespesasColaborador.cdsOutraDespFunc.Params.ParamByName('ID_OUTRADESPESA_FUNC').Value := vgIdConsulta;
  dmOutrasDespesasColaborador.cdsOutraDespFunc.Open;

  if vgOperacao = I then
  begin
    dmOutrasDespesasColaborador.cdsOutraDespFunc.Append;

    if Date < vgDataLancVigente then
      dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('DATA_OUTRADESPESA_FUNC').AsDateTime := vgDataLancVigente
    else
      dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('DATA_OUTRADESPESA_FUNC').AsDateTime := Date;

    dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('DATA_CADASTRO').AsDateTime := Now;
    dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('CAD_ID_USUARIO').Value     := vgUsu_Id;
    dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('DATA_QUITACAO').AsDateTime := Date;
    dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('FLG_CANCELADO').AsString   := 'N';
    dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('FLG_RECORRENTE').AsString  := 'N';
  end;

  if vgOperacao = E then
  begin
    dmOutrasDespesasColaborador.cdsOutraDespFunc.Edit;
    dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('VR_ODESP_ANTERIOR').AsCurrency := dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('VR_OUTRADESPESA_FUNC').AsCurrency;
  end;
end;

procedure TFCadastroOutrasDespesasColaborador.FormShow(Sender: TObject);
begin
  inherited;

  if lcbNomeFuncionario.CanFocus then
    lcbNomeFuncionario.SetFocus;
end;

function TFCadastroOutrasDespesasColaborador.GravarDadosAto(
  var Msg: String): Boolean;
begin
  //
end;

function TFCadastroOutrasDespesasColaborador.GravarDadosGenerico(
  var Msg: String): Boolean;
var
  IdForn, IdNat, IdCat, IdSubCat, Cod: Integer;
  Doc, Obs, NFantasia, RSocial, Descr: String;
  IdODespF, CodLanc, AnoLanc, IdParc, IdItem: Integer;
  sMsg, sIR, sCP, sLA: String;
  QryAuxS: TFDQuery;
  cValorDisp: Currency;
  Op: TOperacao;
  lContinuar: Boolean;
begin
  Result := True;
  Msg    := '';
  Op     := vgOperacao;

  cValorDisp := 0;

  if vgOperacao = C then
    Exit;

  IdODespF := 0;
  CodLanc  := 0;
  AnoLanc  := 0;
  IdParc   := 0;
  IdForn   := 0;
  IdItem   := 0;
  IdNat    := 0;
  IdCat    := 0;
  IdSubCat := 0;
  Cod      := 0;

  Doc       := '';
  Obs       := '';
  NFantasia := '';
  RSocial   := '';
  Descr     := '';

  lContinuar:= True;

  QryAuxS := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    { OUTRA DESPESA }
    if vgOperacao = I then
      IdODespF := BS.ProximoId('ID_OUTRADESPESA_FUNC', 'OUTRADESPESA_FUNC')
    else
      IdODespF := dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('ID_OUTRADESPESA_FUNC').AsInteger;

    dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('ID_OUTRADESPESA_FUNC').Value := IdODespF;

    if not dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('DATA_QUITACAO').IsNull then
      dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('QUIT_ID_USUARIO').Value := vgUsu_Id;

    dmOutrasDespesasColaborador.cdsOutraDespFunc.Post;
    dmOutrasDespesasColaborador.cdsOutraDespFunc.ApplyUpdates(0);

    dmOutrasDespesasColaborador.cdsOutraDespFunc.Close;
    dmOutrasDespesasColaborador.cdsOutraDespFunc.Params.ParamByName('ID_OUTRADESPESA_FUNC').Value := IdODespF;
    dmOutrasDespesasColaborador.cdsOutraDespFunc.Open;

    if (dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('FLG_MODALIDADE').AsString <> 'L') then
    begin
      if dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('VR_ODESP_ANTERIOR').AsCurrency <> dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('VR_OUTRADESPESA_FUNC').AsCurrency then
        dmPrincipal.RecalcularFechamentoSalarios(dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('ID_FECHAMENTO_SALARIO_FK').AsInteger,
                                                 dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('ID_FUNCIONARIO_FK').AsInteger,
                                                 False, False, True);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    lContinuar := False;
  end;

  if lContinuar then
  begin
    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      if dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('FLG_MODALIDADE').AsString = 'L' then
      begin
        sIR := '';
        sCP := '';
        sLA := '';

        dmPrincipal.RetornarClassificacaoNatureza(7, sIR, sCP, sLA);

        { LANCAMENTO DE DESPESA }
        //Fornecedor
        Doc       := dmOutrasDespesasColaborador.qryFuncionarios.FieldByName('CPF').AsString;
        Obs       := 'Cadastro realizado automaticamente em inclus�o de OUTROS LAN�AMENTO DE COLABORADOR.';
        NFantasia := dmOutrasDespesasColaborador.qryFuncionarios.FieldByName('NOME_FUNCIONARIO').AsString;
        RSocial   := dmOutrasDespesasColaborador.qryFuncionarios.FieldByName('NOME_FUNCIONARIO').AsString;

        dmPrincipal.RetornarClienteFornecedor('F',
                                              IdForn,
                                              Doc,
                                              Obs,
                                              NFantasia,
                                              RSocial);

        //Categoria
        IdNat := 7;
        IdCat := 0;
        Cod   := 1;  //Despesa
        Descr := 'COLABORADORES';
        Obs   := 'Inclus�o AUTOM�TICA de Categoria de Despesa';

        dmPrincipal.RetornaCategoria(IdCat, IdNat, Cod, Descr, Obs);

        //SubCategoria
        IdSubCat := 0;
        Cod      := 1;  //Despesa
        Descr    := NFantasia;
        Obs      := 'Inclus�o AUTOM�TICA de Subcategoria de Despesa';

        dmPrincipal.RetornaSubCategoria(IdSubCat, IdCat, Cod, Descr, Obs);

        //Lancamento
        dmPrincipal.InicializarComponenteLancamento;

        DadosLancamento := Lancamento.Create;

        if vgOperacao = I then
        begin
          DadosLancamento.DataLancamento   := dteDataODesp.Date;
          DadosLancamento.TipoLancamento   := 'D';
          DadosLancamento.TipoCadastro     := 'A';
          DadosLancamento.IdCategoria      := IdCat;
          DadosLancamento.IdSubCategoria   := IdSubCat;
          DadosLancamento.IdCliFor         := IdForn;
          DadosLancamento.FlgIR            := sIR;
          DadosLancamento.FlgPessoal       := sCP;
          DadosLancamento.FlgFlutuante     := 'N';
          DadosLancamento.FlgAuxiliar      := sLA;
          DadosLancamento.FlgForaFechCaixa := 'N';
          DadosLancamento.IdNatureza       := 7;  //OUTROS LANCAMENTOS DE FUNCIONARIO

          if iIdCategoria > 0 then
            DadosLancamento.IdCategoria := iIdCategoria;

          if iIdSubCategoria > 0 then
            DadosLancamento.IdSubCategoria := iIdSubCategoria;

          DadosLancamento.QtdParcelas      := 1;
          DadosLancamento.CadIdUsuario     := vgUsu_Id;
          DadosLancamento.DataCadastro     := Now;

          { Independentemente do Lancamento de Colaborador ser ou nao Recorrente
            para nao afetar a geracao automatica de Lancamentos dessa Natureza }
          DadosLancamento.FlgRecorrente    := 'N';

          DadosLancamento.FlgReplicado     := 'N';
          DadosLancamento.IdOrigem         := IdODespF;
        end
        else
        if vgOperacao = E then
        begin
          QryAuxS.SQL.Text := 'SELECT COD_LANCAMENTO, ANO_LANCAMENTO ' +
                              '  FROM LANCAMENTO ' +
                              ' WHERE ID_ORIGEM      = :ID_ORIGEM ' +
                              '   AND ID_NATUREZA_FK = :ID_NATUREZA';
          QryAuxS.Params.ParamByName('ID_ORIGEM').Value   := IdODespF;
          QryAuxS.Params.ParamByName('ID_NATUREZA').Value := 7;
          QryAuxS.Open;

          DadosLancamento.CodLancamento := QryAuxS.FieldByName('COD_LANCAMENTO').AsInteger;
          DadosLancamento.AnoLancamento := QryAuxS.FieldByName('ANO_LANCAMENTO').AsInteger;
        end;

        if not dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('DATA_QUITACAO').IsNull then
        begin
          DadosLancamento.VlrTotalPago := cedValor.Value;
          DadosLancamento.FlgStatus    := 'G';
        end;

        DadosLancamento.VlrTotalPrev := cedValor.Value;
        DadosLancamento.Observacao := 'Despesa originada pelo LAN�AMENTO ' + IntToStr(IdODespF) + '.' + #13#10 +
                                      ' de COLABORADOR' + Trim(mmDescricao.Text);

        ListaLancamentos.Add(DadosLancamento);

        if vgOperacao = I then
          dmPrincipal.InsertLancamento(0)
        else if vgOperacao = E then
          dmPrincipal.UpdateLancamento(0);

        { PARCELAS }
        DadosLancamentoParc := LancamentoParc.Create;

        if vgOperacao = I then
        begin
          DadosLancamentoParc.NumParcela     := 1;
          DadosLancamentoParc.DataLancParc   := dteDataODesp.Date;
          DadosLancamentoParc.IdFormaPagto   := 6;  //Dinheiro
          DadosLancamentoParc.DataVencimento := Date;
          DadosLancamentoParc.CadIdUsuario   := vgUsu_Id;
          DadosLancamentoParc.DataCadastro   := Now;
        end;

        DadosLancamentoParc.VlrPrevisto := cedValor.Value;

        if not dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('DATA_QUITACAO').IsNull then
        begin
          DadosLancamentoParc.VlrPago       := cedValor.Value;
          DadosLancamentoParc.DataPagamento := dmOutrasDespesasColaborador.cdsOutraDespFunc.FieldByName('DATA_QUITACAO').AsDateTime;
          DadosLancamentoParc.FlgStatus     := 'G';
          DadosLancamentoParc.PagIdUsuario  := vgUsu_Id;
        end;

        ListaLancamentoParcs.Add(DadosLancamentoParc);

        if vgOperacao = I then
          dmPrincipal.InsertLancamentoParc(0, 0)
        else if vgOperacao = E then
          dmPrincipal.UpdateLancamentoParc(0, 0);

        { LOG }
        if vgOperacao = I then
        begin
          with QryAuxS do
          begin
            QryAuxS.Close;
            QryAuxS.SQL.Clear;
            QryAuxS.SQL.Text := 'INSERT INTO USUARIO_LOG (ID_USUARIO_LOG, ID_USUARIO, ID_MODULO_FK, ' +
                                '                         TIPO_LANCAMENTO, CODIGO_LAN, ANO_LAN, ID_ORIGEM, ' +
                                '                         TABELA_ORIGEM, TIPO_ACAO, OBS_USUARIO_LOG) ' +
                                '                 VALUES (:ID_USUARIO_LOG, :ID_USUARIO, :ID_MODULO, ' +
                                '                         :TIPO_LANCAMENTO, :CODIGO_LAN, :ANO_LAN, :ID_ORIGEM, ' +
                                '                         :TABELA_ORIGEM, :TIPO_ACAO, :OBS_USUARIO_LOG)';

            QryAuxS.Params.ParamByName('ID_USUARIO_LOG').Value  := BS.ProximoId('ID_USUARIO_LOG', 'USUARIO_LOG');
            QryAuxS.Params.ParamByName('ID_USUARIO').Value      := vgUsu_Id;
            QryAuxS.Params.ParamByName('ID_MODULO').Value       := 80;
            QryAuxS.Params.ParamByName('TIPO_LANCAMENTO').Value := 'D';
            QryAuxS.Params.ParamByName('CODIGO_LAN').Value      := ListaLancamentos[0].CodLancamento;
            QryAuxS.Params.ParamByName('ANO_LAN').Value         := ListaLancamentos[0].AnoLancamento;
            QryAuxS.Params.ParamByName('ID_ORIGEM').Value       := Null;
            QryAuxS.Params.ParamByName('TABELA_ORIGEM').Value   := Null;
            QryAuxS.Params.ParamByName('TIPO_ACAO').Value       := 'I';
            QryAuxS.Params.ParamByName('OBS_USUARIO_LOG').Value := 'Inclus�o AUTOM�TICA de LAN�AMENTO DE COLABORADOR.';

            QryAuxS.ExecSQL;
          end;
        end;

        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Commit;

        dmPrincipal.FinalizarComponenteLancamento;
      end;

      Msg := 'Dados do Lan�amento gravados com sucesso!';
    except
      on E: Exception do
      begin
        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Rollback;

        Result := False;
        Msg := 'Erro na grava��o do Dados do Lan�amento.';
      end;
    end;
  end;

  FreeAndNil(QryAuxS);
end;

procedure TFCadastroOutrasDespesasColaborador.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta de Dados do Lan�amento de Colaborador',
                          vgIdConsulta, 'OUTRADESPESA_FUNC');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclus�o de Dados do Lan�amento de Colaborador',
                          vgIdConsulta, 'OUTRADESPESA_FUNC');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da edi��o desse Lan�amento de Colaborador:', '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de Lan�amento de Colaborador';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          vgIdConsulta, 'OUTRADESPESA_FUNC');
    end;
  end;
end;

procedure TFCadastroOutrasDespesasColaborador.lbMensagemErroDblClick(
  Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroOutrasDespesasColaborador.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

function TFCadastroOutrasDespesasColaborador.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if dteDataODesp.Date = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a DATA DO LAN�AMENTO.';
    DadosMsgErro.Componente := dteDataODesp;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end
  else
  begin
    if dteDataODesp.Date > Date then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') A DATA DO LAN�AMENTO n�o pode ser superior � Data de hoje.';
      DadosMsgErro.Componente := dteDataODesp;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;

    if dteDataODesp.Date < vgDataLancVigente then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') A DATA DO LAN�AMENTO n�o pode ser inferior a ' +
                                                          FormatDateTime('DD/MM/YYYY', vgDataLancVigente) +
                                                          ', pois os Caixas anteriores a essa data j� se encontram FECHADOS.';;
      DadosMsgErro.Componente := dteDataODesp;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;
  end;

  if Trim(lcbNomeFuncionario.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o NOME DO COLABORADOR.';
    DadosMsgErro.Componente := lcbNomeFuncionario;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(mmDescricao.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a DESCRI��O DO LAN�AMENTO.';
    DadosMsgErro.Componente := mmDescricao;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(lcbTipoODespFunc.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a DESCRI��O DO LAN�AMENTO.';
    DadosMsgErro.Componente := lcbTipoODespFunc;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if cedValor.Value = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o VALOR DO LAN�AMENTO.';
    DadosMsgErro.Componente := cedValor;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroOutrasDespesasColaborador.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

end.
