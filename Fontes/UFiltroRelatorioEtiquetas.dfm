inherited FFiltroRelatorioEtiquetas: TFFiltroRelatorioEtiquetas
  Caption = 'Relat'#243'rio de Etiquetas de Seguran'#231'a'
  ClientHeight = 280
  ClientWidth = 429
  ExplicitWidth = 435
  ExplicitHeight = 309
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlFiltro: TPanel
    Width = 423
    Height = 274
    ExplicitWidth = 435
    ExplicitHeight = 309
    inherited pnlBotoes: TPanel
      Top = 227
      Width = 423
      TabOrder = 6
      ExplicitTop = 262
      ExplicitWidth = 435
      inherited btnVisualizar: TJvTransparentButton
        Left = 165
        ExplicitLeft = 165
      end
      inherited btnCancelar: TJvTransparentButton
        Left = 273
        ExplicitLeft = 273
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 57
        OnClick = btnLimparClick
        ExplicitLeft = 57
      end
    end
    inherited gbTipo: TGroupBox
      Left = 246
      Top = 133
      Width = 172
      Height = 41
      TabOrder = 3
      ExplicitLeft = 246
      ExplicitTop = 133
      ExplicitWidth = 172
      ExplicitHeight = 41
      inherited chbTipoAnalitico: TCheckBox
        Left = 93
        Top = 19
        ExplicitLeft = 93
        ExplicitTop = 19
      end
      inherited chbTipoSintetico: TCheckBox
        Left = 19
        ExplicitLeft = 19
      end
    end
    inherited chbExibirGrafico: TCheckBox
      Left = 137
      Top = 205
      TabOrder = 7
      Visible = False
      ExplicitLeft = 137
      ExplicitTop = 205
    end
    inherited chbExportarPDF: TCheckBox
      Left = 5
      Top = 182
      TabOrder = 4
      ExplicitLeft = 5
      ExplicitTop = 182
    end
    inherited chbExportarExcel: TCheckBox
      Left = 5
      Top = 205
      TabOrder = 5
      ExplicitLeft = 5
      ExplicitTop = 205
    end
    object rgstatus: TsRadioGroup
      Left = 5
      Top = 8
      Width = 413
      Height = 41
      Caption = 'Status do Lote'
      TabOrder = 0
      Columns = 4
      Items.Strings = (
        'Todos'
        'Utilizadas'
        'N'#227'o Utilizadas'
        'Canceladas')
    end
    object dtInicial: TsDateEdit
      Left = 5
      Top = 151
      Width = 105
      Height = 23
      AutoSize = False
      EditMask = '!99/99/9999;1; '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 1
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data Compra Inicial'
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
    end
    object dtFinal: TsDateEdit
      Left = 124
      Top = 151
      Width = 105
      Height = 23
      AutoSize = False
      EditMask = '!99/99/9999;1; '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 2
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data Compra Final'
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
    end
    object rgSistema: TsRadioGroup
      Left = 5
      Top = 54
      Width = 413
      Height = 75
      Caption = 'Sistema'
      TabOrder = 8
      Columns = 4
      Items.Strings = (
        'TODOS'
        'FIRMAS'
        'NOTAS'
        'RGI'
        'PROTESTO'
        'RCPN'
        'RTD'
        'RCPJ')
    end
  end
end
