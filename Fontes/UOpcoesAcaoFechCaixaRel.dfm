inherited FOpcoesAcaoFechCaixaRel: TFOpcoesAcaoFechCaixaRel
  Caption = 'FOpcoesAcaoFechCaixaRel'
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlOpcoes: TPanel
    inherited pnlBotoes: TPanel
      inherited btnOpcao2: TJvTransparentButton
        Action = actRelatorioDiferencas
        Spacing = 7
        Images.ActiveImage = dmGerencial.Im32
        Images.ActiveIndex = 6
        Images.DisabledImage = dmGerencial.Im32Dis
        Images.DisabledIndex = 6
        ExplicitLeft = 78
        ExplicitHeight = 72
      end
      inherited btnOpcao1: TJvTransparentButton
        Action = actRelatorioMovimentoCaixa
        Spacing = 3
        WordWrap = True
        Images.ActiveImage = dmGerencial.Im32
        Images.ActiveIndex = 50
        Images.DisabledImage = dmGerencial.Im32Dis
        Images.DisabledIndex = 50
      end
    end
  end
  object acmBotoes: TActionManager
    DisabledImages = dmGerencial.Im32
    Images = dmGerencial.Im32
    Left = 62
    Top = 22
    StyleName = 'Platform Default'
    object actRelatorioDiferencas: TAction
      Category = 'Relatorios'
      Caption = '&Diferen'#231'as'
      ImageIndex = 6
      OnExecute = actRelatorioDiferencasExecute
    end
    object actRelatorioMovimentoCaixa: TAction
      Category = 'Relatorios'
      Caption = '&Movimento de Caixa'
      ImageIndex = 50
      OnExecute = actRelatorioMovimentoCaixaExecute
    end
  end
end
