inherited FFiltroVales: TFFiltroVales
  Caption = 'Filtro de Vales'
  ClientHeight = 394
  ClientWidth = 952
  OnCreate = FormCreate
  ExplicitWidth = 958
  ExplicitHeight = 423
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 946
    Height = 388
    ExplicitWidth = 946
    ExplicitHeight = 388
    inherited pnlGrid: TPanel
      Top = 97
      Width = 862
      ExplicitTop = 97
      ExplicitWidth = 862
      inherited pnlGridPai: TPanel
        Width = 862
        ExplicitWidth = 862
        inherited dbgGridPaiPadrao: TDBGrid
          Width = 862
          OnTitleClick = dbgGridPaiPadraoTitleClick
          Columns = <
            item
              Expanded = False
              FieldName = 'ID'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'ID_FUNCIONARIO_FK'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'DATA_VALE'
              Title.Caption = 'Dt. Vale'
              Width = 85
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEFUNCIONARIO'
              Title.Caption = 'Nome Colaborador'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VR_VALE'
              Title.Alignment = taRightJustify
              Title.Caption = 'Vlr. Vale'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DESCR_VALE'
              Title.Caption = 'Descri'#231#227'o'
              Width = 230
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATA_CADASTRO'
              Title.Caption = 'Dt. Cadastro'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'CAD_ID_USUARIO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'DATA_QUITACAO'
              Title.Caption = 'Dt. Quita'#231#227'o'
              Width = 85
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QUIT_ID_USUARIO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'FLG_CANCELADO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'CANCEL_ID_USUARIO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'DATA_CANCELAMENTO'
              Title.Caption = 'Dt. Cancel.'
              Width = 85
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_FECHAMENTO_SALARIO_FK'
              Visible = False
            end>
        end
      end
    end
    inherited pnlFiltro: TPanel
      Width = 946
      Height = 94
      ExplicitWidth = 946
      ExplicitHeight = 94
      inherited btnFiltrar: TJvTransparentButton
        Left = 860
        Top = 64
        ExplicitLeft = 860
        ExplicitTop = 64
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 860
        Top = 34
        ExplicitLeft = 860
        ExplicitTop = 34
      end
      object lblNomeFuncionario: TLabel
        Left = 81
        Top = 52
        Width = 100
        Height = 14
        Caption = 'Nome Colaborador'
      end
      object lblPeriodoCadastro: TLabel
        Left = 81
        Top = 8
        Width = 110
        Height = 14
        Caption = 'Per'#237'odo de Cadastro'
      end
      object Label3: TLabel
        Left = 187
        Top = 27
        Width = 6
        Height = 14
        Caption = 'a'
      end
      object lcbNomeFuncionario: TDBLookupComboBox
        Left = 81
        Top = 68
        Width = 401
        Height = 22
        Color = 16114127
        KeyField = 'ID_FUNCIONARIO'
        ListField = 'NOME_FUNCIONARIO'
        ListSource = dsFuncionarios
        NullValueKey = 16460
        TabOrder = 2
        OnKeyPress = lcbNomeFuncionarioKeyPress
      end
      object dteIniPeriodoCadastro: TJvDateEdit
        Left = 81
        Top = 24
        Width = 100
        Height = 22
        Color = 16114127
        ShowNullDate = False
        TabOrder = 0
        OnKeyPress = dteIniPeriodoCadastroKeyPress
      end
      object dteFimPeriodoCadastro: TJvDateEdit
        Left = 199
        Top = 24
        Width = 100
        Height = 22
        Color = 16114127
        ShowNullDate = False
        TabOrder = 1
        OnKeyPress = dteFimPeriodoCadastroKeyPress
      end
      object rgSituacaoVale: TRadioGroup
        Left = 488
        Top = 49
        Width = 281
        Height = 41
        Caption = 'Situa'#231#227'o'
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Todas'
          'N'#227'o quitado'
          'Quitado')
        TabOrder = 3
        OnExit = rgSituacaoValeExit
      end
    end
    inherited pnlMenu: TPanel
      Top = 97
      ExplicitTop = 97
    end
  end
  inherited dsGridPaiPadrao: TDataSource
    DataSet = cdsGridPaiPadrao
    Left = 868
    Top = 299
  end
  inherited qryGridPaiPadrao: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT ID_VALE AS ID,'
      '       ID_FUNCIONARIO_FK,'
      '       DATA_VALE,'
      '       VR_VALE,'
      '       DESCR_VALE,'
      '       ID_FECHAMENTO_SALARIO_FK,'
      '       DATA_CADASTRO,'
      '       CAD_ID_USUARIO,'
      '       DATA_QUITACAO,'
      '       QUIT_ID_USUARIO,'
      '       FLG_CANCELADO,'
      '       DATA_CANCELAMENTO,'
      '       CANCEL_ID_USUARIO'
      '  FROM VALE'
      
        ' WHERE ID_FUNCIONARIO_FK = (CASE WHEN (:ID_FUNC01 = 0) THEN ID_F' +
        'UNCIONARIO_FK ELSE :ID_FUNC02 END)'
      '   AND DATA_VALE BETWEEN :DATA_INI AND :DATA_FIM'
      'ORDER BY ID_FUNCIONARIO_FK ASC, DATA_VALE DESC')
    Left = 868
    Top = 151
    ParamData = <
      item
        Position = 1
        Name = 'ID_FUNC01'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'ID_FUNC02'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 3
        Name = 'DATA_INI'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Position = 4
        Name = 'DATA_FIM'
        DataType = ftDate
        ParamType = ptInput
      end>
  end
  object qryFuncionarios: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT ID_FUNCIONARIO, NOME_FUNCIONARIO'
      '  FROM FUNCIONARIO'
      ' WHERE NOME_FUNCIONARIO <> '#39'FUNCION'#193'RIO'#39
      'ORDER BY NOME_FUNCIONARIO')
    Left = 260
    Top = 255
  end
  object dsFuncionarios: TDataSource
    DataSet = qryFuncionarios
    Left = 260
    Top = 303
  end
  object dspGridPaiPadrao: TDataSetProvider
    DataSet = qryGridPaiPadrao
    Left = 868
    Top = 199
  end
  object cdsGridPaiPadrao: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_FUNC01'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID_FUNC02'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'DATA_INI'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'DATA_FIM'
        ParamType = ptInput
      end>
    ProviderName = 'dspGridPaiPadrao'
    OnCalcFields = cdsGridPaiPadraoCalcFields
    Left = 868
    Top = 247
    object cdsGridPaiPadraoID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object cdsGridPaiPadraoID_FUNCIONARIO_FK: TIntegerField
      FieldName = 'ID_FUNCIONARIO_FK'
    end
    object cdsGridPaiPadraoDATA_VALE: TDateField
      FieldName = 'DATA_VALE'
    end
    object cdsGridPaiPadraoVR_VALE: TBCDField
      FieldName = 'VR_VALE'
      OnGetText = cdsGridPaiPadraoVR_VALEGetText
      Precision = 18
      Size = 2
    end
    object cdsGridPaiPadraoDESCR_VALE: TStringField
      FieldName = 'DESCR_VALE'
      Size = 1000
    end
    object cdsGridPaiPadraoDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsGridPaiPadraoCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsGridPaiPadraoDATA_QUITACAO: TDateField
      FieldName = 'DATA_QUITACAO'
    end
    object cdsGridPaiPadraoQUIT_ID_USUARIO: TIntegerField
      FieldName = 'QUIT_ID_USUARIO'
    end
    object cdsGridPaiPadraoNOMEFUNCIONARIO: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEFUNCIONARIO'
      Size = 250
    end
    object cdsGridPaiPadraoFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsGridPaiPadraoDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsGridPaiPadraoCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsGridPaiPadraoID_FECHAMENTO_SALARIO_FK: TIntegerField
      FieldName = 'ID_FECHAMENTO_SALARIO_FK'
    end
  end
end
