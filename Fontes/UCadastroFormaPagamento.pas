{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroFormaPagamento.pas
  Descricao:   Cadastro de Formas de Pagamento
  Author   :   Cristina
  Date:        04-mar-2016
  Last Update: 23-out-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroFormaPagamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.DBCtrls, JvExMask, JvToolEdit, JvBaseEdits, JvDBControls, Vcl.Mask,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.DBClient, Datasnap.Provider,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, JvExStdCtrls, JvCombobox,
  JvDBCombobox;

type
  TFCadastroFormaPagamento = class(TFCadastroGeralPadrao)
    lblDescricao: TLabel;
    lblMaxParcelas: TLabel;
    lblMinParcelas: TLabel;
    lblPrazo: TLabel;
    lblPercTarifa: TLabel;
    lblFlgAtiva: TLabel;
    edtDescricao: TDBEdit;
    edtMaxParcelas: TDBEdit;
    edtMinParcelas: TDBEdit;
    edtPrazo: TDBEdit;
    cedPercTarifa: TJvDBCalcEdit;
    cbFlgAtiva: TDBComboBox;
    qryFormaPagamento: TFDQuery;
    dspFormaPagamento: TDataSetProvider;
    cdsFormaPagamento: TClientDataSet;
    lblTipoFormaPagamento: TLabel;
    lcbTipoFormaPagamento: TJvDBComboBox;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbFlgAtivaKeyPress(Sender: TObject; var Key: Char);
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadastroFormaPagamento: TFCadastroFormaPagamento;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{ TFCadastroFormaPagamento }

procedure TFCadastroFormaPagamento.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroFormaPagamento.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroFormaPagamento.cbFlgAtivaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if Trim(edtDescricao.Text) <> '' then
      btnOk.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroFormaPagamento.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtDescricao.MaxLength := 250;
end;

procedure TFCadastroFormaPagamento.DesabilitarComponentes;
begin
  inherited;

  btnDadosPrincipais.Visible := False;

  edtDescricao.Enabled          := False;
  lcbTipoFormaPagamento.Enabled := False;
end;

procedure TFCadastroFormaPagamento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  if lPodeCancelar then
  begin
    vgOperacao     := VAZIO;
    vgIdConsulta   := 0;
    vgOrigemFiltro := False;
  end
  else
    Action := caNone;
end;

procedure TFCadastroFormaPagamento.FormShow(Sender: TObject);
begin
  inherited;

  cdsFormaPagamento.Close;
  cdsFormaPagamento.Params.ParamByName('ID_FORMAPAGAMENTO').Value := vgIdConsulta;
  cdsFormaPagamento.Open;

  if vgOperacao = I then
  begin
    cdsFormaPagamento.Append;
    cdsFormaPagamento.FieldByName('PERC_TARIFA').AsCurrency := 0.00;
  end;

  if vgOperacao = E then
    cdsFormaPagamento.Edit;

  if vgOperacao = C then
    pnlDados.Enabled := False;

  if edtMinParcelas.CanFocus then
    edtMinParcelas.SetFocus;
end;

function TFCadastroFormaPagamento.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroFormaPagamento.GravarDadosGenerico(var Msg: String): Boolean;
begin
  Result := True;
  Msg := '';

  if vgOperacao = C then
    Exit;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    if cdsFormaPagamento.State in [dsInsert, dsEdit] then
    begin
      if vgOperacao = I then
        cdsFormaPagamento.FieldByName('ID_FORMAPAGAMENTO').AsInteger := BS.ProximoId('ID_FORMAPAGAMENTO', 'FORMAPAGAMENTO');

      cdsFormaPagamento.Post;
      cdsFormaPagamento.ApplyUpdates(0);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    Msg := 'Forma de Pagamento gravada com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na grava��o da Forma de Pagamento.';
  end;
end;

procedure TFCadastroFormaPagamento.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta de Forma de Pagamento',
                          vgIdConsulta, 'FORMAPAGAMENTO');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclus�o de Forma de Pagamento',
                          vgIdConsulta, 'FORMAPAGAMENTO');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO',
                                  'Por favor, indique o motivo da edi��o dessa Forma de Pagamento:',
                                  '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de Forma de Pagamento';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          vgIdConsulta, 'FORMAPAGAMENTO');
    end;
  end;
end;

procedure TFCadastroFormaPagamento.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroFormaPagamento.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

function TFCadastroFormaPagamento.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if Trim(edtDescricao.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a DESCRI��O DA FORMA DE PAGAMENTO.';
    DadosMsgErro.Componente := edtDescricao;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtMinParcelas.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar o M�NIMO DE PARCELAS DA FORMA DE PAGAMENTO.';
    DadosMsgErro.Componente := edtMinParcelas;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(cbFlgAtiva.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar se a FORMA DE PAGAMENTO est� ou n�o ATIVA.';
    DadosMsgErro.Componente := cbFlgAtiva;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(lcbTipoFormaPagamento.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar o TIPO DA FORMA DE PAGAMENTO.';
    DadosMsgErro.Componente := lcbTipoFormaPagamento;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroFormaPagamento.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

end.
