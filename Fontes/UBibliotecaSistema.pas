{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UBibliotecaSistema.pas
  Descricao:   Biblioteca de Procedures e Funcoes comuns no Sistema
  Author   :   Cristina
  Date:        08-dez-2015
  Last Update: 11-jan-2018 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UBibliotecaSistema;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
     System.Classes, Vcl.Forms, Vcl.Dialogs, FireDAC.Comp.Client, System.DateUtils,
     Vcl.StdCtrls, Vcl.Controls, System.StrUtils, FireDAC.Stan.Param;

type           //Edit          //Combobox
  TCampoCN = (EDTCAT,          CBCAT,
              EDTSUBCAT,       CBSUBCAT);

  TBibliotecaSistema = class(TForm)

    procedure DeterminarVersaoSistema;
    procedure GravarUsuarioLog(Modulo: Integer = 0; Selo: String = '';
                               Observacao: String = ''; Id: Integer = 0;
                               Tabela: String = '');
    procedure MontarComboBox(Combo: TComboBox; Campo, Tabela, Where,
                             Parametro, OrderBy: String);
    procedure PreencherVariaveisSistema;
    procedure RecuperCodigoNome(Edt: TEdit; Cb: TComboBox; Campo: TCampoCN;
                                Tipo: String; Valor: Integer = 0);
    procedure VerificarExistenciaPastasSistema;

    function ProximoId(Campo, Tabela: String): Integer;

    private
      { Private declarations }
    public
      { Public declarations }
  end;

var
  BS: TBibliotecaSistema;

implementation

uses UDM, UVariaveisGlobais, UVersaoSistema, UGDM;

procedure TBibliotecaSistema.PreencherVariaveisSistema;
const
  //Financeiro
  iPermF: array[0..65] of Integer = (2001, 2002, 2005, 2006, 2009, 2010, 2011,
                                     2013, 2014, 2015, 2016, 2017, 2020, 2021,
                                     2024, 2025, 2026, 2028, 2029, 2030, 2031,
                                     2032, 2036, 2037, 2040, 2041, 2042, 2043,
                                     2046, 2047, 2048, 2049, 2052, 2053, 2054,
                                     2055, 2058, 2059, 2060, 2061, 2064, 2065,
                                     2076, 2077, 2080, 2082, 2083, 2084, 2085,
                                     2086, 2087, 2090, 2091, 2092, 2094, 2095,
                                     2096, 2097, 2098, 2100, 2101, 2102, 2105,
                                     2106, 2107, 2108);

  //Oficios
  iPermO: array[0..7] of Integer = (3001, 3002, 3005, 3006, 3007, 3008, 3011,
                                    3012);

  //Relatorios
  iPermR: array[0..36] of Integer = (50001, 50002, 50003, 50004, 50005, 50006,
                                     50007, 50008, 50009, 50010, 50011, 50012,
                                     50013, 50014, 50015, 50016, 50017, 50018,
                                     50019, 50020, 50021, 50022, 50023, 50024,
                                     50025, 50026, 50027, 50028, 50029, 50030,
                                     50031, 50032, 50033, 50034, 50035, 50036,
                                     50037);
var
  QryVars, QryUpUsu, QryUpPerm, QryInPerm: TFDQuery;
  sQueryPerm, sModulos, sNomeMaquina: String;
  j: Integer;
  ComputerName: Array [0 .. 256] of Char;
  Size: DWORD;
begin
  QryVars   := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
  QryUpPerm := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
  QryInPerm := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  j := 0;

  with QryVars, SQL do
  begin
    { Preencher as variaveis do Sistema }
    //USUARIO - OFICIAL
    Close;
    Clear;
    Text := 'SELECT FLG_OFICIAL ' +
            '  FROM USUARIO ' +
            ' WHERE ID_USUARIO = :ID_USUARIO';
    Params.ParamByName('ID_USUARIO').Value := vgUsu_Id;
    Open;

    vgUsu_FlgOficial := FieldByName('FLG_OFICIAL').AsString;

    Size := 256;
    GetComputerName(ComputerName, Size);
    sNomeMaquina := ComputerName;

    //USUARIO - NOME MAQUINA
    QryUpUsu := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    QryUpUsu.Close;
    QryUpUsu.SQL.Clear;
    QryUpUsu.SQL.Text := 'UPDATE USUARIO ' +
                         '   SET NOME_MAQUINA = :NOME_MAQUINA ' +
                         ' WHERE ID_USUARIO = :ID_USUARIO';
    QryUpUsu.Params.ParamByName('NOME_MAQUINA').Value := sNomeMaquina;
    QryUpUsu.Params.ParamByName('ID_USUARIO').Value   := vgUsu_Id;
    QryUpUsu.ExecSQL;

    FreeAndNil(QryUpUsu);

    QryUpUsu := dmGerencial.CriarFDQuery(nil, vgConGER);

    QryUpUsu.Close;
    QryUpUsu.SQL.Clear;
    QryUpUsu.SQL.Text := 'UPDATE USUARIO ' +
                         '   SET NOME_MAQUINA = :NOME_MAQUINA ' +
                         ' WHERE ID_USUARIO = :ID_USUARIO';
    QryUpUsu.Params.ParamByName('NOME_MAQUINA').Value := sNomeMaquina;
    QryUpUsu.Params.ParamByName('ID_USUARIO').Value   := vgUsu_Id;
    QryUpUsu.ExecSQL;

    FreeAndNil(QryUpUsu);

    //CONFIGURACOES
    Close;
    Clear;
    Text := 'SELECT * FROM CONFIGURACAO';
    Open;

    vgConf_Atribuicao           := FieldByName('ATRIBUICAO').AsInteger;
    vgConf_Ambiente             := FieldByName('AMBIENTE').AsString;
    vgConf_CodServentia         := FieldByName('COD_SERVENTIA').AsString;
    vgConf_NomeServentia        := FieldByName('NOME_SERVENTIA_RELATORIO').AsString;
    vgConf_DiretorioDatabase    := FieldByName('DIR_DATABASE').AsString;
    vgConf_DiretorioImagens     := FieldByName('DIR_ARQ_IMAGENS').AsString;
    vgConf_DiretorioRelatorios  := FieldByName('DIR_ARQ_RELATORIOS').AsString;
    vgConf_DiretorioCarneLeao   := FieldByName('DIR_ARQ_CARNELEAO').AsString;
    vgConf_FlgExibeAtualizacao  := FieldByName('FLG_EXB_ATUALIZACAO').AsString;
    vgConf_FlgTrabalhaComissao  := FieldByName('FLG_COMISSAO').AsString;
    vgConf_FlgBloqueiaFechPagto := FieldByName('FLG_BLOQUEIO_FECHAMENTO').AsString;
    vgConf_FlgExpedienteSabado  := FieldByName('FLG_EXPEDIENTESABADO').AsString;
    vgConf_FlgExpedienteDomingo := FieldByName('FLG_EXPEDIENTEDOMINGO').AsString;
    vgConf_FlgExpedienteFeriado := FieldByName('FLG_EXPEDIENTEFERIADO').AsString;
    vgConf_FlgZerarNumOficio    := FieldByName('FLG_ZERARNUMOFICIO').AsString;
    vgConf_VersaoSync           := FieldByName('VERSAO_SYNC').AsString;
    vgConf_IntervaloSync        := FieldByName('INTERVALO_SYNC').AsInteger;
    vgConf_DataIniImportacao    := FieldByName('DATA_INI_IMPORTACAO').AsDateTime;
    vgConf_HoraFechamentoPadrao := FieldByName('HORA_FECHAMENTOPADRAO').AsString;
    vgConf_DiaFechamentoCaixa   := FieldByName('DIA_FECHAMENTOCAIXA').AsString;

    //SERVENTIA
    Close;
    Clear;
    Text := 'SELECT * ' +
            '  FROM SERVENTIA ' +
            ' WHERE COD_SERVENTIA = :CodServentia';
    Params.ParamByName('CodServentia').Value := vgConf_CodServentia;
    Open;

    vgSrvn_IdServentia       := FieldByName('ID_SERVENTIA').AsInteger;
    vgSrvn_CodServentia      := FieldByName('COD_SERVENTIA').AsString;
    vgSrvn_NomeServentia     := FieldByName('NOME_SERVENTIA').AsString;
    vgSrvn_EndServentia      := FieldByName('ENDERECO').AsString;
    vgSrvn_CidadeServentia   := FieldByName('CIDADE').AsString;
    vgSrvn_EstadoServentia   := FieldByName('UF').AsString;
    vgSrvn_CEPServentia      := FieldByName('CEP').AsString;
    vgSrvn_TelServentia      := FieldByName('TELEFONE').AsString;
    vgSrvn_EmailServentia    := FieldByName('EMAIL').AsString;
    vgSrvn_NomeTabeliao      := FieldByName('NOME_TABELIAO').AsString;
    vgSrvn_MatriculaTabeliao := FieldByName('MATRICULA_TABELIAO').AsString;
    vgSrvn_NomeTabeliaoSubst := FieldByName('NOME_SUBSTITUTO').AsString;
    vgSrvn_CNPJServentia     := FieldByName('CNPJ').AsString;
    vgSrvn_CodMunicipioServ  := FieldByName('COD_MUN').AsInteger;
    vgSrvn_TituloServentia   := FieldByName('TITULO').AsString;

    //PERMISSAO
    { Cadastro }
    //Categoria de Receitas
    vgPrm_ConsCatRec := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncCatRec  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdCatRec   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcCatRec  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Subcategoria de Receitas
    vgPrm_ConsSubcatRec := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncSubcatRec  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdSubcatRec   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcSubcatRec  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Receitas
    vgPrm_ConsRec := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncRec  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdRec   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcRec  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpRec  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpRec  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_RepRec  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Categoria de Despesas
    vgPrm_ConsCatDesp := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncCatDesp  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdCatDesp   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcCatDesp  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Subcategoria de Despesas
    vgPrm_ConsSubcatDesp := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncSubcatDesp  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdSubcatDesp   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcSubcatDesp  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Despesas
    vgPrm_ConsDesp := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncDesp  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdDesp   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcDesp  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpDesp  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpDesp  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_RepDesp  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_AutDesp  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Formas de Pagamento
    vgPrm_ConsFPagto := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncFPagto  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdFPagto   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcFPagto  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Funcionarios
    vgPrm_ConsFunc := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncFunc  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdFunc   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcFunc  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpFunc  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpFunc  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Clientes
    vgPrm_ConsCli := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncCli  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdCli   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcCli  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpCli  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpCli  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Fornecedores
    vgPrm_ConsForn := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncForn  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdForn   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcForn  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpForn  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpForn  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Produtos
    vgPrm_ConsProd := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncProd  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdProd   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcProd  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpProd  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpProd  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Servicos
    vgPrm_ConsServ := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncServ  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdServ   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcServ  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpServ  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpServ  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Vale
    vgPrm_ConsVale := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncVale  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdVale   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcVale  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpVale  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpVale  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Outras Despesas de Funcionario
    vgPrm_ConsODespF := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncODespF  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdODespF   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcODespF  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpODespF  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpODespF  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Fechamento de Salarios
    vgPrm_ConsFecSal   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_RealFecSal   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpFecSal    := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpFecSal    := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdRealFecSal := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdFecSal     := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcFecSal    := (Trim(vgUsu_FlgSuporte) = 'S');

    //Fechamento de Comissoes
    vgPrm_ConsFecCom := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_RealFecCom := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpFecCom  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpFecCom  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Estoque
    vgPrm_ConsEst := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_AceEst  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpEst  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpEst  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Flutuantes
    vgPrm_BxLanc   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ConsDepo := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncDepo  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdDepo   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcDepo  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpDepo  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpDepo  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Naturezas
    vgPrm_ConsNat := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncNat  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdNat   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcNat  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Feriados
    vgPrm_ConsFer := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncFer  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdFer   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcFer  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpFer  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpFer  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Edicao de Lancamentos Pagos
    vgPrm_EdLancPago := (Trim(vgUsu_FlgSuporte) = 'S');

    //Geracao Carne Leao
    vgPrm_ConsCLeao := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncCLeao  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdCLeao   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcCLeao  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpCLeao  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpCLeao  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Relacao de Carne Leao
    vgPrm_ConsRelacCL := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ConfRelacCL := (Trim(vgUsu_FlgSuporte) = 'S');

    //Cargos
    vgPrm_ConsCrg := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncCrg  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdCrg   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcCrg  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Vinculo Lancamento-Deposito
    vgPrm_ExpVincDepo := (Trim(vgUsu_FlgSuporte) = 'S');

    { Caixa }
    //Abertura
    vgPrm_ConsAbCx := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncAbCx  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdAbCx   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcAbCx  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpAbCx  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpAbCx  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Fechamento
    vgPrm_ConsFecCx := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncFecCx  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdFecCx   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcFecCx  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpFecCx  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpFecCx  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Movimento de Caixa
    vgPrm_ConsMovCx := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncMovCx  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdMovCx   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcMovCx  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpMovCx  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpMovCx  := (Trim(vgUsu_FlgSuporte) = 'S');

    { Oficios }
    //Recebidos
    vgPrm_ConsOficRec := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncOficRec  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdOficRec   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcOficRec  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpOficRec  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpOficRec  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Enviados
    vgPrm_ConsOficEnv := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncOficEnv  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdOficEnv   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcOficEnv  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpOficEnv  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpOficEnv  := (Trim(vgUsu_FlgSuporte) = 'S');

    { Folhas }
    //Lote de Folhas de Seguranca
    vgPrm_ConsLtFlsSeg := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncLtFlsSeg  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdLtFlsSeg   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcLtFlsSeg  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpLtFlsSeg  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpLtFlsSeg  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Lote de Folhas RCPN
    vgPrm_ConsLtFlsRCPN := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncLtFlsRCPN  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdLtFlsRCPN   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcLtFlsRCPN  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpLtFlsRCPN  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpLtFlsRCPN  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Folhas de Seguranca
    vgPrm_ConsFlsSeg := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdFlsSeg   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcFlsSeg  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpFlsSeg  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpFlsSeg  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Folhas RCPN
    vgPrm_ConsFlsRCPN := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdFlsRCPN   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcFlsRCPN  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpFlsRCPN  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpFlsRCPN  := (Trim(vgUsu_FlgSuporte) = 'S');

    { Etiquetas }
    //Lote de Etiquetas
    vgPrm_ConsLtEtq := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncLtEtq  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdLtEtq   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcLtEtq  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpLtEtq  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpLtEtq  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Etiquetas
    vgPrm_ConsEtq := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdEtq   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcEtq  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpEtq  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpEtq  := (Trim(vgUsu_FlgSuporte) = 'S');

    { Relatorios }
    //Receitas x Despesas
    vgPrm_ConsRelRecDesp := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpRelRecDesp  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpRelRecDesp  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Recibos x Selos
    vgPrm_ConsRelRecbSelo := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpRelRecbSelo  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpRelRecbSelo  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Faturamento Mensal
    vgPrm_ConsRelFatMsl := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpRelFatMsl  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpRelFatMsl  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Inadimplentes
    vgPrm_ConsRelInad := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpRelInad  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpRelInad  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Extrato de Conta
    vgPrm_ConsRelExtCnt := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpRelExtCnt  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpRelExtCnt  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Movimento de Caixa
    vgPrm_ConsRelMovCx := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpRelMovCx  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpRelMovCx  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Recibos
    vgPrm_ConsRelRcb := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpRelRcb  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpRelRcb  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Depositos Flutuantes
    vgPrm_ConsRelDepFlu := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpRelDepFlu  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpRelDepFlu  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Depositos de Protestos para Repasse
    vgPrm_ConsRelRep := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpRelRep  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpRelRep  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Despesas com Colaboradores
    vgPrm_ConsRelDespCol := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpRelDespCol  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpRelDespCol  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Atos
    vgPrm_ConsRelAtos := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpRelAtos  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpRelAtos  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Fechamento de Caixa por FP
    vgPrm_ConsRelFechFP := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpRelFechFP  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpRelFechFP  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Fechamento de Caixa por TA
    vgPrm_ConsRelFechTA := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpRelFechTA  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpRelFechTA  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Diferencas do Fechamento de Caixa
    vgPrm_ConsRelDifFechCx := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpRelDifFechCx  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpRelDifFechCx  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Receitas e Despesas para o Imposto de Renda
    vgPrm_ConsRelRecDespIR := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpRelRecDespIR  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpRelRecDespIR  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Recibos Cancelados
    vgPrm_ConsRelCancel := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpRelCancel  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpRelCancel  := (Trim(vgUsu_FlgSuporte) = 'S');

    { Configuracoes }
    //Usuarios
    vgPrm_ConsUsu   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncUsu    := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdUsu     := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcUsu    := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpUsu    := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpUsu    := (Trim(vgUsu_FlgSuporte) = 'S');

    //Log de Usuario
    vgPrm_ConsLogUsu := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ImpLogUsu  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExpLogUsu  := (Trim(vgUsu_FlgSuporte) = 'S');

    //Sistema
    vgPrm_ConsConfigSis := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_RealConfigSis := (Trim(vgUsu_FlgSuporte) = 'S');

    //Permissoes
    vgPrm_ConsPerm := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ConfPerm := (Trim(vgUsu_FlgSuporte) = 'S');

    //Atribuicoes da Serventia
    vgPrm_ConsAtribServ := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_RealAtribServ := (Trim(vgUsu_FlgSuporte) = 'S');

    //Remetente e Destinatario de Email
    vgPrm_ConsRemDest := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_IncRemDest  := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_EdRemDest   := (Trim(vgUsu_FlgSuporte) = 'S');
    vgPrm_ExcRemDest  := (Trim(vgUsu_FlgSuporte) = 'S');

    if Trim(vgUsu_FlgSuporte) = 'N' then
    begin
      Close;
      Clear;
      Text := 'SELECT * ' +
              '  FROM USUARIO_PERMISSAO ' +
              ' WHERE ID_USUARIO = :IdUsuario';
      Params.ParamByName('IdUsuario').Value := vgUsu_Id;
      Open;

      if QryVars.RecordCount = 0 then  //Primeiro login
      begin
        if vgUsu_FlgMaster = 'S' then  //Usuario Master
        begin
          //Autorizar tudo
          sQueryPerm := 'SELECT * ' +
                        '  FROM PERMISSAO ' +
                        ' WHERE ';

          sModulos := '';

          if vgMod_Financeiro then
            sModulos := 'ID_MODULO_FK = 2 ';

          if vgMod_Oficios then
            sModulos := IfThen((Trim(sModulos) = ''), 'ID_MODULO_FK = 3 ', (sModulos + 'OR ID_MODULO_FK = 3 '));

          if vgMod_Folhas then
            sModulos := IfThen((Trim(sModulos) = ''), 'ID_MODULO_FK = 4 ', (sModulos + 'OR ID_MODULO_FK = 4 '));

          if vgMod_Etiquetas then
            sModulos := IfThen((Trim(sModulos) = ''), 'ID_MODULO_FK = 5 ', (sModulos + 'OR ID_MODULO_FK = 5 '));

          if vgMod_Relatorios then
            sModulos := IfThen((Trim(sModulos) = ''), 'ID_MODULO_FK = 50 ', (sModulos + 'OR ID_MODULO_FK = 50 '));

          if vgMod_Configuracoes then
            sModulos := IfThen((Trim(sModulos) = ''), 'ID_MODULO_FK = 80 ', (sModulos + 'OR ID_MODULO_FK = 80 '));

          if Trim(sModulos) = '' then
            sModulos := '(1 = 0) ';

          sQueryPerm := (sQueryPerm + sModulos + 'ORDER BY ID_MODULO_FK, ID_PERMISSAO');

          dmGerencial.DefinirPermissoesMaster(sQueryPerm);

          Close;
          Clear;
          Text := 'SELECT * ' +
                  '  FROM USUARIO_PERMISSAO ' +
                  ' WHERE ID_USUARIO = :IdUsuario';
          Params.ParamByName('IdUsuario').Value := vgUsu_Id;
          Open;
        end
        else
        begin  //Usuario padrao
          //Financeiro
          for j := 0 to 65 do
          begin
            QryInPerm.Close;
            QryInPerm.SQL.Clear;
            QryInPerm.SQL.Text := 'INSERT INTO USUARIO_PERMISSAO (ID_USUARIO_PERMISSAO, ' +
                                  '                               ID_USUARIO, ' +
                                  '                               COD_PERMISSAO) ' +
                                  '                       VALUES (:ID_USUARIO_PERMISSAO, ' +
                                  '                               :ID_USUARIO, ' +
                                  '                               :COD_PERMISSAO)';
            QryInPerm.Params.ParamByName('ID_USUARIO_PERMISSAO').Value := ProximoId('ID_USUARIO_PERMISSAO', 'USUARIO_PERMISSAO');
            QryInPerm.Params.ParamByName('ID_USUARIO').Value           := vgUsu_Id;
            QryInPerm.Params.ParamByName('COD_PERMISSAO').Value        := iPermF[j];
            QryInPerm.ExecSQL;
          end;

          //Oficios
          if vgMod_Oficios then
          begin
            for j := 0 to 7 do
            begin
              QryInPerm.Close;
              QryInPerm.SQL.Clear;
              QryInPerm.SQL.Text := 'INSERT INTO USUARIO_PERMISSAO (ID_USUARIO_PERMISSAO, ' +
                                    '                               ID_USUARIO, ' +
                                    '                               COD_PERMISSAO) ' +
                                    '                       VALUES (:ID_USUARIO_PERMISSAO, ' +
                                    '                               :ID_USUARIO, ' +
                                    '                               :COD_PERMISSAO)';
              QryInPerm.Params.ParamByName('ID_USUARIO_PERMISSAO').Value := ProximoId('ID_USUARIO_PERMISSAO', 'USUARIO_PERMISSAO');
              QryInPerm.Params.ParamByName('ID_USUARIO').Value           := vgUsu_Id;
              QryInPerm.Params.ParamByName('COD_PERMISSAO').Value        := iPermO[j];
              QryInPerm.ExecSQL;
            end;
          end;

          //Relatorios
          for j := 0 to 36 do
          begin
            QryInPerm.Close;
            QryInPerm.SQL.Clear;
            QryInPerm.SQL.Text := 'INSERT INTO USUARIO_PERMISSAO (ID_USUARIO_PERMISSAO, ' +
                                  '                               ID_USUARIO, ' +
                                  '                               COD_PERMISSAO) ' +
                                  '                       VALUES (:ID_USUARIO_PERMISSAO, ' +
                                  '                               :ID_USUARIO, ' +
                                  '                               :COD_PERMISSAO)';
            QryInPerm.Params.ParamByName('ID_USUARIO_PERMISSAO').Value := ProximoId('ID_USUARIO_PERMISSAO', 'USUARIO_PERMISSAO');
            QryInPerm.Params.ParamByName('ID_USUARIO').Value           := vgUsu_Id;
            QryInPerm.Params.ParamByName('COD_PERMISSAO').Value        := iPermR[j];
            QryInPerm.ExecSQL;
          end;

          Close;
          Clear;
          Text := 'SELECT * ' +
                  '  FROM USUARIO_PERMISSAO ' +
                  ' WHERE ID_USUARIO = :IdUsuario';
          Params.ParamByName('IdUsuario').Value := vgUsu_Id;
          Open;
        end;
      end
      else
      begin
        if vgUsu_FlgMaster = 'S' then  //Usuario Master
        begin
          QryUpPerm.Close;
          QryUpPerm.SQL.Clear;
          QryUpPerm.SQL.Text := 'SELECT P.* ' +
                                '  FROM PERMISSAO P ' +
                                '  LEFT JOIN USUARIO_PERMISSAO UP ' +
                                '    ON P.COD_PERMISSAO = UP.COD_PERMISSAO ' +
                                ' WHERE UP.COD_PERMISSAO IS NULL';

          sModulos := '';

          if vgMod_Financeiro then
            sModulos := 'P.ID_MODULO_FK = 2 ';

          if vgMod_Oficios then
            sModulos := IfThen((Trim(sModulos) = ''), 'P.ID_MODULO_FK = 3 ', (sModulos + 'OR P.ID_MODULO_FK = 3 '));

          if vgMod_Folhas then
            sModulos := IfThen((Trim(sModulos) = ''), 'P.ID_MODULO_FK = 4 ', (sModulos + 'OR P.ID_MODULO_FK = 4 '));

          if vgMod_Etiquetas then
            sModulos := IfThen((Trim(sModulos) = ''), 'P.ID_MODULO_FK = 5 ', (sModulos + 'OR P.ID_MODULO_FK = 5 '));

          if vgMod_Relatorios then
            sModulos := IfThen((Trim(sModulos) = ''), 'P.ID_MODULO_FK = 50 ', (sModulos + 'OR P.ID_MODULO_FK = 50 '));

          if vgMod_Configuracoes then
            sModulos := IfThen((Trim(sModulos) = ''), 'P.ID_MODULO_FK = 80 ', (sModulos + 'OR P.ID_MODULO_FK = 80 '));

          if Trim(sModulos) = '' then
            sModulos := '(1 = 0) ';

          QryUpPerm.SQL.Add(' AND (' + sModulos + ') ORDER BY P.ID_MODULO_FK, P.ID_PERMISSAO');

          QryUpPerm.Open;

          if QryUpPerm.RecordCount > 0 then
          begin
            dmGerencial.DefinirPermissoesMaster(QryUpPerm.SQL.Text);

            Close;
            Clear;
            Text := 'SELECT * ' +
                    '  FROM USUARIO_PERMISSAO ' +
                    ' WHERE ID_USUARIO = :IdUsuario';
            Params.ParamByName('IdUsuario').Value := vgUsu_Id;
            Open;
          end;
        end;
      end;

      QryVars.First;

      while not QryVars.Eof do
      begin
        case FieldByName('COD_PERMISSAO').AsInteger of
          { FINANCEIRO - 2000 }
          //Categoria de Receitas
          2001: vgPrm_ConsCatRec := True;
          2002: vgPrm_IncCatRec  := True;
          2003: vgPrm_EdCatRec   := True;
          2004: vgPrm_ExcCatRec  := True;

          //Subcategoria de Receitas
          2005: vgPrm_ConsSubcatRec := True;
          2006: vgPrm_IncSubcatRec  := True;
          2007: vgPrm_EdSubcatRec   := True;
          2008: vgPrm_ExcSubcatRec  := True;

          //Receitas
          2009: vgPrm_ConsRec := True;
          2010: vgPrm_IncRec  := True;
          2011: vgPrm_EdRec   := True;
          2012: vgPrm_ExcRec  := True;
          2013: vgPrm_ImpRec  := True;
          2014: vgPrm_ExpRec  := True;
          2015: vgPrm_RepRec  := True;

          //Categoria de Despesas
          2016: vgPrm_ConsCatDesp := True;
          2017: vgPrm_IncCatDesp  := True;
          2018: vgPrm_EdCatDesp   := True;
          2019: vgPrm_ExcCatDesp  := True;

          //Subcategoria de Despesas
          2020: vgPrm_ConsSubcatDesp := True;
          2021: vgPrm_IncSubcatDesp  := True;
          2022: vgPrm_EdSubcatDesp   := True;
          2023: vgPrm_ExcSubcatDesp  := True;

          //Despesas
          2024: vgPrm_ConsDesp := True;
          2025: vgPrm_IncDesp  := True;
          2026: vgPrm_EdDesp   := True;
          2027: vgPrm_ExcDesp  := True;
          2028: vgPrm_ImpDesp  := True;
          2029: vgPrm_ExpDesp  := True;
          2030: vgPrm_RepDesp  := True;
          2031: vgPrm_AutDesp  := True;

          //Formas de Pagamento
          2032: vgPrm_ConsFPagto := True;
          2033: vgPrm_IncFPagto  := True;
          2034: vgPrm_EdFPagto   := True;
          2035: vgPrm_ExcFPagto  := True;

          //Funcionarios
          2036: vgPrm_ConsFunc := True;
          2037: vgPrm_IncFunc  := True;
          2038: vgPrm_EdFunc   := True;
          2039: vgPrm_ExcFunc  := True;
          2040: vgPrm_ImpFunc  := True;
          2041: vgPrm_ExpFunc  := True;

          //Clientes
          2042: vgPrm_ConsCli := True;
          2043: vgPrm_IncCli  := True;
          2044: vgPrm_EdCli   := True;
          2045: vgPrm_ExcCli  := True;
          2046: vgPrm_ImpCli  := True;
          2047: vgPrm_ExpCli  := True;

          //Fornecedores
          2048: vgPrm_ConsForn := True;
          2049: vgPrm_IncForn  := True;
          2050: vgPrm_EdForn   := True;
          2051: vgPrm_ExcForn  := True;
          2052: vgPrm_ImpForn  := True;
          2053: vgPrm_ExpForn  := True;

          //Bancos
          2054: vgPrm_ConsProd := True;
          2055: vgPrm_IncProd  := True;
          2056: vgPrm_EdProd   := True;
          2057: vgPrm_ExcProd  := True;
          2058: vgPrm_ImpProd  := True;
          2059: vgPrm_ExpProd  := True;

          //Servicos
          2060: vgPrm_ConsServ := True;
          2061: vgPrm_IncServ  := True;
          2062: vgPrm_EdServ   := True;
          2063: vgPrm_ExcServ  := True;
          2064: vgPrm_ImpServ  := True;
          2065: vgPrm_ExpServ  := True;

          //Abertura de Caixa
          2066: vgPrm_ConsAbCx := True;
          2067: vgPrm_IncAbCx  := True;
          2123: vgPrm_EdAbCx   := True;
          2124: vgPrm_ExcAbCx  := True;
          2125: vgPrm_ImpAbCx  := True;
          2126: vgPrm_ExpAbCx  := True;

          //Fechamento de Caixa
          2068: vgPrm_ConsFecCx := True;
          2069: vgPrm_IncFecCx  := True;
          2127: vgPrm_EdFecCx   := True;
          2128: vgPrm_ExcFecCx  := True;
          2129: vgPrm_ImpFecCx  := True;
          2130: vgPrm_ExpFecCx  := True;

          //Movimento de Caixa
          2070: vgPrm_ConsMovCx := True;
          2071: vgPrm_IncMovCx  := True;
          2072: vgPrm_EdMovCx   := True;
          2073: vgPrm_ExcMovCx  := True;
          2074: vgPrm_ImpMovCx  := True;
          2075: vgPrm_ExpMovCx  := True;

          //Vale
          2076: vgPrm_ConsVale := True;
          2077: vgPrm_IncVale  := True;
          2078: vgPrm_EdVale   := True;
          2079: vgPrm_ExcVale  := True;
          2080: vgPrm_ImpVale  := True;
          2081: vgPrm_ExpVale  := True;

          //Fechamento de Salarios
          2082: vgPrm_ConsFecSal   := True;
          2083: vgPrm_RealFecSal   := True;
          2084: vgPrm_ImpFecSal    := True;
          2085: vgPrm_ExpFecSal    := True;
          2120: vgPrm_EdRealFecSal := True;
          2121: vgPrm_EdFecSal     := True;
          2122: vgPrm_ExcFecSal    := True;

          //Outras Despesas de Funcionario
          2086: vgPrm_ConsODespF := True;
          2087: vgPrm_IncODespF  := True;
          2088: vgPrm_EdODespF   := True;
          2089: vgPrm_ExcODespF  := True;
          2090: vgPrm_ImpODespF  := True;
          2091: vgPrm_ExpODespF  := True;

          //Estoque
          2092: vgPrm_ConsEst := True;
          2093: vgPrm_AceEst  := True;
          2094: vgPrm_ImpEst  := True;
          2095: vgPrm_ExpEst  := True;

          //Fechamento de Comissoes
          2096: vgPrm_ConsFecCom := True;
          2097: vgPrm_RealFecCom := True;
          2098: vgPrm_ImpFecCom  := True;
          2099: vgPrm_ExpFecCom  := True;

          //Flutuantes
          2100: vgPrm_BxLanc   := True;
          2101: vgPrm_ConsDepo := True;
          2102: vgPrm_IncDepo  := True;
          2103: vgPrm_EdDepo   := True;
          2104: vgPrm_ExcDepo  := True;
          2105: vgPrm_ImpDepo  := True;
          2106: vgPrm_ExpDepo  := True;

          //Natureza
          2107: vgPrm_ConsNat := True;
          2108: vgPrm_IncNat  := True;
          2109: vgPrm_EdNat   := True;
          2110: vgPrm_ExcNat  := True;

          //Edicao de Lancamentos Pagos
          2111: vgPrm_EdLancPago := True;

          //Geracao Carne Leao
          2112: vgPrm_ConsCLeao := True;
          2113: vgPrm_IncCLeao  := True;
          2114: vgPrm_EdCLeao   := True;
          2115: vgPrm_ExcCLeao  := True;
          2116: vgPrm_ImpCLeao  := True;
          2117: vgPrm_ExpCLeao  := True;

          //Relacao de Carne Leao
          2118: vgPrm_ConsRelacCL := True;
          2119: vgPrm_ConfRelacCL := True;

          //Feriados
          2131: vgPrm_ConsFer := True;
          2132: vgPrm_IncFer  := True;
          2133: vgPrm_EdFer   := True;
          2134: vgPrm_ExcFer  := True;
          2135: vgPrm_ImpFer  := True;
          2136: vgPrm_ExpFer  := True;

          //Cargos
          2137: vgPrm_ConsCrg := True;
          2138: vgPrm_IncCrg  := True;
          2139: vgPrm_EdCrg   := True;
          2140: vgPrm_ExcCrg  := True;

          //Vinculo lancamento-Deposito
          2141: vgPrm_ExpVincDepo := True;

          { OFICIOS - 3000 }
          //Recebidos
          3001: vgPrm_ConsOficRec := True;
          3002: vgPrm_IncOficRec  := True;
          3003: vgPrm_EdOficRec   := True;
          3004: vgPrm_ExcOficRec  := True;
          3005: vgPrm_ImpOficRec  := True;
          3006: vgPrm_ExpOficRec  := True;

          //Enviados
          3007: vgPrm_ConsOficEnv := True;
          3008: vgPrm_IncOficEnv  := True;
          3009: vgPrm_EdOficEnv   := True;
          3010: vgPrm_ExcOficEnv  := True;
          3011: vgPrm_ImpOficEnv  := True;
          3012: vgPrm_ExpOficEnv  := True;

          { FOLHAS - 4000 }
          //Lote de Folhas de Seguranca
          4001: vgPrm_ConsLtFlsSeg := True;
          4002: vgPrm_IncLtFlsSeg  := True;
          4003: vgPrm_EdLtFlsSeg   := True;
          4004: vgPrm_ExcLtFlsSeg  := True;
          4005: vgPrm_ImpLtFlsSeg  := True;
          4006: vgPrm_ExpLtFlsSeg  := True;

          //Lote de Folhas RCPN
          4007: vgPrm_ConsLtFlsRCPN := True;
          4008: vgPrm_IncLtFlsRCPN  := True;
          4009: vgPrm_EdLtFlsRCPN   := True;
          4010: vgPrm_ExcLtFlsRCPN  := True;
          4011: vgPrm_ImpLtFlsRCPN  := True;
          4012: vgPrm_ExpLtFlsRCPN  := True;

          //Folhas de Seguranca
          4013: vgPrm_ConsFlsSeg := True;
          4014: vgPrm_EdFlsSeg   := True;
          4015: vgPrm_ExcFlsSeg  := True;
          4016: vgPrm_ImpFlsSeg  := True;
          4017: vgPrm_ExpFlsSeg  := True;

          //Folhas RCPN
          4018: vgPrm_ConsFlsRCPN := True;
          4019: vgPrm_EdFlsRCPN   := True;
          4020: vgPrm_ExcFlsRCPN  := True;
          4021: vgPrm_ImpFlsRCPN  := True;
          4022: vgPrm_ExpFlsRCPN  := True;

          { ETIQUETAS - 5000 }
          //Lote de Etiquetas
          5001: vgPrm_ConsLtEtq := True;
          5002: vgPrm_IncLtEtq  := True;
          5003: vgPrm_EdLtEtq   := True;
          5004: vgPrm_ExcLtEtq  := True;
          5005: vgPrm_ImpLtEtq  := True;
          5006: vgPrm_ExpLtEtq  := True;

          //Etiquetas
          5007: vgPrm_ConsEtq := True;
          5008: vgPrm_EdEtq   := True;
          5009: vgPrm_ExcEtq  := True;
          5010: vgPrm_ImpEtq  := True;
          5011: vgPrm_ExpEtq  := True;

          { RELATORIOS - 50000 }
          //Receitas x Despesas
          50001: vgPrm_ConsRelRecDesp := True;
          50002: vgPrm_ImpRelRecDesp  := True;
          50003: vgPrm_ExpRelRecDesp  := True;

          //Recibos x Selos
          50004: vgPrm_ConsRelRecbSelo := True;
          50005: vgPrm_ImpRelRecbSelo  := True;
          50006: vgPrm_ExpRelRecbSelo  := True;

          //Faturamento Mensal
          50007: vgPrm_ConsRelFatMsl := True;
          50008: vgPrm_ImpRelFatMsl  := True;
          50009: vgPrm_ExpRelFatMsl  := True;

          //Inadimplentes
          50010: vgPrm_ConsRelInad := True;
          50011: vgPrm_ImpRelInad  := True;
          50012: vgPrm_ExpRelInad  := True;

          //Movimento de Caixa
          50013: vgPrm_ConsRelMovCx := True;
          50014: vgPrm_ImpRelMovCx  := True;
          50015: vgPrm_ExpRelMovCx  := True;

          //Recibos
          50016: vgPrm_ConsRelRcb := True;
          50017: vgPrm_ImpRelRcb  := True;
          50018: vgPrm_ExpRelRcb  := True;

          //Depositos Flutuantes
          50019: vgPrm_ConsRelDepFlu := True;
          50020: vgPrm_ImpRelDepFlu  := True;
          50021: vgPrm_ExpRelDepFlu  := True;

          //Depositos de Protestos para Repasse
          50022: vgPrm_ConsRelRep := True;
          50023: vgPrm_ImpRelRep  := True;
          50024: vgPrm_ExpRelRep  := True;

          //Despesas com Colaboradores
          50025: vgPrm_ConsRelDespCol := True;
          50026: vgPrm_ImpRelDespCol  := True;
          50027: vgPrm_ExpRelDespCol  := True;

          //Atos
          50028: vgPrm_ConsRelAtos := True;
          50029: vgPrm_ImpRelAtos  := True;
          50030: vgPrm_ExpRelAtos  := True;

          //Fechamento de Caixa por FP
          50031: vgPrm_ConsRelFechFP := True;
          50032: vgPrm_ImpRelFechFP  := True;
          50033: vgPrm_ExpRelFechFP  := True;

          //Fechamento de Caixa por TA
          50034: vgPrm_ConsRelFechTA := True;
          50035: vgPrm_ImpRelFechTA  := True;
          50036: vgPrm_ExpRelFechTA  := True;

          //Extrato de Conta
          50037: vgPrm_ConsRelExtCnt := True;
          50038: vgPrm_ImpRelExtCnt  := True;
          50039: vgPrm_ExpRelExtCnt  := True;

          //Diferencas do Fechamento de Caixa
          50040: vgPrm_ConsRelDifFechCx := True;
          50041: vgPrm_ImpRelDifFechCx  := True;
          50042: vgPrm_ExpRelDifFechCx  := True;

          //Receitas e Despesas para o Imposto de Renda
          50043: vgPrm_ConsRelRecDespIR := True;
          50044: vgPrm_ImpRelRecDespIR  := True;
          50045: vgPrm_ExpRelRecDespIR  := True;

          //Recibos Cancelados
          50046: vgPrm_ConsRelCancel := True;
          50047: vgPrm_ImpRelCancel  := True;
          50048: vgPrm_ExpRelCancel  := True;

          { CONFIGURACOES - 80000 }
          //Usuarios
          80001: vgPrm_ConsUsu   := True;
          80002: vgPrm_IncUsu    := True;
          80003: vgPrm_EdUsu     := True;
          80004: vgPrm_ExcUsu    := True;
          80005: vgPrm_ImpUsu    := True;
          80006: vgPrm_ExpUsu    := True;

          //Log de Usuario
          80007: vgPrm_ConsLogUsu := True;
          80008: vgPrm_ImpLogUsu  := True;
          80009: vgPrm_ExpLogUsu  := True;

          //Sistema
          80010: vgPrm_ConsConfigSis := True;
          80011: vgPrm_RealConfigSis := True;

          //Permissoes
          80012: vgPrm_ConsPerm := True;
          80013: vgPrm_ConfPerm := True;

          //Atribuicoes da Serventia
          80014: vgPrm_ConsAtribServ := True;
          80015: vgPrm_RealAtribServ := True;

          //Remetente e Destinatario de Email
          80016: vgPrm_ConsRemDest := True;
          80017: vgPrm_IncRemDest  := True;
          80018: vgPrm_EdRemDest   := True;
          80019: vgPrm_ExcRemDest  := True;
        end;

        Next;
      end;
    end;
  end;

  dmPrincipal.VerificarSituacaoCaixa;

  FreeAndNil(QryVars);
  FreeAndNil(QryUpPerm);
  FreeAndNil(QryInPerm);
end;

procedure TBibliotecaSistema.DeterminarVersaoSistema;
var
  qryAux: TFDQuery;
begin
  qryAux := dmGerencial.CriarFDQuery(nil, vgConGERAux);

  if (dmPrincipal.qryAtualizacao.FieldByName('ULT_VERSAO_SISTEMA').IsNull) or
    (Trim(dmPrincipal.qryAtualizacao.FieldByName('ULT_VERSAO_SISTEMA').AsString) <> vgAtS_VersaoSistema) then
  begin
    if Trim(vgConf_FlgExibeAtualizacao) = 'S' then
    begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Text := 'UPDATE USUARIO ' +
                         '   SET FLG_VISUALIZOU_ATU = ' + QuotedStr('N') +
                         ' WHERE FLG_ATIVO = ' + QuotedStr('S');
      qryAux.ExecSQL;
    end;

    VS.GravarNovaVersaoSistema;
  end
  else
  begin
    //ATUALIZACAO DO SISTEMA
    vgAtS_IdAtualizacaoSist   := dmPrincipal.qryAtualizacao.FieldByName('ID_ATUALIZACAOSIST').AsInteger;
    vgAtS_DataAtualizacaoSist := dmPrincipal.qryAtualizacao.FieldByName('DATA_ATUALIZACAOSIST').AsDateTime;
    vgAtS_VersaoSistema       := dmPrincipal.qryAtualizacao.FieldByName('ULT_VERSAO_SISTEMA').AsString;
  end;

  dmPrincipal.qryAtualizacao.Close;

  FreeAndNil(qryAux);
end;

procedure TBibliotecaSistema.GravarUsuarioLog(Modulo: Integer = 0; Selo: String = '';
  Observacao: String = ''; Id: Integer = 0; Tabela: String = '');
var
  QryLog: TFDQuery;
begin
  { MODULO
    1  - Atalho / 2  - Financeiro / 3  - Oficios / 4 - Folhas de Seguranca / 5 - Etiquetas /
    50  - Relatorios / 80 - Configuracoes / 90  - Suporte / 100 - Login / 200 - LOTE DE SEGURAN�A }

  QryLog := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryLog, SQL do
  begin
    Close;
    Clear;
    Text := 'INSERT INTO USUARIO_LOG (ID_USUARIO_LOG, ID_USUARIO, ID_MODULO_FK, ' +
            '                         TIPO_LANCAMENTO, CODIGO_LAN, ANO_LAN, ID_ORIGEM, ' +
            '                         TABELA_ORIGEM, TIPO_ACAO, OBS_USUARIO_LOG) ' +
            '                 VALUES (:ID_USUARIO_LOG, :ID_USUARIO, :ID_MODULO, ' +
            '                         :TIPO_LANCAMENTO, :CODIGO_LAN, :ANO_LAN, :ID_ORIGEM, ' +
            '                         :TABELA_ORIGEM, :TIPO_ACAO, :OBS_USUARIO_LOG)';

    Params.ParamByName('ID_USUARIO_LOG').Value := ProximoId('ID_USUARIO_LOG', 'USUARIO_LOG');
    Params.ParamByName('ID_USUARIO').Value     := vgUsu_Id;

    if Modulo = 0 then
      Params.ParamByName('ID_MODULO').Value := Null
    else
      Params.ParamByName('ID_MODULO').Value := Modulo;

    if Trim(vgLanc_TpLanc) = '' then
      Params.ParamByName('TIPO_LANCAMENTO').Value := Null
    else
      Params.ParamByName('TIPO_LANCAMENTO').Value := vgLanc_TpLanc;

    if vgLanc_Codigo = 0 then
      Params.ParamByName('CODIGO_LAN').Value := Null
    else
      Params.ParamByName('CODIGO_LAN').Value := vgLanc_Codigo;

    if vgLanc_Ano = 0 then
      Params.ParamByName('ANO_LAN').Value := Null
    else
      Params.ParamByName('ANO_LAN').Value := vgLanc_Ano;

    if Id = 0 then
      Params.ParamByName('ID_ORIGEM').Value := Null
    else
      Params.ParamByName('ID_ORIGEM').Value := Id;

    if Trim(Tabela) = '' then
      Params.ParamByName('TABELA_ORIGEM').Value := Null
    else
      Params.ParamByName('TABELA_ORIGEM').Value := Trim(Tabela);

    if vgOperacao = VAZIO then  //Vazio
      Params.ParamByName('TIPO_ACAO').Value := Null
    else if vgOperacao = I then  //Inclusao
      Params.ParamByName('TIPO_ACAO').Value := 'I'
    else if vgOperacao = E then  //Edicao
      Params.ParamByName('TIPO_ACAO').Value := 'E'
    else if vgOperacao = X then  //Exclusao
      Params.ParamByName('TIPO_ACAO').Value := 'X'
    else if vgOperacao = C then  //Consulta
      Params.ParamByName('TIPO_ACAO').Value := 'C'
    else if vgOperacao = P then  //Impressao
      Params.ParamByName('TIPO_ACAO').Value := 'P'
    else if vgOperacao = T then  //Exportacao
      Params.ParamByName('TIPO_ACAO').Value := 'T';

    if Trim(Observacao) = '' then
      Params.ParamByName('OBS_USUARIO_LOG').Value := Null
    else
      Params.ParamByName('OBS_USUARIO_LOG').Value := Trim(Observacao);

    ExecSQL;
  end;

  FreeAndNil(QryLog);
end;

procedure TBibliotecaSistema.MontarComboBox(Combo: TComboBox; Campo, Tabela,
  Where, Parametro, OrderBy: String);
var
  Qry: TFDQuery;
begin
  if (Tabela = 'MUNICIPIO') or
    (Tabela = 'ESTADO') or
    (Tabela = 'PAIS') or
    (Tabela = 'FUNCIONARIO') or
    (Tabela = 'FUNCIONARIO_COMISSAO') or
    (Tabela = 'OCUPACAO') or
    (Tabela = 'SERV') or
    (Tabela = 'CARGO') or
    (Tabela = 'ESTADOCIVIL') or
    (Tabela = 'ESCOLARIDADE') or
    (Tabela = 'TIPOFILIACAO') or
    (Tabela = 'TIPODOCUMENTO') or
    (Tabela = 'JUSTIFICATIVA_AUSENC_CPF') or
    (Tabela = 'BANCO') or
    (Tabela = 'SELO_CCT') or
    (Tabela = 'SERIE') or
    (Tabela = 'COMPRA') or
    (Tabela = 'USUARIO') or
    (Tabela = 'PESSOA') or
    (Tabela = 'PESSOADOCUMENTO') or
    (Tabela = 'PESSOAFILIACAO') or
    (Tabela = 'PESSOAGEMEO') or
    (Tabela = 'HISTORICO_PESSOA')or
    (Tabela = 'SISTEMA') or
    (Tabela = 'TIPO_ATO') then
    Qry := dmGerencial.CriarFDQuery(nil, vgConGER)
  else
    Qry := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  Qry.Close;
  Qry.SQL.Clear;

  Qry.SQL.Text := 'SELECT ' + Campo +
                  '  FROM ' + Tabela;

  if Trim(Parametro) <> '' then
  begin
    Qry.SQL.Add(' WHERE ' + Where);
    Qry.Params[0].Value := Parametro;
  end;

  Qry.SQL.Add(' ORDER BY ' + OrderBy);

  Qry.Open;

  Combo.Items.Clear;

  Qry.First;

  while not Qry.Eof do
  begin
    Combo.Items.Add(Qry.FieldByName(Campo).AsString);
    Qry.Next;
  end;

  Combo.Update;
end;

function TBibliotecaSistema.ProximoId(Campo, Tabela: String): Integer;
var
  Qry: TFDQuery;
  Id: Integer;
begin
  if (Tabela = 'MUNICIPIO') or
    (Tabela = 'ESTADO') or
    (Tabela = 'PAIS') or
    (Tabela = 'FUNCIONARIO') or
    (Tabela = 'FUNCIONARIO_COMISSAO') or
    (Tabela = 'FERIADO') or
    (Tabela = 'OCUPACAO') or
    (Tabela = 'SERV') or
    (Tabela = 'CARGO') or
    (Tabela = 'ESTADOCIVIL') or
    (Tabela = 'ESCOLARIDADE') or
    (Tabela = 'TIPOFILIACAO') or
    (Tabela = 'TIPODOCUMENTO') or
    (Tabela = 'JUSTIFICATIVA_AUSENC_CPF') or
    (Tabela = 'BANCO') or
    (Tabela = 'SELO_CCT') or
    (Tabela = 'SERIE') or
    (Tabela = 'COMPRA') or
    (Tabela = 'USUARIO') or
    (Tabela = 'PESSOA') or
    (Tabela = 'PESSOADOCUMENTO') or
    (Tabela = 'PESSOAFILIACAO') or
    (Tabela = 'PESSOAGEMEO') or
    (Tabela = 'HISTORICO_PESSOA')or
    (Tabela = 'SISTEMA') or
    (Tabela = 'TIPO_ATO') then
    Qry := dmGerencial.CriarFDQuery(nil, vgConGER)
  else
    Qry := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with Qry, SQL do
  begin
    Id := 0;

    Close;
    Clear;
    Text := 'SELECT MAX(' + Campo + ') AS ID ' +
            '  FROM ' + Tabela;
    Open;

    if Qry.FieldByName('ID').IsNull then
      Id := 1
    else
      Id := Qry.FieldByName('ID').AsInteger + 1;
  end;

  Result := Id;
end;

procedure TBibliotecaSistema.RecuperCodigoNome(Edt: TEdit; Cb: TComboBox;
  Campo: TCampoCN; Tipo: String; Valor: Integer);
var
  Qry: TFDQuery;
begin
  //Edit
  if Tipo = 'E' then
  begin
    if (Trim(Edt.Text) <> '') and
      (Trim(Edt.Text) <> '0') then
    begin
      Qry := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

      with Qry, SQL do
      begin
        Close;
        Clear;

        case Campo of
          EDTCAT:
          begin
            Text := 'SELECT DESCR_CATEGORIA_DESPREC ' +
                    '  FROM CATEGORIA_DESPREC ' +
                    ' WHERE ID_CATEGORIA_DESPREC = :iIdCategoria';
            Params.ParamByName('iIdCategoria').Value := StrToInt(Edt.Text);
          end;
          EDTSUBCAT:
          begin
            Text := 'SELECT DESCR_SUBCATEGORIA_DESPREC ' +
                    '  FROM SUBCATEGORIA_DESPREC ' +
                    ' WHERE ID_SUBCATEGORIA_DESPREC = :iIdSubCategoria';
            Params.ParamByName('iIdSubCategoria').Value := StrToInt(Edt.Text);
          end;
        end;

        Open;
      end;

      Cb.ItemIndex := Cb.Items.IndexOf(Qry.Fields[0].AsString);

      FreeAndNil(Qry);
    end;
  end;

  //Combobox
  if Tipo = 'C' then
  begin
    if Trim(Cb.Text) <> '' then
    begin
      Qry := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

      with Qry, SQL do
      begin
        Close;
        Clear;

        case Campo of
          CBCAT:
          begin
            Text := 'SELECT ID_CATEGORIA_DESPREC ' +
                    '  FROM CATEGORIA_DESPREC ' +
                    ' WHERE DESCR_CATEGORIA_DESPREC = :sDescrCategoria';
            Params.ParamByName('sDescrCategoria').Value := Trim(Cb.Text);
          end;
          CBSUBCAT:
          begin
            Text := 'SELECT ID_SUBCATEGORIA_DESPREC ' +
                    '  FROM SUBCATEGORIA_DESPREC ' +
                    ' WHERE DESCR_SUBCATEGORIA_DESPREC = :sDescrSubCategoria';
            Params.ParamByName('sDescrSubCategoria').Value := Trim(Cb.Text);
          end;
        end;

        Open;
      end;

      Edt.Text := Qry.Fields[0].AsString;

      FreeAndNil(Qry);
    end;
  end;
end;

procedure TBibliotecaSistema.VerificarExistenciaPastasSistema;
var
  sAno: String;
begin
  sAno := IntToStr(YearOf(Date));

  if not DirectoryExists(vgConf_DiretorioImagens + sAno + '\') then
    ForceDirectories(vgConf_DiretorioImagens + sAno + '\');

  if not DirectoryExists(vgConf_DiretorioRelatorios + sAno + '\') then
    ForceDirectories(vgConf_DiretorioRelatorios + sAno + '\');

  if not DirectoryExists(vgConf_DiretorioCarneLeao + sAno + '\') then
    ForceDirectories(vgConf_DiretorioCarneLeao + sAno + '\');
end;

end.
