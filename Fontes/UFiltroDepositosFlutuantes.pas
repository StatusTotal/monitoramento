{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroDepositosFLutuantes.pas
  Descricao:   Filtro de Depoditos Flutuantes
  Author   :   Cristina
  Date:        07-nov-2016
  Last Update: 27-dez-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroDepositosFlutuantes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, System.StrUtils, Vcl.StdCtrls, Vcl.Mask, JvExMask, JvToolEdit,
  System.DateUtils, JvBaseEdits, Datasnap.DBClient, Datasnap.Provider;

type
  TFFiltroDepositosFlutuantes = class(TFFiltroSimplesPadrao)
    dtePeriodoIni: TJvDateEdit;
    dtePeriodoFim: TJvDateEdit;
    lblPeriodo: TLabel;
    Label2: TLabel;
    rgSituacao: TRadioGroup;
    lblIdentificacao: TLabel;
    edtIdentificacao: TEdit;
    lblValor: TLabel;
    cedValor: TJvCalcEdit;
    edtNumRecibo: TEdit;
    lblNumRecibo: TLabel;
    qryGridPaiPadraoID: TIntegerField;
    qryGridPaiPadraoDESCR_DEPOSITO_FLUTUANTE: TStringField;
    qryGridPaiPadraoDATA_DEPOSITO: TDateField;
    qryGridPaiPadraoNUM_COD_DEPOSITO: TStringField;
    qryGridPaiPadraoVR_DEPOSITO_FLUTUANTE: TBCDField;
    qryGridPaiPadraoFLG_STATUS: TStringField;
    qryGridPaiPadraoDATA_FLUTUANTE: TDateField;
    qryGridPaiPadraoDATA_BAIXADO: TDateField;
    qryGridPaiPadraoDATA_CANCELAMENTO: TDateField;
    qryGridPaiPadraoFLUT_ID_USUARIO: TIntegerField;
    qryGridPaiPadraoBAIXA_ID_USUARIO: TIntegerField;
    qryGridPaiPadraoCANCEL_ID_USUARIO: TIntegerField;
    qryGridPaiPadraoCAD_ID_USUARIO: TIntegerField;
    qryGridPaiPadraoNUM_RECIBO: TStringField;
    qryGridPaiPadraoDATA_STATUS: TDateField;
    qryGridPaiPadraoDATA_CADASTRO: TSQLTimeStampField;
    procedure btnEditarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure dtePeriodoIniKeyPress(Sender: TObject; var Key: Char);
    procedure dtePeriodoFimKeyPress(Sender: TObject; var Key: Char);
    procedure rgSituacaoExit(Sender: TObject);
    procedure rgSituacaoClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure dbgGridPaiPadraoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure edtIdentificacaoKeyPress(Sender: TObject; var Key: Char);
    procedure edtNumReciboKeyPress(Sender: TObject; var Key: Char);
    procedure cedValorKeyPress(Sender: TObject; var Key: Char);
    procedure qryGridPaiPadraoCalcFields(DataSet: TDataSet);
    procedure qryGridPaiPadraoVR_DEPOSITO_FLUTUANTEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoFLG_STATUSGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure btnImprimirClick(Sender: TObject);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroDepositosFlutuantes: TFFiltroDepositosFlutuantes;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais,
  UCadastroDepositoFlutuante;

{ TFFiltroDepositosFlutuantes }

procedure TFFiltroDepositosFlutuantes.btnEditarClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  if ((Trim(qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString) = 'B') and
    (not qryGridPaiPadrao.FieldByName('DATA_FLUTUANTE').IsNull)) or
    (Trim(qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString) = 'C') then
    vgOperacao := C
  else
    vgOperacao := E;

  vgIdConsulta   := qryGridPaiPadrao.FieldByName('ID').AsInteger;
  vgOrigemFiltro := True;

  dmGerencial.CriarForm(TFCadastroDepositoFlutuante, FCadastroDepositoFlutuante);

  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroDepositosFlutuantes.btnExcluirClick(Sender: TObject);
var
  qryExclusao: TFDQuery;
begin
  inherited;

  if lPodeExcluir then
  begin
    qryExclusao := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      with qryExclusao, SQL do
      begin
        Close;
        Clear;
        Text := 'UPDATE DEPOSITO_FLUTUANTE ' +
                '   SET FLG_STATUS    = :FLG_STATUS, ' +
                '       DATA_CANCELAMENTO   = :DATA_CANCELAMENTO, ' +
                '       CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO ' +
                ' WHERE ID_DEPOSITO_FLUTUANTE = :ID_DEPOSITO_FLUTUANTE';
        Params.ParamByName('FLG_STATUS').Value            := 'C';
        Params.ParamByName('DATA_CANCELAMENTO').Value     := Date;
        Params.ParamByName('CANCEL_ID_USUARIO').Value     := vgUsu_Id;
        Params.ParamByName('ID_DEPOSITO_FLUTUANTE').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      GravarLog;
      Application.MessageBox('Dep�sito Flutuante exclu�do com sucesso!', 'Aviso', MB_OK)
    except
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Application.MessageBox('Erro na exclus�o do Dep�sito Flutuante.', 'Erro', MB_OK + MB_ICONERROR)
    end;

    FreeAndNil(qryExclusao);

    btnFiltrar.Click;
  end;
end;

procedure TFFiltroDepositosFlutuantes.btnFiltrarClick(Sender: TObject);
begin
  qryGridPaiPadrao.Close;
  qryGridPaiPadrao.SQL.Clear;
  qryGridPaiPadrao.SQL.Text := 'SELECT D.ID_DEPOSITO_FLUTUANTE AS ID, ' +
                               '       D.DESCR_DEPOSITO_FLUTUANTE, ' +
                               '       D.DATA_DEPOSITO, ' +
                               '       D.NUM_COD_DEPOSITO, ' +
                               '       D.VR_DEPOSITO_FLUTUANTE, ' +
                               '       D.FLG_STATUS, ' +
                               '       D.DATA_FLUTUANTE, ' +
                               '       D.DATA_BAIXADO, ' +
                               '       D.DATA_CANCELAMENTO, ' +
                               '       D.FLUT_ID_USUARIO, ' +
                               '       D.BAIXA_ID_USUARIO, ' +
                               '       D.CANCEL_ID_USUARIO, ' +
                               '       D.CAD_ID_USUARIO, ' +
                               '       D.DATA_CADASTRO, ' +
                               '       CAST((CASE D.FLG_STATUS ' +
                               '                  WHEN ' + QuotedStr('P') + ' THEN CAST(D.DATA_CADASTRO AS DATE) ' +
                               '                  WHEN ' + QuotedStr('F') + ' THEN D.DATA_FLUTUANTE ' +
                               '                  WHEN ' + QuotedStr('B') + ' THEN D.DATA_BAIXADO ' +
                               '                  WHEN ' + QuotedStr('C') + ' THEN D.DATA_CANCELAMENTO ' +
                               '             END) AS DATE) AS DATA_STATUS' +
                               '  FROM DEPOSITO_FLUTUANTE D ' +
                               ' WHERE D.DATA_DEPOSITO BETWEEN :DATA_INI AND :DATA_FIM';

  //Periodo
  qryGridPaiPadrao.Params.ParamByName('DATA_INI').Value := dtePeriodoIni.Date;
  qryGridPaiPadrao.Params.ParamByName('DATA_FIM').Value := dtePeriodoFim.Date;

  //Identificacao
  if Trim(edtIdentificacao.Text) <> '' then
    qryGridPaiPadrao.SQL.Add(' AND UPPER(D.NUM_COD_DEPOSITO) LIKE ' + QuotedStr('%' + Trim(UpperCase(edtIdentificacao.Text)) + '%'));

  //Valor
  if cedValor.Value > 0 then
    qryGridPaiPadrao.SQL.Add(' AND D.VR_DEPOSITO_FLUTUANTE = ' + ReplaceStr(cedValor.EditText, ',', '.'));

  //Recibo
  if Trim(edtNumRecibo.Text) <> '' then
    qryGridPaiPadrao.SQL.Add(' AND EXISTS (SELECT L.NUM_RECIBO ' +
                             '               FROM VINCULO_LANCDEPO V ' +
                             '              INNER JOIN LANCAMENTO_PARC LP ' +
                             '                 ON V.ID_LANCAMENTO_PARC_FK = LP.ID_LANCAMENTO_PARC ' +
                             '              INNER JOIN LANCAMENTO L ' +
                             '                 ON LP.COD_LANCAMENTO_FK = L.COD_LANCAMENTO ' +
                             '                AND LP.ANO_LANCAMENTO_FK = L.ANO_LANCAMENTO ' +
                             '              WHERE V.ID_DEPOSITO_FLUTUANTE_FK = D.ID_DEPOSITO_FLUTUANTE ' +
                             '                AND V.CANCEL_ID_USUARIO IS NULL ' +
                             '                AND L.NUM_RECIBO = ' + Trim(edtNumRecibo.Text) + ' )');

  //Situacao
  case rgSituacao.ItemIndex of
    1:  //Flutuante
      qryGridPaiPadrao.SQL.Add(' AND D.FLG_STATUS = ' + QuotedStr('P'));
    2:  //Flutuante
      qryGridPaiPadrao.SQL.Add(' AND D.FLG_STATUS = ' + QuotedStr('F'));
    3:  //Baixado
      qryGridPaiPadrao.SQL.Add(' AND D.FLG_STATUS = ' + QuotedStr('B'));
    4:  //Cancelado
      qryGridPaiPadrao.SQL.Add(' AND D.FLG_STATUS = ' + QuotedStr('C'));
  end;

  qryGridPaiPadrao.SQL.Add(' ORDER BY D.DATA_DEPOSITO');

  qryGridPaiPadrao.Open;

  inherited;
end;

procedure TFFiltroDepositosFlutuantes.btnImprimirClick(Sender: TObject);
begin
  inherited;

  dmPrincipal.AbrirRelatorio(4);
end;

procedure TFFiltroDepositosFlutuantes.btnIncluirClick(Sender: TObject);
begin
  inherited;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := True;

  dmGerencial.CriarForm(TFCadastroDepositoFlutuante, FCadastroDepositoFlutuante);

  btnFiltrar.Click;
end;

procedure TFFiltroDepositosFlutuantes.btnLimparClick(Sender: TObject);
begin
  inherited;

  dtePeriodoIni.Date := StartOfTheMonth(Date);
  dtePeriodoFim.Date := Date;
  edtIdentificacao.Clear;
  cedValor.Value := 0;
  edtNumRecibo.Clear;
  rgSituacao.ItemIndex := 2;

  inherited;

  if dtePeriodoIni.CanFocus then
    dtePeriodoIni.SetFocus;
end;

procedure TFFiltroDepositosFlutuantes.cedValorKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroDepositosFlutuantes.dbgGridPaiPadraoDblClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao   := C;
  vgIdConsulta := qryGridPaiPadrao.FieldByName('ID').AsInteger;

  dmGerencial.CriarForm(TFCadastroDepositoFlutuante, FCadastroDepositoFlutuante);

  DefinirPosicaoGrid;
end;

procedure TFFiltroDepositosFlutuantes.dbgGridPaiPadraoDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  Grid: TDBGrid;
begin
  inherited;

  Grid := Sender as TDBGrid;

  if Grid.DataSource.DataSet.FieldByName('FLG_STATUS').AsString = 'C' then
  begin
    Grid.Canvas.Font.Color := clGray;
    Grid.Canvas.FillRect(Rect);
    Grid.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TFFiltroDepositosFlutuantes.dtePeriodoFimKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    if dtePeriodoFim.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso', MB_OK + MB_ICONWARNING);

      if dtePeriodoFim.CanFocus then
        dtePeriodoFim.SetFocus;
    end
    else if dtePeriodoIni.Date > dtePeriodoFim.Date then
    begin
      Application.MessageBox('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso', MB_OK + MB_ICONWARNING);

      dtePeriodoFim.Clear;

      if dtePeriodoFim.CanFocus then
        dtePeriodoFim.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dtePeriodoFim.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso', MB_OK + MB_ICONWARNING);

      if dtePeriodoFim.CanFocus then
        dtePeriodoFim.SetFocus;
    end
    else if dtePeriodoIni.Date > dtePeriodoFim.Date then
    begin
      Application.MessageBox('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso', MB_OK + MB_ICONWARNING);

      dtePeriodoFim.Clear;

      if dtePeriodoFim.CanFocus then
        dtePeriodoFim.SetFocus;
    end
    else
      btnFiltrar.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFFiltroDepositosFlutuantes.dtePeriodoIniKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    if dtePeriodoIni.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso', MB_OK + MB_ICONWARNING);

      if dtePeriodoIni.CanFocus then
        dtePeriodoIni.SetFocus;
    end
    else if dtePeriodoIni.Date > dtePeriodoFim.Date then
    begin
      Application.MessageBox('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso', MB_OK + MB_ICONWARNING);

      dtePeriodoIni.Clear;

      if dtePeriodoIni.CanFocus then
        dtePeriodoIni.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dtePeriodoIni.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso', MB_OK + MB_ICONWARNING);

      if dtePeriodoIni.CanFocus then
        dtePeriodoIni.SetFocus;
    end
    else if dtePeriodoIni.Date > dtePeriodoFim.Date then
    begin
      Application.MessageBox('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso', MB_OK + MB_ICONWARNING);

      dtePeriodoIni.Clear;

      if dtePeriodoIni.CanFocus then
        dtePeriodoIni.SetFocus;
    end
    else
      btnFiltrar.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    Self.Close;
    Key := #0;
  end;
end;

procedure TFFiltroDepositosFlutuantes.edtIdentificacaoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroDepositosFlutuantes.edtNumReciboKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroDepositosFlutuantes.FormCreate(Sender: TObject);
begin
  inherited;

  dtePeriodoIni.Date   := StartOfTheMonth(Date);
  dtePeriodoFim.Date   := Date;
  rgSituacao.ItemIndex := 2;
end;

procedure TFFiltroDepositosFlutuantes.FormShow(Sender: TObject);
begin
  btnFiltrar.Click;

  inherited;

  ShowScrollBar(dbgGridPaiPadrao.Handle, SB_HORZ, False);

  if dtePeriodoIni.CanFocus then
    dtePeriodoIni.SetFocus;
end;

procedure TFFiltroDepositosFlutuantes.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    X:  //EXCLUSAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da exclus�o desse Dep�sito Flutuante:', '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'DEPOSITO_FLUTUANTE');
    end;
    P:  //IMPRESSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente impress�o de Lista de Dep�sitos Flutuantes',
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'DEPOSITO_FLUTUANTE');
    end;
    T:  //EXPORTACAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente exporta��o de Lista de Dep�sitos Flutuantes',
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'DEPOSITO_FLUTUANTE');
    end;
  end;
end;

function TFFiltroDepositosFlutuantes.PodeExcluir(var Msg: String): Boolean;
var
  QryVerif: TFDQuery;
begin
  inherited;

  Result := True;

  QryVerif := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryVerif, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT * FROM VINCULO_LANCDEPO WHERE ID_DEPOSITO_FLUTUANTE_FK = :ID_DEPOSITO_FLUTUANTE';
    Params.ParamByName('ID_DEPOSITO_FLUTUANTE').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
    Open;
  end;

  if QryVerif.RecordCount > 0 then
    msg := 'N�o � poss�vel excluir o Dep�sito informado, pois o mesmo j� foi baixado no Sistema.'
  else if Trim(qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString) = 'C' then
    msg := 'O Dep�sito j� se encontra cancelado.'
  else if Trim(qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString) = 'B' then
    msg := 'O Dep�sito j� se encontra Vinculado.';

  Result := (QryVerif.RecordCount = 0) and
            (qryGridPaiPadrao.RecordCount > 0) and
            (Trim(qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString) <> 'C') and
            (Trim(qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString) <> 'B');

  FreeAndNil(QryVerif);
end;

procedure TFFiltroDepositosFlutuantes.qryGridPaiPadraoCalcFields(
  DataSet: TDataSet);
var
  qryAux: TFDQuery;
  sNumRecibo: String;
begin
  inherited;

  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with qryAux, SQL do
  begin
    //Numero do Recibo e Id do Sistema
    if qryGridPaiPadrao.RecordCount > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT L.NUM_RECIBO ' +
              '  FROM VINCULO_LANCDEPO V ' +
              ' INNER JOIN LANCAMENTO_PARC LP ' +
              '    ON V.ID_LANCAMENTO_PARC_FK = LP.ID_LANCAMENTO_PARC ' +
              ' INNER JOIN LANCAMENTO L ' +
              '    ON LP.COD_LANCAMENTO_FK = L.COD_LANCAMENTO ' +
              '   AND LP.ANO_LANCAMENTO_FK = L.ANO_LANCAMENTO ' +
              ' WHERE V.ID_DEPOSITO_FLUTUANTE_FK = :ID_DEPOSITO_FLUTUANTE ' +
              '   AND V.CANCEL_ID_USUARIO IS NULL ' +
              'ORDER BY L.NUM_RECIBO';
      Params.ParamByName('ID_DEPOSITO_FLUTUANTE').AsInteger := qryGridPaiPadrao.FieldByName('ID').AsInteger;
      Open;
    end;

    if qryAux.RecordCount > 0 then
    begin
      qryAux.First;

      sNumRecibo := '';

      while not qryAux.Eof do
      begin
        if Trim(qryAux.FieldByName('NUM_RECIBO').AsString) <> '' then
        begin
          if Trim(sNumRecibo) = '' then
            sNumRecibo := qryAux.FieldByName('NUM_RECIBO').AsString
          else
            sNumRecibo := sNumRecibo + ' | ' + qryAux.FieldByName('NUM_RECIBO').AsString;
        end;

        qryAux.Next;
      end;

      qryGridPaiPadrao.FieldByName('NUM_RECIBO').AsString := sNumRecibo;
    end;
  end;

  FreeAndNil(qryAux);
end;

procedure TFFiltroDepositosFlutuantes.qryGridPaiPadraoFLG_STATUSGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

   if Sender.AsString <> '' then
  begin
    case AnsiIndexStr(UpperCase(Sender.AsString), ['P', 'F', 'B', 'C']) of
      0: Text := 'A Compensar';
      1: Text := 'Flutuante';
      2: Text := 'Baixado';
      3: Text := 'Cancelado';
    end;
  end;
end;

procedure TFFiltroDepositosFlutuantes.qryGridPaiPadraoVR_DEPOSITO_FLUTUANTEGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFFiltroDepositosFlutuantes.rgSituacaoClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroDepositosFlutuantes.rgSituacaoExit(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroDepositosFlutuantes.VerificarPermissoes;
begin
  inherited;

  btnIncluir.Enabled  := vgPrm_IncDepo;
  btnEditar.Enabled   := vgPrm_EdDepo and (qryGridPaiPadrao.RecordCount > 0);
  btnExcluir.Enabled  := vgPrm_ExcDepo and (qryGridPaiPadrao.RecordCount > 0);
  btnImprimir.Enabled := vgPrm_ImpDepo and (qryGridPaiPadrao.RecordCount > 0);
end;

end.
