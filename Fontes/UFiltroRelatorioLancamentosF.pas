{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroRelatorioLancamentosF.pas
  Descricao:   Filtro Relatorio de Lancamentos (R14 - R14DET)
  Author   :   Cristina
  Date:        06-jun-2017
  Last Update: 04-jan-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroRelatorioLancamentosF;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroRelatorioPadrao, Vcl.StdCtrls,
  JvExControls, JvButton, JvTransparentButton, Vcl.ExtCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFFiltroRelatorioLancamentosF = class(TFFiltroRelatorioPadrao)
    chbExibirItens: TCheckBox;
    procedure btnLimparClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
  protected
    procedure GravarLog; override;
    procedure GerarRelatorio(ImpDet: Boolean); override;
    function VerificarFiltro: Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
    dDataInicio,
    dDataFim: TDateTime;

    sLstLancamentos,
    sTipoLanc1,
    sTipoLanc2: String;
  end;

var
  FFiltroRelatorioLancamentosF: TFFiltroRelatorioLancamentosF;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMRelatorios;

{ TFFiltroRelatorioLancamentosF }

procedure TFFiltroRelatorioLancamentosF.btnLimparClick(Sender: TObject);
begin
  inherited;

  chbExportarExcel.Enabled := vgPrm_ExpRec or vgPrm_ExpDesp;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;

  chbExibirItens.Checked := True;
  chbExportarPDF.Checked := False;

  if chbExibirItens.CanFocus then
    chbExibirItens.SetFocus;
end;

procedure TFFiltroRelatorioLancamentosF.btnVisualizarClick(Sender: TObject);
begin
  inherited;

  if chbExibirItens.Checked then
    GerarRelatorio(True)
  else
    GerarRelatorio(False);
end;

procedure TFFiltroRelatorioLancamentosF.FormShow(Sender: TObject);
begin
  inherited;

  chbTipoSintetico.Visible := False;
  chbTipoAnalitico.Visible := False;
  gbTipo.Visible           := False;

  chbExportarExcel.Enabled := vgPrm_ExpRec or vgPrm_ExpDesp;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;

  chbExibirGrafico.Visible := False;

  chbExibirItens.Checked := True;
  chbExportarPDF.Checked := False;

  if chbExibirItens.CanFocus then
    chbExibirItens.SetFocus;
end;

procedure TFFiltroRelatorioLancamentosF.GerarRelatorio(ImpDet: Boolean);
begin
  inherited;

  try
    sTituloRelatorio    := 'LAN�AMENTOS';
    sSubtituloRelatorio := 'Per�odo de ' + DateToStr(dDataInicio) + ' a ' + DateToStr(dDataFim);

    dmRelatorios.qryRel14_LancsF.Close;
    dmRelatorios.qryRel14_LancsF.SQL.Clear;

    dmRelatorios.qryRel14_LancsF.SQL.Text := 'SELECT LP.DATA_PAGAMENTO, ' +
                                             '       (CASE WHEN (L.TIPO_LANCAMENTO = ' + QuotedStr('R') + ') ' +
                                             '             THEN 1 ' +
                                             '             ELSE 2 ' +
                                             '        END) AS TIPO_LANCAMENTO, ' +
                                             '       (CASE WHEN (L.TIPO_LANCAMENTO = ' + QuotedStr('R') + ') ' +
                                             '             THEN ' + QuotedStr('RECEITAS') + ' ' +
                                             '             ELSE ' + QuotedStr('DESPESAS') + ' ' +
                                             '        END) AS DESCR_TIPO_LANCAMENTO, ' +
                                             '       EXTRACT(MONTH FROM LP.DATA_LANCAMENTO_PARC) AS MES_LANCAMENTO, ' +
                                             '       (CASE EXTRACT(MONTH FROM LP.DATA_LANCAMENTO_PARC) ' +
                                             '          WHEN ' + QuotedStr('01') + ' THEN ' + QuotedStr('JANEIRO') +
                                             '          WHEN ' + QuotedStr('02') + ' THEN ' + QuotedStr('FEVEREIRO') +
                                             '          WHEN ' + QuotedStr('03') + ' THEN ' + QuotedStr('MAR�O') +
                                             '          WHEN ' + QuotedStr('04') + ' THEN ' + QuotedStr('ABRIL') +
                                             '          WHEN ' + QuotedStr('05') + ' THEN ' + QuotedStr('MAIO') +
                                             '          WHEN ' + QuotedStr('06') + ' THEN ' + QuotedStr('JUNHO') +
                                             '          WHEN ' + QuotedStr('07') + ' THEN ' + QuotedStr('JULHO') +
                                             '          WHEN ' + QuotedStr('08') + ' THEN ' + QuotedStr('AGOSTO') +
                                             '          WHEN ' + QuotedStr('09') + ' THEN ' + QuotedStr('SETEMBRO') +
                                             '          WHEN ' + QuotedStr('10') + ' THEN ' + QuotedStr('OUTUBRO') +
                                             '          WHEN ' + QuotedStr('11') + ' THEN ' + QuotedStr('NOVEMBRO') +
                                             '          WHEN ' + QuotedStr('12') + ' THEN ' + QuotedStr('DEZEMBRO') +
                                             '        END) AS DESCR_MES_LANCAMENTO, ' +
                                             '       (CASE EXTRACT(MONTH FROM LP.DATA_LANCAMENTO_PARC) ' +
                                             '          WHEN ' + QuotedStr('01') + ' THEN ' + QuotedStr('JAN') +
                                             '          WHEN ' + QuotedStr('02') + ' THEN ' + QuotedStr('FEV') +
                                             '          WHEN ' + QuotedStr('03') + ' THEN ' + QuotedStr('MAR') +
                                             '          WHEN ' + QuotedStr('04') + ' THEN ' + QuotedStr('ABR') +
                                             '          WHEN ' + QuotedStr('05') + ' THEN ' + QuotedStr('MAI') +
                                             '          WHEN ' + QuotedStr('06') + ' THEN ' + QuotedStr('JUN') +
                                             '          WHEN ' + QuotedStr('07') + ' THEN ' + QuotedStr('JUL') +
                                             '          WHEN ' + QuotedStr('08') + ' THEN ' + QuotedStr('AGO') +
                                             '          WHEN ' + QuotedStr('09') + ' THEN ' + QuotedStr('SET') +
                                             '          WHEN ' + QuotedStr('10') + ' THEN ' + QuotedStr('OUT') +
                                             '          WHEN ' + QuotedStr('11') + ' THEN ' + QuotedStr('NOV') +
                                             '          WHEN ' + QuotedStr('12') + ' THEN ' + QuotedStr('DEZ') +
                                             '        END) AS SIGLA_MES_LANCAMENTO, ' +
                                             '       (LP.NUM_PARCELA || ' + QuotedStr('/') + ' || L.QTD_PARCELAS) AS PARCELA, ' +
                                             '       LP.ID_BANCO_FK, ' +
                                             '       B.COD_BANCO, ' +
                                             '       CAST((LP.AGENCIA || ' + QuotedStr('/') + ' || LP.CONTA) AS VARCHAR(21)) AS AG_CONTA, ' +
                                             '       (CASE LP.ID_FORMAPAGAMENTO_FK ' +
                                             '             WHEN 3 THEN LP.NUM_CHEQUE ' +
                                             '             WHEN 5 THEN LP.NUM_COD_DEPOSITO ' +
                                             '        END) AS NUM_DOCUMENTO, ' +
                                             '       L.FLG_FLUTUANTE, ' +
                                             '       L.ID_NATUREZA_FK, ' +
                                             '       N.DESCR_NATUREZA, ' +
                                             '       L.ID_CATEGORIA_DESPREC_FK, ' +
                                             '       C.DESCR_CATEGORIA_DESPREC, ' +
                                             '       L.ID_SUBCATEGORIA_DESPREC_FK, ' +
                                             '       S.DESCR_SUBCATEGORIA_DESPREC, ' +
                                             '       L.NUM_RECIBO, ' +
                                             '       L.VR_TOTAL_PREV, ' +
                                             '       L.VR_TOTAL_JUROS, ' +
                                             '       L.VR_TOTAL_DESCONTO, ' +
                                             '       L.VR_TOTAL_PAGO, ' +
                                             '       LP.VR_PARCELA_PREV, ' +
                                             '       LP.VR_PARCELA_JUROS, ' +
                                             '       LP.VR_PARCELA_DESCONTO, ' +
                                             '       LP.VR_PARCELA_PAGO, ' +
                                             '       L.COD_LANCAMENTO, ' +
                                             '       L.ANO_LANCAMENTO, ' +
                                             '       L.OBSERVACAO, ' +
                                             '       LP.OBS_LANCAMENTO_PARC, ' +
                                             '       LP.ID_FORMAPAGAMENTO_FK, ' +
                                             '       FP.TIPO AS TIPO_PAGAMENTO, ' +
                                             '       FP.DESCR_FORMAPAGAMENTO ' +
                                             '  FROM LANCAMENTO L ' +
                                             '  LEFT JOIN NATUREZA N ' +
                                             '    ON L.ID_NATUREZA_FK = N.ID_NATUREZA ' +
                                             '  LEFT JOIN CATEGORIA_DESPREC C ' +
                                             '    ON L.ID_CATEGORIA_DESPREC_FK = C.ID_CATEGORIA_DESPREC ' +
                                             '  LEFT JOIN SUBCATEGORIA_DESPREC S ' +
                                             '    ON L.ID_SUBCATEGORIA_DESPREC_FK = S.ID_SUBCATEGORIA_DESPREC ' +
                                             '   AND L.ID_CATEGORIA_DESPREC_FK    = S.ID_CATEGORIA_DESPREC_FK ' +
                                             ' INNER JOIN LANCAMENTO_PARC LP ' +
                                             '    ON L.COD_LANCAMENTO = LP.COD_LANCAMENTO_FK ' +
                                             '   AND L.ANO_LANCAMENTO = LP.ANO_LANCAMENTO_FK ' +
                                             '  LEFT JOIN BANCO B ' +
                                             '    ON LP.ID_BANCO_FK = B.ID_BANCO ' +
                                             '  LEFT JOIN FORMAPAGAMENTO FP ' +
                                             '    ON LP.ID_FORMAPAGAMENTO_FK = FP.ID_FORMAPAGAMENTO ' +
                                             ' WHERE (L.COD_LANCAMENTO || ' + QuotedStr('/') + ' || L.ANO_LANCAMENTO) IN (' + sLstLancamentos + ') ' +
                                             '   AND (L.TIPO_LANCAMENTO = :TP_LANC01 ' +
                                             '    OR  L.TIPO_LANCAMENTO = :TP_LANC02) ' +
                                             'ORDER BY TIPO_LANCAMENTO, ' +
                                             '         MES_LANCAMENTO, ' +
                                             '         TIPO_PAGAMENTO, ' +
                                             '         LP.DATA_PAGAMENTO, ' +
                                             '         L.NUM_RECIBO, ' +
                                             '         L.TIPO_LANCAMENTO, ' +
                                             '         L.ID_NATUREZA_FK, ' +
                                             '         L.ID_CATEGORIA_DESPREC_FK, ' +
                                             '         L.ID_SUBCATEGORIA_DESPREC_FK';

    dmRelatorios.qryRel14_LancsF.Params.ParamByName('TP_LANC01').Value := sTipoLanc1;
    dmRelatorios.qryRel14_LancsF.Params.ParamByName('TP_LANC02').Value := sTipoLanc2;

    dmRelatorios.qryRel14_LancsF.Open;

    dmRelatorios.qryRel14_LancsFDet.Close;
    dmRelatorios.qryRel14_LancsFDet.Params.ParamByName('COD_LANCAMENTO').Value := dmRelatorios.qryRel14_LancsF.FieldByName('COD_LANCAMENTO').AsInteger;
    dmRelatorios.qryRel14_LancsFDet.Params.ParamByName('ANO_LANCAMENTO').Value := dmRelatorios.qryRel14_LancsF.FieldByName('ANO_LANCAMENTO').AsInteger;
    dmRelatorios.qryRel14_LancsFDet.Open;

    if ImpDet then
    begin
      lExibeDetalhe := True;
      dmRelatorios.DefinirRelatorio(R14DET);
    end
    else
    begin
      lExibeDetalhe := False;
      dmRelatorios.DefinirRelatorio(R14);
    end;

    dmRelatorios.ImprimirRelatorio(chbExportarPDF.Checked, chbExportarExcel.Checked);

    GravarLog;
  except
    Application.MessageBox('Erro ao gerar relat�rio de Lan�amentos.',
                           'Erro',
                           MB_OK + MB_ICONERROR);
  end;
end;

procedure TFFiltroRelatorioLancamentosF.GravarLog;
begin
  inherited;

  BS.GravarUsuarioLog(50,
                      '',
                      'Impress�o de Relat�rio de Lan�amentos (' + DateToStr(dDataRelatorio) +
                                                              ' ' +
                                                              TimeToStr(tHoraRelatorio) + ')',
                      0,
                      '');
end;

function TFFiltroRelatorioLancamentosF.VerificarFiltro: Boolean;
begin
  //
end;

end.
