{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UListaRecibos.pas
  Descricao:   Lista Simples de Recibos para Reimpressao
  Author   :   Cristina
  Date:        23-mai-2017
  Last Update: 21-ago-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UListaRecibos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UListaSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Mask, JvExMask, JvToolEdit, Datasnap.DBClient,
  Datasnap.Provider, system.DateUtils;

type
  TFListaRecibos = class(TFListaSimplesPadrao)
    btnReimprimir: TJvTransparentButton;
    lblPeriodo: TLabel;
    dteDataInicio: TJvDateEdit;
    lblA: TLabel;
    dteDataFim: TJvDateEdit;
    lblNumeroRecibo: TLabel;
    edtNumeroRecibo: TEdit;
    dspGridListaPadrao: TDataSetProvider;
    cdsGridListaPadrao: TClientDataSet;
    cdsGridListaPadraoID_RECIBO_IMPRESSAO: TIntegerField;
    cdsGridListaPadraoNUM_RECIBO: TIntegerField;
    cdsGridListaPadraoVR_RECIBO: TBCDField;
    cdsGridListaPadraoVR_RECIBO_EXTENSO: TStringField;
    cdsGridListaPadraoCOD_SERVENTIA: TStringField;
    cdsGridListaPadraoNOME_SERVENTIA: TStringField;
    cdsGridListaPadraoOBS_RECIBO_IMPRESSAO: TStringField;
    cdsGridListaPadraoDATA_RECIBO: TDateField;
    cdsGridListaPadraoCOD_MUNICIPIO: TStringField;
    cdsGridListaPadraoNOME_MUNICIPIO: TStringField;
    cdsGridListaPadraoEM_ID_PESSOA: TIntegerField;
    cdsGridListaPadraoNOME_EMITENTE: TStringField;
    cdsGridListaPadraoCPF_CNPJ_EMITENTE: TStringField;
    cdsGridListaPadraoENDERECO_EMITENTE: TStringField;
    cdsGridListaPadraoCOD_LANCAMENTO_FK: TIntegerField;
    cdsGridListaPadraoANO_LANCAMENTO_FK: TIntegerField;
    cdsGridListaPadraoDATA_IMPRESSAO: TSQLTimeStampField;
    cdsGridListaPadraoID_USUARIO: TIntegerField;
    cdsGridListaPadraoFLG_FUNCIONARIO: TStringField;
    cdsGridListaPadraoSELECIONADO: TBooleanField;
    dsReimpressao: TDataSource;
    cdsReimpressao: TClientDataSet;
    dspReimpressao: TDataSetProvider;
    qryReimpressao: TFDQuery;
    cdsGridListaPadraoDESCR_SERVICOS: TStringField;
    procedure btnReimprimirClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure dteDataInicioKeyPress(Sender: TObject; var Key: Char);
    procedure dteDataFimKeyPress(Sender: TObject; var Key: Char);
    procedure edtNumeroReciboKeyPress(Sender: TObject; var Key: Char);
    procedure btnLimparClick(Sender: TObject);
    procedure cdsGridListaPadraoVR_RECIBOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure dbgGridListaPadraoCellClick(Column: TColumn);
    procedure dbgGridListaPadraoColEnter(Sender: TObject);
    procedure dbgGridListaPadraoColExit(Sender: TObject);
    procedure dbgGridListaPadraoDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FOriginalOptions: TDBGridOptions;

    procedure SalvarBoleano;
  public
    { Public declarations }
  end;

var
  FListaRecibos: TFListaRecibos;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMRelatorios;

procedure TFListaRecibos.btnFiltrarClick(Sender: TObject);
begin
  inherited;

  cdsGridListaPadrao.Close;

  cdsGridListaPadrao.Params.ParamByName('DATA_INI').Value := StartOfTheDay(dteDataInicio.Date);
  cdsGridListaPadrao.Params.ParamByName('DATA_FIM').Value := EndOfTheDay(dteDataFim.Date);

  if Trim(edtNumeroRecibo.Text) = '' then
  begin
    cdsGridListaPadrao.Params.ParamByName('NUM_REC1').Value := 0;
    cdsGridListaPadrao.Params.ParamByName('NUM_REC2').Value := 0;
  end
  else
  begin
    cdsGridListaPadrao.Params.ParamByName('NUM_REC1').Value := StrToInt(edtNumeroRecibo.Text);
    cdsGridListaPadrao.Params.ParamByName('NUM_REC2').Value := StrToInt(edtNumeroRecibo.Text);
  end;

  cdsGridListaPadrao.Open;
end;

procedure TFListaRecibos.btnLimparClick(Sender: TObject);
begin
  inherited;

  dteDataInicio.Date := StartOfTheMonth(Date);
  dteDataFim.Date    := EndOfTheMonth(Date);

  btnFiltrar.Click;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFListaRecibos.btnReimprimirClick(Sender: TObject);
var
  sMsg: String;
  iCont, IdReimp, iIdUsu: Integer;
  sLstNumRec, sLstIdRec, sNomeUsu, sSenhaUsu: String;
  lCancelado: Boolean;
begin
  inherited;

  iCont   := 0;
  sMsg    := '';
  IdReimp := 0;

  sLstNumRec := '';
  sLstIdRec  := '';

  lCancelado := False;

  cdsReimpressao.Close;
  cdsReimpressao.Open;

  cdsGridListaPadrao.First;

  while not cdsGridListaPadrao.Eof do
  begin

    if cdsGridListaPadrao.FieldByName('SELECIONADO').AsBoolean then
    begin
      if Trim(sLstNumRec) = '' then
      begin
        sLstNumRec := cdsGridListaPadrao.FieldByName('NUM_RECIBO').AsString;
        sLstIdRec  := cdsGridListaPadrao.FieldByName('ID_RECIBO_IMPRESSAO').AsString;
      end
      else
      begin
        sLstNumRec := (sLstNumRec + #13#10 + cdsGridListaPadrao.FieldByName('NUM_RECIBO').AsString);
        sLstIdRec  := (sLstIdRec + ', ' + cdsGridListaPadrao.FieldByName('ID_RECIBO_IMPRESSAO').AsString);
      end;

      Inc(iCont);
    end;

    cdsGridListaPadrao.Next;
  end;

  if iCont = 0 then
    Application.MessageBox('Nenhum Recibo selecionado.', 'Aviso', MB_OK + MB_ICONWARNING)
  else
  begin
    if iCont = 1 then
      sMsg := 'Confirma a sele��o do Recibo abaixo? ' + #13#10 + sLstNumRec
    else
      sMsg := 'Confirma a sele��o dos Recibos abaixo? ' + #13#10 + sLstNumRec;

    if Application.MessageBox(PChar(sMsg), 'Confirma��o', MB_YESNO + MB_ICONQUESTION) = ID_YES then
    begin
      //Solicita Login e Senha de Autorizador
      iIdUsu    := 0;
      sNomeUsu  := '';
      sSenhaUsu := '';

      repeat
        repeat
          if not InputQuery('LOGIN', 'Por favor, informe o LOGIN do Autorizador:', sNomeUsu) then
            lCancelado := True;
        until (Trim(sNomeUsu) <> '') or lCancelado;

        if not lCancelado then
        begin
          repeat
            if not InputQuery('SENHA', #31'Por favor, informe a SENHA do Autorizador:', sSenhaUsu) then
              lCancelado := True;
          until (Trim(sSenhaUsu) <> '') or lCancelado;
        end;
      until (dmGerencial.VerificarAutorizacao(iIdUsu, sNomeUsu, sSenhaUsu) = True) or lCancelado;

      if not lCancelado then
      begin
        //Adiciona Recibo ao ClientDataSet dos Relatorios
        dmRelatorios.CarregarRecibos('', sLstIdRec, False);

        //Gerar impressao de Recibos
        dmRelatorios.DefinirRelatorio(R05);
        dmRelatorios.ImprimirRelatorio(False, False);

        //Grava Reimpressao
        IdReimp := BS.ProximoId('ID_RECIBO_REIMPRESSAO', 'RECIBO_REIMPRESSAO');

        cdsGridListaPadrao.First;

        while not cdsGridListaPadrao.Eof do
        begin
          cdsReimpressao.Append;
          cdsReimpressao.FieldByName('ID_RECIBO_REIMPRESSAO').AsInteger  := IdReimp;
          cdsReimpressao.FieldByName('ID_RECIBO_IMPRESSAO_FK').AsInteger := cdsGridListaPadrao.FieldByName('ID_RECIBO_IMPRESSAO').AsInteger;
          cdsReimpressao.FieldByName('ID_USUARIO').AsInteger             := vgUsu_Id;
          cdsReimpressao.FieldByName('AUT_ID_USUARIO').AsInteger         := iIdUsu;
          cdsReimpressao.FieldByName('AUT_NOME_USUARIO').AsString        := sNomeUsu;
          cdsReimpressao.FieldByName('DATA_REIMPRESSAO').AsDateTime      := Date;
          cdsReimpressao.Post;

          Inc(IdReimp);

          cdsGridListaPadrao.Next;
        end;

        cdsReimpressao.ApplyUpdates(0);

        BS.GravarUsuarioLog(50,
                            '',
                            'Impress�o de Recibo (' + DateToStr(dDataRelatorio) +
                                                  ' ' +
                                                  TimeToStr(tHoraRelatorio) + ')',
                            0,
                            '');
      end;
    end;
  end;

  cdsGridListaPadrao.First;
end;

procedure TFListaRecibos.cdsGridListaPadraoVR_RECIBOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFListaRecibos.dbgGridListaPadraoCellClick(Column: TColumn);
begin
  inherited;

  if dbgGridListaPadrao.SelectedField.DataType = ftBoolean then
    SalvarBoleano;
end;

procedure TFListaRecibos.dbgGridListaPadraoColEnter(Sender: TObject);
begin
  inherited;

  if dbgGridListaPadrao.SelectedField.DataType = ftBoolean then
  begin
    FOriginalOptions           := dbgGridListaPadrao.Options;
    dbgGridListaPadrao.Options := dbgGridListaPadrao.Options - [dgEditing];
  end;
end;

procedure TFListaRecibos.dbgGridListaPadraoColExit(Sender: TObject);
begin
  inherited;

  if dbgGridListaPadrao.SelectedField.DataType = ftBoolean then
    dbgGridListaPadrao.Options := FOriginalOptions;
end;

procedure TFListaRecibos.dbgGridListaPadraoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
const
  CtrlState: array[Boolean] of Integer = (DFCS_BUTTONCHECK,
                                          DFCS_BUTTONCHECK or DFCS_CHECKED);
var
  CheckBoxRectangle: TRect;
begin
  inherited;

  if (Column.Field.FieldName = 'SELECIONADO') then
  begin
    dbgGridListaPadrao.Canvas.FillRect(Rect);

    CheckBoxRectangle.Left   := (Rect.Left + 2);
    CheckBoxRectangle.Right  := (Rect.Right - 2);
    CheckBoxRectangle.Top    := (Rect.Top + 2);
    CheckBoxRectangle.Bottom := (Rect.Bottom - 2);

    DrawFrameControl(dbgGridListaPadrao.Canvas.Handle,
                     CheckBoxRectangle,
                     DFC_BUTTON,
                     CtrlState[Column.Field.AsBoolean]);
  end;
end;

procedure TFListaRecibos.dteDataFimKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if ((dteDataInicio.Date > 0) and
      (dteDataFim.Date > 0)) or
      (Trim(edtNumeroRecibo.Text) <> '') then
      btnFiltrar.Click
    else
      SelectNext(ActiveControl, True, True);

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFListaRecibos.dteDataInicioKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if ((dteDataInicio.Date > 0) and
      (dteDataFim.Date > 0)) or
      (Trim(edtNumeroRecibo.Text) <> '') then
      btnFiltrar.Click
    else
      SelectNext(ActiveControl, True, True);

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFListaRecibos.edtNumeroReciboKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if ((dteDataInicio.Date > 0) and
      (dteDataFim.Date > 0)) or
      (Trim(edtNumeroRecibo.Text) <> '') then
      btnFiltrar.Click
    else
      SelectNext(ActiveControl, True, True);

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFListaRecibos.FormShow(Sender: TObject);
begin
  inherited;

  dteDataInicio.Date := StartOfTheMonth(Date);
  dteDataFim.Date    := EndOfTheMonth(Date);

  btnFiltrar.Click;

  ShowScrollBar(dbgGridListaPadrao.Handle, SB_HORZ, False);

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFListaRecibos.SalvarBoleano;
begin
  dbgGridListaPadrao.SelectedField.Dataset.Edit;
  dbgGridListaPadrao.SelectedField.AsBoolean := not dbgGridListaPadrao.SelectedField.AsBoolean;
  dbgGridListaPadrao.SelectedField.Dataset.Post;
end;

end.

