{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UDMLancamento.pas
  Descricao:   Data Module de Lancamentos de Despesas e Receitas
  Author   :   Cristina
  Date:        11-abr-2016
  Last Update: 06-fev-2018 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UDMLancamento;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, System.Variants;

type
  TdmLancamento = class(TDataModule)
    qryLancamento_F: TFDQuery;
    qryParcela_F: TFDQuery;
    dspLancamento_F: TDataSetProvider;
    dspParcela_F: TDataSetProvider;
    cdsLancamento_F: TClientDataSet;
    cdsParcela_F: TClientDataSet;
    dsLancamento_F: TDataSource;
    dsParcela_F: TDataSource;
    qryCliFor: TFDQuery;
    qryCategoria: TFDQuery;
    qrySubCategoria: TFDQuery;
    qryFormaPagto: TFDQuery;
    qryItem: TFDQuery;
    dsCliFor: TDataSource;
    dsCategoria: TDataSource;
    dsSubCategoria: TDataSource;
    dsFormaPagto: TDataSource;
    dsItem: TDataSource;
    cdsParcela_FNOVO: TBooleanField;
    cdsParcela_FPRE_CADASTRADO: TBooleanField;
    cdsLancamento_FCOD_LANCAMENTO: TIntegerField;
    cdsLancamento_FANO_LANCAMENTO: TIntegerField;
    cdsLancamento_FTIPO_LANCAMENTO: TStringField;
    cdsLancamento_FID_CLIENTE_FORNECEDOR_FK: TIntegerField;
    cdsLancamento_FID_CATEGORIA_DESPREC_FK: TIntegerField;
    cdsLancamento_FID_SUBCATEGORIA_DESPREC_FK: TIntegerField;
    cdsLancamento_FFLG_IMPOSTORENDA: TStringField;
    cdsLancamento_FFLG_PESSOAL: TStringField;
    cdsLancamento_FFLG_AUXILIAR: TStringField;
    cdsLancamento_FFLG_REAL: TStringField;
    cdsLancamento_FQTD_PARCELAS: TIntegerField;
    cdsLancamento_FVR_TOTAL_PREV: TBCDField;
    cdsLancamento_FVR_TOTAL_JUROS: TBCDField;
    cdsLancamento_FVR_TOTAL_PAGO: TBCDField;
    cdsLancamento_FCAD_ID_USUARIO: TIntegerField;
    cdsLancamento_FFLG_STATUS: TStringField;
    cdsLancamento_FFLG_CANCELADO: TStringField;
    cdsLancamento_FCANCEL_ID_USUARIO: TIntegerField;
    cdsLancamento_FFLG_RECORRENTE: TStringField;
    cdsLancamento_FOBSERVACAO: TStringField;
    cdsParcela_FID_LANCAMENTO_PARC: TIntegerField;
    cdsParcela_FNUM_PARCELA: TIntegerField;
    cdsParcela_FVR_PARCELA_PREV: TBCDField;
    cdsParcela_FVR_PARCELA_JUROS: TBCDField;
    cdsParcela_FVR_PARCELA_PAGO: TBCDField;
    cdsParcela_FFLG_STATUS: TStringField;
    cdsParcela_FCANCEL_ID_USUARIO: TIntegerField;
    cdsParcela_FPAG_ID_USUARIO: TIntegerField;
    cdsParcela_FOBS_LANCAMENTO_PARC: TStringField;
    cdsParcela_FCAD_ID_USUARIO: TIntegerField;
    cdsLancamento_FNUM_RECIBO: TIntegerField;
    cdsParcela_FCOD_LANCAMENTO_FK: TIntegerField;
    cdsParcela_FANO_LANCAMENTO_FK: TIntegerField;
    dsImagem_F: TDataSource;
    cdsImagem_F: TClientDataSet;
    cdsImagem_FNOME_FIXO: TStringField;
    cdsImagem_FNOME_VARIAVEL: TStringField;
    cdsImagem_FEXTENSAO: TStringField;
    cdsImagem_FNOVO: TBooleanField;
    cdsImagem_FORIGEM_ANTIGA: TStringField;
    cdsImagem_FNOME_V_ANTIGO: TStringField;
    cdsImagem_FCAMINHO_COMPLETO: TStringField;
    dspImagem_F: TDataSetProvider;
    qryImagem_F: TFDQuery;
    cdsImagem_FID_LANCAMENTO_IMG: TIntegerField;
    cdsImagem_FCOD_LANCAMENTO_FK: TIntegerField;
    cdsImagem_FANO_LANCAMENTO_FK: TIntegerField;
    qryBanco: TFDQuery;
    dsBanco: TDataSource;
    cdsParcela_FAGENCIA: TStringField;
    cdsParcela_FCONTA: TStringField;
    cdsParcela_FID_BANCO_FK: TIntegerField;
    cdsLancamento_FFLG_REPLICADO: TStringField;
    cdsLancamento_FTIPO_CADASTRO: TStringField;
    cdsLancamento_FID_ORIGEM: TIntegerField;
    cdsLancamento_FID_SISTEMA_FK: TIntegerField;
    qryDetalhe_F: TFDQuery;
    dspDetalhe_F: TDataSetProvider;
    cdsDetalhe_F: TClientDataSet;
    dsDetalhe_F: TDataSource;
    cdsDetalhe_FID_LANCAMENTO_DET: TIntegerField;
    cdsDetalhe_FCOD_LANCAMENTO_FK: TIntegerField;
    cdsDetalhe_FANO_LANCAMENTO_FK: TIntegerField;
    cdsDetalhe_FVR_UNITARIO: TBCDField;
    cdsDetalhe_FQUANTIDADE: TIntegerField;
    cdsDetalhe_FVR_TOTAL: TBCDField;
    cdsDetalhe_FSELO_ORIGEM: TStringField;
    cdsDetalhe_FALEATORIO_ORIGEM: TStringField;
    cdsDetalhe_FTIPO_COBRANCA: TStringField;
    cdsDetalhe_FCOD_ADICIONAL: TIntegerField;
    cdsDetalhe_FEMOLUMENTOS: TBCDField;
    cdsDetalhe_FFETJ: TBCDField;
    cdsDetalhe_FFUNDPERJ: TBCDField;
    cdsDetalhe_FFUNPERJ: TBCDField;
    cdsDetalhe_FFUNARPEN: TBCDField;
    cdsDetalhe_FPMCMV: TBCDField;
    cdsDetalhe_FISS: TBCDField;
    cdsDetalhe_FFLG_CANCELADO: TStringField;
    cdsDetalhe_FNOVO: TBooleanField;
    cdsDetalhe_FPRE_CADASTRADO: TBooleanField;
    cdsLancamento_FID_NATUREZA_FK: TIntegerField;
    dsNatureza: TDataSource;
    qryNatureza: TFDQuery;
    cdsDetalhe_FID_ITEM_FK: TIntegerField;
    cdsDetalhe_FDESCR_ITEM: TStringField;
    cdsDetalhe_FQTD_ANTERIOR: TIntegerField;
    cdsLancamento_FVR_TOTAL_DESCONTO: TBCDField;
    cdsParcela_FVR_PARCELA_DESCONTO: TBCDField;
    cdsParcela_FNUM_CHEQUE: TStringField;
    cdsLancamento_FFLG_FLUTUANTE: TStringField;
    cdsParcela_FID_FORMAPAGAMENTO_FK: TIntegerField;
    cdsDetalhe_FNUM_PROTOCOLO: TIntegerField;
    cdsLancamento_FDATA_CADASTRO: TSQLTimeStampField;
    cdsLancamento_FDATA_LANCAMENTO: TDateField;
    cdsParcela_FDATA_VENCIMENTO: TDateField;
    cdsParcela_FDATA_CADASTRO: TDateField;
    cdsParcela_FDATA_LANCAMENTO_PARC: TDateField;
    cdsParcela_FVR_DIFERENCA: TCurrencyField;
    cdsParcela_FTP_DIFERENCA: TStringField;
    cdsLancamento_FDATA_CANCELAMENTO: TDateField;
    cdsParcela_FDATA_CANCELAMENTO: TDateField;
    cdsParcela_FDATA_PAGAMENTO: TDateField;
    cdsDetalhe_FMUTUA: TBCDField;
    cdsDetalhe_FACOTERJ: TBCDField;
    cdsLancamento_FID_CARNELEAO_EQUIVALENCIA_FK: TIntegerField;
    qryCarneLeao: TFDQuery;
    dsCarneLeao: TDataSource;
    qryNaturezaID_NATUREZA: TIntegerField;
    qryNaturezaDESCR_NATUREZA: TStringField;
    qryNaturezaTIPO_NATUREZA: TStringField;
    qryNaturezaFLG_EDITAVEL: TStringField;
    qryNaturezaFLG_IMPOSTORENDA: TStringField;
    qryNaturezaFLG_AUXILIAR: TStringField;
    qryNaturezaFLG_PESSOAL: TStringField;
    cdsParcela_FDATA_PAGAMENTO_ANT: TDateField;
    cdsLancamento_FFLG_FORAFECHCAIXA: TStringField;
    cdsParcela_FID_FORMAPAGAMENTO_ORIG_FK: TIntegerField;
    cdsParcela_FNUM_COD_DEPOSITO: TStringField;
    procedure cdsLancamento_FVR_TOTAL_PREVGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsLancamento_FVR_TOTAL_JUROSGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsLancamento_FVR_TOTAL_PAGOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsItem_FVR_UNITARIOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsItem_FQUANTIDADEGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsItem_FVALOR_TOTALGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsParcela_FVR_PARCELA_PREVGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsParcela_FVR_PARCELA_JUROSGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsParcela_FVR_PARCELA_PAGOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsImagem_FCalcFields(DataSet: TDataSet);
    procedure cdsDetalhe_FCalcFields(DataSet: TDataSet);
    procedure cdsDetalhe_FVR_UNITARIOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsDetalhe_FVR_TOTALGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsDetalhe_FQUANTIDADEGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsDetalhe_FEMOLUMENTOSGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsDetalhe_FFETJGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsDetalhe_FFUNDPERJGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsDetalhe_FFUNPERJGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsDetalhe_FFUNARPENGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsDetalhe_FPMCMVGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsDetalhe_FISSGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsLancamento_FVR_TOTAL_DESCONTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsParcela_FVR_PARCELA_DESCONTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsDetalhe_FMUTUAGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsDetalhe_FACOTERJGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure QuitarLancamento;
  end;

var
  dmLancamento: TdmLancamento;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{$R *.dfm}

procedure TdmLancamento.cdsDetalhe_FACOTERJGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsDetalhe_FCalcFields(DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with qryAux, SQL do
  begin
    //Item
    if cdsDetalhe_F.FieldByName('ID_ITEM_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT ID_ITEM, DESCR_ITEM ' +
              '  FROM ITEM ' +
              ' WHERE ID_ITEM = :ID_ITEM';
      Params.ParamByName('ID_ITEM').AsInteger := cdsDetalhe_F.FieldByName('ID_ITEM_FK').AsInteger;
      Open;

      cdsDetalhe_F.FieldByName('DESCR_ITEM').AsString := qryAux.FieldByName('DESCR_ITEM').AsString;
    end;
  end;

  FreeAndNil(qryAux);
end;

procedure TdmLancamento.cdsDetalhe_FEMOLUMENTOSGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsDetalhe_FFETJGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsDetalhe_FFUNARPENGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsDetalhe_FFUNDPERJGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsDetalhe_FFUNPERJGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsDetalhe_FISSGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsDetalhe_FMUTUAGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsDetalhe_FPMCMVGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsDetalhe_FQUANTIDADEGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsDetalhe_FVR_TOTALGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsDetalhe_FVR_UNITARIOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsImagem_FCalcFields(DataSet: TDataSet);
var
  sArquivo: String;
begin
  sArquivo := vgConf_DiretorioImagens +
              IntToStr(vgLanc_Ano) +
              '\' +
              cdsImagem_F.FieldByName('NOME_FIXO').AsString +
              cdsImagem_F.FieldByName('NOME_VARIAVEL').AsString + '.' +
              cdsImagem_F.FieldByName('EXTENSAO').AsString;

  if FileExists(sArquivo) then
    cdsImagem_F.FieldByName('CAMINHO_COMPLETO').AsString := sArquivo;
end;

procedure TdmLancamento.cdsItem_FQUANTIDADEGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsItem_FVALOR_TOTALGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsItem_FVR_UNITARIOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsLancamento_FVR_TOTAL_DESCONTOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsLancamento_FVR_TOTAL_JUROSGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsLancamento_FVR_TOTAL_PAGOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsLancamento_FVR_TOTAL_PREVGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsParcela_FVR_PARCELA_DESCONTOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsParcela_FVR_PARCELA_JUROSGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsParcela_FVR_PARCELA_PAGOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.cdsParcela_FVR_PARCELA_PREVGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmLancamento.QuitarLancamento;
var
  QryAuxS: TFDQuery;
begin
  QryAuxS := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    QryAuxS.Close;
    QryAuxS.SQL.Clear;
    QryAuxS.SQL.Text := 'SELECT SUM(VR_PARCELA_PREV) AS TOTAL_PREV, ' +
                        '       SUM(VR_PARCELA_JUROS) AS TOTAL_JUROS, ' +
                        '       SUM(VR_PARCELA_DESCONTO) AS TOTAL_DESCONTO, ' +
                        '       SUM(VR_PARCELA_PAGO) AS TOTAL_PAGO ' +
                        '  FROM LANCAMENTO_PARC LP ' +
                        ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                        '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO';
    QryAuxS.Params.ParamByName('COD_LANCAMENTO').Value := vgLanc_Codigo;
    QryAuxS.Params.ParamByName('ANO_LANCAMENTO').Value := vgLanc_Ano;
    QryAuxS.Open;

    { DESPESA/RECEITA }
    cdsLancamento_F.Close;
    cdsLancamento_F.Params.ParamByName('COD_LANCAMENTO').Value := vgLanc_Codigo;
    cdsLancamento_F.Params.ParamByName('ANO_LANCAMENTO').Value := vgLanc_Ano;
    cdsLancamento_F.Open;

    dmPrincipal.InicializarComponenteLancamento;

    { LANCAMENTO }
    DadosLancamento := Lancamento.Create;

    DadosLancamento.CodLancamento    := vgLanc_Codigo;  //cdsLancamento_F.FieldByName('COD_LANCAMENTO').AsInteger;
    DadosLancamento.AnoLancamento    := vgLanc_Ano;  //cdsLancamento_F.FieldByName('ANO_LANCAMENTO').AsInteger;
    DadosLancamento.VlrTotalPrev     := QryAuxS.FieldByName('TOTAL_PREV').AsCurrency;
    DadosLancamento.VlrTotalJuros    := QryAuxS.FieldByName('TOTAL_JUROS').AsCurrency;
    DadosLancamento.VlrTotalDesconto := QryAuxS.FieldByName('TOTAL_DESCONTO').AsCurrency;
    DadosLancamento.VlrTotalPago     := QryAuxS.FieldByName('TOTAL_PAGO').AsCurrency;
    DadosLancamento.FlgStatus        := 'G';

    ListaLancamentos.Add(DadosLancamento);

    dmPrincipal.UpdateLancamento(0);

    { LOG - DESPESA/RECEITA }
    QryAuxS.Close;
    QryAuxS.SQL.Clear;
    QryAuxS.SQL.Text := 'INSERT INTO USUARIO_LOG (ID_USUARIO_LOG, ID_USUARIO, ID_MODULO_FK, ' +
                        '                         TIPO_LANCAMENTO, CODIGO_LAN, ANO_LAN, ID_ORIGEM, ' +
                        '                         TABELA_ORIGEM, TIPO_ACAO,  OBS_USUARIO_LOG) ' +
                        '                 VALUES (:ID_USUARIO_LOG, :ID_USUARIO, :ID_MODULO, ' +
                        '                         :TIPO_LANCAMENTO, :CODIGO_LAN, :ANO_LAN, :ID_ORIGEM, ' +
                        '                         :TABELA_ORIGEM, :TIPO_ACAO,  :OBS_USUARIO_LOG)';

    QryAuxS.Params.ParamByName('ID_USUARIO_LOG').Value  := BS.ProximoId('ID_USUARIO_LOG', 'USUARIO_LOG');
    QryAuxS.Params.ParamByName('ID_USUARIO').Value      := vgUsu_Id;
    QryAuxS.Params.ParamByName('ID_MODULO').Value       := 2;
    QryAuxS.Params.ParamByName('TIPO_LANCAMENTO').Value := vgLanc_TpLanc;
    QryAuxS.Params.ParamByName('CODIGO_LAN').Value      := vgLanc_Codigo;  //ListaLancamentos[0].CodLancamento;
    QryAuxS.Params.ParamByName('ANO_LAN').Value         := vgLanc_Ano;  //ListaLancamentos[0].AnoLancamento;
    QryAuxS.Params.ParamByName('ID_ORIGEM').Value       := Null;
    QryAuxS.Params.ParamByName('TABELA_ORIGEM').Value   := Null;
    QryAuxS.Params.ParamByName('TIPO_ACAO').Value       := 'E';
    QryAuxS.Params.ParamByName('OBS_USUARIO_LOG').Value := 'Baixa completa de Lanšamento.';

    QryAuxS.ExecSQL;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    dmPrincipal.FinalizarComponenteLancamento;

    FreeAndNil(QryAuxS);
  except
    on E: Exception do
    begin
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      dmPrincipal.FinalizarComponenteLancamento;
      FreeAndNil(QryAuxS);
    end;
  end;
end;

end.
