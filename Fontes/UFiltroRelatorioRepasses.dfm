inherited FFiltroRelatorioRepasses: TFFiltroRelatorioRepasses
  Caption = 'Relat'#243'rio de Repasses'
  ClientHeight = 253
  ClientWidth = 340
  ExplicitWidth = 346
  ExplicitHeight = 282
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlFiltro: TPanel
    Width = 334
    Height = 247
    ExplicitWidth = 334
    ExplicitHeight = 277
    object lblPeriodo: TLabel [0]
      Left = 5
      Top = 8
      Width = 130
      Height = 14
      Caption = 'Per'#237'odo de Lan'#231'amento'
    end
    object lblPeriodoInicio: TLabel [1]
      Left = 5
      Top = 31
      Width = 32
      Height = 14
      Caption = 'In'#237'cio:'
    end
    object lblPeriodoFim: TLabel [2]
      Left = 149
      Top = 31
      Width = 22
      Height = 14
      Caption = 'Fim:'
    end
    inherited pnlBotoes: TPanel
      Top = 200
      Width = 334
      TabOrder = 7
      ExplicitTop = 230
      ExplicitWidth = 334
      inherited btnVisualizar: TJvTransparentButton
        Left = 116
        Top = 0
        ExplicitLeft = 116
        ExplicitTop = 0
      end
      inherited btnCancelar: TJvTransparentButton
        Left = 224
        Top = 0
        ExplicitLeft = 224
        ExplicitTop = 0
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 8
        Top = 0
        OnClick = btnLimparClick
        ExplicitLeft = 8
        ExplicitTop = 0
      end
    end
    inherited gbTipo: TGroupBox
      Top = 131
      TabOrder = 3
      ExplicitTop = 131
    end
    inherited chbExibirGrafico: TCheckBox
      Top = 133
      TabOrder = 4
      ExplicitTop = 133
    end
    inherited chbExportarPDF: TCheckBox
      Top = 156
      TabOrder = 5
      ExplicitTop = 156
    end
    inherited chbExportarExcel: TCheckBox
      Top = 179
      TabOrder = 6
      ExplicitTop = 179
    end
    object dteDataInicio: TJvDateEdit
      Left = 43
      Top = 28
      Width = 100
      Height = 22
      Color = 16114127
      ShowNullDate = False
      TabOrder = 0
      OnKeyPress = FormKeyPress
    end
    object dteDataFim: TJvDateEdit
      Left = 177
      Top = 28
      Width = 100
      Height = 22
      Color = 16114127
      ShowNullDate = False
      TabOrder = 1
      OnKeyPress = FormKeyPress
    end
    object gbTipoRepasse: TGroupBox
      Left = 5
      Top = 56
      Width = 324
      Height = 69
      Caption = 'Tipo Repasse'
      TabOrder = 2
      object chbFUNDPERJ: TCheckBox
        Left = 5
        Top = 18
        Width = 77
        Height = 17
        Caption = 'FUNDPERJ'
        TabOrder = 0
        OnKeyPress = FormKeyPress
      end
      object chbFUNPERJ: TCheckBox
        Left = 126
        Top = 18
        Width = 69
        Height = 17
        Caption = 'FUNPERJ'
        TabOrder = 1
        OnKeyPress = FormKeyPress
      end
      object chbFUNARPEN: TCheckBox
        Left = 239
        Top = 18
        Width = 80
        Height = 17
        Caption = 'FUNARPEN'
        TabOrder = 2
        OnKeyPress = FormKeyPress
      end
      object chbFETJ: TCheckBox
        Left = 5
        Top = 43
        Width = 47
        Height = 17
        Caption = 'FETJ'
        TabOrder = 3
        OnKeyPress = FormKeyPress
      end
      object chbISS: TCheckBox
        Left = 83
        Top = 43
        Width = 38
        Height = 17
        Caption = 'ISS'
        TabOrder = 4
        OnKeyPress = FormKeyPress
      end
      object chbMUTUA: TCheckBox
        Left = 154
        Top = 43
        Width = 62
        Height = 17
        Caption = 'M'#218'TUA'
        TabOrder = 5
        OnKeyPress = FormKeyPress
      end
      object chbACOTERJ: TCheckBox
        Left = 247
        Top = 43
        Width = 72
        Height = 17
        Caption = 'ACOTERJ'
        TabOrder = 6
        OnKeyPress = FormKeyPress
      end
    end
  end
end
