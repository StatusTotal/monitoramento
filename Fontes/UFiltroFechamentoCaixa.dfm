inherited FFiltroFechamentoCaixa: TFFiltroFechamentoCaixa
  Caption = 'Filtro de Fechamento de Caixa'
  ClientHeight = 349
  ClientWidth = 902
  OnCreate = FormCreate
  ExplicitWidth = 908
  ExplicitHeight = 378
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 896
    Height = 343
    ExplicitWidth = 896
    ExplicitHeight = 343
    inherited pnlGrid: TPanel
      Top = 52
      Width = 812
      ExplicitTop = 52
      ExplicitWidth = 812
      inherited pnlGridPai: TPanel
        Width = 812
        ExplicitWidth = 812
        inherited dbgGridPaiPadrao: TDBGrid
          Width = 812
          OnTitleClick = dbgGridPaiPadraoTitleClick
          Columns = <
            item
              Expanded = False
              FieldName = 'ID'
              Visible = False
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'DATA_FECHAMENTO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Title.Alignment = taCenter
              Title.Caption = 'Data'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -12
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 85
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'HORA_FECHAMENTO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Title.Alignment = taCenter
              Title.Caption = 'Hora'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -12
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VR_TOTAL_FECHAMENTO'
              Title.Caption = 'Vlr. Tot. Fecham.'
              Width = 110
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VR_TOTAL_ULT_AB'
              Title.Caption = 'Vlr. Tot. '#218'lt. Ab.'
              Width = 110
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VR_TOTAL_RECEITA'
              Title.Caption = 'Vlr. Tot. Receitas'
              Width = 110
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VR_TOTAL_DESPESA'
              Title.Caption = 'Vlr. Tot. Despesas'
              Width = 110
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VR_NOTAS'
              Title.Caption = 'Vlr. Tot. Notas'
              Width = 110
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VR_MOEDAS'
              Title.Caption = 'Vlr. Tot. Moedas'
              Width = 110
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VR_CONTACORRENTE'
              Title.Caption = 'Vlr. Tot.  C/C'
              Width = 110
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VR_DIFERENCA'
              Title.Caption = 'Vlr. Tot. Diferen'#231'a'
              Width = 110
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CAD_ID_USUARIO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'NOME_USUARIO'
              Title.Caption = 'Usu'#225'rio Respons'#225'vel'
              Width = 125
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'FLG_CANCELADO'
              Title.Alignment = taCenter
              Title.Caption = 'Cancel.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATA_CANCELAMENTO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'CANCEL_ID_USUARIO'
              Visible = False
            end>
        end
      end
    end
    inherited pnlFiltro: TPanel
      Width = 896
      Height = 49
      ExplicitWidth = 896
      ExplicitHeight = 49
      inherited btnFiltrar: TJvTransparentButton
        Left = 721
        Top = 20
        ExplicitLeft = 721
        ExplicitTop = 20
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 810
        Top = 20
        ExplicitLeft = 810
        ExplicitTop = 20
      end
      object lblPeriodoFechamento: TLabel
        Left = 81
        Top = 8
        Width = 131
        Height = 14
        Caption = 'Per'#237'odo de Fechamento'
      end
      object Label3: TLabel
        Left = 187
        Top = 27
        Width = 6
        Height = 14
        Caption = 'a'
      end
      object dteIniPeriodoFechamento: TJvDateEdit
        Left = 81
        Top = 24
        Width = 100
        Height = 22
        Color = 16114127
        ShowNullDate = False
        TabOrder = 0
        OnKeyPress = dteIniPeriodoFechamentoKeyPress
      end
      object dteFimPeriodoFechamento: TJvDateEdit
        Left = 199
        Top = 24
        Width = 100
        Height = 22
        Color = 16114127
        ShowNullDate = False
        TabOrder = 1
        OnKeyPress = dteFimPeriodoFechamentoKeyPress
      end
      object rgSituacao: TRadioGroup
        Left = 305
        Top = 5
        Width = 344
        Height = 41
        Caption = 'Situa'#231#227'o'
        Columns = 3
        ItemIndex = 1
        Items.Strings = (
          'Todas'
          'N'#227'o Cancelada'
          'Cancelada')
        TabOrder = 2
        OnClick = rgSituacaoClick
        OnExit = rgSituacaoExit
      end
    end
    inherited pnlMenu: TPanel
      Top = 52
      ExplicitTop = 52
    end
  end
  inherited dsGridPaiPadrao: TDataSource
    Left = 820
    Top = 243
  end
  inherited qryGridPaiPadrao: TFDQuery
    OnCalcFields = qryGridPaiPadraoCalcFields
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT ID_CAIXA_FECHAMENTO AS ID,'
      '       DATA_FECHAMENTO,'
      '       HORA_FECHAMENTO,'
      '       VR_TOTAL_ULT_AB,'
      '       VR_RECIBO_IMPORTADO,'
      '       VR_IMPORT_DINHEIRO,'
      '       VR_IMPORT_DEPOSITO,'
      '       VR_IMPORT_CARTAO,'
      '       VR_IMPORT_CHEQUE,'
      '       VR_IMPORT_PROMISSORIA,'
      '       VR_IMPORT_FATURADO,'
      '       VR_IMPORT_REPASSES,'
      '       VR_OUTRASREC_TOTAL,'
      '       VR_OUTRASREC_DINHEIRO,'
      '       VR_OUTRASREC_DEPOSITO,'
      '       VR_OUTRASREC_CARTAO,'
      '       VR_OUTRASREC_CHEQUE,'
      '       VR_OUTRASREC_PROMISSORIA,'
      '       VR_OUTRASREC_FATURADO,'
      '       VR_OUTRASREC_REPASSES,'
      '       VR_SRVANT_CARTAO,'
      '       VR_SRVANT_CHEQUE,'
      '       VR_SRVANT_PROMISSORIA,'
      '       VR_SRVANT_FATURADO,'
      '       VR_RECPEND_CARTAO,'
      '       VR_RECPEND_CHEQUE,'
      '       VR_RECPEND_PROMISSORIA,'
      '       VR_RECPEND_FATURADO,'
      '       VR_FLUT_DEPOSITO,'
      '       VR_FLUT_REPASSE,'
      '       VR_FECH_ANTERIOR,'
      '       VR_TOTAL_RECEITA,'
      '       VR_TOTAL_DESPESA,'
      '       VR_SALDO_DIA,'
      '       VR_DESP_DINHEIRO,'
      '       VR_DESP_DESCONTO,'
      '       VR_DESP_REPASSE,'
      '       VR_NOTAS,'
      '       VR_MOEDAS,'
      '       VR_CONTACORRENTE,'
      '       VR_CONTAGEM_FINAL,'
      '       VR_TOTAL_FECHAMENTO,'
      '       VR_DIFERENCA,'
      '       OBS_CAIXA_FECHAMENTO,'
      '       CAD_ID_USUARIO,'
      '       FLG_CANCELADO,'
      '       DATA_CANCELAMENTO,'
      '       CANCEL_ID_USUARIO'
      '  FROM CAIXA_FECHAMENTO'
      ' WHERE DATA_FECHAMENTO BETWEEN :DATA_INI AND :DATA_FIM'
      '   AND (FLG_CANCELADO = :SIT1'
      '    OR  FLG_CANCELADO = :SIT2)'
      'ORDER BY DATA_FECHAMENTO DESC,'
      '         HORA_FECHAMENTO DESC')
    Left = 820
    Top = 191
    ParamData = <
      item
        Name = 'DATA_INI'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'DATA_FIM'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'SIT1'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'SIT2'
        DataType = ftString
        ParamType = ptInput
      end>
    object qryGridPaiPadraoID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID_CAIXA_FECHAMENTO'
      Required = True
    end
    object qryGridPaiPadraoDATA_FECHAMENTO: TDateField
      FieldName = 'DATA_FECHAMENTO'
      Origin = 'DATA_FECHAMENTO'
    end
    object qryGridPaiPadraoHORA_FECHAMENTO: TTimeField
      FieldName = 'HORA_FECHAMENTO'
      Origin = 'HORA_FECHAMENTO'
    end
    object qryGridPaiPadraoVR_TOTAL_ULT_AB: TBCDField
      FieldName = 'VR_TOTAL_ULT_AB'
      Origin = 'VR_TOTAL_ULT_AB'
      OnGetText = qryGridPaiPadraoVR_TOTAL_ULT_ABGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_RECIBO_IMPORTADO: TBCDField
      FieldName = 'VR_RECIBO_IMPORTADO'
      Origin = 'VR_RECIBO_IMPORTADO'
      OnGetText = qryGridPaiPadraoVR_RECIBO_IMPORTADOGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_IMPORT_DINHEIRO: TBCDField
      FieldName = 'VR_IMPORT_DINHEIRO'
      Origin = 'VR_IMPORT_DINHEIRO'
      OnGetText = qryGridPaiPadraoVR_IMPORT_DINHEIROGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_IMPORT_DEPOSITO: TBCDField
      FieldName = 'VR_IMPORT_DEPOSITO'
      Origin = 'VR_IMPORT_DEPOSITO'
      OnGetText = qryGridPaiPadraoVR_IMPORT_DEPOSITOGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_IMPORT_CARTAO: TBCDField
      FieldName = 'VR_IMPORT_CARTAO'
      Origin = 'VR_IMPORT_CARTAO'
      OnGetText = qryGridPaiPadraoVR_IMPORT_CARTAOGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_IMPORT_CHEQUE: TBCDField
      FieldName = 'VR_IMPORT_CHEQUE'
      Origin = 'VR_IMPORT_CHEQUE'
      OnGetText = qryGridPaiPadraoVR_IMPORT_CHEQUEGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_IMPORT_PROMISSORIA: TBCDField
      FieldName = 'VR_IMPORT_PROMISSORIA'
      Origin = 'VR_IMPORT_PROMISSORIA'
      OnGetText = qryGridPaiPadraoVR_IMPORT_PROMISSORIAGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_IMPORT_FATURADO: TBCDField
      FieldName = 'VR_IMPORT_FATURADO'
      Origin = 'VR_IMPORT_FATURADO'
      OnGetText = qryGridPaiPadraoVR_IMPORT_FATURADOGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_IMPORT_REPASSES: TBCDField
      FieldName = 'VR_IMPORT_REPASSES'
      Origin = 'VR_IMPORT_REPASSES'
      OnGetText = qryGridPaiPadraoVR_IMPORT_REPASSESGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_OUTRASREC_TOTAL: TBCDField
      FieldName = 'VR_OUTRASREC_TOTAL'
      Origin = 'VR_OUTRASREC_TOTAL'
      OnGetText = qryGridPaiPadraoVR_OUTRASREC_TOTALGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_OUTRASREC_DINHEIRO: TBCDField
      FieldName = 'VR_OUTRASREC_DINHEIRO'
      Origin = 'VR_OUTRASREC_DINHEIRO'
      OnGetText = qryGridPaiPadraoVR_OUTRASREC_DINHEIROGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_OUTRASREC_DEPOSITO: TBCDField
      FieldName = 'VR_OUTRASREC_DEPOSITO'
      Origin = 'VR_OUTRASREC_DEPOSITO'
      OnGetText = qryGridPaiPadraoVR_OUTRASREC_DEPOSITOGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_OUTRASREC_CARTAO: TBCDField
      FieldName = 'VR_OUTRASREC_CARTAO'
      Origin = 'VR_OUTRASREC_CARTAO'
      OnGetText = qryGridPaiPadraoVR_OUTRASREC_CARTAOGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_OUTRASREC_CHEQUE: TBCDField
      FieldName = 'VR_OUTRASREC_CHEQUE'
      Origin = 'VR_OUTRASREC_CHEQUE'
      OnGetText = qryGridPaiPadraoVR_OUTRASREC_CHEQUEGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_OUTRASREC_PROMISSORIA: TBCDField
      FieldName = 'VR_OUTRASREC_PROMISSORIA'
      Origin = 'VR_OUTRASREC_PROMISSORIA'
      OnGetText = qryGridPaiPadraoVR_OUTRASREC_PROMISSORIAGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_OUTRASREC_FATURADO: TBCDField
      FieldName = 'VR_OUTRASREC_FATURADO'
      Origin = 'VR_OUTRASREC_FATURADO'
      OnGetText = qryGridPaiPadraoVR_OUTRASREC_FATURADOGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_OUTRASREC_REPASSES: TBCDField
      FieldName = 'VR_OUTRASREC_REPASSES'
      Origin = 'VR_OUTRASREC_REPASSES'
      OnGetText = qryGridPaiPadraoVR_OUTRASREC_REPASSESGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_SRVANT_CARTAO: TBCDField
      FieldName = 'VR_SRVANT_CARTAO'
      Origin = 'VR_SRVANT_CARTAO'
      OnGetText = qryGridPaiPadraoVR_SRVANT_CARTAOGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_SRVANT_CHEQUE: TBCDField
      FieldName = 'VR_SRVANT_CHEQUE'
      Origin = 'VR_SRVANT_CHEQUE'
      OnGetText = qryGridPaiPadraoVR_SRVANT_CHEQUEGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_SRVANT_PROMISSORIA: TBCDField
      FieldName = 'VR_SRVANT_PROMISSORIA'
      Origin = 'VR_SRVANT_PROMISSORIA'
      OnGetText = qryGridPaiPadraoVR_SRVANT_PROMISSORIAGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_SRVANT_FATURADO: TBCDField
      FieldName = 'VR_SRVANT_FATURADO'
      Origin = 'VR_SRVANT_FATURADO'
      OnGetText = qryGridPaiPadraoVR_SRVANT_FATURADOGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_RECPEND_CARTAO: TBCDField
      FieldName = 'VR_RECPEND_CARTAO'
      Origin = 'VR_RECPEND_CARTAO'
      OnGetText = qryGridPaiPadraoVR_RECPEND_CARTAOGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_RECPEND_CHEQUE: TBCDField
      FieldName = 'VR_RECPEND_CHEQUE'
      Origin = 'VR_RECPEND_CHEQUE'
      OnGetText = qryGridPaiPadraoVR_RECPEND_CHEQUEGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_RECPEND_PROMISSORIA: TBCDField
      FieldName = 'VR_RECPEND_PROMISSORIA'
      Origin = 'VR_RECPEND_PROMISSORIA'
      OnGetText = qryGridPaiPadraoVR_RECPEND_PROMISSORIAGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_RECPEND_FATURADO: TBCDField
      FieldName = 'VR_RECPEND_FATURADO'
      Origin = 'VR_RECPEND_FATURADO'
      OnGetText = qryGridPaiPadraoVR_RECPEND_FATURADOGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_FLUT_DEPOSITO: TBCDField
      FieldName = 'VR_FLUT_DEPOSITO'
      Origin = 'VR_FLUT_DEPOSITO'
      OnGetText = qryGridPaiPadraoVR_FLUT_DEPOSITOGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_FLUT_REPASSE: TBCDField
      FieldName = 'VR_FLUT_REPASSE'
      Origin = 'VR_FLUT_REPASSE'
      OnGetText = qryGridPaiPadraoVR_FLUT_REPASSEGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_FECH_ANTERIOR: TBCDField
      FieldName = 'VR_FECH_ANTERIOR'
      Origin = 'VR_FECH_ANTERIOR'
      OnGetText = qryGridPaiPadraoVR_FECH_ANTERIORGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_TOTAL_RECEITA: TBCDField
      FieldName = 'VR_TOTAL_RECEITA'
      Origin = 'VR_TOTAL_RECEITA'
      OnGetText = qryGridPaiPadraoVR_TOTAL_RECEITAGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_TOTAL_DESPESA: TBCDField
      FieldName = 'VR_TOTAL_DESPESA'
      Origin = 'VR_TOTAL_DESPESA'
      OnGetText = qryGridPaiPadraoVR_TOTAL_DESPESAGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_SALDO_DIA: TBCDField
      FieldName = 'VR_SALDO_DIA'
      Origin = 'VR_SALDO_DIA'
      OnGetText = qryGridPaiPadraoVR_SALDO_DIAGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_DESP_DINHEIRO: TBCDField
      FieldName = 'VR_DESP_DINHEIRO'
      Origin = 'VR_DESP_DINHEIRO'
      OnGetText = qryGridPaiPadraoVR_DESP_DINHEIROGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_DESP_DESCONTO: TBCDField
      FieldName = 'VR_DESP_DESCONTO'
      Origin = 'VR_DESP_DESCONTO'
      OnGetText = qryGridPaiPadraoVR_DESP_DESCONTOGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_DESP_REPASSE: TBCDField
      FieldName = 'VR_DESP_REPASSE'
      Origin = 'VR_DESP_REPASSE'
      OnGetText = qryGridPaiPadraoVR_DESP_REPASSEGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_NOTAS: TBCDField
      FieldName = 'VR_NOTAS'
      Origin = 'VR_NOTAS'
      OnGetText = qryGridPaiPadraoVR_NOTASGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_MOEDAS: TBCDField
      FieldName = 'VR_MOEDAS'
      Origin = 'VR_MOEDAS'
      OnGetText = qryGridPaiPadraoVR_MOEDASGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_CONTACORRENTE: TBCDField
      FieldName = 'VR_CONTACORRENTE'
      Origin = 'VR_CONTACORRENTE'
      OnGetText = qryGridPaiPadraoVR_CONTACORRENTEGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_CONTAGEM_FINAL: TBCDField
      FieldName = 'VR_CONTAGEM_FINAL'
      Origin = 'VR_CONTAGEM_FINAL'
      OnGetText = qryGridPaiPadraoVR_CONTAGEM_FINALGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_TOTAL_FECHAMENTO: TBCDField
      FieldName = 'VR_TOTAL_FECHAMENTO'
      Origin = 'VR_TOTAL_FECHAMENTO'
      OnGetText = qryGridPaiPadraoVR_TOTAL_FECHAMENTOGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoVR_DIFERENCA: TBCDField
      FieldName = 'VR_DIFERENCA'
      Origin = 'VR_DIFERENCA'
      OnGetText = qryGridPaiPadraoVR_DIFERENCAGetText
      Precision = 18
      Size = 2
    end
    object qryGridPaiPadraoOBS_CAIXA_FECHAMENTO: TStringField
      FieldName = 'OBS_CAIXA_FECHAMENTO'
      Origin = 'OBS_CAIXA_FECHAMENTO'
      Size = 1000
    end
    object qryGridPaiPadraoCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
      Origin = 'CAD_ID_USUARIO'
    end
    object qryGridPaiPadraoFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      Origin = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object qryGridPaiPadraoDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
      Origin = 'DATA_CANCELAMENTO'
    end
    object qryGridPaiPadraoCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
      Origin = 'CANCEL_ID_USUARIO'
    end
    object qryGridPaiPadraoNOME_USUARIO: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOME_USUARIO'
      Size = 30
    end
  end
end
