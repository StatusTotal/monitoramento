inherited FFiltroMovimentacaoCaixa: TFFiltroMovimentacaoCaixa
  Caption = 'Filtro de Movimenta'#231#227'o de Caixa'
  ClientHeight = 348
  ClientWidth = 521
  OnCreate = FormCreate
  ExplicitWidth = 527
  ExplicitHeight = 377
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 515
    Height = 342
    ExplicitWidth = 515
    ExplicitHeight = 342
    inherited pnlGrid: TPanel
      Top = 51
      Width = 431
      ExplicitTop = 51
      ExplicitWidth = 431
      inherited pnlGridPai: TPanel
        Width = 431
        ExplicitWidth = 431
        inherited dbgGridPaiPadrao: TDBGrid
          Width = 431
          OnTitleClick = dbgGridPaiPadraoTitleClick
          Columns = <
            item
              Expanded = False
              FieldName = 'ID'
              Visible = False
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'DATA_MOVIMENTO'
              Title.Alignment = taCenter
              Title.Caption = 'Dt. Movimento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DESCR_MOVIMENTO'
              Title.Caption = 'Descri'#231#227'o'
              Width = 220
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TIPO_ORIGEM'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'ID_ORIGEM'
              Visible = False
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'TIPO_LANCAMENTO'
              Title.Alignment = taCenter
              Title.Caption = 'Tipo'
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NUM_RECIBO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Title.Alignment = taCenter
              Title.Caption = 'Recibo'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -12
              Title.Font.Name = 'Tahoma'
              Title.Font.Style = [fsBold]
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FLG_CANCELADO'
              Visible = False
            end>
        end
      end
    end
    inherited pnlFiltro: TPanel
      Width = 515
      Height = 48
      ExplicitWidth = 515
      ExplicitHeight = 48
      inherited btnFiltrar: TJvTransparentButton
        Left = 340
        Top = 20
        ExplicitLeft = 340
        ExplicitTop = 20
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 429
        Top = 20
        ExplicitLeft = 429
        ExplicitTop = 20
      end
      object lblData: TLabel
        Left = 81
        Top = 8
        Width = 107
        Height = 14
        Caption = 'Data do Movimento'
      end
      object dteData: TJvDateEdit
        Left = 81
        Top = 24
        Width = 100
        Height = 22
        Color = 16114127
        ShowNullDate = False
        TabOrder = 0
        OnKeyPress = dteDataKeyPress
      end
    end
    inherited pnlMenu: TPanel
      Top = 51
      ExplicitTop = 51
      inherited btnIncluir: TJvTransparentButton
        Top = 72
        Visible = False
        ExplicitTop = 144
      end
      inherited btnEditar: TJvTransparentButton
        Top = 144
        Visible = False
        ExplicitTop = 216
      end
      inherited btnExcluir: TJvTransparentButton
        Top = 216
        Visible = False
        ExplicitTop = 294
      end
      inherited btnImprimir: TJvTransparentButton
        Top = 0
        ExplicitLeft = -6
        ExplicitTop = -25
      end
    end
  end
  inherited dsGridPaiPadrao: TDataSource
    Left = 452
    Top = 275
  end
  inherited qryGridPaiPadrao: TFDQuery
    OnCalcFields = qryGridPaiPadraoCalcFields
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT ID_MOVIMENTO AS ID,'
      '       DATA_MOVIMENTO,'
      '       DESCR_MOVIMENTO,'
      '       TIPO_ORIGEM,'
      '       ID_ORIGEM,'
      '       NUM_RECIBO,'
      '       FLG_CANCELADO'
      '  FROM MOVIMENTO'
      ' WHERE DATA_MOVIMENTO BETWEEN :DATA_INI AND :DATA_FIM'
      '   AND FLG_CANCELADO = '#39'N'#39
      'ORDER BY ID_MOVIMENTO DESC')
    Left = 452
    Top = 223
    ParamData = <
      item
        Name = 'DATA_INI'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'DATA_FIM'
        DataType = ftDate
        ParamType = ptInput
      end>
    object qryGridPaiPadraoID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID_MOVIMENTO'
      Required = True
    end
    object qryGridPaiPadraoDATA_MOVIMENTO: TDateField
      FieldName = 'DATA_MOVIMENTO'
      Origin = 'DATA_MOVIMENTO'
    end
    object qryGridPaiPadraoDESCR_MOVIMENTO: TStringField
      FieldName = 'DESCR_MOVIMENTO'
      Origin = 'DESCR_MOVIMENTO'
      Size = 150
    end
    object qryGridPaiPadraoTIPO_ORIGEM: TStringField
      FieldName = 'TIPO_ORIGEM'
      Origin = 'TIPO_ORIGEM'
      FixedChar = True
      Size = 1
    end
    object qryGridPaiPadraoID_ORIGEM: TIntegerField
      FieldName = 'ID_ORIGEM'
      Origin = 'ID_ORIGEM'
    end
    object qryGridPaiPadraoNUM_RECIBO: TIntegerField
      FieldName = 'NUM_RECIBO'
      Origin = 'NUM_RECIBO'
    end
    object qryGridPaiPadraoTIPO_LANCAMENTO: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'TIPO_LANCAMENTO'
      Size = 1
    end
    object qryGridPaiPadraoFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      Origin = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
  end
end
