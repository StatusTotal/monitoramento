{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroRelatorioDespesasColaboradores.pas
  Descricao:   Filtro Relatorios de Despesas com Colaboradores (R12 - R12DET)
  Author   :   Cristina
  Date:        08-mai-2017
  Last Update: 11-out-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroRelatorioDespesasColaboradores;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroRelatorioPadrao, Vcl.StdCtrls,
  JvExControls, JvButton, JvTransparentButton, Vcl.ExtCtrls, Vcl.Mask, JvExMask,
  JvToolEdit, Vcl.DBCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.DateUtils, System.StrUtils;

type
  TFFiltroRelatorioDespesasColaboradores = class(TFFiltroRelatorioPadrao)
    lblPeriodo: TLabel;
    lblPeriodoInicio: TLabel;
    dteDataInicio: TJvDateEdit;
    lblPeriodoFim: TLabel;
    dteDataFim: TJvDateEdit;
    rgFuncionario: TRadioGroup;
    lcbFuncionario: TDBLookupComboBox;
    qryFuncionario: TFDQuery;
    dsFuncionario: TDataSource;
    chbComRecibo: TCheckBox;
    GroupBox1: TGroupBox;
    chbComissao: TCheckBox;
    chbVale: TCheckBox;
    chbOutrasDespesas: TCheckBox;
    chbSalario: TCheckBox;
    procedure btnLimparClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure rgFuncionarioClick(Sender: TObject);
    procedure rgFuncionarioExit(Sender: TObject);
  protected
    procedure GravarLog; override;
    procedure GerarRelatorio(ImpDet: Boolean); override;
    function VerificarFiltro: Boolean; override;
  private
    { Private declarations }

    lImprimirRecibos: Boolean;
  public
    { Public declarations }
  end;

var
  FFiltroRelatorioDespesasColaboradores: TFFiltroRelatorioDespesasColaboradores;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMRelatorios;

{ TFFiltroRelatorioDespesasColaboradores }

procedure TFFiltroRelatorioDespesasColaboradores.btnLimparClick(
  Sender: TObject);
begin
  inherited;

  dteDataInicio.Date := StartOfTheMonth(Date);
  dteDataFim.Date    := EndOfTheMonth(Date);

  rgFuncionario.ItemIndex := 0;

  chbComissao.Checked       := True;
  chbVale.Checked           := True;
  chbOutrasDespesas.Checked := True;
  chbSalario.Checked        := True;

  lcbFuncionario.Enabled := False;

  chbComRecibo.Checked     := False;
  chbExportarPDF.Checked   := False;

  chbExportarExcel.Enabled := vgPrm_ExpODespF or vgPrm_ExpFunc or vgPrm_ExpRelDespCol;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioDespesasColaboradores.btnVisualizarClick(
  Sender: TObject);
var
  IdFuncionario, Tipo, IdRecibo, IdPesFuncionario: Integer;
  cTotalFuncionario, cTotalTipo: Currency;
  DescrServico, ListaRecibos, NomeFuncionario, CPFFuncionario, EndFuncionario: String;
begin
  inherited;

  if not VerificarFiltro then
    Exit;

  lImprimirRecibos := False;

  dmRelatorios.qryRel12_DespCol.Close;

  //Periodo
  dmRelatorios.qryRel12_DespCol.Params.ParamByName('DATA_INI1').Value := (FormatDateTime('YYYY-MM-DD', dteDataInicio.Date) + ' 00:00:00');
  dmRelatorios.qryRel12_DespCol.Params.ParamByName('DATA_FIM1').Value := (FormatDateTime('YYYY-MM-DD', dteDataFim.Date) + ' 23:59:59');
  dmRelatorios.qryRel12_DespCol.Params.ParamByName('DATA_INI2').Value := (FormatDateTime('YYYY-MM-DD', dteDataInicio.Date) + ' 00:00:00');
  dmRelatorios.qryRel12_DespCol.Params.ParamByName('DATA_FIM2').Value := (FormatDateTime('YYYY-MM-DD', dteDataFim.Date) + ' 23:59:59');
  dmRelatorios.qryRel12_DespCol.Params.ParamByName('DATA_INI3').Value := (FormatDateTime('YYYY-MM-DD', dteDataInicio.Date) + ' 00:00:00');
  dmRelatorios.qryRel12_DespCol.Params.ParamByName('DATA_FIM3').Value := (FormatDateTime('YYYY-MM-DD', dteDataFim.Date) + ' 23:59:59');
  dmRelatorios.qryRel12_DespCol.Params.ParamByName('DATA_INI4').Value := (FormatDateTime('YYYY-MM-DD', dteDataInicio.Date) + ' 00:00:00');
  dmRelatorios.qryRel12_DespCol.Params.ParamByName('DATA_FIM4').Value := (FormatDateTime('YYYY-MM-DD', dteDataFim.Date) + ' 23:59:59');

  //Funcionario
  if rgFuncionario.ItemIndex = 0 then
  begin
    dmRelatorios.qryRel12_DespCol.Params.ParamByName('ID_FUNC1').Value := 0;
    dmRelatorios.qryRel12_DespCol.Params.ParamByName('ID_FUNC2').Value := 0;
  end
  else
  begin
    dmRelatorios.qryRel12_DespCol.Params.ParamByName('ID_FUNC1').Value := lcbFuncionario.KeyValue;
    dmRelatorios.qryRel12_DespCol.Params.ParamByName('ID_FUNC2').Value := lcbFuncionario.KeyValue;
  end;

  //Tipo de Despesa
  dmRelatorios.qryRel12_DespCol.Params.ParamByName('EXIBIR_TP1').Value := IfThen(chbComissao.Checked, '1', '0');
  dmRelatorios.qryRel12_DespCol.Params.ParamByName('EXIBIR_TP2').Value := IfThen(chbVale.Checked, '2', '0');
  dmRelatorios.qryRel12_DespCol.Params.ParamByName('EXIBIR_TP3').Value := IfThen(chbOutrasDespesas.Checked, '3', '0');
  dmRelatorios.qryRel12_DespCol.Params.ParamByName('EXIBIR_TP4').Value := IfThen(chbSalario.Checked, '4', '0');
  dmRelatorios.qryRel12_DespCol.Open;

  if dmRelatorios.qryRel12_DespCol.RecordCount = 0 then
  begin
    Application.MessageBox('N�o h� Despesas registradas.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
  end
  else
  begin
    //Despesas com Colaboradores
    IdFuncionario     := 0;
    IdPesFuncionario  := 0;
    Tipo              := 0;
    cTotalFuncionario := 0;
    cTotalTipo        := 0;

    NomeFuncionario := '';
    CPFFuncionario  := '';
    EndFuncionario  := '';

    dmRelatorios.qryRel12_DespCol.First;

    IdFuncionario    := dmRelatorios.qryRel12_DespCol.FieldByName('ID_FUNCIONARIO_FK').AsInteger;
    Tipo             := dmRelatorios.qryRel12_DespCol.FieldByName('TIPO').AsInteger;
    IdPesFuncionario := dmRelatorios.qryRel12_DespCol.FieldByName('ID_PESSOA').AsInteger;
    NomeFuncionario  := dmRelatorios.qryRel12_DespCol.FieldByName('NOME_FUNCIONARIO').AsString;
    CPFFuncionario   := dmRelatorios.qryRel12_DespCol.FieldByName('CPF').AsString;
    EndFuncionario   := dmRelatorios.qryRel12_DespCol.FieldByName('ENDERECO').AsString;

    while not dmRelatorios.qryRel12_DespCol.Eof do
    begin
      if IdFuncionario = dmRelatorios.qryRel12_DespCol.FieldByName('ID_FUNCIONARIO_FK').AsInteger then
      begin
        cTotalFuncionario := (cTotalFuncionario +
                              dmRelatorios.qryRel12_DespCol.FieldByName('VR_LANCAMENTO').AsCurrency);

        if Tipo = dmRelatorios.qryRel12_DespCol.FieldByName('TIPO').AsInteger then
          cTotalTipo := (cTotalTipo +
                         dmRelatorios.qryRel12_DespCol.FieldByName('VR_LANCAMENTO').AsCurrency)
        else
          cTotalTipo := dmRelatorios.qryRel12_DespCol.FieldByName('VR_LANCAMENTO').AsCurrency;
      end
      else
      begin
        //Recibos
        if chbComRecibo.Checked then
        begin
          IdRecibo := 0;

          dmRelatorios.GravarReciboImpressao(IdRecibo, 'S', vgSrvn_CodServentia,
                                             dmGerencial.RetornarValorPorExtenso(cTotalFuncionario),
                                             vgConf_NomeServentia,
                                             DescrServico, '', IntToStr(vgSrvn_CodMunicipioServ),
                                             vgSrvn_CidadeServentia,
                                             NomeFuncionario, CPFFuncionario,
                                             EndFuncionario, Date, cTotalFuncionario,
                                             0, IdPesFuncionario, 0, 0);

          if Trim(ListaRecibos) = '' then
            ListaRecibos := IntToStr(IdRecibo)
          else
            ListaRecibos := (ListaRecibos + ', ' + IntToStr(IdRecibo));

          DescrServico := '';
        end;

        //Despesas com Colaboradores
        cTotalFuncionario := dmRelatorios.qryRel12_DespCol.FieldByName('VR_LANCAMENTO').AsCurrency;
        cTotalTipo        := dmRelatorios.qryRel12_DespCol.FieldByName('VR_LANCAMENTO').AsCurrency;
      end;

      if chbComRecibo.Checked then
      begin
        dmRelatorios.qryRel12_DespCol.Edit;
        dmRelatorios.qryRel12_DespCol.FieldByName('VR_TOTAL_FUNCIONARIO').AsCurrency := cTotalFuncionario;
        dmRelatorios.qryRel12_DespCol.FieldByName('VR_TOTAL_TIPO').AsCurrency        := cTotalTipo;
        dmRelatorios.qryRel12_DespCol.Post;
      end;

      IdFuncionario    := dmRelatorios.qryRel12_DespCol.FieldByName('ID_FUNCIONARIO_FK').AsInteger;
      Tipo             := dmRelatorios.qryRel12_DespCol.FieldByName('TIPO').AsInteger;
      IdPesFuncionario := dmRelatorios.qryRel12_DespCol.FieldByName('ID_PESSOA').AsInteger;
      NomeFuncionario  := dmRelatorios.qryRel12_DespCol.FieldByName('NOME_FUNCIONARIO').AsString;
      CPFFuncionario   := dmRelatorios.qryRel12_DespCol.FieldByName('CPF').AsString;
      EndFuncionario   := dmRelatorios.qryRel12_DespCol.FieldByName('ENDERECO').AsString;

      if Trim(DescrServico) = '' then
        DescrServico := '*(' + dmRelatorios.qryRel12_DespCol.FieldByName('DESCR_TIPO').AsString + ') ' +
                        dmRelatorios.qryRel12_DespCol.FieldByName('DESCR_LANCAMENTO').AsString +
                        ' (R$' + dmRelatorios.qryRel12_DespCol.FieldByName('VR_LANCAMENTO').AsString + ')'
      else
        DescrServico := DescrServico + #13#10 + '*(' +
                        dmRelatorios.qryRel12_DespCol.FieldByName('DESCR_TIPO').AsString + ') ' +
                        dmRelatorios.qryRel12_DespCol.FieldByName('DESCR_LANCAMENTO').AsString +
                        ' (R$' + dmRelatorios.qryRel12_DespCol.FieldByName('VR_LANCAMENTO').AsString + ')';

      dmRelatorios.qryRel12_DespCol.Next;
    end;

    //Recibos
    if chbComRecibo.Checked then
    begin
      //Ultimo Recibo
      IdRecibo := 0;

      dmRelatorios.GravarReciboImpressao(IdRecibo, 'S', vgSrvn_CodServentia,
                                         dmGerencial.RetornarValorPorExtenso(cTotalFuncionario),
                                         vgConf_NomeServentia,
                                         DescrServico, '', IntToStr(vgSrvn_CodMunicipioServ),
                                         vgSrvn_CidadeServentia,
                                         NomeFuncionario, CPFFuncionario,
                                         EndFuncionario, Date, cTotalFuncionario,
                                         0, IdPesFuncionario, 0, 0);

      if Trim(ListaRecibos) = '' then
        ListaRecibos := IntToStr(IdRecibo)
      else
        ListaRecibos := (ListaRecibos + ', ' + IntToStr(IdRecibo));

      //Carrega todos os recibos gravados
      dmRelatorios.CarregarRecibos('S', ListaRecibos, False);
    end;

    GerarRelatorio(False);
  end;
end;

procedure TFFiltroRelatorioDespesasColaboradores.FormShow(Sender: TObject);
begin
  inherited;

  iQtdLinhas := 0;

  dteDataInicio.Date := StartOfTheMonth(Date);
  dteDataFim.Date    := EndOfTheMonth(Date);

  rgFuncionario.ItemIndex := 0;

  chbComissao.Checked       := True;
  chbVale.Checked           := True;
  chbOutrasDespesas.Checked := True;
  chbSalario.Checked        := True;

  lcbFuncionario.Enabled := False;

  chbComRecibo.Checked     := False;
  chbExportarPDF.Checked   := False;

  chbExportarExcel.Enabled := vgPrm_ExpODespF or vgPrm_ExpFunc or vgPrm_ExpRelDespCol;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;

  chbExibirGrafico.Visible := False;

  chbTipoSintetico.Visible := False;
  chbTipoAnalitico.Visible := False;

  gbTipo.Visible := False;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioDespesasColaboradores.GerarRelatorio(
  ImpDet: Boolean);
begin
  inherited;

  try
    sTituloRelatorio    := 'DESPESAS COM COLABORADORES';
    sSubtituloRelatorio := 'Per�odo de ' + DateToStr(dteDataInicio.Date) + ' a ' + DateToStr(dteDataFim.Date);

    dmRelatorios.DefinirRelatorio(R12);
    dmRelatorios.ImprimirRelatorio(chbExportarPDF.Checked, chbExportarExcel.Checked);

    GravarLog;

    if chbComRecibo.Checked then
    begin
      dmRelatorios.DefinirRelatorio(R05);
      dmRelatorios.ImprimirRelatorio(chbExportarPDF.Checked, False);

      lImprimirRecibos := True;

      GravarLog;
    end;
  except
    Application.MessageBox('Erro ao gerar Relat�rio de Despesas com Colaboradores.',
                           'Erro',
                           MB_OK + MB_ICONERROR);
  end;
end;

procedure TFFiltroRelatorioDespesasColaboradores.GravarLog;
begin
  inherited;

  if lImprimirRecibos then
  begin
    BS.GravarUsuarioLog(50,
                      '',
                      'Impress�o de Recibo (' + DateToStr(dDataRelatorio) +
                                            ' ' +
                                            TimeToStr(tHoraRelatorio) + ')',
                      0,
                      '');

    lImprimirRecibos := False;
  end
  else
  begin
    BS.GravarUsuarioLog(50,
                        '',
                        'Impress�o de Relatorio de Despesas com Colaboradores (' + DateToStr(dDataRelatorio) +
                                                                               ' ' +
                                                                               TimeToStr(tHoraRelatorio) + ')',
                        0,
                        '');
  end;
end;

procedure TFFiltroRelatorioDespesasColaboradores.rgFuncionarioClick(
  Sender: TObject);
begin
  inherited;

  if rgFuncionario.ItemIndex = 0 then  //Todos
  begin
    lcbFuncionario.KeyValue := Null;
    lcbFuncionario.Enabled := False;
  end
  else  //Especifico
  begin
    qryFuncionario.Close;
    qryFuncionario.Params.ParamByName('DATA_FIM').Value := dteDataFim.Date;
    qryFuncionario.Open;

    lcbFuncionario.Enabled := True;
  end;
end;

procedure TFFiltroRelatorioDespesasColaboradores.rgFuncionarioExit(
  Sender: TObject);
begin
  inherited;

  if rgFuncionario.ItemIndex = 0 then  //Todos
  begin
    lcbFuncionario.KeyValue := Null;
    lcbFuncionario.Enabled := False;
  end
  else  //Especifico
  begin
    qryFuncionario.Close;
    qryFuncionario.Params.ParamByName('DATA_FIM').Value := dteDataFim.Date;
    qryFuncionario.Open;

    lcbFuncionario.Enabled := True;
  end;
end;

function TFFiltroRelatorioDespesasColaboradores.VerificarFiltro: Boolean;
var
  Msg: String;
begin
  Result := True;

  if dteDataInicio.Date = 0 then
  begin
    Msg := '- Informe a DATA DE IN�CIO do Per�odo.';
    Result := False;
  end;

  if dteDataFim.Date = 0 then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe a DATA DE FIM do Per�odo.'
    else
      Msg := #13#10 + '- Informe a DATA DE FIM do Per�odo.';

    Result := False;
  end;

  if dteDataFim.Date < dteDataInicio.Date then
  begin
    if Trim(Msg) = '' then
      Msg := '- A DATA DE FIM do Per�odo n�o pode inferior � DATA DE IN�CIO do mesmo.'
    else
      Msg := #13#10 + '- A DATA DE FIM do Per�odo n�o pode ser inferior � DATA DE IN�CIO do mesmo.';

    Result := False;
  end;

  if rgFuncionario.ItemIndex > 0 then
  begin
    if Trim(lcbFuncionario.Text) = '' then
    begin
      if Trim(Msg) = '' then
        Msg := '- Informe o Nome do Colaborador.'
      else
        Msg := #13#10 + '- Informe o Nome do Colaborador.';

      Result := False;
    end;
  end;

  if not Result then
    Application.MessageBox(PChar(':: ATEN��O ::' + #13#10 + #13#10 + Msg),
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
end;

end.
