{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UPagamentoParcelaLancamento.pas
  Descricao:   Formulario de pagamento de Parcela de Lancamento (Despesa ou Receita)
  Author   :   Cristina
  Date:        27-set-2017
  Last Update: 29-dez-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UPagamentoParcelaLancamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  JvExMask, JvToolEdit, JvDBControls, Vcl.Mask, Vcl.DBCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, JvBaseEdits;

type
  TFPagamentoParcelaLancamento = class(TFCadastroGeralPadrao)
    lblCodigo: TLabel;
    edtCodigo: TDBEdit;
    Label3: TLabel;
    edtAno: TDBEdit;
    lblAno: TLabel;
    lblDataLancamento: TLabel;
    dteDataLancamento: TJvDBDateEdit;
    lblNumeroRecibo: TLabel;
    gbParcela: TGroupBox;
    lblDataPagamentoParc: TLabel;
    dteDataPagamentoParc: TJvDBDateEdit;
    lblValorParcelaPrev: TLabel;
    cedValorParcelaPrev: TJvDBCalcEdit;
    lblValorParcelaJuros: TLabel;
    cedValorParcelaJuros: TJvDBCalcEdit;
    lblValorParcelaDesconto: TLabel;
    cedValorParcelaDesconto: TJvDBCalcEdit;
    lblValorParcelaPago: TLabel;
    cedValorParcelaPago: TJvDBCalcEdit;
    lblNumParcela: TLabel;
    edtNumParcela: TDBEdit;
    lblDataVencimento: TLabel;
    dteDataVencimento: TJvDBDateEdit;
    lblValorTotalPrev: TLabel;
    cedValorTotalPrev: TJvDBCalcEdit;
    lblJuros: TLabel;
    cedValorTotalJuros: TJvDBCalcEdit;
    lblDesconto: TLabel;
    cedValorTotalDesconto: TJvDBCalcEdit;
    lblValorTotalPago: TLabel;
    cedValorTotalPago: TJvDBCalcEdit;
    lblNumParcelas: TLabel;
    edtQtdParcelasLanc: TDBEdit;
    lblObsParcela: TLabel;
    mmObsParcela: TDBMemo;
    qryParc: TFDQuery;
    qryLanc: TFDQuery;
    dspLanc: TDataSetProvider;
    dspParc: TDataSetProvider;
    cdsLanc: TClientDataSet;
    cdsParc: TClientDataSet;
    dsLanc: TDataSource;
    cdsLancCOD_LANCAMENTO: TIntegerField;
    cdsLancANO_LANCAMENTO: TIntegerField;
    cdsLancDATA_LANCAMENTO: TDateField;
    cdsLancTIPO_LANCAMENTO: TStringField;
    cdsLancTIPO_CADASTRO: TStringField;
    cdsLancID_CATEGORIA_DESPREC_FK: TIntegerField;
    cdsLancID_SUBCATEGORIA_DESPREC_FK: TIntegerField;
    cdsLancID_NATUREZA_FK: TIntegerField;
    cdsLancID_CLIENTE_FORNECEDOR_FK: TIntegerField;
    cdsLancFLG_IMPOSTORENDA: TStringField;
    cdsLancFLG_PESSOAL: TStringField;
    cdsLancFLG_AUXILIAR: TStringField;
    cdsLancFLG_REAL: TStringField;
    cdsLancFLG_FLUTUANTE: TStringField;
    cdsLancQTD_PARCELAS: TIntegerField;
    cdsLancVR_TOTAL_PREV: TBCDField;
    cdsLancVR_TOTAL_JUROS: TBCDField;
    cdsLancVR_TOTAL_DESCONTO: TBCDField;
    cdsLancVR_TOTAL_PAGO: TBCDField;
    cdsLancCAD_ID_USUARIO: TIntegerField;
    cdsLancDATA_CADASTRO: TSQLTimeStampField;
    cdsLancFLG_STATUS: TStringField;
    cdsLancFLG_CANCELADO: TStringField;
    cdsLancDATA_CANCELAMENTO: TDateField;
    cdsLancCANCEL_ID_USUARIO: TIntegerField;
    cdsLancFLG_RECORRENTE: TStringField;
    cdsLancFLG_REPLICADO: TStringField;
    cdsLancNUM_RECIBO: TIntegerField;
    cdsLancID_ORIGEM: TIntegerField;
    cdsLancID_SISTEMA_FK: TIntegerField;
    cdsLancOBSERVACAO: TStringField;
    cdsLancID_CARNELEAO_EQUIVALENCIA_FK: TIntegerField;
    cdsLancFLG_FORAFECHCAIXA: TStringField;
    cdsParcID_LANCAMENTO_PARC: TIntegerField;
    cdsParcCOD_LANCAMENTO_FK: TIntegerField;
    cdsParcANO_LANCAMENTO_FK: TIntegerField;
    cdsParcDATA_LANCAMENTO_PARC: TDateField;
    cdsParcNUM_PARCELA: TIntegerField;
    cdsParcDATA_VENCIMENTO: TDateField;
    cdsParcVR_PARCELA_PREV: TBCDField;
    cdsParcVR_PARCELA_JUROS: TBCDField;
    cdsParcVR_PARCELA_DESCONTO: TBCDField;
    cdsParcVR_PARCELA_PAGO: TBCDField;
    cdsParcID_FORMAPAGAMENTO_FK: TIntegerField;
    cdsParcAGENCIA: TStringField;
    cdsParcCONTA: TStringField;
    cdsParcID_BANCO_FK: TIntegerField;
    cdsParcNUM_CHEQUE: TStringField;
    cdsParcNUM_COD_DEPOSITO: TStringField;
    cdsParcFLG_STATUS: TStringField;
    cdsParcDATA_CANCELAMENTO: TDateField;
    cdsParcCANCEL_ID_USUARIO: TIntegerField;
    cdsParcDATA_PAGAMENTO: TDateField;
    cdsParcPAG_ID_USUARIO: TIntegerField;
    cdsParcOBS_LANCAMENTO_PARC: TStringField;
    cdsParcCAD_ID_USUARIO: TIntegerField;
    cdsParcDATA_CADASTRO: TDateField;
    cdsParcID_FORMAPAGAMENTO_ORIG_FK: TIntegerField;
    btnAtualizarPagamento: TJvTransparentButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure mmObsParcelaKeyPress(Sender: TObject; var Key: Char);
    procedure btnAtualizarPagamentoClick(Sender: TObject);
    procedure dteDataPagamentoParcExit(Sender: TObject);
    procedure dteDataPagamentoParcKeyPress(Sender: TObject; var Key: Char);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }

    IdParcela: Integer;

    procedure InicializarTela;
  end;

var
  FPagamentoParcelaLancamento: TFPagamentoParcelaLancamento;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{ TFPagamentoParcelaLancamento }

procedure TFPagamentoParcelaLancamento.btnAtualizarPagamentoClick(
  Sender: TObject);
begin
  inherited;

  if not (cdsParc.State in [dsInsert, dsEdit]) then
    cdsParc.Edit;

  cdsParc.FieldByName('VR_PARCELA_JUROS').AsCurrency    := cedValorParcelaJuros.Value;
  cdsParc.FieldByName('VR_PARCELA_DESCONTO').AsCurrency := cedValorParcelaDesconto.Value;

  cdsParc.FieldByName('VR_PARCELA_PAGO').AsCurrency := ((cdsParc.FieldByName('VR_PARCELA_PREV').AsCurrency +
                                                         cdsParc.FieldByName('VR_PARCELA_JUROS').AsCurrency) -
                                                        cdsParc.FieldByName('VR_PARCELA_DESCONTO').AsCurrency);
end;

procedure TFPagamentoParcelaLancamento.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFPagamentoParcelaLancamento.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFPagamentoParcelaLancamento.DefinirTamanhoMaxCampos;
begin
  inherited;
  //
end;

procedure TFPagamentoParcelaLancamento.DesabilitarComponentes;
begin
  inherited;

  btnDadosPrincipais.Visible := False;
end;

procedure TFPagamentoParcelaLancamento.dteDataPagamentoParcExit(
  Sender: TObject);
begin
  inherited;

  if (vgOperacao = E) and
    (cdsParc.State in [dsInsert, dsEdit]) then
  begin
    if cdsLanc.FieldByName('FLG_FORAFECHCAIXA').AsString = 'N' then
    begin
      if (dteDataPagamentoParc.Date > 0) and
        (dteDataPagamentoParc.Date <> Null) and
        (Trim(dteDataPagamentoParc.Text) <> '/  /') then
      begin
        if (dteDataPagamentoParc.Date < vgDataLancVigente) or
          (dteDataPagamentoParc.Date < dteDataLancamento.Date) then
        begin
          if vgDataLancVigente < dteDataLancamento.Date then
            cdsParc.FieldByName('DATA_PAGAMENTO').AsDateTime := dteDataLancamento.Date
          else
            cdsParc.FieldByName('DATA_PAGAMENTO').AsDateTime := vgDataLancVigente;
        end;
      end;
    end;
  end;
end;

procedure TFPagamentoParcelaLancamento.dteDataPagamentoParcKeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if (vgOperacao = E) and
      (cdsParc.State in [dsInsert, dsEdit]) then
    begin
      if cdsLanc.FieldByName('FLG_FORAFECHCAIXA').AsString = 'N' then
      begin
        if (dteDataPagamentoParc.Date > 0) and
          (dteDataPagamentoParc.Date <> Null) and
          (Trim(dteDataPagamentoParc.Text) <> '/  /') then
        begin
          if (dteDataPagamentoParc.Date < vgDataLancVigente) or
            (dteDataPagamentoParc.Date < dteDataLancamento.Date) then
          begin
            if vgDataLancVigente < dteDataLancamento.Date then
              cdsParc.FieldByName('DATA_PAGAMENTO').AsDateTime := dteDataLancamento.Date
            else
              cdsParc.FieldByName('DATA_PAGAMENTO').AsDateTime := vgDataLancVigente;
          end;
        end;
      end;
    end;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFPagamentoParcelaLancamento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  cdsParc.Cancel;
  cdsLanc.Cancel;

  vgOperacao     := VAZIO;
  vgIdConsulta   := 0;
  vgOrigemFiltro := False;

  inherited;
end;

procedure TFPagamentoParcelaLancamento.FormCreate(Sender: TObject);
begin
  inherited;

  IdParcela := 0;
end;

procedure TFPagamentoParcelaLancamento.FormShow(Sender: TObject);
begin
  inherited;

  InicializarTela;
end;

function TFPagamentoParcelaLancamento.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFPagamentoParcelaLancamento.GravarDadosGenerico(
  var Msg: String): Boolean;
var
  QryAuxS: TFDQuery;
  IdEquiv: Integer;
begin
  Result  := True;
  Msg     := '';
  IdEquiv := 0;

  QryAuxS := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    cdsParc.Post;

    QryAuxS.Close;
    QryAuxS.SQL.Clear;
    QryAuxS.SQL.Text := 'SELECT SUM(VR_PARCELA_PAGO) AS TOTAL_PAGO ' +
                        '  FROM LANCAMENTO_PARC ' +
                        ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                        '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ' +
                        '   AND ID_LANCAMENTO_PARC <> :ID_LANCAMENTO_PARC ' +
                        '   AND FLG_STATUS <> ' + QuotedStr('C');
    QryAuxS.Params.ParamByName('COD_LANCAMENTO').Value     := vgLanc_Codigo;
    QryAuxS.Params.ParamByName('ANO_LANCAMENTO').Value     := vgLanc_Ano;
    QryAuxS.Params.ParamByName('ID_LANCAMENTO_PARC').Value := IdParcela;
    QryAuxS.Open;

    cdsLanc.FieldByName('VR_TOTAL_JUROS').AsCurrency    := (cdsLanc.FieldByName('VR_TOTAL_JUROS').AsCurrency +
                                                            cdsParc.FieldByName('VR_PARCELA_JUROS').AsCurrency);
    cdsLanc.FieldByName('VR_TOTAL_DESCONTO').AsCurrency := (cdsLanc.FieldByName('VR_TOTAL_DESCONTO').AsCurrency +
                                                            cdsParc.FieldByName('VR_PARCELA_DESCONTO').AsCurrency);
    cdsLanc.FieldByName('VR_TOTAL_PAGO').AsCurrency     := (QryAuxS.FieldByName('TOTAL_PAGO').AsCurrency +
                                                            cdsParc.FieldByName('VR_PARCELA_PAGO').AsCurrency);

    cdsParc.ApplyUpdates(0);

    //Carne-Leao
    IdEquiv := dmPrincipal.RetornaEquivalenciaCarneLeao(cdsLanc.FieldByName('ID_CATEGORIA_DESPREC_FK').AsInteger,
                                                        cdsLanc.FieldByName('ID_SUBCATEGORIA_DESPREC_FK').AsInteger,
                                                        cdsLanc.FieldByName('ID_NATUREZA_FK').AsInteger);

    if IdEquiv > 0 then
      cdsLanc.FieldByName('ID_CARNELEAO_EQUIVALENCIA_FK').AsInteger := IdEquiv
    else
      cdsLanc.FieldByName('ID_CARNELEAO_EQUIVALENCIA_FK').Value     := Null;

    //Quitacao do Lancamento completa
    if cdsLanc.FieldByName('VR_TOTAL_PREV').AsCurrency = ((cdsLanc.FieldByName('VR_TOTAL_PAGO').AsCurrency -
                                                           cdsLanc.FieldByName('VR_TOTAL_JUROS').AsCurrency) +
                                                          cdsLanc.FieldByName('VR_TOTAL_DESCONTO').AsCurrency) then
    begin
      cdsLanc.FieldByName('FLG_STATUS').AsString := 'G';

      { LOG - DESPESA/RECEITA }
      QryAuxS.Close;
      QryAuxS.SQL.Clear;
      QryAuxS.SQL.Text := 'INSERT INTO USUARIO_LOG (ID_USUARIO_LOG, ID_USUARIO, ID_MODULO_FK, ' +
                          '                         TIPO_LANCAMENTO, CODIGO_LAN, ANO_LAN, ID_ORIGEM, ' +
                          '                         TABELA_ORIGEM, TIPO_ACAO,  OBS_USUARIO_LOG) ' +
                          '                 VALUES (:ID_USUARIO_LOG, :ID_USUARIO, :ID_MODULO, ' +
                          '                         :TIPO_LANCAMENTO, :CODIGO_LAN, :ANO_LAN, :ID_ORIGEM, ' +
                          '                         :TABELA_ORIGEM, :TIPO_ACAO,  :OBS_USUARIO_LOG)';

      QryAuxS.Params.ParamByName('ID_USUARIO_LOG').Value  := BS.ProximoId('ID_USUARIO_LOG', 'USUARIO_LOG');
      QryAuxS.Params.ParamByName('ID_USUARIO').Value      := vgUsu_Id;
      QryAuxS.Params.ParamByName('ID_MODULO').Value       := 2;
      QryAuxS.Params.ParamByName('TIPO_LANCAMENTO').Value := vgLanc_TpLanc;
      QryAuxS.Params.ParamByName('CODIGO_LAN').Value      := vgLanc_Codigo;
      QryAuxS.Params.ParamByName('ANO_LAN').Value         := vgLanc_Ano;
      QryAuxS.Params.ParamByName('ID_ORIGEM').Value       := Null;
      QryAuxS.Params.ParamByName('TABELA_ORIGEM').Value   := Null;
      QryAuxS.Params.ParamByName('TIPO_ACAO').Value       := 'E';
      QryAuxS.Params.ParamByName('OBS_USUARIO_LOG').Value := 'Baixa completa de Lan�amento.';

      QryAuxS.ExecSQL;
    end;

    cdsLanc.Post;
    cdsLanc.ApplyUpdates(0);

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    FreeAndNil(QryAuxS);

    Msg := 'Parcela de Lancamento paga com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    FreeAndNil(QryAuxS);

    Result := False;
    Msg := 'Erro no pagamento da Parcela de Lan�amento.';
  end;
end;

procedure TFPagamentoParcelaLancamento.GravarLog;
begin
  inherited;

  BS.GravarUsuarioLog(2, '', 'Pagamento de Parcela de Lan�amento.',
                      IdParcela, 'LANCAMENTO_PARC');
end;

procedure TFPagamentoParcelaLancamento.InicializarTela;
begin
  cdsLanc.Close;
  cdsLanc.Params.ParamByName('COD_LANCAMENTO').Value := vgLanc_Codigo;
  cdsLanc.Params.ParamByName('ANO_LANCAMENTO').Value := vgLanc_Ano;
  cdsLanc.Open;

  if cdsLanc.FieldByName('TIPO_LANCAMENTO').AsString = 'R' then
  begin
    Self.Caption := 'Pagamento de Parcela de Lan�amento - Receita';

    lblNumeroRecibo.Visible := True;
    lblNumeroRecibo.Caption := 'N� do Recibo: ' + cdsLanc.FieldByName('NUM_RECIBO').AsString;
  end
  else if cdsLanc.FieldByName('TIPO_LANCAMENTO').AsString = 'D' then
  begin
    Self.Caption := 'Pagamento de Parcela de Lan�amento - Despesa';

    lblNumeroRecibo.Visible := False;
  end;

  cdsLanc.Edit;

  cdsParc.Close;
  cdsParc.Params.ParamByName('ID_LANCAMENTO_PARC').Value := IdParcela;
  cdsParc.Open;

  cdsParc.Edit;
  cdsParc.FieldByName('VR_PARCELA_JUROS').AsCurrency    := 0.00;
  cdsParc.FieldByName('VR_PARCELA_DESCONTO').AsCurrency := 0.00;
  cdsParc.FieldByName('VR_PARCELA_PAGO').AsCurrency     := cdsParc.FieldByName('VR_PARCELA_PREV').AsCurrency;
  cdsParc.FieldByName('DATA_PAGAMENTO').AsDateTime      := Date;
  cdsParc.FieldByName('PAG_ID_USUARIO').AsInteger       := vgUsu_Id;
  cdsParc.FieldByName('FLG_STATUS').AsString            := 'G';

  if cedValorParcelaPago.CanFocus then
    cedValorParcelaPago.SetFocus;
end;

procedure TFPagamentoParcelaLancamento.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFPagamentoParcelaLancamento.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

procedure TFPagamentoParcelaLancamento.mmObsParcelaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

function TFPagamentoParcelaLancamento.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if dteDataPagamentoParc.Date = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a DATA DE PAGAMENTO DA PARCELA.';
    DadosMsgErro.Componente := dteDataPagamentoParc;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if dteDataPagamentoParc.Date < vgDataLancVigente  then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') A DATA DE PAGAMENTO n�o pode ser inferior a ' +
                                                        FormatDateTime('DD/MM/YYYY', vgDataLancVigente) + ', pois os Caixas anteriores a essa data j� se encontram FECHADOS.';
    DadosMsgErro.Componente := dteDataPagamentoParc;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if cdsParc.FieldByName('VR_PARCELA_PREV').AsCurrency <> ((cdsParc.FieldByName('VR_PARCELA_PAGO').AsCurrency -
                                                            cdsParc.FieldByName('VR_PARCELA_JUROS').AsCurrency) +
                                                           cdsParc.FieldByName('VR_PARCELA_DESCONTO').AsCurrency)  then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Valor de pagamento incorreto.';
    DadosMsgErro.Componente := cedValorParcelaPago;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFPagamentoParcelaLancamento.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

end.
