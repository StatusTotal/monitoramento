{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroTipoDespesaColaborador.pas
  Descricao:   Formulario de cadastro de Tipos de Despesa de Colaborador
  Author   :   Cristina
  Date:        22-dez-2016
  Last Update:
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroTipoDespesaColaborador;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Mask, Vcl.DBCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Datasnap.DBClient,
  Datasnap.Provider, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFCadastroTipoDespesaColaborador = class(TFCadastroGeralPadrao)
    lblDescricaoTipoDespesa: TLabel;
    edtDescricaoTipoDespesa: TDBEdit;
    qryTpDespColab: TFDQuery;
    dspTpDespColab: TDataSetProvider;
    cdsTpDespColab: TClientDataSet;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure edtDescricaoTipoDespesaKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }

    IdNovoTipoDespColab: Integer;

  end;

var
  FCadastroTipoDespesaColaborador: TFCadastroTipoDespesaColaborador;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{ TFCadastroGeralPadrao1 }

procedure TFCadastroTipoDespesaColaborador.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroTipoDespesaColaborador.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroTipoDespesaColaborador.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtDescricaoTipoDespesa.MaxLength := 200;
end;

procedure TFCadastroTipoDespesaColaborador.DesabilitarComponentes;
begin
  inherited;

  if vgOperacao = C then
    pnlDados.Enabled := False;
end;

procedure TFCadastroTipoDespesaColaborador.edtDescricaoTipoDespesaKeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroTipoDespesaColaborador.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  cdsTpDespColab.Cancel;

  vgOperacao     := VAZIO;
  vgIdConsulta   := 0;
  vgOrigemFiltro := False;

  inherited;
end;

procedure TFCadastroTipoDespesaColaborador.FormShow(Sender: TObject);
begin
  inherited;

  if edtDescricaoTipoDespesa.CanFocus then
    edtDescricaoTipoDespesa.SetFocus;
end;

function TFCadastroTipoDespesaColaborador.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroTipoDespesaColaborador.GravarDadosGenerico(var Msg: String): Boolean;
begin
  Result := True;
  Msg := '';

  if vgOperacao = C then
    Exit;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    if cdsTpDespColab.State in [dsInsert, dsEdit] then
    begin
      if vgOperacao = I then
        cdsTpDespColab.FieldByName('ID_TIPO_OUTRADESP_FUNC').AsInteger := BS.ProximoId('ID_TIPO_OUTRADESP_FUNC', 'TIPO_OUTRADESP_FUNC');

      if vgOrigemCadastro then
      IdNovoTipoDespColab := cdsTpDespColab.FieldByName('ID_TIPO_OUTRADESP_FUNC').AsInteger;

      cdsTpDespColab.Post;
      cdsTpDespColab.ApplyUpdates(0);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    Msg := 'Tipo de Despesa de Colaborador gravado com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na grava��o do Tipo de Despesa de Colaborador.';
  end;
end;

procedure TFCadastroTipoDespesaColaborador.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta de TIPO DE DESPESA DE COLABORADOR',
                          vgIdConsulta, 'TIPO_OUTRADESP_FUNC');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclus�o de TIPO DE DESPESA DE COLABORADOR',
                          vgIdConsulta, 'TIPO_OUTRADESP_FUNC');
    end;
    E:  //EDICAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO',
                                'Por favor, indique o motivo da edi��o desse TIPO DE DESPESA DE COLABORADOR:',
                                '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          vgIdConsulta, 'TIPO_OUTRADESP_FUNC');
    end;
  end;
end;

procedure TFCadastroTipoDespesaColaborador.lbMensagemErroDblClick(
  Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroTipoDespesaColaborador.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

function TFCadastroTipoDespesaColaborador.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if Trim(edtDescricaoTipoDespesa.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar a DESCRI��O DO TIPO DA DESPESA DE COLABORADOR.';
    DadosMsgErro.Componente := edtDescricaoTipoDespesa;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroTipoDespesaColaborador.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

end.
