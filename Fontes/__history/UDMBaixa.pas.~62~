{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UDMBaixa.pas
  Descricao:   Data Module de Baixa de Lancamentos de Despesas e Receitas e Depositos
  Author   :   Cristina
  Date:        07-fev-2017
  Last Update: 13-dez-2017 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UDMBaixa;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TdmBaixa = class(TDataModule)
    qryBaixaLancI: TFDQuery;
    dspBaixaLancI: TDataSetProvider;
    cdsBaixaLancI: TClientDataSet;
    dsBaixaLancI: TDataSource;
    qryBaixaLancG: TFDQuery;
    dspBaixaLancG: TDataSetProvider;
    cdsBaixaLancG: TClientDataSet;
    dsBaixaLancG: TDataSource;
    cdsBaixaLancICOD_LANCAMENTO_FK: TIntegerField;
    cdsBaixaLancIANO_LANCAMENTO_FK: TIntegerField;
    cdsBaixaLancIID_LANCAMENTO_PARC: TIntegerField;
    cdsBaixaLancINUM_PARCELA: TIntegerField;
    cdsBaixaLancIDATA_LANCAMENTO_PARC: TDateField;
    cdsBaixaLancIVR_PARCELA_PREV: TBCDField;
    cdsBaixaLancIVR_PARCELA_JUROS: TBCDField;
    cdsBaixaLancIVR_PARCELA_DESCONTO: TBCDField;
    cdsBaixaLancIVR_PARCELA_PAGO: TBCDField;
    cdsBaixaLancIID_FORMAPAGAMENTO_FK: TIntegerField;
    cdsBaixaLancIDATA_PAGAMENTO: TDateField;
    cdsBaixaLancIFLG_STATUS: TStringField;
    cdsBaixaLancIPAG_ID_USUARIO: TIntegerField;
    cdsBaixaLancIDESCR_BAIXA_LANC_DERIVADO: TStringField;
    cdsBaixaLancIOBS_BAIXA_LANC_DERIVADO: TStringField;
    cdsBaixaLancIID_SISTEMA_FK: TIntegerField;
    cdsBaixaLancINUM_RECIBO: TIntegerField;
    cdsBaixaLancIVR_DIFERENCA: TCurrencyField;
    cdsBaixaLancITP_DIFERENCA: TStringField;
    cdsBaixaLancGSELECIONADO: TBooleanField;
    cdsBaixaLancGCOD_LANCAMENTO: TIntegerField;
    cdsBaixaLancGANO_LANCAMENTO: TIntegerField;
    cdsBaixaLancGDATA_LANCAMENTO: TDateField;
    cdsBaixaLancGTIPO_LANCAMENTO: TStringField;
    cdsBaixaLancGTIPO_CADASTRO: TStringField;
    cdsBaixaLancGID_CATEGORIA_DESPREC_FK: TIntegerField;
    cdsBaixaLancGID_SUBCATEGORIA_DESPREC_FK: TIntegerField;
    cdsBaixaLancGID_NATUREZA_FK: TIntegerField;
    cdsBaixaLancGID_CLIENTE_FORNECEDOR_FK: TIntegerField;
    cdsBaixaLancGFLG_IMPOSTORENDA: TStringField;
    cdsBaixaLancGFLG_PESSOAL: TStringField;
    cdsBaixaLancGFLG_AUXILIAR: TStringField;
    cdsBaixaLancGFLG_REAL: TStringField;
    cdsBaixaLancGFLG_FLUTUANTE: TStringField;
    cdsBaixaLancGQTD_PARCELAS: TIntegerField;
    cdsBaixaLancGVR_TOTAL_PREV: TBCDField;
    cdsBaixaLancGVR_TOTAL_JUROS: TBCDField;
    cdsBaixaLancGVR_TOTAL_DESCONTO: TBCDField;
    cdsBaixaLancGVR_TOTAL_PAGO: TBCDField;
    cdsBaixaLancGCAD_ID_USUARIO: TIntegerField;
    cdsBaixaLancGDATA_CADASTRO: TSQLTimeStampField;
    cdsBaixaLancGFLG_STATUS: TStringField;
    cdsBaixaLancGFLG_CANCELADO: TStringField;
    cdsBaixaLancGDATA_CANCELAMENTO: TDateField;
    cdsBaixaLancGCANCEL_ID_USUARIO: TIntegerField;
    cdsBaixaLancGFLG_RECORRENTE: TStringField;
    cdsBaixaLancGFLG_REPLICADO: TStringField;
    cdsBaixaLancGNUM_RECIBO: TIntegerField;
    cdsBaixaLancGID_ORIGEM: TIntegerField;
    cdsBaixaLancGID_SISTEMA_FK: TIntegerField;
    cdsBaixaLancGOBSERVACAO: TStringField;
    cdsBaixaLancGID_CARNELEAO_EQUIVALENCIA_FK: TIntegerField;
    cdsBaixaLancGID_LANCAMENTO_PARC: TIntegerField;
    cdsBaixaLancGCOD_LANCAMENTO_FK: TIntegerField;
    cdsBaixaLancGANO_LANCAMENTO_FK: TIntegerField;
    cdsBaixaLancGDATA_LANCAMENTO_PARC: TDateField;
    cdsBaixaLancGNUM_PARCELA: TIntegerField;
    cdsBaixaLancGDATA_VENCIMENTO: TDateField;
    cdsBaixaLancGVR_PARCELA_PREV: TBCDField;
    cdsBaixaLancGVR_PARCELA_JUROS: TBCDField;
    cdsBaixaLancGVR_PARCELA_DESCONTO: TBCDField;
    cdsBaixaLancGVR_PARCELA_PAGO: TBCDField;
    cdsBaixaLancGID_FORMAPAGAMENTO_FK: TIntegerField;
    cdsBaixaLancGAGENCIA: TStringField;
    cdsBaixaLancGCONTA: TStringField;
    cdsBaixaLancGID_BANCO_FK: TIntegerField;
    cdsBaixaLancGNUM_CHEQUE: TStringField;
    cdsBaixaLancGFLG_STATUS_1: TStringField;
    cdsBaixaLancGDATA_CANCELAMENTO_1: TDateField;
    cdsBaixaLancGCANCEL_ID_USUARIO_1: TIntegerField;
    cdsBaixaLancGDATA_PAGAMENTO: TDateField;
    cdsBaixaLancGPAG_ID_USUARIO: TIntegerField;
    cdsBaixaLancGOBS_LANCAMENTO_PARC: TStringField;
    cdsBaixaLancGCAD_ID_USUARIO_1: TIntegerField;
    cdsBaixaLancGDATA_CADASTRO_1: TDateField;
    cdsBaixaLancGFLG_STATUS_PARC: TStringField;
    cdsBaixaLancGFLG_FORAFECHCAIXA: TStringField;
    qryDespPagas: TFDQuery;
    dspDespPagas: TDataSetProvider;
    cdsDespPagas: TClientDataSet;
    dsDespPagas: TDataSource;
    cdsBaixaLancGDESCRICAO: TStringField;
    cdsDespPagasCOD_LANCAMENTO_FK: TIntegerField;
    cdsDespPagasANO_LANCAMENTO_FK: TIntegerField;
    cdsDespPagasID_LANCAMENTO_PARC: TIntegerField;
    cdsDespPagasDATA_LANCAMENTO_PARC: TDateField;
    cdsDespPagasVR_PARCELA_PREV: TBCDField;
    cdsDespPagasVR_PARCELA_PAGO: TBCDField;
    cdsDespPagasDATA_PAGAMENTO: TDateField;
    cdsDespPagasID_NATUREZA_FK: TIntegerField;
    cdsDespPagasDESCR_NATUREZA: TStringField;
    cdsDespPagasID_CATEGORIA_DESPREC_FK: TIntegerField;
    cdsDespPagasDESCR_CATEGORIA_DESPREC: TStringField;
    cdsDespPagasID_SUBCATEGORIA_DESPREC_FK: TIntegerField;
    cdsDespPagasOBSERVACAO: TStringField;
    cdsDespPagasBAIXADO: TStringField;
    cdsDespPagasDESCR_SUBCATEGORIA_DESPREC: TStringField;
    cdsBaixaLancGID_FORMAPAGAMENTO_ORIG_FK: TIntegerField;
    cdsBaixaLancGBAIXADO: TStringField;
    qryDepos: TFDQuery;
    dspDepos: TDataSetProvider;
    cdsDepos: TClientDataSet;
    dsDepos: TDataSource;
    qryLanc: TFDQuery;
    dspLanc: TDataSetProvider;
    cdsLanc: TClientDataSet;
    dsLanc: TDataSource;
    cdsDeposSELECIONADO: TBooleanField;
    cdsDeposID_DEPOSITO_FLUTUANTE: TIntegerField;
    cdsDeposNUM_COD_DEPOSITO: TStringField;
    cdsDeposDESCR_DEPOSITO_FLUTUANTE: TStringField;
    cdsDeposDATA_DEPOSITO: TDateField;
    cdsDeposVR_DEPOSITO_FLUTUANTE: TBCDField;
    cdsDeposFLG_STATUS: TStringField;
    cdsLancSELECIONADO: TBooleanField;
    cdsLancCOD_LANCAMENTO: TIntegerField;
    cdsLancANO_LANCAMENTO: TIntegerField;
    cdsLancDATA_LANCAMENTO: TDateField;
    cdsLancTIPO_LANCAMENTO: TStringField;
    cdsLancTIPO_CADASTRO: TStringField;
    cdsLancID_CATEGORIA_DESPREC_FK: TIntegerField;
    cdsLancID_SUBCATEGORIA_DESPREC_FK: TIntegerField;
    cdsLancID_NATUREZA_FK: TIntegerField;
    cdsLancID_CLIENTE_FORNECEDOR_FK: TIntegerField;
    cdsLancFLG_IMPOSTORENDA: TStringField;
    cdsLancFLG_PESSOAL: TStringField;
    cdsLancFLG_AUXILIAR: TStringField;
    cdsLancFLG_REAL: TStringField;
    cdsLancFLG_FLUTUANTE: TStringField;
    cdsLancQTD_PARCELAS: TIntegerField;
    cdsLancVR_TOTAL_PREV: TBCDField;
    cdsLancVR_TOTAL_JUROS: TBCDField;
    cdsLancVR_TOTAL_DESCONTO: TBCDField;
    cdsLancVR_TOTAL_PAGO: TBCDField;
    cdsLancCAD_ID_USUARIO: TIntegerField;
    cdsLancDATA_CADASTRO: TSQLTimeStampField;
    cdsLancFLG_STATUS: TStringField;
    cdsLancFLG_CANCELADO: TStringField;
    cdsLancDATA_CANCELAMENTO: TDateField;
    cdsLancCANCEL_ID_USUARIO: TIntegerField;
    cdsLancFLG_RECORRENTE: TStringField;
    cdsLancFLG_REPLICADO: TStringField;
    cdsLancNUM_RECIBO: TIntegerField;
    cdsLancID_ORIGEM: TIntegerField;
    cdsLancID_SISTEMA_FK: TIntegerField;
    cdsLancOBSERVACAO: TStringField;
    cdsLancID_CARNELEAO_EQUIVALENCIA_FK: TIntegerField;
    cdsLancFLG_FORAFECHCAIXA: TStringField;
    cdsLancID_LANCAMENTO_PARC: TIntegerField;
    cdsLancCOD_LANCAMENTO_FK: TIntegerField;
    cdsLancANO_LANCAMENTO_FK: TIntegerField;
    cdsLancDATA_LANCAMENTO_PARC: TDateField;
    cdsLancNUM_PARCELA: TIntegerField;
    cdsLancDATA_VENCIMENTO: TDateField;
    cdsLancVR_PARCELA_PREV: TBCDField;
    cdsLancVR_PARCELA_JUROS: TBCDField;
    cdsLancVR_PARCELA_DESCONTO: TBCDField;
    cdsLancVR_PARCELA_PAGO: TBCDField;
    cdsLancID_FORMAPAGAMENTO_FK: TIntegerField;
    cdsLancAGENCIA: TStringField;
    cdsLancCONTA: TStringField;
    cdsLancID_BANCO_FK: TIntegerField;
    cdsLancNUM_CHEQUE: TStringField;
    cdsLancFLG_STATUS_1: TStringField;
    cdsLancDATA_CANCELAMENTO_1: TDateField;
    cdsLancCANCEL_ID_USUARIO_1: TIntegerField;
    cdsLancDATA_PAGAMENTO: TDateField;
    cdsLancPAG_ID_USUARIO: TIntegerField;
    cdsLancOBS_LANCAMENTO_PARC: TStringField;
    cdsLancCAD_ID_USUARIO_1: TIntegerField;
    cdsLancDATA_CADASTRO_1: TDateField;
    cdsLancID_FORMAPAGAMENTO_ORIG_FK: TIntegerField;
    cdsLancFLG_STATUS_PARC: TStringField;
    cdsLancVINCULADO: TStringField;
    cdsLancDESCRICAO: TStringField;
    cdsLancDESCR_NATUREZA: TStringField;
    cdsBaixaLancGNUM_COD_DEPOSITO: TStringField;
    cdsLancNUM_COD_DEPOSITO: TStringField;
    cdsDeposCAD_ID_USUARIO: TIntegerField;
    cdsDeposDATA_CADASTRO: TSQLTimeStampField;
    cdsDeposFLUT_ID_USUARIO: TIntegerField;
    cdsDeposBAIXA_ID_USUARIO: TIntegerField;
    cdsDeposCANCEL_ID_USUARIO: TIntegerField;
    cdsDeposDATA_FLUTUANTE: TDateField;
    cdsDeposDATA_BAIXADO: TDateField;
    cdsDeposDATA_CANCELAMENTO: TDateField;
    procedure cdsBaixaLancICalcFields(DataSet: TDataSet);
    procedure cdsBaixaLancGVR_TOTAL_PREVGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsBaixaLancGVR_PARCELA_PREVGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsBaixaLancGVR_PARCELA_PAGOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsBaixaLancGAfterPost(DataSet: TDataSet);
    procedure cdsBaixaLancIVR_PARCELA_PREVGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsBaixaLancIVR_PARCELA_JUROSGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsBaixaLancIVR_PARCELA_DESCONTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsBaixaLancIVR_PARCELA_PAGOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsBaixaLancGVR_TOTAL_JUROSGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsBaixaLancGVR_TOTAL_DESCONTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsBaixaLancGVR_TOTAL_PAGOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsBaixaLancGVR_PARCELA_JUROSGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsBaixaLancGVR_PARCELA_DESCONTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsDespPagasVR_PARCELA_PREVGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsDespPagasVR_PARCELA_PAGOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsLancAfterPost(DataSet: TDataSet);
    procedure cdsDeposAfterPost(DataSet: TDataSet);
    procedure cdsLancVR_PARCELA_PAGOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsDeposVR_DEPOSITO_FLUTUANTEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }

    cValorTotalFlutuante,
    cValorTotalLancamentos,
    cValorTotalDepositos,
    cDifTot: Currency;

    sTipoBaixa: String;

    dDataVinculo: TDateTime;
  end;

var
  dmBaixa: TdmBaixa;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{$R *.dfm}

procedure TdmBaixa.cdsBaixaLancGAfterPost(DataSet: TDataSet);
var
  iPos: Integer;
begin
  inherited;

  cValorTotalFlutuante := 0;

  iPos := cdsBaixaLancG.RecNo;

  cdsBaixaLancG.DisableControls;

  cdsBaixaLancG.First;

  while not cdsBaixaLancG.Eof do
  begin
    if cdsBaixaLancG.FieldByName('SELECIONADO').AsBoolean then
      cValorTotalFlutuante := (cValorTotalFlutuante + cdsBaixaLancG.FieldByName('VR_PARCELA_PREV').AsCurrency);

    cdsBaixaLancG.Next;
  end;

  cdsBaixaLancG.EnableControls;
  cdsBaixaLancG.RecNo := iPos;
end;

procedure TdmBaixa.cdsBaixaLancGVR_PARCELA_DESCONTOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmBaixa.cdsBaixaLancGVR_PARCELA_JUROSGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmBaixa.cdsBaixaLancGVR_PARCELA_PAGOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmBaixa.cdsBaixaLancGVR_PARCELA_PREVGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmBaixa.cdsBaixaLancGVR_TOTAL_DESCONTOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmBaixa.cdsBaixaLancGVR_TOTAL_JUROSGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmBaixa.cdsBaixaLancGVR_TOTAL_PAGOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmBaixa.cdsBaixaLancGVR_TOTAL_PREVGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmBaixa.cdsBaixaLancICalcFields(DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with qryAux, SQL do
  begin
    //Numero do Recibo e Id do Sistema
    if cdsBaixaLancI.RecordCount > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT NUM_RECIBO, ID_SISTEMA_FK ' +
              '  FROM LANCAMENTO L ' +
              ' WHERE L.COD_LANCAMENTO = :COD_LANCAMENTO ' +
              '   AND L.ANO_LANCAMENTO = :ANO_LANCAMENTO';
      Params.ParamByName('COD_LANCAMENTO').AsInteger := cdsBaixaLancI.FieldByName('COD_LANCAMENTO_FK').AsInteger;
      Params.ParamByName('ANO_LANCAMENTO').AsInteger := cdsBaixaLancI.FieldByName('ANO_LANCAMENTO_FK').AsInteger;
      Open;
    end;

    if qryAux.RecordCount > 0 then
    begin
      cdsBaixaLancI.FieldByName('NUM_RECIBO').AsInteger    := qryAux.FieldByName('NUM_RECIBO').AsInteger;
      cdsBaixaLancI.FieldByName('ID_SISTEMA_FK').AsInteger := qryAux.FieldByName('ID_SISTEMA_FK').AsInteger;
    end;
  end;

  FreeAndNil(qryAux);
end;

procedure TdmBaixa.cdsBaixaLancIVR_PARCELA_DESCONTOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmBaixa.cdsBaixaLancIVR_PARCELA_JUROSGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmBaixa.cdsBaixaLancIVR_PARCELA_PAGOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmBaixa.cdsBaixaLancIVR_PARCELA_PREVGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmBaixa.cdsDeposAfterPost(DataSet: TDataSet);
var
  iPos: Integer;
begin
  inherited;

  cValorTotalDepositos := 0;

  iPos := cdsDepos.RecNo;

  cdsDepos.DisableControls;

  cdsDepos.First;

  while not cdsDepos.Eof do
  begin
    if cdsDepos.FieldByName('SELECIONADO').AsBoolean then
      cValorTotalDepositos := (cValorTotalDepositos + cdsDepos.FieldByName('VR_DEPOSITO_FLUTUANTE').AsCurrency);

    cdsDepos.Next;
  end;

  cdsDepos.EnableControls;
  cdsDepos.RecNo := iPos;
end;

procedure TdmBaixa.cdsDeposVR_DEPOSITO_FLUTUANTEGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmBaixa.cdsDespPagasVR_PARCELA_PAGOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmBaixa.cdsDespPagasVR_PARCELA_PREVGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmBaixa.cdsLancAfterPost(DataSet: TDataSet);
var
  iPos: Integer;
begin
  inherited;

  cValorTotalLancamentos := 0;

  iPos := cdsLanc.RecNo;

  cdsLanc.DisableControls;

  cdsLanc.First;

  while not cdsLanc.Eof do
  begin
    if cdsLanc.FieldByName('SELECIONADO').AsBoolean then
      cValorTotalLancamentos := (cValorTotalLancamentos + cdsLanc.FieldByName('VR_PARCELA_PAGO').AsCurrency);

    cdsLanc.Next;
  end;

  cdsLanc.EnableControls;
  cdsLanc.RecNo := iPos;
end;

procedure TdmBaixa.cdsLancVR_PARCELA_PAGOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

end.
