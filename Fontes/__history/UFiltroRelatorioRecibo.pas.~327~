{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroRelatorioRecibo.pas
  Descricao:   Filtro Relatorio de Recibo (R05 - R05DET)
  Author   :   Cristina
  Date:        24-nov-2016
  Last Update: 24-mai-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroRelatorioRecibo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroRelatorioPadrao, Vcl.StdCtrls,
  JvExControls, JvButton, JvTransparentButton, Vcl.ExtCtrls, Vcl.DBCtrls,
  Vcl.Mask, JvExMask, JvToolEdit, JvBaseEdits, JvDBControls, JvMaskEdit,
  System.StrUtils, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFFiltroRelatorioRecibo = class(TFFiltroRelatorioPadrao)
    rgTipoRecibo: TRadioGroup;
    gbNumRecibo: TGroupBox;
    lblReciboInicial: TLabel;
    edtReciboInicial: TEdit;
    lblReciboFinal: TLabel;
    edtReciboFinal: TEdit;
    lblDadosRecibo: TGroupBox;
    lblRecebiDe: TLabel;
    lblAImportanciaDe: TLabel;
    lblReferenteA: TLabel;
    lblObservacoes: TLabel;
    Label5: TLabel;
    edtRecebiDe: TDBEdit;
    lblValorRecibo: TLabel;
    cedValorRecibo: TJvDBCalcEdit;
    edtAImportanciaDe: TDBEdit;
    edtLocal: TDBEdit;
    mmReferenteA: TDBMemo;
    mmObservacoes: TDBMemo;
    dteDataRecibo: TJvDBDateEdit;
    Label9: TLabel;
    gbEmitente: TGroupBox;
    lblNomeEmitente: TLabel;
    edtNomeEmitente: TDBEdit;
    edtEnderecoEmitente: TDBEdit;
    lblEnderecoEmitente: TLabel;
    lblCPFEmitente: TLabel;
    lblNumRecibo: TLabel;
    edtNumRecibo: TDBEdit;
    ntbOpcoes: TNotebook;
    edtQtdCopias: TEdit;
    lblQtdCopias: TLabel;
    dbnRecibo: TDBNavigator;
    btnAlteraListaRecibos: TJvTransparentButton;
    edtCPFEmitente: TDBEdit;
    btnPropriaServentia: TJvTransparentButton;
    procedure FormShow(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure rgTipoReciboClick(Sender: TObject);
    procedure rgTipoReciboExit(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
    procedure edtReciboInicialExit(Sender: TObject);
    procedure edtReciboFinalExit(Sender: TObject);
    procedure btnAlteraListaRecibosClick(Sender: TObject);
    procedure dbnReciboBeforeAction(Sender: TObject; Button: TNavigateBtn);
    procedure edtCPFEmitenteKeyPress(Sender: TObject; var Key: Char);
    procedure btnPropriaServentiaClick(Sender: TObject);
  protected
    procedure GravarLog; override;
    procedure GerarRelatorio(ImpDet: Boolean); override;
    function VerificarFiltro: Boolean; override;
  private
    { Private declarations }
    procedure HabilitarDesabilitarPreenchimentoRecibo(Hab, Autom: Boolean);
    procedure GerarRecibos;
  public
    { Public declarations }
  end;

var
  FFiltroRelatorioRecibo: TFFiltroRelatorioRecibo;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMRelatorios;

{ TFFiltroRelatorioRecibo }

procedure TFFiltroRelatorioRecibo.btnAlteraListaRecibosClick(Sender: TObject);
begin
  inherited;

  dmRelatorios.cdsRel05_Recibo.Close;
  dmRelatorios.qryRel05_Recibo.SQL.Clear;
  dmRelatorios.qryRel05_Recibo.SQL.Text := 'SELECT * ' +
                                           '  FROM RECIBO_IMPRESSAO ' +
                                           ' WHERE (1 = 0)';
  dmRelatorios.cdsRel05_Recibo.Open;

  edtReciboInicial.Clear;
  edtReciboFinal.Clear;

  HabilitarDesabilitarPreenchimentoRecibo(True, False);

  if edtReciboInicial.CanFocus then
    edtReciboInicial.SetFocus;
end;

procedure TFFiltroRelatorioRecibo.btnLimparClick(Sender: TObject);
begin
  inherited;

  rgTipoRecibo.ItemIndex := 0;

  chbExportarPDF.Checked := True;

  HabilitarDesabilitarPreenchimentoRecibo(False, False);

  if rgTipoRecibo.CanFocus then
    rgTipoRecibo.SetFocus;
end;

procedure TFFiltroRelatorioRecibo.btnPropriaServentiaClick(Sender: TObject);
begin
  inherited;

  if Trim(edtNomeEmitente.Text) = '' then
  begin
    edtNomeEmitente.Text     := vgConf_NomeServentia;
    edtCPFEmitente.Text      := vgSrvn_CNPJServentia;
    edtEnderecoEmitente.Text := vgSrvn_EndServentia;
  end
  else
  begin
    edtNomeEmitente.Text     := '';
    edtCPFEmitente.Text      := '';
    edtEnderecoEmitente.Text := '';
  end;
end;

procedure TFFiltroRelatorioRecibo.btnVisualizarClick(Sender: TObject);
var
  ListaRecibos: String;
  NumRecibo, IdRecibo: Integer;
  qryNumRec: TFDQuery;
begin
  inherited;

  ListaRecibos := '';

  iQtdLinhas := 0;
  IdRecibo   := 0;
  NumRecibo  := 0;

  if rgTipoRecibo.ItemIndex = 0 then  //EM BRANCO
  begin
    iQtdLinhas := StrToInt(edtQtdCopias.Text);

    //Carrega todos os recibos gravados
    dmRelatorios.CarregarRecibos('N', '', True);
  end
  else if rgTipoRecibo.ItemIndex = 1 then  //PREENCHIDO
  begin
    if not VerificarFiltro then
      Exit;

    if (Trim(edtReciboInicial.Text) = '') and
      (Trim(edtReciboFinal.Text) = '') then
    begin
      qryNumRec := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

      qryNumRec.Close;
      qryNumRec.SQL.Clear;
      qryNumRec.SQL.Text := 'SELECT MAX(NUM_RECIBO) AS NUM_RECIBO ' +
                            '  FROM RECIBO_IMPRESSAO ' +
                            ' WHERE FLG_FUNCIONARIO = ' + QuotedStr('N') +
                            '   AND COD_LANCAMENTO_FK IS NULL ' +
                            '   AND ANO_LANCAMENTO_FK IS NULL';
      qryNumRec.Open;

      if qryNumRec.FieldByName('NUM_RECIBO').IsNull then
        NumRecibo := 1
      else
        NumRecibo := (qryNumRec.FieldByName('NUM_RECIBO').AsInteger + 1);

      FreeAndNil(qryNumRec);
    end;

    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      IdRecibo := BS.ProximoId('ID_RECIBO_IMPRESSAO', 'RECIBO_IMPRESSAO');

      if dmRelatorios.cdsRel05_Recibo.State in [dsInsert, dsEdit] then
      begin
        if dmRelatorios.cdsRel05_Recibo.FieldByName('ID_RECIBO_IMPRESSAO').IsNull then
        begin
          dmRelatorios.cdsRel05_Recibo.FieldByName('ID_RECIBO_IMPRESSAO').AsInteger := IdRecibo;
          dmRelatorios.cdsRel05_Recibo.FieldByName('FLG_FUNCIONARIO').AsString      := 'N';
          dmRelatorios.cdsRel05_Recibo.FieldByName('ID_USUARIO').AsInteger          := vgUsu_Id;
        end;

        dmRelatorios.cdsRel05_Recibo.Post;
      end;

      dmRelatorios.cdsRel05_Recibo.First;

      while not dmRelatorios.cdsRel05_Recibo.Eof do
      begin
        dmRelatorios.cdsRel05_Recibo.Edit;

        if (Trim(edtReciboInicial.Text) = '') and
          (Trim(edtReciboFinal.Text) = '') then
          dmRelatorios.cdsRel05_Recibo.FieldByName('NUM_RECIBO').AsInteger := NumRecibo;

        dmRelatorios.cdsRel05_Recibo.FieldByName('ID_RECIBO_IMPRESSAO').AsInteger := IdRecibo;
        dmRelatorios.cdsRel05_Recibo.FieldByName('DATA_IMPRESSAO').AsDateTime     := Now;
        dmRelatorios.cdsRel05_Recibo.Post;

        if Trim(ListaRecibos) = '' then
          ListaRecibos := IntToStr(IdRecibo)
        else
          ListaRecibos := (ListaRecibos + ', ' + IntToStr(IdRecibo));

        dmRelatorios.cdsRel05_Recibo.Next;

        Inc(IdRecibo);
        Inc(NumRecibo);
      end;

      dmRelatorios.cdsRel05_Recibo.ApplyUpdates(0);

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;
    except
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;
    end;

    //Carrega todos os recibos gravados
    dmRelatorios.CarregarRecibos('N', ListaRecibos, False);
  end;

  GerarRelatorio(False);

  if rgTipoRecibo.ItemIndex = 0 then
    dmRelatorios.cdsRel05_Recibo.Close
  else if rgTipoRecibo.ItemIndex = 1 then
  begin
    edtReciboInicial.Clear;
    edtReciboFinal.Clear;

    HabilitarDesabilitarPreenchimentoRecibo(True, False);
  end;
end;

procedure TFFiltroRelatorioRecibo.dbnReciboBeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
  inherited;

  if Button = nbDelete then
  begin
    if Application.MessageBox('Confirma a exclus�o desse Recibo da Lista para Impress�o?',
                              'Confirma��o', MB_YESNO + MB_ICONQUESTION) = ID_NO then
      Abort;
  end;
end;

procedure TFFiltroRelatorioRecibo.edtCPFEmitenteKeyPress(Sender: TObject;
  var Key: Char);
var
  sCPF_CNPJ: String;
begin
  inherited;

  sCPF_CNPJ := ReplaceStr(ReplaceStr(edtCPFEmitente.Text, '.', ''), '-', '');

  if (Key = #9) or  //TAB
    (Key = #13) then  //ENTER
  begin
    if not dmGerencial.VerificarCPFCNPJ(Trim(sCPF_CNPJ)) then
    begin
      Application.MessageBox('CPF inv�lido.', 'Erro', MB_OK + MB_ICONERROR);

      edtCPFEmitente.Clear;

      if edtCPFEmitente.CanFocus then
        edtCPFEmitente.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFFiltroRelatorioRecibo.edtReciboFinalExit(Sender: TObject);
begin
  inherited;

  GerarRecibos;
end;

procedure TFFiltroRelatorioRecibo.edtReciboInicialExit(Sender: TObject);
begin
  GerarRecibos;
end;

procedure TFFiltroRelatorioRecibo.FormShow(Sender: TObject);
begin
  inherited;

  gbTipo.Visible           := False;
  chbTipoAnalitico.Visible := False;
  chbTipoSintetico.Visible := False;

  chbExibirGrafico.Visible := False;
  chbExportarExcel.Visible := False;

  chbExportarPDF.Checked := True;

  HabilitarDesabilitarPreenchimentoRecibo(False, False);

  if rgTipoRecibo.CanFocus then
    rgTipoRecibo.SetFocus;
end;

procedure TFFiltroRelatorioRecibo.GerarRecibos;
var
  IdRecibo: Integer;
  DescrRec: String;
  qryLancs, qryDet: TFDQuery;
  lContinuar: Boolean;
begin
  inherited;

  lContinuar := False;

  if (Trim(edtReciboInicial.Text) <> '') and
    (Trim(edtReciboFinal.Text) <> '') then
  begin
    //Recupera os dados para os Recibos
    qryLancs := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);
    qryDet   := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

    qryLancs.Close;
    qryLancs.SQL.Clear;

    qryLancs.SQL.Text := 'SELECT REC.NUM_RECIBO, ' +
                         '       SUM(REC.VR_PARCELA_PAGO) AS VR_RECIBO, ' +
                         '       (SELECT CF.NOME_FANTASIA ' +
                         '          FROM CLIENTE_FORNECEDOR CF ' +
                         '         WHERE CF.ID_CLIENTE_FORNECEDOR = REC.ID_CLIENTE_FORNECEDOR_FK) AS NOME_EMISSOR, ' +
                         '       REC.OBS_RECIBO_IMPRESSAO, ' +
                         '       REC.COD_LANCAMENTO, ' +
                         '       REC.ANO_LANCAMENTO ' +
                         '  FROM (SELECT L.NUM_RECIBO, ' +
                         '               LP.VR_PARCELA_PAGO, ' +
                         '               (CASE WHEN (L.FLG_FLUTUANTE = ' + QuotedStr('S') + ') ' +
                         '                     THEN (SELECT FIRST 1 LANC.ID_CLIENTE_FORNECEDOR_FK ' +
                         '                             FROM LANCAMENTO LANC ' +
                         '                            WHERE LANC.NUM_RECIBO = L.NUM_RECIBO ' +
                         '                              AND LANC.ID_CLIENTE_FORNECEDOR_FK IS NOT NULL ' +
                         '                              AND L.FLG_FLUTUANTE = ' + QuotedStr('N') + ') ' +
                         '                     ELSE L.ID_CLIENTE_FORNECEDOR_FK ' +
                         '                END) AS ID_CLIENTE_FORNECEDOR_FK, ' +
                         '               L.OBSERVACAO AS OBS_RECIBO_IMPRESSAO, ' +
                         '               L.COD_LANCAMENTO, ' +
                         '               L.ANO_LANCAMENTO ' +
                         '          FROM LANCAMENTO L ' +
                         '         INNER JOIN LANCAMENTO_PARC LP ' +
                         '            ON L.COD_LANCAMENTO = LP.COD_LANCAMENTO_FK ' +
                         '           AND L.ANO_LANCAMENTO = LP.ANO_LANCAMENTO_FK ' +
                         '         WHERE L.NUM_RECIBO BETWEEN :REC_INI AND :REC_FIM ' +
                         '           AND L.FLG_CANCELADO = ' + QuotedStr('N') +
                         '           AND LP.FLG_STATUS = ' + QuotedStr('G') + ') AS REC ' +
                         ' WHERE NOT EXISTS(SELECT IMP.ID_RECIBO_IMPRESSAO ' +
                         '                    FROM RECIBO_IMPRESSAO IMP '+
                         '                   WHERE IMP.NUM_RECIBO = REC.NUM_RECIBO ' +
                         '                     AND IMP.COD_LANCAMENTO_FK = REC.COD_LANCAMENTO ' +
                         '                     AND IMP.ANO_LANCAMENTO_FK = REC.ANO_LANCAMENTO) ' +
                         'GROUP BY REC.NUM_RECIBO, ' +
                         '         REC.ID_CLIENTE_FORNECEDOR_FK, ' +
                         '         REC.OBS_RECIBO_IMPRESSAO, ' +
                         '         REC.COD_LANCAMENTO, ' +
                         '         REC.ANO_LANCAMENTO ' +
                         'ORDER BY REC.NUM_RECIBO';

    qryLancs.Params.ParamByName('REC_INI').Value := edtReciboInicial.Text;
    qryLancs.Params.ParamByName('REC_FIM').Value := edtReciboFinal.Text;
    qryLancs.Open;

    lContinuar := (qryLancs.RecordCount > 0);

    if lContinuar then
    begin
      dmRelatorios.cdsRel05_Recibo.Close;
      dmRelatorios.qryRel05_Recibo.SQL.Clear;
      dmRelatorios.qryRel05_Recibo.SQL.Text := 'SELECT * ' +
                                               '  FROM RECIBO_IMPRESSAO ' +
                                               ' WHERE (1 = 0)';
      dmRelatorios.cdsRel05_Recibo.Open;

      //Grava no ClientDataSet os Recibos a serem impressos
      IdRecibo := 900000;

      qryLancs.First;

      while not qryLancs.Eof do
      begin
        DescrRec := '';

        qryDet.Close;
        qryDet.SQL.Clear;

        qryDet.SQL.Text := 'SELECT I.DESCR_ITEM, ' +
                           '       SUM(LD.VR_TOTAL) AS VR_TOTAL ' +
                           '  FROM LANCAMENTO_DET LD ' +
                           ' INNER JOIN ITEM I ' +
                           '    ON LD.ID_ITEM_FK = I.ID_ITEM ' +
                           ' WHERE LD.COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                           '   AND LD.ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ' +
                           '   AND LD.FLG_CANCELADO = ' + QuotedStr('N') +
                           'GROUP BY I.DESCR_ITEM ' +
                           'ORDER BY I.DESCR_ITEM';

        qryDet.Params.ParamByName('COD_LANCAMENTO').Value := qryLancs.FieldByName('COD_LANCAMENTO').AsInteger;
        qryDet.Params.ParamByName('ANO_LANCAMENTO').Value := qryLancs.FieldByName('ANO_LANCAMENTO').AsInteger;
        qryDet.Open;

        qryDet.First;

        while not qryDet.Eof do
        begin
          if Trim(DescrRec) = '' then
            DescrRec := '* ' + qryDet.FieldByName('DESCR_ITEM').AsString +
                        ' (R$' + qryDet.FieldByName('VR_TOTAL').AsString + ')'
          else
            DescrRec := (DescrRec + #13#10 + '* ' + qryDet.FieldByName('DESCR_ITEM').AsString) +
                        ' (R$' + qryDet.FieldByName('VR_TOTAL').AsString + ')';

          qryDet.Next;
        end;

        dmRelatorios.GravarReciboImpressao(IdRecibo, 'N',
                                           '',
                                           dmGerencial.RetornarValorPorExtenso(qryLancs.FieldByName('VR_RECIBO').AsCurrency),
                                           qryLancs.FieldByName('NOME_EMISSOR').AsString,
                                           DescrRec,
                                           qryLancs.FieldByName('OBS_RECIBO_IMPRESSAO').AsString,
                                           IntToStr(vgSrvn_CodMunicipioServ),
                                           vgSrvn_CidadeServentia,
                                           vgConf_NomeServentia,
                                           vgSrvn_CNPJServentia,
                                           vgSrvn_EndServentia,
                                           Date,
                                           qryLancs.FieldByName('VR_RECIBO').AsCurrency,
                                           qryLancs.FieldByName('NUM_RECIBO').AsInteger,
                                           0,
                                           qryLancs.FieldByName('COD_LANCAMENTO').AsInteger,
                                           qryLancs.FieldByName('ANO_LANCAMENTO').AsInteger);

        qryLancs.Next;

        Inc(IdRecibo);
      end;

      HabilitarDesabilitarPreenchimentoRecibo(False, True);

      dmRelatorios.cdsRel05_Recibo.First;
    end;

    FreeAndNil(qryLancs);
    FreeAndNil(qryDet);
  end
  else
    HabilitarDesabilitarPreenchimentoRecibo(True, False);
end;

procedure TFFiltroRelatorioRecibo.GerarRelatorio(ImpDet: Boolean);
begin
  inherited;

  try
    dmRelatorios.DefinirRelatorio(R05);
    dmRelatorios.ImprimirRelatorio(chbExportarPDF.Checked, False);

    GravarLog;
  except
    Application.MessageBox('Erro ao gerar Recibos.',
                           'Erro',
                           MB_OK + MB_ICONERROR);
  end;
end;

procedure TFFiltroRelatorioRecibo.GravarLog;
begin
  inherited;

  BS.GravarUsuarioLog(50,
                      '',
                      'Impress�o de Recibo (' + DateToStr(dDataRelatorio) +
                                            ' ' +
                                            TimeToStr(tHoraRelatorio) + ')',
                      0,
                      '');
end;

procedure TFFiltroRelatorioRecibo.HabilitarDesabilitarPreenchimentoRecibo(
  Hab, Autom: Boolean);
begin
  if Autom then
    ntbOpcoes.PageIndex := 1
  else
  begin
    ntbOpcoes.PageIndex := 0;
    edtQtdCopias.Text   := '1';

    if not Hab then
    begin
      edtReciboInicial.Clear;
      edtReciboFinal.Clear;
    end;

    dmRelatorios.cdsRel05_Recibo.Close;
    dmRelatorios.qryRel05_Recibo.SQL.Clear;
    dmRelatorios.qryRel05_Recibo.SQL.Text := 'SELECT * ' +
                                             '  FROM RECIBO_IMPRESSAO ' +
                                             ' WHERE (1 = 0)';
    dmRelatorios.cdsRel05_Recibo.Open;
  end;

  ntbOpcoes.Visible := not Hab;

  gbNumRecibo.Enabled           := Hab or ((not Hab) and Autom);
  edtReciboInicial.Enabled      := Hab or ((not Hab) and Autom);
  edtReciboFinal.Enabled        := Hab or ((not Hab) and Autom);
  btnAlteraListaRecibos.Enabled := ((not Hab) and Autom);

  cedValorRecibo.Enabled := Hab or ((not Hab) and Autom);
  edtNumRecibo.Enabled   := (not Hab) and Autom;

  edtAImportanciaDe.ReadOnly := True;

  edtRecebiDe.Enabled         := Hab or ((not Hab) and Autom);
  edtAImportanciaDe.Enabled   := Hab or ((not Hab) and Autom);
  mmReferenteA.Enabled        := Hab or ((not Hab) and Autom);
  mmObservacoes.Enabled       := Hab or ((not Hab) and Autom);
  edtLocal.Enabled            := Hab or ((not Hab) and Autom);
  dteDataRecibo.Enabled       := Hab or ((not Hab) and Autom);
  edtNomeEmitente.Enabled     := Hab or ((not Hab) and Autom);
  edtCPFEmitente.Enabled      := Hab or ((not Hab) and Autom);
  edtEnderecoEmitente.Enabled := Hab or ((not Hab) and Autom);
  btnPropriaServentia.Enabled := Hab;

  cedValorRecibo.ReadOnly := Autom;
  edtNumRecibo.ReadOnly   := Hab or Autom;

  edtRecebiDe.ReadOnly         := Autom;
  mmReferenteA.ReadOnly        := Autom;
  mmObservacoes.ReadOnly       := Autom;
  edtLocal.ReadOnly            := Autom;
  dteDataRecibo.ReadOnly       := Autom;
  edtNomeEmitente.ReadOnly     := Autom;
  edtCPFEmitente.ReadOnly      := Autom;
  edtEnderecoEmitente.ReadOnly := Autom;

  if not Hab then
  begin
    edtReciboInicial.Color := cl3DLight;
    edtReciboFinal.Color   := cl3DLight;

    cedValorRecibo.Color := cl3DLight;
    edtNumRecibo.Color   := cl3DLight;

    edtRecebiDe.Color         := cl3DLight;
    edtAImportanciaDe.Color   := cl3DLight;
    mmReferenteA.Color        := cl3DLight;
    mmObservacoes.Color       := cl3DLight;
    edtLocal.Color            := cl3DLight;
    dteDataRecibo.Color       := cl3DLight;
    edtNomeEmitente.Color     := cl3DLight;
    edtCPFEmitente.Color      := cl3DLight;
    edtEnderecoEmitente.Color := cl3DLight;
  end
  else
  begin
    edtReciboInicial.Color := $00F5E1CF;
    edtReciboFinal.Color   := $00F5E1CF;

    cedValorRecibo.Color := $00F5E1CF;

    if Autom then
      edtNumRecibo.Color := $00F5E1CF
    else
      edtNumRecibo.Color := cl3DLight;

    edtRecebiDe.Color       := $00F5E1CF;
    edtAImportanciaDe.Color := $00F5E1CF;
    mmReferenteA.Color      := $00F5E1CF;
    edtLocal.Color          := $00F5E1CF;
    dteDataRecibo.Color     := $00F5E1CF;
    edtNomeEmitente.Color   := $00F5E1CF;
    edtCPFEmitente.Color    := $00F5E1CF;

    mmObservacoes.Color       := $00FEF3E9;
    edtEnderecoEmitente.Color := $00FEF3E9;
  end;
end;

procedure TFFiltroRelatorioRecibo.rgTipoReciboClick(Sender: TObject);
begin
  inherited;

  if rgTipoRecibo.ItemIndex = 0 then
    HabilitarDesabilitarPreenchimentoRecibo(False, False)
  else
  begin
    if (Trim(edtReciboInicial.Text) <> '') and
      (Trim(edtReciboFinal.Text) <> '') then
      HabilitarDesabilitarPreenchimentoRecibo(False, True)
    else
    begin
      HabilitarDesabilitarPreenchimentoRecibo(True, False);
      dmRelatorios.cdsRel05_Recibo.Append;
    end;
  end;
end;

procedure TFFiltroRelatorioRecibo.rgTipoReciboExit(Sender: TObject);
begin
  inherited;

  if rgTipoRecibo.ItemIndex = 0 then
    HabilitarDesabilitarPreenchimentoRecibo(False, False)
  else
  begin
    if (Trim(edtReciboInicial.Text) <> '') and
      (Trim(edtReciboFinal.Text) <> '') then
      HabilitarDesabilitarPreenchimentoRecibo(False, True)
    else
    begin
      HabilitarDesabilitarPreenchimentoRecibo(True, False);
      dmRelatorios.cdsRel05_Recibo.Append;
    end;
  end;
end;

function TFFiltroRelatorioRecibo.VerificarFiltro: Boolean;
var
  sCPF: String;
  Msg: String;
begin
  Result := True;
  sCPF   := '';

  if rgTipoRecibo.ItemIndex = 0 then  //EM BRANCO
  begin
    if (Trim(edtQtdCopias.Text) = '') or
      (Trim(edtQtdCopias.Text) = '') then
    begin
      Msg := '- Informe a QUANTIDADE DE C�PIAS DE RECIBOS EM BRANCO.';
      Result := False;
    end;
  end
  else if rgTipoRecibo.ItemIndex = 1 then  //PREENCHIDO
  begin
    if (Trim(edtReciboInicial.Text) = '') and
      (Trim(edtReciboFinal.Text) = '') then  //Manual
    begin
      if cedValorRecibo.Value <= 0 then
      begin
        Msg := '- Informe o VALOR CORRETO DO RECIBO.';
        Result := False;
      end;

      if Trim(edtNumRecibo.Text) = '' then
      begin
        if Trim(Msg) = '' then
          Msg := '- Informe o N�MERO DO RECIBO.'
        else
          Msg := #13#10 + '- Informe o N�MERO DO RECIBO.';

        Result := False;
      end;

      if Trim(edtRecebiDe.Text) = '' then
      begin
        if Trim(Msg) = '' then
          Msg := '- Informe de quem a import�ncia foi recebida (RECEBI DE...).'
        else
          Msg := #13#10 + '- Informe de quem a import�ncia foi recebida (RECEBI DE...).';

        Result := False;
      end;

      if Trim(mmReferenteA.Text) = '' then
      begin
        if Trim(Msg) = '' then
          Msg := '- Liste os itens referentes ao Recibo (REFRENTE A...).'
        else
          Msg := #13#10 + '- Liste os itens referentes ao Recibo (REFRENTE A...).';

        Result := False;
      end;

      if Trim(edtLocal.Text) = '' then
      begin
        if Trim(Msg) = '' then
          Msg := '- Informe o LOCAL DO RECIBO.'
        else
          Msg := #13#10 + '- Informe o LOCAL DO RECIBO.';

        Result := False;
      end;

      if dteDataRecibo.Date = 0 then
      begin
        if Trim(Msg) = '' then
          Msg := '- Informe a DATA DO RECIBO.'
        else
          Msg := #13#10 + '- Informe a DATA DO RECIBO.';

        Result := False;
      end;

      if Trim(edtNomeEmitente.Text) = '' then
      begin
        if Trim(Msg) = '' then
          Msg := '- Informe o NOME DO EMITENTE.'
        else
          Msg := #13#10 + '- Informe o NOME DO EMITENTE.';

        Result := False;
      end;

      sCPF := dmGerencial.PegarNumeroTexto(edtCPFEmitente.Text);

      if Trim(sCPF) = '' then
      begin
        if Trim(Msg) = '' then
          Msg := '- O CPF DO EMITENTE deve ser informado.'
        else
          Msg := #13#10 + '- O CPF DO EMITENTE deve ser informado.';

        Result := False;
      end;
    end;
  end;

  if not Result then
    Application.MessageBox(PChar(':: ATEN��O ::' + #13#10 + #13#10 + Msg),
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
end;

end.
