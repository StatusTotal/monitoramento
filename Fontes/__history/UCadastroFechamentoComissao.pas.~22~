{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroFechamentoComissao.pas
  Descricao:   Tela de cadastro de Fechamento de Comissoes de Funcionarios
  Author   :   Cristina
  Date:        13-dez-2016
  Last Update: 17-out-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroFechamentoComissao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.DBClient, Datasnap.Provider,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.ComCtrls, Vcl.Mask, JvExMask,
  JvToolEdit, JvBaseEdits, JvDBControls, System.DateUtils, Vcl.DBCtrls,
  System.StrUtils, System.Math;

type
  TFCadastroFechamentoComissao = class(TFCadastroGeralPadrao)
    tvwComissoes: TTreeView;
    qryComissoes: TFDQuery;
    dspComissoes: TDataSetProvider;
    cdsComissoes: TClientDataSet;
    dsComissoes: TDataSource;
    lblMes: TLabel;
    lblAno: TLabel;
    lblValorTotalItem: TLabel;
    lblPercentPrevista: TLabel;
    lblValorPrevisto: TLabel;
    lblPercentPaga: TLabel;
    lblValorPago: TLabel;
    cedValorTotalItem: TJvDBCalcEdit;
    cedPercentPrevista: TJvDBCalcEdit;
    cedValorPrevisto: TJvDBCalcEdit;
    cedPercentPaga: TJvDBCalcEdit;
    cedValorPago: TJvDBCalcEdit;
    qryComissao: TFDQuery;
    dspComissao: TDataSetProvider;
    cdsComissao: TClientDataSet;
    cdsComissoesPERCENT_COMISSAO_PAGO: TCurrencyField;
    cdsComissoesVR_COMISSAO_PAGO: TCurrencyField;
    edtAno: TEdit;
    cbMes: TComboBox;
    btnConfirmarComissao: TJvTransparentButton;
    btnCancelarComissao: TJvTransparentButton;
    btnAbrirComissoes: TJvTransparentButton;
    lblNomeFuncionario: TLabel;
    lblNomeSistema: TLabel;
    lblDescricaoItem: TLabel;
    edtNomeFuncionario: TDBEdit;
    edtNomeSistema: TDBEdit;
    edtDescricaoItem: TDBEdit;
    cdsComissaoID_COMISSAO: TIntegerField;
    cdsComissaoID_FUNCIONARIO_FK: TIntegerField;
    cdsComissaoID_SISTEMA_FK: TIntegerField;
    cdsComissaoCOD_ADICIONAL: TIntegerField;
    cdsComissaoID_ORIGEM: TIntegerField;
    cdsComissaoTIPO_ORIGEM: TStringField;
    cdsComissaoPERCENT_COMISSAO: TBCDField;
    cdsComissaoVR_COMISSAO: TBCDField;
    cdsComissaoID_FECHAMENTO_SALARIO_FK: TIntegerField;
    cdsComissaoDATA_CADASTRO: TSQLTimeStampField;
    cdsComissaoCAD_ID_USUARIO: TIntegerField;
    cdsComissaoDATA_PAGAMENTO: TDateField;
    cdsComissaoPAG_ID_USUARIO: TIntegerField;
    cdsComissaoFLG_CANCELADO: TStringField;
    cdsComissaoDATA_CANCELAMENTO: TDateField;
    cdsComissaoCANCEL_ID_USUARIO: TIntegerField;
    cdsComissaoDATA_COMISSAO: TDateField;
    cdsComissaoID_ITEM_FK: TIntegerField;
    lblDataComissao: TLabel;
    dteDataComissao: TJvDBDateEdit;
    lblDataPagamento: TLabel;
    dteDataPagamento: TJvDBDateEdit;
    cdsComissoesNOME_FUNCIONARIO: TStringField;
    cdsComissoesCPF: TStringField;
    cdsComissoesNOME_SISTEMA: TStringField;
    cdsComissoesID_SISTEMA_FK: TIntegerField;
    cdsComissoesDESCR_ITEM: TStringField;
    cdsComissoesID_ITEM_FK: TIntegerField;
    cdsComissoesVR_TOTAL_ITEM: TBCDField;
    cdsComissoesPERCENT_COMISSAO_PREV: TBCDField;
    cdsComissoesID_ORIGEM: TIntegerField;
    cdsComissoesTIPO_ORIGEM: TStringField;
    cdsComissoesCOD_ADICIONAL: TIntegerField;
    cdsComissoesVR_COMISSAO_PREV: TBCDField;
    cdsComissoesDATA_COMISSAO: TDateField;
    cdsComissoesFLG_CANCELADO: TStringField;
    cdsComissoesDATA_PAGAMENTO: TDateField;
    cdsComissoesPAG_ID_USUARIO: TIntegerField;
    lblCancelada: TLabel;
    cdsComissoesID_FUNCIONARIO: TIntegerField;
    procedure btnAbrirComissoesClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnCancelarComissaoClick(Sender: TObject);
    procedure btnConfirmarComissaoClick(Sender: TObject);
    procedure tvwComissoesCustomDrawItem(Sender: TCustomTreeView;
      Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure cedPercentPagaExit(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure chbCanceladaKeyPress(Sender: TObject; var Key: Char);
    procedure tvwComissoesClick(Sender: TObject);
    procedure dteDataComissaoKeyPress(Sender: TObject; var Key: Char);
    procedure cedValorPagoExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dteDataComissaoExit(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
    procedure HabDesabBotoesCRUD(Ed: Boolean);
    procedure DesabilitaHabilitaCampos(Hab: Boolean);
  public
    { Public declarations }
  end;

var
  FCadastroFechamentoComissao: TFCadastroFechamentoComissao;

implementation

{$R *.dfm}

uses UDM, UGDM, UVariaveisGlobais, UBibliotecaSistema;

{

uses UBibliotecaSistema; TFCadastroComissoes }

procedure TFCadastroFechamentoComissao.btnAbrirComissoesClick(Sender: TObject);
var
  ItemUltiimoFunc, ItemUltimoSis, ItemUltimoItem: TTreeNode;
  xFuncAnt, xSisAnt, xItemAnt: String;
begin
  inherited;

  cdsComissoes.Close;
  cdsComissoes.Params.ParamByName('MES').Value := (cbMes.ItemIndex + 1);
  cdsComissoes.Params.ParamByName('ANO').Value := StrToInt(edtAno.Text);
  cdsComissoes.Open;

  cdsComissoes.First;

  while not cdsComissoes.Eof do
  begin
    if xFuncAnt <> cdsComissoes.FieldByName('NOME_FUNCIONARIO').AsString then
    begin
      ItemUltiimoFunc := tvwComissoes.Items.AddChild(Nil,
                                                     cdsComissoes.FieldByName('NOME_FUNCIONARIO').AsString);
    end;
    if xSisAnt <> cdsComissoes.FieldByName('NOME_SISTEMA').AsString then
    begin
      ItemUltimoSis := tvwComissoes.Items.AddChild(ItemUltiimoFunc,
                                                   cdsComissoes.FieldByName('NOME_SISTEMA').AsString);
    end;
    if xItemAnt <> cdsComissoes.FieldByName('DESCR_ITEM').AsString then
    begin
      ItemUltimoItem := tvwComissoes.Items.AddChild(ItemUltimoSis,
                                                    cdsComissoes.FieldByName('DESCR_ITEM').AsString);
    end;

    xFuncAnt := cdsComissoes.FieldByName('NOME_FUNCIONARIO').AsString;
    xSisAnt  := cdsComissoes.FieldByName('NOME_SISTEMA').AsString;
    xItemAnt := cdsComissoes.FieldByName('DESCR_ITEM').AsString;

    cdsComissoes.Edit;
    cdsComissoes.FieldByName('PERCENT_COMISSAO_PAGO').AsCurrency := cdsComissoes.FieldByName('PERCENT_COMISSAO_PREV').AsCurrency;
    cdsComissoes.FieldByName('VR_COMISSAO_PAGO').AsCurrency      := cdsComissoes.FieldByName('VR_COMISSAO_PREV').AsCurrency;

    if Date < vgDataLancVigente then
      cdsComissoes.FieldByName('DATA_COMISSAO').AsDateTime := vgDataLancVigente
    else
      cdsComissoes.FieldByName('DATA_COMISSAO').AsDateTime := Date;

    if cdsComissoes.FieldByName('FLG_CANCELADO').IsNull then
      cdsComissoes.FieldByName('FLG_CANCELADO').AsString := 'N';

    cdsComissoes.Post;

    cdsComissoes.Next;
  end;
end;

procedure TFCadastroFechamentoComissao.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroFechamentoComissao.btnCancelarComissaoClick(Sender: TObject);
begin
  inherited;

  cdsComissoes.Cancel;

  HabDesabBotoesCRUD(True);
  DesabilitaHabilitaCampos(False);

  if edtAno.CanFocus then
    edtAno.SetFocus;
end;

procedure TFCadastroFechamentoComissao.btnConfirmarComissaoClick(Sender: TObject);
begin
  inherited;

  cdsComissoes.Post;

  HabDesabBotoesCRUD(True);
  DesabilitaHabilitaCampos(False);

  if edtAno.CanFocus then
    edtAno.SetFocus;
end;

procedure TFCadastroFechamentoComissao.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroFechamentoComissao.cedPercentPagaExit(Sender: TObject);
begin
  inherited;

  if cdsComissoes.State in [dsInsert, dsEdit] then
    cdsComissoes.FieldByName('VR_COMISSAO_PAGO').AsCurrency := ((cedValorTotalItem.Value * cedPercentPaga.Value)/100);
end;

procedure TFCadastroFechamentoComissao.cedValorPagoExit(Sender: TObject);
begin
  inherited;

  if cdsComissoes.State in [dsInsert, dsEdit] then
    cdsComissoes.FieldByName('PERCENT_COMISSAO_PAGO').AsCurrency := ((cedValorPago.Value * 100) /
                                                                     cedValorTotalItem.Value);
end;

procedure TFCadastroFechamentoComissao.chbCanceladaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnConfirmarComissao.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    btnCancelar.Click;
end;

procedure TFCadastroFechamentoComissao.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtAno.MaxLength := 4;
end;

procedure TFCadastroFechamentoComissao.DesabilitaHabilitaCampos(Hab: Boolean);
begin
  cbMes.Enabled  := not Hab;
  edtAno.Enabled := not Hab;

  cedPercentPaga.Enabled     := Hab;
  cedValorPago.Enabled       := Hab;
  dteDataComissao.Enabled    := Hab;
end;

procedure TFCadastroFechamentoComissao.DesabilitarComponentes;
begin
  inherited;

  tvwComissoes.ReadOnly       := True;
  edtNomeFuncionario.ReadOnly := True;
  edtNomeSistema.ReadOnly     := True;
  edtDescricaoItem.ReadOnly   := True;
  cedValorTotalItem.ReadOnly  := True;
  cedPercentPrevista.ReadOnly := True;
  cedValorPrevisto.ReadOnly   := True;

  dteDataPagamento.Enabled := False;

  lblCancelada.Visible := False;

  if vgOperacao = C then
  begin
    cbMes.Enabled             := False;
    edtAno.Enabled            := False;
    btnAbrirComissoes.Enabled := False;

    cedPercentPaga.Enabled     := False;
    cedValorPago.Enabled       := False;
    dteDataComissao.Enabled    := False;
  end
  else
  begin
    HabDesabBotoesCRUD(True);
    DesabilitaHabilitaCampos(False);
  end;

  lblNomeFuncionario.Visible   := False;
  edtNomeFuncionario.Visible   := False;
  lblNomeSistema.Visible       := False;
  edtNomeSistema.Visible       := False;
  lblDescricaoItem.Visible     := False;
  edtDescricaoItem.Visible     := False;
  lblValorTotalItem.Visible    := False;
  cedValorTotalItem.Visible    := False;
  lblPercentPrevista.Visible   := False;
  cedPercentPrevista.Visible   := False;
  lblValorPrevisto.Visible     := False;
  cedValorPrevisto.Visible     := False;
  lblPercentPaga.Visible       := False;
  cedPercentPaga.Visible       := False;
  lblValorPago.Visible         := False;
  cedValorPago.Visible         := False;
  lblDataComissao.Visible      := False;
  dteDataComissao.Visible      := False;
  lblDataPagamento.Visible     := False;
  dteDataPagamento.Visible     := False;
  btnConfirmarComissao.Visible := False;
  btnCancelarComissao.Visible  := False;
end;

procedure TFCadastroFechamentoComissao.dteDataComissaoExit(Sender: TObject);
begin
  inherited;

  if cdsComissoes.State in [dsInsert, dsEdit] then
  begin
    if dteDataComissao.Date < vgDataLancVigente then
      cdsComissoes.FieldByName('DATA_COMISSAO').AsDateTime := vgDataLancVigente
    else
      cdsComissoes.FieldByName('DATA_COMISSAO').AsDateTime := dteDataComissao.Date;
  end;
end;

procedure TFCadastroFechamentoComissao.dteDataComissaoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if cdsComissoes.State in [dsInsert, dsEdit] then
    begin
      if dteDataComissao.Date < vgDataLancVigente then
        cdsComissoes.FieldByName('DATA_COMISSAO').AsDateTime := vgDataLancVigente
      else
        cdsComissoes.FieldByName('DATA_COMISSAO').AsDateTime := dteDataComissao.Date;
    end;

    btnConfirmarComissao.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    btnCancelar.Click;
end;

procedure TFCadastroFechamentoComissao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  cdsComissoes.Cancel;
  cdsComissao.Cancel;

  vgOperacao     := VAZIO;
  vgIdConsulta   := 0;
  vgOrigemFiltro := False;

  inherited;
end;

procedure TFCadastroFechamentoComissao.FormCreate(Sender: TObject);
begin
  inherited;

  cbMes.ItemIndex := (MonthOf(Date) - 1);
  edtAno.Text     := IntToStr(YearOf(Date));
end;

function TFCadastroFechamentoComissao.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroFechamentoComissao.GravarDadosGenerico(var Msg: String): Boolean;
var
  sIR, sCP, sLA: String;
  IdCli, IdCom: Integer;
  Doc, Obs, NFantasia, RSocial: String;
  Op: TOperacao;
  QryAuxS: TFDQuery;
begin
  Result := True;
  Msg    := '';

  if vgOperacao = C then
    Exit;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    cdsComissoes.First;

    while not cdsComissoes.Eof do
    begin
      if cdsComissoes.FieldByName('VR_COMISSAO_PAGO').AsCurrency > 0 then
      begin
        sIR    := '';
        sCP    := '';
        sLA    := '';
        Op     := vgOperacao;

        IdCli     := 0;
        IdCom     := 0;
        Doc       := '';
        Obs       := '';
        NFantasia := '';
        RSocial   := '';

        cdsComissao.Close;
        cdsComissao.Params.ParamByName('ID_FUNCIONARIO').Value := cdsComissoes.FieldByName('ID_FUNCIONARIO').Value;
        cdsComissao.Params.ParamByName('ID_SISTEMA').Value     := cdsComissoes.FieldByName('ID_SISTEMA_FK').Value;
        cdsComissao.Params.ParamByName('ID_ITEM').Value        := cdsComissoes.FieldByName('ID_ITEM_FK').Value;
        cdsComissao.Params.ParamByName('COD_ADICIONAL').Value  := cdsComissoes.FieldByName('COD_ADICIONAL').Value;
        cdsComissao.Params.ParamByName('ID_ORIGEM').Value      := cdsComissoes.FieldByName('ID_ORIGEM').AsInteger;
        cdsComissao.Params.ParamByName('TIPO_ORIGEM').Value    := cdsComissoes.FieldByName('TIPO_ORIGEM').AsString;
        cdsComissao.Params.ParamByName('MES').Value            := (cbMes.ItemIndex + 1);
        cdsComissao.Params.ParamByName('ANO').Value            := StrToInt(edtAno.Text);
        cdsComissao.Open;

        if cdsComissao.RecordCount = 0 then
        begin  //Inclusao
          { COMISSAO }
          cdsComissao.Append;

          IdCom := BS.ProximoId('ID_COMISSAO', 'COMISSAO');

          cdsComissao.FieldByName('ID_COMISSAO').AsInteger       := IdCom;
          cdsComissao.FieldByName('ID_FUNCIONARIO_FK').Value     := cdsComissoes.FieldByName('ID_FUNCIONARIO').Value;
          cdsComissao.FieldByName('ID_SISTEMA_FK').Value         := cdsComissoes.FieldByName('ID_SISTEMA_FK').Value;
          cdsComissao.FieldByName('ID_ITEM_FK').Value            := cdsComissoes.FieldByName('ID_ITEM_FK').Value;
          cdsComissao.FieldByName('COD_ADICIONAL').AsInteger     := cdsComissoes.FieldByName('COD_ADICIONAL').AsInteger;
          cdsComissao.FieldByName('ID_ORIGEM').AsInteger         := cdsComissoes.FieldByName('ID_ORIGEM').AsInteger;
          cdsComissao.FieldByName('TIPO_ORIGEM').AsString        := cdsComissoes.FieldByName('TIPO_ORIGEM').AsString;
          cdsComissao.FieldByName('PERCENT_COMISSAO').AsCurrency := cdsComissoes.FieldByName('PERCENT_COMISSAO_PAGO').AsCurrency;
          cdsComissao.FieldByName('VR_COMISSAO').AsCurrency      := cdsComissoes.FieldByName('VR_COMISSAO_PAGO').AsCurrency;
          cdsComissao.FieldByName('DATA_CADASTRO').AsDateTime    := Now;
          cdsComissao.FieldByName('CAD_ID_USUARIO').AsInteger    := vgUsu_Id;
          cdsComissao.FieldByName('DATA_COMISSAO').AsDateTime    := cdsComissoes.FieldByName('DATA_COMISSAO').AsDateTime;
          cdsComissao.FieldByName('FLG_CANCELADO').AsString      := cdsComissoes.FieldByName('FLG_CANCELADO').AsString;
          cdsComissao.FieldByName('PAG_ID_USUARIO').Value        := cdsComissoes.FieldByName('PAG_ID_USUARIO').Value;
          cdsComissao.FieldByName('DATA_PAGAMENTO').Value        := cdsComissoes.FieldByName('DATA_PAGAMENTO').Value;

          cdsComissao.Post;
          cdsComissao.ApplyUpdates(0);

          { LANCAMENTO }
          dmPrincipal.RetornarClassificacaoNatureza(2, sIR, sCP, sLA);

          //Fornecedor
          Doc       := cdsComissoes.FieldByName('CPF').AsString;
          Obs       := 'Cadastro realizado automaticamente em inclus�o de COMISS�O.';
          NFantasia := cdsComissoes.FieldByName('NOME_FUNCIONARIO').AsString;
          RSocial   := cdsComissoes.FieldByName('NOME_FUNCIONARIO').AsString;

          dmPrincipal.RetornarClienteFornecedor('C',
                                                IdCli,
                                                Doc,
                                                Obs,
                                                NFantasia,
                                                RSocial);

          dmPrincipal.InicializarComponenteLancamento;

          DadosLancamento := Lancamento.Create;

          DadosLancamento.DataLancamento   := cdsComissoes.FieldByName('DATA_COMISSAO').AsDateTime;
          DadosLancamento.TipoLancamento   := 'R';
          DadosLancamento.TipoCadastro     := 'A';
          DadosLancamento.IdCliFor         := IdCli;
          DadosLancamento.FlgIR            := sIR;
          DadosLancamento.FlgPessoal       := sCP;
          DadosLancamento.FlgFlutuante     := 'N';
          DadosLancamento.FlgAuxiliar      := sLA;
          DadosLancamento.FlgForaFechCaixa := 'N';
          DadosLancamento.IdNatureza       := 2;  //COMISSAO
          DadosLancamento.QtdParcelas      := 1;
          DadosLancamento.CadIdUsuario     := vgUsu_Id;
          DadosLancamento.DataCadastro     := Now;
          DadosLancamento.FlgRecorrente    := 'N';
          DadosLancamento.FlgReplicado     := 'N';
          DadosLancamento.IdOrigem         := IdCom;

          if not cdsComissoes.FieldByName('DATA_PAGAMENTO').IsNull then
          begin
            DadosLancamento.VlrTotalPago := cdsComissoes.FieldByName('VR_COMISSAO_PAGO').AsCurrency;
            DadosLancamento.FlgStatus    := 'G';
          end;

          DadosLancamento.VlrTotalPrev := cdsComissoes.FieldByName('VR_COMISSAO_PAGO').AsCurrency;
          DadosLancamento.Observacao   := 'Receita originada pelo COMISS�O ' + IntToStr(IdCom) + '.';

          ListaLancamentos.Add(DadosLancamento);

          dmPrincipal.InsertLancamento(0);

          { PARCELAS }
          DadosLancamentoParc := LancamentoParc.Create;

          DadosLancamentoParc.NumParcela     := 1;
          DadosLancamentoParc.IdFormaPagto   := 6;  //Dinheiro
          DadosLancamentoParc.DataLancParc   := cdsComissoes.FieldByName('DATA_COMISSAO').AsDateTime;
          DadosLancamentoParc.DataVencimento := Date;
          DadosLancamentoParc.CadIdUsuario   := vgUsu_Id;
          DadosLancamentoParc.DataCadastro   := Now;

          DadosLancamentoParc.VlrPrevisto := cdsComissoes.FieldByName('VR_COMISSAO_PAGO').AsCurrency;

          if not cdsComissoes.FieldByName('DATA_PAGAMENTO').IsNull then
          begin
            DadosLancamentoParc.VlrPago        := cdsComissoes.FieldByName('VR_COMISSAO_PAGO').AsCurrency;
            DadosLancamentoParc.DataPagamento  := cdsComissoes.FieldByName('DATA_PAGAMENTO').AsDateTime;
            DadosLancamentoParc.FlgStatus      := 'G';
            DadosLancamentoParc.PagIdUsuario   := vgUsu_Id;
          end;

          ListaLancamentoParcs.Add(DadosLancamentoParc);

          dmPrincipal.InsertLancamentoParc(0, 0);

          dmPrincipal.FinalizarComponenteLancamento;
        end
        else
        begin  //Edicao
          { COMISSAO }
          IdCom := cdsComissao.FieldByName('ID_COMISSAO').AsInteger;

          cdsComissao.Edit;
          cdsComissao.FieldByName('PERCENT_COMISSAO').AsCurrency := cdsComissoes.FieldByName('PERCENT_COMISSAO_PAGO').AsCurrency;
          cdsComissao.FieldByName('VR_COMISSAO').AsCurrency      := cdsComissoes.FieldByName('VR_COMISSAO_PAGO').AsCurrency;
          cdsComissao.Post;
          cdsComissao.ApplyUpdates(0);

          { LANCAMENTO }
          QryAuxS.Close;
          QryAuxS.SQL.Clear;
          QryAuxS.SQL.Text := 'SELECT COD_LANCAMENTO, ANO_LANCAMENTO ' +
                              '  FROM LANCAMENTO ' +
                              ' WHERE ID_ORIGEM      = :ID_ORIGEM ' +
                              '   AND ID_NATUREZA_FK = :ID_NATUREZA';
          QryAuxS.Params.ParamByName('ID_ORIGEM').Value   := IdCom;
          QryAuxS.Params.ParamByName('ID_NATUREZA').Value := 2;  //COMISSAO
          QryAuxS.Open;

          dmPrincipal.InicializarComponenteLancamento;

          DadosLancamento := Lancamento.Create;

          DadosLancamento.CodLancamento := QryAuxS.FieldByName('COD_LANCAMENTO').AsInteger;
          DadosLancamento.AnoLancamento := QryAuxS.FieldByName('ANO_LANCAMENTO').AsInteger;

          if not cdsComissoes.FieldByName('DATA_PAGAMENTO').IsNull then
          begin
            DadosLancamento.VlrTotalPago := cdsComissoes.FieldByName('VR_COMISSAO_PAGO').AsCurrency;
            DadosLancamento.FlgStatus    := 'G';
          end;

          DadosLancamento.VlrTotalPrev := cdsComissoes.FieldByName('VR_COMISSAO_PAGO').AsCurrency;
          DadosLancamento.Observacao   := 'Receita originada pelo COMISS�O ' + IntToStr(IdCom) + '.';

          ListaLancamentos.Add(DadosLancamento);

          dmPrincipal.UpdateLancamento(0);

          { PARCELAS }
          DadosLancamentoParc := LancamentoParc.Create;

          DadosLancamentoParc.VlrPrevisto := cdsComissoes.FieldByName('VR_COMISSAO_PAGO').AsCurrency;

          if not cdsComissoes.FieldByName('DATA_PAGAMENTO').IsNull then
          begin
            DadosLancamentoParc.VlrPago        := cdsComissoes.FieldByName('VR_COMISSAO_PAGO').AsCurrency;
            DadosLancamentoParc.DataPagamento  := cdsComissoes.FieldByName('DATA_PAGAMENTO').AsDateTime;
            DadosLancamentoParc.FlgStatus      := 'G';
            DadosLancamentoParc.PagIdUsuario   := vgUsu_Id;
          end;

          ListaLancamentoParcs.Add(DadosLancamentoParc);

          dmPrincipal.UpdateLancamentoParc(0, 0);

          dmPrincipal.FinalizarComponenteLancamento;
        end;

        { LOG }
        if cdsComissao.RecordCount = 0 then
        begin
          with QryAuxS do
          begin
            QryAuxS.Close;
            QryAuxS.SQL.Clear;
            QryAuxS.SQL.Text := 'INSERT INTO USUARIO_LOG (ID_USUARIO_LOG, ID_USUARIO, ID_MODULO_FK, ' +
                                '                         TIPO_LANCAMENTO, CODIGO_LAN, ANO_LAN, ID_ORIGEM, ' +
                                '                         TABELA_ORIGEM, TIPO_ACAO,  OBS_USUARIO_LOG) ' +
                                '                 VALUES (:ID_USUARIO_LOG, :ID_USUARIO, :ID_MODULO, ' +
                                '                         :TIPO_LANCAMENTO, :CODIGO_LAN, :ANO_LAN, :ID_ORIGEM, ' +
                                '                         :TABELA_ORIGEM, :TIPO_ACAO,  :OBS_USUARIO_LOG)';

            QryAuxS.Params.ParamByName('ID_USUARIO_LOG').Value  := BS.ProximoId('ID_USUARIO_LOG', 'USUARIO_LOG');
            QryAuxS.Params.ParamByName('ID_USUARIO').Value      := vgUsu_Id;
            QryAuxS.Params.ParamByName('ID_MODULO').Value       := 80;
            QryAuxS.Params.ParamByName('TIPO_LANCAMENTO').Value := 'D';
            QryAuxS.Params.ParamByName('CODIGO_LAN').Value      := ListaLancamentos[0].CodLancamento;
            QryAuxS.Params.ParamByName('ANO_LAN').Value         := ListaLancamentos[0].AnoLancamento;
            QryAuxS.Params.ParamByName('ID_ORIGEM').Value       := IdCom;
            QryAuxS.Params.ParamByName('TABELA_ORIGEM').Value   := 'COMISSAO';
            QryAuxS.Params.ParamByName('TIPO_ACAO').Value       := 'I';
            QryAuxS.Params.ParamByName('OBS_USUARIO_LOG').Value := IfThen(cdsComissao.RecordCount = 0, 'Inclus�o', 'Edi��o') +
                                                                   ' AUTOM�TICA de Despesa de COMISSAO.';

            QryAuxS.ExecSQL;
          end;
        end;
      end;

      cdsComissoes.Next;
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    Msg := 'Dados do Fechamento de Comiss�es gravados com sucesso!';
  except
    on E: Exception do
    begin
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Result := False;
      Msg := 'Erro na grava��o do Dados do Fechamento de Comiss�es.';
    end;
  end;
end;

procedure TFCadastroFechamentoComissao.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  { Obs.: Para esse log, coloquei o ID_ORIGEM como sendo o mes e o ano das,
          comissoes visto que o fechamento e feito com 1 ou mais comissoes e
          nao ha como definir qual foi consultada/incuida/editada. }

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta de Fechamento de Comiss�es',
                          (StrToInt(IntToStr(cbMes.ItemIndex + 1) + edtAno.Text)),
                          'COMISSAO');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclus�o de Fechamento de Comiss�es',
                          (StrToInt(IntToStr(cbMes.ItemIndex + 1) + edtAno.Text)),
                          'COMISSAO');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da edi��o desse Fechamento de Comiss�es:', '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de Fechamento de Comiss�es';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          (StrToInt(IntToStr(cbMes.ItemIndex + 1) + edtAno.Text)),
                          'COMISSAO');
    end;
  end;
end;

procedure TFCadastroFechamentoComissao.HabDesabBotoesCRUD(Ed: Boolean);
begin
  btnConfirmarComissao.Enabled := vgPrm_RealFecCom and (not Ed);
  btnCancelarComissao.Enabled  := vgPrm_RealFecCom and (not Ed);

  btnAbrirComissoes.Enabled := Ed;

  btnOk.Enabled       := Ed;
  btnCancelar.Enabled := Ed;
end;

procedure TFCadastroFechamentoComissao.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroFechamentoComissao.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

procedure TFCadastroFechamentoComissao.tvwComissoesClick(Sender: TObject);
begin
  inherited;

  if tvwComissoes.SelectionCount > 0 then
  begin
    if tvwComissoes.Selected.Level = 2 then
    begin
      cdsComissoes.Locate('NOME_FUNCIONARIO; NOME_SISTEMA; DESCR_ITEM',
                          VarArrayOf([tvwComissoes.Selected.Parent.Parent.Text,
                                      tvwComissoes.Selected.Parent.Text,
                                      tvwComissoes.Selected.Text]),
                          [loCaseInsensitive, loPartialKey]);

      if cdsComissoes.FieldByName('FLG_CANCELADO').AsString = 'S' then
        lblCancelada.Visible := True
      else
      begin
        lblCancelada.Visible := False;

        cdsComissoes.Edit;

        HabDesabBotoesCRUD(False);
        DesabilitaHabilitaCampos(True);

        if cedPercentPaga.CanFocus then
          cedPercentPaga.SetFocus;
      end;
    end
    else
    begin
      if cdsComissoes.State in [dsInsert, dsEdit] then
        cdsComissoes.Cancel;

      if tvwComissoes.Selected.Level = 0 then
      begin
        cdsComissoes.Locate('NOME_FUNCIONARIO',
                            tvwComissoes.Selected.Text,
                            [loCaseInsensitive, loPartialKey]);
      end;

      if tvwComissoes.Selected.Level = 1 then
      begin
        cdsComissoes.Locate('NOME_FUNCIONARIO; NOME_SISTEMA',
                            VarArrayOf([tvwComissoes.Selected.Parent.Text,
                                        tvwComissoes.Selected.Text]),
                            [loCaseInsensitive, loPartialKey]);
      end;

      HabDesabBotoesCRUD(True);
      DesabilitaHabilitaCampos(False);
    end;

    lblNomeFuncionario.Visible   := tvwComissoes.Selected.Level in [0, 1, 2];
    edtNomeFuncionario.Visible   := tvwComissoes.Selected.Level in [0, 1, 2];
    lblNomeSistema.Visible       := tvwComissoes.Selected.Level in [1, 2];
    edtNomeSistema.Visible       := tvwComissoes.Selected.Level in [1, 2];
    lblDescricaoItem.Visible     := tvwComissoes.Selected.Level = 2;
    edtDescricaoItem.Visible     := tvwComissoes.Selected.Level = 2;
    lblValorTotalItem.Visible    := tvwComissoes.Selected.Level = 2;
    cedValorTotalItem.Visible    := tvwComissoes.Selected.Level = 2;
    lblPercentPrevista.Visible   := tvwComissoes.Selected.Level = 2;
    cedPercentPrevista.Visible   := tvwComissoes.Selected.Level = 2;
    lblValorPrevisto.Visible     := tvwComissoes.Selected.Level = 2;
    cedValorPrevisto.Visible     := tvwComissoes.Selected.Level = 2;
    lblPercentPaga.Visible       := tvwComissoes.Selected.Level = 2;
    cedPercentPaga.Visible       := tvwComissoes.Selected.Level = 2;
    lblValorPago.Visible         := tvwComissoes.Selected.Level = 2;
    cedValorPago.Visible         := tvwComissoes.Selected.Level = 2;
    lblDataComissao.Visible      := tvwComissoes.Selected.Level = 2;
    dteDataComissao.Visible      := tvwComissoes.Selected.Level = 2;
    lblDataPagamento.Visible     := tvwComissoes.Selected.Level = 2;
    dteDataPagamento.Visible     := tvwComissoes.Selected.Level = 2;
    btnConfirmarComissao.Visible := tvwComissoes.Selected.Level = 2;
    btnCancelarComissao.Visible  := tvwComissoes.Selected.Level = 2;
  end;
end;

procedure TFCadastroFechamentoComissao.tvwComissoesCustomDrawItem(
  Sender: TCustomTreeView; Node: TTreeNode; State: TCustomDrawState;
  var DefaultDraw: Boolean);
begin
  inherited;

  with Sender as TCustomTreeView do
  begin
    if Node.Selected then
    begin
      Canvas.Brush.Color := $00F5E1CF;
      Canvas.Font.Color  := clNavy;
    end
    else
    begin
      Canvas.Brush.Color := clWhite;
      Canvas.Font.Color  := clBlack;
    end;
  end;
end;

function TFCadastroFechamentoComissao.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;
end;

function TFCadastroFechamentoComissao.VerificarLayoutAto(var QtdErros: Integer): Boolean;
begin
  //
end;

end.
