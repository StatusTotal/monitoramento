{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroEstoqueAcerto.pas
  Descricao:   Formulario de cadastro de Acertos de Estoque
  Author   :   Cristina
  Date:        29-jul-2016
  Last Update: 11-out-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroEstoqueAcerto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  JvToolEdit, JvDBControls, Vcl.Mask, JvExMask, JvBaseEdits, Vcl.Grids,
  Vcl.DBGrids, Vcl.DBCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Datasnap.DBClient,
  Datasnap.Provider, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.ComCtrls,
  JvExComCtrls, JvDateTimePicker, JvDBDateTimePicker, JvMaskEdit;

type
  TFCadastroEstoqueAcerto = class(TFCadastroGeralPadrao)
    lblItem: TLabel;
    lblDataHistorico: TLabel;
    lblTipoHistorico: TLabel;
    lblQuantidade: TLabel;
    lblValor: TLabel;
    lcbTipoHistorico: TDBLookupComboBox;
    dbgHistoricoItem: TDBGrid;
    cedQuantidade: TJvDBCalcEdit;
    cedValor: TJvDBCalcEdit;
    qryHistoricoItem: TFDQuery;
    qryTipoHistorico: TFDQuery;
    dsTipoHistorico: TDataSource;
    dspHistoricoItem: TDataSetProvider;
    cdsHistoricoItem: TClientDataSet;
    cdsHistoricoItemID_HISTORICO_ITEM: TIntegerField;
    cdsHistoricoItemID_ITEM_FK: TIntegerField;
    cdsHistoricoItemID_USUARIO: TIntegerField;
    cdsHistoricoItemQUANTIDADE: TIntegerField;
    cdsHistoricoItemVR_UNITARIO: TBCDField;
    cdsHistoricoItemID_TIPO_HISTORICO_FK: TIntegerField;
    cdsHistoricoItemFLG_TIPO_HISTORICO: TStringField;
    cdsHistoricoItemDESCR_TIPO_HISTORICO: TStringField;
    lblQtdAtual: TLabel;
    edtItem: TEdit;
    cdsHistoricoItemDATA_HISTORICO: TSQLTimeStampField;
    medDataHistorico: TJvDBMaskEdit;
    procedure cdsHistoricoItemVR_UNITARIOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsHistoricoItemQUANTIDADEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure cdsHistoricoItemCalcFields(DataSet: TDataSet);
    procedure lcbTipoHistoricoExit(Sender: TObject);
    procedure cedValorKeyPress(Sender: TObject; var Key: Char);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }

    QtdAtual: Integer;
    NomeItem: String;
    ValorItem: Currency;
  end;

var
  FCadastroEstoqueAcerto: TFCadastroEstoqueAcerto;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{ TFCadastroEstoqueAcerto }

procedure TFCadastroEstoqueAcerto.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroEstoqueAcerto.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroEstoqueAcerto.cdsHistoricoItemCalcFields(DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  inherited;

  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with qryAux, SQL do
  begin
    //Item
    if cdsHistoricoItem.FieldByName('ID_TIPO_HISTORICO_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT ID_TIPO_HISTORICO, DESCR_TIPO_HISTORICO ' +
              '  FROM TIPO_HISTORICO ' +
              ' WHERE ID_TIPO_HISTORICO = :ID_TIPO_HISTORICO';
      Params.ParamByName('ID_TIPO_HISTORICO').AsInteger := cdsHistoricoItem.FieldByName('ID_TIPO_HISTORICO_FK').AsInteger;
      Open;

      cdsHistoricoItem.FieldByName('DESCR_TIPO_HISTORICO').AsString := qryAux.FieldByName('DESCR_TIPO_HISTORICO').AsString;
    end;
  end;

  FreeAndNil(qryAux);
end;

procedure TFCadastroEstoqueAcerto.cdsHistoricoItemQUANTIDADEGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFCadastroEstoqueAcerto.cdsHistoricoItemVR_UNITARIOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFCadastroEstoqueAcerto.cedValorKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    if cedValor.Value < 0 then
      cedValor.Value := (cedValor.Value * -1);

    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if cedValor.Value < 0 then
      cedValor.Value := (cedValor.Value * -1);

    btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroEstoqueAcerto.DefinirTamanhoMaxCampos;
begin
  inherited;

  //
end;

procedure TFCadastroEstoqueAcerto.DesabilitarComponentes;
begin
  inherited;

  edtItem.Enabled           := False;
  medDataHistorico.Enabled  := False;

  dbgHistoricoItem.ReadOnly := True;

  if vgOperacao = C then
  begin
    lblDataHistorico.Visible := False;
    medDataHistorico.Visible := False;
    lblTipoHistorico.Visible := False;
    lcbTipoHistorico.Visible := False;
    lblQuantidade.Visible    := False;
    cedQuantidade.Visible    := False;
    lblValor.Visible         := False;
    cedValor.Visible         := False;

    dbgHistoricoItem.Top    := lblDataHistorico.Top;
    dbgHistoricoItem.Height := ((pnlDados.Height - dbgHistoricoItem.Top) - 4);
  end
  else
  begin
    dbgHistoricoItem.Top    := (cedQuantidade.Top + cedQuantidade.Height + 4);
    dbgHistoricoItem.Height := ((pnlDados.Height - dbgHistoricoItem.Top) - 4);
  end;
end;

procedure TFCadastroEstoqueAcerto.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  cdsHistoricoItem.Cancel;

  vgOperacao     := VAZIO;
  vgIdConsulta   := 0;
  vgOrigemFiltro := False;

  inherited;
end;

procedure TFCadastroEstoqueAcerto.FormCreate(Sender: TObject);
begin
  inherited;

  qryTipoHistorico.Close;
  qryTipoHistorico.Open;

  cdsHistoricoItem.Close;
  cdsHistoricoItem.Params.ParamByName('ID_ITEM').AsInteger := vgIdConsulta;
  cdsHistoricoItem.Open;

  if vgOperacao = I then
    cdsHistoricoItem.Append;
end;

procedure TFCadastroEstoqueAcerto.FormShow(Sender: TObject);
begin
  if vgOperacao = I then
  begin
    if ValorItem >= 0 then
      cdsHistoricoItem.FieldByName('VR_UNITARIO').AsCurrency  := ValorItem;

    cdsHistoricoItem.FieldByName('ID_ITEM_FK').AsInteger      := vgIdConsulta;
    cdsHistoricoItem.FieldByName('DATA_HISTORICO').AsDateTime := Now;
    cdsHistoricoItem.FieldByName('ID_USUARIO').AsInteger      := vgUsu_Id;
  end;

  inherited;

  lblQtdAtual.Caption := 'Qtd. Atual: ' + IntToStr(QtdAtual);
  edtItem.Text        := UpperCase(NomeItem);

  if QtdAtual < 0 then
    lblQtdAtual.Color := clMaroon
  else if QtdAtual = 0 then
    lblQtdAtual.Color := clGray
  else if QtdAtual > 0 then
    lblQtdAtual.Color := clGreen;

  ShowScrollBar(dbgHistoricoItem.Handle, SB_HORZ, False);

  if lcbTipoHistorico.CanFocus then
    lcbTipoHistorico.SetFocus;
end;

function TFCadastroEstoqueAcerto.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroEstoqueAcerto.GravarDadosGenerico(var Msg: String): Boolean;
var
  lOk: Boolean;
  Valor: Currency;
begin
  Result := True;
  lOk    := True;

  Msg := '';

  Valor := -1;

  if vgOperacao = C then
    Exit;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    if cdsHistoricoItem.State in [dsInsert] then
    begin
      cdsHistoricoItem.FieldByName('ID_HISTORICO_ITEM').AsInteger := BS.ProximoId('ID_HISTORICO_ITEM', 'HISTORICO_ITEM');
      cdsHistoricoItem.Post;

      cdsHistoricoItem.ApplyUpdates(0);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na gravação de Estoque de Item.';

    lOk := False;
  end;

  //Atualiza Estoque Atual do Item
  if lOk then
  begin
    if not cdsHistoricoItem.FieldByName('VR_UNITARIO').IsNull then
      Valor := cdsHistoricoItem.FieldByName('VR_UNITARIO').AsCurrency;

    Msg := dmPrincipal.AtualizarEstoqueProduto(vgIdConsulta, Valor);

    if Trim(Msg) = '' then
      Msg := 'Estoque de Item gravado com sucesso!'
    else
    begin
      Msg := 'Erro na Atualização de Estoque do Item';
      Result := False;
    end;
  end;
end;

procedure TFCadastroEstoqueAcerto.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta da Movimentação de Estoque ',
                          vgIdConsulta, 'HISTORICO_ITEM');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclusão de Movimentação de Estoque',
                          vgIdConsulta, 'HISTORICO_ITEM');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO',
                                  'Por favor, indique o motivo da edição dessa Movimentação de Estoque' + ':',
                                  '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edição de Movimentação de Estoque';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          vgIdConsulta, 'HISTORICO_ITEM');
    end;
  end;
end;

procedure TFCadastroEstoqueAcerto.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroEstoqueAcerto.lcbTipoHistoricoExit(Sender: TObject);
begin
  inherited;

  if Trim(lcbTipoHistorico.Text) <> '' then
    cdsHistoricoItem.FieldByName('FLG_TIPO_HISTORICO').AsString := qryTipoHistorico.FieldByName('FLG_TIPO_HISTORICO').AsString
  else
    cdsHistoricoItem.FieldByName('FLG_TIPO_HISTORICO').Value    := Null;
end;

procedure TFCadastroEstoqueAcerto.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

function TFCadastroEstoqueAcerto.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if Trim(lcbTipoHistorico.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar o TIPO DE ACERTO.';
    DadosMsgErro.Componente := lcbTipoHistorico;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if cedQuantidade.Value <= 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar a QUANTIDADE.';
    DadosMsgErro.Componente := cedQuantidade;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroEstoqueAcerto.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

end.
