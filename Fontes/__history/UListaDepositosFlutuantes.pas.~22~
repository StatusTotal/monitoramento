{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UListaDepositosFlutuantes.pas
  Descricao:   Lista Simples de Depositos Flutuantes
  Author   :   Cristina
  Date:        08-nov-2016
  Last Update:
------------------------------------------------------------------------------------------------------------------------}

unit UListaDepositosFlutuantes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UListaSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Mask, JvExMask, JvToolEdit, System.DateUtils;

type
  TFListaDepositosFlutuantes = class(TFListaSimplesPadrao)
    lblPeriodo: TLabel;
    dtePeriodoIni: TJvDateEdit;
    dtePeriodoFim: TJvDateEdit;
    Label2: TLabel;
    qryGridListaPadraoID_DEPOSITO_FLUTUANTE: TIntegerField;
    qryGridListaPadraoDESCR_DEPOSITO_FLUTUANTE: TStringField;
    qryGridListaPadraoDATA_DEPOSITO: TDateField;
    qryGridListaPadraoNUM_COD_DEPOSITO: TStringField;
    qryGridListaPadraoVR_DEPOSITO_FLUTUANTE: TBCDField;
    qryGridListaPadraoFLG_STATUS: TStringField;
    qryGridListaPadraoDATA_STATUS: TDateField;
    qryGridListaPadraoST_ID_USUARIO: TIntegerField;
    qryGridListaPadraoCAD_ID_USUARIO: TIntegerField;
    qryGridListaPadraoDATA_CADASTRO: TSQLTimeStampField;
    procedure qryGridListaPadraoVR_DEPOSITO_FLUTUANTEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure dbgGridListaPadraoDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dtePeriodoFimKeyPress(Sender: TObject; var Key: Char);
    procedure dtePeriodoIniKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
    IdDepositoF: Integer;

    NumDepositoF: String;

    ValorDepositoF: Currency;

    DataDepositoF: TDateTime;
  end;

var
  FListaDepositosFlutuantes: TFListaDepositosFlutuantes;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

procedure TFListaDepositosFlutuantes.btnFiltrarClick(Sender: TObject);
begin
  inherited;

  qryGridListaPadrao.Close;
  qryGridListaPadrao.Params.ParamByName('DATA_INI').Value := dtePeriodoIni.Date;
  qryGridListaPadrao.Params.ParamByName('DATA_FIM').Value := dtePeriodoFim.Date;
  qryGridListaPadrao.Open;
end;

procedure TFListaDepositosFlutuantes.btnLimparClick(Sender: TObject);
begin
  dtePeriodoIni.Date := StartOfTheMonth(Date);
  dtePeriodoFim.Date := EndOfTheMonth(Date);

  inherited;

  if dtePeriodoIni.CanFocus then
    dtePeriodoFim.SetFocus;
end;

procedure TFListaDepositosFlutuantes.dbgGridListaPadraoDblClick(
  Sender: TObject);
var
  sTexto: String;
begin
  inherited;

  if qryGridListaPadrao.RecordCount = 0 then
  begin
    IdDepositoF    := 0;
    NumDepositoF   := '';
    ValorDepositoF := 0;
    DataDepositoF  := 0;
  end
  else
  begin
    IdDepositoF    := qryGridListaPadrao.FieldByName('ID').AsInteger;
    NumDepositoF   := qryGridListaPadrao.FieldByName('NUM_COD_DEPOSITO').AsString;
    ValorDepositoF := qryGridListaPadrao.FieldByName('VR_DEPOSITO_FLUTUANTE').AsCurrency;
    DataDepositoF  := qryGridListaPadrao.FieldByName('DATA_DEPOSITO').AsDateTime;

    sTexto := 'Confirma a sele��o do Dep�sito ' + qryGridListaPadrao.FieldByName('NUM_COD_DEPOSITO').AsString + '?';

    if Application.MessageBox(PChar(sTexto), 'Confirma��o', MB_YESNO + MB_ICONQUESTION) = ID_YES then
      Self.Close
    else
    begin
      IdDepositoF    := 0;
      NumDepositoF   := '';
      ValorDepositoF := 0;
    end;
  end;
end;

procedure TFListaDepositosFlutuantes.dtePeriodoFimKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
     if dtePeriodoFim.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso', MB_OK + MB_ICONWARNING);

      if dtePeriodoFim.CanFocus then
        dtePeriodoFim.SetFocus;
    end
    else if dtePeriodoIni.Date > dtePeriodoFim.Date then
    begin
      Application.MessageBox('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso', MB_OK + MB_ICONWARNING);

      dtePeriodoFim.Clear;

      if dtePeriodoFim.CanFocus then
        dtePeriodoFim.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dtePeriodoFim.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso', MB_OK + MB_ICONWARNING);

      if dtePeriodoFim.CanFocus then
        dtePeriodoFim.SetFocus;
    end
    else if dtePeriodoIni.Date > dtePeriodoFim.Date then
    begin
      Application.MessageBox('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso', MB_OK + MB_ICONWARNING);

      dtePeriodoFim.Clear;

      if dtePeriodoFim.CanFocus then
        dtePeriodoFim.SetFocus;
    end
    else
      btnFiltrar.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    btnFechar.Click;
end;

procedure TFListaDepositosFlutuantes.dtePeriodoIniKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
     if dtePeriodoIni.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso', MB_OK + MB_ICONWARNING);

      if dtePeriodoIni.CanFocus then
        dtePeriodoIni.SetFocus;
    end
    else if dtePeriodoIni.Date > dtePeriodoFim.Date then
    begin
      Application.MessageBox('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso', MB_OK + MB_ICONWARNING);

      dtePeriodoIni.Clear;

      if dtePeriodoIni.CanFocus then
        dtePeriodoIni.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dtePeriodoIni.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso', MB_OK + MB_ICONWARNING);

      if dtePeriodoIni.CanFocus then
        dtePeriodoIni.SetFocus;
    end
    else if dtePeriodoIni.Date > dtePeriodoFim.Date then
    begin
      Application.MessageBox('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso', MB_OK + MB_ICONWARNING);

      dtePeriodoIni.Clear;

      if dtePeriodoIni.CanFocus then
        dtePeriodoIni.SetFocus;
    end
    else
      btnFiltrar.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    btnFechar.Click;
end;

procedure TFListaDepositosFlutuantes.FormCreate(Sender: TObject);
begin
  inherited;

  IdDepositoF    := 0;
  NumDepositoF   := '';
  ValorDepositoF := 0;
end;

procedure TFListaDepositosFlutuantes.FormShow(Sender: TObject);
begin
  inherited;

  dtePeriodoIni.Date := StartOfTheMonth(Date);
  dtePeriodoFim.Date := EndOfTheMonth(Date);

  btnFiltrar.Click;

  ShowScrollBar(dbgGridListaPadrao.Handle, SB_HORZ, False);

  if dtePeriodoIni.CanFocus then
    dtePeriodoFim.SetFocus;
end;

procedure TFListaDepositosFlutuantes.qryGridListaPadraoVR_DEPOSITO_FLUTUANTEGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

end.
