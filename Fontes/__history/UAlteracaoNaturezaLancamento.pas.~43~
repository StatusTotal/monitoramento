{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UAlteracaoNaturezaLancamento.pas
  Descricao:   Formulario de alteracao da Natureza do Lancamento (Despesa ou Receita)
  Author   :   Cristina
  Date:        26-out-2017
  Last Update:
------------------------------------------------------------------------------------------------------------------------}

unit UAlteracaoNaturezaLancamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Datasnap.DBClient, Datasnap.Provider, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, JvExControls, JvButton, JvTransparentButton,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.DBCtrls, Vcl.Mask, JvBaseEdits, JvDBControls,
  JvExMask, JvToolEdit, System.StrUtils;

type
  TFAlteracaoNaturezaLancamento = class(TFCadastroGeralPadrao)
    qryLanc: TFDQuery;
    dspLanc: TDataSetProvider;
    cdsLanc: TClientDataSet;
    qryNatureza: TFDQuery;
    qryNaturezaID_NATUREZA: TIntegerField;
    qryNaturezaDESCR_NATUREZA: TStringField;
    qryNaturezaTIPO_NATUREZA: TStringField;
    qryNaturezaFLG_EDITAVEL: TStringField;
    qryNaturezaFLG_IMPOSTORENDA: TStringField;
    qryNaturezaFLG_AUXILIAR: TStringField;
    qryNaturezaFLG_PESSOAL: TStringField;
    dsNatureza: TDataSource;
    lblCodigo: TLabel;
    Label3: TLabel;
    lblAno: TLabel;
    lblDataLancamento: TLabel;
    lblNumeroRecibo: TLabel;
    lblValorTotalPrev: TLabel;
    lblJuros: TLabel;
    lblDesconto: TLabel;
    lblValorTotalPago: TLabel;
    lblNumParcelas: TLabel;
    edtCodigo: TDBEdit;
    edtAno: TDBEdit;
    dteDataLancamento: TJvDBDateEdit;
    cedValorTotalPrev: TJvDBCalcEdit;
    cedValorTotalJuros: TJvDBCalcEdit;
    cedValorTotalDesconto: TJvDBCalcEdit;
    cedValorTotalPago: TJvDBCalcEdit;
    edtQtdParcelasLanc: TDBEdit;
    gbNatureza: TGroupBox;
    btnIncluirNatureza: TJvTransparentButton;
    lblNaturezaAtual: TLabel;
    lblNaturezaNova: TLabel;
    lcbNaturezaNova: TDBLookupComboBox;
    edtNaturezaAtual: TEdit;
    gbClassificacaoAtual: TGroupBox;
    gbClassificacaoNova: TGroupBox;
    chbImpostoRendaN: TDBCheckBox;
    chbAuxiliarN: TDBCheckBox;
    chbPessoalN: TDBCheckBox;
    chbImpostoRendaA: TCheckBox;
    chbAuxiliarA: TCheckBox;
    chbPessoalA: TCheckBox;
    cdsLancID_NATUREZA_FK: TIntegerField;
    cdsLancCOD_LANCAMENTO: TIntegerField;
    cdsLancANO_LANCAMENTO: TIntegerField;
    cdsLancDATA_LANCAMENTO: TDateField;
    cdsLancTIPO_LANCAMENTO: TStringField;
    cdsLancFLG_IMPOSTORENDA: TStringField;
    cdsLancFLG_AUXILIAR: TStringField;
    cdsLancFLG_PESSOAL: TStringField;
    cdsLancQTD_PARCELAS: TIntegerField;
    cdsLancVR_TOTAL_PREV: TBCDField;
    cdsLancVR_TOTAL_JUROS: TBCDField;
    cdsLancVR_TOTAL_DESCONTO: TBCDField;
    cdsLancVR_TOTAL_PAGO: TBCDField;
    cdsLancNUM_RECIBO: TIntegerField;
    cdsLancDESCR_NATUREZA: TStringField;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure cdsLancCalcFields(DataSet: TDataSet);
    procedure btnIncluirNaturezaClick(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure chbPessoalNKeyPress(Sender: TObject; var Key: Char);
    procedure lcbNaturezaNovaClick(Sender: TObject);
    procedure lcbNaturezaNovaExit(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }

    procedure AbrirListaNatureza(TipoLanc: String);
  public
    { Public declarations }
  end;

var
  FAlteracaoNaturezaLancamento: TFAlteracaoNaturezaLancamento;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UCadastroNatureza;

{ TFAlteracaoNaturezaLancamento }

procedure TFAlteracaoNaturezaLancamento.AbrirListaNatureza(TipoLanc: String);
begin
  qryNatureza.Close;

  if vgOperacao in [I, E] then
  begin
    qryNatureza.SQL.Clear;
    qryNatureza.SQL.Text := 'SELECT * ' +
                            '  FROM NATUREZA ' +
                            ' WHERE TIPO_NATUREZA = :TIPO_NATUREZA ' +
                            '   AND ID_NATUREZA NOT IN (1, 2, 3, 7)' +
                            'ORDER BY DESCR_NATUREZA';
  end
  else
  begin
    qryNatureza.SQL.Clear;
    qryNatureza.SQL.Text := 'SELECT * ' +
                            '  FROM NATUREZA ' +
                            ' WHERE TIPO_NATUREZA = :TIPO_NATUREZA ' +
                            'ORDER BY DESCR_NATUREZA';
  end;

  qryNatureza.Params.ParamByName('TIPO_NATUREZA').Value := Trim(TipoLanc);
  qryNatureza.Open;

  if cdsLanc.State in [dsInsert, dsEdit] then
  begin
    cdsLanc.FieldByName('FLG_IMPOSTORENDA').AsString := qryNatureza.FieldByName('FLG_IMPOSTORENDA').AsString;
    cdsLanc.FieldByName('FLG_AUXILIAR').AsString     := qryNatureza.FieldByName('FLG_AUXILIAR').AsString;
    cdsLanc.FieldByName('FLG_PESSOAL').AsString      := qryNatureza.FieldByName('FLG_PESSOAL').AsString;
  end;
end;

procedure TFAlteracaoNaturezaLancamento.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFAlteracaoNaturezaLancamento.btnIncluirNaturezaClick(
  Sender: TObject);
var
  Op: TOperacao;
  IdCons: Integer;
  OrigF, OrigC: Boolean;
begin
  inherited;

  Op     := vgOperacao;
  IdCons := vgIdConsulta;
  OrigF  := vgOrigemFiltro;
  OrigC  := vgOrigemCadastro;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := False;
  vgOrigemCadastro := True;

  try
    Application.CreateForm(TFCadastroNatureza, FCadastroNatureza);

    if vgLanc_TpLanc = 'D' then
      FCadastroNatureza.TipoNat := DESP
    else
      FCadastroNatureza.TipoNat := REC;

    FCadastroNatureza.ShowModal;
  finally
    AbrirListaNatureza(vgLanc_TpLanc);

    if FCadastroNatureza.IdNovaNatureza <> 0 then
      cdsLanc.FieldByName('ID_NATUREZA_FK').AsInteger := FCadastroNatureza.IdNovaNatureza;

    FCadastroNatureza.Free;
  end;

  vgOperacao       := Op;
  vgIdConsulta     := IdCons;
  vgOrigemFiltro   := OrigF;
  vgOrigemCadastro := OrigC;
end;

procedure TFAlteracaoNaturezaLancamento.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFAlteracaoNaturezaLancamento.cdsLancCalcFields(DataSet: TDataSet);
var
  QryAux: TFDQuery;
begin
  inherited;

  QryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

  QryAux.Close;
  QryAux.SQL.Clear;
  QryAux.SQL.Text := 'SELECT DESCR_NATUREZA ' +
                     '  FROM NATUREZA ' +
                     ' WHERE ID_NATUREZA = :ID_NATUREZA';
  QryAux.Params.ParamByName('ID_NATUREZA').Value := cdsLanc.FieldByName('ID_NATUREZA_FK').AsInteger;
  QryAux.Open;

  if QryAux.RecordCount > 0 then
    cdsLanc.FieldByName('DESCR_NATUREZA').AsString := QryAux.FieldByName('DESCR_NATUREZA').AsString;

  FreeAndNil(QryAux);
end;

procedure TFAlteracaoNaturezaLancamento.chbPessoalNKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFAlteracaoNaturezaLancamento.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtNaturezaAtual.MaxLength := 250;
end;

procedure TFAlteracaoNaturezaLancamento.DesabilitarComponentes;
begin
  inherited;

  btnDadosPrincipais.Visible := False;

  chbImpostoRendaN.ReadOnly := True;
  chbAuxiliarN.ReadOnly     := True;
  chbPessoalN.ReadOnly      := True;
end;

procedure TFAlteracaoNaturezaLancamento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  cdsLanc.Cancel;

  vgOperacao     := VAZIO;
  vgIdConsulta   := 0;
  vgOrigemFiltro := False;

  inherited;
end;

procedure TFAlteracaoNaturezaLancamento.FormShow(Sender: TObject);
begin
  inherited;

  AbrirListaNatureza(vgLanc_TpLanc);

  if cdsLanc.FieldByName('TIPO_LANCAMENTO').AsString = 'R' then
  begin
    Self.Caption := 'Pagamento de Parcela de Lan�amento - Receita';

    lblNumeroRecibo.Visible := True;
    lblNumeroRecibo.Caption := 'N� do Recibo: ' + cdsLanc.FieldByName('NUM_RECIBO').AsString;
  end
  else if cdsLanc.FieldByName('TIPO_LANCAMENTO').AsString = 'D' then
  begin
    Self.Caption := 'Pagamento de Parcela de Lan�amento - Despesa';

    lblNumeroRecibo.Visible := False;
  end;

  cdsLanc.Close;
  cdsLanc.Params.ParamByName('COD_LANCAMENTO').Value := vgLanc_Codigo;
  cdsLanc.Params.ParamByName('ANO_LANCAMENTO').Value := vgLanc_Ano;
  cdsLanc.Open;

  edtNaturezaAtual.Text    := Trim(cdsLanc.FieldByName('DESCR_NATUREZA').AsString);
  chbImpostoRendaA.Checked := (Trim(cdsLanc.FieldByName('FLG_IMPOSTORENDA').AsString) = 'S');
  chbAuxiliarA.Checked     := (Trim(cdsLanc.FieldByName('FLG_AUXILIAR').AsString) = 'S');
  chbPessoalA.Checked      := (Trim(cdsLanc.FieldByName('FLG_PESSOAL').AsString) = 'S');

  cdsLanc.Edit;

  if lcbNaturezaNova.CanFocus then
    lcbNaturezaNova.SetFocus;
end;

function TFAlteracaoNaturezaLancamento.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFAlteracaoNaturezaLancamento.GravarDadosGenerico(
  var Msg: String): Boolean;
begin
  Result := True;
  Msg    := '';

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    cdsLanc.Post;
    cdsLanc.ApplyUpdates(0);

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    Msg := 'Natureza de Lancamento alterada com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na altera��o da Natureza de Lan�amento.';
  end;
end;

procedure TFAlteracaoNaturezaLancamento.GravarLog;
var
  sTipo: String;
begin
  inherited;

  sTipo := IfThen((Trim(cdsLanc.FieldByName('TIPO_LANCAMENTO').AsString) = 'D'), 'Despesa', 'Receita');

  BS.GravarUsuarioLog(2, '', 'Edi��o de Natureza de Lan�amento de ' + sTipo + ' ap�s Lan�amento estar PAGO.');
end;

procedure TFAlteracaoNaturezaLancamento.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFAlteracaoNaturezaLancamento.lcbNaturezaNovaClick(Sender: TObject);
begin
  inherited;

  if cdsLanc.State in [dsInsert, dsEdit] then
  begin
    cdsLanc.FieldByName('FLG_IMPOSTORENDA').AsString := qryNatureza.FieldByName('FLG_IMPOSTORENDA').AsString;
    cdsLanc.FieldByName('FLG_AUXILIAR').AsString     := qryNatureza.FieldByName('FLG_AUXILIAR').AsString;
    cdsLanc.FieldByName('FLG_PESSOAL').AsString      := qryNatureza.FieldByName('FLG_PESSOAL').AsString;
  end;
end;

procedure TFAlteracaoNaturezaLancamento.lcbNaturezaNovaExit(Sender: TObject);
begin
  inherited;

  if cdsLanc.State in [dsInsert, dsEdit] then
  begin
    cdsLanc.FieldByName('FLG_IMPOSTORENDA').AsString := qryNatureza.FieldByName('FLG_IMPOSTORENDA').AsString;
    cdsLanc.FieldByName('FLG_AUXILIAR').AsString     := qryNatureza.FieldByName('FLG_AUXILIAR').AsString;
    cdsLanc.FieldByName('FLG_PESSOAL').AsString      := qryNatureza.FieldByName('FLG_PESSOAL').AsString;
  end;
end;

procedure TFAlteracaoNaturezaLancamento.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

function TFAlteracaoNaturezaLancamento.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if Trim(lcbNaturezaNova.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a nova NATUREZA DO LAN�AMENTO..';
    DadosMsgErro.Componente := lcbNaturezaNova;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFAlteracaoNaturezaLancamento.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

end.
