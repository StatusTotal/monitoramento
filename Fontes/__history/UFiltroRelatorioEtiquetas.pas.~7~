{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroRelatorioEtiquetas.pas
  Descricao:   Filtro de Relatorio de Etiquetas de Seguranca
  Author   :   Pedro
  Date:        19-jun-2017
  Last Update: 25-ago-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroRelatorioEtiquetas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils,System.DateUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroRelatorioPadrao, Vcl.StdCtrls,
  JvExControls, JvButton, JvTransparentButton, Vcl.ExtCtrls, sGroupBox,
  UDMRelatorios, Vcl.Mask, sMaskEdit, sCustomComboEdit, sToolEdit, UDM,
  UBibliotecaSistema, UGDM;

type
  TFFiltroRelatorioEtiquetas = class(TFFiltroRelatorioPadrao)
    rgstatus: TsRadioGroup;
    dtInicial: TsDateEdit;
    dtFinal: TsDateEdit;
    rgSistema: TsRadioGroup;
    procedure btnVisualizarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    procedure GravarLog; override;
    procedure GerarRelatorio(ImpDet: Boolean); override;
    function  VerificarFiltro: Boolean; override;
  private
    { Private declarations }
    lPodeCalcular:Boolean;
  public
    { Public declarations }
  end;

var
  FFiltroRelatorioEtiquetas: TFFiltroRelatorioEtiquetas;

implementation

{$R *.dfm}

uses UVariaveisGlobais;

procedure TFFiltroRelatorioEtiquetas.btnLimparClick(Sender: TObject);
begin
  inherited;

  dtInicial.Date :=StartOfTheMonth(Date);
  dtFinal.Date:=EndOfTheMonth(Date);
  rgstatus.ItemIndex :=0;
  rgSistema.ItemIndex:=0;

  chbExportarPDF.Checked := False;

  chbExportarExcel.Enabled := vgPrm_ExpEtq;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;
end;

procedure TFFiltroRelatorioEtiquetas.btnVisualizarClick(Sender: TObject);
begin
  inherited;

  if not VerificarFiltro then
    Exit;

  dmRelatorios.FDEtiquetas.Close;

  case rgstatus.ItemIndex of
    0: dmRelatorios.FDEtiquetas.Params.ParamByName('ST').Value :='*';
    1: dmRelatorios.FDEtiquetas.Params.ParamByName('ST').Value :='U';
    2: dmRelatorios.FDEtiquetas.Params.ParamByName('ST').Value :='N';
    3: dmRelatorios.FDEtiquetas.Params.ParamByName('ST').Value :='C';
  end;

  case rgSistema.ItemIndex of
    0: dmRelatorios.FDEtiquetas.Params.ParamByName('SISTEMA').Value :=dmGerencial.Replicar('*',15);
    1: dmRelatorios.FDEtiquetas.Params.ParamByName('SISTEMA').Value :='%'+'FIRMAS'+'%';
    2: dmRelatorios.FDEtiquetas.Params.ParamByName('SISTEMA').Value :='%'+'NOTAS'+'%';
    3: dmRelatorios.FDEtiquetas.Params.ParamByName('SISTEMA').Value :='%'+'RCPN'+'%';
    4: dmRelatorios.FDEtiquetas.Params.ParamByName('SISTEMA').Value :='%'+'PROTESTO'+'%';
    5: dmRelatorios.FDEtiquetas.Params.ParamByName('SISTEMA').Value :='%'+'RGI'+'%';
    6: dmRelatorios.FDEtiquetas.Params.ParamByName('SISTEMA').Value :='%'+'RTD'+'%';
    7: dmRelatorios.FDEtiquetas.Params.ParamByName('SISTEMA').Value :='%'+'RCPJ'+'%';
  end;

  if (dtInicial.Date<> 0) and (dtFinal.Date <> 0) then
  begin
    dmRelatorios.FDEtiquetas.Params.ParamByName('D1').Value := dtInicial.Date;
    dmRelatorios.FDEtiquetas.Params.ParamByName('D2').Value := dtFinal.Date;
  end
  else
  begin
    dmRelatorios.FDEtiquetas.Params.ParamByName('D1').Value := '12/30/1899';
    dmRelatorios.FDEtiquetas.Params.ParamByName('D2').Value := '12/30/1899';
  end;

  dmRelatorios.FDEtiquetas.Open;

  if not dmRelatorios.FDEtiquetas.IsEmpty then
  begin
    if chbTipoSintetico.Checked then
       GerarRelatorio(False);

    if chbTipoAnalitico.Checked then
       GerarRelatorio(True);
  end;
end;

procedure TFFiltroRelatorioEtiquetas.FormCreate(Sender: TObject);
begin
  inherited;

  lblNomeRelatorio.Caption := 'Relat�rio Etiqueta de Seguran�a';

  dtInicial.Date :=StartOfTheMonth(Date);
  dtFinal.Date:=EndOfTheMonth(Date);
  rgstatus.ItemIndex :=0;
  rgSistema.ItemIndex:=0;
end;

procedure TFFiltroRelatorioEtiquetas.FormShow(Sender: TObject);
begin
  inherited;

  chbExportarExcel.Enabled := vgPrm_ExpEtq;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;
end;

procedure TFFiltroRelatorioEtiquetas.GerarRelatorio(ImpDet: Boolean);
begin
  inherited;
    try
    sTituloRelatorio    := 'ETIQUETA DE SEGURAN�A';
    sSubtituloRelatorio := 'Per�odo: ' + DateToStr(dtInicial.Date)+' at� '+DateToStr(dtFinal.Date);

    if ImpDet then
    begin
      lExibeDetalhe := True;
      dmRelatorios.DefinirRelatorio(R103DET);
    end
    else
    begin
      lExibeDetalhe := False;
      dmRelatorios.DefinirRelatorio(R103);
    end;

    lPodeCalcular := True;
    dmRelatorios.ImprimirRelatorio(chbExportarPDF.Checked, chbExportarExcel.Checked);

    GravarLog;
  except
    Application.MessageBox('Erro ao gerar Etiqueta de Seguran�a.',
                           'Erro',
                           MB_OK + MB_ICONERROR);
  end;

end;

procedure TFFiltroRelatorioEtiquetas.GravarLog;
begin
  inherited;
  BS.GravarUsuarioLog(5,
                      '',
                      'Impress�o de Relatorio de Etiqueta de Seguran�a (' + DateToStr(dDataRelatorio) +
                                                                     ' ' +
                                                                     TimeToStr(tHoraRelatorio) + ')',
                      0,
                      'ETIQUETA');
end;

function TFFiltroRelatorioEtiquetas.VerificarFiltro: Boolean;
begin
  Result :=True;

  if (dtInicial.Date = 0) and (dtFinal.Date<>0) then
  begin
    dmGerencial.Aviso('Digite corretamente os campos de data');
    Result := False;
    Exit;
  end;

    if (dtInicial.Date <> 0) and (dtFinal.Date=0) then
  begin
    dmGerencial.Aviso('Digite corretamente os campos de data');
    Result :=False;
    Exit;
  end;
end;

end.
