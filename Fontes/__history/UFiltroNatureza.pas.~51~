{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroNatureza.pas
  Descricao:   Filtro de Naturezas de Despesas ou Receitas
  Author   :   Cristina
  Date:        11-nov-2016
  Last Update: 03-dez-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroNatureza;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.StdCtrls;

type
  TFFiltroNatureza = class(TFFiltroSimplesPadrao)
    lblDescrNatureza: TLabel;
    edtDescrNatureza: TEdit;
    qryGridPaiPadraoID: TIntegerField;
    qryGridPaiPadraoDESCR_NATUREZA: TStringField;
    qryGridPaiPadraoTIPO_NATUREZA: TStringField;
    qryGridPaiPadraoFLG_EDITAVEL: TStringField;
    qryGridPaiPadraoFLG_IMPOSTORENDA: TStringField;
    qryGridPaiPadraoFLG_AUXILIAR: TStringField;
    qryGridPaiPadraoFLG_PESSOAL: TStringField;
    procedure btnEditarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure edtDescrNaturezaKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroNatureza: TFFiltroNatureza;

  IdNatureza: Integer;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UCadastroNatureza;

{ TFFiltroNatureza }

procedure TFFiltroNatureza.btnEditarClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao     := E;
  vgIdConsulta   := qryGridPaiPadrao.FieldByName('ID').AsInteger;
  vgOrigemFiltro := True;

  try
    Application.CreateForm(TFCadastroNatureza, FCadastroNatureza);
    FCadastroNatureza.TipoNat := NONE;
    FCadastroNatureza.ShowModal;
  finally
    FCadastroNatureza.Free;
  end;

  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroNatureza.btnExcluirClick(Sender: TObject);
var
  qryExclusao: TFDQuery;
begin
  inherited;

  if lPodeExcluir then
  begin
    qryExclusao := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      with qryExclusao, SQL do
      begin
        Close;
        Clear;
        Text := 'DELETE FROM NATUREZA WHERE ID_NATUREZA = :ID_NATUREZA';
        Params.ParamByName('ID_NATUREZA').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      GravarLog;
      Application.MessageBox('Natureza exclu�da com sucesso!', 'Aviso', MB_OK)
    except
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Application.MessageBox('Erro na exclus�o da Natureza.', 'Erro', MB_OK + MB_ICONERROR)
    end;

    FreeAndNil(qryExclusao);

    btnFiltrar.Click;
  end;
end;

procedure TFFiltroNatureza.btnFiltrarClick(Sender: TObject);
begin
  with qryGridPaiPadrao, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_NATUREZA AS ID, ' +
            '       TIPO_NATUREZA, ' +
            '       DESCR_NATUREZA, ' +
            '       FLG_EDITAVEL, ' +
            '       FLG_IMPOSTORENDA, ' +
            '       FLG_AUXILIAR, ' +
            '       FLG_PESSOAL ' +
            '  FROM NATUREZA ';

    if Trim(edtDescrNatureza.Text) <> '' then
    begin
      Add(' WHERE CAST(UPPER(DESCR_NATUREZA) AS VARCHAR(250)) LIKE CAST(:DESCR_NAT AS VARCHAR(250))');
      Params.ParamByName('DESCR_NAT').Value := '%' + Trim(UpperCase(edtDescrNatureza.Text)) + '%';
    end;

    Add('ORDER BY DESCR_NATUREZA');
    Open;
  end;

  inherited;
end;

procedure TFFiltroNatureza.btnIncluirClick(Sender: TObject);
begin
  inherited;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := True;

  try
    Application.CreateForm(TFCadastroNatureza, FCadastroNatureza);
    FCadastroNatureza.TipoNat := NONE;
    FCadastroNatureza.ShowModal;
  finally
    FCadastroNatureza.Free;
  end;

  btnFiltrar.Click;
end;

procedure TFFiltroNatureza.btnLimparClick(Sender: TObject);
begin
  edtDescrNatureza.Clear;

  inherited;

  if edtDescrNatureza.CanFocus then
    edtDescrNatureza.SetFocus;
end;

procedure TFFiltroNatureza.dbgGridPaiPadraoDblClick(Sender: TObject);
var
  sTexto: String;
begin
  inherited;

  if vgOrigemCadastro then
  begin
    if qryGridPaiPadrao.RecordCount = 0 then
      IdNatureza := 0
    else
    begin
      IdNatureza := qryGridPaiPadrao.FieldByName('ID').AsInteger;

      sTexto := 'Confirma a sele��o de ' + qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString + '?';

      if Application.MessageBox(PChar(sTexto), 'Confirma��o', MB_YESNO + MB_ICONQUESTION) = ID_YES then
        Self.Close
      else
        IdNatureza := 0;
    end;
  end
  else
  begin
    PegarPosicaoGrid;

    vgOperacao     := C;
    vgIdConsulta   := qryGridPaiPadrao.FieldByName('ID').AsInteger;
    vgOrigemFiltro := True;

    try
      Application.CreateForm(TFCadastroNatureza, FCadastroNatureza);
      FCadastroNatureza.TipoNat := NONE;
      FCadastroNatureza.ShowModal;
    finally
      FCadastroNatureza.Free;
    end;

    DefinirPosicaoGrid;
  end;
end;

procedure TFFiltroNatureza.edtDescrNaturezaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroNatureza.FormCreate(Sender: TObject);
begin
  inherited;

  IdNatureza := 0;
end;

procedure TFFiltroNatureza.FormShow(Sender: TObject);
begin
  with qryGridPaiPadrao, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_NATUREZA AS ID, ' +
            '       TIPO_NATUREZA, ' +
            '       DESCR_NATUREZA, ' +
            '       FLG_EDITAVEL, ' +
            '       FLG_IMPOSTORENDA, ' +
            '       FLG_AUXILIAR, ' +
            '       FLG_PESSOAL ' +
            '  FROM NATUREZA ' +
            ' ORDER BY DESCR_NATUREZA';
    Open;
  end;

  ShowScrollBar(dbgGridPaiPadrao.Handle, SB_HORZ, False);

  inherited;

  btnImprimir.Visible := False;

  if edtDescrNatureza.CanFocus then
    edtDescrNatureza.SetFocus;
end;

procedure TFFiltroNatureza.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  if vgOperacao = X then  //EXCLUSAO
  begin
    repeat
      sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da exclus�o dessa Natureza:', '');
    until sObservacao <> '';

    BS.GravarUsuarioLog(2, '', sObservacao,
                        qryGridPaiPadrao.FieldByName('ID').AsInteger, 'NATUREZA');
  end;
end;

function TFFiltroNatureza.PodeExcluir(var Msg: String): Boolean;
var
  QryVerif: TFDQuery;
begin
  inherited;

  Result := True;

  QryVerif := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryVerif, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT * FROM LANCAMENTO WHERE ID_NATUREZA_FK = :ID_NATUREZA';
    Params.ParamByName('ID_NATUREZA').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
    Open;

    if QryVerif.RecordCount > 0 then
      Msg := 'N�o � poss�vel excluir a Natureza informada, pois a mesma j� possui registro no Sistema.';
  end;

  if QryVerif.RecordCount > 0 then
  begin
    with QryVerif, SQL do
    begin
      Close;
      Clear;
      Text := 'SELECT ID_NATUREZA ' +
              '  FROM NATUREZA ' +
              ' WHERE FLG_EDITAVEL = ' + QuotedStr('N') +
              '   AND ID_NATUREZA_FK = :ID_NATUREZA';
      Params.ParamByName('ID_NATUREZA').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
      Open;

      if QryVerif.RecordCount > 0 then
        Msg := 'N�o � permitido excluir essa Natureza.';
    end;
  end;

  Result := (QryVerif.RecordCount = 0) and (qryGridPaiPadrao.RecordCount > 0);

  FreeAndNil(QryVerif);
end;

procedure TFFiltroNatureza.VerificarPermissoes;
begin
  inherited;

  btnIncluir.Enabled  := vgPrm_IncNat;
  btnEditar.Enabled   := vgPrm_EdNat and (qryGridPaiPadrao.RecordCount > 0);
  btnExcluir.Enabled  := vgPrm_ExcNat and (qryGridPaiPadrao.RecordCount > 0);
  btnImprimir.Enabled := False;
end;

end.
