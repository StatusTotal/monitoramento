{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroFolhaRCPN.pas
  Descricao:   Filtro de Folhas de RCPN
  Author   :   Pedro
  Date:        19-jun-2017
  Last Update: 08-nov-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroFolhaRCPN;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls,System.DateUtils,sBitBtn, Vcl.Mask, sMaskEdit, sCustomComboEdit,
  sToolEdit, Vcl.StdCtrls, sGroupBox, sComboBox, sEdit, Datasnap.DBClient,
  Datasnap.Provider, JvComponentBase, JvDBGridExport,
  System.StrUtils;

type
  TFFiltroFolhaRcpn = class(TFFiltroSimplesPadrao)
    edpesquisa: TsEdit;
    rgStatus: TsRadioGroup;
    edInicio: TsDateEdit;
    edFim: TsDateEdit;
    dbgxExport: TJvDBGridExcelExport;
    cbFiltro: TsComboBox;
    qryGridPaiPadraoID: TIntegerField;
    qryGridPaiPadraoID_LOTEFOLHA_FK: TIntegerField;
    qryGridPaiPadraoORDEM: TIntegerField;
    qryGridPaiPadraoLETRA: TStringField;
    qryGridPaiPadraoNUMERO: TIntegerField;
    qryGridPaiPadraoALEATORIO: TStringField;
    qryGridPaiPadraoNUM_FOLHA: TStringField;
    qryGridPaiPadraoDATA_UTILIZACAO: TDateField;
    qryGridPaiPadraoID_SISTEMA_FK: TIntegerField;
    qryGridPaiPadraoFLG_RCPN: TStringField;
    qryGridPaiPadraoFLG_SITUACAO: TStringField;
    qryGridPaiPadraoID_FUNCIONARIO_FK: TIntegerField;
    qryGridPaiPadraoID_NATUREZAATO_FK: TIntegerField;
    qryGridPaiPadraoOBS_FOLHASEGURANCA: TStringField;
    qryGridPaiPadraoDATA_CADASTRO: TSQLTimeStampField;
    qryGridPaiPadraoCAD_ID_USUARIO: TIntegerField;
    qryGridPaiPadraoDATA_CANCELAMENTO: TDateField;
    qryGridPaiPadraoCANCEL_ID_USUARIO: TIntegerField;
    qryGridPaiPadraoDATA_COMPRA: TDateField;
    qryGridPaiPadraoNOME_SISTEMA: TStringField;
    qryGridPaiPadraoNOME_FUNCIONARIO: TStringField;
    qryGridPaiPadraoSELO: TStringField;
    qryGridPaiPadraoSITUACAO: TStringField;
    procedure btnIncluirClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
//    procedure btnExportarClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure qryGridPaiPadraoCalcFields(DataSet: TDataSet);
    procedure dbgGridPaiPadraoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure cbFiltroChange(Sender: TObject);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure rgStatusExit(Sender: TObject);
    procedure rgStatusClick(Sender: TObject);
    procedure cbFiltroKeyPress(Sender: TObject; var Key: Char);
    procedure edInicioKeyPress(Sender: TObject; var Key: Char);
    procedure edFimKeyPress(Sender: TObject; var Key: Char);
    procedure edpesquisaKeyPress(Sender: TObject; var Key: Char);
    procedure btnImprimirClick(Sender: TObject);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }

    procedure AbrirLista;
  public
    { Public declarations }
  end;

var
  FFiltroFolhaRcpn: TFFiltroFolhaRcpn;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMFolhaSeguranca,
  UCadastroFolhaRCPN;

procedure TFFiltroFolhaRcpn.FormCreate(Sender: TObject);
begin
  edInicio.Date := StartOfTheMonth(Date);
  edFim.Date    := EndOfTheMonth(Date);

  cbFiltro.ItemIndex := 0;

  btnFiltrarClick(Sender);
end;

procedure TFFiltroFolhaRcpn.VerificarPermissoes;
begin
  inherited;

  btnIncluir.Visible  := False;

  btnEditar.Enabled   := vgPrm_EdFlsRCPN and (qryGridPaiPadrao.RecordCount > 0);
  btnExcluir.Enabled  := vgPrm_ExcFlsRCPN and (qryGridPaiPadrao.RecordCount > 0);
  btnImprimir.Enabled := vgPrm_ImpFlsRCPN and (qryGridPaiPadrao.RecordCount > 0);
end;

function TFFiltroFolhaRcpn.PodeExcluir(var Msg: String): Boolean;
begin
  Result := True;

  if qryGridPaiPadrao.FieldByName('FLG_SITUACAO').AsString = 'C' then
    msg := 'N�o � poss�vel cancelar a folha, pois o mesmo j� foi cancelado.';

  Result := (qryGridPaiPadrao.FieldByName('FLG_SITUACAO').AsString <> 'C') and
            (qryGridPaiPadrao.RecordCount > 0);
end;

procedure TFFiltroFolhaRcpn.rgStatusClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroFolhaRcpn.rgStatusExit(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroFolhaRcpn.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    X:  //EXCLUSAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo do cancelamento dessa Folha:', '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(4, '', sObservacao,
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'FOLHASEGURANCA');
    end;
    P:  //IMPRESSAO
    begin
      BS.GravarUsuarioLog(4, '', 'Somente Gerais do Lote.',
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'FOLHASEGURANCA');
    end;
  end;
end;

procedure TFFiltroFolhaRcpn.AbrirLista;
begin
  qryGridPaiPadrao.Close;
  qryGridPaiPadrao.SQL.Clear;
  qryGridPaiPadrao.SQL.Text := 'SELECT F.ID_FOLHASEGURANCA AS ID, ' +
                               '       F.ID_LOTEFOLHA_FK, ' +
                               '       F.ORDEM, ' +
                               '       F.LETRA, ' +
                               '       F.NUMERO, ' +
                               '       F.ALEATORIO, ' +
                               '       F.NUM_FOLHA, ' +
                               '       F.DATA_UTILIZACAO, ' +
                               '       F.ID_SISTEMA_FK, ' +
                               '       F.FLG_RCPN, ' +
                               '       F.FLG_SITUACAO, ' +
                               '       F.ID_FUNCIONARIO_FK, ' +
                               '       F.ID_NATUREZAATO_FK, ' +
                               '       F.OBS_FOLHASEGURANCA, ' +
                               '       F.DATA_CADASTRO, ' +
                               '       F.CAD_ID_USUARIO, ' +
                               '       F.DATA_CANCELAMENTO, ' +
                               '       F.CANCEL_ID_USUARIO, ' +
                               '       L.DATA_COMPRA, ' +
                               '       S.NOME_SISTEMA, ' +
                               '       FUNC.NOME_FUNCIONARIO ' +
                               '  FROM FOLHASEGURANCA F ' +
                               ' INNER JOIN LOTEFOLHA L ' +
                               '    ON F.ID_LOTEFOLHA_FK = L.ID_LOTEFOLHA ' +
                               '  LEFT JOIN FUNCIONARIO FUNC ' +
                               '    ON F.ID_FUNCIONARIO_FK = FUNC.ID_FUNCIONARIO ' +
                               '  LEFT JOIN NATUREZAATO N ' +
                               '    ON F.ID_NATUREZAATO_FK = N.ID_NATUREZAATO ' +
                               '  LEFT JOIN SISTEMA S ' +
                               '    ON F.ID_SISTEMA_FK = S.ID_SISTEMA ' +
                               ' WHERE F.FLG_RCPN = ' + QuotedStr('S');

  case cbFiltro.ItemIndex of
    0: //DATA COMPRA
    begin
      qryGridPaiPadrao.SQL.Add(' AND L.DATA_COMPRA BETWEEN :DT_COMP1 AND :DT_COMP2 ');
      qryGridPaiPadrao.Params.ParamByName('DT_COMP1').Value := edInicio.Date;
      qryGridPaiPadrao.Params.ParamByName('DT_COMP2').Value := edFim.Date;

      if Trim(edpesquisa.Text) <> '' then
      begin
        qryGridPaiPadrao.SQL.Add(' AND (UPPER(F.LETRA||CAST(F.NUMERO AS VARCHAR(5))||F.ALEATORIO) LIKE (' + QuotedStr('%' + UpperCase(edpesquisa.Text) + '%') + ') ');
        qryGridPaiPadrao.SQL.Add('  OR UPPER(F.NUM_FOLHA) LIKE (' + QuotedStr('%' + UpperCase(edpesquisa.Text) + '%') + ') ');
        qryGridPaiPadrao.SQL.Add('  OR UPPER(S.NOME_SISTEMA) LIKE (' + QuotedStr('%' + UpperCase(edpesquisa.Text) + '%') + ') ');
        qryGridPaiPadrao.SQL.Add('  OR UPPER(N.DESCR_NATUREZAATO) LIKE (' + QuotedStr('%' + UpperCase(edpesquisa.Text) + '%') + ') ');
        qryGridPaiPadrao.SQL.Add('  OR UPPER(FUNC.NOME_FUNCIONARIO) LIKE (' + QuotedStr('%' + UpperCase(edpesquisa.Text) + '%') + ')) ');
      end;
    end;
    1: //DATA DE UTILIZADO
    begin
      qryGridPaiPadrao.SQL.Add(' AND F.DATA_UTILIZACAO BETWEEN :DT_UT1 AND :DT_UT2 ');
      qryGridPaiPadrao.Params.ParamByName('DT_UT1').Value := edInicio.Date;
      qryGridPaiPadrao.Params.ParamByName('DT_UT2').Value := edFim.Date;

      if Trim(edpesquisa.Text) <> '' then
      begin
        qryGridPaiPadrao.SQL.Add(' AND (UPPER(F.LETRA||CAST(F.NUMERO AS VARCHAR(5))||F.ALEATORIO) LIKE (' + QuotedStr('%' + UpperCase(edpesquisa.Text) + '%') + ') ');
        qryGridPaiPadrao.SQL.Add('  OR UPPER(F.NUM_FOLHA) LIKE (' + QuotedStr('%' + UpperCase(edpesquisa.Text) + '%') + ') ');
        qryGridPaiPadrao.SQL.Add('  OR UPPER(S.NOME_SISTEMA) LIKE (' + QuotedStr('%' + UpperCase(edpesquisa.Text) + '%') + ') ');
        qryGridPaiPadrao.SQL.Add('  OR UPPER(N.DESCR_NATUREZAATO) LIKE (' + QuotedStr('%' + UpperCase(edpesquisa.Text) + '%') + ') ');
        qryGridPaiPadrao.SQL.Add('  OR UPPER(FUNC.NOME_FUNCIONARIO) LIKE (' + QuotedStr('%' + UpperCase(edpesquisa.Text) + '%') + ')) ');
      end;
    end;
    2://SELO
    begin
      if Trim(edpesquisa.Text) <> '' then
        qryGridPaiPadrao.SQL.Add(' AND UPPER(F.LETRA||CAST(F.NUMERO AS VARCHAR(5))||F.ALEATORIO) LIKE (' + QuotedStr('%' + UpperCase(edpesquisa.Text) + '%') + ') ');
    end;
    3://NUMERA��O FOLHA
    begin
      if Trim(edpesquisa.Text) <> '' then
        qryGridPaiPadrao.SQL.Add(' AND UPPER(F.NUM_FOLHA) LIKE (' + '%' + UpperCase(edpesquisa.Text) + '%) ');
    end;
    4://SISTEMA
    begin
      if Trim(edpesquisa.Text) <> '' then
        qryGridPaiPadrao.SQL.Add(' AND UPPER(S.NOME_SISTEMA) LIKE (' + '%' + UpperCase(edpesquisa.Text) + '%) ');
    end;
    5://NATUREZA
    begin
      if Trim(edpesquisa.Text) <> '' then
        qryGridPaiPadrao.SQL.Add(' AND UPPER(N.DESCR_NATUREZAATO) LIKE (' + '%' + UpperCase(edpesquisa.Text) + '%) ');
    end;
    6://FUNCIONARIO
    begin
      if Trim(edpesquisa.Text) <> '' then
        qryGridPaiPadrao.SQL.Add(' AND UPPER(FUNC.NOME_FUNCIONARIO) LIKE (' + '%' + UpperCase(edpesquisa.Text) + '%) ');
    end;
  end;

  case rgStatus.ItemIndex of
    1: qryGridPaiPadrao.SQL.Add(' AND F.FLG_SITUACAO = ' + QuotedStr('U'));
    2: qryGridPaiPadrao.SQL.Add(' AND F.FLG_SITUACAO = ' + QuotedStr('N'));
    3: qryGridPaiPadrao.SQL.Add(' AND F.FLG_SITUACAO = ' + QuotedStr('C'));
  end;

  qryGridPaiPadrao.SQL.Add(' ORDER BY L.ID_LOTEFOLHA, '+
                           '          F.ORDEM');

  qryGridPaiPadrao.Open;
end;

procedure TFFiltroFolhaRcpn.btnEditarClick(Sender: TObject);
begin
  inherited;

  if qryGridPaiPadrao.FieldByName('FLG_SITUACAO').AsString = 'C' then
    vgOperacao := C
  else
    vgOperacao := E;

  PegarPosicaoGrid;

  vgIdConsulta := qryGridPaiPadrao.FieldByName('ID').AsInteger;
  vgOrigemFiltro := True;

  Application.CreateForm(TdmFolhaSeguranca, dmFolhaSeguranca);
  dmGerencial.CriarForm(TFCadastroFolhaRCPN, FCadastroFolhaRCPN);
  FreeAndNil(dmFolhaSeguranca);

  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroFolhaRcpn.btnExcluirClick(Sender: TObject);
var
  qryExclusao: TFDQuery;
begin
  inherited;

  if lPodeExcluir then
  begin
    qryExclusao := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      with qryExclusao, SQL do
      begin
        Close;
        Clear;
        Text := 'UPDATE FOLHASEGURANCA ' +
                '   SET FLG_SITUACAO = ' + QuotedStr('C') + ', ' +
                '       DATA_CANCELAMENTO = :DATA_CANCELAMENTO, ' +
                '       CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO ' +
                ' WHERE ID_FOLHASEGURANCA = :ID_FOLHASEGURANCA';
        Params.ParamByName('DATA_CANCELAMENTO').Value          := Date;
        Params.ParamByName('CANCEL_ID_USUARIO').Value          := vgUsu_Id;
        Params.ParamByName('ID_FOLHASEGURANCA').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      GravarLog;
      Application.MessageBox('Folha de Seguran�a Cancelada com sucesso!', 'Aviso', MB_OK)
    except
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Application.MessageBox('Erro no cancelamento da Folha.', 'Erro', MB_OK + MB_ICONERROR)
    end;

    FreeAndNil(qryExclusao);

    btnFiltrar.Click;
  end;
end;

{procedure TFFiltroFolhaRcpn.btnExportarClick(Sender: TObject);
begin
  inherited;
  try
    dbgxExport.Grid        := dbgGridPaiPadrao;
    dbgxExport.FileName    := vgConf_DiretorioRelatorios +
                              IntToStr(YearOf(Now)) + '\' +
                              'FolhadeSeguranca' +
                              dmGerencial.PegarNumeroTexto(edInicio.Text) + '_a_' +
                              dmGerencial.PegarNumeroTexto(edFim.Text);
    dbgxExport.Orientation := woLandscape;
    dbgxExport.AutoFit     := True;
    dbgxExport.Silent      := False;
    dbgxExport.Visible     := True;
    dbgxExport.Close       := scNever;
    dbgxExport.ExportGrid;

    Application.MessageBox('Lista exportada com sucesso!',
                           'Sucesso',
                           MB_OK);
  except
    dmGerencial.Aviso('N�o foi poss�vel gerar o arquivo. Por favor, tente novamente!');
  end;
end;  }

procedure TFFiltroFolhaRcpn.btnFiltrarClick(Sender: TObject);
begin
  AbrirLista;

  inherited;
end;

procedure TFFiltroFolhaRcpn.btnImprimirClick(Sender: TObject);
begin
  inherited;

  dmPrincipal.AbrirRelatorio(101, True);
end;

procedure TFFiltroFolhaRcpn.btnIncluirClick(Sender: TObject);
begin
  inherited;
  //
end;

procedure TFFiltroFolhaRcpn.btnLimparClick(Sender: TObject);
begin
  inherited;

  edInicio.Date := StartOfTheMonth(Date);
  edFim.Date    := EndOfTheMonth(Date);

  edpesquisa.Text    := '';
  rgStatus.ItemIndex := 0;

  inherited;

  if edInicio.CanFocus then
    edInicio.SetFocus;
end;

procedure TFFiltroFolhaRcpn.cbFiltroChange(Sender: TObject);
begin
  inherited;
  if (cbFiltro.ItemIndex<>0) and (cbFiltro.ItemIndex<>1) then
  begin
    edInicio.Enabled := False;
    edFim.Enabled    := False;
    edInicio.Color   := clSilver;
    edFim.Color      := clSilver;
  end
  else
  begin
    edInicio.Enabled := True;
    edFim.Enabled    := True;
    edInicio.Color   := clWindow;
    edFim.Color      := clWindow;
  end;
end;

procedure TFFiltroFolhaRcpn.cbFiltroKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroFolhaRcpn.qryGridPaiPadraoCalcFields(DataSet: TDataSet);
begin
  inherited;

  case AnsiIndexStr(UpperCase(qryGridPaiPadrao.FieldByName('FLG_SITUACAO').AsString), ['U', 'N', 'C']) of
    0: qryGridPaiPadrao.FieldByName('SITUACAO').AsString := 'Utilizado';
    1: qryGridPaiPadrao.FieldByName('SITUACAO').AsString := 'N�o Utilizado';
    2: qryGridPaiPadrao.FieldByName('SITUACAO').AsString := 'Cancelado';
  end;

  qryGridPaiPadrao.FieldByName('SELO').AsString := qryGridPaiPadraoLETRA.AsString +
                                                   qryGridPaiPadraoNUMERO.AsString + ' ' +
                                                   qryGridPaiPadraoALEATORIO.AsString;
end;

procedure TFFiltroFolhaRcpn.dbgGridPaiPadraoDblClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao     := C;
  vgIdConsulta   := qryGridPaiPadrao.FieldByName('ID').AsInteger;
  vgOrigemFiltro := True;

  Application.CreateForm(TdmFolhaSeguranca, dmFolhaSeguranca);
  dmGerencial.CriarForm(TFCadastroFolhaRCPN, FCadastroFolhaRCPN);
  FreeAndNil(dmFolhaSeguranca);

  DefinirPosicaoGrid;
end;

procedure TFFiltroFolhaRcpn.dbgGridPaiPadraoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;

  dbgGridPaiPadrao := Sender as TDBGrid;

  if dbgGridPaiPadrao.DataSource.DataSet.FieldByName('FLG_SITUACAO').AsString = 'C' then
  begin
    dbgGridPaiPadrao.Canvas.Font.Color := clGray;
    dbgGridPaiPadrao.Canvas.FillRect(Rect);
    dbgGridPaiPadrao.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TFFiltroFolhaRcpn.edFimKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroFolhaRcpn.edInicioKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroFolhaRcpn.edpesquisaKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroFolhaRcpn.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #27 then Close;
  inherited;
end;

procedure TFFiltroFolhaRcpn.FormShow(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;

  btnIncluir.Visible := False;

  ShowScrollBar(dbgGridPaiPadrao.Handle, SB_HORZ, False);

  if edInicio.CanFocus then
    edInicio.SetFocus;
end;

end.
