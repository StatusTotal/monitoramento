{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroRelatorioDiferencasFechamentoCaixa.pas
  Descricao:   Filtro Relatorios de Diferencas do Fechamento de Caixa (R10)
  Author   :   Cristina
  Date:        02-mai-2017
  Last Update:
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroRelatorioDiferencasFechamentoCaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroRelatorioPadrao, Vcl.StdCtrls,
  JvExControls, JvButton, JvTransparentButton, Vcl.ExtCtrls, Vcl.Mask, JvExMask,
  JvToolEdit, FireDAC.Stan.Param;

type
  TFFiltroRelatorioDiferencasFechamentoCaixa = class(TFFiltroRelatorioPadrao)
    lblPeriodo: TLabel;
    lblPeriodoInicio: TLabel;
    dteDataInicio: TJvDateEdit;
    lblPeriodoFim: TLabel;
    dteDataFim: TJvDateEdit;
    procedure btnLimparClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    procedure GravarLog; override;
    procedure GerarRelatorio(ImpDet: Boolean); override;
    function VerificarFiltro: Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroRelatorioDiferencasFechamentoCaixa: TFFiltroRelatorioDiferencasFechamentoCaixa;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UDMRelatorios, UGDM, UVariaveisGlobais;

{ TFFiltroRelatorioDiferencasFechamentoCaixa }

procedure TFFiltroRelatorioDiferencasFechamentoCaixa.btnLimparClick(
  Sender: TObject);
begin
  inherited;

  dteDataInicio.Date := Date;
  dteDataFim.Date    := Date;

  chbExportarPDF.Checked   := True;
  chbExportarExcel.Checked := False;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioDiferencasFechamentoCaixa.btnVisualizarClick(
  Sender: TObject);
begin
  inherited;

  if not VerificarFiltro then
    Exit;

  dmRelatorios.qryRel10_Dif.Close;
  dmRelatorios.qryRel10_Dif.Params.ParamByName('DATA_INI').Value := (FormatDateTime('YYYY-MM-DD', dteDataInicio.Date) + ' 00:00:00');
  dmRelatorios.qryRel10_Dif.Params.ParamByName('DATA_FIM').Value := (FormatDateTime('YYYY-MM-DD', dteDataFim.Date) + ' 23:59:59');
  dmRelatorios.qryRel10_Dif.Open;

  if dmRelatorios.qryRel10_Dif.RecordCount = 0 then
  begin
    Application.MessageBox('N�o h� Diferen�as registradas para esse per�odo.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
  end
  else
    GerarRelatorio(True);
end;

procedure TFFiltroRelatorioDiferencasFechamentoCaixa.FormShow(Sender: TObject);
begin
  inherited;

  dteDataInicio.Date := Date;
  dteDataFim.Date    := Date;

  chbTipoSintetico.Visible := False;
  chbTipoAnalitico.Visible := False;
  chbExibirGrafico.Visible := False;

  chbExportarPDF.Checked   := True;
  chbExportarExcel.Checked := False;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioDiferencasFechamentoCaixa.GerarRelatorio(
  ImpDet: Boolean);
begin
  inherited;

  try
    sTituloRelatorio    := 'DIFEREN�AS DO FECHAMENTO DE CAIXA';
    sSubtituloRelatorio := 'Per�odo de ' + DateToStr(dteDataInicio.Date) + ' a ' + DateToStr(dteDataFim.Date);

    dmRelatorios.DefinirRelatorio(R10);

    dmRelatorios.ImprimirRelatorio(chbExportarPDF.Checked, chbExportarExcel.Checked);

    GravarLog;
  except
    Application.MessageBox('Erro ao gerar o relat�rio de Diferen�as do Fechamento de Caixa.',
                           'Erro',
                           MB_OK + MB_ICONERROR);
  end;
end;

procedure TFFiltroRelatorioDiferencasFechamentoCaixa.GravarLog;
begin
  inherited;

  BS.GravarUsuarioLog(50,
                      '',
                      'Impress�o de Relatorio de Diferen�as do Fechamento de Caixa (' + DateToStr(dDataRelatorio) +
                                                                                    ' ' +
                                                                                    TimeToStr(tHoraRelatorio) + ')',
                      0,
                      '');
end;

function TFFiltroRelatorioDiferencasFechamentoCaixa.VerificarFiltro: Boolean;
var
  Msg: String;
begin
  Result := True;

  if dteDataInicio.Date = 0 then
  begin
    Msg := '- Informe a DATA DE IN�CIO do Per�odo.';
    Result := False;
  end;

  if dteDataFim.Date = 0 then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe a DATA DE FIM do Per�odo.'
    else
      Msg := #13#10 + '- Informe a DATA DE FIM do Per�odo.';

    Result := False;
  end;

  if dteDataFim.Date < dteDataInicio.Date then
  begin
    if Trim(Msg) = '' then
      Msg := '- A DATA DE FIM do Per�odo n�o pode inferior � DATA DE IN�CIO do mesmo.'
    else
      Msg := #13#10 + '- A DATA DE FIM do Per�odo n�o pode ser inferior � DATA DE IN�CIO do mesmo.';

    Result := False;
  end;

  if not Result then
    Application.MessageBox(PChar(':: ATEN��O ::' + #13#10 + #13#10 + Msg),
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
end;

end.
