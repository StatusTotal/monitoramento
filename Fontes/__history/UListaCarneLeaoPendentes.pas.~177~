{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UListaListaCarneLeaoPendentes.pas
  Descricao:   Lista Simples de Lancamentos Pendentes de classificacao do Carne-Leao
  Author   :   Cristina
  Date:        09-dez-2016
  Last Update: 09-nov-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UListaCarneLeaoPendentes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UListaSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.DBCtrls, Datasnap.DBClient, Datasnap.Provider;

type
  TFListaCarneLeaoPendentes = class(TFListaSimplesPadrao)
    btnRelacionarPLC: TJvTransparentButton;
    dsEquivPend_F: TDataSource;
    qryEquivPend_F: TFDQuery;
    dspEquivPend_F: TDataSetProvider;
    cdsEquivPend_F: TClientDataSet;
    cdsEquivPend_FID_CARNELEAO_EQUIVALENCIA: TIntegerField;
    cdsEquivPend_FM_TIPO_LANCAMENTO: TStringField;
    cdsEquivPend_FM_ID_CATEGORIA_DESPREC_FK: TIntegerField;
    cdsEquivPend_FM_ID_SUBCATEGORIA_DESPREC_FK: TIntegerField;
    cdsEquivPend_FM_ID_NATUREZA_FK: TIntegerField;
    cdsEquivPend_FID_CARNELEAO_CLASSIFICACAO_FK: TIntegerField;
    cdsEquivPend_FOBS_CARNELEAO_EQUIVALENCIA: TStringField;
    cdsEquivPend_FDATA_CADASTRO: TSQLTimeStampField;
    cdsEquivPend_FCAD_ID_USUARIO: TIntegerField;
    cdsEquivPend_FFLG_CANCELADO: TStringField;
    cdsEquivPend_FDATA_CANCELAMENTO: TDateField;
    cdsEquivPend_FCANCEL_ID_USUARIO: TIntegerField;
    chbSelecionarTodos: TCheckBox;
    rgCarneLeao: TRadioGroup;
    cdsEquivPend_FDESCR_CATEGORIA_DESPREC: TStringField;
    cdsEquivPend_FDESCR_SUBCATEGORIA_DESPREC: TStringField;
    cdsEquivPend_FDESCR_NATUREZA: TStringField;
    cdsEquivPend_FSELECIONADO: TBooleanField;
    dbgEquivP: TDBGrid;
    dspGridListaPadrao: TDataSetProvider;
    cdsGridListaPadrao: TClientDataSet;
    cdsGridListaPadraoCOD_LANCAMENTO: TIntegerField;
    cdsGridListaPadraoANO_LANCAMENTO: TIntegerField;
    cdsGridListaPadraoTIPO_LANCAMENTO: TStringField;
    cdsGridListaPadraoTIPO_CADASTRO: TStringField;
    cdsGridListaPadraoID_CATEGORIA_DESPREC_FK: TIntegerField;
    cdsGridListaPadraoID_SUBCATEGORIA_DESPREC_FK: TIntegerField;
    cdsGridListaPadraoID_NATUREZA_FK: TIntegerField;
    cdsGridListaPadraoID_CLIENTE_FORNECEDOR_FK: TIntegerField;
    cdsGridListaPadraoFLG_IMPOSTORENDA: TStringField;
    cdsGridListaPadraoFLG_PESSOAL: TStringField;
    cdsGridListaPadraoFLG_AUXILIAR: TStringField;
    cdsGridListaPadraoFLG_REAL: TStringField;
    cdsGridListaPadraoQTD_PARCELAS: TIntegerField;
    cdsGridListaPadraoVR_TOTAL_PREV: TBCDField;
    cdsGridListaPadraoVR_TOTAL_JUROS: TBCDField;
    cdsGridListaPadraoVR_TOTAL_DESCONTO: TBCDField;
    cdsGridListaPadraoVR_TOTAL_PAGO: TBCDField;
    cdsGridListaPadraoCAD_ID_USUARIO: TIntegerField;
    cdsGridListaPadraoDATA_CADASTRO: TSQLTimeStampField;
    cdsGridListaPadraoFLG_STATUS: TStringField;
    cdsGridListaPadraoFLG_CANCELADO: TStringField;
    cdsGridListaPadraoDATA_CANCELAMENTO: TDateField;
    cdsGridListaPadraoCANCEL_ID_USUARIO: TIntegerField;
    cdsGridListaPadraoFLG_RECORRENTE: TStringField;
    cdsGridListaPadraoFLG_REPLICADO: TStringField;
    cdsGridListaPadraoNUM_RECIBO: TIntegerField;
    cdsGridListaPadraoID_ORIGEM: TIntegerField;
    cdsGridListaPadraoID_SISTEMA_FK: TIntegerField;
    cdsGridListaPadraoOBSERVACAO: TStringField;
    cdsGridListaPadraoDATA_LANCAMENTO: TDateField;
    cdsGridListaPadraoFLG_FLUTUANTE: TStringField;
    cdsGridListaPadraoID_CARNELEAO_EQUIVALENCIA_FK: TIntegerField;
    cdsGridListaPadraoDESCR_CATEGORIA_DESPREC: TStringField;
    cdsGridListaPadraoDESCR_SUBCATEGORIA_DESPREC: TStringField;
    cdsGridListaPadraoDESCR_NATUREZA: TStringField;
    cdsGridListaPadraoDESCR_CARNELEAO_CLASSIFICACAO: TStringField;
    procedure btnRelacionarPLCClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cdsEquivPend_FCalcFields(DataSet: TDataSet);
    procedure dbgEquivPCellClick(Column: TColumn);
    procedure dbgEquivPColEnter(Sender: TObject);
    procedure dbgEquivPColExit(Sender: TObject);
    procedure dbgEquivPDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure cdsGridListaPadraoVR_TOTAL_PREVGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsGridListaPadraoVR_TOTAL_JUROSGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsGridListaPadraoVR_TOTAL_DESCONTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsGridListaPadraoVR_TOTAL_PAGOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsGridListaPadraoCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FOriginalOptions: TDBGridOptions;

    IdEquivP: Integer;

    procedure AtualizarPendencias;

    procedure MarcarDesmarcarTodos;
    procedure SalvarBoleano;
  public
    { Public declarations }
    ListaLancamentos: String;

    AnoDeclaracao: Integer;
  end;

var
  FListaCarneLeaoPendentes: TFListaCarneLeaoPendentes;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMCarneLeao;

procedure TFListaCarneLeaoPendentes.AtualizarPendencias;
var
  IdEquiv: Integer;
  qryPlPend: TFDQuery;
begin
  IdEquiv := 900000;

  qryPlPend := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  qryPlPend.Close;
  qryPlPend.SQL.Clear;
  qryPlPend.SQL.Text := 'SELECT TIPO_LANCAMENTO, ' +
                        '       ID_CATEGORIA_DESPREC_FK, ' +
                        '       ID_SUBCATEGORIA_DESPREC_FK, ' +
                        '       ID_NATUREZA_FK ' +
                        '  FROM LANCAMENTO ' +
                        ' WHERE COD_LANCAMENTO IN (' + ListaLancamentos + ') ' +
                        '   AND ANO_LANCAMENTO = :ANO_LANCAMENTO ' +
                        'GROUP BY TIPO_LANCAMENTO, ' +
                        '         ID_CATEGORIA_DESPREC_FK, ' +
                        '         ID_SUBCATEGORIA_DESPREC_FK, ' +
                        '         ID_NATUREZA_FK';
  qryPlPend.Params.ParamByName('ANO_LANCAMENTO').Value := AnoDeclaracao;
  qryPlPend.Open;

  if qryPlPend.RecordCount > 0 then
  begin
    //Plano de Contas Carne-Leao
    cdsEquivPend_F.Close;
    cdsEquivPend_F.Open;

    qryPlPend.First;

    while not qryPlPend.Eof do
    begin
      Inc(IdEquiv);

      cdsEquivPend_F.Append;
      cdsEquivPend_F.FieldByName('ID_CARNELEAO_EQUIVALENCIA').AsInteger     := IdEquiv;
      cdsEquivPend_F.FieldByName('M_TIPO_LANCAMENTO').AsString              := qryPlPend.FieldByName('TIPO_LANCAMENTO').AsString;

      if not qryPlPend.FieldByName('ID_CATEGORIA_DESPREC_FK').IsNull then
        cdsEquivPend_F.FieldByName('M_ID_CATEGORIA_DESPREC_FK').AsInteger := qryPlPend.FieldByName('ID_CATEGORIA_DESPREC_FK').AsInteger;

      if not qryPlPend.FieldByName('ID_SUBCATEGORIA_DESPREC_FK').IsNull then
        cdsEquivPend_F.FieldByName('M_ID_SUBCATEGORIA_DESPREC_FK').AsInteger := qryPlPend.FieldByName('ID_SUBCATEGORIA_DESPREC_FK').AsInteger;

      if not qryPlPend.FieldByName('ID_NATUREZA_FK').IsNull then
        cdsEquivPend_F.FieldByName('M_ID_NATUREZA_FK').AsInteger := qryPlPend.FieldByName('ID_NATUREZA_FK').AsInteger;

      cdsEquivPend_F.FieldByName('DATA_CADASTRO').AsDateTime                := Now;
      cdsEquivPend_F.FieldByName('CAD_ID_USUARIO').AsInteger                := vgUsu_Id;
      cdsEquivPend_F.FieldByName('FLG_CANCELADO').AsString                  := 'N';
      cdsEquivPend_F.Post;

      qryPlPend.Next;
    end;

    cdsEquivPend_F.First;

    //Lancamentos
    qryGridListaPadrao.Close;
    qryGridListaPadrao.SQL.Clear;
    qryGridListaPadrao.SQL.Text := 'SELECT * ' +
                                   '  FROM LANCAMENTO ' +
                                   ' WHERE COD_LANCAMENTO IN (' + ListaLancamentos + ') ' +
                                   '   AND ANO_LANCAMENTO = :ANO_LANCAMENTO ' +
                                   'ORDER BY DATA_LANCAMENTO';
    qryGridListaPadrao.Params.ParamByName('ANO_LANCAMENTO').Value := AnoDeclaracao;

    cdsGridListaPadrao.Close;
    cdsGridListaPadrao.Open;
  end
  else
  begin
    Application.MessageBox('Todas as Pend�ncias foram acertadas.',
                           'Sucesso!',
                           MB_OK);
    Self.Close;
  end;

  FreeAndNil(qryPlPend);
end;

procedure TFListaCarneLeaoPendentes.btnRelacionarPLCClick(Sender: TObject);
var
  iSelect: Integer;
  sLstLancs: String;
begin
  inherited;

  if rgCarneLeao.ItemIndex < 0 then
  begin
    Application.MessageBox('N�o h� classifica��o de Carn�-Le�o selecionada.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
    Exit;
  end;

  cdsEquivPend_F.First;

//  cdsEquivPend_F.DisableControls;

  while not cdsEquivPend_F.Eof do
  begin
    if cdsEquivPend_F.FieldByName('SELECIONADO').AsBoolean then
      Inc(iSelect);

    cdsEquivPend_F.Next;
  end;

//  cdsEquivPend_F.EnableControls;

  if iSelect = 0 then
  begin
    Application.MessageBox('N�o h� registros selecionados.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
    Exit;
  end;

  iSelect   := 0;
  sLstLancs := '';

  if Application.MessageBox('Confirma o Relacionamento de Plano de Contas feito?',
                            'Confirma��o',
                            MB_YESNO) = ID_YES then
  begin
    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      //Plano de Contas
      if IdEquivP = 0 then
        IdEquivP := BS.ProximoId('ID_CARNELEAO_EQUIVALENCIA', 'CARNELEAO_EQUIVALENCIA');

      cdsEquivPend_F.First;

//      cdsEquivPend_F.DisableControls;

      while not cdsEquivPend_F.Eof do
      begin
        if (cdsEquivPend_F.FieldByName('SELECIONADO').AsBoolean) and
          (cdsEquivPend_F.FieldByName('ID_CARNELEAO_CLASSIFICACAO_FK').IsNull) then
        begin
          cdsEquivPend_F.Edit;
          cdsEquivPend_F.FieldByName('ID_CARNELEAO_EQUIVALENCIA').AsInteger := IdEquivP;

          if rgCarneLeao.ItemIndex = 9 then
            cdsEquivPend_F.FieldByName('ID_CARNELEAO_CLASSIFICACAO_FK').AsInteger := (rgCarneLeao.ItemIndex + 2)
          else
            cdsEquivPend_F.FieldByName('ID_CARNELEAO_CLASSIFICACAO_FK').AsInteger := (rgCarneLeao.ItemIndex + 1);

          cdsEquivPend_F.Post;

          Inc(IdEquivP);
        end;

        cdsEquivPend_F.Next;
      end;

//      cdsEquivPend_F.EnableControls;

      //Lancamentos
      cdsEquivPend_F.First;

      while not cdsEquivPend_F.Eof do
      begin
        if cdsEquivPend_F.FieldByName('SELECIONADO').AsBoolean and
          (not cdsEquivPend_F.FieldByName('ID_CARNELEAO_CLASSIFICACAO_FK').IsNull) then
        begin
          cdsGridListaPadrao.First;

          while not cdsGridListaPadrao.Eof do
          begin
            if (cdsGridListaPadrao.FieldByName('ID_CATEGORIA_DESPREC_FK').Value = cdsEquivPend_F.FieldByName('M_ID_CATEGORIA_DESPREC_FK').Value) and
              (cdsGridListaPadrao.FieldByName('ID_SUBCATEGORIA_DESPREC_FK').Value = cdsEquivPend_F.FieldByName('M_ID_SUBCATEGORIA_DESPREC_FK').Value) and
              (cdsGridListaPadrao.FieldByName('ID_NATUREZA_FK').Value = cdsEquivPend_F.FieldByName('M_ID_NATUREZA_FK').Value) and
              cdsGridListaPadrao.FieldByName('ID_CARNELEAO_EQUIVALENCIA_FK').IsNull then
            begin
              cdsGridListaPadrao.Edit;
              cdsGridListaPadrao.FieldByName('ID_CARNELEAO_EQUIVALENCIA_FK').AsInteger := cdsEquivPend_F.FieldByName('ID_CARNELEAO_EQUIVALENCIA').AsInteger;
              cdsGridListaPadrao.Post;
            end;

            cdsGridListaPadrao.Next;
          end;
        end;

        cdsEquivPend_F.Next;
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;
    except
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;
    end;


    //Verificacao de Pendencias restantes
    cdsEquivPend_F.First;

    iSelect := 0;

//    cdsEquivPend_F.DisableControls;

    while not cdsEquivPend_F.Eof do
    begin
      if not cdsEquivPend_F.FieldByName('ID_CARNELEAO_CLASSIFICACAO_FK').IsNull then
        Inc(iSelect);

      cdsEquivPend_F.Next;
    end;

//    cdsEquivPend_F.EnableControls;

    if cdsEquivPend_F.RecordCount = iSelect then
    begin
      try
        if vgConSISTEMA.Connected then
          vgConSISTEMA.StartTransaction;

        cdsEquivPend_F.ApplyUpdates(0);
        cdsGridListaPadrao.ApplyUpdates(0);

        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Commit;

        Self.Close;
      except
        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Rollback;
      end;
    end;
  end;
end;

procedure TFListaCarneLeaoPendentes.cdsEquivPend_FCalcFields(DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with qryAux, SQL do
  begin
    //Categoria
    if cdsEquivPend_F.FieldByName('M_ID_CATEGORIA_DESPREC_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT UPPER(DESCR_CATEGORIA_DESPREC) AS DESCR_CATEGORIA_DESPREC ' +
              '  FROM CATEGORIA_DESPREC ' +
              ' WHERE ID_CATEGORIA_DESPREC = :ID_CATEGORIA_DESPREC';
      Params.ParamByName('ID_CATEGORIA_DESPREC').AsInteger := cdsEquivPend_F.FieldByName('M_ID_CATEGORIA_DESPREC_FK').AsInteger;
      Open;

      cdsEquivPend_F.FieldByName('DESCR_CATEGORIA_DESPREC').AsString := qryAux.FieldByName('DESCR_CATEGORIA_DESPREC').AsString;
    end;

    //Subcategoria
    if cdsEquivPend_F.FieldByName('M_ID_SUBCATEGORIA_DESPREC_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT UPPER(DESCR_SUBCATEGORIA_DESPREC) AS DESCR_SUBCATEGORIA_DESPREC ' +
              '  FROM SUBCATEGORIA_DESPREC ' +
              ' WHERE ID_SUBCATEGORIA_DESPREC = :ID_SUBCATEGORIA_DESPREC';
      Params.ParamByName('ID_SUBCATEGORIA_DESPREC').AsInteger := cdsEquivPend_F.FieldByName('M_ID_SUBCATEGORIA_DESPREC_FK').AsInteger;
      Open;

      cdsEquivPend_F.FieldByName('DESCR_SUBCATEGORIA_DESPREC').AsString := qryAux.FieldByName('DESCR_SUBCATEGORIA_DESPREC').AsString;
    end;

    //Natureza
    if cdsEquivPend_F.FieldByName('M_ID_NATUREZA_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT UPPER(DESCR_NATUREZA) AS DESCR_NATUREZA ' +
              '  FROM NATUREZA ' +
              ' WHERE ID_NATUREZA = :ID_NATUREZA';
      Params.ParamByName('ID_NATUREZA').AsInteger := cdsEquivPend_F.FieldByName('M_ID_NATUREZA_FK').AsInteger;
      Open;

      cdsEquivPend_F.FieldByName('DESCR_NATUREZA').AsString := qryAux.FieldByName('DESCR_NATUREZA').AsString;
    end;
  end;

  FreeAndNil(qryAux);
end;

procedure TFListaCarneLeaoPendentes.cdsGridListaPadraoCalcFields(
  DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with qryAux, SQL do
  begin
    //Categoria
    if cdsGridListaPadrao.FieldByName('ID_CATEGORIA_DESPREC_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT UPPER(DESCR_CATEGORIA_DESPREC) AS DESCR_CATEGORIA_DESPREC ' +
              '  FROM CATEGORIA_DESPREC ' +
              ' WHERE ID_CATEGORIA_DESPREC = :ID_CATEGORIA_DESPREC';
      Params.ParamByName('ID_CATEGORIA_DESPREC').AsInteger := cdsGridListaPadrao.FieldByName('ID_CATEGORIA_DESPREC_FK').AsInteger;
      Open;

      cdsGridListaPadrao.FieldByName('DESCR_CATEGORIA_DESPREC').AsString := qryAux.FieldByName('DESCR_CATEGORIA_DESPREC').AsString;
    end;

    //Subcategoria
    if cdsGridListaPadrao.FieldByName('ID_SUBCATEGORIA_DESPREC_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT UPPER(DESCR_SUBCATEGORIA_DESPREC) AS DESCR_SUBCATEGORIA_DESPREC ' +
              '  FROM SUBCATEGORIA_DESPREC ' +
              ' WHERE ID_SUBCATEGORIA_DESPREC = :ID_SUBCATEGORIA_DESPREC';
      Params.ParamByName('ID_SUBCATEGORIA_DESPREC').AsInteger := cdsGridListaPadrao.FieldByName('ID_SUBCATEGORIA_DESPREC_FK').AsInteger;
      Open;

      cdsGridListaPadrao.FieldByName('DESCR_SUBCATEGORIA_DESPREC').AsString := qryAux.FieldByName('DESCR_SUBCATEGORIA_DESPREC').AsString;
    end;

    //Natureza
    if cdsGridListaPadrao.FieldByName('ID_NATUREZA_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT UPPER(DESCR_NATUREZA) AS DESCR_NATUREZA ' +
              '  FROM NATUREZA ' +
              ' WHERE ID_NATUREZA = :ID_NATUREZA';
      Params.ParamByName('ID_NATUREZA').AsInteger := cdsGridListaPadrao.FieldByName('ID_NATUREZA_FK').AsInteger;
      Open;

      cdsGridListaPadrao.FieldByName('DESCR_NATUREZA').AsString := qryAux.FieldByName('DESCR_NATUREZA').AsString;
    end;

    //Carne-Leao
    if cdsGridListaPadrao.FieldByName('ID_CARNELEAO_EQUIVALENCIA_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT UPPER(CLC.DESCR_CARNELEAO_CLASSIFICACAO) AS DESCR_CARNELEAO_CLASSIFICACAO ' +
              '  FROM CARNELEAO_CLASSIFICACAO CLC ' +
              ' INNER JOIN CARNELEAO_EQUIVALENCIA CLE ' +
              '    ON CLC.ID_CARNELEAO_CLASSIFICACAO = CLE.ID_CARNELEAO_CLASSIFICACAO_FK ' +
              ' WHERE CLE.ID_CARNELEAO_EQUIVALENCIA = :ID_CARNELEAO_EQUIVALENCIA';
      Params.ParamByName('ID_CARNELEAO_EQUIVALENCIA').AsInteger := cdsGridListaPadrao.FieldByName('ID_CARNELEAO_EQUIVALENCIA_FK').AsInteger;
      Open;

      cdsGridListaPadrao.FieldByName('DESCR_CARNELEAO_CLASSIFICACAO').AsString := qryAux.FieldByName('DESCR_CARNELEAO_CLASSIFICACAO').AsString;
    end;
  end;

  FreeAndNil(qryAux);
end;

procedure TFListaCarneLeaoPendentes.cdsGridListaPadraoVR_TOTAL_DESCONTOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFListaCarneLeaoPendentes.cdsGridListaPadraoVR_TOTAL_JUROSGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFListaCarneLeaoPendentes.cdsGridListaPadraoVR_TOTAL_PAGOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFListaCarneLeaoPendentes.cdsGridListaPadraoVR_TOTAL_PREVGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFListaCarneLeaoPendentes.dbgEquivPCellClick(Column: TColumn);
begin
  inherited;

  if (cdsEquivPend_F.FieldByName('ID_CARNELEAO_CLASSIFICACAO_FK').IsNull) and
    (dbgEquivP.SelectedField.DataType = ftBoolean) then
    SalvarBoleano;
end;

procedure TFListaCarneLeaoPendentes.dbgEquivPColEnter(Sender: TObject);
begin
  inherited;

  if dbgEquivP.SelectedField.DataType = ftBoolean then
  begin
    FOriginalOptions     := dbgEquivP.Options;
    dbgEquivP.Options := dbgEquivP.Options - [dgEditing];
  end;
end;

procedure TFListaCarneLeaoPendentes.dbgEquivPColExit(Sender: TObject);
begin
  inherited;

  if dbgEquivP.SelectedField.DataType = ftBoolean then
    dbgEquivP.Options := FOriginalOptions;
end;

procedure TFListaCarneLeaoPendentes.dbgEquivPDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
const
  CtrlState: array[Boolean] of Integer = (DFCS_BUTTONCHECK,
                                          DFCS_BUTTONCHECK or DFCS_CHECKED);
var
  CheckBoxRectangle: TRect;
begin
  inherited;

  AlternarCorGrid(Sender, Rect, DataCol, Column, State);

  if not cdsEquivPend_F.FieldByName('ID_CARNELEAO_CLASSIFICACAO_FK').IsNull then
  begin
    dbgEquivP.Canvas.Font.Color := clGray;
    dbgEquivP.Canvas.FillRect(Rect);
    dbgEquivP.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;

  if (Column.Field.FieldName = 'SELECIONADO') then
  begin
    dbgEquivP.Canvas.FillRect(Rect);

    CheckBoxRectangle.Left   := (Rect.Left + 2);
    CheckBoxRectangle.Right  := (Rect.Right - 2);
    CheckBoxRectangle.Top    := (Rect.Top + 2);
    CheckBoxRectangle.Bottom := (Rect.Bottom - 2);

    DrawFrameControl(dbgEquivP.Canvas.Handle,
                     CheckBoxRectangle,
                     DFC_BUTTON,
                     CtrlState[Column.Field.AsBoolean]);
  end;
end;

procedure TFListaCarneLeaoPendentes.FormShow(Sender: TObject);
begin
  inherited;

  IdEquivP  := 0;

  btnFiltrar.Visible := False;
  btnLimpar.Visible  := False;

  AtualizarPendencias;

  ShowScrollBar(dbgEquivP.Handle, SB_HORZ, False);
  ShowScrollBar(dbgGridListaPadrao.Handle, SB_HORZ, False);
end;

procedure TFListaCarneLeaoPendentes.MarcarDesmarcarTodos;
begin
  cdsEquivPend_F.First;

  while not cdsEquivPend_F.Eof do
  begin
    cdsEquivPend_F.Edit;

    if chbSelecionarTodos.Checked then
      cdsEquivPend_F.FieldByName('SELECIONADO').AsBoolean := True
    else
      cdsEquivPend_F.FieldByName('SELECIONADO').AsBoolean := False;

    cdsEquivPend_F.Post;

    cdsEquivPend_F.Next;
  end;

  if cdsEquivPend_F.FieldByName('SELECIONADO').AsBoolean then
    chbSelecionarTodos.Caption := 'Desmarcar Todos'
  else
    chbSelecionarTodos.Caption := 'Desmarcar Todos';
end;

procedure TFListaCarneLeaoPendentes.SalvarBoleano;
begin
  dbgEquivP.SelectedField.Dataset.Edit;
  dbgEquivP.SelectedField.AsBoolean := not dbgEquivP.SelectedField.AsBoolean;
  dbgEquivP.SelectedField.Dataset.Post;
end;

end.
