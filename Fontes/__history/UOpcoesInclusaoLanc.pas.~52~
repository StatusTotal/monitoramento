{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UOpcoesInclusaoLanc.pas
  Descricao:   Tela de Opcoes de Inclusao de Lancamento
  Author   :   Cristina
  Date:        25-abr-2016
  Last Update: 26-nov-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UOpcoesInclusaoLanc;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UOpcoesAcaoPadrao, JvExControls,
  JvButton, JvTransparentButton, Vcl.ExtCtrls,
  Vcl.PlatformDefaultStyleActnCtrls, System.Actions, Vcl.ActnList, Vcl.ActnMan,
  Vcl.Menus, JvMenus;

type
  TFOpcoesInclusaoLanc = class(TFOpcoesAcaoPadrao)
    acmBotoes: TActionManager;
    actIncluirReceita: TAction;
    actIncluirDespesa: TAction;
    actReceitasMenu: TAction;
    actDespesasMenu: TAction;
    ppmReceita: TJvPopupMenu;
    ppmDespesa: TJvPopupMenu;
    actIncluirRepasseMenu: TAction;
    actIncluirRepasseVincDesp: TAction;
    actIncluirRepasseGerarNDesp: TAction;
    actIncluirDeposito: TAction;
    LanarReceita1: TMenuItem;
    Depsito1: TMenuItem;
    LanarDespesa1: TMenuItem;
    RepassedeReceita1: TMenuItem;
    VincularDespesa1: TMenuItem;
    GerarNovaDespesa1: TMenuItem;
    procedure actIncluirReceitaExecute(Sender: TObject);
    procedure actIncluirDespesaExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure actIncluirRepasseMenuExecute(Sender: TObject);
    procedure actDespesasMenuExecute(Sender: TObject);
    procedure actReceitasMenuExecute(Sender: TObject);
    procedure actIncluirRepasseGerarNDespExecute(Sender: TObject);
    procedure actIncluirRepasseVincDespExecute(Sender: TObject);
    procedure actIncluirDepositoExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FOpcoesInclusaoLanc: TFOpcoesInclusaoLanc;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMLancamento,
  UCadastroLancamento, UFiltroDepositosFlutuantes, UDMBaixa, UBaixaLancamento,
  UListaBaixaFlutuantes, URepassesVincularDespesa;

procedure TFOpcoesInclusaoLanc.actDespesasMenuExecute(Sender: TObject);
begin
  inherited;

  //pop-up
end;

procedure TFOpcoesInclusaoLanc.actIncluirDepositoExecute(Sender: TObject);
var
  lEncerrar: Boolean;
begin
  inherited;

  repeat
    vgOperacao     := I;
    vgOrigemFiltro := True;
    vgIdConsulta   := 0;

    vgLanc_CriarNovo := False;
    lEncerrar        := False;

    dmGerencial.CriarForm(TFFiltroDepositosFlutuantes, FFiltroDepositosFlutuantes);

    if vgLanc_CriarNovo then
    begin
      if Application.MessageBox('Deseja cadastrar um novo Dep�sito?',
                                'Novo Dep�sito',
                                 MB_YESNO + MB_ICONQUESTION) = ID_NO then
      lEncerrar := True;
    end
    else
      lEncerrar := True;
  until lEncerrar;

  if lEncerrar then
    Self.Close;
end;

procedure TFOpcoesInclusaoLanc.actIncluirDespesaExecute(Sender: TObject);
var
  lEncerrar: Boolean;
begin
  inherited;

  repeat
    vgOperacao     := I;
    vgOrigemFiltro := True;
    vgIdConsulta   := 0;

    vgLanc_CriarNovo := False;
    lEncerrar        := False;

    try
      Application.CreateForm(TdmLancamento, dmLancamento);
      Application.CreateForm(TFCadastroLancamento, FCadastroLancamento);

      FCadastroLancamento.TipoLanc := DESP;
      FCadastroLancamento.lSomentePagamento := False;

      FCadastroLancamento.ShowModal;
    finally
      FreeAndNil(dmLancamento);
      FCadastroLancamento.Free;
    end;

    if vgLanc_CriarNovo then
    begin
      if Application.MessageBox('Deseja criar um novo Lan�amento de Despesa?',
                                'Novo Lan�amento',
                                 MB_YESNO + MB_ICONQUESTION) = ID_NO then
      lEncerrar := True;
    end
    else
      lEncerrar := True;
  until lEncerrar;

  if lEncerrar then
    Self.Close;
end;

procedure TFOpcoesInclusaoLanc.actIncluirReceitaExecute(Sender: TObject);
var
  lEncerrar: Boolean;
begin
  inherited;

  repeat
    vgOperacao     := I;
    vgOrigemFiltro := True;
    vgIdConsulta   := 0;

    vgLanc_CriarNovo := False;
    lEncerrar        := False;

    try
      Application.CreateForm(TdmLancamento, dmLancamento);
      Application.CreateForm(TFCadastroLancamento, FCadastroLancamento);

      FCadastroLancamento.TipoLanc := REC;
      FCadastroLancamento.lSomentePagamento := False;

      FCadastroLancamento.ShowModal;
    finally
      FreeAndNil(dmLancamento);
      FCadastroLancamento.Free;
    end;

    if vgLanc_CriarNovo then
    begin
      if Application.MessageBox('Deseja criar um novo Lan�amento de Receita?',
                                'Novo Lan�amento',
                                 MB_YESNO + MB_ICONQUESTION) = ID_NO then
      lEncerrar := True;
    end
    else
      lEncerrar := True;
  until lEncerrar;

  if lEncerrar then
    Self.Close;
end;

procedure TFOpcoesInclusaoLanc.actIncluirRepasseGerarNDespExecute(
  Sender: TObject);
begin
  inherited;

  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmBaixa, dmBaixa);
  dmGerencial.CriarForm(TFListaBaixaFlutuantes, FListaBaixaFlutuantes);
  FreeAndNil(dmBaixa);

  Self.Close;
end;

procedure TFOpcoesInclusaoLanc.actIncluirRepasseMenuExecute(Sender: TObject);
begin
  inherited;

  //pop-up
end;

procedure TFOpcoesInclusaoLanc.actIncluirRepasseVincDespExecute(
  Sender: TObject);
begin
  inherited;

  vgOperacao := VAZIO;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmBaixa, dmBaixa);

//  dmGerencial.CriarForm(TFBaixaLancamento, FBaixaLancamento);
  dmGerencial.CriarForm(TFRepassesVincularDespesa, FRepassesVincularDespesa);

  FreeAndNil(dmBaixa);

  Self.Close;
end;

procedure TFOpcoesInclusaoLanc.actReceitasMenuExecute(Sender: TObject);
begin
  inherited;

  //pop-up
end;

procedure TFOpcoesInclusaoLanc.FormShow(Sender: TObject);
begin
  inherited;

  //RECEITA
  actIncluirReceita.Enabled  := vgPrm_IncRec;
  actIncluirDeposito.Enabled := vgPrm_ConsDepo;

  //DESPESA
  actIncluirDespesa.Enabled           := vgPrm_IncDesp;
  actIncluirRepasseMenu.Enabled       := vgPrm_BxLanc;
  actIncluirRepasseVincDesp.Enabled   := vgPrm_BxLanc;
  actIncluirRepasseGerarNDesp.Enabled := vgPrm_BxLanc;
end;

end.
