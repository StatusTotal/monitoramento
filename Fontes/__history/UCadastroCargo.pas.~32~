{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroCargo.pas
  Descricao:   Formulario de cadastro de Cargo de Colaborador
  Author   :   Cristina
  Date:        28-ago-2017
  Last Update:
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroCargo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Mask, Vcl.DBCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Datasnap.DBClient,
  Datasnap.Provider, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFCadastroCargo = class(TFCadastroGeralPadrao)
    edtDescricaoCargo: TDBEdit;
    lblDescricaoCargo: TLabel;
    qryCargo: TFDQuery;
    dspCargo: TDataSetProvider;
    cdsCargo: TClientDataSet;
    cdsCargoID_CARGO: TIntegerField;
    cdsCargoDESCR_CARGO: TStringField;
    cdsCargoCOD_CARGO: TIntegerField;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure edtDescricaoCargoKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }

    IdNovoCargo: Integer;
  end;

var
  FCadastroCargo: TFCadastroCargo;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{ TFCadastroCargo }

procedure TFCadastroCargo.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroCargo.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroCargo.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtDescricaoCargo.MaxLength := 250;
end;

procedure TFCadastroCargo.DesabilitarComponentes;
begin
  inherited;

  if vgOperacao = C then
    pnlDados.Enabled := False;
end;

procedure TFCadastroCargo.edtDescricaoCargoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroCargo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  cdsCargo.Cancel;

  vgOperacao     := VAZIO;
  vgIdConsulta   := 0;
  vgOrigemFiltro := False;

  inherited;
end;

procedure TFCadastroCargo.FormCreate(Sender: TObject);
begin
  inherited;

  IdNovoCargo := 0;

  cdsCargo.Close;
  cdsCargo.Params.ParamByName('ID_CARGO').AsInteger := vgIdConsulta;
  cdsCargo.Open;

  if vgOperacao = I then
    cdsCargo.Append;

  if vgOperacao = E then
    cdsCargo.Edit;
end;

procedure TFCadastroCargo.FormShow(Sender: TObject);
begin
  inherited;

  if edtDescricaoCargo.CanFocus then
    edtDescricaoCargo.SetFocus;
end;

function TFCadastroCargo.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroCargo.GravarDadosGenerico(var Msg: String): Boolean;
var
  qryCargoS: TFDQuery;
begin
  Result := True;
  Msg := '';

  if vgOperacao = C then
    Exit;

  try
    if vgConGER.Connected then
      vgConGER.StartTransaction;

    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    qryCargoS := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    if cdsCargo.State in [dsInsert, dsEdit] then
    begin
      { Grava as especificacoes da Natureza para serem usadas a partir
        dos novos Lancamentos }
      if vgOperacao = I then
        cdsCargo.FieldByName('ID_CARGO').AsInteger := BS.ProximoId('ID_CARGO', 'CARGO');

      cdsCargo.FieldByName('COD_CARGO').AsInteger := cdsCargo.FieldByName('ID_CARGO').AsInteger;

      if vgOrigemCadastro then
        IdNovoCargo := cdsCargo.FieldByName('ID_CARGO').AsInteger;

      cdsCargo.Post;
      cdsCargo.ApplyUpdates(0);
    end;

    qryCargoS.Close;
    qryCargoS.SQL.Clear;

    if vgOperacao = I then
    begin
      qryCargoS.SQL.Text := 'INSERT INTO CARGO (ID_CARGO, DESCR_CARGO, COD_CARGO) ' +
                            '           VALUES (:ID_CARGO, :DESCR_CARGO, :COD_CARGO)';
      qryCargoS.Params.ParamByName('ID_CARGO').Value    := cdsCargo.FieldByName('ID_CARGO').AsInteger;
      qryCargoS.Params.ParamByName('DESCR_CARGO').Value := cdsCargo.FieldByName('DESCR_CARGO').AsString;
      qryCargoS.Params.ParamByName('COD_CARGO').Value   := cdsCargo.FieldByName('COD_CARGO').AsInteger;
    end
    else if vgOperacao = E then
    begin
      qryCargoS.SQL.Text := 'UPDATE CARGO ' +
                            '   SET DESCR_CARGO = :DESCR_CARGO ' +
                            ' WHERE ID_CARGO = :ID_CARGO';
      qryCargoS.Params.ParamByName('DESCR_CARGO').Value := cdsCargo.FieldByName('ID_CARGO').AsString;
      qryCargoS.Params.ParamByName('ID_CARGO').Value    := cdsCargo.FieldByName('ID_CARGO').AsInteger;
    end;

    qryCargoS.ExecSQL;

    FreeAndNil(qryCargoS);

    if vgConGER.InTransaction then
      vgConGER.Commit;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    Msg := 'Cargo gravado com sucesso!';
  except
    if vgConGER.InTransaction then
      vgConGER.Rollback;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na grava��o do Cargo.';
  end;
end;

procedure TFCadastroCargo.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta de Cargo',
                          vgIdConsulta, 'CARGO');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclus�o de Cargo',
                          vgIdConsulta, 'CARGO');
    end;
    E:  //EDICAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO',
                                'Por favor, indique o motivo da edi��o desse Cargo:',
                                '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          vgIdConsulta, 'CARGO');
    end;
  end;
end;

procedure TFCadastroCargo.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroCargo.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

function TFCadastroCargo.VerificarDadosGenerico(var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if Trim(edtDescricaoCargo.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar a DESCRI��O DO CARGO.';
    DadosMsgErro.Componente := edtDescricaoCargo;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroCargo.VerificarLayoutAto(var QtdErros: Integer): Boolean;
begin
  //
end;

end.
