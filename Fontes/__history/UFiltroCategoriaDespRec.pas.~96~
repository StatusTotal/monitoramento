{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltrooCategoriaDespRec.pas
  Descricao:   Filtro de Categorias de Despesas ou Receitas
  Author   :   Cristina
  Date:        03-mar-2016
  Last Update: 08-nov-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroCategoriaDespRec;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.StdCtrls, frxClass, frxDBSet;

type
  TFFiltroCategoriaDespRec = class(TFFiltroSimplesPadrao)
    edtCategoria: TEdit;
    lblCategoria: TLabel;
    qryGridPaiPadraoID: TIntegerField;
    qryGridPaiPadraoTIPO: TStringField;
    qryGridPaiPadraoDESCR_CATEGORIA_DESPREC: TStringField;
    cbNatureza: TComboBox;
    lblNatureza: TLabel;
    qryNatureza: TFDQuery;
    qryGridPaiPadraoID_NATUREZA_FK: TIntegerField;
    qryGridPaiPadraoDESCR_NATUREZA: TStringField;
    procedure edtCategoriaKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure cbNaturezaKeyPress(Sender: TObject; var Key: Char);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroCategoriaDespRec: TFFiltroCategoriaDespRec;

  TipoCategoria: String;
  IdCategoria: Integer;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais,
  UCadastroCategoriaDespRec;

procedure TFFiltroCategoriaDespRec.btnEditarClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao     := E;
  vgIdConsulta   := qryGridPaiPadrao.FieldByName('ID').AsInteger;
  vgOrigemFiltro := True;

  try
    Application.CreateForm(TFCadastroCategoriaDespRec, FCadastroCategoriaDespRec);

    TipoCat := TipoCategoria;

    FCadastroCategoriaDespRec.ShowModal;
  finally
    FCadastroCategoriaDespRec.Free;
  end;

  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroCategoriaDespRec.btnExcluirClick(Sender: TObject);
var
  qryExclusao: TFDQuery;
begin
  inherited;

  if lPodeExcluir then
  begin
    qryExclusao := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      with qryExclusao, SQL do
      begin
        Close;
        Clear;
        Text := 'DELETE FROM CATEGORIA_DESPREC WHERE ID_CATEGORIA_DESPREC = :iIdCatDespRec';
        Params.ParamByName('iIdCatDespRec').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      GravarLog;
      Application.MessageBox('Categoria exclu�do com sucesso!', 'Aviso', MB_OK)
    except
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Application.MessageBox('Erro na exclus�o da Categoria.', 'Erro', MB_OK + MB_ICONERROR)
    end;

    FreeAndNil(qryExclusao);

    btnFiltrar.Click;
  end;
end;

procedure TFFiltroCategoriaDespRec.btnFiltrarClick(Sender: TObject);
begin
  with qryGridPaiPadrao, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT C.ID_CATEGORIA_DESPREC AS ID, ' +
            '       C.TIPO, ' +
            '       C.DESCR_CATEGORIA_DESPREC, ' +
            '       C.ID_NATUREZA_FK, ' +
            '       N.DESCR_NATUREZA ' +
            '  FROM CATEGORIA_DESPREC C ' +
            '  LEFT JOIN NATUREZA N ' +
            '    ON C.ID_NATUREZA_FK = N.ID_NATUREZA ' +
            ' WHERE C.TIPO = ' + QuotedStr(TipoCategoria);

    if Trim(cbNatureza.Text) <> '' then
    begin
      qryNatureza.Close;
      qryNatureza.Params.ParamByName('DESCR_NATUREZA').Value := Trim(cbNatureza.Text);
      qryNatureza.Params.ParamByName('TIPO_NATUREZA').Value  := Trim(TipoCategoria);
      qryNatureza.Open;

      Add(' AND C.ID_NATUREZA_FK = ' + qryNatureza.FieldByName('ID_NATUREZA').AsString);
    end;

    if Trim(edtCategoria.Text) <> '' then
    begin
      Add(' AND CAST(UPPER(C.DESCR_CATEGORIA_DESPREC) AS VARCHAR(250)) LIKE CAST(:CATEGORIA AS VARCHAR(250))');
      Params.ParamByName('CATEGORIA').Value := '%' + Trim(UpperCase(edtCategoria.Text)) + '%';
    end;

    Add('ORDER BY C.DESCR_CATEGORIA_DESPREC');
    Open;
  end;

  inherited;
end;

procedure TFFiltroCategoriaDespRec.btnIncluirClick(Sender: TObject);
begin
  inherited;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := True;

  try
    Application.CreateForm(TFCadastroCategoriaDespRec, FCadastroCategoriaDespRec);

    TipoCat := TipoCategoria;

    FCadastroCategoriaDespRec.ShowModal;
  finally
    FCadastroCategoriaDespRec.Free;
  end;

  btnFiltrar.Click;
end;

procedure TFFiltroCategoriaDespRec.btnLimparClick(Sender: TObject);
begin
  cbNatureza.ItemIndex := -1;
  edtCategoria.Clear;

  inherited;

  if edtCategoria.CanFocus then
    edtCategoria.SetFocus;
end;

procedure TFFiltroCategoriaDespRec.cbNaturezaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroCategoriaDespRec.dbgGridPaiPadraoDblClick(Sender: TObject);
var
  sTexto: String;
begin
  inherited;

  if vgOrigemCadastro then
  begin
    if qryGridPaiPadrao.RecordCount = 0 then
      IdCategoria := 0
    else
    begin
      IdCategoria := qryGridPaiPadrao.FieldByName('ID').AsInteger;

      sTexto := 'Confirma a sele��o de ' + qryGridPaiPadrao.FieldByName('DESCR_CATEGORIA_DESPREC').AsString + '?';

      if Application.MessageBox(PChar(sTexto), 'Confirma��o', MB_YESNO + MB_ICONQUESTION) = ID_YES then
        Self.Close
      else
        IdCategoria := 0;
    end;
  end
  else
  begin
    PegarPosicaoGrid;

    vgOperacao     := C;
    vgIdConsulta   := qryGridPaiPadrao.FieldByName('ID').AsInteger;
    vgOrigemFiltro := True;

    try
      Application.CreateForm(TFCadastroCategoriaDespRec, FCadastroCategoriaDespRec);

      TipoCat := TipoCategoria;

      FCadastroCategoriaDespRec.ShowModal;
    finally
      FCadastroCategoriaDespRec.Free;
    end;

    DefinirPosicaoGrid;
  end;
end;

procedure TFFiltroCategoriaDespRec.edtCategoriaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroCategoriaDespRec.FormCreate(Sender: TObject);
begin
  inherited;

  IdCategoria := 0;
end;

procedure TFFiltroCategoriaDespRec.FormShow(Sender: TObject);
begin
  btnImprimir.Visible := False;

  BS.MontarComboBox(cbNatureza,
                    'DESCR_NATUREZA',
                    'NATUREZA',
                    '',
                    '',
                    'ID_NATUREZA');

  with qryGridPaiPadrao, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT C.ID_CATEGORIA_DESPREC AS ID, ' +
            '       C.TIPO, ' +
            '       C.DESCR_CATEGORIA_DESPREC, ' +
            '       C.ID_NATUREZA_FK, ' +
            '       N.DESCR_NATUREZA ' +
            '  FROM CATEGORIA_DESPREC C ' +
            '  LEFT JOIN NATUREZA N ' +
            '    ON C.ID_NATUREZA_FK = N.ID_NATUREZA ' +
            ' WHERE C.TIPO = ' + QuotedStr(TipoCategoria) +
            ' ORDER BY C.DESCR_CATEGORIA_DESPREC';
    Open;
  end;

  ShowScrollBar(dbgGridPaiPadrao.Handle, SB_HORZ, False);

  if TipoCategoria = 'D' then
    lblCategoria.Caption := 'Categoria de Despesa'
  else if TipoCategoria = 'R' then
    lblCategoria.Caption := 'Categoria de Receita';

  inherited;

  if edtCategoria.CanFocus then
    edtCategoria.SetFocus;
end;

procedure TFFiltroCategoriaDespRec.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  if vgOperacao = X then  //EXCLUSAO
  begin
    repeat
      sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da exclus�o dessa Categoria:', '');
    until sObservacao <> '';

    BS.GravarUsuarioLog(2, '', sObservacao,
                        qryGridPaiPadrao.FieldByName('ID').AsInteger, 'CATEGORIA_DESPREC');
  end;
end;

function TFFiltroCategoriaDespRec.PodeExcluir(var Msg: String): Boolean;
var
  QryVerif: TFDQuery;
begin
  inherited;

  Result := True;

  QryVerif := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryVerif, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT * FROM LANCAMENTO WHERE ID_CATEGORIA_DESPREC_FK = :ID_CATEGORIA_DESPREC';
    Params.ParamByName('ID_CATEGORIA_DESPREC').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
    Open;
  end;

  if QryVerif.RecordCount > 0 then
  begin
    with QryVerif, SQL do
    begin
      Close;
      Clear;
      Text := 'SELECT * FROM SUBCATEGORIA_DESPREC WHERE ID_CATEGORIA_DESPREC_FK = :ID_CATEGORIA_DESPREC';
      Params.ParamByName('ID_CATEGORIA_DESPREC').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
      Open;

      if QryVerif.RecordCount > 0 then
        msg := 'N�o � poss�vel excluir a Categoria informada, pois a mesma j� possui registro no Sistema.';
    end;
  end;

  Result := (QryVerif.RecordCount = 0) and (qryGridPaiPadrao.RecordCount > 0);

  FreeAndNil(QryVerif);
end;

procedure TFFiltroCategoriaDespRec.VerificarPermissoes;
begin
  inherited;

  if TipoCategoria = 'R' then
  begin
    btnIncluir.Enabled  := vgPrm_IncCatRec;
    btnEditar.Enabled   := vgPrm_EdCatRec and (qryGridPaiPadrao.RecordCount > 0);
    btnExcluir.Enabled  := vgPrm_ExcCatRec and (qryGridPaiPadrao.RecordCount > 0);
  end;

  if TipoCategoria = 'D' then
  begin
    btnIncluir.Enabled  := vgPrm_IncCatDesp;
    btnEditar.Enabled   := vgPrm_EdCatDesp and (qryGridPaiPadrao.RecordCount > 0);
    btnExcluir.Enabled  := vgPrm_ExcCatDesp and (qryGridPaiPadrao.RecordCount > 0);
  end;

  btnImprimir.Enabled := False;
end;

end.
