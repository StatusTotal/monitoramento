{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroOficioFormaEnvio.pas
  Descricao:   Formulario de cadastro de Forma de Envio do Oficio
  Author   :   Cristina
  Date:        23-jan-2017
  Last Update:
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroOficioFormaEnvio;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Mask, Vcl.DBCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Datasnap.DBClient,
  Datasnap.Provider, FireDAC.Comp.DataSet, FireDAC.Comp.Client, UDM, System.StrUtils;

type
  TFCadastroOficioFormaEnvio = class(TFCadastroGeralPadrao)
    lblFormaEnvio: TLabel;
    lblNome: TLabel;
    edtNome: TDBEdit;
    qryOfcFEnv: TFDQuery;
    dspOfcFEnv: TDataSetProvider;
    cdsOfcFEnv: TClientDataSet;
    cdsOfcFEnvID_OFICIO_FORMAENVIO: TIntegerField;
    cdsOfcFEnvDESCR_OFICIO_FORMAENVIO: TStringField;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure edtNomeKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }

    IdNovaFEnvio: Integer;
  end;

var
  FCadastroOficioFormaEnvio: TFCadastroOficioFormaEnvio;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UGDM, UVariaveisGlobais;

{ TFCadastroOficioFormaEnvio }

procedure TFCadastroOficioFormaEnvio.btnCancelarClick(Sender: TObject);
begin
  inherited;

  Self.Close;
end;

procedure TFCadastroOficioFormaEnvio.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroOficioFormaEnvio.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtNome.MaxLength := 150;
end;

procedure TFCadastroOficioFormaEnvio.DesabilitarComponentes;
begin
  inherited;

  btnDadosPrincipais.Visible := False;
end;

procedure TFCadastroOficioFormaEnvio.edtNomeKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if Trim(edtNome.Text) <> '' then
      btnOk.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroOficioFormaEnvio.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  vgOperacao     := VAZIO;
  vgIdConsulta   := 0;
  vgOrigemFiltro := False;

  inherited;
end;

procedure TFCadastroOficioFormaEnvio.FormCreate(Sender: TObject);
begin
  inherited;

  IdNovaFEnvio := 0;
end;

procedure TFCadastroOficioFormaEnvio.FormShow(Sender: TObject);
begin
  inherited;

  cdsOfcFEnv.Close;
  cdsOfcFEnv.Params.ParamByName('ID_OFICIO_FORMAENVIO').Value := vgIdConsulta;
  cdsOfcFEnv.Open;

  if vgOperacao = I then
    cdsOfcFEnv.Append;

  if vgOperacao = E then
    cdsOfcFEnv.Edit;

  if vgOperacao = C then
    pnlDados.Enabled := False;

  if vgOfc_TpOfc = 'E' then
  begin
    lblFormaEnvio.Caption    := 'FORMA DE ENVIO DO OF�CIO';
    lblFormaEnvio.Font.Color := clNavy;
  end
  else if vgOfc_TpOfc = 'R' then
  begin
    lblFormaEnvio.Caption    := 'FORMA DE RECEBIMENTO DO OF�CIO';
    lblFormaEnvio.Font.Color := $00005BE6;
  end;

  if edtNome.CanFocus then
    edtNome.SetFocus;
end;

function TFCadastroOficioFormaEnvio.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroOficioFormaEnvio.GravarDadosGenerico(
  var Msg: String): Boolean;
begin
  Result := True;
  Msg := '';

  if vgOperacao = C then
    Exit;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    if cdsOfcFEnv.State in [dsInsert, dsEdit] then
    begin
      if vgOperacao = I then
      begin
        cdsOfcFEnv.FieldByName('ID_OFICIO_FORMAENVIO').AsInteger := BS.ProximoId('ID_OFICIO_FORMAENVIO', 'OFICIO_FORMAENVIO');

        if vgOrigemCadastro then
          IdNovaFEnvio := cdsOfcFEnv.FieldByName('ID_OFICIO_FORMAENVIO').AsInteger;
      end;

      cdsOfcFEnv.Post;
      cdsOfcFEnv.ApplyUpdates(0);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    Msg := 'Forma de ' +
           IfThen((vgOfc_TpOfc = 'E'),
                  'Envio',
                  'Recebimento') +
           ' gravado com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na grava��o da Forma de ' +
           IfThen((vgOfc_TpOfc = 'E'),
                  'Envio',
                  'Recebimento') +
           '.';
  end;
end;

procedure TFCadastroOficioFormaEnvio.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(3, '', 'Somente consulta de Forma de ' + IfThen((vgOfc_TpOfc = 'E'),
                                                                 'Envio',
                                                                 'Recebimento'),
                          vgIdConsulta, 'OFICIO_FORMAENVIO');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(3, '', 'Somente inclus�o de Forma de ' + IfThen((vgOfc_TpOfc = 'E'),
                                                                 'Envio',
                                                                 'Recebimento'),
                          vgIdConsulta, 'OFICIO_FORMAENVIO');
    end;
    E:  //EDICAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO',
                                'Por favor, indique o motivo da edi��o dessa Forma de ' +
                                IfThen((vgOfc_TpOfc = 'E'),
                                       'Envio',
                                       'Recebimento') + ':',
                                '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(3, '', sObservacao,
                          vgIdConsulta, 'OFICIO_FORMAENVIO');
    end;
  end;
end;

procedure TFCadastroOficioFormaEnvio.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroOficioFormaEnvio.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

function TFCadastroOficioFormaEnvio.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if Trim(edtNome.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher o NOME DA FORMA DE ' +
                               IfThen((vgOfc_TpOfc = 'E'),
                                      'ENVIO',
                                      'RECEBIMENTO') +
                               ' DO OF�CIO.';
    DadosMsgErro.Componente := edtNome;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroOficioFormaEnvio.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

end.
