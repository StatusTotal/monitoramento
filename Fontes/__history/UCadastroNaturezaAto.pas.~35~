{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroNaturezaAto.pas
  Descricao:   Formulario de cadastro de Natureza de Ato
  Author   :   Pedro
  Date:        19-jun-2017
  Last Update: 28-ago-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroNaturezaAto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Vcl.StdCtrls,
  Vcl.Mask, Vcl.DBCtrls, sDBEdit, Data.DB, JvExControls, JvButton,
  JvTransparentButton, Vcl.ExtCtrls, sDBComboBox, sGroupBox, sDBRadioGroup;

type
  TFCadastroNaturezaAto = class(TFCadastroGeralPadrao)
    edNatureza: TsDBEdit;
    lcbSistema: TDBLookupComboBox;
    lblSistema: TLabel;
    procedure btnOkClick(Sender: TObject);
    procedure rgSistemaKeyPress(Sender: TObject; var Key: Char);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }

    iOrigem,
    iIdSistema,
    IdNovaNatureza: Integer;
  end;

var
  FCadastroNaturezaAto: TFCadastroNaturezaAto;

implementation

{$R *.dfm}

uses UDM, UBibliotecaSistema, UGDM, UVariaveisGlobais, UDMEtiqueta,
  UDMFolhaSeguranca;

{ TFCadastroNaturezaAto }

procedure TFCadastroNaturezaAto.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroNaturezaAto.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroNaturezaAto.DefinirTamanhoMaxCampos;
begin
  inherited;
  //
end;

procedure TFCadastroNaturezaAto.DesabilitarComponentes;
begin
  inherited;

  if vgOperacao = C then
    pnlDados.Enabled := False;
end;

procedure TFCadastroNaturezaAto.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if iOrigem = 4 then
    dmFolhaSeguranca.cdsNatureza.Cancel;

  if iOrigem = 5 then
    dmEtiqueta.cdsNatureza.Cancel;

  vgOperacao     := VAZIO;
  vgIdConsulta   := 0;
  vgOrigemFiltro := False;

  inherited;
end;

procedure TFCadastroNaturezaAto.FormCreate(Sender: TObject);
begin
  inherited;

  IdNovaNatureza := 0;
end;

procedure TFCadastroNaturezaAto.FormShow(Sender: TObject);
begin
  inherited;

  if iOrigem = 4 then
  begin
    dsCadastro.DataSet    := dmFolhaSeguranca.cdsNatureza;
    lcbSistema.ListSource := dmFolhaSeguranca.dsSistN;

    dmFolhaSeguranca.cdsNatureza.Close;
    dmFolhaSeguranca.cdsNatureza.Params.ParamByName('ID_NATUREZAATO').AsInteger := vgIdConsulta;
    dmFolhaSeguranca.cdsNatureza.Open;

    if vgOperacao = I then
      dmFolhaSeguranca.cdsNatureza.Append;

    if vgOperacao = E then
      dmFolhaSeguranca.cdsNatureza.Edit;
  end;

  if iOrigem = 5 then
  begin
    dsCadastro.DataSet    := dmEtiqueta.cdsNatureza;
    lcbSistema.ListSource := dmEtiqueta.dsSistN;

    dmEtiqueta.cdsNatureza.Close;
    dmEtiqueta.cdsNatureza.Params.ParamByName('ID_NATUREZAATO').AsInteger := vgIdConsulta;
    dmEtiqueta.cdsNatureza.Open;

    if vgOperacao = I then
    begin
      dmEtiqueta.cdsNatureza.Append;

      if vgOrigemFiltro then
        dmEtiqueta.cdsNatureza.FieldByName('ID_SISTEMA_FK').AsInteger := iIdSistema;
    end;

    if vgOperacao = E then
      dmEtiqueta.cdsNatureza.Edit;
  end;

  if iIdSistema > 0 then
    lcbSistema.Enabled := False
  else
    lcbSistema.Enabled := True;

  if edNatureza.CanFocus then
    edNatureza.SetFocus;
end;

function TFCadastroNaturezaAto.GravarDadosAto(var Msg: String): Boolean;
begin
   //
end;

function TFCadastroNaturezaAto.GravarDadosGenerico(
  var Msg: String): Boolean;
begin
  Result := True;
  Msg := '';

  if vgOperacao = C then
    Exit;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    { FOLHA DE SEGURANCA }
    if iOrigem = 4 then
    begin
      if vgOperacao = I then
        dmFolhaSeguranca.cdsNaturezaID_NATUREZAATO.AsInteger := dmGerencial.ValorGeneratorFD('GEN_NATUREZAATO_ID', dmPrincipal.conSISTEMA);

      IdNovaNatureza := dmFolhaSeguranca.cdsNaturezaID_NATUREZAATO.AsInteger;

      dmFolhaSeguranca.cdsNatureza.Post;
      dmFolhaSeguranca.cdsNatureza.ApplyUpdates(0);
    end;

    { ETIQUETA }
    if iOrigem = 5 then
    begin
      if vgOperacao = I then
        dmEtiqueta.cdsNaturezaID_NATUREZAATO.AsInteger := dmGerencial.ValorGeneratorFD('GEN_NATUREZAATO_ID', dmPrincipal.conSISTEMA);

      IdNovaNatureza := dmEtiqueta.cdsNaturezaID_NATUREZAATO.AsInteger;

      dmEtiqueta.cdsNatureza.Post;
      dmEtiqueta.cdsNatureza.ApplyUpdates(0);
    end;

    Msg := 'Natureza ' + edNatureza.Text + ' gravada com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na grava��o da Natureza: ' + edNatureza.Text + '.';
  end;

end;

procedure TFCadastroNaturezaAto.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(iOrigem, '', 'Somente consulta de Natureza',
                          vgIdConsulta, 'NATUREZAATO');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(iOrigem, '', 'Somente inclus�o de Natureza do Ato',
                          vgIdConsulta, 'NATUREZAATO');
    end;
    E:  //EDICAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da edi��o dessa Natureza:', '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(iOrigem, '', sObservacao,
                          vgIdConsulta, 'NATUREZAATO');
    end;
  end;

end;

procedure TFCadastroNaturezaAto.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroNaturezaAto.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

procedure TFCadastroNaturezaAto.rgSistemaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

function TFCadastroNaturezaAto.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if Trim(edNatureza.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar a DESCRI��O DA NATUREZA.';
    DadosMsgErro.Componente := edNatureza;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(lcbSistema.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar o SISTEMA.';
    DadosMsgErro.Componente := lcbSistema;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroNaturezaAto.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

end.
