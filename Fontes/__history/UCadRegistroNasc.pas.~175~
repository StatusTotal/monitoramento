{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadRegistroNasc.pas
  Descricao:   Formulario de Cadastro de Ato de registro de Nascimento
  Author   :   Cristina
  Date:        11-dez-2015
  Last Update:
------------------------------------------------------------------------------------------------------------------------}

unit UCadRegistroNasc;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroPadrao, JvExControls, JvButton,
  JvTransparentButton, Vcl.ExtCtrls, JvExExtCtrls, JvExtComponent, JvPanel,
  Vcl.Mask, JvExMask, JvToolEdit, Vcl.StdCtrls, Vcl.ComCtrls, JvExComCtrls,
  JvComCtrls, Vcl.DBCtrls, JvSpin, Data.DB, Vcl.Grids, Vcl.DBGrids, JvMaskEdit,
  JvBaseEdits, System.StrUtils, System.Generics.Collections, FireDAC.Comp.Client,
  FireDAC.Stan.Param, Datasnap.DBClient;

type
  { ATO REGISTRO NASCIMENTO }
  TDadosClasseAtoRegN = class(TObject)
  public
    Id:                  Integer;
    CodAto:              Integer;
    AnoAto:              Integer;
    CodTipoDocumento:    Integer;
    NumDocumento:        String;
    NumRGAtribuido:      String;
    NumCPFAtribuido:     String;
    FlgGemeos:           String;
    NumGemeos:           Integer;
    OrdemGemeo:          Integer;
    IdPessoaGemeo:       Integer;
    CodLocalNascimento:  Integer;
    NomeLocalNascimento: String;
    HoraNascimento:      TTime;

    constructor Create(); virtual;
    destructor  Destroy; override;
  end;

  TClasseAtoRegN = class(TDadosClasseAtoRegN)
  public
    Id:                  Integer;
    CodAto:              Integer;
    AnoAto:              Integer;
    CodTipoDocumento:    Integer;
    NumDocumento:        String;
    NumRGAtribuido:      String;
    NumCPFAtribuido:     String;
    FlgGemeos:           String;
    NumGemeos:           Integer;
    OrdemGemeo:          Integer;
    IdPessoaGemeo:       Integer;
    CodLocalNascimento:  Integer;
    NomeLocalNascimento: String;
    HoraNascimento:      TTime;
  end;

  TAtoRegN = class of TDadosClasseAtoRegN;

  TFCadRegistroNasc = class(TFCadastroAtoPadrao)
    tsNascido: TTabSheet;
    tsTestemunhas: TTabSheet;
    pnlNascido: TPanel;
    lblNomeNascido: TLabel;
    lblSexoNascido: TLabel;
    lblDataNascNascido: TLabel;
    lblNaturalidadeNascido: TLabel;
    shpGenitor: TShape;
    lblGenitor: TLabel;
    edtNomeNascido: TEdit;
    dteDataNascNascido: TJvDateEdit;
    cbDescrNaturalNascido: TComboBox;
    cbSexoNascido: TComboBox;
    pnlTestemunhas: TPanel;
    lblTipoDocRN: TLabel;
    lblNumDocRN: TLabel;
    lblNumRGAtribRN: TLabel;
    lblLocalNascRN: TLabel;
    lblHoraNascRN: TLabel;
    edtCodTipoDocRN: TEdit;
    cbDescrTipoDocRN: TComboBox;
    chbFlgGemeosRN: TCheckBox;
    edtNumDocRN: TEdit;
    edtNumRGAtribRN: TEdit;
    cbLocalNascRN: TComboBox;
    tedHoraNascRN: TJvTimeEdit;
    pnlDadosGenitor: TPanel;
    lblCPFFilNascido: TLabel;
    lblSexoFilNascido: TLabel;
    lblTpFiliacaoFilNascido: TLabel;
    lblNomeFilNascido: TLabel;
    lblNumIdentFilNascido: TLabel;
    lblOrgEmisFilNascido: TLabel;
    lblOutroDocFilNascido: TLabel;
    lblTipoOutroDocFilNascido: TLabel;
    lblDataEmisFilNascido: TLabel;
    lblOcupacaoFilNascido: TLabel;
    lblIdadeFilNascido: TLabel;
    lblNacionalFilNascido: TLabel;
    lblPaisNascFilNascido: TLabel;
    lblNaturalFilNascido: TLabel;
    btnEnderecoFilNascido: TJvTransparentButton;
    btnProgenitorFilNascido: TJvTransparentButton;
    cbTpFiliacaoFilNascido: TComboBox;
    edtNomeFilNascido: TEdit;
    cbSexoFilNascido: TComboBox;
    medCPFFilNascido: TJvMaskEdit;
    edtNumIdentFilNascido: TEdit;
    edtOrgEmisFilNascido: TEdit;
    edtOutroDocFilNascido: TEdit;
    cbTipoOutroDocFilNascido: TComboBox;
    edtDataEmisFilNascido: TJvDateEdit;
    cbDescrOcupacaoFilNascido: TComboBox;
    edtIdadeFilNascido: TEdit;
    cbNacionalFilNascido: TComboBox;
    cbPaisNascFilNascido: TComboBox;
    cbDescrNaturalFilNascido: TComboBox;
    pnlGridGenitor: TPanel;
    btnExcluirFilNascido: TJvTransparentButton;
    btnEditarFilNascido: TJvTransparentButton;
    btnIncluirFilNascido: TJvTransparentButton;
    btnConfirmarFilNascido: TJvTransparentButton;
    btnCancelarFilNascido: TJvTransparentButton;
    dbgFilNascido: TDBGrid;
    lblDataEmisRGFilNascido: TLabel;
    edtDataEmisRGFilNascido: TJvDateEdit;
    edtDescrOcupacaoEspFilNascido: TEdit;
    lblOcupacaoEspFilNascido: TLabel;
    cbRGFilNascido: TComboBox;
    lblUFFilNascido: TLabel;
    pnlGridTestemunhas: TPanel;
    btnExcluirTestemunha: TJvTransparentButton;
    btneditarTestemunha: TJvTransparentButton;
    btnIncluirTestemunha: TJvTransparentButton;
    btnConfirmarTestemunha: TJvTransparentButton;
    btnCancelarTestemunha: TJvTransparentButton;
    dbgTestemunha: TDBGrid;
    pnlDadosTestemunhas: TPanel;
    lblCPFTestemunha: TLabel;
    lblNomeTestemunha: TLabel;
    lblNumIdentTestemunha: TLabel;
    lblOrgEmissorTestemunha: TLabel;
    medCPFTestemunha: TJvMaskEdit;
    edtNomeTestemunha: TEdit;
    edtNumIdentTestemunha: TEdit;
    edtOrgEmissorTestemunha: TEdit;
    edtDataEmisRGTestemunha: TJvDateEdit;
    lblDataEmisRGTestemunha: TLabel;
    edtCodNaturalFilNascido: TEdit;
    edtCodNaturalNascido: TEdit;
    edtCodOcupacaoFilNascido: TEdit;
    lblRGNascido: TLabel;
    cbRGNascido: TComboBox;
    edtEspLocalNascimento: TEdit;
    lblEspLocalNascimento: TLabel;
    Label3: TLabel;
    ComboBox1: TComboBox;
    Edit1: TEdit;
    Label4: TLabel;
    Edit2: TEdit;
    JvTransparentButton1: TJvTransparentButton;
    gbGemeos: TGroupBox;
    btnExcluirGemeo: TJvTransparentButton;
    btnIncluirGemeo: TJvTransparentButton;
    dbgGemeo: TDBGrid;
    lblQtdGemeosRN: TLabel;
    edtQtdGemeosRN: TEdit;
    lblCPFAtribuidoRN: TLabel;
    medCPFAtribuidoRN: TJvMaskEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbSexoFilNascidoChange(Sender: TObject);
    procedure cbPaisNascFilNascidoChange(Sender: TObject);
    procedure cbRGNascidoChange(Sender: TObject);
    procedure cbRGFilNascidoChange(Sender: TObject);
    procedure chbFlgGemeosRNClick(Sender: TObject);
    procedure edtCodOcupacaoFilNascidoExit(Sender: TObject);
    procedure cbDescrOcupacaoFilNascidoExit(Sender: TObject);
    procedure edtCodNaturalNascidoExit(Sender: TObject);
    procedure cbDescrNaturalNascidoExit(Sender: TObject);
    procedure edtCodNaturalFilNascidoExit(Sender: TObject);
    procedure cbDescrNaturalFilNascidoExit(Sender: TObject);
    procedure btnIncluirGemeoClick(Sender: TObject);
    procedure btnExcluirGemeoClick(Sender: TObject);
    procedure cbDescrTipoDocRNExit(Sender: TObject);
    procedure edtCodTipoDocRNExit(Sender: TObject);
    procedure medCPFFilNascidoKeyPress(Sender: TObject; var Key: Char);

  protected
    procedure CarregarTabelasAuxiliaresDetalheAto; override;
    procedure CarregarDadosDetalheAto; override;
    procedure DesabilitarComponentesDetalheAto; override;
    procedure DefinirTamanhoMaxCamposDetalheAto; override;
    procedure GravarLog; override;

    function VerificarLayoutDetalheAto(var Msg: String;
      var QtdErros: Integer): Boolean; override;
    function GravarDadosDetalheAto(var msg: String): Boolean; override;

  private
    { Private declarations }

    procedure CarregarGemeos;

    procedure InicializarComponenteRegN;
    procedure FinalizarComponenteRegN;
  public
    { Public declarations }
  end;

var
  FCadRegistroNasc: TFCadRegistroNasc;
  AtoRegN: TAtoRegN;
  DadosAtoRegN: TDadosClasseAtoRegN;
  ListaAtosRegN: TList<TDadosClasseAtoRegN>;

implementation

{$R *.dfm}

uses UDM, UDMAto, UDMRegistroNasc, UVariaveisGlobais, UBibliotecaSistema,
  UPesquisaPessoa;

{ TFCadRegistroNasc }

procedure TFCadRegistroNasc.btnExcluirGemeoClick(Sender: TObject);
begin
  inherited;

  //...

  CarregarGemeos;
end;

procedure TFCadRegistroNasc.btnIncluirGemeoClick(Sender: TObject);
begin
  inherited;

  //...

  CarregarGemeos;
end;

procedure TFCadRegistroNasc.CarregarDadosDetalheAto;
begin
  inherited;

  { ATO REGISTRO DE NASCIMENTO }
  InicializarComponenteRegN;

  DadosAtoRegN := AtoRegN.Create;

  if vgAto_Operacao = I then
  begin
    DadosAtoRegN.Id     := 0;
    DadosAtoRegN.CodAto := vgAto_CodigoAto;
    DadosAtoRegN.AnoAto := vgAto_AnoAto;
  end
  else
  begin
    dmRegistroNasc.qryRegistroNasc.Close;
    dmRegistroNasc.qryRegistroNasc.Params.ParamByName('Codigo').Value := vgAto_CodigoAto;
    dmRegistroNasc.qryRegistroNasc.Params.ParamByName('Ano').Value    := vgAto_AnoAto;
    dmRegistroNasc.qryRegistroNasc.Open;

    DadosAtoRegN.Id                  := dmRegistroNasc.qryRegistroNasc.FieldByName('ID_ATO_REGISTRONASC').AsInteger;
    DadosAtoRegN.CodAto              := dmRegistroNasc.qryRegistroNasc.FieldByName('COD_ATO_FK').AsInteger;
    DadosAtoRegN.AnoAto              := dmRegistroNasc.qryRegistroNasc.FieldByName('ANO_ATO_FK').AsInteger;
    DadosAtoRegN.CodTipoDocumento    := dmRegistroNasc.qryRegistroNasc.FieldByName('COD_TIPODOCUMENTO_NASC').AsInteger;
    DadosAtoRegN.NumDocumento        := dmRegistroNasc.qryRegistroNasc.FieldByName('NUM_DOCUMENTO').AsString;
    DadosAtoRegN.NumRGAtribuido      := dmRegistroNasc.qryRegistroNasc.FieldByName('NUM_RG_ATRIBUIDO').AsString;
    DadosAtoRegN.NumCPFAtribuido     := dmRegistroNasc.qryRegistroNasc.FieldByName('NUM_CPF_ATRIBUIDO').AsString;
    DadosAtoRegN.FlgGemeos           := dmRegistroNasc.qryRegistroNasc.FieldByName('FLG_GEMEOS').AsString;
    DadosAtoRegN.NumGemeos           := dmRegistroNasc.qryRegistroNasc.FieldByName('NUM_GEMEOS').AsInteger;
    DadosAtoRegN.OrdemGemeo          := dmRegistroNasc.qryRegistroNasc.FieldByName('ORDEM_GEMEO').AsInteger;
    DadosAtoRegN.IdPessoaGemeo       := dmRegistroNasc.qryRegistroNasc.FieldByName('ID_PESSOAGEMEO_FK').AsInteger;
    DadosAtoRegN.CodLocalNascimento  := dmRegistroNasc.qryRegistroNasc.FieldByName('COD_LOCAL_NASC').AsInteger;
    DadosAtoRegN.NomeLocalNascimento := dmRegistroNasc.qryRegistroNasc.FieldByName('NOME_LOCAL_NASC').AsString;
    DadosAtoRegN.HoraNascimento      := dmRegistroNasc.qryRegistroNasc.FieldByName('HORA_NASCIMENTO').AsInteger;
  end;

  edtCodTipoDocRN.Text       := IntToStr(DadosAtoRegN.CodTipoDocumento);
  edtNumDocRN.Text           := DadosAtoRegN.NumDocumento;
  edtNumRGAtribRN.Text       := DadosAtoRegN.NumRGAtribuido;
  medCPFAtribuidoRN.Text     := DadosAtoRegN.NumCPFAtribuido;
  tedHoraNascRN.Text         := TimeToStr(DadosAtoRegN.HoraNascimento);
  edtEspLocalNascimento.Text := DadosAtoRegN.NomeLocalNascimento;

  //Recupera o Nome do Tipo de Documento cujo Codigo foi informado
  BS.RecuperCodigoNome(edtCodTipoDocRN, cbDescrTipoDocRN, EDTPDOCRN, 'E');

  medMatNacionalAto.Text := GerarMatricula('0');

  if Trim(DadosAtoRegN.FlgGemeos) = 'S' then
    CarregarGemeos;
end;

procedure TFCadRegistroNasc.CarregarGemeos;
begin
  with dmRegistroNasc.qryPessoaGemeo do
  begin
    Close;
    Params.ParamByName('IdPessoaGemeo').Value := DadosAtoRegN.IdPessoaGemeo;
    Open;

    edtQtdGemeosRN.Text := IntToStr(RecordCount);
  end;
end;

procedure TFCadRegistroNasc.CarregarTabelasAuxiliaresDetalheAto;
begin
  inherited;

  { REGISTRO DE NASCIMENTO }
  //Tipo de Documento
  BS.MontarComboBox(cbDescrTipoDocRN,
                    dmRegistroNasc.qryTipoDocumentoNasc,
                    'DESCR_TIPODOCUMENTO_NASC');

  //Local do Nascimento
  BS.MontarComboBox(cbLocalNascRN,
                    dmRegistroNasc.qryLocalNasc,
                    'DESCR_LOCAL_NASC');

  { NASCIDO }
  //UF
  BS.MontarComboBox(cbRGFilNascido,
                    dmRegistroNasc.qryUFNascido,
                    'SIGLA_ESTADO');

  //Tipo Filiacao (Genitor)
  BS.MontarComboBox(cbTpFiliacaoFilNascido,
                    dmAto.qryTipoFiliacao,
                    'DESCR_TIPOFILIACAO');

  //Tipo Documento - Outro Documento (Genitor)
  BS.MontarComboBox(cbTipoOutroDocFilNascido,
                    dmAto.qryTipoDocumento,
                    'DESCR_DOCUMENTO');

  //Ocupacao SDC (Genitor)
  BS.MontarComboBox(cbDescrOcupacaoFilNascido,
                    dmAto.qryOcupacao,
                    'DESCR_OCUPACAO');

  //Pais de Nascimento (Genitor)
  BS.MontarComboBox(cbPaisNascFilNascido,
                    dmRegistroNasc.qryPaisFilNascido,
                    'PAIS');

  //Nacionalidade (Genitor)
  BS.MontarComboBox(cbNacionalFilNascido,
                    dmRegistroNasc.qryNacionalFilNascido,
                    'NACIONALIDADE');

  //UF (Genitor)
  BS.MontarComboBox(cbRGFilNascido,
                    dmRegistroNasc.qryUFGenitor,
                    'SIGLA_ESTADO');
end;

procedure TFCadRegistroNasc.cbDescrNaturalFilNascidoExit(Sender: TObject);
begin
  inherited;

  //Recupera o Codigo de Naturalidade cujo Nome foi informado
  BS.RecuperCodigoNome(edtCodNaturalFilNascido, cbDescrNaturalFilNascido, CBNATURAL, 'C');
end;

procedure TFCadRegistroNasc.cbDescrNaturalNascidoExit(Sender: TObject);
begin
  inherited;

  //Recupera o Codigo de Naturalidade cujo Nome foi informado
  BS.RecuperCodigoNome(edtCodNaturalNascido, cbDescrNaturalNascido, CBNATURAL, 'C');
end;

procedure TFCadRegistroNasc.cbDescrOcupacaoFilNascidoExit(Sender: TObject);
begin
  inherited;

  //Recupera o Codigo de Ocupacao cujo Nome foi informado
  BS.RecuperCodigoNome(edtCodOcupacaoFilNascido, cbDescrOcupacaoFilNascido, CBOCUP, 'C');
end;

procedure TFCadRegistroNasc.cbDescrTipoDocRNExit(Sender: TObject);
begin
  inherited;

  //Recupera o Codigo do Tipo de Documento cujo Nome foi informado
  BS.RecuperCodigoNome(edtCodTipoDocRN, cbDescrTipoDocRN, CBTPDOCRN, 'C');
end;

procedure TFCadRegistroNasc.cbPaisNascFilNascidoChange(Sender: TObject);
begin
  inherited;

  cbNacionalFilNascido.ItemIndex := cbTpFiliacaoFilNascido.ItemIndex;
end;

procedure TFCadRegistroNasc.cbRGFilNascidoChange(Sender: TObject);
begin
  inherited;

  if Trim(cbRGFilNascido.Text) <> '' then
  begin
    //Naturalidade (Genitor)
    BS.MontarComboBox(cbDescrNaturalFilNascido,
                      dmRegistroNasc.qryNaturalFilNascido,
                      'MUNICIPIO',
                      cbRGFilNascido.Items[cbRGFilNascido.ItemIndex]);
  end;
end;

procedure TFCadRegistroNasc.cbSexoFilNascidoChange(Sender: TObject);
begin
  inherited;

  if cbSexoFilNascido.Text = 'F' then
    cbTpFiliacaoFilNascido.ItemIndex := cbTpFiliacaoFilNascido.Items.IndexOf('M�E');

  if cbSexoFilNascido.Text = 'M' then
    cbTpFiliacaoFilNascido.ItemIndex := cbTpFiliacaoFilNascido.Items.IndexOf('PAI');
end;

procedure TFCadRegistroNasc.chbFlgGemeosRNClick(Sender: TObject);
begin
  inherited;

  if chbFlgGemeosRN.Checked then
  begin
    gbGemeos.Visible := True;
    CarregarGemeos;
  end
  else
  begin
    gbGemeos.Visible := False;
    dmRegistroNasc.qryPessoaGemeo.Close;
  end;

  DadosAtoRegN.FlgGemeos := IfThen(chbFlgGemeosRN.Checked, 'S', 'N');
end;

procedure TFCadRegistroNasc.cbRGNascidoChange(Sender: TObject);
begin
  inherited;

  if Trim(cbRGNascido.Text) <> '' then
  begin
    //Naturalidade (Nascido)
    BS.MontarComboBox(cbDescrNaturalNascido,
                      dmRegistroNasc.qryNaturalidadeNascido,
                      'MUNICIPIO',
                      cbRGNascido.Items[cbRGNascido.ItemIndex]);
  end;
end;

procedure TFCadRegistroNasc.DefinirTamanhoMaxCamposDetalheAto;
begin
  inherited;
  { ATO }
  edtCodTipoDocRN.MaxLength := 1;
  edtNumDocRN.MaxLength     := 25;
  edtNumRGAtribRN.MaxLength := 25;

  cbLocalNascRN.MaxLength  := 1;
  edtQtdGemeosRN.MaxLength := 1;

  { NASCIDO }
  edtNomeNascido.MaxLength := 250;

  edtNomeFilNascido.MaxLength := 250;

  edtNumIdentFilNascido.MaxLength := 25;
  edtOrgEmisFilNascido.MaxLength  := 70;
  edtOutroDocFilNascido.MaxLength := 25;

  edtIdadeFilNascido.MaxLength := 3;

  { TESTEMUNHAS }
  edtNomeTestemunha.MaxLength       := 250;
  edtNumIdentTestemunha.MaxLength   := 25;
  edtOrgEmissorTestemunha.MaxLength := 70;

  { IMPRESSOS DE SEGURANCA }
  edtNomeTestemunha.MaxLength := 250;

  { EMOLUMENTOS }
  edtTipoCobEmol.MaxLength := 2;
end;

procedure TFCadRegistroNasc.DesabilitarComponentesDetalheAto;
begin
  inherited;

  //ABAS E PAINEIS
  pnlAto.Enabled              := vgAto_Operacao in [I, E];
  gbGemeos.Visible            := False;
  pnlNascido.Enabled          := vgAto_Operacao in [I, E];
  pnlTestemunhas.Enabled      := vgAto_Operacao in [I, E];
  pnlObservacoes.Enabled      := vgAto_Operacao in [I, E];
  pnlDadosConferencia.Enabled := vgAto_Operacao in [I, E];

  //BOTOES
  btnExibirPDF.Enabled := vgPrm_ImpRegNasc and vgPrm_EdRegNasc and
                          (vgAto_Operacao in [E, C, P]);
  btnImprimir.Enabled  := vgPrm_ImpRegNasc and vgPrm_EdRegNasc and
                          (vgAto_Operacao in [E, C, P]);
  btnEditar.Enabled    := vgPrm_EdRegNasc and (vgAto_Operacao in [E, C]);
  btnExcluir.Enabled   := vgPrm_ExcRegNasc and (vgAto_Operacao in [E, C]);
  btnOk.Enabled        := vgPrm_IncRegNasc and vgPrm_EdRegNasc and
                          (vgAto_Operacao in [I, E]);
  btnCancelar.Enabled  := vgPrm_IncRegNasc and vgPrm_EdRegNasc and
                          (vgAto_Operacao in [I, E]);

  btnEditar.Visible  := vgPrm_EdRegNasc and (vgAto_Operacao in [E, C]);
  btnExcluir.Visible := vgPrm_ExcRegNasc and (vgAto_Operacao in [E, C]);

  //CAMPOS
end;

procedure TFCadRegistroNasc.edtCodNaturalFilNascidoExit(Sender: TObject);
begin
  inherited;

  //Recupera o Nome da Naturalidade cujo Codigo foi informado
  BS.RecuperCodigoNome(edtCodNaturalFilNascido, cbDescrNaturalFilNascido, EDNATURAL, 'E');
end;

procedure TFCadRegistroNasc.edtCodNaturalNascidoExit(Sender: TObject);
begin
  inherited;

  //Recupera o Nome da Naturalidade cujo Codigo foi informado
  BS.RecuperCodigoNome(edtCodNaturalNascido, cbDescrNaturalNascido, EDNATURAL, 'E');
end;

procedure TFCadRegistroNasc.edtCodOcupacaoFilNascidoExit(Sender: TObject);
begin
  inherited;

  //Recupera o Nome da Ocupacao cujo Codigo foi informado
  BS.RecuperCodigoNome(edtCodOcupacaoFilNascido, cbDescrOcupacaoFilNascido, EDOCUP, 'E');
end;

procedure TFCadRegistroNasc.edtCodTipoDocRNExit(Sender: TObject);
begin
  inherited;

  //Recupera o Nome do Tipo de Documento cujo Codigo foi informado
  BS.RecuperCodigoNome(edtCodTipoDocRN, cbDescrTipoDocRN, EDTPDOCRN, 'E');
end;

procedure TFCadRegistroNasc.FinalizarComponenteRegN;
begin
  FreeAndNil(ListaAtosRegN);
end;

procedure TFCadRegistroNasc.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  FinalizarComponenteRegN;
  FreeAndNil(dmAto);
  FreeAndNil(dmRegistroNasc);
end;

function TFCadRegistroNasc.GravarDadosDetalheAto(var msg: String): Boolean;
begin
  //
end;

procedure TFCadRegistroNasc.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgAto_Operacao of
    I, C:  //INCLUSAO /CONSULTA
    begin
      BS.GravarUsuarioLog(2, DadosAto.Selo, '');
    end;
    E:  //EDICAO
    begin
      sObservacao := 'Ato anterior: ' +
                     vgConf_DirDigitNascimento +
                     IntToStr(vgAto_AnoAto) + '/' +
                     IntToStr(vgAto_CodigoAto) + '_' +
                     IntToStr(vgAto_AnoAto) + '_' +
                     IntToStr(vgAto_VersaoAto - 1) + '.pdf';

      BS.GravarUsuarioLog(2, DadosAto.Selo, sObservacao);
    end;
    X:  //EXCLUSAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da exclus�o desse Ato:', '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(2, DadosAto.Selo, sObservacao);
    end;
    P:  //IMPRESSAO
    begin
      sObservacao := vgConf_DirDigitNascimento +
                     IntToStr(vgAto_AnoAto) + '/' +
                     IntToStr(vgAto_CodigoAto) + '_' +
                     IntToStr(vgAto_AnoAto) + '_' +
                     IntToStr(vgAto_VersaoAto) + '.pdf';

      BS.GravarUsuarioLog(2, DadosAto.Selo, sObservacao);
    end;
  end;
end;

procedure TFCadRegistroNasc.InicializarComponenteRegN;
begin
  AtoRegN       := TClasseAtoRegN;
  ListaAtosRegN := TList<TDadosClasseAtoRegN>.Create;
end;

procedure TFCadRegistroNasc.medCPFFilNascidoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
     SelectNext(ActiveControl, True, True);
     Key := #0;
   end;

  if Key = #13 then  //ENTER
  begin
    if Sender = medCPFFilNascido then
      BS.CriarForm(TFPesquisaPessoa, FPesquisaPessoa)
    else
    begin
      SelectNext(ActiveControl, True, True);
      Key := #0;
    end;
  end;

  if Key = #27 then  //ESCAPE
    btnCancelar.Click;
end;

function TFCadRegistroNasc.VerificarLayoutDetalheAto(var Msg: String;
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  Msg      := '';
  QtdErros := 0;

  //

  if Trim(Msg) <> '' then
    Result := False;
end;

{ TDadosClasseAtoRegN }

constructor TDadosClasseAtoRegN.Create;
begin
  Id                  := 0;
  CodAto              := 0;
  AnoAto              := 0;
  CodTipoDocumento    := 1;
  NumDocumento        := '';
  NumRGAtribuido      := '';
  NumCPFAtribuido     := '';
  FlgGemeos           := 'N';
  NumGemeos           := 0;
  OrdemGemeo          := 0;
  IdPessoaGemeo       := 0;
  CodLocalNascimento  := 0;
  NomeLocalNascimento := '';
  HoraNascimento      := 0;
end;

destructor TDadosClasseAtoRegN.Destroy;
begin

  inherited;
end;

end.
