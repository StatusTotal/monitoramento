{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroLoteRCPN.pas
  Descricao:   Formulario de cadastro de Lotes de Folhas de RCPN
  Author   :   Pedro
  Date:        20-jun-2017
  Last Update: 01-set-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroLoteRCPN;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Mask, sMaskEdit, sCustomComboEdit, sToolEdit, sDBDateEdit, Vcl.DBCtrls,
  sDBEdit, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, sComboBox, sGroupBox;

type
  TFCadastroLoteRCPN = class(TFCadastroGeralPadrao)
    edLote: TsDBEdit;
    edLetra: TsDBEdit;
    edInicial: TsDBEdit;
    edQuantidade: TsDBEdit;
    edFinal: TsDBEdit;
    sDBDateEdit1: TsDBDateEdit;
    gbDetalhes: TsGroupBox;
    lblNatureza: TLabel;
    lblNomeFuncionario: TLabel;
    btnIncluirNatureza: TJvTransparentButton;
    lcbNatureza: TDBLookupComboBox;
    lcbNomeFuncionario: TDBLookupComboBox;
    dtUtilizacao: TsDateEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edQuantidadeExit(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure gbDetalhesCheckBoxChanged(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnIncluirNaturezaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lcbNomeFuncionarioKeyPress(Sender: TObject; var Key: Char);
    procedure lbMensagemErroDblClick(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }

    procedure AbrirListaNatureza;
    procedure HabDesabBotoes(Hab:Boolean);
  public
    { Public declarations }
  end;

var
  FCadastroLoteRCPN: TFCadastroLoteRCPN;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais,
  UCadastroNaturezaAto, UDMFolhaSeguranca;

procedure TFCadastroLoteRCPN.AbrirListaNatureza;
begin
  dmFolhaSeguranca.qryNatAto.Close;
  dmFolhaSeguranca.qryNatAto.Params.ParamByName('ID_SIST01').Value := 5;
  dmFolhaSeguranca.qryNatAto.Params.ParamByName('ID_SIST02').Value := 5;
  dmFolhaSeguranca.qryNatAto.Open;
end;

procedure TFCadastroLoteRCPN.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroLoteRCPN.btnIncluirNaturezaClick(Sender: TObject);
var
  Op: TOperacao;
  IdCons: Integer;
  OrigF, OrigC: Boolean;
begin
  inherited;

  Op     := vgOperacao;
  IdCons := vgIdConsulta;
  OrigF  := vgOrigemFiltro;
  OrigC  := vgOrigemCadastro;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := False;
  vgOrigemCadastro := False;

  try
    Application.CreateForm(TFCadastroNaturezaAto, FCadastroNaturezaAto);
    FCadastroNaturezaAto.iOrigem    := 4;
    FCadastroNaturezaAto.iIdSistema := 5;
    FCadastroNaturezaAto.ShowModal;
  finally
    AbrirListaNatureza;

    if FCadastroNaturezaAto.IdNovaNatureza <> 0 then
      lcbNatureza.KeyValue := IntToStr(FCadastroNaturezaAto.IdNovaNatureza);

    FCadastroNaturezaAto.Free;
  end;

  vgOperacao       := Op;
  vgIdConsulta     := IdCons;
  vgOrigemFiltro   := OrigF;
  vgOrigemCadastro := OrigC;
end;

procedure TFCadastroLoteRCPN.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroLoteRCPN.DefinirTamanhoMaxCampos;
begin
  inherited;
//
end;

procedure TFCadastroLoteRCPN.DesabilitarComponentes;
begin
  inherited;

  if vgOperacao = C then
    pnlDados.Enabled := False;
end;

procedure TFCadastroLoteRCPN.edQuantidadeExit(Sender: TObject);
var
  iNumIni, iNumFin, iQtd:Integer;
begin
  iNumIni := dmGerencial.PegarNumero(edInicial.Text);
  iQtd := dmGerencial.PegarNumero(edQuantidade.Text);

  iNumFin := iNumIni + iQtd - 1;

  dmFolhaSeguranca.cdsLoteFlsNUM_FINAL.AsString := dmGerencial.Zeros(IntToStr(iNumFin), 'D', Length(edInicial.Text));

  inherited;
end;

procedure TFCadastroLoteRCPN.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dmFolhaSeguranca.cdsLoteFls.Cancel;

  vgOperacao     := VAZIO;
  vgIdConsulta   := 0;
  vgOrigemFiltro := False;

  inherited;
end;

procedure TFCadastroLoteRCPN.FormCreate(Sender: TObject);
begin
  inherited;

  dmFolhaSeguranca.qryFuncionarios.Close;
  dmFolhaSeguranca.qryFuncionarios.Open;

  AbrirListaNatureza;

  dmFolhaSeguranca.cdsLoteFls.Close;
  dmFolhaSeguranca.cdsLoteFls.Params.ParamByName('ID_LOTEFOLHA').AsInteger := vgIdConsulta;
  dmFolhaSeguranca.cdsLoteFls.Open;

  if vgOperacao = I then
  begin
    dmFolhaSeguranca.cdsLoteFls.Append;
    dmFolhaSeguranca.cdsLoteFls.FieldByName('FLG_RCPN').AsString        := 'S';
    dmFolhaSeguranca.cdsLoteFls.FieldByName('FLG_SITUACAO').AsString    := 'N';
    dmFolhaSeguranca.cdsLoteFls.FieldByName('CAD_ID_USUARIO').AsInteger := vgUsu_Id;
  end;

  if vgOperacao = E then
    dmFolhaSeguranca.cdsLoteFls.Edit;

  gbDetalhes.Checked:=False;
  gbDetalhesCheckBoxChanged(Sender);
end;

procedure TFCadastroLoteRCPN.FormShow(Sender: TObject);
begin
  inherited;

  if edLote.CanFocus then
    edLote.SetFocus;
end;

procedure TFCadastroLoteRCPN.gbDetalhesCheckBoxChanged(Sender: TObject);
begin
  inherited;
  if gbDetalhes.Checked then
  begin
    dtUtilizacao.Enabled       := True;
    lcbNatureza.Enabled        := True;
    lcbNomeFuncionario.Enabled := True;
    btnIncluirNatureza.Enabled := True;

    dtUtilizacao.Color       := clWindow;
    lcbNatureza.Color        := clWindow;
    lcbNomeFuncionario.Color := clWindow;
  end
  else
  begin
    dtUtilizacao.Enabled       := False;
    lcbNatureza.Enabled        := False;
    lcbNomeFuncionario.Enabled := False;
    btnIncluirNatureza.Enabled := False;

    dtUtilizacao.Color       := clSilver;
    lcbNatureza.Color        := clSilver;
    lcbNomeFuncionario.Color := clSilver;

    dtUtilizacao.Clear;
    lcbNatureza.KeyValue        := 0;
    lcbNomeFuncionario.KeyValue := 0;
  end;
end;

function TFCadastroLoteRCPN.GravarDadosAto(var Msg: String): Boolean;
begin
//
end;

function TFCadastroLoteRCPN.GravarDadosGenerico(var Msg: String): Boolean;
var
  nInicial, nFinal, nCount,
  iIdLtFls:Integer;
begin
  Result := True;
  Msg := '';

  iIdLtFls := 0;

  if vgOperacao = C then
    Exit;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    HabDesabBotoes(False);

    if vgOperacao = I then
    begin
      iIdLtFls := dmGerencial.ValorGeneratorFD('GEN_LOTEFOLHA_ID',dmPrincipal.conSISTEMA);
      dmFolhaSeguranca.cdsLoteFls.FieldByName('ID_LOTEFOLHA').AsInteger := iIdLtFls;
    end
    else
      iIdLtFls := dmFolhaSeguranca.cdsLoteFls.FieldByName('ID_LOTEFOLHA').AsInteger;

    dmFolhaSeguranca.cdsLoteFlsFLG_SITUACAO.AsString := dmGerencial.IIF(gbDetalhes.Checked,'U','N');
    dmFolhaSeguranca.cdsLoteFls.Post;
    dmFolhaSeguranca.cdsLoteFls.ApplyUpdates(0);

    nFinal := dmGerencial.PegarNumero(edFinal.Text);

    dmFolhaSeguranca.cdsLoteFls.Close;
    dmFolhaSeguranca.cdsLoteFls.Params.ParamByName('ID_LOTEFOLHA').Value := iIdLtFls;
    dmFolhaSeguranca.cdsLoteFls.Open;

    nCount := 1;

    for nInicial := dmGerencial.PegarNumero(edInicial.Text) to nFinal do
    begin
      Application.ProcessMessages;
      dmGerencial.Processando(True,'Aguarde: '+IntToStr(nInicial)+' | '+Inttostr(nFinal));

      dmFolhaSeguranca.qryFlxAux.Close;
      dmFolhaSeguranca.qryFlxAux.ParamByName('ID_LOTEFOLHA').AsInteger := dmFolhaSeguranca.cdsLoteFlsID_LOTEFOLHA.AsInteger;
      dmFolhaSeguranca.qryFlxAux.ParamByName('ORDEM').AsInteger        := nCount;
      dmFolhaSeguranca.qryFlxAux.Open;

      if dmFolhaSeguranca.qryFlxAux.IsEmpty then
      begin
        dmFolhaSeguranca.qryFlxAux.Append;
        dmFolhaSeguranca.qryFlxAuxID_FOLHASEGURANCA.AsInteger := dmGerencial.ValorGeneratorFD('GEN_FOLHASEGURANCA_ID',
                                                                                              dmPrincipal.conSISTEMA);
      end
      else
        dmFolhaSeguranca.qryFlxAux.Edit;

      dmFolhaSeguranca.qryFlxAuxID_LOTEFOLHA_FK.AsInteger := dmFolhaSeguranca.cdsLoteFlsID_LOTEFOLHA.AsInteger;
      dmFolhaSeguranca.qryFlxAuxORDEM.AsInteger:=nCount;

      dmFolhaSeguranca.qryFlxAuxNUM_FOLHA.AsString := dmFolhaSeguranca.cdsLoteFlsNUM_LOTE.AsString +
                                                      dmFolhaSeguranca.cdsLoteFlsLETRA.AsString +
                                                      dmGerencial.Zeros(IntToStr(nInicial), 'D', Length(edInicial.Text));

      dmFolhaSeguranca.qryFlxAuxFLG_RCPN.AsString := 'S';

      if gbDetalhes.Checked then
      begin
        dmFolhaSeguranca.qryFlxAuxID_SISTEMA_FK.AsInteger     := 5;
        dmFolhaSeguranca.qryFlxAuxDATA_UTILIZACAO.AsDateTime  := dtUtilizacao.Date;
        dmFolhaSeguranca.qryFlxAuxID_NATUREZAATO_FK.AsInteger := lcbNatureza.KeyValue;
        dmFolhaSeguranca.qryFlxAuxID_FUNCIONARIO_FK.AsInteger := lcbNomeFuncionario.KeyValue;
        dmFolhaSeguranca.qryFlxAuxCAD_ID_USUARIO.AsInteger    := vgUsu_Id;
        dmFolhaSeguranca.qryFlxAuxFLG_SITUACAO.AsString       := 'U';
      end
      else
        dmFolhaSeguranca.qryFlxAuxFLG_SITUACAO.AsString := 'N';

      dmFolhaSeguranca.qryFlxAux.Post;

      Inc(nCount);
    end;

    dmGerencial.Processando(False);

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    Msg := 'Lote de Folhas RCPN ' + edLote.Text + ' gravado com sucesso!';

    HabDesabBotoes(True);
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    HabDesabBotoes(True);

    dmGerencial.Processando(False);

    Result := False;
    Msg := 'Erro na grava��o da Lote de Folhas RCPN' + edLote.Text + '.';
  end;
end;

procedure TFCadastroLoteRCPN.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(4, '', 'Somente consulta de Dados de Lote de Folha de RCPN',
                          vgIdConsulta, 'LOTEFOLHA');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(4, '', 'Somente inclus�o de Dados de Lote de Folha de RCPN',
                          vgIdConsulta, 'LOTEFOLHA');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da edi��o desse Lote de Folha de RCPN:', '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de Lote de Folha de RCPN';

      BS.GravarUsuarioLog(4, '', sObservacao,
                          vgIdConsulta, 'LOTEFOLHA');
    end;
  end;
end;

procedure TFCadastroLoteRCPN.HabDesabBotoes(Hab: Boolean);
begin
  btnOk.Enabled:=Hab;
  btnCancelar.Enabled:=Hab;
  btnIncluirNatureza.Enabled:=Hab;

  btnIncluirNatureza.Enabled := Hab;
end;

procedure TFCadastroLoteRCPN.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroLoteRCPN.lcbNomeFuncionarioKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroLoteRCPN.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
//
end;

function TFCadastroLoteRCPN.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if edLote.Text = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha o campo Numera��o Lote.';
    DadosMsgErro.Componente := edLote;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if edLetra.Text ='' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo Letra.';
    DadosMsgErro.Componente := edletra;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if edInicial.Text ='' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo Numera��o Inicial.';
    DadosMsgErro.Componente := edInicial;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if (edQuantidade.Text = '') or (edQuantidade.Text='0') then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo Quantidade.';
    DadosMsgErro.Componente := edQuantidade;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if gbDetalhes.Checked then
  begin
    if (dtUtilizacao.Date = 0) then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo Data Utiliza��o.';
      DadosMsgErro.Componente := dtUtilizacao;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;

    if (lcbNatureza.KeyValue = -1) then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo Natureza.';
      DadosMsgErro.Componente := lcbNatureza;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;

    if (lcbNomeFuncionario.KeyValue = -1) then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo Nome do Funcion�rio.';
      DadosMsgErro.Componente := lcbNomeFuncionario;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroLoteRCPN.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
//
end;

end.
