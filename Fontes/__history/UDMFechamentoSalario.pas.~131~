{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UDMFechamentoSalario.pas
  Descricao:   Data Module de Fechamento de Salario dos Funcionarios
  Author   :   Cristina
  Date:        07-jun-2016
  Last Update: 27-out-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UDMFechamentoSalario;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.Provider,
  Datasnap.DBClient, System.DateUtils;

type
  TdmFechamentoSalario = class(TDataModule)
    cdsSalario: TClientDataSet;
    cdsSalarioFUNCIONARIO: TStringField;
    dspSalario: TDataSetProvider;
    qrySalario: TFDQuery;
    dsSalario: TDataSource;
    dsFechamento: TDataSource;
    dspFechamento: TDataSetProvider;
    cdsFechamento: TClientDataSet;
    qryFechamento: TFDQuery;
    cdsFechamentoID_FECHAMENTO_SALARIO: TIntegerField;
    cdsFechamentoMES_REFERENCIA: TIntegerField;
    cdsFechamentoANO_REFERENCIA: TIntegerField;
    cdsFechamentoMES_PAGAMENTO: TIntegerField;
    cdsFechamentoANO_PAGAMENTO: TIntegerField;
    cdsFechamentoDATA_PREV_PAGAMENTO: TDateField;
    cdsFechamentoPREV_ID_USUARIO: TIntegerField;
    cdsFechamentoDATA_REAL_PAGAMENTO: TDateField;
    cdsFechamentoPAG_ID_USUARIO: TIntegerField;
    cdsFechamentoFLG_STATUS: TStringField;
    cdsFechamentoDATA_CANCELAMENTO: TDateField;
    cdsFechamentoCANCEL_ID_USUARIO: TIntegerField;
    cdsSalarioID_SALARIO: TIntegerField;
    cdsSalarioID_FECHAMENTO_SALARIO_FK: TIntegerField;
    cdsSalarioID_FUNCIONARIO_FK: TIntegerField;
    cdsSalarioVR_SALARIO_BASE: TBCDField;
    cdsSalarioVR_TOTAL_COMISSAO: TBCDField;
    cdsSalarioVR_TOTAL_VALE: TBCDField;
    cdsSalarioPRE_CADASTRADO: TBooleanField;
    cdsSalarioCPF: TStringField;
    qryDetalheLanc: TFDQuery;
    dspDetalheLanc: TDataSetProvider;
    cdsDetalheLanc: TClientDataSet;
    dsDetalheLanc: TDataSource;
    cdsDetalheLancID_LANCAMENTO_DET: TIntegerField;
    cdsDetalheLancCOD_LANCAMENTO_FK: TIntegerField;
    cdsDetalheLancANO_LANCAMENTO_FK: TIntegerField;
    cdsDetalheLancSELO_ORIGEM: TStringField;
    cdsDetalheLancALEATORIO_ORIGEM: TStringField;
    cdsDetalheLancTIPO_COBRANCA: TStringField;
    cdsDetalheLancCOD_ADICIONAL: TIntegerField;
    cdsDetalheLancEMOLUMENTOS: TBCDField;
    cdsDetalheLancFETJ: TBCDField;
    cdsDetalheLancFUNDPERJ: TBCDField;
    cdsDetalheLancFUNPERJ: TBCDField;
    cdsDetalheLancFUNARPEN: TBCDField;
    cdsDetalheLancPMCMV: TBCDField;
    cdsDetalheLancISS: TBCDField;
    cdsDetalheLancFLG_CANCELADO: TStringField;
    cdsDetalheLancVR_UNITARIO: TBCDField;
    cdsDetalheLancQUANTIDADE: TIntegerField;
    cdsDetalheLancVR_TOTAL: TBCDField;
    cdsDetalheLancID_ITEM_FK: TIntegerField;
    cdsDetalheLancDESCR_ITEM: TStringField;
    cdsSalarioNOVO: TBooleanField;
    cdsSalarioEXCLUIDO: TBooleanField;
    cdsSalarioVR_SALARIO_APAGAR: TBCDField;
    cdsFechamentoVR_TOTAL_BASE: TBCDField;
    cdsFechamentoVR_TOTAL_COMISSAO: TBCDField;
    cdsFechamentoVR_TOTAL_OUTRADESPESA: TBCDField;
    cdsFechamentoVR_TOTAL_VALE: TBCDField;
    cdsFechamentoVR_TOTAL_APAGAR: TBCDField;
    cdsSalarioVR_TOTAL_OUTRADESPESA: TBCDField;
    cdsDetalheLancNUM_PROTOCOLO: TIntegerField;
    cdsDetalheLancMUTUA: TBCDField;
    cdsDetalheLancACOTERJ: TBCDField;
    cdsFechamentoDATA_REAL_PAGAMENTO_ANT: TDateField;
    cdsSalarioVR_TOTAL_OD: TCurrencyField;
    cdsSalarioVR_TOTAL_OD_A: TCurrencyField;
    cdsSalarioVR_TOTAL_OD_D: TCurrencyField;
    procedure cdsFechamentoVR_TOTAL_SALARIOSGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsSalarioVR_SALARIO_BASEGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsSalarioVR_TOTAL_COMISSAOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsSalarioVR_TOTAL_VALEGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsSalarioVR_SALARIO_PAGOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryComissoesPERCENT_COMISSAOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsSalarioCalcFields(DataSet: TDataSet);
    procedure cdsSalarioAfterScroll(DataSet: TDataSet);
    procedure cdsFechamentoAfterScroll(DataSet: TDataSet);
    procedure cdsComissoesVR_COMISSAOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsValesVR_VALEGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsDetalheLancVR_UNITARIOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsDetalheLancVR_TOTALGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsDetalheLancCalcFields(DataSet: TDataSet);
    procedure cdsSalarioVR_TOTAL_OUTRASDESPESASGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsOutrasDespVR_OUTRADESPESA_FUNCGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsSalarioVR_TOTAL_OUTRADESPESAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsSalarioVR_TOTAL_ODGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsSalarioVR_SALARIO_APAGARGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsSalarioVR_TOTAL_OD_AGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsSalarioVR_TOTAL_OD_DGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CalcularValoresTotais(IdFuncionario: Integer);
  end;

var
  dmFechamentoSalario: TdmFechamentoSalario;

  iMesRef,
  iAnoRef: Integer;

  cTotalComissoes,
  cTotalVales,
  cTotalOutrasDespesasD,
  cTotalOutrasDespesasA: Currency;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{$R *.dfm}

procedure TdmFechamentoSalario.CalcularValoresTotais(IdFuncionario: Integer);
var
  iMes, iAno: Integer;
begin
  //Calcula os Valores Totais das Comissoes, dos Vales e das Outras Despesas com Funcionario
  iMes := 0;
  iAno := 0;

  //Verifica Mes e Ano de Referencia
  if iMesRef < Integer(MonthOf(Date)) then
  begin
    if iMesRef = 12 then
    begin
      iMes := 1;
      iAno := (iAnoRef + 1);
    end
    else
    begin
      iMes := (iMesRef + 1);
      iAno := iAnoRef;
    end;
  end
  else
  begin
    iMes := iMesRef;
    iAno := iAnoRef;
  end;

  //COMISSOES
  cTotalComissoes := 0;

  dmPrincipal.cdsComissoes.Close;
  dmPrincipal.cdsComissoes.Params.ParamByName('MES_REFERENCIA').Value := iMes;
  dmPrincipal.cdsComissoes.Params.ParamByName('ANO_REFERENCIA').Value := iAno;
  dmPrincipal.cdsComissoes.Params.ParamByName('ID_FUNC01').Value      := IdFuncionario;
  dmPrincipal.cdsComissoes.Params.ParamByName('ID_FUNC02').Value      := IdFuncionario;
  dmPrincipal.cdsComissoes.Open;

  dmPrincipal.cdsComissoes.First;

  while not dmPrincipal.cdsComissoes.Eof do
  begin
    cTotalComissoes := (cTotalComissoes + dmPrincipal.cdsComissoes.FieldByName('VR_COMISSAO').AsCurrency);
    dmPrincipal.cdsComissoes.Next;
  end;

  //OUTRAS DESPESAS
  cTotalOutrasDespesasD := 0;
  cTotalOutrasDespesasA := 0;

  dmPrincipal.cdsOutrasDesp.Close;
  dmPrincipal.cdsOutrasDesp.Params.ParamByName('MES_REFERENCIA').Value := iMes;
  dmPrincipal.cdsOutrasDesp.Params.ParamByName('ANO_REFERENCIA').Value := iAno;
  dmPrincipal.cdsOutrasDesp.Params.ParamByName('ID_FUNCIONARIO').Value := IdFuncionario;
  dmPrincipal.cdsOutrasDesp.Open;

  dmPrincipal.cdsOutrasDesp.First;

  while not dmPrincipal.cdsOutrasDesp.Eof do
  begin
    if dmPrincipal.cdsOutrasDesp.FieldByName('FLG_MODALIDADE').AsString = 'D' then
      cTotalOutrasDespesasD := (cTotalOutrasDespesasD + dmPrincipal.cdsOutrasDesp.FieldByName('VR_OUTRADESPESA_FUNC').AsCurrency)
    else if dmPrincipal.cdsOutrasDesp.FieldByName('FLG_MODALIDADE').AsString = 'A' then
      cTotalOutrasDespesasA := (cTotalOutrasDespesasA + dmPrincipal.cdsOutrasDesp.FieldByName('VR_OUTRADESPESA_FUNC').AsCurrency);

    if not cdsSalario.State in [dsInsert, dsEdit] then
    begin
      cdsSalario.FieldByName('VR_TOTAL_OD').AsCurrency   := Abs((cTotalOutrasDespesasA * -1) - (cTotalOutrasDespesasD * -1));
      cdsSalario.FieldByName('VR_TOTAL_OD_A').AsCurrency := cTotalOutrasDespesasA;
      cdsSalario.FieldByName('VR_TOTAL_OD_D').AsCurrency := cTotalOutrasDespesasD;
    end;

    dmPrincipal.cdsOutrasDesp.Next;
  end;

  //VALES
  cTotalVales := 0;

  dmPrincipal.cdsVales.Close;
  dmPrincipal.cdsVales.Params.ParamByName('MES_REFERENCIA').Value := iMes;
  dmPrincipal.cdsVales.Params.ParamByName('ANO_REFERENCIA').Value := iAno;
  dmPrincipal.cdsVales.Params.ParamByName('ID_FUNCIONARIO').Value := IdFuncionario;
  dmPrincipal.cdsVales.Open;

  dmPrincipal.cdsVales.First;

  while not dmPrincipal.cdsVales.Eof do
  begin
    cTotalVales := (cTotalVales + dmPrincipal.cdsVales.FieldByName('VR_VALE').AsCurrency);
    dmPrincipal.cdsVales.Next;
  end;
end;

procedure TdmFechamentoSalario.cdsComissoesVR_COMISSAOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmFechamentoSalario.cdsDetalheLancCalcFields(DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with qryAux, SQL do
  begin
    //Item
    if cdsDetalheLanc.FieldByName('ID_ITEM_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT ID_ITEM, DESCR_ITEM ' +
              '  FROM ITEM ' +
              ' WHERE ID_ITEM = :ID_ITEM';
      Params.ParamByName('ID_ITEM').AsInteger := cdsDetalheLanc.FieldByName('ID_ITEM_FK').AsInteger;
      Open;

      cdsDetalheLanc.FieldByName('DESCR_ITEM').AsString := qryAux.FieldByName('DESCR_ITEM').AsString;
    end;
  end;

  FreeAndNil(qryAux);
end;

procedure TdmFechamentoSalario.cdsDetalheLancVR_TOTALGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmFechamentoSalario.cdsDetalheLancVR_UNITARIOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmFechamentoSalario.cdsFechamentoAfterScroll(DataSet: TDataSet);
begin
  //SALARIO
  cdsSalario.Close;
  cdsSalario.Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := cdsFechamento.FieldByName('ID_FECHAMENTO_SALARIO').AsInteger;
  cdsSalario.Open;
end;

procedure TdmFechamentoSalario.cdsFechamentoVR_TOTAL_SALARIOSGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmFechamentoSalario.cdsOutrasDespVR_OUTRADESPESA_FUNCGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmFechamentoSalario.cdsSalarioAfterScroll(DataSet: TDataSet);
begin
  CalcularValoresTotais(cdsSalario.FieldByName('ID_FUNCIONARIO_FK').AsInteger);
end;

procedure TdmFechamentoSalario.cdsSalarioCalcFields(DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  qryAux := dmGerencial.CriarFDQuery(nil, vgConGER);

  with qryAux, SQL do
  begin
    //Funcionario
    if cdsSalario.FieldByName('ID_FUNCIONARIO_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT NOME_FUNCIONARIO, CPF ' +
              '  FROM FUNCIONARIO ' +
              ' WHERE ID_FUNCIONARIO = :ID_FUNCIONARIO';
      Params.ParamByName('ID_FUNCIONARIO').AsInteger := cdsSalario.FieldByName('ID_FUNCIONARIO_FK').AsInteger;
      Open;

      cdsSalario.FieldByName('FUNCIONARIO').AsString := qryAux.FieldByName('NOME_FUNCIONARIO').AsString;
      cdsSalario.FieldByName('CPF').AsString         := qryAux.FieldByName('CPF').AsString;
    end;
  end;

  FreeAndNil(qryAux);
end;

procedure TdmFechamentoSalario.cdsSalarioVR_SALARIO_APAGARGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmFechamentoSalario.cdsSalarioVR_SALARIO_BASEGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmFechamentoSalario.cdsSalarioVR_SALARIO_PAGOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmFechamentoSalario.cdsSalarioVR_TOTAL_COMISSAOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmFechamentoSalario.cdsSalarioVR_TOTAL_ODGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmFechamentoSalario.cdsSalarioVR_TOTAL_OD_AGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmFechamentoSalario.cdsSalarioVR_TOTAL_OD_DGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmFechamentoSalario.cdsSalarioVR_TOTAL_OUTRADESPESAGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmFechamentoSalario.cdsSalarioVR_TOTAL_OUTRASDESPESASGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmFechamentoSalario.cdsSalarioVR_TOTAL_VALEGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmFechamentoSalario.cdsValesVR_VALEGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmFechamentoSalario.qryComissoesPERCENT_COMISSAOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

end.
