{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroEquivalenciaCarneLeao.pas
  Descricao:   Tela de cadastro de Equivalencias do Carne-Leao
  Author   :   Cristina
  Date:        06-dez-2016
  Last Update: 09-nov-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroEquivalenciaCarneLeao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  UCadastroGeralPadrao, Data.DB, JvExControls, JvButton, JvTransparentButton,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.DBCtrls, Vcl.Grids, Vcl.DBGrids, Vcl.Mask,
  JvExMask, JvToolEdit, JvDBControls, System.StrUtils, FireDAC.Stan.Param,
  FireDAC.Comp.Client, Datasnap.DBClient;

type
  TFCadastroEquivalenciaCarneLeao = class(TFCadastroGeralPadrao)
    dbgEquivalencia: TDBGrid;
    lblCategoria: TLabel;
    rgTipo: TDBRadioGroup;
    lcbCategoria: TDBLookupComboBox;
    lcbSubCategoria: TDBLookupComboBox;
    lcbNatureza: TDBLookupComboBox;
    mmObservacao: TDBMemo;
    lblSubCategoria: TLabel;
    lblNatureza: TLabel;
    lblObservacao: TLabel;
    chbCancelado: TDBCheckBox;
    btnIncluirEquiv: TJvTransparentButton;
    btnEditarEquiv: TJvTransparentButton;
    btnExcluirEquiv: TJvTransparentButton;
    btnConfirmarEquiv: TJvTransparentButton;
    btnCancelarEquiv: TJvTransparentButton;
    lblDataCancelamento: TLabel;
    dteDataCancelamento: TJvDBDateEdit;
    lcbCarneLeao: TDBLookupComboBox;
    lblCarneLeao: TLabel;
    btnIncluirCategoria: TJvTransparentButton;
    btnIncluirSubCategoria: TJvTransparentButton;
    btnIncluirNatureza: TJvTransparentButton;
    procedure FormCreate(Sender: TObject);
    procedure rgTipoClick(Sender: TObject);
    procedure rgTipoExit(Sender: TObject);
    procedure btnExcluirEquivClick(Sender: TObject);
    procedure btnEditarEquivClick(Sender: TObject);
    procedure btnIncluirEquivClick(Sender: TObject);
    procedure btnCancelarEquivClick(Sender: TObject);
    procedure btnConfirmarEquivClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure mmObservacaoKeyPress(Sender: TObject; var Key: Char);
    procedure lcbCategoriaClick(Sender: TObject);
    procedure lcbCategoriaExit(Sender: TObject);
    procedure rgTipoChange(Sender: TObject);
    procedure btnIncluirCategoriaClick(Sender: TObject);
    procedure btnIncluirSubCategoriaClick(Sender: TObject);
    procedure btnIncluirNaturezaClick(Sender: TObject);
    procedure dbgEquivalenciaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure lcbNaturezaExit(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
    iIdIncEquiv: Integer;

    procedure AbrirListaCarneLeao;
    procedure AbrirListaCategorias;
    procedure AbrirListaSubCategorias;
    procedure AbrirListaNaturezas;

    procedure HabDesabBotoesCRUD(Inc, Ed, Exc: Boolean);
    procedure DesabilitaHabilitaCampos(Hab: Boolean);

    function VerificarDadosEquivalencia(var Msg: String; var QtdErros: Integer): Boolean;
  public
    { Public declarations }
  end;

var
  FCadastroEquivalenciaCarneLeao: TFCadastroEquivalenciaCarneLeao;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UDMCarneLeao, UGDM, UVariaveisGlobais,
  UCadastroCategoriaDespRec, UCadastroSubCategoriaDespRec, UCadastroNatureza;

{ TFCadastroEquivalenciaCarneLeao }

procedure TFCadastroEquivalenciaCarneLeao.AbrirListaCarneLeao;
begin
  dmCarneLeao.qryClassif.Close;
  dmCarneLeao.qryClassif.Params.ParamByName('TIPO').Value := IfThen(rgTipo.ItemIndex = 0, 'R', 'D');
  dmCarneLeao.qryClassif.Open;
end;

procedure TFCadastroEquivalenciaCarneLeao.AbrirListaCategorias;
begin
  dmCarneLeao.qryCategoria.Close;
  dmCarneLeao.qryCategoria.Params.ParamByName('TIPO').Value           := IfThen(rgTipo.ItemIndex = 0, 'R', 'D');
  dmCarneLeao.qrySubCategoria.Params.ParamByName('ID_NATUREZA').Value := dmCarneLeao.qryNatureza.FieldByName('ID').AsInteger;
  dmCarneLeao.qryCategoria.Open;
end;

procedure TFCadastroEquivalenciaCarneLeao.AbrirListaNaturezas;
begin
  dmCarneLeao.qryNatureza.Close;
  dmCarneLeao.qryNatureza.Params.ParamByName('TIPO').Value := IfThen(rgTipo.ItemIndex = 0, 'R', 'D');
  dmCarneLeao.qryNatureza.Open;
end;

procedure TFCadastroEquivalenciaCarneLeao.AbrirListaSubCategorias;
begin
  dmCarneLeao.qrySubCategoria.Close;
  dmCarneLeao.qrySubCategoria.Params.ParamByName('TIPO').Value                 := IfThen(rgTipo.ItemIndex = 0, 'R', 'D');
  dmCarneLeao.qrySubCategoria.Params.ParamByName('ID_CATEGORIA_DESPREC').Value := dmCarneLeao.qryCategoria.FieldByName('ID').AsInteger;
  dmCarneLeao.qrySubCategoria.Open;
end;

procedure TFCadastroEquivalenciaCarneLeao.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroEquivalenciaCarneLeao.btnCancelarEquivClick(
  Sender: TObject);
begin
  inherited;

  dmCarneLeao.cdsEquiv_F.Cancel;

  if dmCarneLeao.cdsEquiv_F.RecordCount = 0 then
    HabDesabBotoesCRUD(True, False, False)
  else
    HabDesabBotoesCRUD(True, True, True);

  DesabilitaHabilitaCampos(False);
end;

procedure TFCadastroEquivalenciaCarneLeao.btnConfirmarEquivClick(
  Sender: TObject);
var
  Msg: String;
  iCount, Qtd: Integer;
  lInserindo:  Boolean;
begin
  inherited;

  lblMensagemErro.Caption := '  Mensagem:';
  lbMensagemErro.Color    := clWindow;
  lbMensagemErro.Clear;

  Msg    := '';
  iCount := 0;
  Qtd    := 0;
  lInserindo := False;

  if not VerificarDadosEquivalencia(Msg, Qtd) then
  begin
    case Qtd of
      0:
      begin
        lblMensagemErro.Caption := '  Mensagem:';
        lbMensagemErro.Color    := clWindow;
      end;
      1:
      begin
        lblMensagemErro.Caption := '  Durante a verifica��o dos dados, o seguinte erro foi encontrado:';
        lbMensagemErro.Color    := clInfoBk;
      end
      else
      begin
        lblMensagemErro.Caption := '  Durante a verifica��o dos dados, os seguintes erros foram encontrados:';
        lbMensagemErro.Color    := clInfoBk;
      end;
    end;

    for iCount := 0 to Pred(ListaMsgErro.Count) do
      lbMensagemErro.Items.Add(ListaMsgErro[iCount].Mensagem);
  end
  else
  begin
    if dmCarneLeao.cdsEquiv_F.State = dsInsert then
    begin
      Inc(iIdIncEquiv);
      dmCarneLeao.cdsEquiv_F.FieldByName('ID_CARNELEAO_EQUIVALENCIA').AsInteger := (iIdIncEquiv + 900000);
      lInserindo := True;
    end;

    dmCarneLeao.cdsEquiv_F.Post;

    HabDesabBotoesCRUD(True, True, True);

    if lInserindo then
    begin
      if vgOperacao = E then
        DesabilitaHabilitaCampos(False)
      else
      begin
        if Application.MessageBox('Deseja incluir outra Equival�ncia?', 'Confirma��o', MB_YESNO + MB_ICONQUESTION) = ID_YES then
        begin
          DesabilitaHabilitaCampos(True);
          btnIncluirEquiv.Click;
        end
        else
          DesabilitaHabilitaCampos(False);
      end;
    end
    else
      DesabilitaHabilitaCampos(False);
  end;
end;

procedure TFCadastroEquivalenciaCarneLeao.btnEditarEquivClick(Sender: TObject);
begin
  inherited;

  if dmCarneLeao.cdsEquiv_F.RecordCount > 0 then
  begin
    dmCarneLeao.cdsEquiv_F.Edit;
    HabDesabBotoesCRUD(False, False, False);
    DesabilitaHabilitaCampos(True);
  end;
end;

procedure TFCadastroEquivalenciaCarneLeao.btnExcluirEquivClick(Sender: TObject);
var
  QryLanc: TFDQuery;
begin
  inherited;

  if dmCarneLeao.cdsEquiv_F.RecordCount > 0 then
  begin
    if dmCarneLeao.cdsEquiv_F.FieldByName('NOVO').AsBoolean then
      dmCarneLeao.cdsEquiv_F.Delete
    else
    begin
      QryLanc := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

      QryLanc.Close;
      QryLanc.SQL.Clear;
      QryLanc.SQL.Text := 'SELECT ID_CARNELEAO_EQUIVALENCIA_FK ' +
                          '  FROM LANCAMENTO ' +
                          ' WHERE ID_CARNELEAO_EQUIVALENCIA_FK = :ID_CARNELEAO_EQUIVALENCIA';
      QryLanc.Params.ParamByName('ID_CARNELEAO_EQUIVALENCIA').Value := dmCarneLeao.cdsEquiv_F.FieldByName('ID_CARNELEAO_EQUIVALENCIA').AsInteger;
      QryLanc.Open;

      if QryLanc.RecordCount > 0 then
      begin
        Application.MessageBox('N�o � poss�vel excluir essa Equival�ncia, pois a mesma j� foi utilizada em um Lan�amento. Portanto, a mesma ser� desativada.',
                               'Aviso',
                               MB_OK + MB_ICONWARNING);

        dmCarneLeao.cdsEquiv_F.Edit;
        dmCarneLeao.cdsEquiv_F.FieldByName('FLG_CANCELADO').AsString       := 'S';
        dmCarneLeao.cdsEquiv_F.FieldByName('DATA_CANCELAMENTO').AsDateTime := Date;
        dmCarneLeao.cdsEquiv_F.FieldByName('CANCEL_ID_USUARIO').AsInteger  := vgUsu_Id;
        dmCarneLeao.cdsEquiv_F.Post;
      end
      else
        dmCarneLeao.cdsEquiv_F.Delete;

      FreeAndNil(QryLanc);
    end;
  end;
end;

procedure TFCadastroEquivalenciaCarneLeao.btnIncluirCategoriaClick(
  Sender: TObject);
var
  Op: TOperacao;
  IdCons: Integer;
  OrigF, OrigC: Boolean;
begin
  inherited;

  Op     := vgOperacao;
  IdCons := vgIdConsulta;
  OrigF  := vgOrigemFiltro;
  OrigC  := vgOrigemCadastro;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := False;
  vgOrigemCadastro := True;

  try
    Application.CreateForm(TFCadastroCategoriaDespRec, FCadastroCategoriaDespRec);

    TipoCat := IfThen((rgTipo.ItemIndex = 0), 'R', 'D');

    FCadastroCategoriaDespRec.ShowModal;
  finally
    AbrirListaCategorias;

    if FCadastroCategoriaDespRec.IdNovaCategoria <> 0 then
      dmCarneLeao.cdsEquiv_F.FieldByName('M_ID_CATEGORIA_DESPREC_FK').AsInteger := FCadastroCategoriaDespRec.IdNovaCategoria;

    FCadastroCategoriaDespRec.Free;
  end;

  vgOperacao       := Op;
  vgIdConsulta     := IdCons;
  vgOrigemFiltro   := OrigF;
  vgOrigemCadastro := OrigC;

  if lcbCategoria.KeyValue <> Null then
  begin
    lcbSubCategoria.Enabled        := True;
    lcbSubCategoria.Enabled        := True;
    btnIncluirSubCategoria.Enabled := True;

    AbrirListaSubCategorias;

    if lcbSubCategoria.CanFocus then
      lcbSubCategoria.SetFocus;
  end;
end;

procedure TFCadastroEquivalenciaCarneLeao.btnIncluirEquivClick(Sender: TObject);
begin
  inherited;

  dmCarneLeao.cdsEquiv_F.Append;
  dmCarneLeao.cdsEquiv_F.FieldByName('NOVO').AsBoolean              := True;
  dmCarneLeao.cdsEquiv_F.FieldByName('M_TIPO_LANCAMENTO').AsString  := 'D';
  dmCarneLeao.cdsEquiv_F.FieldByName('FLG_CANCELADO').AsString      := 'N';
  dmCarneLeao.cdsEquiv_F.FieldByName('CAD_ID_USUARIO').AsInteger    := vgUsu_Id;
  dmCarneLeao.cdsEquiv_F.FieldByName('DATA_CADASTRO').AsDateTime    := Date;

  HabDesabBotoesCRUD(False, False, False);
  DesabilitaHabilitaCampos(True);

  if rgTipo.CanFocus then
    rgTipo.SetFocus;
end;

procedure TFCadastroEquivalenciaCarneLeao.btnIncluirNaturezaClick(
  Sender: TObject);
var
  Op: TOperacao;
  IdCons: Integer;
  OrigF, OrigC: Boolean;
begin
  inherited;

  Op     := vgOperacao;
  IdCons := vgIdConsulta;
  OrigF  := vgOrigemFiltro;
  OrigC  := vgOrigemCadastro;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := False;
  vgOrigemCadastro := True;

  try
    Application.CreateForm(TFCadastroNatureza, FCadastroNatureza);

    if rgTipo.ItemIndex = 0 then
      FCadastroNatureza.TipoNat := REC
    else if rgTipo.ItemIndex = 1 then
      FCadastroNatureza.TipoNat := DESP;

    FCadastroNatureza.ShowModal;
  finally
    AbrirListaNaturezas;

    if FCadastroNatureza.IdNovaNatureza <> 0 then
      dmCarneLeao.cdsEquiv_F.FieldByName('M_ID_NATUREZA_FK').AsInteger := FCadastroNatureza.IdNovaNatureza;

    FCadastroNatureza.Free;
  end;

  vgOperacao       := Op;
  vgIdConsulta     := IdCons;
  vgOrigemFiltro   := OrigF;
  vgOrigemCadastro := OrigC;

  if lcbNatureza.KeyValue <> Null then
  begin
    lcbCategoria.Enabled        := True;
    lcbCategoria.Enabled        := True;
    btnIncluirCategoria.Enabled := True;

    AbrirListaCategorias;

    if lcbCategoria.CanFocus then
      lcbCategoria.SetFocus;
  end;
end;

procedure TFCadastroEquivalenciaCarneLeao.btnIncluirSubCategoriaClick(
  Sender: TObject);
var
  Op: TOperacao;
  IdCons: Integer;
  OrigF, OrigC: Boolean;
begin
  inherited;

  Op     := vgOperacao;
  IdCons := vgIdConsulta;
  OrigF  := vgOrigemFiltro;
  OrigC  := vgOrigemCadastro;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := False;
  vgOrigemCadastro := True;

  try
    Application.CreateForm(TFCadastroSubCategoriaDespRec, FCadastroSubCategoriaDespRec);

    TipoSubCat := IfThen((rgTipo.ItemIndex = 0), 'R', 'D');

    FCadastroSubCategoriaDespRec.ShowModal;
  finally
    AbrirListaSubCategorias;

    if FCadastroSubCategoriaDespRec.IdNovaSubCategoria <> 0 then
      dmCarneLeao.cdsEquiv_F.FieldByName('M_ID_SUBCATEGORIA_DESPREC_FK').AsInteger := FCadastroSubCategoriaDespRec.IdNovaSubCategoria;

    FCadastroSubCategoriaDespRec.Free;
  end;

  vgOperacao       := Op;
  vgIdConsulta     := IdCons;
  vgOrigemFiltro   := OrigF;
  vgOrigemCadastro := OrigC;

  if lcbSubCategoria.KeyValue <> Null then
  begin
    if chbCancelado.CanFocus then
      chbCancelado.SetFocus;
  end;
end;

procedure TFCadastroEquivalenciaCarneLeao.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroEquivalenciaCarneLeao.dbgEquivalenciaDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;

  AlternarCorGrid(Sender, Rect, DataCol, Column, State);
end;

procedure TFCadastroEquivalenciaCarneLeao.DefinirTamanhoMaxCampos;
begin
  inherited;

  mmObservacao.MaxLength := 300;
end;

procedure TFCadastroEquivalenciaCarneLeao.DesabilitaHabilitaCampos(
  Hab: Boolean);
begin
  rgTipo.Enabled                 := Hab;
  lcbCarneLeao.Enabled           := Hab;
  lcbCategoria.Enabled           := Hab;
  btnIncluirCategoria.Enabled    := Hab;
  lcbSubCategoria.Enabled        := Hab;
  btnIncluirSubCategoria.Enabled := Hab;
  lcbNatureza.Enabled            := Hab;
  btnIncluirNatureza.Enabled     := Hab;
  mmObservacao.ReadOnly          := not Hab;
  chbCancelado.Enabled           := Hab;

  dbgEquivalencia.ReadOnly := Hab;
end;

procedure TFCadastroEquivalenciaCarneLeao.DesabilitarComponentes;
begin
  inherited;

  dteDataCancelamento.Enabled := False;
end;

procedure TFCadastroEquivalenciaCarneLeao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  vgOperacao     := VAZIO;
  vgIdConsulta   := 0;
  vgOrigemFiltro := False;

  inherited;
end;

procedure TFCadastroEquivalenciaCarneLeao.FormCreate(Sender: TObject);
begin
  inherited;

  dmCarneLeao.cdsEquiv_F.Close;
  dmCarneLeao.cdsEquiv_F.Open;

  AbrirListaCarneLeao;
end;

procedure TFCadastroEquivalenciaCarneLeao.FormShow(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
  begin
    if dmCarneLeao.cdsEquiv_F.RecordCount = 0 then
      HabDesabBotoesCRUD(True, False, False)
    else
      HabDesabBotoesCRUD(True, True, True);

    DesabilitaHabilitaCampos(False);
  end;

  ShowScrollBar(dbgEquivalencia.Handle, SB_HORZ, False);
end;

function TFCadastroEquivalenciaCarneLeao.GravarDadosAto(
  var Msg: String): Boolean;
begin
  //
end;

function TFCadastroEquivalenciaCarneLeao.GravarDadosGenerico(
  var Msg: String): Boolean;
var
  IdEquivalencia: Integer;
begin
  Result := True;
  Msg := '';

  IdEquivalencia := 0;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    if dmCarneLeao.cdsEquiv_F.RecordCount > 0 then
    begin
      IdEquivalencia := BS.ProximoId('ID_CARNELEAO_EQUIVALENCIA', 'CARNELEAO_EQUIVALENCIA');

      dmCarneLeao.cdsEquiv_F.First;

      while not dmCarneLeao.cdsEquiv_F.Eof do
      begin
        if dmCarneLeao.cdsEquiv_F.FieldByName('NOVO').AsBoolean then
        begin
          dmCarneLeao.cdsEquiv_F.Edit;
          dmCarneLeao.cdsEquiv_F.FieldByName('ID_CARNELEAO_EQUIVALENCIA').AsInteger := IdEquivalencia;

          dmCarneLeao.cdsEquiv_F.Post;

          Inc(IdEquivalencia);
        end;

        dmCarneLeao.cdsEquiv_F.Next;
      end;

      dmCarneLeao.cdsEquiv_F.ApplyUpdates(0);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    Msg := 'Dados da Equival�ncia gravados com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na grava��o do Dados da Equival�ncia.';
  end;
end;

procedure TFCadastroEquivalenciaCarneLeao.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta de Equival�ncia');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclus�o de Equival�ncia');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO',
                                  'Por favor, indique o motivo da edi��o dessa Equival�ncia:',
                                  '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Equival�ncia';

      BS.GravarUsuarioLog(2, '', sObservacao);
    end;
  end;
end;

procedure TFCadastroEquivalenciaCarneLeao.HabDesabBotoesCRUD(Inc, Ed,
  Exc: Boolean);
begin
  btnIncluirEquiv.Enabled   := vgPrm_ConfRelacCL and Inc;
  btnEditarEquiv.Enabled    := vgPrm_ConfRelacCL and Ed;
  btnExcluirEquiv.Enabled   := vgPrm_ConfRelacCL and Exc;
  btnConfirmarEquiv.Enabled := vgPrm_ConfRelacCL and (not (Inc or Ed or Exc));
  btnCancelarEquiv.Enabled  := vgPrm_ConfRelacCL and (not (Inc or Ed or Exc));

  btnOk.Enabled       := (Inc or Ed or Exc);
  btnCancelar.Enabled := (Inc or Ed or Exc);
end;

procedure TFCadastroEquivalenciaCarneLeao.lbMensagemErroDblClick(
  Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroEquivalenciaCarneLeao.lcbCategoriaClick(Sender: TObject);
begin
  inherited;

  AbrirListaSubCategorias;
end;

procedure TFCadastroEquivalenciaCarneLeao.lcbCategoriaExit(Sender: TObject);
begin
  inherited;

  AbrirListaSubCategorias;
end;

procedure TFCadastroEquivalenciaCarneLeao.lcbNaturezaExit(Sender: TObject);
begin
  inherited;

  AbrirListaNaturezas;
end;

procedure TFCadastroEquivalenciaCarneLeao.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

procedure TFCadastroEquivalenciaCarneLeao.mmObservacaoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnConfirmarEquiv.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    btnCancelar.Click;
end;

procedure TFCadastroEquivalenciaCarneLeao.rgTipoChange(Sender: TObject);
begin
  inherited;

  if dmCarneLeao.cdsEquiv_F.State = dsBrowse then
  begin
    AbrirListaCarneLeao;
    AbrirListaNaturezas;
    AbrirListaCategorias;
    AbrirListaSubCategorias;
  end;
end;

procedure TFCadastroEquivalenciaCarneLeao.rgTipoClick(Sender: TObject);
begin
  inherited;

  AbrirListaCarneLeao;
  AbrirListaNaturezas;
  AbrirListaCategorias;
end;

procedure TFCadastroEquivalenciaCarneLeao.rgTipoExit(Sender: TObject);
begin
  inherited;

  AbrirListaCarneLeao;
  AbrirListaNaturezas;
  AbrirListaCategorias;
end;

function TFCadastroEquivalenciaCarneLeao.VerificarDadosEquivalencia(
  var Msg: String; var QtdErros: Integer): Boolean;
begin
  Result   := True;
  Msg      := '';
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if rgTipo.ItemIndex < 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o TIPO.';
    DadosMsgErro.Componente := rgTipo;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if lcbCarneLeao.KeyValue = Null then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o CARN�-LE�O.';
    DadosMsgErro.Componente := lcbCarneLeao;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if (lcbCategoria.KeyValue = Null) and
    (lcbSubCategoria.KeyValue = Null) and
    (lcbNatureza.KeyValue = Null) then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') � necess�rio informar uma CATEGORIA, SUBCATEGORIA e/ou NATUREZA.';
    DadosMsgErro.Componente := lcbCategoria;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if QtdErros > 0 then
    Result := False;
end;

function TFCadastroEquivalenciaCarneLeao.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result := True;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if dmCarneLeao.cdsEquiv_F.RecordCount = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Nenhuma EQUIVAL�NCIA FOI CADASTRADA.';
    DadosMsgErro.Componente := rgTipo;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if QtdErros > 0 then
    Result := False;
end;

function TFCadastroEquivalenciaCarneLeao.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

end.

