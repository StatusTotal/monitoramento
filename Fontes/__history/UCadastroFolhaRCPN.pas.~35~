{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroFolhaRCPN.pas
  Descricao:   Formulario de cadastro de Folha RCPN
  Author   :   Pedro
  Date:        19-jun-2017
  Last Update: 28-ago-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroFolhaRCPN;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Mask, sMaskEdit, sCustomComboEdit, sToolEdit, sDBDateEdit, Vcl.DBCtrls,
  sDBEdit, sDBMemo, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  sDBComboBox;

type
  TFCadastroFolhaRCPN = class(TFCadastroGeralPadrao)
    edDataPratica: TsDBDateEdit;
    edFolha: TsDBEdit;
    edLetra: TsDBEdit;
    edNumero: TsDBEdit;
    edAleatorio: TsDBEdit;
    MMObs: TsDBMemo;
    lcbNomeFuncionario: TDBLookupComboBox;
    lblNomeFuncionario: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure MMObsKeyPress(Sender: TObject; var Key: Char);
    procedure lbMensagemErroDblClick(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadastroFolhaRCPN: TFCadastroFolhaRCPN;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMFolhaSeguranca;

procedure TFCadastroFolhaRCPN.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroFolhaRCPN.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroFolhaRCPN.DefinirTamanhoMaxCampos;
begin
  inherited;
//
end;

procedure TFCadastroFolhaRCPN.DesabilitarComponentes;
begin
  inherited;

  if vgOperacao = C then
    pnlDados.Enabled := False;
end;

procedure TFCadastroFolhaRCPN.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dmFolhaSeguranca.cdsFolha.Cancel;

  vgOperacao     := VAZIO;
  vgIdConsulta   := 0;
  vgOrigemFiltro := False;

  inherited;
end;

procedure TFCadastroFolhaRCPN.FormCreate(Sender: TObject);
begin
  inherited;

  dmFolhaSeguranca.qryFuncionarios.Close;
  dmFolhaSeguranca.qryFuncionarios.Open;

  dmFolhaSeguranca.cdsFolha.Close;
  dmFolhaSeguranca.cdsFolha.Params.ParamByName('ID_FOLHASEGURANCA').AsInteger := vgIdConsulta;
  dmFolhaSeguranca.cdsFolha.Open;

  if vgOperacao=I then
  begin
    dmFolhaSeguranca.cdsFolha.Append;
    dmFolhaSeguranca.cdsFolha.FieldByName('FLG_RCPN').AsString        := 'N';
    dmFolhaSeguranca.cdsFolha.FieldByName('FLG_SITUACAO').AsString    := 'N';
    dmFolhaSeguranca.cdsFolha.FieldByName('CAD_ID_USUARIO').AsInteger := vgUsu_Id;
    dmFolhaSeguranca.cdsFolha.FieldByName('ID_SISTEMA_FK').AsInteger  := 5;
  end;

  if vgOperacao = E then
    dmFolhaSeguranca.cdsFolha.Edit;
end;

function TFCadastroFolhaRCPN.GravarDadosAto(var Msg: String): Boolean;
begin
//
end;

function TFCadastroFolhaRCPN.GravarDadosGenerico(var Msg: String): Boolean;
begin
  Result := True;
  Msg := '';

  if vgOperacao = C then
    Exit;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    if dmGerencial.Pergunta('CONFIRMA A UTILIZA��O: ' + #13#13 +
                            'DATA PR�TICA: ' + FormatDateTime('dd/mm/yyyy', edDataPratica.Date) + #13 +
                            'SELO: ' + edLetra.Text + edNumero.Text + ' ' + edAleatorio.Text) then
    begin
      dmFolhaSeguranca.cdsFolhaID_FUNCIONARIO_FK.AsInteger := lcbNomeFuncionario.KeyValue;
      dmFolhaSeguranca.cdsFolhaFLG_SITUACAO.AsString       := 'S';
      dmFolhaSeguranca.cdsFolha.Post;
      dmFolhaSeguranca.cdsFolha.ApplyUpdates(0);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    Msg := 'Folha de Seguran�a ' + edFolha.Text + ' gravada com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na grava��o da Folha de Seguran�a ' + edFolha.Text + '.';
  end;
end;

procedure TFCadastroFolhaRCPN.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(4, '', 'Somente consulta de Dados de Folha de Seguran�a',
                          vgIdConsulta, 'FOLHASEGURANCA');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(4, '', 'Somente inclus�o de Dados Folha de Seguran�a',
                          vgIdConsulta, 'FOLHASEGURANCA');
    end;
    E:  //EDICAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da edi��o desse Folha de Seguran�a:', '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(4, '', sObservacao,
                          vgIdConsulta, 'FOLHASEGURANCA');
    end;
  end;
end;

procedure TFCadastroFolhaRCPN.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroFolhaRCPN.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
//
end;

procedure TFCadastroFolhaRCPN.MMObsKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

function TFCadastroFolhaRCPN.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if edDataPratica.Date = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha o campo Data.';
    DadosMsgErro.Componente := edDataPratica;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Length(edLetra.Text) < 4 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo Letra.';
    DadosMsgErro.Componente := edletra;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Length(edNumero.Text) < 5 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo N�mero.';
    DadosMsgErro.Componente := edNumero;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Length(edAleatorio.Text) < 3 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo Aleat�rio.';
    DadosMsgErro.Componente := edAleatorio;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if (lcbNomeFuncionario.KeyValue = -1) then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo Nome do Funcion�rio.';
    DadosMsgErro.Componente := lcbNomeFuncionario;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroFolhaRCPN.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
//
end;

end.
