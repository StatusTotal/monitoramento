{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UOpcoesAcaoVinculoDeposito.pas
  Descricao:   Tela de Opcoes de Acao com Sobra ou Falta durante Vinculo de Deposito
  Author   :   Cristina
  Date:        07-dez-2017
  Last Update: 19-dez-2017 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UOpcoesAcaoVinculoDeposito;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UOpcoesAcaoPadrao, JvExControls,
  JvButton, JvTransparentButton, Vcl.ExtCtrls, Vcl.StdCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Menus, JvMenus;

type
  TFOpcoesAcaoVinculoDeposito = class(TFOpcoesAcaoPadrao)
    btnOpcao6: TJvTransparentButton;
    btnOpcao5: TJvTransparentButton;
    btnOpcao4: TJvTransparentButton;
    btnOpcao3: TJvTransparentButton;
    pnlValores: TPanel;
    lblSobra: TLabel;
    lblFalta: TLabel;
    ppmDesmembrar: TJvPopupMenu;
    ppmJuros: TJvPopupMenu;
    ppmNovaParc: TJvPopupMenu;
    ppmDesconto: TJvPopupMenu;
    procedure FormShow(Sender: TObject);
    procedure btnOpcao3Click(Sender: TObject);
    procedure btnOpcao6Click(Sender: TObject);
    procedure btnOpcao5Click(Sender: TObject);
    procedure btnOpcao1Click(Sender: TObject);
    procedure btnOpcao4Click(Sender: TObject);
    procedure btnOpcao2Click(Sender: TObject);
  private
    { Private declarations }

    IdNat, Cod: Integer;

    Descr, Obs, sIR, sCP, sLA: String;

    miItensDesmembramento,
    miItensJuros,
    miItensNovaParcela,
    miItensDesconto: TMenuItem;

    procedure LancarSobraFaltaCaixa;
    procedure LancarJurosDescontoParcela(Sender: TObject);
    procedure AbrirDesmembramentoDeposito(Sender: TObject);
    procedure CriarNovaParcela(Sender: TObject);
  public
    { Public declarations }

    lGravarVinculos: Boolean;

    dDataVinculo: TDatetime;

    sLstDepo: String;
  end;

var
  FOpcoesAcaoVinculoDeposito: TFOpcoesAcaoVinculoDeposito;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDesmembramentoDeposito,
  UDMBaixa;

procedure TFOpcoesAcaoVinculoDeposito.AbrirDesmembramentoDeposito(
  Sender: TObject);
begin
  //Desmembramento de Deposito
  if dmBaixa.cdsDepos.Locate('ID_DEPOSITO_FLUTUANTE', TMenuItem(Sender).Tag, []) then
  begin
    try
      Application.CreateForm(TFDesmembramentoDeposito, FDesmembramentoDeposito);

      FDesmembramentoDeposito.iIdDeposFlutP   := dmBaixa.cdsDepos.FieldByName('ID_DEPOSITO_FLUTUANTE').AsInteger;
      FDesmembramentoDeposito.cValorUtilizado := (dmBaixa.cdsDepos.FieldByName('VR_DEPOSITO_FLUTUANTE').AsCurrency -
                                                  dmBaixa.cDifTot);

      FDesmembramentoDeposito.ShowModal;
    finally
      lGravarVinculos := FDesmembramentoDeposito.lGravarVinculos;
      FDesmembramentoDeposito.Free;
    end;

    Self.Close;
  end;
end;

procedure TFOpcoesAcaoVinculoDeposito.btnOpcao1Click(Sender: TObject);
begin
  inherited;

  //Lancamento de Sobra de Caixa
  LancarSobraFaltaCaixa;
end;

procedure TFOpcoesAcaoVinculoDeposito.btnOpcao2Click(Sender: TObject);
begin
  inherited;

  //pop-up
end;

procedure TFOpcoesAcaoVinculoDeposito.btnOpcao3Click(Sender: TObject);
begin
  inherited;

  //pou-up
end;

procedure TFOpcoesAcaoVinculoDeposito.btnOpcao4Click(Sender: TObject);
begin
  inherited;

  //Lancamento de Falta de Caixa
  LancarSobraFaltaCaixa;
end;

procedure TFOpcoesAcaoVinculoDeposito.btnOpcao5Click(Sender: TObject);
begin
  inherited;

  //pop-up
end;

procedure TFOpcoesAcaoVinculoDeposito.btnOpcao6Click(Sender: TObject);
begin
  inherited;

  //pop-up
end;

procedure TFOpcoesAcaoVinculoDeposito.CriarNovaParcela(Sender: TObject);
begin
  //Lancamento de Nova Parcela
  if dmBaixa.cdsLanc.Locate('ID_LANCAMENTO_PARC', TMenuItem(Sender).Tag, []) then
  begin
    dmPrincipal.InicializarComponenteLancamento;

    //Lancamento
    DadosLancamento := Lancamento.Create;

    DadosLancamento.CodLancamento := dmBaixa.cdsLanc.FieldByName('COD_LANCAMENTO').AsInteger;
    DadosLancamento.AnoLancamento := dmBaixa.cdsLanc.FieldByName('ANO_LANCAMENTO').AsInteger;
    DadosLancamento.QtdParcelas   := (dmBaixa.cdsLanc.FieldByName('QTD_PARCELAS').AsInteger + 1);

    ListaLancamentos.Add(DadosLancamento);

    dmPrincipal.UpdateLancamento(0);

    //Parcela Anterior
    DadosLancamentoParc := LancamentoParc.Create;

    DadosLancamentoParc.CodLancamento    := dmBaixa.cdsLanc.FieldByName('COD_LANCAMENTO').AsInteger;
    DadosLancamentoParc.AnoLancamento    := dmBaixa.cdsLanc.FieldByName('ANO_LANCAMENTO').AsInteger;
    DadosLancamentoParc.IdLancamentoParc := dmBaixa.cdsLanc.FieldByName('ID_LANCAMENTO_PARC').AsInteger;
    DadosLancamentoParc.VlrPrevisto      := (dmBaixa.cdsLanc.FieldByName('VR_PARCELA_PREV').AsCurrency -
                                             dmBaixa.cDifTot);
    DadosLancamentoParc.FlgStatus        := 'G';
    DadosLancamentoParc.DataPagamento    := dDataVinculo;
    DadosLancamentoParc.PagIdUsuario     := vgUsu_Id;
    DadosLancamentoParc.VlrPago          := ((DadosLancamentoParc.VlrPrevisto +
                                              dmBaixa.cdsLanc.FieldByName('VR_PARCELA_JUROS').AsCurrency) -
                                             dmBaixa.cdsLanc.FieldByName('VR_PARCELA_DESCONTO').AsCurrency);

    ListaLancamentoParcs.Add(DadosLancamentoParc);

    dmPrincipal.UpdateLancamentoParc(0, 0);

    //Parcela Nova
    DadosLancamentoParc := LancamentoParc.Create;

    DadosLancamentoParc.DataLancParc   := dmBaixa.cdsLanc.FieldByName('DATA_LANCAMENTO_PARC').AsDateTime;
    DadosLancamentoParc.CodLancamento  := dmBaixa.cdsLanc.FieldByName('COD_LANCAMENTO').AsInteger;
    DadosLancamentoParc.AnoLancamento  := dmBaixa.cdsLanc.FieldByName('ANO_LANCAMENTO').AsInteger;
    DadosLancamentoParc.NumParcela     := (dmBaixa.cdsLanc.FieldByName('QTD_PARCELAS').AsInteger + 1);
    DadosLancamentoParc.DataVencimento := IncMonth(Date, 1);
    DadosLancamentoParc.VlrPrevisto    := dmBaixa.cDifTot;
    DadosLancamentoParc.IdFormaPagto   := dmBaixa.cdsLanc.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger;
    DadosLancamentoParc.FlgStatus      := 'P';  //dmBaixa.cdsLanc.FieldByName('FLG_STATUS').AsString;

{    if Trim(dmBaixa.cdsLanc.FieldByName('FLG_STATUS').AsString) = 'G' then
    begin
      DadosLancamentoParc.VlrPago      := dmBaixa.cDifTot;
      DadosLancamentoParc.PagIdUsuario := dmBaixa.cdsLanc.FieldByName('PAG_ID_USUARIO').AsInteger;
    end;  }

    DadosLancamentoParc.CadIdUsuario   := vgUsu_Id;
    DadosLancamentoParc.DataCadastro   := Now;
    DadosLancamentoParc.Observacao     := 'Lan�amento gerado por V�nculo de Parcela a Dep�sito (Parcela N� ' +
                                          dmBaixa.cdsLanc.FieldByName('NUM_PARCELA').AsString +
                                          ' - Recibo: ' + dmBaixa.cdsLanc.FieldByName('NUM_RECIBO').AsString;

    ListaLancamentoParcs.Add(DadosLancamentoParc);

    dmPrincipal.InsertLancamentoParc(1, 0);

    dmPrincipal.FinalizarComponenteLancamento;

    lGravarVinculos := True;

    Self.Close;
  end;
end;

procedure TFOpcoesAcaoVinculoDeposito.FormShow(Sender: TObject);
var
  iCont: Integer;
begin
  inherited;

  btnOpcao1.Enabled  := (dmBaixa.sTipoBaixa = 'S');   //Lancamento de Sobra de Caixa
  btnOpcao2.Enabled  := (dmBaixa.sTipoBaixa = 'S') and vgPrm_IncDepo;  //Desmembramento de Deposito
  btnOpcao3.Enabled  := (dmBaixa.sTipoBaixa = 'S');   //Lancamento de Juros em Parcela
  btnOpcao4.Enabled  := (dmBaixa.sTipoBaixa = 'F');  //Lancamento de Falta de Caixa
  btnOpcao5.Enabled  := (dmBaixa.sTipoBaixa = 'F');   //Lancamento de Nova Parcela
  btnOpcao6.Enabled  := (dmBaixa.sTipoBaixa = 'F');  //Lancamento de Desconto de Parcela

  if dmBaixa.sTipoBaixa = 'S' then
    lblSobra.Caption := 'Sobra: R$' + FloatToStrF(dmBaixa.cDifTot, ffCurrency, 10, 2)
  else if dmBaixa.sTipoBaixa = 'F' then
    lblFalta.Caption := 'Falta: R$' + FloatToStrF(dmBaixa.cDifTot, ffCurrency, 10, 2);

  //PREENCHIMENTO DOS SUBMENUS
  //Sobra
  if dmBaixa.sTipoBaixa = 'S' then
  begin
    //Juros
    iCont := 1;

    dmBaixa.cdsLanc.DisableControls;

    dmBaixa.cdsLanc.First;

    while not dmBaixa.cdsLanc.Eof do
    begin
      if dmBaixa.cdsLanc.FieldByName('SELECIONADO').AsBoolean then
      begin
        miItensJuros := TMenuItem.Create(ppmJuros);

        ppmJuros.Items.Add(miItensJuros);

        miItensJuros.Caption := dmBaixa.cdsLanc.FieldByName('NUM_RECIBO').AsString + ' (' +
                                FloatToStrF(dmBaixa.cdsLanc.FieldByName('VR_PARCELA_PREV').AsFloat, ffCurrency, 10, 2) + ')';
        miItensJuros.Tag     := dmBaixa.cdsLanc.FieldByName('ID_LANCAMENTO_PARC').AsInteger;
        miItensJuros.OnClick := LancarJurosDescontoParcela;

        Inc(iCont);
      end;

      dmBaixa.cdsLanc.Next;
    end;

    dmBaixa.cdsLanc.EnableControls;

    //Desmembramento
    iCont := 1;

    dmBaixa.cdsDepos.DisableControls;

    dmBaixa.cdsDepos.First;

    while not dmBaixa.cdsDepos.Eof do
    begin
      if dmBaixa.cdsDepos.FieldByName('SELECIONADO').AsBoolean and
        (dmBaixa.cdsDepos.FieldByName('VR_DEPOSITO_FLUTUANTE').AsFloat > dmBaixa.cDifTot) and
        (Trim(dmBaixa.cdsDepos.FieldByName('FLG_STATUS').AsString) = 'F') then
      begin
        miItensDesmembramento := TMenuItem.Create(ppmDesmembrar);

        ppmDesmembrar.Items.Add(miItensDesmembramento);

        miItensDesmembramento.Caption := dmBaixa.cdsDepos.FieldByName('NUM_COD_DEPOSITO').AsString + ' (' +
                                         FloatToStrF(dmBaixa.cdsDepos.FieldByName('VR_DEPOSITO_FLUTUANTE').AsFloat, ffCurrency, 10, 2) + ')';
        miItensDesmembramento.Tag     := dmBaixa.cdsDepos.FieldByName('ID_DEPOSITO_FLUTUANTE').AsInteger;
        miItensDesmembramento.OnClick := AbrirDesmembramentoDeposito;

        Inc(iCont);
      end;

      dmBaixa.cdsDepos.Next;
    end;

    dmBaixa.cdsDepos.EnableControls;
  end;

  //Falta
  if dmBaixa.sTipoBaixa = 'F' then
  begin
    //Nova Parcela e Desconto
    iCont := 1;

    dmBaixa.cdsLanc.DisableControls;

    dmBaixa.cdsLanc.First;

    while not dmBaixa.cdsLanc.Eof do
    begin
      if dmBaixa.cdsLanc.FieldByName('SELECIONADO').AsBoolean then
      begin
        //Nova Parcela
        if dmBaixa.cdsLanc.FieldByName('VR_PARCELA_PREV').AsFloat > dmBaixa.cDifTot then
        begin
          miItensNovaParcela := TMenuItem.Create(ppmNovaParc);

          ppmNovaParc.Items.Add(miItensNovaParcela);

          miItensNovaParcela.Caption := dmBaixa.cdsLanc.FieldByName('NUM_RECIBO').AsString + ' (' +
                                        FloatToStrF(dmBaixa.cdsLanc.FieldByName('VR_PARCELA_PREV').AsFloat, ffCurrency, 10, 2) + ')';
          miItensNovaParcela.Tag     := dmBaixa.cdsLanc.FieldByName('ID_LANCAMENTO_PARC').AsInteger;
          miItensNovaParcela.OnClick := CriarNovaParcela;
        end;

        //Desconto
        miItensDesconto := TMenuItem.Create(ppmDesconto);

        ppmDesconto.Items.Add(miItensDesconto);

        miItensDesconto.Caption := dmBaixa.cdsLanc.FieldByName('NUM_RECIBO').AsString + ' (' +
                                   FloatToStrF(dmBaixa.cdsLanc.FieldByName('VR_PARCELA_PREV').AsFloat, ffCurrency, 10, 2) + ')';
        miItensDesconto.Tag     := dmBaixa.cdsLanc.FieldByName('ID_LANCAMENTO_PARC').AsInteger;
        miItensDesconto.OnClick := LancarJurosDescontoParcela;

        Inc(iCont);
      end;

      dmBaixa.cdsLanc.Next;
    end;

    dmBaixa.cdsLanc.EnableControls;
  end;
end;

procedure TFOpcoesAcaoVinculoDeposito.LancarJurosDescontoParcela(Sender: TObject);
var
  Tipo: String;
begin
  //Lancamento de Juros em Parcela -- J = Juros / D = Desconto
  Tipo := '';

  if dmBaixa.sTipoBaixa = 'S' then
    Tipo := 'J'
  else if dmBaixa.sTipoBaixa = 'F' then
    Tipo := 'D';

  if dmBaixa.cdsLanc.Locate('ID_LANCAMENTO_PARC', TMenuItem(Sender).Tag, []) then
  begin
    dmPrincipal.InicializarComponenteLancamento;

    //Lancamento
    DadosLancamento := Lancamento.Create;

    DadosLancamento.CodLancamento := dmBaixa.cdsLanc.FieldByName('COD_LANCAMENTO').AsInteger;
    DadosLancamento.AnoLancamento := dmBaixa.cdsLanc.FieldByName('ANO_LANCAMENTO').AsInteger;

    if Trim(Tipo) = 'J' then
      DadosLancamento.VlrTotalJuros := (dmBaixa.cdsLanc.FieldByName('VR_TOTAL_JUROS').AsCurrency + dmBaixa.cDifTot)
    else if Trim(Tipo) = 'D' then
      DadosLancamento.VlrTotalDesconto := (dmBaixa.cdsLanc.FieldByName('VR_TOTAL_DESCONTO').AsCurrency + dmBaixa.cDifTot);

    ListaLancamentos.Add(DadosLancamento);

    dmPrincipal.UpdateLancamento(0);

    //Parcela
    DadosLancamentoParc := LancamentoParc.Create;

    DadosLancamentoParc.CodLancamento    := dmBaixa.cdsLanc.FieldByName('COD_LANCAMENTO').AsInteger;
    DadosLancamentoParc.AnoLancamento    := dmBaixa.cdsLanc.FieldByName('ANO_LANCAMENTO').AsInteger;
    DadosLancamentoParc.IdLancamentoParc := dmBaixa.cdsLanc.FieldByName('ID_LANCAMENTO_PARC').AsInteger;
    DadosLancamentoParc.FlgStatus        := 'G';
    DadosLancamentoParc.DataPagamento    := dDataVinculo;
    DadosLancamentoParc.PagIdUsuario     := vgUsu_Id;

    if Trim(Tipo) = 'J' then
    begin
      DadosLancamentoParc.VlrJuros  := (dmBaixa.cdsLanc.FieldByName('VR_PARCELA_JUROS').AsCurrency + dmBaixa.cDifTot);
      DadosLancamentoParc.VlrPago   := ((dmBaixa.cdsLanc.FieldByName('VR_PARCELA_PREV').AsCurrency +
                                         DadosLancamentoParc.VlrJuros) -
                                        dmBaixa.cdsLanc.FieldByName('VR_PARCELA_DESCONTO').AsCurrency);
    end
    else if Trim(Tipo) = 'D' then
    begin
      DadosLancamentoParc.VlrDesconto := (dmBaixa.cdsLanc.FieldByName('VR_PARCELA_DESCONTO').AsCurrency + dmBaixa.cDifTot);
      DadosLancamentoParc.VlrPago     := ((dmBaixa.cdsLanc.FieldByName('VR_PARCELA_PREV').AsCurrency -
                                           DadosLancamentoParc.VlrDesconto) +
                                          dmBaixa.cdsLanc.FieldByName('VR_PARCELA_JUROS').AsCurrency);
    end;

    ListaLancamentoParcs.Add(DadosLancamentoParc);

    dmPrincipal.UpdateLancamentoParc(0, 0);

{    //Lancamento - Acerto do Valor Total e Quitacao (se for o caso)
    DadosLancamento.VlrTotalPago := DadosLancamentoParc.VlrPago;

    if DadosLancamento.VlrTotalPrev = DadosLancamento.VlrTotalPago then
      DadosLancamento.FlgStatus := 'G';

    dmPrincipal.UpdateLancamento(0);  }

    dmPrincipal.FinalizarComponenteLancamento;

    lGravarVinculos := True;

    Self.Close;
  end;
end;

procedure TFOpcoesAcaoVinculoDeposito.LancarSobraFaltaCaixa;
begin
  //Natureza
  IdNat := 0;
  Cod := 0;
  Descr := '';
  Obs := '';
  sIR := '';
  sCP := '';
  sLA := '';

  IdNat := 0;
  Obs := 'Inclus�o AUTOM�TICA de Natureza';

  if Trim(dmBaixa.sTipoBaixa) = 'S' then
  begin
    Cod := 2; // Receita
    Descr := 'SOBRA DE CAIXA';
  end
  else if Trim(dmBaixa.sTipoBaixa) = 'F' then
  begin
    Cod := 1; // Receita
    Descr := 'FALTA DE CAIXA';
  end;

  dmPrincipal.RetornaNatureza(IdNat, Cod, Descr, Obs);
  dmPrincipal.RetornarClassificacaoNatureza(IdNat, sIR, sCP, sLA);

  dmPrincipal.InicializarComponenteLancamento;

  //Lancamento
  DadosLancamento := Lancamento.Create;

  DadosLancamento.DataLancamento := dmBaixa.dDataVinculo;

  if Trim(dmBaixa.sTipoBaixa) = 'S' then
    DadosLancamento.TipoLancamento := 'R'
  else if Trim(dmBaixa.sTipoBaixa) = 'F' then
    DadosLancamento.TipoLancamento := 'D';

  DadosLancamento.TipoCadastro     := 'A';
  DadosLancamento.FlgReal          := 'S';
  DadosLancamento.FlgIR            := sIR;
  DadosLancamento.FlgPessoal       := sCP;
  DadosLancamento.FlgFlutuante     := 'N';
  DadosLancamento.FlgAuxiliar      := sLA;
  DadosLancamento.FlgForaFechCaixa := 'N';
  DadosLancamento.IdNatureza       := IdNat;
  DadosLancamento.VlrTotalPrev     := dmBaixa.cDifTot;
  DadosLancamento.QtdParcelas      := 1;
  DadosLancamento.FlgRecorrente    := 'N';
  DadosLancamento.FlgReplicado     := 'N';
  DadosLancamento.DataCadastro     := Now;
  DadosLancamento.CadIdUsuario     := vgUsu_Id;
  DadosLancamento.IdSistema        := dmBaixa.cdsLanc.FieldByName('ID_SISTEMA_FK').AsInteger;
  DadosLancamento.FlgStatus        := 'G';
  DadosLancamento.VlrTotalPago     := dmBaixa.cDifTot;

  if Trim(dmBaixa.sTipoBaixa) = 'S' then
  begin
    DadosLancamento.Observacao := 'Lan�amento gerado por V�nculo de Parcela a Dep�sito (' +
                                  'Recibo: ' + dmBaixa.cdsLanc.FieldByName('NUM_RECIBO').AsString + '/' +
                                  FloatToStrF(dmBaixa.cdsLanc.FieldByName('VR_PARCELA_PREV').AsFloat, ffCurrency, 10, 2) +
                                  ') com valor acima do Previsto.';
  end
  else if Trim(dmBaixa.sTipoBaixa) = 'F' then
  begin
    DadosLancamento.Observacao := 'Lan�amento gerado por V�nculo de Parcela a Dep�sito (' +
                                  'Recibo: ' + dmBaixa.cdsLanc.FieldByName('NUM_RECIBO').AsString + '/' +
                                  FloatToStrF(dmBaixa.cdsLanc.FieldByName('VR_PARCELA_PREV').AsFloat, ffCurrency, 10, 2) +
                                  ') com valor abaixo do Previsto.';
  end;

  ListaLancamentos.Add(DadosLancamento);

  dmPrincipal.InsertLancamento(0);

  //Parcela
  DadosLancamentoParc := LancamentoParc.Create;

  DadosLancamentoParc.DataLancParc   := dmBaixa.dDataVinculo;
  DadosLancamentoParc.NumParcela     := 1;
  DadosLancamentoParc.DataVencimento := dmBaixa.dDataVinculo;
  DadosLancamentoParc.VlrPrevisto    := dmBaixa.cDifTot;
  DadosLancamentoParc.IdFormaPagto   := dmBaixa.cdsLanc.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger;
  DadosLancamentoParc.CadIdUsuario   := vgUsu_Id;
  DadosLancamentoParc.DataCadastro   := Now;
  DadosLancamentoParc.VlrPago        := dmBaixa.cDifTot;
  DadosLancamentoParc.FlgStatus      := 'G';
  DadosLancamentoParc.PagIdUsuario   := vgUsu_Id;
  DadosLancamentoParc.DataPagamento  := dmBaixa.dDataVinculo;
  DadosLancamentoParc.NumCodDeposito := sLstDepo;

  ListaLancamentoParcs.Add(DadosLancamentoParc);

  dmPrincipal.InsertLancamentoParc(0, 0);

  dmPrincipal.FinalizarComponenteLancamento;

  lGravarVinculos := True;

  Self.Close;
end;

end.
