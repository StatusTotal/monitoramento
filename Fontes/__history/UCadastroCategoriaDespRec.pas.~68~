{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroCategoriaDespRec.pas
  Descricao:   Formulario de cadastro de Categorias de Despesas ou Receitas
  Author   :   Cristina
  Date:        03-mar-2016
  Last Update: 03-dez-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroCategoriaDespRec;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Vcl.DBCtrls, Vcl.Mask, Datasnap.DBClient, Datasnap.Provider, System.StrUtils;

type
  TFCadastroCategoriaDespRec = class(TFCadastroGeralPadrao)
    lblNome: TLabel;
    edtNome: TDBEdit;
    rgTipo: TDBRadioGroup;
    qryCategoria: TFDQuery;
    dspCategoria: TDataSetProvider;
    cdsCategoria: TClientDataSet;
    lcbNomeNatureza: TDBLookupComboBox;
    lblNomeNatureza: TLabel;
    qryNatureza: TFDQuery;
    dsNatureza: TDataSource;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtNomeKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnOkClick(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
    sTipo: String;
  public
    { Public declarations }

    IdNovaCategoria: Integer;
  end;

var
  FCadastroCategoriaDespRec: TFCadastroCategoriaDespRec;

  TipoCat: String;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{ TFCadastroCategoriaDespRec }

procedure TFCadastroCategoriaDespRec.btnCancelarClick(Sender: TObject);
begin
  inherited;

  Self.Close;
end;

procedure TFCadastroCategoriaDespRec.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroCategoriaDespRec.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtNome.MaxLength := 250;
end;

procedure TFCadastroCategoriaDespRec.DesabilitarComponentes;
begin
  inherited;

  btnDadosPrincipais.Visible := False;
end;

procedure TFCadastroCategoriaDespRec.edtNomeKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if Trim(edtNome.Text) <> '' then
      btnOk.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroCategoriaDespRec.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  vgOperacao     := VAZIO;
  vgIdConsulta   := 0;
  vgOrigemFiltro := False;

  inherited;
end;

procedure TFCadastroCategoriaDespRec.FormCreate(Sender: TObject);
begin
  inherited;

  IdNovaCategoria := 0;
  sTipo := '';
end;

procedure TFCadastroCategoriaDespRec.FormShow(Sender: TObject);
begin
  inherited;

  qryNatureza.Close;

  qryNatureza.SQL.Clear;
  qryNatureza.SQL.Text := '';

  qryNatureza.Params.ParamByName('TIPO_NATUREZA').Value := Trim(TipoCat);

  qryNatureza.Open;

  cdsCategoria.Close;
  cdsCategoria.Params.ParamByName('ID_CATEGORIA_DESPREC').Value := vgIdConsulta;
  cdsCategoria.Open;

  if vgOperacao = I then
    cdsCategoria.Append;

  if vgOperacao = E then
    cdsCategoria.Edit;

  if vgOperacao = C then
    pnlDados.Enabled := False;

  if TipoCat = 'D' then
  begin
    rgTipo.ItemIndex := 0;
    sTipo := 'Despesa';
  end
  else if TipoCat = 'R' then
  begin
    rgTipo.ItemIndex := 1;
    sTipo := 'Receita';
  end;

  if lcbNomeNatureza.CanFocus then
    lcbNomeNatureza.SetFocus;
end;

function TFCadastroCategoriaDespRec.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroCategoriaDespRec.GravarDadosGenerico(
  var Msg: String): Boolean;
begin
  Result := True;
  Msg := '';

  if vgOperacao = C then
    Exit;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    if cdsCategoria.State in [dsInsert, dsEdit] then
    begin
      if vgOperacao = I then
      begin
        cdsCategoria.FieldByName('ID_CATEGORIA_DESPREC').AsInteger := BS.ProximoId('ID_CATEGORIA_DESPREC', 'CATEGORIA_DESPREC');

        if vgOrigemCadastro then
          IdNovaCategoria := cdsCategoria.FieldByName('ID_CATEGORIA_DESPREC').AsInteger;

        if rgTipo.ItemIndex = 0 then
          cdsCategoria.FieldByName('TIPO').AsString := 'D'
        else if rgTipo.ItemIndex = 1 then
          cdsCategoria.FieldByName('TIPO').AsString := 'R';
      end;

      cdsCategoria.Post;
      cdsCategoria.ApplyUpdates(0);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    Msg := 'Categoria de ' + sTipo + ' gravada com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na grava��o da Categoria de ' + sTipo + '.';
  end;
end;

procedure TFCadastroCategoriaDespRec.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta de Categoria de ' + sTipo,
                          vgIdConsulta, 'CATEGORIA_DESPREC');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclus�o de Categoria de ' + sTipo,
                          vgIdConsulta, 'CATEGORIA_DESPREC');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO',
                                  'Por favor, indique o motivo da edi��o dessa Categoria de ' + sTipo + ':',
                                  '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de Categoria de ' + sTipo;

      BS.GravarUsuarioLog(2, '', sObservacao,
                          vgIdConsulta, 'CATEGORIA_DESPREC');
    end;
  end;
end;

procedure TFCadastroCategoriaDespRec.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroCategoriaDespRec.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

function TFCadastroCategoriaDespRec.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if Trim(lcbNomeNatureza.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher o NOME DA NATUREZA DA ' + UpperCase(sTipo) + '.';
    DadosMsgErro.Componente := lcbNomeNatureza;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtNome.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher o NOME DA CATEGORIA DE ' + UpperCase(sTipo) + '.';
    DadosMsgErro.Componente := edtNome;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroCategoriaDespRec.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

end.
