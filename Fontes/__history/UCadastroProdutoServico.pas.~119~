{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroProdutoServico.pas
  Descricao:   Formulario de Cadastro de Produtos e Servicos (de acordo com o tipo informado)
  Author   :   Cristina
  Date:        08-abr-2016
  Last Update: 11-out-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroProdutoServico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.DBClient, Datasnap.Provider,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, UDM, System.StrUtils, Vcl.DBCtrls,
  JvToolEdit, JvDBControls, JvExMask, JvBaseEdits, Vcl.Mask;

type
  TFCadastroProdutoServico = class(TFCadastroGeralPadrao)
    qryProdServ: TFDQuery;
    dspProdServ: TDataSetProvider;
    cdsProdServ: TClientDataSet;
    qryUnidadeMedida: TFDQuery;
    dsUnidadeMedida: TDataSource;
    lblDescricao: TLabel;
    lblUltValorCusto: TLabel;
    lblEstoqueAtual: TLabel;
    lblObservacao: TLabel;
    lblDataCadastro: TLabel;
    lblDataInativacao: TLabel;
    edtDescricao: TDBEdit;
    cedUltValorCusto: TJvDBCalcEdit;
    mmObservacao: TDBMemo;
    dteDataCadastro: TJvDBDateEdit;
    dteDataInativacao: TJvDBDateEdit;
    rgFlgAtivo: TDBRadioGroup;
    cedEstoqueAtual: TJvDBCalcEdit;
    cdsProdServID_ITEM: TIntegerField;
    cdsProdServDESCR_ITEM: TStringField;
    cdsProdServABREV_ITEM: TStringField;
    cdsProdServULT_VALOR_CUSTO: TBCDField;
    cdsProdServESTOQUE_ATUAL: TBCDField;
    cdsProdServDATA_CADASTRO: TDateField;
    cdsProdServOBS_ITEM: TStringField;
    cdsProdServFLG_ATIVO: TStringField;
    cdsProdServDATA_INATIVACAO: TDateField;
    cdsProdServMEDIDA_PADRAO: TBCDField;
    cdsProdServTIPO_ITEM: TStringField;
    cdsProdServDESCR_UNIDADEMEDIDA: TStringField;
    cdsProdServFLG_FUNCIONARIO: TStringField;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure rgFlgAtivoExit(Sender: TObject);
    procedure mmObservacaoKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure cdsProdServULT_VALOR_CUSTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsProdServESTOQUE_ATUALGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsProdServMEDIDA_PADRAOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lbMensagemErroDblClick(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }

    IdNovoItem: Integer;

    TipoProdserv: TTipoProdutoServico;
  end;

var
  FCadastroProdutoServico: TFCadastroProdutoServico;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UGDM, UVariaveisGlobais;

{ TFCadastroProdutoServico }

procedure TFCadastroProdutoServico.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroProdutoServico.btnOkClick(Sender: TObject);
var
  QryAux: TFDQuery;
  sTexto, sTipo: String;
begin
  inherited;

  sTexto := '';
  sTipo  := IfThen((TipoProdserv = PROD), 'Produto', 'Servi�o');

  QryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

  if vgOperacao <> C then
  begin
    QryAux.Close;
    QryAux.SQL.Clear;
    QryAux.SQL.Text := 'SELECT DISTINCT DESCR_ITEM ' +
                       '  FROM ITEM ' +
                       ' WHERE TIPO_ITEM = ' + QuotedStr(IfThen((TipoProdserv = PROD), 'P', 'S')) +
                       '   AND FLG_ATIVO = ' + QuotedStr('S') +
                       '   AND (UPPER(DESCR_ITEM) LIKE (' + QuotedStr('%' + edtDescricao.Text + '%') + ') ' +
                       '    OR  UPPER(REPLACE(DESCR_ITEM, ' +
                       '                      ' + QuotedStr(' ') + ', ' +
                       '                      ' + QuotedStr('') + ')) LIKE (' + QuotedStr('%' + dmGerencial.PegarLetra(edtDescricao.Text) + '%') + ')) ';
    QryAux.Open;

    if QryAux.RecordCount > 0 then
    begin
      QryAux.First;

      while not QryAux.Eof do
      begin
        if Trim(sTexto) = '' then
          sTexto := 'Existe(m) o(s) seguinte(s) ' + sTipo + '(s) semelhantes: ' + #13#10 +
                    QryAux.FieldByName('DESCR_ITEM').AsString
        else
          sTexto := sTexto + #13#10 +
                    QryAux.FieldByName('DESCR_ITEM').AsString;

        QryAux.Next;
      end;

      sTexto := sTexto + #13#10 +
                'Caso o ' + sTipo + ' que est� tentando cadastrar seja diferente desses, clique em Sim para prosseguir.';

      if Application.MessageBox(PChar(sTexto), 'Aviso', MB_YESNO + MB_ICONQUESTION) = ID_YES then
        GravarGenerico;
    end
    else
      GravarGenerico;
  end
  else
    Self.Close;

  FreeAndNil(QryAux);
end;

procedure TFCadastroProdutoServico.cdsProdServESTOQUE_ATUALGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFCadastroProdutoServico.cdsProdServMEDIDA_PADRAOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFCadastroProdutoServico.cdsProdServULT_VALOR_CUSTOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFCadastroProdutoServico.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtDescricao.MaxLength  := 250;
  mmObservacao.MaxLength  := 1000;
end;

procedure TFCadastroProdutoServico.DesabilitarComponentes;
begin
  inherited;

  cedUltValorCusto.Enabled  := vgOrigemCadastro;
  cedEstoqueAtual.Enabled   := vgOrigemCadastro;
  dteDataCadastro.Enabled   := False;
  dteDataInativacao.Enabled := False;

  if vgOperacao = C then
    pnlDados.Enabled := False;
end;

procedure TFCadastroProdutoServico.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  if lPodeCancelar then
  begin
    cdsProdServ.Cancel;

    vgOperacao     := VAZIO;
    vgIdConsulta   := 0;
    vgOrigemFiltro := False;
  end
  else
    Action := caNone;
end;

procedure TFCadastroProdutoServico.FormCreate(Sender: TObject);
begin
  inherited;

  IdNovoItem := 0;

  qryUnidadeMedida.Close;
  qryUnidadeMedida.Open;

  cdsProdServ.Close;
  cdsProdServ.Params.ParamByName('ID_ITEM').AsInteger := vgIdConsulta;
  cdsProdServ.Open;

  if vgOperacao = I then
    cdsProdServ.Append;

  if vgOperacao = E then
    cdsProdServ.Edit;
end;

procedure TFCadastroProdutoServico.FormShow(Sender: TObject);
begin
  inherited;

  if vgOperacao = I then
  begin
    cdsProdServ.FieldByName('TIPO_ITEM').AsString         := IfThen((TipoProdserv = PROD), 'P', 'S');
    cdsProdServ.FieldByName('DATA_CADASTRO').AsDateTime   := Date;
    cdsProdServ.FieldByName('MEDIDA_PADRAO').AsCurrency   := 0.00;
    cdsProdServ.FieldByName('ULT_VALOR_CUSTO').AsCurrency := 0.00;
    cdsProdServ.FieldByName('ESTOQUE_ATUAL').AsCurrency   := 0.00;
    cdsProdServ.FieldByName('FLG_ATIVO').AsString         := 'S';
  end;

  if edtDescricao.CanFocus then
    edtDescricao.SetFocus;
end;

function TFCadastroProdutoServico.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroProdutoServico.GravarDadosGenerico(var Msg: String): Boolean;
var
  sTipo: String;
begin
  Result := True;
  Msg := '';
  sTipo := '';

  if vgOperacao = C then
    Exit;

  sTipo := IfThen((TipoProdserv = PROD), 'Produto', 'Servi�o');

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    if cdsProdServ.State in [dsInsert, dsEdit] then
    begin
      if vgOperacao = I then
      begin
        cdsProdServ.FieldByName('ID_ITEM').AsInteger := BS.ProximoId('ID_ITEM', 'ITEM');

        if TipoProdserv = PROD then
        begin
          cdsProdServ.FieldByName('MEDIDA_PADRAO').AsFloat        := 1.00;
          cdsProdServ.FieldByName('DESCR_UNIDADEMEDIDA').AsString := 'Unidade(s)';
        end;
      end;

      if vgOrigemCadastro then
        IdNovoItem := cdsProdServ.FieldByName('ID_ITEM').AsInteger;

      cdsProdServ.FieldByName('TIPO_ITEM').AsString       := IfThen((TipoProdserv = PROD), 'P', 'S');
      cdsProdServ.FieldByName('FLG_FUNCIONARIO').AsString := 'N';

      cdsProdServ.Post;
      cdsProdServ.ApplyUpdates(0);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    Msg := Trim(sTipo) + ' gravado com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na grava��o do ' + Trim(sTipo) + '.';
  end;
end;

procedure TFCadastroProdutoServico.GravarLog;
var
  sObservacao: String;
  sTipo: String;
begin
  inherited;

  sObservacao := '';

  sTipo := IfThen((TipoProdserv = PROD), 'Produto', 'Servi�o');

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta de ' + sTipo,
                          vgIdConsulta, 'ITEM');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclus�o de ' + sTipo,
                          vgIdConsulta, 'ITEM');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO',
                                  'Por favor, indique o motivo da edi��o desse ' + sTipo + ':',
                                  '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de ' + sTipo;

      BS.GravarUsuarioLog(2, '', sObservacao,
                          vgIdConsulta, 'ITEM');
    end;
  end;
end;

procedure TFCadastroProdutoServico.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroProdutoServico.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

procedure TFCadastroProdutoServico.mmObservacaoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroProdutoServico.rgFlgAtivoExit(Sender: TObject);
begin
  inherited;

  if rgFlgAtivo.ItemIndex = 1 then
    dteDataInativacao.Date := Date
  else
    dteDataInativacao.Clear;

  if mmObservacao.CanFocus then
    mmObservacao.SetFocus;
end;

function TFCadastroProdutoServico.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
var
  sTipo: String;
begin
  Result   := True;
  QtdErros := 0;

  sTipo := IfThen((TipoProdserv = PROD), 'PRODUTO', 'SERVI�O');

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if Trim(edtDescricao.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar a DESCRI��O DO ' + sTipo + '.';
    DadosMsgErro.Componente := edtDescricao;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

{  if Trim(edtAbreviacao.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar a ABREVIA��O DA DESCRI��O DO ' + sTipo + '.';
    DadosMsgErro.Componente := edtAbreviacao;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if TipoProdserv = PROD then
  begin
    if cedPadraoMedida.Value = 0 then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar o PADR�O DE MEDIDA DO PRODUTO.';
      DadosMsgErro.Componente := cedPadraoMedida;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;

    if Trim(lcbPadraoMedida.Text) = '' then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar o PADR�O DE MEDIDA DO PRODUTO.';
      DadosMsgErro.Componente := lcbPadraoMedida;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;
  end;  }

  if rgFlgAtivo.ItemIndex = -1 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar a SITUA��O DO ' + sTipo + '.';
    DadosMsgErro.Componente := rgFlgAtivo;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroProdutoServico.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

end.
