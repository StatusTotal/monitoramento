{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroFechamentoSalario.pas
  Descricao:   Tela de Filtro de Fechamentos de Salario dos Funcionarios
  Author   :   Cristina
  Date:        03-jun-2016
  Last Update: 16-out-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroFechamentoSalario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.StdCtrls, System.DateUtils, frxClass, frxDBSet;

type
  TFFiltroFechamentoSalario = class(TFFiltroSimplesPadrao)
    lblMes: TLabel;
    cbMes: TComboBox;
    lblAno: TLabel;
    edtAno: TEdit;
    qryGridPaiPadraoID: TIntegerField;
    qryGridPaiPadraoMES_REFERENCIA: TIntegerField;
    qryGridPaiPadraoANO_REFERENCIA: TIntegerField;
    qryGridPaiPadraoMES_PAGAMENTO: TIntegerField;
    qryGridPaiPadraoANO_PAGAMENTO: TIntegerField;
    qryGridPaiPadraoDATA_PREV_PAGAMENTO: TDateField;
    qryGridPaiPadraoPREV_ID_USUARIO: TIntegerField;
    qryGridPaiPadraoDATA_REAL_PAGAMENTO: TDateField;
    qryGridPaiPadraoPAG_ID_USUARIO: TIntegerField;
    qryGridPaiPadraoFLG_STATUS: TStringField;
    qryGridPaiPadraoDATA_CANCELAMENTO: TDateField;
    qryGridPaiPadraoCANCEL_ID_USUARIO: TIntegerField;
    btnRealizarPagamento: TJvTransparentButton;
    qryGridPaiPadraoVR_TOTAL_BASE: TBCDField;
    qryGridPaiPadraoVR_TOTAL_COMISSAO: TBCDField;
    qryGridPaiPadraoVR_TOTAL_OUTRADESPESA: TBCDField;
    qryGridPaiPadraoVR_TOTAL_VALE: TBCDField;
    qryGridPaiPadraoVR_TOTAL_APAGAR: TBCDField;
    rgSituacaoFechamento: TRadioGroup;
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure dbgGridPaiPadraoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnIncluirClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure cbMesKeyPress(Sender: TObject; var Key: Char);
    procedure edtAnoKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryGridPaiPadraoVR_TOTAL_SALARIOSGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure btnRealizarPagamentoClick(Sender: TObject);
    procedure qryGridPaiPadraoAfterScroll(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure qryGridPaiPadraoVR_TOTAL_BASEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_TOTAL_COMISSAOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_TOTAL_OUTRADESPESAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_TOTAL_VALEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_TOTAL_APAGARGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure rgSituacaoFechamentoExit(Sender: TObject);
    procedure rgSituacaoFechamentoClick(Sender: TObject);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;

    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }
    procedure RealizarPagamentoSalarios;
  public
    { Public declarations }
  end;

var
  FFiltroFechamentoSalario: TFFiltroFechamentoSalario;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais,
  UCadastroFechamentoSalario, UDMFechamentoSalario, USolicitaData;

{ TFFiltroPagamentoFuncionario }

procedure TFFiltroFechamentoSalario.btnEditarClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;
  vgIdConsulta := qryGridPaiPadrao.FieldByName('ID').AsInteger;

  if qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString = 'C' then
  begin
    vgOperacao := C;
    Application.CreateForm(TdmFechamentoSalario, dmFechamentoSalario);
    dmGerencial.CriarForm(TFCadastroFechamentoSalario, FCadastroFechamentoSalario);
  end
  else
  begin
    vgOperacao := E;
    Application.CreateForm(TdmFechamentoSalario, dmFechamentoSalario);
    dmGerencial.CriarForm(TFCadastroFechamentoSalario, FCadastroFechamentoSalario);
    btnFiltrar.Click;
  end;

  DefinirPosicaoGrid;
end;

procedure TFFiltroFechamentoSalario.btnExcluirClick(Sender: TObject);
var
  QryExcSal, QryExcAux: TFDQuery;
  CodLanc, AnoLanc: Integer;
  iIdUsu: Integer;
  sNomeUsu, sSenhaUsu: String;
  lCancelado: Boolean;
begin
  inherited;

  CodLanc := 0;
  AnoLanc := 0;

  lCancelado := False;

  if lPodeExcluir then
  begin
    if Application.MessageBox(PChar('Os Pagamentos j� foram Realizados e ser� preciso informar LOGIN ' +
                                    'e SENHA de Autorizador para excluir esse Fechamento de Pagamento. ' + #13#10 +
                                    'Deseja prosseguir?'), 'Aviso', MB_YESNO + MB_ICONQUESTION) = ID_YES then
    begin
      iIdUsu    := 0;
      sNomeUsu  := '';
      sSenhaUsu := '';

      repeat
        repeat
          if not InputQuery('LOGIN', 'Por favor, informe o LOGIN do Autorizador:', sNomeUsu) then
            lCancelado := True;
        until (Trim(sNomeUsu) <> '') or lCancelado;

        if not lCancelado then
        begin
          repeat
            if not InputQuery('SENHA', #31'Por favor, informe a SENHA do Autorizador:', sSenhaUsu) then
              lCancelado := True;
          until (Trim(sSenhaUsu) <> '') or lCancelado;
        end;
      until (dmGerencial.VerificarAutorizacao(iIdUsu, sNomeUsu, sSenhaUsu) = True) or lCancelado;

      if lCancelado then
        Exit;
    end
    else
      Exit;

    QryExcSal := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
    QryExcAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      with QryExcSal, SQL do
      begin
        //Consulta aos Salarios
        Close;
        Clear;
        Text := 'SELECT ID_SALARIO ' +
                '  FROM SALARIO ' +
                ' WHERE ID_FECHAMENTO_SALARIO_FK = :ID_FECHAMENTO_SALARIO';
        Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
        Open;

        //LANCAMENTOS DE SALARIOS
        QryExcSal.First;

        while not QryExcSal.Eof do
        begin
          //Lancamento (Consulta)
          QryExcAux.Close;
          QryExcAux.SQL.Clear;
          QryExcAux.SQL.Text := 'SELECT COD_LANCAMENTO, ANO_LANCAMENTO ' +
                                '  FROM LANCAMENTO ' +
                                ' WHERE ID_ORIGEM      = :ID_ORIGEM ' +
                                '   AND ID_NATUREZA_FK = :ID_NATUREZA';
          QryExcAux.Params.ParamByName('ID_ORIGEM').Value   := QryExcSal.FieldByName('ID_SALARIO').AsInteger;
          QryExcAux.Params.ParamByName('ID_NATUREZA').Value := 1;  //SALARIO
          QryExcAux.Open;

          CodLanc := QryExcAux.FieldByName('COD_LANCAMENTO').AsInteger;
          AnoLanc := QryExcAux.FieldByName('ANO_LANCAMENTO').AsInteger;

          //Parcela do Lancamento
          QryExcAux.Close;
          QryExcAux.SQL.Clear;

          QryExcAux.SQL.Text := 'UPDATE LANCAMENTO_PARC ' +
                                '   SET FLG_STATUS        = :FLG_STATUS, ' +
                                '       DATA_CANCELAMENTO = :DATA_CANCELAMENTO, ' +
                                '       CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO ' +
                                ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                                '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ';

          QryExcAux.Params.ParamByName('FLG_STATUS').Value        := 'C';
          QryExcAux.Params.ParamByName('DATA_CANCELAMENTO').Value := Now;
          QryExcAux.Params.ParamByName('CANCEL_ID_USUARIO').Value := vgUsu_Id;
          QryExcAux.Params.ParamByName('COD_LANCAMENTO').Value    := CodLanc;
          QryExcAux.Params.ParamByName('ANO_LANCAMENTO').Value    := AnoLanc;

          QryExcAux.ExecSQL;

          //Detalhe do Lancamento
          QryExcAux.Close;
          QryExcAux.SQL.Clear;

          QryExcAux.SQL.Text := 'UPDATE LANCAMENTO_DET ' +
                                '   SET FLG_CANCELADO = :FLG_CANCELADO ' +
                                ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                                '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ';

          QryExcAux.Params.ParamByName('FLG_CANCELADO').Value  := 'S';
          QryExcAux.Params.ParamByName('COD_LANCAMENTO').Value := CodLanc;
          QryExcAux.Params.ParamByName('ANO_LANCAMENTO').Value := AnoLanc;

          QryExcAux.ExecSQL;

          //Lancamento
          QryExcAux.Close;
          QryExcAux.SQL.Clear;

          QryExcAux.SQL.Text := 'UPDATE LANCAMENTO ' +
                                '   SET FLG_CANCELADO     = :FLG_CANCELADO, ' +
                                '       DATA_CANCELAMENTO = :DATA_CANCELAMENTO, ' +
                                '       CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO ' +
                                ' WHERE COD_LANCAMENTO = :COD_LANCAMENTO ' +
                                '   AND ANO_LANCAMENTO = :ANO_LANCAMENTO ';

          QryExcAux.Params.ParamByName('FLG_CANCELADO').Value     := 'S';
          QryExcAux.Params.ParamByName('DATA_CANCELAMENTO').Value := Now;
          QryExcAux.Params.ParamByName('CANCEL_ID_USUARIO').Value := vgUsu_Id;
          QryExcAux.Params.ParamByName('COD_LANCAMENTO').Value    := CodLanc;
          QryExcAux.Params.ParamByName('ANO_LANCAMENTO').Value    := AnoLanc;

          QryExcAux.ExecSQL;

          QryExcSal.Next;
        end;

        //FECHAMENTO DE SALARIO
        Close;
        Clear;

        Text := 'UPDATE FECHAMENTO_SALARIO ' +
                '   SET DATA_CANCELAMENTO = :DATA_CANCELAMENTO, ' +
                '       FLG_STATUS        = :FLG_STATUS, ' +
                '       CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO ' +
                ' WHERE ID_FECHAMENTO_SALARIO = :ID_FECHAMENTO_SALARIO';

        Params.ParamByName('DATA_CANCELAMENTO').Value     := Date;
        Params.ParamByName('FLG_STATUS').Value            := 'C';
        Params.ParamByName('CANCEL_ID_USUARIO').Value     := vgUsu_Id;
        Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;

        ExecSQL;

      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      GravarLog;
      Application.MessageBox('Fechamento de Sal�rio cancelado com sucesso!', 'Aviso', MB_OK)
    except
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Application.MessageBox('Erro no cancelamento do Fechamento de Sal�rio.', 'Erro', MB_OK + MB_ICONERROR)
    end;

    FreeAndNil(QryExcAux);
    FreeAndNil(QryExcSal);

    btnFiltrar.Click;
  end;
end;

procedure TFFiltroFechamentoSalario.btnFiltrarClick(Sender: TObject);
begin
  qryGridPaiPadrao. Close;

  if cbMes.ItemIndex = -1 then
  begin
    qryGridPaiPadrao.Params.ParamByName('MES_REF01').Value := 0;
    qryGridPaiPadrao.Params.ParamByName('MES_REF02').Value := 0;
  end
  else
  begin
    qryGridPaiPadrao.Params.ParamByName('MES_REF01').Value := (cbMes.ItemIndex + 1);
    qryGridPaiPadrao.Params.ParamByName('MES_REF02').Value := (cbMes.ItemIndex + 1);
  end;

  if Trim(edtAno.Text) = '' then
  begin
    qryGridPaiPadrao.Params.ParamByName('ANO_REF01').Value := 0;
    qryGridPaiPadrao.Params.ParamByName('ANO_REF02').Value := 0;
  end
  else
  begin
    qryGridPaiPadrao.Params.ParamByName('ANO_REF01').Value := StrToInt(edtAno.Text);
    qryGridPaiPadrao.Params.ParamByName('ANO_REF02').Value := StrToInt(edtAno.Text);
  end;

  case rgSituacaoFechamento.ItemIndex of
    0: qryGridPaiPadrao.Params.ParamByName('FLG_STATUS').Value := 'P';
    1: qryGridPaiPadrao.Params.ParamByName('FLG_STATUS').Value := 'G';
    2: qryGridPaiPadrao.Params.ParamByName('FLG_STATUS').Value := 'C';
  end;

  qryGridPaiPadrao.Open;

  inherited;
end;

procedure TFFiltroFechamentoSalario.btnIncluirClick(Sender: TObject);
var
  MsgErro: String;
begin
  inherited;

  MsgErro := '';

  { REABILITAR VERIFICACAO APOS IMPLEMENTAR CADASTRO DE COMISSOES }

{  if vgConf_FlgTrabalhaComissao = 'S' then
  begin
    if ((vgConf_FlgTrabalhaComissao = 'N') or
        ((vgConf_FlgTrabalhaComissao = 'S') and
        not dmPrincipal.FechamentoComissoesPendente)) then
    begin
      MsgErro := 'Para incluir Fechamento de Pagamentos de Colaboradores, ' +
                 '� preciso incluir antes as porcentagens das Comiss�es para ' +
                 'os mesmos na tela de Cadastro de Colaborador.';

      Application.MessageBox(PChar(MsgErro), 'Aviso', MB_OK + MB_ICONWARNING);

      Exit;
    end;
  end;  }

  vgOperacao   := I;
  vgIdConsulta := 0;

  Application.CreateForm(TdmFechamentoSalario, dmFechamentoSalario);
  dmGerencial.CriarForm(TFCadastroFechamentoSalario, FCadastroFechamentoSalario);

  btnFiltrar.Click;
end;

procedure TFFiltroFechamentoSalario.btnLimparClick(Sender: TObject);
begin
  cbMes.ItemIndex := (MonthOf(Date) - 1);
  edtAno.Text := IntToStr(YearOf(Date));
  rgSituacaoFechamento.ItemIndex := 0;

  inherited;

  if cbMes.CanFocus then
    cbMes.SetFocus;
end;

procedure TFFiltroFechamentoSalario.btnRealizarPagamentoClick(Sender: TObject);
var
  Msg: String;
  QrySal: TFDQuery;
  lOk: Boolean;
  iMes, iAno: Integer;
begin
  inherited;

  lOk := True;

  iMes := 0;
  iAno := 0;

  if qryGridPaiPadrao.RecordCount = 0 then
    Exit
  else if qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString = 'G' then
    Msg := 'N�o � poss�vel realizar o Fechamento de Sal�rios informado, pois os pagamentos j� foram realizados.'
  else if qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString = 'C' then
    Msg := 'N�o � poss�vel realizar o Fechamento de Sal�rio informado, pois o mesmo j� foi cancelado.'
  else
  begin
    //Se existirem Comissoes pendentes nao incluidas no cadastro do fechamento, inclui-las e recalcular Fechamento
    if dmPrincipal.ExistemComissoesPendentes then
    begin
      try
        if vgConSISTEMA.Connected then
          vgConSISTEMA.StartTransaction;

        //Verifica Mes e Ano de Referencia
        if iMesRef < Integer(MonthOf(Date)) then
        begin
          if iMesRef = 12 then
          begin
            iMes := 1;
            iAno := (iAnoRef + 1);
          end
          else
          begin
            iMes := (iMesRef + 1);
            iAno := iAnoRef;
          end;
        end
        else
        begin
          iMes := iMesRef;
          iAno := iAnoRef;
        end;

        //Apontar o ID do Fechamento nas tabelas agregadas
        dmPrincipal.ApontarFechamentoAgregadas(iMesRef,
                                               iAnoRef,
                                               qryGridPaiPadrao.FieldByName('ID').AsInteger,
                                               0);

        //Consulta ID de Funcionarios
        QrySal := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

        QrySal.Close;
        QrySal.SQL.Clear;
        QrySal.SQL.Text := 'SELECT ID_SALARIO, ID_FUNCIONARIO_FK ' +
                           '  FROM SALARIO ' +
                           ' WHERE ID_FECHAMENTO_SALARIO_FK = :ID_FECHAMENTO_SALARIO';
        QrySal.Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
        QrySal.Open;

        QrySal.First;

        while not QrySal.Eof do
        begin
          //Recalcular Fechamento
          dmPrincipal.RecalcularFechamentoSalarios(qryGridPaiPadrao.FieldByName('ID').AsInteger,
                                                   QrySal.FieldByName('ID_FUNCIONARIO_FK').AsInteger,
                                                   False, True, False);

          QrySal.Next;
        end;

        FreeAndNil(QrySal);

        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Commit;
      except
        on E: Exception do
        begin
          if vgConSISTEMA.InTransaction then
            vgConSISTEMA.Rollback;

          lOk := False;

          Application.MessageBox('Erro ao recalcular o Fechamento de Sal�rios.',
                                 'Erro', MB_OK + MB_ICONERROR);
        end;
      end;
    end;

    if lOk then
      RealizarPagamentoSalarios;
  end;
end;

procedure TFFiltroFechamentoSalario.cbMesKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if (Trim(cbMes.Text) <> '') and
      (Trim(cbMes.Text) <> '') then
      btnFiltrar.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroFechamentoSalario.dbgGridPaiPadraoDblClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao   := C;
  vgIdConsulta := qryGridPaiPadrao.FieldByName('ID').AsInteger;

  Application.CreateForm(TdmFechamentoSalario, dmFechamentoSalario);
  dmGerencial.CriarForm(TFCadastroFechamentoSalario, FCadastroFechamentoSalario);

  DefinirPosicaoGrid;
end;

procedure TFFiltroFechamentoSalario.dbgGridPaiPadraoDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
   CtrlState: array[Boolean] of Integer = (DFCS_BUTTONCHECK,
                                           DFCS_BUTTONCHECK or DFCS_CHECKED) ;

var
  Grid: TDBGrid;
begin
  inherited;

  Grid := Sender as TDBGrid;

  if Grid.DataSource.DataSet.FieldByName('FLG_STATUS').AsString = 'C' then
  begin
    Grid.Canvas.Font.Color := clGray;
    Grid.Canvas.FillRect(Rect);
    Grid.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;

  if (Column.Field.DataType = ftBoolean) then
  begin
    dbgGridPaiPadrao.Canvas.FillRect(Rect);

    if VarIsNull(Column.Field.Value) then
    begin
      DrawFrameControl(dbgGridPaiPadrao.Canvas.Handle,
                       Rect,
                       DFC_BUTTON,
                       DFCS_BUTTONCHECK or DFCS_INACTIVE)
    end
    else
    begin
      DrawFrameControl(dbgGridPaiPadrao.Canvas.Handle,
                       Rect,
                       DFC_BUTTON,
                       CtrlState[Column.Field.AsBoolean]);
    end;
  end;
end;

procedure TFFiltroFechamentoSalario.edtAnoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if (Trim(cbMes.Text) <> '') and
      (Trim(edtAno.Text) <> '') then
      btnFiltrar.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroFechamentoSalario.FormActivate(Sender: TObject);
begin
  inherited;

  if vgOrigFechPend then
    Self.Position := poDesktopCenter
  else
    Self.Position := poMainFormCenter;
end;

procedure TFFiltroFechamentoSalario.FormCreate(Sender: TObject);
begin
  inherited;

  cbMes.ItemIndex := (MonthOf(Date) - 1);
  edtAno.Text := IntToStr(YearOf(Date));
  rgSituacaoFechamento.ItemIndex := 0;

  qryGridPaiPadrao. Close;
  qryGridPaiPadrao.Params.ParamByName('MES_REF01').Value  := 0;
  qryGridPaiPadrao.Params.ParamByName('MES_REF02').Value  := 0;
  qryGridPaiPadrao.Params.ParamByName('ANO_REF01').Value  := StrToInt(edtAno.Text);
  qryGridPaiPadrao.Params.ParamByName('ANO_REF02').Value  := StrToInt(edtAno.Text);
  qryGridPaiPadrao.Params.ParamByName('FLG_STATUS').Value := 'P';
  qryGridPaiPadrao.Open;
end;

procedure TFFiltroFechamentoSalario.FormShow(Sender: TObject);
begin
  inherited;

  btnImprimir.Visible := False;

  ShowScrollBar(dbgGridPaiPadrao.Handle, SB_HORZ, False);

  if cbMes.CanFocus then
    cbMes.SetFocus;
end;

procedure TFFiltroFechamentoSalario.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    X:  //EXCLUSAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da exclus�o do Fechamento de Sal�rio:', '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'FECHAMENTO_SALARIO');
    end;
    P:  //IMPRESSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente impress�o de Dados de Fechamento de Sal�rio',
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'FECHAMENTO_SALARIO');
    end;
    T:  //EXPORTACAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente exporta��o de Dados de Fechamento de Sal�rio',
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'FECHAMENTO_SALARIO');
    end;
  end;
end;

function TFFiltroFechamentoSalario.PodeExcluir(var Msg: String): Boolean;
begin
  inherited;

  Result := True;

  if qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString = 'G' then
    msg := 'N�o � poss�vel excluir o Fechamento de Sal�rio informado, pois os pagamentos j� foram realizados.'
  else if qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString = 'C' then
    msg := 'N�o � poss�vel excluir o Fechamento de Sal�rio informado, pois o mesmo j� foi cancelado.';

  Result := (qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString = 'P') and (qryGridPaiPadrao.RecordCount > 0);
end;

procedure TFFiltroFechamentoSalario.qryGridPaiPadraoAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  btnRealizarPagamento.Enabled := (vgPrm_RealFecSal and
                                   (qryGridPaiPadrao.RecordCount > 0) and
                                   (qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString = 'P'));
end;

procedure TFFiltroFechamentoSalario.qryGridPaiPadraoVR_TOTAL_APAGARGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoSalario.qryGridPaiPadraoVR_TOTAL_BASEGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoSalario.qryGridPaiPadraoVR_TOTAL_COMISSAOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoSalario.qryGridPaiPadraoVR_TOTAL_OUTRADESPESAGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoSalario.qryGridPaiPadraoVR_TOTAL_SALARIOSGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoSalario.qryGridPaiPadraoVR_TOTAL_VALEGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoSalario.RealizarPagamentoSalarios;
var
  QryPag, QryPagAux: TFDQuery;
  CodLanc, AnoLanc: Integer;
  ValorPago: Currency;
  dDataRealizacao: TDateTime;
begin
  inherited;

  CodLanc   := 0;
  AnoLanc   := 0;
  ValorPago := 0;

  dDataRealizacao := 0;

  QryPag    := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
  QryPagAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  //Solicita a Data de Realizacao dos Pagamentos
  repeat
    try
      Application.CreateForm(TFSolicitaData, FSolicitaData);

      FSolicitaData.sCaptionLabel      := 'Por favor, informe a Data de Realiza��o do Pagamento:';
      FSolicitaData.lVerifCaixaVigente := True;

      FSolicitaData.ShowModal;
    finally
      dDataRealizacao := FSolicitaData.dDataSolicitada;
      FSolicitaData.Free;
    end;
  until (dDataRealizacao > 0) ;

  //Realiza os Pagamentos
  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    with QryPag, SQL do
    begin
      { SALARIOS }
      Close;
      Clear;
      Text := 'SELECT ID_SALARIO ' +
              '  FROM SALARIO ' +
              ' WHERE ID_FECHAMENTO_SALARIO_FK = :ID_FECHAMENTO_SALARIO';
      Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
      Open;

      QryPag.First;

      while not QryPag.Eof do
      begin
        //Lancamento (Consulta)
        QryPagAux.Close;
        QryPagAux.SQL.Clear;
        QryPagAux.SQL.Text := 'SELECT COD_LANCAMENTO, ANO_LANCAMENTO, VR_TOTAL_PREV, VR_TOTAL_DESCONTO ' +
                              '  FROM LANCAMENTO ' +
                              ' WHERE ID_ORIGEM      = :ID_ORIGEM ' +
                              '   AND ID_NATUREZA_FK = :ID_NATUREZA';
        QryPagAux.Params.ParamByName('ID_ORIGEM').Value   := QryPag.FieldByName('ID_SALARIO').AsInteger;
        QryPagAux.Params.ParamByName('ID_NATUREZA').Value := 1;  //SALARIO
        QryPagAux.Open;

        CodLanc   := QryPagAux.FieldByName('COD_LANCAMENTO').AsInteger;
        AnoLanc   := QryPagAux.FieldByName('ANO_LANCAMENTO').AsInteger;
        ValorPago := (QryPagAux.FieldByName('VR_TOTAL_PREV').AsCurrency -
                      QryPagAux.FieldByName('VR_TOTAL_DESCONTO').AsCurrency);

        //Parcela do Lancamento
        QryPagAux.Close;
        QryPagAux.SQL.Clear;

        QryPagAux.SQL.Text := 'UPDATE LANCAMENTO_PARC ' +
                              '   SET FLG_STATUS      = :FLG_STATUS, ' +
                              '       DATA_PAGAMENTO  = :DATA_PAGAMENTO, ' +
                              '       PAG_ID_USUARIO  = :PAG_ID_USUARIO, ' +
                              '       VR_PARCELA_PAGO = :VR_PARCELA_PAGO ' +
                              ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                              '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ';

        QryPagAux.Params.ParamByName('FLG_STATUS').Value      := 'G';
        QryPagAux.Params.ParamByName('DATA_PAGAMENTO').Value  := dDataRealizacao;
        QryPagAux.Params.ParamByName('PAG_ID_USUARIO').Value  := vgUsu_Id;
        QryPagAux.Params.ParamByName('VR_PARCELA_PAGO').Value := ValorPago;
        QryPagAux.Params.ParamByName('COD_LANCAMENTO').Value  := CodLanc;
        QryPagAux.Params.ParamByName('ANO_LANCAMENTO').Value  := AnoLanc;

        QryPagAux.ExecSQL;

        //Lancamento
        QryPagAux.Close;
        QryPagAux.SQL.Clear;

        QryPagAux.SQL.Text := 'UPDATE LANCAMENTO ' +
                              '   SET FLG_STATUS    = :FLG_STATUS, ' +
                              '       VR_TOTAL_PAGO = :VR_TOTAL_PAGO ' +
                              ' WHERE COD_LANCAMENTO = :COD_LANCAMENTO ' +
                              '   AND ANO_LANCAMENTO = :ANO_LANCAMENTO ';

        QryPagAux.Params.ParamByName('FLG_STATUS').Value     := 'G';
        QryPagAux.Params.ParamByName('VR_TOTAL_PAGO').Value  := ValorPago;
        QryPagAux.Params.ParamByName('COD_LANCAMENTO').Value := CodLanc;
        QryPagAux.Params.ParamByName('ANO_LANCAMENTO').Value := AnoLanc;

        QryPagAux.ExecSQL;

        QryPag.Next;
      end;

(*      { COMISSOES }
      Close;
      Clear;
      Text := 'SELECT ID_COMISSAO ' +
              '  FROM COMISSAO ' +
              ' WHERE ID_FECHAMENTO_SALARIO_FK = :ID_FECHAMENTO_SALARIO';
      Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
      Open;

      QryPag.First;

      while not QryPag.Eof do
      begin
        //Lancamento (Consulta)
        QryPagAux.Close;
        QryPagAux.SQL.Clear;
        QryPagAux.SQL.Text := 'SELECT COD_LANCAMENTO, ANO_LANCAMENTO, VR_TOTAL_PREV, VR_TOTAL_DESCONTO ' +
                              '  FROM LANCAMENTO ' +
                              ' WHERE ID_ORIGEM      = :ID_ORIGEM ' +
                              '   AND ID_NATUREZA_FK = :ID_NATUREZA';
        QryPagAux.Params.ParamByName('ID_ORIGEM').Value   := QryPag.FieldByName('ID_COMISSAO').AsInteger;
        QryPagAux.Params.ParamByName('ID_NATUREZA').Value := 2;  //COMISSAO
        QryPagAux.Open;

        CodLanc   := QryPagAux.FieldByName('COD_LANCAMENTO').AsInteger;
        AnoLanc   := QryPagAux.FieldByName('ANO_LANCAMENTO').AsInteger;
        ValorPago := (QryPagAux.FieldByName('VR_TOTAL_PREV').AsCurrency -
                      QryPagAux.FieldByName('VR_TOTAL_DESCONTO').AsCurrency);

        //Parcela do Lancamento
        QryPagAux.Close;
        QryPagAux.SQL.Clear;

        QryPagAux.SQL.Text := 'UPDATE LANCAMENTO_PARC ' +
                              '   SET FLG_STATUS      = :FLG_STATUS, ' +
                              '       DATA_PAGAMENTO  = :DATA_PAGAMENTO, ' +
                              '       PAG_ID_USUARIO  = :PAG_ID_USUARIO, ' +
                              '       VR_PARCELA_PAGO = :VR_PARCELA_PAGO ' +
                              ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                              '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ';

        QryPagAux.Params.ParamByName('FLG_STATUS').Value      := 'G';
        QryPagAux.Params.ParamByName('DATA_PAGAMENTO').Value  := dDataRealizacao;
        QryPagAux.Params.ParamByName('PAG_ID_USUARIO').Value  := vgUsu_Id;
        QryPagAux.Params.ParamByName('VR_PARCELA_PAGO').Value := ValorPago;
        QryPagAux.Params.ParamByName('COD_LANCAMENTO').Value  := CodLanc;
        QryPagAux.Params.ParamByName('ANO_LANCAMENTO').Value  := AnoLanc;

        QryPagAux.ExecSQL;

        //Lancamento
        QryPagAux.Close;
        QryPagAux.SQL.Clear;

        QryPagAux.SQL.Text := 'UPDATE LANCAMENTO ' +
                              '   SET FLG_STATUS    = :FLG_STATUS, ' +
                              '       VR_TOTAL_PAGO = :VR_TOTAL_PAGO ' +
                              ' WHERE COD_LANCAMENTO = :COD_LANCAMENTO ' +
                              '   AND ANO_LANCAMENTO = :ANO_LANCAMENTO ';

        QryPagAux.Params.ParamByName('FLG_STATUS').Value     := 'G';
        QryPagAux.Params.ParamByName('VR_TOTAL_PAGO').Value  := ValorPago;
        QryPagAux.Params.ParamByName('COD_LANCAMENTO').Value := CodLanc;
        QryPagAux.Params.ParamByName('ANO_LANCAMENTO').Value := AnoLanc;

        QryPagAux.ExecSQL;

        QryPag.Next;
      end;  *)

(*      { VALES }
      Close;
      Clear;
      Text := 'SELECT ID_VALE ' +
              '  FROM VALE ' +
              ' WHERE ID_FECHAMENTO_SALARIO_FK = :ID_FECHAMENTO_SALARIO';
      Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
      Open;

      QryPag.First;

      while not QryPag.Eof do
      begin
        //Lancamento (Consulta)
        QryPagAux.Close;
        QryPagAux.SQL.Clear;
        QryPagAux.SQL.Text := 'SELECT COD_LANCAMENTO, ANO_LANCAMENTO, VR_TOTAL_PREV, VR_TOTAL_DESCONTO ' +
                              '  FROM LANCAMENTO ' +
                              ' WHERE ID_ORIGEM      = :ID_ORIGEM ' +
                              '   AND ID_NATUREZA_FK = :ID_NATUREZA';
        QryPagAux.Params.ParamByName('ID_ORIGEM').Value   := QryPag.FieldByName('ID_VALE').AsInteger;
        QryPagAux.Params.ParamByName('ID_NATUREZA').Value := 3;  //VALE
        QryPagAux.Open;

        CodLanc   := QryPagAux.FieldByName('COD_LANCAMENTO').AsInteger;
        AnoLanc   := QryPagAux.FieldByName('ANO_LANCAMENTO').AsInteger;
        ValorPago := (QryPagAux.FieldByName('VR_TOTAL_PREV').AsCurrency -
                      QryPagAux.FieldByName('VR_TOTAL_DESCONTO').AsCurrency);

        //Parcela do Lancamento
        QryPagAux.Close;
        QryPagAux.SQL.Clear;

        QryPagAux.SQL.Text := 'UPDATE LANCAMENTO_PARC ' +
                              '   SET FLG_STATUS      = :FLG_STATUS, ' +
                              '       DATA_PAGAMENTO  = :DATA_PAGAMENTO, ' +
                              '       PAG_ID_USUARIO  = :PAG_ID_USUARIO, ' +
                              '       VR_PARCELA_PAGO = :VR_PARCELA_PAGO ' +
                              ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                              '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ';

        QryPagAux.Params.ParamByName('FLG_STATUS').Value      := 'G';
        QryPagAux.Params.ParamByName('DATA_PAGAMENTO').Value  := dDataRealizacao;
        QryPagAux.Params.ParamByName('PAG_ID_USUARIO').Value  := vgUsu_Id;
        QryPagAux.Params.ParamByName('VR_PARCELA_PAGO').Value := ValorPago;
        QryPagAux.Params.ParamByName('COD_LANCAMENTO').Value  := CodLanc;
        QryPagAux.Params.ParamByName('ANO_LANCAMENTO').Value  := AnoLanc;

        QryPagAux.ExecSQL;

        //Lancamento
        QryPagAux.Close;
        QryPagAux.SQL.Clear;

        QryPagAux.SQL.Text := 'UPDATE LANCAMENTO ' +
                              '   SET FLG_STATUS    = :FLG_STATUS, ' +
                              '       VR_TOTAL_PAGO = :VR_TOTAL_PAGO ' +
                              ' WHERE COD_LANCAMENTO = :COD_LANCAMENTO ' +
                              '   AND ANO_LANCAMENTO = :ANO_LANCAMENTO ';

        QryPagAux.Params.ParamByName('FLG_STATUS').Value     := 'G';
        QryPagAux.Params.ParamByName('VR_TOTAL_PAGO').Value  := ValorPago;
        QryPagAux.Params.ParamByName('COD_LANCAMENTO').Value := CodLanc;
        QryPagAux.Params.ParamByName('ANO_LANCAMENTO').Value := AnoLanc;

        QryPagAux.ExecSQL;

        QryPag.Next;
      end;  *)

(*      { OUTRAS DESPESAS }
      Close;
      Clear;
      Text := 'SELECT ID_OUTRADESPESA_FUNC ' +
              '  FROM OUTRADESPESA_FUNC ' +
              ' WHERE ID_FECHAMENTO_SALARIO_FK = :ID_FECHAMENTO_SALARIO ' +
              '   AND FLG_MODALIDADE <> ' + QuotedStr('L');
      Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
      Open;

      QryPag.First;

      while not QryPag.Eof do
      begin
        //Lancamento (Consulta)
        QryPagAux.Close;
        QryPagAux.SQL.Clear;
        QryPagAux.SQL.Text := 'SELECT COD_LANCAMENTO, ANO_LANCAMENTO, VR_TOTAL_PREV, VR_TOTAL_DESCONTO ' +
                              '  FROM LANCAMENTO ' +
                              ' WHERE ID_ORIGEM      = :ID_ORIGEM ' +
                              '   AND ID_NATUREZA_FK = :ID_NATUREZA';
        QryPagAux.Params.ParamByName('ID_ORIGEM').Value   := QryPag.FieldByName('ID_OUTRADESPESA_FUNC').AsInteger;
        QryPagAux.Params.ParamByName('ID_NATUREZA').Value := 7;  //OUTRA DESPESA
        QryPagAux.Open;

        CodLanc   := QryPagAux.FieldByName('COD_LANCAMENTO').AsInteger;
        AnoLanc   := QryPagAux.FieldByName('ANO_LANCAMENTO').AsInteger;
        ValorPago := (QryPagAux.FieldByName('VR_TOTAL_PREV').AsCurrency -
                      QryPagAux.FieldByName('VR_TOTAL_DESCONTO').AsCurrency);

        //Parcela do Lancamento
        QryPagAux.Close;
        QryPagAux.SQL.Clear;

        QryPagAux.SQL.Text := 'UPDATE LANCAMENTO_PARC ' +
                              '   SET FLG_STATUS      = :FLG_STATUS, ' +
                              '       DATA_PAGAMENTO  = :DATA_PAGAMENTO, ' +
                              '       PAG_ID_USUARIO  = :PAG_ID_USUARIO, ' +
                              '       VR_PARCELA_PAGO = :VR_PARCELA_PAGO ' +
                              ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                              '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ';

        QryPagAux.Params.ParamByName('FLG_STATUS').Value      := 'G';
        QryPagAux.Params.ParamByName('DATA_PAGAMENTO').Value  := dDataRealizacao;
        QryPagAux.Params.ParamByName('PAG_ID_USUARIO').Value  := vgUsu_Id;
        QryPagAux.Params.ParamByName('VR_PARCELA_PAGO').Value := ValorPago;
        QryPagAux.Params.ParamByName('COD_LANCAMENTO').Value  := CodLanc;
        QryPagAux.Params.ParamByName('ANO_LANCAMENTO').Value  := AnoLanc;

        QryPagAux.ExecSQL;

        //Lancamento
        QryPagAux.Close;
        QryPagAux.SQL.Clear;

        QryPagAux.SQL.Text := 'UPDATE LANCAMENTO ' +
                              '   SET FLG_STATUS    = :FLG_STATUS, ' +
                              '       VR_TOTAL_PAGO = :VR_TOTAL_PAGO ' +
                              ' WHERE COD_LANCAMENTO = :COD_LANCAMENTO ' +
                              '   AND ANO_LANCAMENTO = :ANO_LANCAMENTO ';

        QryPagAux.Params.ParamByName('FLG_STATUS').Value     := 'G';
        QryPagAux.Params.ParamByName('VR_TOTAL_PAGO').Value  := ValorPago;
        QryPagAux.Params.ParamByName('COD_LANCAMENTO').Value := CodLanc;
        QryPagAux.Params.ParamByName('ANO_LANCAMENTO').Value := AnoLanc;

        QryPagAux.ExecSQL;

        QryPag.Next;
      end;  *)
    end;

    //FECHAMENTO DE SALARIO
    QryPagAux.Close;
    QryPagAux.SQL.Clear;

    QryPagAux.SQL.Text := 'UPDATE FECHAMENTO_SALARIO ' +
                          '   SET DATA_REAL_PAGAMENTO = :DATA_REAL_PAGAMENTO, ' +
                          '       FLG_STATUS          = :FLG_STATUS, ' +
                          '       PAG_ID_USUARIO      = :PAG_ID_USUARIO ' +
                          ' WHERE ID_FECHAMENTO_SALARIO = :ID_FECHAMENTO_SALARIO';

    QryPagAux.Params.ParamByName('DATA_REAL_PAGAMENTO').Value   := dDataRealizacao;
    QryPagAux.Params.ParamByName('FLG_STATUS').Value            := 'G';
    QryPagAux.Params.ParamByName('PAG_ID_USUARIO').Value        := vgUsu_Id;
    QryPagAux.Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;

    QryPagAux.ExecSQL;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    GravarLog;
    Application.MessageBox('Fechamento de Sal�rio pago com sucesso!', 'Aviso', MB_OK)
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Application.MessageBox('Erro no pagamento do Fechamento de Sal�rio.', 'Erro', MB_OK + MB_ICONERROR)
  end;

  FreeAndNil(QryPagAux);
  FreeAndNil(QryPag);

  btnFiltrar.Click;
end;

procedure TFFiltroFechamentoSalario.rgSituacaoFechamentoClick(Sender: TObject);
begin
  inherited;

  if (Trim(cbMes.Text) <> '') and
    (Trim(edtAno.Text) <> '') then
    btnFiltrar.Click;
end;

procedure TFFiltroFechamentoSalario.rgSituacaoFechamentoExit(Sender: TObject);
begin
  inherited;

  if (Trim(cbMes.Text) <> '') and
    (Trim(edtAno.Text) <> '') then
    btnFiltrar.Click;
end;

procedure TFFiltroFechamentoSalario.VerificarPermissoes;
begin
  inherited;

  btnIncluir.Enabled           := vgPrm_RealFecSal;
  btnEditar.Enabled            := vgPrm_EdFecSal and (qryGridPaiPadrao.RecordCount > 0);
  btnExcluir.Enabled           := vgPrm_ExcFecSal and (qryGridPaiPadrao.RecordCount > 0);

  btnRealizarPagamento.Enabled := (vgPrm_RealFecSal and
                                   (qryGridPaiPadrao.RecordCount > 0) and
                                   (qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString = 'P'));

  btnImprimir.Enabled          := vgPrm_ImpFecSal and (qryGridPaiPadrao.RecordCount > 0);
end;

end.
