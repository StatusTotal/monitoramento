{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroAberturaCaixa.pas
  Descricao:   Tela de filtro de Abertura de Caixa
  Author   :   Cristina
  Date:        26-dez-2016
  Last Update: 25-ago-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroAberturaCaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Mask, JvExMask, JvToolEdit, System.DateUtils;

type
  TFFiltroAberturaCaixa = class(TFFiltroSimplesPadrao)
    lblPeriodoAbertura: TLabel;
    Label3: TLabel;
    dteIniPeriodoAbertura: TJvDateEdit;
    dteFimPeriodoAbertura: TJvDateEdit;
    qryGridPaiPadraoID: TIntegerField;
    qryGridPaiPadraoDATA_ABERTURA: TDateField;
    qryGridPaiPadraoHORA_ABERTURA: TTimeField;
    qryGridPaiPadraoVR_NOTAS: TBCDField;
    qryGridPaiPadraoVR_MOEDAS: TBCDField;
    qryGridPaiPadraoVR_TOTAL_ABERTURA: TBCDField;
    qryGridPaiPadraoVR_TOTAL_ULT_FECH: TBCDField;
    qryGridPaiPadraoNOME_USUARIO: TStringField;
    qryGridPaiPadraoCAD_ID_USUARIO: TIntegerField;
    qryGridPaiPadraoFLG_CANCELADO: TStringField;
    qryGridPaiPadraoDATA_CANCELAMENTO: TDateField;
    qryGridPaiPadraoCANCEL_ID_USUARIO: TIntegerField;
    rgSituacao: TRadioGroup;
    qryGridPaiPadraoVR_CONTACORRENTE: TBCDField;
    procedure qryGridPaiPadraoCalcFields(DataSet: TDataSet);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure qryGridPaiPadraoVR_NOTASGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_MOEDASGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_CONTA_CORRENTEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_TOTAL_ABERTURAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_TOTAL_ULT_FECHGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_DIFERENCAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure dbgGridPaiPadraoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgGridPaiPadraoTitleClick(Column: TColumn);
    procedure dteFimPeriodoAberturaKeyPress(Sender: TObject; var Key: Char);
    procedure dteIniPeriodoAberturaKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure rgSituacaoExit(Sender: TObject);
    procedure rgSituacaoClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroAberturaCaixa: TFFiltroAberturaCaixa;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMCaixa,
  UCadastroAberturaCaixa;

{ TFFiltroAberturaCaixa }

procedure TFFiltroAberturaCaixa.btnEditarClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  if qryGridPaiPadrao.FieldByName('DATA_ABERTURA').AsDateTime <> Date then
    vgOperacao := C
  else
    vgOperacao := E;

  vgIdConsulta := qryGridPaiPadrao.FieldByName('ID').AsInteger;

  Application.CreateForm(TdmCaixa, dmCaixa);
  dmGerencial.CriarForm(TFCadastroAberturaCaixa, FCadastroAberturaCaixa);

  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroAberturaCaixa.btnExcluirClick(Sender: TObject);
var
  iIdUsu: Integer;
  qryExclusao: TFDQuery;
  sNomeUsu, sSenhaUsu: String;
  lCancelado: Boolean;
begin
  inherited;

  lCancelado := False;

  if lPodeExcluir then
  begin
     if Application.MessageBox(PChar('Para excluir essa Abertura de Caixa ser� preciso informar LOGIN ' +
                                    'e SENHA de Autorizador. ' + #13#10 +
                                    'Deseja prosseguir?'), 'Aviso', MB_YESNO + MB_ICONQUESTION) = ID_YES then
    begin
      iIdUsu    := 0;
      sNomeUsu  := '';
      sSenhaUsu := '';

      repeat
        repeat
          if not InputQuery('LOGIN', 'Por favor, informe o LOGIN do Autorizador:', sNomeUsu) then
            lCancelado := True;
        until (Trim(sNomeUsu) <> '') or lCancelado;

        if not lCancelado then
        begin
          repeat
            if not InputQuery('SENHA', #31'Por favor, informe a SENHA do Autorizador:', sSenhaUsu) then
              lCancelado := True;
          until (Trim(sSenhaUsu) <> '') or lCancelado;
        end;
      until (dmGerencial.VerificarAutorizacao(iIdUsu, sNomeUsu, sSenhaUsu) = True) or lCancelado;

      if lCancelado then
        Exit;
    end
    else
      Exit;

    qryExclusao := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      with qryExclusao, SQL do
      begin
        Close;
        Clear;
        Text := 'UPDATE CAIXA_ABERTURA ' +
                '   SET FLG_CANCELADO     = :FLG_CANCELADO, ' +
                '       DATA_CANCELAMENTO = :DATA_CANCELAMENTO, ' +
                '       CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO ' +
                ' WHERE ID_CAIXA_ABERTURA = :ID_CAIXA_ABERTURA';
        Params.ParamByName('FLG_CANCELADO').Value     := 'S';
        Params.ParamByName('DATA_CANCELAMENTO').Value := Date;
        Params.ParamByName('CANCEL_ID_USUARIO').Value := vgUsu_Id;
        Params.ParamByName('ID_CAIXA_ABERTURA').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      GravarLog;
      Application.MessageBox('Abertura de Caixa exclu�da com sucesso!', 'Aviso', MB_OK)
    except
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Application.MessageBox('Erro na exclus�o da Abertura de Caixa.', 'Erro', MB_OK + MB_ICONERROR)
    end;

    FreeAndNil(qryExclusao);

    btnFiltrar.Click;
  end;
end;

procedure TFFiltroAberturaCaixa.btnFiltrarClick(Sender: TObject);
begin
  qryGridPaiPadrao.Close;
  qryGridPaiPadrao.Params.ParamByName('DATA_INI').Value := dteIniPeriodoAbertura.Date;
  qryGridPaiPadrao.Params.ParamByName('DATA_FIM').Value := dteFimPeriodoAbertura.Date;

  case rgSituacao.ItemIndex of
    0:
    begin
      qryGridPaiPadrao.Params.ParamByName('SIT1').Value := 'N';
      qryGridPaiPadrao.Params.ParamByName('SIT2').Value := 'S';
    end;
    1:
    begin
      qryGridPaiPadrao.Params.ParamByName('SIT1').Value := 'N';
      qryGridPaiPadrao.Params.ParamByName('SIT2').Value := 'N';
    end;
    2:
    begin
      qryGridPaiPadrao.Params.ParamByName('SIT1').Value := 'S';
      qryGridPaiPadrao.Params.ParamByName('SIT2').Value := 'S';
    end;
  end;

  qryGridPaiPadrao.Open;

  inherited;
end;

procedure TFFiltroAberturaCaixa.btnImprimirClick(Sender: TObject);
begin
  inherited;

  dmPrincipal.AbrirRelatorio(10);
end;

procedure TFFiltroAberturaCaixa.btnIncluirClick(Sender: TObject);
begin
  inherited;

  Application.CreateForm(TdmCaixa, dmCaixa);

  //Recupera o Valor Total do Ultimo Operacao
  dmCaixa.qryUltOperacao.Close;
  dmCaixa.qryUltOperacao.Open;

  if dmCaixa.qryUltOperacao.RecordCount > 0 then
  begin
    if dmCaixa.qryUltOperacao.FieldByName('TIPO').AsString = 'A' then
    begin
      Application.MessageBox('O CAIXA j� se encontra aberto.',
                             'Aviso',
                             MB_OK + MB_ICONWARNING);
      FreeAndNil(dmCaixa);
      Exit;
    end;
  end;

  vgOperacao   := I;
  vgIdConsulta := 0;

  dmGerencial.CriarForm(TFCadastroAberturaCaixa, FCadastroAberturaCaixa);

  btnFiltrar.Click;
end;

procedure TFFiltroAberturaCaixa.btnLimparClick(Sender: TObject);
begin
  dteIniPeriodoAbertura.Date := StartOfTheMonth(Date);
  dteFimPeriodoAbertura.Date := EndOfTheMonth(Date);

  rgSituacao.ItemIndex := 1;

  inherited;

  if dteIniPeriodoAbertura.CanFocus then
    dteIniPeriodoAbertura.SetFocus;
end;

procedure TFFiltroAberturaCaixa.dbgGridPaiPadraoDblClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao   := C;
  vgIdConsulta := qryGridPaiPadrao.FieldByName('ID').AsInteger;

  Application.CreateForm(TdmCaixa, dmCaixa);
  dmGerencial.CriarForm(TFCadastroAberturaCaixa, FCadastroAberturaCaixa);

  DefinirPosicaoGrid;
end;

procedure TFFiltroAberturaCaixa.dbgGridPaiPadraoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Grid: TDBGrid;
begin
  inherited;

  Grid := Sender as TDBGrid;

  if Grid.DataSource.DataSet.FieldByName('FLG_CANCELADO').AsString = 'S' then
  begin
    Grid.Canvas.Font.Color := clGray;
    Grid.Canvas.FillRect(Rect);
    Grid.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TFFiltroAberturaCaixa.dbgGridPaiPadraoTitleClick(Column: TColumn);
begin
  inherited;

  qryGridPaiPadrao.IndexFieldNames := Column.FieldName;
end;

procedure TFFiltroAberturaCaixa.dteFimPeriodoAberturaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
     if dteFimPeriodoAbertura.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteFimPeriodoAbertura.CanFocus then
        dteFimPeriodoAbertura.SetFocus;
    end
    else if dteIniPeriodoAbertura.Date > dteFimPeriodoAbertura.Date then
    begin
      Application.MessageBox('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso', MB_OK + MB_ICONWARNING);

      dteFimPeriodoAbertura.Clear;

      if dteFimPeriodoAbertura.CanFocus then
        dteFimPeriodoAbertura.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dteFimPeriodoAbertura.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteFimPeriodoAbertura.CanFocus then
        dteFimPeriodoAbertura.SetFocus;
    end
    else if dteIniPeriodoAbertura.Date > dteFimPeriodoAbertura.Date then
    begin
      Application.MessageBox('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso', MB_OK + MB_ICONWARNING);

      dteFimPeriodoAbertura.Clear;

      if dteFimPeriodoAbertura.CanFocus then
        dteFimPeriodoAbertura.SetFocus;
    end
    else
      btnFiltrar.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroAberturaCaixa.dteIniPeriodoAberturaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
     if dteIniPeriodoAbertura.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteIniPeriodoAbertura.CanFocus then
        dteIniPeriodoAbertura.SetFocus;
    end
    else if dteIniPeriodoAbertura.Date > dteFimPeriodoAbertura.Date then
    begin
      Application.MessageBox('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso', MB_OK + MB_ICONWARNING);

      dteIniPeriodoAbertura.Clear;

      if dteIniPeriodoAbertura.CanFocus then
        dteIniPeriodoAbertura.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dteIniPeriodoAbertura.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteIniPeriodoAbertura.CanFocus then
        dteIniPeriodoAbertura.SetFocus;
    end
    else if dteIniPeriodoAbertura.Date > dteFimPeriodoAbertura.Date then
    begin
      Application.MessageBox('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso', MB_OK + MB_ICONWARNING);

      dteIniPeriodoAbertura.Clear;

      if dteIniPeriodoAbertura.CanFocus then
        dteIniPeriodoAbertura.SetFocus;
    end
    else
      btnFiltrar.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroAberturaCaixa.FormCreate(Sender: TObject);
begin
  inherited;

  dteIniPeriodoAbertura.Date := StartOfTheMonth(Date);
  dteFimPeriodoAbertura.Date := EndOfTheMonth(Date);

  btnFiltrar.Click;
end;

procedure TFFiltroAberturaCaixa.FormShow(Sender: TObject);
begin
  inherited;

  if dteIniPeriodoAbertura.CanFocus then
    dteIniPeriodoAbertura.SetFocus;
end;

procedure TFFiltroAberturaCaixa.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    X:  //EXCLUSAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da exclus�o dessa Abertura de Caixa:', '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'CAIXA_ABERTURA');
    end;
    P:  //IMPRESSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente impress�o de Lista de Aberturas de Caixa',
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'CAIXA_ABERTURA');
    end;
    T:  //EXPORTACAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente exporta��o de Lista de Aberturas de Caixa',
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'CAIXA_ABERTURA');
    end;
  end;
end;

function TFFiltroAberturaCaixa.PodeExcluir(var Msg: String): Boolean;
begin
  Result := True;

  if qryGridPaiPadrao.FieldByName('FLG_CANCELADO').AsString = 'S' then
    msg := 'N�o � poss�vel excluir a Abertura de Caixa informada, pois a mesma j� foi cancelada.';

  Result := (qryGridPaiPadrao.FieldByName('FLG_CANCELADO').AsString = 'N') and
            (qryGridPaiPadrao.RecordCount > 0);
end;

procedure TFFiltroAberturaCaixa.qryGridPaiPadraoCalcFields(DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  inherited;

  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with qryAux, SQL do
  begin
    //Item
    if qryGridPaiPadrao.FieldByName('CAD_ID_USUARIO').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT NOME ' +
              '  FROM USUARIO ' +
              ' WHERE ID_USUARIO = :ID_USUARIO';
      Params.ParamByName('ID_USUARIO').AsInteger := qryGridPaiPadrao.FieldByName('CAD_ID_USUARIO').AsInteger;
      Open;

      qryGridPaiPadrao.FieldByName('NOME_USUARIO').AsString := qryAux.FieldByName('NOME').AsString;
    end;
  end;

  FreeAndNil(qryAux);
end;

procedure TFFiltroAberturaCaixa.qryGridPaiPadraoVR_CONTA_CORRENTEGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroAberturaCaixa.qryGridPaiPadraoVR_DIFERENCAGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroAberturaCaixa.qryGridPaiPadraoVR_MOEDASGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroAberturaCaixa.qryGridPaiPadraoVR_NOTASGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroAberturaCaixa.qryGridPaiPadraoVR_TOTAL_ABERTURAGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroAberturaCaixa.qryGridPaiPadraoVR_TOTAL_ULT_FECHGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroAberturaCaixa.rgSituacaoClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroAberturaCaixa.rgSituacaoExit(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroAberturaCaixa.VerificarPermissoes;
begin
  inherited;

  btnIncluir.Enabled  := vgPrm_IncAbCx;
  btnEditar.Enabled   := vgPrm_EdAbCx and (qryGridPaiPadrao.RecordCount > 0);
  btnExcluir.Enabled  := vgPrm_ExcAbCx and (qryGridPaiPadrao.RecordCount > 0);
  btnImprimir.Enabled := vgPrm_ImpAbCx and (qryGridPaiPadrao.RecordCount > 0);
end;

end.
