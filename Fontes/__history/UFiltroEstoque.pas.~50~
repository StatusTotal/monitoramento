{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroEstoque.pas
  Descricao:   Filtro de Estoque de Itens
  Author   :   Cristina
  Date:        29-jul-2016
  Last Update: 25-ago-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, frxClass, frxDBSet, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, JvExControls, JvButton, JvTransparentButton, Vcl.Grids,
  Vcl.DBGrids, Vcl.ExtCtrls, Vcl.DBCtrls, Vcl.StdCtrls;

type
  TFFiltroEstoque = class(TFFiltroSimplesPadrao)
    lblItem: TLabel;
    lcbItem: TDBLookupComboBox;
    dsItem: TDataSource;
    qryItem: TFDQuery;
    btnAcertarEstoque: TJvTransparentButton;
    qryGridPaiPadraoID: TIntegerField;
    qryGridPaiPadraoESTOQUE_ATUAL: TBCDField;
    qryGridPaiPadraoTOT_ENTRADAS: TLargeintField;
    qryGridPaiPadraoTOT_SAIDAS: TLargeintField;
    qryGridPaiPadraoDESCR_ITEM: TStringField;
    qryGridPaiPadraoFLG_ATIVO: TStringField;
    qryGridPaiPadraoULT_VALOR_CUSTO: TBCDField;
    qryGridPaiPadraoDATA_ULTMOV: TSQLTimeStampField;
    procedure btnAcertarEstoqueClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure dbgGridPaiPadraoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure lcbItemKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryGridPaiPadraoAfterScroll(DataSet: TDataSet);
    procedure qryGridPaiPadraoULT_VALOR_CUSTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoESTOQUE_ATUALGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoTOT_ENTRADASGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoTOT_SAIDASGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroEstoque: TFFiltroEstoque;

implementation

{$R *.dfm}

uses UDM, UGDM, UVariaveisGlobais, UBibliotecaSistema, UCadastroEstoqueAcerto;

{ TFFiltroEstoque }

procedure TFFiltroEstoque.btnAcertarEstoqueClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao   := I;
  vgIdConsulta := qryGridPaiPadrao.FieldByName('ID').AsInteger;

  try
    Application.CreateForm(TFCadastroEstoqueAcerto, FCadastroEstoqueAcerto);

    FCadastroEstoqueAcerto.QtdAtual  := qryGridPaiPadrao.FieldByName('ESTOQUE_ATUAL').AsInteger;
    FCadastroEstoqueAcerto.NomeItem  := qryGridPaiPadrao.FieldByName('DESCR_ITEM').AsString;
    FCadastroEstoqueAcerto.ValorItem := qryGridPaiPadrao.FieldByName('ULT_VALOR_CUSTO').AsCurrency;

    FCadastroEstoqueAcerto.ShowModal;
  finally
    FCadastroEstoqueAcerto.Free;
  end;

  btnFiltrar.Click;
  DefinirPosicaoGrid;
end;

procedure TFFiltroEstoque.btnExcluirClick(Sender: TObject);
begin
  inherited;

  //
end;

procedure TFFiltroEstoque.btnFiltrarClick(Sender: TObject);
begin
  inherited;

  qryGridPaiPadrao.Close;

  if Trim(lcbItem.Text) <> '' then
  begin
    qryGridPaiPadrao.Params.ParamByName('ID_ITEM01').Value := lcbItem.KeyValue;
    qryGridPaiPadrao.Params.ParamByName('ID_ITEM02').Value := lcbItem.KeyValue;
  end
  else
  begin
    qryGridPaiPadrao.Params.ParamByName('ID_ITEM01').Value := 0;
    qryGridPaiPadrao.Params.ParamByName('ID_ITEM02').Value := 0;
  end;

  qryGridPaiPadrao.Open;
end;

procedure TFFiltroEstoque.btnLimparClick(Sender: TObject);
begin
  lcbItem.KeyValue := -1;

  inherited;

  if lcbItem.CanFocus then
    lcbItem.SetFocus;
end;

procedure TFFiltroEstoque.dbgGridPaiPadraoDblClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao   := C;
  vgIdConsulta := qryGridPaiPadrao.FieldByName('ID').AsInteger;

  try
    Application.CreateForm(TFCadastroEstoqueAcerto, FCadastroEstoqueAcerto);

    FCadastroEstoqueAcerto.QtdAtual  := qryGridPaiPadrao.FieldByName('ESTOQUE_ATUAL').AsInteger;
    FCadastroEstoqueAcerto.NomeItem  := qryGridPaiPadrao.FieldByName('DESCR_ITEM').AsString;
    FCadastroEstoqueAcerto.ValorItem := qryGridPaiPadrao.FieldByName('ULT_VALOR_CUSTO').AsCurrency;

    FCadastroEstoqueAcerto.ShowModal;
  finally
    FCadastroEstoqueAcerto.Free;
  end;

  DefinirPosicaoGrid;
end;

procedure TFFiltroEstoque.dbgGridPaiPadraoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;

  if qryGridPaiPadrao.FieldByName('FLG_ATIVO').AsString = 'N' then
  begin
    dbgGridPaiPadrao.Canvas.Font.Color := clGray;
    dbgGridPaiPadrao.Canvas.FillRect(Rect);
    dbgGridPaiPadrao.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end
end;

procedure TFFiltroEstoque.FormCreate(Sender: TObject);
begin
  inherited;

  qryItem.Close;
  qryItem.Open;

  qryGridPaiPadrao.Close;
  qryGridPaiPadrao.Params.ParamByName('ID_ITEM01').Value := 0;
  qryGridPaiPadrao.Params.ParamByName('ID_ITEM02').Value := 0;
  qryGridPaiPadrao.Open;
end;

procedure TFFiltroEstoque.FormShow(Sender: TObject);
begin
  inherited;

  ShowScrollBar(dbgGridPaiPadrao.Handle, SB_HORZ, False);

  btnIncluir.Enabled := False;
  btnEditar.Enabled  := False;
  btnExcluir.Enabled := False;

  btnImprimir.Visible := False;

  if lcbItem.CanFocus then
    lcbItem.SetFocus;
end;

procedure TFFiltroEstoque.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    X:  //EXCLUSAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da Movimentação do Estoque' + ':', '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          qryGridPaiPadrao.FieldByName('ID').AsInteger, 'HISTORICO_ITEM');
    end;
    P:  //IMPRESSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente impressão de Lista de Movimentações do Estoque',
                          qryGridPaiPadrao.FieldByName('ID').AsInteger, 'HISTORICO_ITEM');
    end;
    T:  //EXPORTACAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente exportação de Lista de Movimentações do Estoque',
                          qryGridPaiPadrao.FieldByName('ID').AsInteger, 'HISTORICO_ITEM');
    end;
  end;
end;

procedure TFFiltroEstoque.lcbItemKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

function TFFiltroEstoque.PodeExcluir(var Msg: String): Boolean;
begin
  inherited;

  Result := (qryGridPaiPadrao.RecordCount > 0);
end;

procedure TFFiltroEstoque.qryGridPaiPadraoAfterScroll(DataSet: TDataSet);
begin
  inherited;

  btnAcertarEstoque.Enabled := vgPrm_AceEst and (qryGridPaiPadrao.RecordCount > 0);
//  btnImprimir.Enabled       := vgPrm_ImpEst and (qryGridPaiPadrao.RecordCount > 0);
end;

procedure TFFiltroEstoque.qryGridPaiPadraoESTOQUE_ATUALGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString = '' then
    Text := '0'
  else
    Text := Sender.AsString;
end;

procedure TFFiltroEstoque.qryGridPaiPadraoTOT_ENTRADASGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString = '' then
    Text := '0'
  else
    Text := Sender.AsString;
end;

procedure TFFiltroEstoque.qryGridPaiPadraoTOT_SAIDASGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString = '' then
    Text := '0'
  else
    Text := Sender.AsString;
end;

procedure TFFiltroEstoque.qryGridPaiPadraoULT_VALOR_CUSTOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFFiltroEstoque.VerificarPermissoes;
begin
  inherited;

  //
end;

end.
