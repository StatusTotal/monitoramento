{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFRMDespesasPendentes.pas
  Descricao:   Frame de exibicao das Despesas A Pagar no Dia ou Vencidas
  Author   :   Cristina
  Date:        25-abr-2016
  Last Update: 02-jan-2018 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFRMDespesasPendentes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  JvExControls, JvButton, JvTransparentButton, Vcl.ExtCtrls, FireDAC.Comp.Client,
  FireDAC.Stan.Param, Data.DB, Vcl.Grids, Vcl.DBGrids, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  Vcl.Mask, JvExMask, JvToolEdit, System.DateUtils, System.StrUtils,
  JvExDBGrids, JvDBGrid, JvDBUltimGrid;

type
  TFFRMLancamentosPendentes = class(TFrame)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    btnFechar: TJvTransparentButton;
    lblLegendaSituacaoLanc: TLabel;
    spLegendaVenceHoje: TShape;
    lblLegendaVenceHoje: TLabel;
    spLegendaVencido: TShape;
    lblLegendaVencido: TLabel;
    lblPeriodoLanc: TLabel;
    Label2: TLabel;
    rgTipoLanc: TRadioGroup;
    dtePeriodoIni: TJvDateEdit;
    dtePeriodoFim: TJvDateEdit;
    btnEditarLanc: TJvTransparentButton;
    btnRealizarPagto: TJvTransparentButton;
    Panel4: TPanel;
    dbgLancPend: TJvDBUltimGrid;
    chbSelecionarTodos: TCheckBox;
    procedure btnFecharClick(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure Panel1Resize(Sender: TObject);
    procedure rgTipoLancClick(Sender: TObject);
    procedure dtePeriodoIniKeyPress(Sender: TObject; var Key: Char);
    procedure dtePeriodoFimKeyPress(Sender: TObject; var Key: Char);
    procedure btnEditarLancClick(Sender: TObject);
    procedure btnRealizarPagtoClick(Sender: TObject);
    procedure dbgLancPendTitleClick(Column: TColumn);
    procedure dbgLancPendDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure chbSelecionarTodosClick(Sender: TObject);
    procedure dbgLancPendCellClick(Column: TColumn);
  private
    { Private declarations }

    procedure AbrirCadastro(SomentePagamento: Boolean);
    procedure MarcarDesmarcarTodos;
    procedure SalvarBoleano;
  public
    { Public declarations }

    procedure AbrirListaPendentes;
  end;

var
  UFrameDespPend: TFFRMLancamentosPendentes;

implementation

{$R *.dfm}

uses UDM, UBibliotecaSistema, UVariaveisGlobais, UGDM, UDMLancamento,
  UCadastroLancamento, UPagamentoParcelaLancamento;

procedure TFFRMLancamentosPendentes.AbrirCadastro(SomentePagamento: Boolean);
begin
  dmPrincipal.cdsDespesas.DisableControls;

  dmPrincipal.cdsDespesas.First;

  while not dmPrincipal.cdsDespesas.Eof do
  begin
    if dmPrincipal.cdsDespesas.FieldByName('SELECIONADO').AsBoolean then
    begin
      vgOperacao     := E;
      vgOrigemFiltro := True;
      vgLanc_Codigo  := dmPrincipal.cdsDespesas.FieldByName('COD_LANCAMENTO').AsInteger;
      vgLanc_Ano     := dmPrincipal.cdsDespesas.FieldByName('ANO_LANCAMENTO').AsInteger;
      vgLanc_TpLanc  := dmPrincipal.cdsDespesas.FieldByName('TIPO_LANCAMENTO').AsString;

      try
        Application.CreateForm(TdmLancamento, dmLancamento);
        Application.CreateForm(TFCadastroLancamento, FCadastroLancamento);

        if dmPrincipal.cdsDespesas.FieldByName('TIPO_LANCAMENTO').AsString = 'D' then
          FCadastroLancamento.TipoLanc := DESP
        else
          FCadastroLancamento.TipoLanc := REC;

        FCadastroLancamento.lSomentePagamento := SomentePagamento;

        FCadastroLancamento.ShowModal;
      finally
        FCadastroLancamento.Free;
        FreeAndNil(dmLancamento);

        vgOperacao     := VAZIO;
        vgOrigemFiltro := False;
        vgLanc_Codigo  := 0;
        vgLanc_Ano     := 0;
      end;
    end;

    dmPrincipal.cdsDespesas.Next;
  end;

  dmPrincipal.cdsDespesas.EnableControls;
end;

procedure TFFRMLancamentosPendentes.AbrirListaPendentes;
begin
  dmPrincipal.cdsDespesas.Close;

  case rgTipoLanc.ItemIndex of
    0:
    begin
      dmPrincipal.cdsDespesas.Params.ParamByName('TIPO1').Value := 'D';
      dmPrincipal.cdsDespesas.Params.ParamByName('TIPO2').Value := 'R';
    end;
    1:
    begin
      dmPrincipal.cdsDespesas.Params.ParamByName('TIPO1').Value := 'R';
      dmPrincipal.cdsDespesas.Params.ParamByName('TIPO2').Value := 'R';
    end;
    2:
    begin
      dmPrincipal.cdsDespesas.Params.ParamByName('TIPO1').Value := 'D';
      dmPrincipal.cdsDespesas.Params.ParamByName('TIPO2').Value := 'D';
    end;
  end;

  dmPrincipal.cdsDespesas.Params.ParamByName('DATA_INI').Value := dtePeriodoIni.Date;
  dmPrincipal.cdsDespesas.Params.ParamByName('DATA_FIM').Value := dtePeriodoFim.Date;
  dmPrincipal.cdsDespesas.Open;

  if dmPrincipal.cdsDespesas.RecordCount = 0 then
  begin
    btnEditarLanc.Enabled    := False;
    btnRealizarPagto.Enabled := False;
  end
  else
  begin
    btnEditarLanc.Enabled    := True;
    btnRealizarPagto.Enabled := True;

    MarcarDesmarcarTodos;
  end;
end;

procedure TFFRMLancamentosPendentes.btnEditarLancClick(Sender: TObject);
begin
  AbrirCadastro(False);
end;

procedure TFFRMLancamentosPendentes.btnFecharClick(Sender: TObject);
begin
  Self.Visible := False;
end;

procedure TFFRMLancamentosPendentes.btnRealizarPagtoClick(Sender: TObject);
var
  lAlterou: Boolean;
  sMensagem: String;
  iCont: Integer;
  cTotLancs: Currency;
begin
  lAlterou := False;

  sMensagem := '';

  iCont     := 0;
  cTotLancs := 0;

  dmPrincipal.cdsDespesas.DisableControls;

  dmPrincipal.cdsDespesas.First;

  while not dmPrincipal.cdsDespesas.Eof do
  begin
    if dmPrincipal.cdsDespesas.FieldByName('SELECIONADO').AsBoolean then
    begin
      Inc(iCont);

      if Trim(sMensagem) = '' then
        sMensagem := dmPrincipal.cdsDespesas.FieldByName('LANC').AsString +
                     IfThen(dmPrincipal.cdsDespesas.FieldByName('NUM_RECIBO').IsNull,
                            '',
                            ' (Recibo: ' + dmPrincipal.cdsDespesas.FieldByName('NUM_RECIBO').AsString) + ')'
      else
        sMensagem := sMensagem + #13#10 + dmPrincipal.cdsDespesas.FieldByName('LANC').AsString +
                                          IfThen(dmPrincipal.cdsDespesas.FieldByName('NUM_RECIBO').IsNull,
                                                 '',
                                                 ' (Recibo: ' + dmPrincipal.cdsDespesas.FieldByName('NUM_RECIBO').AsString) + ')';
    end;

    dmPrincipal.cdsDespesas.Next;
  end;

  dmPrincipal.cdsDespesas.EnableControls;

  if iCont = 0 then
    Application.MessageBox('',
                           '',
                           MB_OK + MB_ICONWARNING)
  else
  begin
    if iCont = 1 then
      sMensagem := 'Confirma o pagamento do Lançamento ' + sMensagem + '?'
    else
      sMensagem := 'Confirma o pagamento dos Lançamentos: ' + #13#10 + #13#10 +
                   sMensagem + #13#10 + #13#10 +
                   'Total: ' + FloatToStrF(dmBaixa.cdsDepos.FieldByName('VR_DEPOSITO_FLUTUANTE').AsFloat, ffCurrency, 10, 2) +'?';

    dmPrincipal.cdsDespesas.DisableControls;

    dmPrincipal.cdsDespesas.First;

    while not dmPrincipal.cdsDespesas.Eof do
    begin
      if dmPrincipal.cdsDespesas.FieldByName('SELECIONADO').AsBoolean then
      begin
  {      if dmPrincipal.cdsDespesas.FieldByName('ID_NATUREZA_FK').AsInteger in [1, 2, 3, 7] then
          Application.MessageBox(PChar('O Lançamento ' +
                                        dmPrincipal.cdsDespesas.FieldByName('LANC').AsString +
                                        IfThen(dmPrincipal.cdsDespesas.FieldByName('NUM_RECIBO').IsNull,
                                               '',
                                               ' (Recibo: ' + dmPrincipal.cdsDespesas.FieldByName('NUM_RECIBO').AsString) + ')' +
                                        ' somente pode ser alterado na Origem.'),
                                 'Aviso',
                                 MB_OK + MB_ICONWARNING)
        else
        begin     }
          if Application.MessageBoxPChar('Confirma a edição do Lançamento ' +
                                         dmPrincipal.cdsDespesas.FieldByName('LANC').AsString +
                                         IfThen(dmPrincipal.cdsDespesas.FieldByName('NUM_RECIBO').IsNull,
                                                '',
                                                ' (Recibo: ' + dmPrincipal.cdsDespesas.FieldByName('NUM_RECIBO').AsString) + ')' + '?'), 'Confirmação', MB_YESNO + MB_ICONQUESTION) = ID_YES then
          begin
            vgOperacao       := E;
            vgOrigemCadastro := False;
            vgLanc_Codigo    := dmPrincipal.cdsDespesas.FieldByName('COD_LANCAMENTO').AsInteger;
            vgLanc_Ano       := dmPrincipal.cdsDespesas.FieldByName('ANO_LANCAMENTO').AsInteger;
            vgLanc_TpLanc    := dmPrincipal.cdsDespesas.FieldByName('TIPO_LANCAMENTO').AsString;

            try
              Application.CreateForm(TFPagamentoParcelaLancamento, FPagamentoParcelaLancamento);

              FPagamentoParcelaLancamento.IdParcela := dmPrincipal.cdsDespesas.FieldByName('ID_LANCAMENTO_PARC').AsInteger;
              FPagamentoParcelaLancamento.InicializarTela;
              FPagamentoParcelaLancamento.btnAtualizarPagamento.Click;
              FPagamentoParcelaLancamento.btnOk.Click;

              FPagamentoParcelaLancamento.Hide;
            finally
              FPagamentoParcelaLancamento.Free;
            end;

            lAlterou := True;
          end;
  //      end;
      end;

      dmPrincipal.cdsDespesas.Next;
    end;

    if lAlterou then
    begin
      Application.MessageBox('Pagamento(s) realizado(s) com sucesso!',
                             'Sucesso',
                             MB_OK);
      AbrirListaPendentes;
    end;

    dmPrincipal.cdsDespesas.EnableControls;
  end;
end;

procedure TFFRMLancamentosPendentes.chbSelecionarTodosClick(Sender: TObject);
begin
  MarcarDesmarcarTodos;
end;

procedure TFFRMLancamentosPendentes.dbgLancPendCellClick(Column: TColumn);
begin
  if dbgLancPend.SelectedField.DataType = ftBoolean then
    SalvarBoleano;
end;

procedure TFFRMLancamentosPendentes.dbgLancPendDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if gdSelected in State then
  begin
    dbgLancPend.Canvas.Brush.Color := clHighlight;
    dbgLancPend.Canvas.Font.Style  := [fsBold];
  end
  else
  begin
    if dmPrincipal.cdsDespesas.FieldByName('TIPO_LANCAMENTO').AsString = 'R' then
      dbgLancPend.Canvas.Brush.Color := $00DFFFDF
    else if dmPrincipal.cdsDespesas.FieldByName('TIPO_LANCAMENTO').AsString = 'D' then
      dbgLancPend.Canvas.Brush.Color := $00DCD9FF;
  end;

  //Legenda
  if dmPrincipal.cdsDespesas.FieldByName('DATA_VENCIMENTO').AsDateTime = Date then  //Vence Hoje
    dbgLancPend.Canvas.Font.Color := clGreen
  else if dmPrincipal.cdsDespesas.FieldByName('DATA_VENCIMENTO').AsDateTime < Date then  //Vencido
    dbgLancPend.Canvas.Font.Color := clMaroon;

  dbgLancPend.DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure TFFRMLancamentosPendentes.dbgLancPendTitleClick(Column: TColumn);
begin
  dmPrincipal.cdsDespesas.IndexFieldNames := Column.FieldName;
end;

procedure TFFRMLancamentosPendentes.dtePeriodoFimKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (dtePeriodoIni.Date > 0) and
    (dtePeriodoFim.Date > 0) then
    AbrirListaPendentes;

  if Key = #13 then  //ENTER
  begin
    if (dtePeriodoIni.Date > 0) and
      (dtePeriodoFim.Date > 0) then
      AbrirListaPendentes;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnFechar.Click;
    Key := #0;
  end;
end;

procedure TFFRMLancamentosPendentes.dtePeriodoIniKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (dtePeriodoIni.Date > 0) and
    (dtePeriodoFim.Date > 0) then
    AbrirListaPendentes;

  if Key = #13 then  //ENTER
  begin
    if (dtePeriodoIni.Date > 0) and
      (dtePeriodoFim.Date > 0) then
      AbrirListaPendentes;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnFechar.Click;
    Key := #0;
  end;
end;

procedure TFFRMLancamentosPendentes.FrameResize(Sender: TObject);
begin
  dmGerencial.ArredondarBordas(Self);
end;

procedure TFFRMLancamentosPendentes.MarcarDesmarcarTodos;
begin
  dmPrincipal.cdsDespesas.DisableControls;

  dmPrincipal.cdsDespesas.First;

  while not dmPrincipal.cdsDespesas.Eof do
  begin
    dmPrincipal.cdsDespesas.Edit;

    if chbSelecionarTodos.Checked then
      dmPrincipal.cdsDespesas.FieldByName('SELECIONADO').AsBoolean := True
    else
      dmPrincipal.cdsDespesas.FieldByName('SELECIONADO').AsBoolean := False;

    dmPrincipal.cdsDespesas.Post;

    dmPrincipal.cdsDespesas.Next;
  end;

  if dmPrincipal.cdsDespesas.FieldByName('SELECIONADO').AsBoolean then
    chbSelecionarTodos.Caption := 'Desmarcar Todos'
  else
    chbSelecionarTodos.Caption := 'Desmarcar Todos';

  dmPrincipal.cdsDespesas.EnableControls;
end;

procedure TFFRMLancamentosPendentes.Panel1Resize(Sender: TObject);
begin
  dmGerencial.ArredondarBordas(Panel1);
end;

procedure TFFRMLancamentosPendentes.rgTipoLancClick(Sender: TObject);
begin
  AbrirListaPendentes;
end;

procedure TFFRMLancamentosPendentes.SalvarBoleano;
begin
  dbgLancPend.SelectedField.DataSet.Edit;
  dbgLancPend.SelectedField.AsBoolean := not dbgLancPend.SelectedField.AsBoolean;
  dbgLancPend.SelectedField.DataSet.Post;
end;

end.
