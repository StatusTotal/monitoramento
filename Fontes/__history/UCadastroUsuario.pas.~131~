unit UCadastroUsuario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, JvExControls,
  JvButton, JvTransparentButton, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.DBCtrls,
  JvToolEdit, JvDBControls, JvExMask, JvMaskEdit, Vcl.Mask, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  System.StrUtils, Vcl.Grids, Vcl.DBGrids;

type
  TFCadastroUsuario = class(TFCadastroGeralPadrao)
    btnLogUsuario: TJvTransparentButton;
    ntbCadastro: TNotebook;
    edtNomeUsu: TDBEdit;
    medCPFUsu: TJvDBMaskEdit;
    dteDataCadastroUsu: TJvDBDateEdit;
    edtQualificacaoUsu: TDBEdit;
    edtMatriculaUsu: TDBEdit;
    chbFlgMasterUsu: TDBCheckBox;
    cbSexoUsu: TDBComboBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    dsUsuarioLog: TDataSource;
    qryUsuario: TFDQuery;
    qryUsuarioLog: TFDQuery;
    chbInativoUsu: TDBCheckBox;
    dteDataInativacaoUsu: TJvDBDateEdit;
    lblDataInativacaoUsu: TLabel;
    lcbFuncionario: TDBLookupComboBox;
    Funcion�rio: TLabel;
    qryFuncionarios: TFDQuery;
    dsFuncionarios: TDataSource;
    DBGrid1: TDBGrid;
    Label8: TLabel;
    Label9: TLabel;
    JvDateEdit1: TJvDateEdit;
    JvDateEdit2: TJvDateEdit;
    edtSenhaUsu: TEdit;
    procedure btnDadosPrincipaisClick(Sender: TObject);
    procedure btnLogUsuarioClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chbInativoUsuClick(Sender: TObject);
    procedure chbInativoUsuExit(Sender: TObject);
    procedure lcbFuncionarioExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  protected
    procedure MarcarDesmarcarDBCheckBox(ChB: TDBCheckBox); override;
    function VerificarDados(msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadastroUsuario: TFCadastroUsuario;

implementation

{$R *.dfm}

uses UDM, UGDM, UBibliotecaSistema, UVariaveisGlobais;

procedure TFCadastroUsuario.btnDadosPrincipaisClick(Sender: TObject);
begin
  inherited;

  ntbCadastro.PageIndex := 0;
end;

procedure TFCadastroUsuario.btnLogUsuarioClick(Sender: TObject);
begin
  inherited;

  ntbCadastro.PageIndex := 1;
end;

procedure TFCadastroUsuario.btnOkClick(Sender: TObject);
var
  QryUsu: TFDQuery;
begin
  inherited;

  if vgOperacao = C then
    Exit;

  if lPodeGravar then
  begin
    try
      if dmGerencial.conGERENCIAL.Connected then
        dmGerencial.conGERENCIAL.StartTransaction;

      QryUsu := BS.CriarFDQuery(nil, vgConGER);

      with QryUsu, SQL do
      begin
        Close;
        Clear;
        if vgOperacao = I then
        begin
          Text := 'INSERT INTO USUARIO (ID_USUARIO, NOME, CPF, SENHA, QUALIFICACAO, ' +
                  '                     FLG_MASTER, MATRICULA, FLG_SEXO, ' +
                  '                     ID_FUNCIONARIO_FK, FLG_ATIVO, DATA_INATIVACAO) ' +
                  '             VALUES (:ID_USUARIO, :NOME, :CPF, :SENHA, :QUALIFICACAO, ' +
                  '                     :FLG_MASTER, :MATRICULA, :FLG_SEXO, ' +
                  '                     :ID_FUNCIONARIO, :FLG_ATIVO, :DATA_INATIVACAO)';

          vgIdCosulta := BS.ProximoId('ID_USUARIO', 'USUARIO');
        end;

        if vgOperacao = E then
        begin
          Text := 'UPDATE USUARIO ' +
                  '   SET NOME              = :NOME, ' +
                  '       CPF               = :CPF, ' +
                  '       SENHA             = :SENHA, ' +
                  '       QUALIFICACAO      = :QUALIFICACAO, ' +
                  '       FLG_MASTER        = :FLG_MASTER, ' +
                  '       MATRICULA         = :MATRICULA, ' +
                  '       FLG_SEXO          = :FLG_SEXO, ' +
                  '       ID_FUNCIONARIO_FK = :ID_FUNCIONARIO, ' +
                  '       FLG_ATIVO         = :FLG_ATIVO, ' +
                  '       DATA_INATIVACAO   = :DATA_INATIVACAO ' +
                  ' WHERE ID_USUARIO = :ID_USUARIO';
        end;

        Params.ParamByName('ID_USUARIO').Value     := vgIdCosulta;
        Params.ParamByName('NOME').Value           := Trim(edtNomeUsu.Text);
        Params.ParamByName('CPF').Value            := Trim(medCPFUsu.Text);
        Params.ParamByName('SENHA').AsString       := edtSenhaUsu.Text;
        Params.ParamByName('QUALIFICACAO').Value   := Trim(edtQualificacaoUsu.Text);
        Params.ParamByName('FLG_MASTER').AsString  := IfThen(chbFlgMasterUsu.Checked, 'S', 'N');
        Params.ParamByName('MATRICULA').Value      := Trim(edtMatriculaUsu.Text);
        Params.ParamByName('FLG_SEXO').Value       := Trim(cbSexoUsu.Text);

        if Trim(lcbFuncionario.Text) = '' then
          Params.ParamByName('ID_FUNCIONARIO').Value := Null
        else
          Params.ParamByName('ID_FUNCIONARIO').Value := qryFuncionarios.FieldByName('ID_FUNCIONARIO').AsInteger;

        Params.ParamByName('FLG_ATIVO').AsString   := IfThen(chbInativoUsu.Checked, 'N', 'S');

        if chbInativoUsu.Checked then
          Params.ParamByName('DATA_INATIVACAO').Value := Date
        else
          Params.ParamByName('DATA_INATIVACAO').Value := Null;

        ExecSQL;
      end;

      if dmGerencial.conGERENCIAL.InTransaction then
        dmGerencial.conGERENCIAL.Commit;

      Self.Close;
    except
      if dmGerencial.conGERENCIAL.InTransaction then
        dmGerencial.conGERENCIAL.Rollback;
    end;

    FreeAndNil(QryUsu);
  end;
end;

procedure TFCadastroUsuario.chbInativoUsuClick(Sender: TObject);
begin
  inherited;

  MarcarDesmarcarDBCheckBox(chbInativoUsu);
end;

procedure TFCadastroUsuario.chbInativoUsuExit(Sender: TObject);
begin
  inherited;

  MarcarDesmarcarDBCheckBox(chbInativoUsu);
end;

procedure TFCadastroUsuario.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  vgOperacao  := VAZIO;
  vgIdCosulta := 0;
end;

procedure TFCadastroUsuario.FormCreate(Sender: TObject);
begin
  inherited;

  qryUsuario.Close;
  qryUsuario.Params.ParamByName('iIdUsuario').Value := vgIdCosulta;
  qryUsuario.Open;

  if vgOperacao = C then
  begin
    qryUsuarioLog.Close;
    qryUsuarioLog.Params.ParamByName('iIdUsuario').Value := vgIdCosulta;
    qryUsuarioLog.Open;
  end;
end;

procedure TFCadastroUsuario.FormShow(Sender: TObject);
begin
  inherited;

  if vgOperacao = I then
  begin
    chbFlgMasterUsu.Checked := False;
    chbInativoUsu.Visible   := False;

    lblDataInativacaoUsu.Visible := False;
    dteDataInativacaoUsu.Visible := False;

    dteDataCadastroUsu.Date := Date;
  end
  else
  begin
    chbInativoUsu.Visible := True;
    chbInativoUsu.Checked := (qryUsuario.FieldByName('FLG_ATIVO').AsString = 'N');

    lblDataInativacaoUsu.Visible := (qryUsuario.FieldByName('FLG_ATIVO').AsString = 'N');
    dteDataInativacaoUsu.Visible := (qryUsuario.FieldByName('FLG_ATIVO').AsString = 'N');

    edtSenhaUsu.Text := qryUsuario.FieldByName('SENHA').AsString;

    chbFlgMasterUsu.Checked := (qryUsuario.FieldByName('FLG_MASTER').AsString = 'S');
  end;

  qryFuncionarios.Close;
  qryFuncionarios.Open;
end;

procedure TFCadastroUsuario.lcbFuncionarioExit(Sender: TObject);
begin
  inherited;

  if Trim(lcbFuncionario.Text) <> '' then
  begin
    cbSexoUsu.Text       := qryFuncionarios.FieldByName('FLG_SEXO').AsString;
    medCPFUsu.EditText   := qryFuncionarios.FieldByName('CPF').AsString;
  end;
end;

procedure TFCadastroUsuario.MarcarDesmarcarDBCheckBox(ChB: TDBCheckBox);
begin
  inherited;

  if ChB = chbInativoUsu then
  begin
    lblDataInativacaoUsu.Visible := chbInativoUsu.Checked;
    dteDataInativacaoUsu.Visible := chbInativoUsu.Checked;

    if chbInativoUsu.Checked then
      dteDataInativacaoUsu.Date := Date;
  end;

end;

function TFCadastroUsuario.VerificarDados(msg: String): Boolean;
var
  LstErros: TStringList;
  i: Integer;
begin
  Result := True;

  LstErros := TStringList.Create;

  if Trim(edtNomeUsu.Text) = '' then
    LstErros.Add('O campo NOME � obrigat�rio.');

  if Trim(cbSexoUsu.Text) = '' then
    LstErros.Add('O campo SEXO � obrigat�rio.');

  if Trim(StringReplace(StringReplace(medCPFUsu.Text, '.', '', [rfReplaceAll, rfIgnoreCase]), '-', '', [rfReplaceAll, rfIgnoreCase])) = '' then
    LstErros.Add('O campo CPF � obrigat�rio.');

  if Trim(edtSenhaUsu.Text) = '' then
    LstErros.Add('O campo SENHA � obrigat�rio.');

  if LstErros.Count > 0 then
  begin
    for i := 0 to (LstErros.Count - 1) do
    begin
      if i = 0 then
        msg := LstErros[i]
      else
        msg := msg + #1310 + LstErros[i];
    end;

    msg    := PChar(msg);
    Result := False;
  end
  else
    msg := '';
end;

end.
