{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UListaItens.pas
  Descricao:   Lista Simples de Itens
  Author   :   Cristina
  Date:        27-mar-2017
  Last Update:
------------------------------------------------------------------------------------------------------------------------}

unit UListaProdutoServico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UListaSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.StdCtrls, System.StrUtils, UDM;

type
  TFListaProdutoServico = class(TFListaSimplesPadrao)
    lblDescricao: TLabel;
    edtDescricao: TEdit;
    qryGridListaPadraoID: TIntegerField;
    qryGridListaPadraoTIPO_ITEM: TStringField;
    qryGridListaPadraoDESCR_ITEM: TStringField;
    qryGridListaPadraoABREV_ITEM: TStringField;
    qryGridListaPadraoFLG_ATIVO: TStringField;
    qryGridListaPadraoDATA_INATIVACAO: TDateField;
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure dbgGridListaPadraoDblClick(Sender: TObject);
    procedure dbgGridListaPadraoTitleClick(Column: TColumn);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edtDescricaoKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }

    IdProdServ: Integer;

    TpProdServ: TTipoProdutoServico;
  end;

var
  FListaProdutoServico: TFListaProdutoServico;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UVariaveisGlobais, UGDM;

procedure TFListaProdutoServico.btnFiltrarClick(Sender: TObject);
begin
  inherited;

  with qryGridListaPadrao, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_ITEM AS ID, ' +
            '       TIPO_ITEM, ' +
            '       DESCR_ITEM, ' +
            '       ABREV_ITEM, ' +
            '       FLG_ATIVO, ' +
            '       DATA_INATIVACAO ' +
            '  FROM ITEM ' +
            ' WHERE FLG_ATIVO = ' + QuotedStr('S') +
            '   AND ID_ITEM <> ' + IntToStr(IdProdServ);

    if TpProdServ in [PROD, SERV] then
    begin
      Add(' AND TIPO_ITEM = :TIPO_ITEM');
      Params.ParamByName('TIPO_ITEM').Value := IfThen((TpProdServ = PROD), 'P', 'S');
    end;

    if Trim(edtDescricao.Text) <> '' then
    begin
      Add(' AND CAST(UPPER(DESCR_ITEM) AS VARCHAR(250)CHARACTER SET ISO8859_1) COLLATE PT_BR CONTAINING CAST(:DESCR_ITEM AS VARCHAR(250))');
      Params.ParamByName('DESCR_ITEM').Value := Trim(edtDescricao.Text);
    end;

    Add('ORDER BY DESCR_ITEM');
    Open;
  end;
end;

procedure TFListaProdutoServico.btnLimparClick(Sender: TObject);
begin

  inherited;

  if edtDescricao.CanFocus then
    edtDescricao.SetFocus;
end;

procedure TFListaProdutoServico.dbgGridListaPadraoDblClick(Sender: TObject);
var
  sTexto: String;
begin
  inherited;

  if qryGridListaPadrao.RecordCount = 0 then
    IdProdServ := 0
  else
  begin
    IdProdServ := qryGridListaPadrao.FieldByName('ID').AsInteger;

    sTexto := 'Confirma a sele��o do Item ' + qryGridListaPadrao.FieldByName('ID').AsString + '?';

    if Application.MessageBox(PChar(sTexto), 'Confirma��o', MB_YESNO + MB_ICONQUESTION) = ID_YES then
      Self.Close
    else
      IdProdServ := 0;
  end;
end;

procedure TFListaProdutoServico.dbgGridListaPadraoTitleClick(Column: TColumn);
begin
  inherited;

  qryGridListaPadrao.IndexFieldNames := Column.FieldName;
end;

procedure TFListaProdutoServico.edtDescricaoKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnFiltrar.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    btnFechar.Click;
end;

procedure TFListaProdutoServico.FormCreate(Sender: TObject);
begin
  inherited;

  IdProdServ := 0;
end;

procedure TFListaProdutoServico.FormShow(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;

  if edtDescricao.CanFocus then
    edtDescricao.SetFocus;
end;

end.
