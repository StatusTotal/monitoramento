{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UGDM.pas
  Descricao:   Data Module Geral
  Author   :   Cristina
  Date:        11-dez-2015
  Last Update:
------------------------------------------------------------------------------------------------------------------------}

unit UGDM;

interface

uses
  System.SysUtils, System.Classes, Xml.xmldom, Xml.XMLIntf, Xml.Win.msxmldom,
  Xml.XMLDoc, System.ImageList, Vcl.ImgList, Vcl.Controls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, FireDAC.Comp.UI, FireDAC.Phys.IBBase,
  System.Generics.Collections, System.Variants;

type
  { SELO }
  TDadosClasseSelo = class(TObject)
  public
    Letra:              String;
    Numero:             Integer;
    Aleatorio:          String;
    CCT:                String;
    IdCompra:           Integer;
    IdSerie:            Integer;
    Status:             String;
    Sistema:            String;
    IdReservado:        Integer;
    Reservado:          String;
    DataReservado:      TDateTime;
    IdUsuarioReservado: Integer;
    UsuarioReservado:   String;
    FormReservado:      String;
    FormUtilizado:      String;
    IdAto:              Integer;
    IdReferencia:       Integer;
    Atribuicao:         Integer;
    DataPratica:        TDateTime;
    Livro:              String;
    FolhaInicial:       Integer;
    FolhaFinal:         Integer;
    NumeroAto:          Integer;
    Protocolo:          Integer;
    Recibo:             Integer;
    Codigo:             Integer;
    Conjunto:           String;
    Natureza:           String;
    Escrevente:         String;
    Convenio:           String;
    TipoCobranca:       String;
    Emolumentos:        Currency;
    FETJ:               Currency;
    FUNDPERJ:           Currency;
    FUNPERJ:            Currency;
    FUNARPEN:           Currency;
    PMCMV:              Currency;
    MUTUA:              Currency;
    ACOTERJ:            Currency;
    Distribuicao:       Currency;
    Indisponibilidade:  Currency;
    Prenotacao:         Currency;
    AR:                 Currency;
    Bancaria:           Currency;
    Total:              Currency;

    constructor Create(); virtual;
    destructor  Destroy; override;
  end;

  TClasseSelo = class(TDadosClasseSelo)
  public
    Letra:              String;
    Numero:             Integer;
    Aleatorio:          String;
    CCT:                String;
    IdCompra:           Integer;
    IdSerie:            Integer;
    Status:             String;
    Sistema:            String;
    IdReservado:        Integer;
    Reservado:          String;
    DataReservado:      TDateTime;
    IdUsuarioReservado: Integer;
    UsuarioReservado:   String;
    FormReservado:      String;
    FormUtilizado:      String;
    IdAto:              Integer;
    IdReferencia:       Integer;
    Atribuicao:         Integer;
    DataPratica:        TDateTime;
    Livro:              String;
    FolhaInicial:       Integer;
    FolhaFinal:         Integer;
    NumeroAto:          Integer;
    Protocolo:          Integer;
    Recibo:             Integer;
    Codigo:             Integer;
    Conjunto:           String;
    Natureza:           String;
    Escrevente:         String;
    Convenio:           String;
    TipoCobranca:       String;
    Emolumentos:        Currency;
    FETJ:               Currency;
    FUNDPERJ:           Currency;
    FUNPERJ:            Currency;
    FUNARPEN:           Currency;
    PMCMV:              Currency;
    MUTUA:              Currency;
    ACOTERJ:            Currency;
    Distribuicao:       Currency;
    Indisponibilidade:  Currency;
    Prenotacao:         Currency;
    AR:                 Currency;
    Bancaria:           Currency;
    Total:              Currency;
  end;

  TSelo = class of TDadosClasseSelo;

  TdmGerencial = class(TDataModule)
    conGERENCIAL: TFDConnection;
    FDPhysFBDriverLink1: TFDPhysFBDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    dsSelo: TDataSource;
    qrySelo: TFDQuery;

    procedure ReservarSelo(var Selo: String; var Aleatorio: String; Formulario: String);
    procedure CancelarReservaSelo(Selo, Aleatorio: String);
    procedure UtilizarSelo(var Selo: String; Aleatorio: String; Formulario: String);

    procedure InicializarComponenteSelo;
    procedure FinalizarComponenteSelo;
  private
    { Private declarations }

    procedure CarregarDadosSelo(Qry: TFDQuery);

    function VerificarStatusSelo(Selo, Aleatorio: String): Boolean;

  public
    { Public declarations }

    function PegarLetraSelo(Selo: String): String;
    function PegarNumeroSelo(Selo: String): Integer;
    function MontarSelo: String;
  end;

var
  dmGerencial: TdmGerencial;

  { SELO }
  Selo: TSelo;
  DadosSelo: TDadosClasseSelo;
  ListaSelo: TList<TDadosClasseSelo>;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses UBibliotecaSistema, UVariaveisGlobais;

{$R *.dfm}

{ TdmGerencial }

procedure TdmGerencial.CancelarReservaSelo(Selo, Aleatorio: String);
var
  QrySelo: TFDQuery;
begin
  //Retorna o Selo reservado ao status de LIVRE
  QrySelo := BS.CriarFDQuery(nil, conGERENCIAL);

  try
    if conGERENCIAL.Connected then
      conGERENCIAL.StartTransaction;

    with QrySelo, SQL do
    begin
      Close;
      Clear;

      Text := 'UPDATE SELOS_CCT ' +
              '   SET STATUS = :STATUS, ' +
              '       SISTEMA = :SISTEMA, ' +
              '       ID_RESERVADO = :ID_RESERVADO, ' +
              '       RESERVADO = :RESERVADO, ' +
              '       DATA_RESERVADO = :DATA_RESERVADO, ' +
              '       ID_USUARIO_RESERVADO = :ID_USUARIO_RESERVADO, ' +
              '       USUARIO_RESERVADO = :USUARIO_RESERVADO, ' +
              '       FORM_RESERVADO = :FORM_RESERVADO, ' +
              '       FORM_UTILIZADO = :FORM_UTILIZADO, ' +
              '       ID_ATO = :ID_ATO, ' +
              '       ID_REFERENCIA = :ID_REFERENCIA, ' +
              '       ATRIBUICAO = :ATRIBUICAO, ' +
              '       DATA_PRATICA = :DATA_PRATICA, ' +
              '       LIVRO = :LIVRO, ' +
              '       FOLHA_INICIAL = :FOLHA_INICIAL, ' +
              '       FOLHA_FINAL = :FOLHA_FINAL, ' +
              '       NUMERO_ATO = :NUMERO_ATO, ' +
              '       PROTOCOLO = :PROTOCOLO, ' +
              '       RECIBO = :RECIBO, ' +
              '       CODIGO = :CODIGO, ' +
              '       CONJUNTO = :CONJUNTO, ' +
              '       NATUREZA = :NATUREZA, ' +
              '       ESCREVENTE = :ESCREVENTE, ' +
              '       CONVENIO = :CONVENIO, ' +
              '       TIPOCOBRANCA = :TIPOCOBRANCA, ' +
              '       EMOLUMENTOS = :EMOLUMENTOS, ' +
              '       FETJ = :FETJ, ' +
              '       FUNDPERJ = :FUNDPERJ, ' +
              '       FUNPERJ = :FUNPERJ, ' +
              '       FUNARPEN = :FUNARPEN, ' +
              '       PMCMV = :PMCMV, ' +
              '       MUTUA = :MUTUA, ' +
              '       ACOTERJ = :ACOTERJ, ' +
              '       DISTRIBUICAO = :DISTRIBUICAO, ' +
              '       INDISPONIBILIDADE = :INDISPONIBILIDADE, ' +
              '       PRENOTACAO = :PRENOTACAO, ' +
              '       AR = :AR, ' +
              '       BANCARIA = :BANCARIA, ' +
              '       TOTAL = :TOTAL ' +
              ' WHERE LETRA = :LETRA ' +
              '   AND NUMERO = :NUMERO ' +
              '   AND ALEATORIO = :ALEATORIO';

      ParamByName('LETRA').Value                := PegarLetraSelo(Selo);
      ParamByName('NUMERO').Value               := PegarNumeroSelo(Selo);
      ParamByName('ALEATORIO').Value            := Aleatorio;
      ParamByName('STATUS').Value               := 'LIVRE';
      ParamByName('SISTEMA').Value              := Null;
      ParamByName('ID_RESERVADO').Value         := Null;
      ParamByName('RESERVADO').Value            := Null;
      ParamByName('DATA_RESERVADO').Value       := Null;
      ParamByName('ID_USUARIO_RESERVADO').Value := Null;
      ParamByName('USUARIO_RESERVADO').Value    := Null;
      ParamByName('FORM_RESERVADO').Value       := Null;
      ParamByName('FORM_UTILIZADO').Value       := Null;
      ParamByName('ID_ATO').Value               := Null;
      ParamByName('ID_REFERENCIA').Value        := Null;
      ParamByName('ATRIBUICAO').Value           := Null;
      ParamByName('DATA_PRATICA').Value         := Null;
      ParamByName('LIVRO').Value                := Null;
      ParamByName('FOLHA_INICIAL').Value        := Null;
      ParamByName('FOLHA_FINAL').Value          := Null;
      ParamByName('NUMERO_ATO').Value           := Null;
      ParamByName('PROTOCOLO').Value            := Null;
      ParamByName('RECIBO').Value               := Null;
      ParamByName('CODIGO').Value               := Null;
      ParamByName('CONJUNTO').Value             := Null;
      ParamByName('NATUREZA').Value             := Null;
      ParamByName('ESCREVENTE').Value           := Null;
      ParamByName('CONVENIO').Value             := Null;
      ParamByName('TIPOCOBRANCA').Value         := Null;
      ParamByName('EMOLUMENTOS').Value          := 0;
      ParamByName('FETJ').Value                 := 0;
      ParamByName('FUNDPERJ').Value             := 0;
      ParamByName('FUNPERJ').Value              := 0;
      ParamByName('FUNARPEN').Value             := 0;
      ParamByName('PMCMV').Value                := 0;
      ParamByName('MUTUA').Value                := 0;
      ParamByName('ACOTERJ').Value              := 0;
      ParamByName('DISTRIBUICAO').Value         := 0;
      ParamByName('INDISPONIBILIDADE').Value    := 0;
      ParamByName('PRENOTACAO').Value           := 0;
      ParamByName('AR').Value                   := 0;
      ParamByName('BANCARIA').Value             := 0;
      ParamByName('TOTAL').Value                := 0;

      ExecSQL;
    end;

    if conGERENCIAL.Connected then
      conGERENCIAL.Commit;
  except
    if conGERENCIAL.Connected then
      conGERENCIAL.Rollback;
  end;

  FreeAndNil(QrySelo);
end;

procedure TdmGerencial.CarregarDadosSelo(Qry: TFDQuery);
begin
  with Qry do
  begin
    DadosSelo.Letra              := FieldByName('LETRA').AsString;
    DadosSelo.Numero             := FieldByName('NUMERO').AsInteger;
    DadosSelo.Aleatorio          := FieldByName('ALEATORIO').AsString;
    DadosSelo.CCT                := FieldByName('CCT').AsString;
    DadosSelo.IdCompra           := FieldByName('ID_COMPRA').AsInteger;
    DadosSelo.IdSerie            := FieldByName('ID_SERIE').AsInteger;
    DadosSelo.Status             := FieldByName('STATUS').AsString;
    DadosSelo.Sistema            := FieldByName('SISTEMA').AsString;
    DadosSelo.IdReservado        := FieldByName('ID_RESERVADO').AsInteger;
    DadosSelo.Reservado          := FieldByName('RESERVADO').AsString;
    DadosSelo.DataReservado      := FieldByName('DATA_RESERVADO').AsDateTime;
    DadosSelo.IdUsuarioReservado := FieldByName('ID_USUARIO_RESERVADO').AsInteger;
    DadosSelo.UsuarioReservado   := FieldByName('USUARIO_RESERVADO').AsString;
    DadosSelo.FormReservado      := FieldByName('FORM_RESERVADO').AsString;
    DadosSelo.FormUtilizado      := FieldByName('FORM_UTILIZADO').AsString;
    DadosSelo.IdAto              := FieldByName('ID_ATO').AsInteger;
    DadosSelo.IdReferencia       := FieldByName('ID_REFERENCIA').AsInteger;
    DadosSelo.Atribuicao         := FieldByName('ATRIBUICAO').AsInteger;
    DadosSelo.DataPratica        := FieldByName('DATA_PRATICA').AsDateTime;
    DadosSelo.Livro              := FieldByName('LIVRO').AsString;
    DadosSelo.FolhaInicial       := FieldByName('FOLHA_INICIAL').AsInteger;
    DadosSelo.FolhaFinal         := FieldByName('FOLHA_FINAL').AsInteger;
    DadosSelo.NumeroAto          := FieldByName('NUMERO_ATO').AsInteger;
    DadosSelo.Protocolo          := FieldByName('PROTOCOLO').AsInteger;
    DadosSelo.Recibo             := FieldByName('RECIBO').AsInteger;
    DadosSelo.Codigo             := FieldByName('CODIGO').AsInteger;
    DadosSelo.Conjunto           := FieldByName('CONJUNTO').AsString;
    DadosSelo.Natureza           := FieldByName('NATUREZA').AsString;
    DadosSelo.Escrevente         := FieldByName('ESCREVENTE').AsString;
    DadosSelo.Convenio           := FieldByName('CONVENIO').AsString;
    DadosSelo.TipoCobranca       := FieldByName('TIPOCOBRANCA').AsString;
    DadosSelo.Emolumentos        := FieldByName('EMOLUMENTOS').AsInteger;
    DadosSelo.FETJ               := FieldByName('FETJ').AsInteger;
    DadosSelo.FUNDPERJ           := FieldByName('FUNDPERJ').AsInteger;
    DadosSelo.FUNPERJ            := FieldByName('FUNPERJ').AsInteger;
    DadosSelo.FUNARPEN           := FieldByName('FUNARPEN').AsInteger;
    DadosSelo.PMCMV              := FieldByName('PMCMV').AsInteger;
    DadosSelo.MUTUA              := FieldByName('MUTUA').AsInteger;
    DadosSelo.ACOTERJ            := FieldByName('ACOTERJ').AsInteger;
    DadosSelo.Distribuicao       := FieldByName('DISTRIBUICAO').AsInteger;
    DadosSelo.Indisponibilidade  := FieldByName('INDISPONIBILIDADE').AsInteger;
    DadosSelo.Prenotacao         := FieldByName('PRENOTACAO').AsInteger;
    DadosSelo.AR                 := FieldByName('AR').AsInteger;
    DadosSelo.Bancaria           := FieldByName('BANCARIA').AsInteger;
    DadosSelo.Total              := FieldByName('TOTAL').AsInteger;
  end;
end;

procedure TdmGerencial.FinalizarComponenteSelo;
begin
  FreeAndNil(ListaSelo);
end;

function TdmGerencial.MontarSelo: String;
begin
  Result := DadosSelo.Letra + IntToStr(DadosSelo.Numero);
end;

procedure TdmGerencial.InicializarComponenteSelo;
begin
  Selo      := TClasseSelo;
  ListaSelo := TList<TDadosClasseSelo>.Create;

  DadosSelo := Selo.Create;
end;

function TdmGerencial.PegarLetraSelo(Selo: String): String;
var
  i: Integer;
  l: String;
begin
  l := '';

  if Trim(Selo) = '' then
  begin
    l := '';
    Exit;
  end;

  for i := 0 to Length(Selo) do
    if not CharInSet(Selo[i], ['0'..'9']) then
      l := l + Selo[i];

  Result := Trim(l);
end;

function TdmGerencial.PegarNumeroSelo(Selo: String): Integer;
var
  i: Integer;
  n: String;
begin
  if Selo = '' then
  begin
      Result := 0;
      Exit;
  end;

  for i := 0 to Length(Selo) do
    if CharInSet(Selo[i], ['0'..'9']) then
      n := n + Selo[i];

  if n <> '' then
    Result := StrToInt(n)
  else
    Result := 0;
end;

procedure TdmGerencial.ReservarSelo(var Selo: String; var Aleatorio: String; Formulario: String);
var
  QrySelo: TFDQuery;
begin
  //Atribui o status de RESERVADO ao Selo
  QrySelo := BS.CriarFDQuery(nil, conGERENCIAL);

  try
    if conGERENCIAL.Connected then
      conGERENCIAL.StartTransaction;

    with QrySelo, SQL do
    begin
      Close;
      Clear;

      Text := 'SELECT FIRST 1 * ' +
              '  FROM SELOS_CCT ' +
              ' WHERE STATUS = ' + QuotedStr('LIVRE') +
              'ORDER BY LETRA ASC, ' +
              '         NUMERO ASC ' +
              'FOR UPDATE WITH LOCK';

      Open;

      CarregarDadosSelo(QrySelo);

      Close;
      Clear;

      Text := 'UPDATE SELOS_CCT ' +
              '   SET STATUS = :STATUS, ' +
              '       SISTEMA = :SISTEMA, ' +
              '       ID_RESERVADO = :ID_RESERVADO, ' +
              '       RESERVADO = :RESERVADO, ' +
              '       DATA_RESERVADO = :DATA_RESERVADO, ' +
              '       ID_USUARIO_RESERVADO = :ID_USUARIO_RESERVADO, ' +
              '       USUARIO_RESERVADO = :USUARIO_RESERVADO, ' +
              '       FORM_RESERVADO = :FORM_RESERVADO' +
              ' WHERE LETRA = :LETRA ' +
              '   AND NUMERO = :NUMERO ' +
              '   AND ALEATORIO = :ALEATORIO';

      DadosSelo.Status               := 'RESERVADO';
      DadosSelo.Sistema              := 'RCPN';
      DadosSelo.IdReservado          := BS.ValorGeneratorDBX('ID_RESERVADO', conGERENCIAL);
      DadosSelo.Reservado            := 'S';
      DadosSelo.DataReservado        := Date;
      DadosSelo.IdUsuarioReservado   := vgUsu_Id;
      DadosSelo.UsuarioReservado     := vgUsu_Nome;
      DadosSelo.FormReservado        := Formulario;

      Selo      := MontarSelo;
      Aleatorio := DadosSelo.Aleatorio;

      Params.ParamByName('STATUS').Value               := DadosSelo.Status;
      Params.ParamByName('SISTEMA').Value              := DadosSelo.Sistema;
      Params.ParamByName('ID_RESERVADO').Value         := DadosSelo.IdReservado;
      Params.ParamByName('RESERVADO').Value            := DadosSelo.Reservado;
      Params.ParamByName('DATA_RESERVADO').Value       := DadosSelo.DataReservado;
      Params.ParamByName('ID_USUARIO_RESERVADO').Value := DadosSelo.IdUsuarioReservado;
      Params.ParamByName('USUARIO_RESERVADO').Value    := DadosSelo.UsuarioReservado;
      Params.ParamByName('FORM_RESERVADO').Value       := DadosSelo.FormReservado;
      Params.ParamByName('LETRA').Value                := DadosSelo.Letra;
      Params.ParamByName('NUMERO').Value               := DadosSelo.Numero;
      Params.ParamByName('ALEATORIO').Value            := DadosSelo.Aleatorio;

      ExecSQL;
    end;

    if conGERENCIAL.Connected then
      conGERENCIAL.Commit;
  except
    if conGERENCIAL.Connected then
      conGERENCIAL.Rollback;
  end;

  FreeAndNil(QrySelo);
end;

procedure TdmGerencial.UtilizarSelo(var Selo: String; Aleatorio: String; Formulario: String);
begin
  //Atribui o status de UTILIZADO ao Selo previamente reservado
  //verificar se o selo continua com status reservado (VerificarStatusSelo)
  //se sim, passar para utilizado e gravar tudo o que for necessario E result True
  //se n�o, gravar log de erro, reservar outro selo e tentar gravar esse novo selo como utilizado;
end;

function TdmGerencial.VerificarStatusSelo(Selo, Aleatorio: String): Boolean;
begin
  //
end;

{ TDadosClasseSelo }

constructor TDadosClasseSelo.Create;
begin
  Letra              := '';
  Numero             := 0;
  Aleatorio          := '';
  CCT                := '';
  IdCompra           := 0;
  IdSerie            := 0;
  Status             := '';
  Sistema            := '';
  IdReservado        := 0;
  Reservado          := '';
  DataReservado      := 0;
  IdUsuarioReservado := 0;
  UsuarioReservado   := '';
  FormReservado      := '';
  FormUtilizado      := '';
  IdAto              := 0;
  IdReferencia       := 0;
  Atribuicao         := 0;
  DataPratica        := 0;
  Livro              := '';
  FolhaInicial       := 0;
  FolhaFinal         := 0;
  NumeroAto          := 0;
  Protocolo          := 0;
  Recibo             := 0;
  Codigo             := 0;
  Conjunto           := '';
  Natureza           := '';
  Escrevente         := '';
  Convenio           := '';
  TipoCobranca       := '';
  Emolumentos        := 0;
  FETJ               := 0;
  FUNDPERJ           := 0;
  FUNPERJ            := 0;
  FUNARPEN           := 0;
  PMCMV              := 0;
  MUTUA              := 0;
  ACOTERJ            := 0;
  Distribuicao       := 0;
  Indisponibilidade  := 0;
  Prenotacao         := 0;
  AR                 := 0;
  Bancaria           := 0;
  Total              := 0;
end;

destructor TDadosClasseSelo.Destroy;
begin

  inherited;
end;

end.
