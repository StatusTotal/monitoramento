{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroRelatorioRepasses.pas
  Descricao:   Filtro Relatorio de Repasses: ISS, MUTUA e ACOTERJ (R06 - R06DET)
  Author   :   Cristina
  Date:        24-nov-2016
  Last Update: 11-set-2017 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroRelatorioRepasses;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroRelatorioPadrao, Vcl.StdCtrls,
  JvExControls, JvButton, JvTransparentButton, Vcl.ExtCtrls, Vcl.Mask, JvExMask,
  JvToolEdit, FireDAC.Stan.Param, System.Math;

type
  TFFiltroRelatorioRepasses = class(TFFiltroRelatorioPadrao)
    lblPeriodo: TLabel;
    lblPeriodoInicio: TLabel;
    dteDataInicio: TJvDateEdit;
    lblPeriodoFim: TLabel;
    dteDataFim: TJvDateEdit;
    gbTipoRepasse: TGroupBox;
    chbFUNDPERJ: TCheckBox;
    chbFUNPERJ: TCheckBox;
    chbFUNARPEN: TCheckBox;
    chbFETJ: TCheckBox;
    chbISS: TCheckBox;
    chbMUTUA: TCheckBox;
    chbACOTERJ: TCheckBox;
    procedure btnLimparClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    procedure GravarLog; override;
    procedure GerarRelatorio(ImpDet: Boolean); override;
    function VerificarFiltro: Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroRelatorioRepasses: TFFiltroRelatorioRepasses;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UDMRelatorios, UGDM, UVariaveisGlobais;

{ TFFiltroRelatorioRepasses }

procedure TFFiltroRelatorioRepasses.btnLimparClick(Sender: TObject);
begin
  inherited;

  dteDataInicio.Date := Date;
  dteDataFim.Date    := Date;

  chbFUNDPERJ.Checked := True;
  chbFUNPERJ.Checked  := True;
  chbFUNARPEN.Checked := True;
  chbFETJ.Checked     := True;
  chbISS.Checked      := True;
  chbMUTUA.Checked    := True;
  chbACOTERJ.Checked  := True;

  chbTipoSintetico.Checked := True;
  chbTipoAnalitico.Checked := False;

  chbExportarPDF.Checked := False;

  chbExportarExcel.Enabled := vgPrm_ExpRelRep;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioRepasses.btnVisualizarClick(Sender: TObject);
begin
  inherited;

  if not VerificarFiltro then
    Exit;

  //Depositos
  dmRelatorios.qryRel06_Totais.Close;

  dmRelatorios.qryRel06_Totais.Params.ParamByName('DATA_INI1').Value := dteDataInicio.Date;
  dmRelatorios.qryRel06_Totais.Params.ParamByName('DATA_FIM1').Value := dteDataFim.Date;
  dmRelatorios.qryRel06_Totais.Params.ParamByName('DATA_INI2').Value := dteDataInicio.Date;
  dmRelatorios.qryRel06_Totais.Params.ParamByName('DATA_FIM2').Value := dteDataFim.Date;
  dmRelatorios.qryRel06_Totais.Params.ParamByName('DATA_INI3').Value := dteDataInicio.Date;
  dmRelatorios.qryRel06_Totais.Params.ParamByName('DATA_FIM3').Value := dteDataFim.Date;
  dmRelatorios.qryRel06_Totais.Params.ParamByName('DATA_INI4').Value := dteDataInicio.Date;
  dmRelatorios.qryRel06_Totais.Params.ParamByName('DATA_FIM4').Value := dteDataFim.Date;
  dmRelatorios.qryRel06_Totais.Params.ParamByName('DATA_INI5').Value := dteDataInicio.Date;
  dmRelatorios.qryRel06_Totais.Params.ParamByName('DATA_FIM5').Value := dteDataFim.Date;
  dmRelatorios.qryRel06_Totais.Params.ParamByName('DATA_INI6').Value := dteDataInicio.Date;
  dmRelatorios.qryRel06_Totais.Params.ParamByName('DATA_FIM6').Value := dteDataFim.Date;
  dmRelatorios.qryRel06_Totais.Params.ParamByName('DATA_INI7').Value := dteDataInicio.Date;
  dmRelatorios.qryRel06_Totais.Params.ParamByName('DATA_FIM7').Value := dteDataFim.Date;

  dmRelatorios.qryRel06_Totais.Params.ParamByName('EXIBIR1').Value := IfThen(chbACOTERJ.Checked, 1, 0);   //ACOTERJ
  dmRelatorios.qryRel06_Totais.Params.ParamByName('EXIBIR2').Value := IfThen(chbFETJ.Checked, 1, 0);      //FETJ
  dmRelatorios.qryRel06_Totais.Params.ParamByName('EXIBIR3').Value := IfThen(chbFUNPERJ.Checked, 1, 0);   //FUNPERJ
  dmRelatorios.qryRel06_Totais.Params.ParamByName('EXIBIR4').Value := IfThen(chbMUTUA.Checked, 1, 0);     //MUTUA
  dmRelatorios.qryRel06_Totais.Params.ParamByName('EXIBIR5').Value := IfThen(chbISS.Checked, 1, 0);       //ISS
  dmRelatorios.qryRel06_Totais.Params.ParamByName('EXIBIR6').Value := IfThen(chbFUNDPERJ.Checked, 1, 0);  //FUNDPERJ
  dmRelatorios.qryRel06_Totais.Params.ParamByName('EXIBIR7').Value := IfThen(chbFUNARPEN.Checked, 1, 0);  //FUNARPEN

  dmRelatorios.qryRel06_Totais.Open;

  if dmRelatorios.qryRel06_Totais.RecordCount = 0 then
  begin
    Application.MessageBox('N�o h� Repasses registrados.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
  end
  else
  begin
    if chbTipoSintetico.Checked then
      GerarRelatorio(False);

    if chbTipoAnalitico.Checked then
      GerarRelatorio(True);
  end;
end;

procedure TFFiltroRelatorioRepasses.FormShow(Sender: TObject);
begin
  inherited;

  dteDataInicio.Date := Date;
  dteDataFim.Date    := Date;

  chbFUNDPERJ.Checked := True;
  chbFUNPERJ.Checked  := True;
  chbFUNARPEN.Checked := True;
  chbFETJ.Checked     := True;
  chbISS.Checked      := True;
  chbMUTUA.Checked    := True;
  chbACOTERJ.Checked  := True;

  chbTipoSintetico.Checked := True;
  chbTipoAnalitico.Checked := False;

  chbExportarPDF.Checked := False;
  
  chbExportarExcel.Enabled := vgPrm_ExpRelRep;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;

  chbExibirGrafico.Visible := False;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioRepasses.GerarRelatorio(ImpDet: Boolean);
begin
  inherited;

  try
    sTituloRelatorio    := 'REPASSES';
    sSubtituloRelatorio := 'Per�odo de ' + DateToStr(dteDataInicio.Date) + ' a ' + DateToStr(dteDataFim.Date);

    if ImpDet then
    begin
      lExibeDetalhe := True;

      lExibirFUNDPERJ := False;
      lExibirFUNPERJ  := False;
      lExibirFUNARPEN := False;
      lExibirFETJ     := False;
      lExibirISS      := False;
      lExibirMUTUA    := False;
      lExibirACOTERJ  := False;

      dmRelatorios.DefinirRelatorio(R06DET);

      dmRelatorios.qryRel06_Acoterj.Close;
      dmRelatorios.qryRel06_FETJ.Close;
      dmRelatorios.qryRel06_FUNPERJ.Close;
      dmRelatorios.qryRel06_Mutua.Close;
      dmRelatorios.qryRel06_ISS.Close;
      dmRelatorios.qryRel06_FUNDPERJ.Close;
      dmRelatorios.qryRel06_FUNARPEN.Close;

      //ACOTERJ
      if chbACOTERJ.Checked then
      begin
        dmRelatorios.qryRel06_Acoterj.Params.ParamByName('DATA_INI').Value := dteDataInicio.Date;
        dmRelatorios.qryRel06_Acoterj.Params.ParamByName('DATA_FIM').Value := dteDataFim.Date;
        dmRelatorios.qryRel06_Acoterj.Open;

        lExibirACOTERJ := True;
      end;

      //FETJ
      if chbFETJ.Checked then
      begin
        dmRelatorios.qryRel06_FETJ.Params.ParamByName('DATA_INI').Value := dteDataInicio.Date;
        dmRelatorios.qryRel06_FETJ.Params.ParamByName('DATA_FIM').Value := dteDataFim.Date;
        dmRelatorios.qryRel06_FETJ.Open;

        lExibirFETJ := True;
      end;

      //FUNPERJ
      if chbFUNPERJ.Checked then
      begin
        dmRelatorios.qryRel06_FUNPERJ.Params.ParamByName('DATA_INI').Value := dteDataInicio.Date;
        dmRelatorios.qryRel06_FUNPERJ.Params.ParamByName('DATA_FIM').Value := dteDataFim.Date;
        dmRelatorios.qryRel06_FUNPERJ.Open;

        lExibirFUNPERJ := True;
      end;

      //MUTUA
      if chbMUTUA.Checked then
      begin
        dmRelatorios.qryRel06_Mutua.Params.ParamByName('DATA_INI').Value := dteDataInicio.Date;
        dmRelatorios.qryRel06_Mutua.Params.ParamByName('DATA_FIM').Value := dteDataFim.Date;
        dmRelatorios.qryRel06_Mutua.Open;

        lExibirMUTUA := True;
      end;

      //ISS
      if chbISS.Checked then
      begin
        dmRelatorios.qryRel06_ISS.Params.ParamByName('DATA_INI').Value := dteDataInicio.Date;
        dmRelatorios.qryRel06_ISS.Params.ParamByName('DATA_FIM').Value := dteDataFim.Date;
        dmRelatorios.qryRel06_ISS.Open;

        lExibirISS := True;
      end;

      //FUNDPERJ
      if chbFUNDPERJ.Checked then
      begin
        dmRelatorios.qryRel06_FUNDPERJ.Params.ParamByName('DATA_INI').Value := dteDataInicio.Date;
        dmRelatorios.qryRel06_FUNDPERJ.Params.ParamByName('DATA_FIM').Value := dteDataFim.Date;
        dmRelatorios.qryRel06_FUNDPERJ.Open;

        lExibirFUNDPERJ := True;
      end;

      //FUNARPEN
      if chbFUNARPEN.Checked then
      begin
        dmRelatorios.qryRel06_FUNARPEN.Params.ParamByName('DATA_INI').Value := dteDataInicio.Date;
        dmRelatorios.qryRel06_FUNARPEN.Params.ParamByName('DATA_FIM').Value := dteDataFim.Date;
        dmRelatorios.qryRel06_FUNARPEN.Open;

        lExibirFUNARPEN := True;
      end;
    end
    else
    begin
      lExibeDetalhe := False;
      dmRelatorios.DefinirRelatorio(R06);
    end;

    dmRelatorios.ImprimirRelatorio(chbExportarPDF.Checked, chbExportarExcel.Checked);

    GravarLog;
  except
    Application.MessageBox('Erro ao gerar relat�rio de Repasses.',
                           'Erro',
                           MB_OK + MB_ICONERROR);
  end;
end;

procedure TFFiltroRelatorioRepasses.GravarLog;
begin
  inherited;

  BS.GravarUsuarioLog(50,
                      '',
                      'Impress�o de Repasses (' + DateToStr(dDataRelatorio) +
                                              ' ' +
                                              TimeToStr(tHoraRelatorio) + ')',
                      0,
                      '');
end;

function TFFiltroRelatorioRepasses.VerificarFiltro: Boolean;
var
  Msg: String;
begin
  Result := True;

  if dteDataInicio.Date = 0 then
  begin
    Msg := '- Informe a DATA DE IN�CIO do Per�odo.';
    Result := False;
  end;

  if dteDataFim.Date = 0 then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe a DATA DE FIM do Per�odo.'
    else
      Msg := #13#10 + '- Informe a DATA DE FIM do Per�odo.';

    Result := False;
  end;

  if dteDataFim.Date < dteDataInicio.Date then
  begin
    if Trim(Msg) = '' then
      Msg := '- A DATA DE FIM do Per�odo n�o pode inferior � DATA DE IN�CIO do mesmo.'
    else
      Msg := #13#10 + '- A DATA DE FIM do Per�odo n�o pode ser inferior � DATA DE IN�CIO do mesmo.';

    Result := False;
  end;

  if (not chbFUNDPERJ.Checked) and
    (not chbFUNPERJ.Checked) and
    (not chbFUNARPEN.Checked) and
    (not chbFETJ.Checked) and
    (not chbISS.Checked) and
    (not chbMUTUA.Checked) and
    (not chbACOTERJ.Checked) then
  begin
    if Trim(Msg) = '' then
      Msg := '- Selecione, pelo menos, um Tipo de Repasse.'
    else
      Msg := #13#10 + '- Selecione, pelo menos, um Tipo de Repasse.';

    Result := False;
  end;

  if (not chbTipoAnalitico.Checked) and
    (not chbTipoSintetico.Checked) then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe o TIPO do Relat�rio de Repasses.'
    else
      Msg := #13#10 + '- Informe o TIPO do Relat�rio de Repasses.';

    Result := False;
  end;

  if not Result then
    Application.MessageBox(PChar(':: ATEN��O ::' + #13#10 + #13#10 + Msg),
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
end;

end.
