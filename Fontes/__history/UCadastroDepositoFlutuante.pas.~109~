{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroDepositoFlutuante.pas
  Descricao:   Cadastro de Dep�sitos Flutuantes
  Author   :   Cristina
  Date:        07-nov-2016
  Last Update: 02-abr-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroDepositoFlutuante;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.DBClient, Datasnap.Provider,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.DBCtrls, JvToolEdit,
  JvDBControls, Vcl.Mask, JvExMask, JvBaseEdits, JvExStdCtrls, JvCombobox,
  JvDBCombobox;

type
  TFCadastroDepositoFlutuante = class(TFCadastroGeralPadrao)
    lblDataDeposito: TLabel;
    lblNumCodDeposito: TLabel;
    lblDescricaoDeposito: TLabel;
    lblValorDeposito: TLabel;
    lblSituacao: TLabel;
    lblDataCadastro: TLabel;
    cedValorDeposito: TJvDBCalcEdit;
    dteDataDeposito: TJvDBDateEdit;
    dteDataCadastro: TJvDBDateEdit;
    edtNumCodDeposito: TDBEdit;
    edtDescricaoDeposito: TDBEdit;
    qryDepFlu: TFDQuery;
    dspDepFlu: TDataSetProvider;
    cdsDepFlu: TClientDataSet;
    cdsDepFluID_DEPOSITO_FLUTUANTE: TIntegerField;
    cdsDepFluDESCR_DEPOSITO_FLUTUANTE: TStringField;
    cdsDepFluDATA_DEPOSITO: TDateField;
    cdsDepFluNUM_COD_DEPOSITO: TStringField;
    cdsDepFluVR_DEPOSITO_FLUTUANTE: TBCDField;
    cdsDepFluFLG_STATUS: TStringField;
    cbSituacao: TJvDBComboBox;
    cdsDepFluCAD_ID_USUARIO: TIntegerField;
    cdsDepFluDATA_CADASTRO: TSQLTimeStampField;
    cdsDepFluFLUT_ID_USUARIO: TIntegerField;
    cdsDepFluBAIXA_ID_USUARIO: TIntegerField;
    cdsDepFluCANCEL_ID_USUARIO: TIntegerField;
    cdsDepFluDATA_FLUTUANTE: TDateField;
    cdsDepFluDATA_BAIXADO: TDateField;
    cdsDepFluDATA_CANCELAMENTO: TDateField;
    dteDataFlutuante: TJvDBDateEdit;
    lblDataFlutuante: TLabel;
    dteDataBaixa: TJvDBDateEdit;
    lblDataBaixa: TLabel;
    dteDataCancelamento: TJvDBDateEdit;
    lblDataCancelamento: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cedValorDepositoKeyPress(Sender: TObject; var Key: Char);
    procedure cdsDepFluVR_DEPOSITO_FLUTUANTEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure cbSituacaoKeyPress(Sender: TObject; var Key: Char);
    procedure cbSituacaoClick(Sender: TObject);
    procedure cbSituacaoExit(Sender: TObject);
    procedure dteDataFlutuanteExit(Sender: TObject);
    procedure dteDataFlutuanteKeyPress(Sender: TObject; var Key: Char);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }

    procedure MudaSituacaoDeposito;
  public
    { Public declarations }
  end;

var
  FCadastroDepositoFlutuante: TFCadastroDepositoFlutuante;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{ TFCadastroDepositoFlutuante }

procedure TFCadastroDepositoFlutuante.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroDepositoFlutuante.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroDepositoFlutuante.cbSituacaoClick(Sender: TObject);
begin
  inherited;

  MudaSituacaoDeposito;
end;

procedure TFCadastroDepositoFlutuante.cbSituacaoExit(Sender: TObject);
begin
  inherited;

  MudaSituacaoDeposito;
end;

procedure TFCadastroDepositoFlutuante.cbSituacaoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dteDataFlutuante.Enabled then
      SelectNext(ActiveControl, True, True)
    else
      btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroDepositoFlutuante.cdsDepFluVR_DEPOSITO_FLUTUANTEGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFCadastroDepositoFlutuante.cedValorDepositoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if vgOperacao = I then
      SelectNext(ActiveControl, True, True)
    else
      btnOk.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroDepositoFlutuante.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtNumCodDeposito.MaxLength    := 25;
  edtDescricaoDeposito.MaxLength := 250;
end;

procedure TFCadastroDepositoFlutuante.DesabilitarComponentes;
begin
  inherited;

  dteDataCadastro.Enabled     := False;
  dteDataFlutuante.Enabled    := False;
  dteDataBaixa.Enabled        := False;
  dteDataCancelamento.Enabled := False;

  if vgOperacao = C then
    pnlDados.Enabled := False;
end;

procedure TFCadastroDepositoFlutuante.dteDataFlutuanteExit(Sender: TObject);
begin
  inherited;

  if cdsDepFlu.State in [dsInsert, dsEdit] then
  begin
    if (dteDataBaixa.Date = 0) or
      (dteDataBaixa.Date = Null) then
    begin
      if dteDataFlutuante.Date < dteDataCadastro.Date then
        dteDataFlutuante.Date := vgDataLancVigente;
    end
    else
    begin
      if dteDataBaixa.Date < dteDataFlutuante.Date then
        cdsDepFlu.FieldByName('DATA_FLUTUANTE').AsDateTime := dteDataBaixa.Date
      else
      begin
        if dteDataFlutuante.Date < dteDataCadastro.Date then
          dteDataFlutuante.Date := vgDataLancVigente;
      end;
    end;
  end;
end;

procedure TFCadastroDepositoFlutuante.dteDataFlutuanteKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if cdsDepFlu.State in [dsInsert, dsEdit] then
    begin
      if (dteDataBaixa.Date = 0) or
        (dteDataBaixa.Date = Null) then
      begin
        if dteDataFlutuante.Date < dteDataCadastro.Date then
          dteDataFlutuante.Date := vgDataLancVigente;
      end
      else
      begin
        if dteDataBaixa.Date < dteDataFlutuante.Date then
          cdsDepFlu.FieldByName('DATA_FLUTUANTE').AsDateTime := dteDataBaixa.Date
        else
        begin
          if dteDataFlutuante.Date < dteDataCadastro.Date then
            dteDataFlutuante.Date := vgDataLancVigente;
        end;
      end;
    end;

    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    btnCancelar.Click;
end;

procedure TFCadastroDepositoFlutuante.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  vgOperacao     := VAZIO;
  vgIdConsulta   := 0;
  vgOrigemFiltro := False;

  inherited;
end;

procedure TFCadastroDepositoFlutuante.FormCreate(Sender: TObject);
begin
  inherited;

  if vgOperacao in [I, E] then
  begin
    cbSituacao.Items.BeginUpdate;
    cbSituacao.Items.Clear;
    cbSituacao.Items.Add('A COMPENSAR');
    cbSituacao.Values.Add('P');
    cbSituacao.Items.Add('FLUTUANTE');
    cbSituacao.Values.Add('F');
    cbSituacao.Items.EndUpdate;
  end
  else
  begin
    cbSituacao.Items.BeginUpdate;
    cbSituacao.Items.Clear;
    cbSituacao.Items.Add('A COMPENSAR');
    cbSituacao.Values.Add('P');
    cbSituacao.Items.Add('FLUTUANTE');
    cbSituacao.Values.Add('F');
    cbSituacao.Items.Add('BAIXADO');
    cbSituacao.Values.Add('B');
    cbSituacao.Items.Add('CANCELADO');
    cbSituacao.Items.Add('C');
    cbSituacao.Items.EndUpdate;
  end;

  cdsDepFlu.Close;
  cdsDepFlu.Params.ParamByName('ID_DEPOSITO_FLUTUANTE').Value := vgIdConsulta;
  cdsDepFlu.Open;

  if vgOperacao = I then
  begin
    cdsDepFlu.Append;
    cdsDepFlu.FieldByName('DATA_DEPOSITO').AsDateTime  := Date;
    cdsDepFlu.FieldByName('FLG_STATUS').AsString       := 'F';
    cdsDepFlu.FieldByName('DATA_CADASTRO').AsDateTime  := Date;
    cdsDepFlu.FieldByName('DATA_FLUTUANTE').AsDateTime := vgDataLancVigente;
    cdsDepFlu.FieldByName('CAD_ID_USUARIO').AsInteger  := vgUsu_Id;
    cdsDepFlu.FieldByName('FLUT_ID_USUARIO').AsInteger := vgUsu_Id;
  end;

  if vgOperacao = E then
    cdsDepFlu.Edit;
end;

procedure TFCadastroDepositoFlutuante.FormShow(Sender: TObject);
begin
  inherited;

  if vgOperacao = E then
  begin
    if (Trim(cdsDepFlu.FieldByName('FLG_STATUS').AsString) = 'B') and
      (cdsDepFlu.FieldByName('DATA_FLUTUANTE').IsNull) then
    begin
      cbSituacao.Items.BeginUpdate;
      cbSituacao.Items.Add('BAIXADO');
      cbSituacao.Values.Add('B');
      cbSituacao.Items.EndUpdate;

      dteDataDeposito.Enabled      := False;
      edtNumCodDeposito.Enabled    := False;
      edtDescricaoDeposito.Enabled := False;
      cedValorDeposito.Enabled     := False;
      cbSituacao.Enabled           := False;
      dteDataFlutuante.Enabled     := True;
    end;
  end;

  if dteDataDeposito.CanFocus then
    dteDataDeposito.SetFocus
  else if dteDataFlutuante.CanFocus then
    dteDataFlutuante.SetFocus;
end;

function TFCadastroDepositoFlutuante.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroDepositoFlutuante.GravarDadosGenerico(
  var Msg: String): Boolean;
begin
  Result := True;
  Msg := '';

  if vgOperacao = C then
    Exit;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    if cdsDepFlu.State in [dsInsert, dsEdit] then
    begin
      if vgOperacao = I then
        cdsDepFlu.FieldByName('ID_DEPOSITO_FLUTUANTE').AsInteger := BS.ProximoId('ID_DEPOSITO_FLUTUANTE',
                                                                                 'DEPOSITO_FLUTUANTE');

{      if Trim(cdsDepFlu.FieldByName('FLG_STATUS').AsString) = 'P' then
      begin
        cdsDepFlu.FieldByName('FLUT_ID_USUARIO').Value := Null;
        cdsDepFlu.FieldByName('DATA_FLUTUANTE').Value  := Null;
      end
      else if Trim(cdsDepFlu.FieldByName('FLG_STATUS').AsString) = 'F' then
      begin
        if cdsDepFlu.FieldByName('DATA_FLUTUANTE').IsNull then
          cdsDepFlu.FieldByName('DATA_FLUTUANTE').AsDateTime := Date;
      end;  }

      MudaSituacaoDeposito;

      cdsDepFlu.Post;
      cdsDepFlu.ApplyUpdates(0);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    vgLanc_CriarNovo := True;

    Msg := 'Dep�sito Flutuante gravado com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na grava��o do Dep�sito Flutuante.';
  end;
end;

procedure TFCadastroDepositoFlutuante.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta de Dep�sito Flutuante',
                          vgIdConsulta, 'DEPOSITO_FLUTUANTE');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclus�o de Dep�sito Flutuante',
                          vgIdConsulta, 'DEPOSITO_FLUTUANTE');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO',
                                  'Por favor, indique o motivo da edi��o desse Dep�sito Flutuante:',
                                  '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dep�sito Flutuante';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          vgIdConsulta, 'DEPOSITO_FLUTUANTE');
    end;
  end;
end;

procedure TFCadastroDepositoFlutuante.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroDepositoFlutuante.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

procedure TFCadastroDepositoFlutuante.MudaSituacaoDeposito;
begin
  if vgOperacao = I then
  begin
    if cbSituacao.ItemIndex = 0 then  //A Compensar
    begin
      cdsDepFlu.FieldByName('FLG_STATUS').AsString   := 'P';
      cdsDepFlu.FieldByName('DATA_FLUTUANTE').Value  := Null;
      cdsDepFlu.FieldByName('FLUT_ID_USUARIO').Value := Null;

      dteDataFlutuante.Enabled := False;
    end
    else if cbSituacao.ItemIndex = 1 then  //Flutuante
    begin
      cdsDepFlu.FieldByName('FLG_STATUS').AsString       := 'F';
      cdsDepFlu.FieldByName('DATA_FLUTUANTE').AsDateTime := vgDataLancVigente;
      cdsDepFlu.FieldByName('FLUT_ID_USUARIO').AsInteger := vgUsu_Id;

      dteDataFlutuante.Enabled := True;
    end;
  end
  else if vgOperacao = E then
  begin
    if cbSituacao.ItemIndex = 0 then  //A Compensar
    begin
      cdsDepFlu.FieldByName('FLG_STATUS').AsString   := 'P';
      cdsDepFlu.FieldByName('FLUT_ID_USUARIO').Value := Null;
      cdsDepFlu.FieldByName('DATA_FLUTUANTE').Value  := Null;

      dteDataFlutuante.Enabled := False;
    end
    else if cbSituacao.ItemIndex = 1 then  //Flutuante
    begin
      cdsDepFlu.FieldByName('FLG_STATUS').AsString       := 'F';

      if cdsDepFlu.FieldByName('DATA_FLUTUANTE').IsNull then
      begin
        dteDataFlutuante.Enabled := True;

        cdsDepFlu.FieldByName('FLUT_ID_USUARIO').AsInteger := vgUsu_Id;

        if cdsDepFlu.FieldByName('DATA_BAIXADO').IsNull then
          cdsDepFlu.FieldByName('DATA_FLUTUANTE').AsDateTime := vgDataLancVigente
        else
          cdsDepFlu.FieldByName('DATA_FLUTUANTE').AsDateTime := cdsDepFlu.FieldByName('DATA_BAIXADO').AsDateTime;
      end;
    end;
  end;
end;

function TFCadastroDepositoFlutuante.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if dteDataDeposito.Date = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a DATA DO DEP�SITO.';
    DadosMsgErro.Componente := dteDataDeposito;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtNumCodDeposito.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar a IDENTIFICA��O DO DEP�SITO.';
    DadosMsgErro.Componente := edtNumCodDeposito;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if cedValorDeposito.Value <= 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta informar o VALOR DO DEP�SITO.';
    DadosMsgErro.Componente := cedValorDeposito;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroDepositoFlutuante.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

end.
