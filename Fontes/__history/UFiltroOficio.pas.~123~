{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroOficio.pas
  Descricao:   Tela de filtro de Oficios Recebidos e Enviados
  Author   :   Cristina
  Date:        12-jan-2017
  Last Update: 11-out-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroOficio;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.DBCtrls, Vcl.Mask, JvExMask, JvToolEdit,
  System.DateUtils;

type
  TFFiltroOficio = class(TFFiltroSimplesPadrao)
    lblPeriodo: TLabel;
    Label1: TLabel;
    lblPessoaOficio: TLabel;
    dtePeriodoIni: TJvDateEdit;
    dtePeriodoFim: TJvDateEdit;
    rgTipoOficio: TRadioGroup;
    edtPessoaOficio: TEdit;
    lblOrgaoOficio: TLabel;
    edtOrgaoOficio: TEdit;
    lblFormaEnvio: TLabel;
    lcbFormaEnvio: TDBLookupComboBox;
    edtNumProtocolo: TEdit;
    lblNumProtocolo: TLabel;
    gbControle: TGroupBox;
    lblNumOficio: TLabel;
    edtNumOficio: TEdit;
    edtAnoOficio: TEdit;
    lblAnoOficio: TLabel;
    lblNumControle: TLabel;
    edtNumControle: TEdit;
    lblNumProcesso: TLabel;
    edtNumProcesso: TEdit;
    Label3: TLabel;
    qryOfcFrmEnv: TFDQuery;
    dsOfcFrmEnv: TDataSource;
    gbLegenda: TGroupBox;
    lblLegendaSituacaoOfc: TLabel;
    lblLegendaUsado: TLabel;
    spLegendaUsado: TShape;
    lblLegendaReservado: TLabel;
    spLegendaReservado: TShape;
    qryGridPaiPadraoID: TIntegerField;
    qryGridPaiPadraoCOD_OFICIO: TIntegerField;
    qryGridPaiPadraoANO_OFICIO: TIntegerField;
    qryGridPaiPadraoNUM_OFICIO: TStringField;
    qryGridPaiPadraoTIPO_OFICIO: TStringField;
    qryGridPaiPadraoDATA_OFICIO: TDateField;
    qryGridPaiPadraoHORA_OFICIO: TTimeField;
    qryGridPaiPadraoID_FUNCIONARIO_FK: TIntegerField;
    qryGridPaiPadraoNOME_FUNCIONARIO: TStringField;
    qryGridPaiPadraoCAD_ID_USUARIO: TIntegerField;
    qryGridPaiPadraoDATA_CADASTRO: TSQLTimeStampField;
    qryGridPaiPadraoFLG_STATUS: TStringField;
    qryGridPaiPadraoSTA_ID_USUARIO: TIntegerField;
    qryGridPaiPadraoDATA_STATUS: TSQLTimeStampField;
    qryGridPaiPadraoASSUNTO_OFICIO: TStringField;
    qryGridPaiPadraoID_OFICIO_PESSOA_FK: TIntegerField;
    qryGridPaiPadraoNOME_OFICIO_PESSOA: TStringField;
    qryGridPaiPadraoNUM_PROCESSO: TStringField;
    qryGridPaiPadraoID_OFICIO_ORGAO_FK: TIntegerField;
    qryGridPaiPadraoNOME_OFICIO_ORGAO: TStringField;
    qryGridPaiPadraoID_OFICIO_FORMAENVIO_FK: TIntegerField;
    qryGridPaiPadraoDESCR_OFICIO_FORMAENVIO: TStringField;
    qryGridPaiPadraoNUM_RASTREIO: TStringField;
    qryGridPaiPadraoNUM_PROTOCOLO: TStringField;
    qryGridPaiPadraoDATA_SITUACAO_AR: TDateField;
    qryGridPaiPadraoFLG_SITUACAO_AR: TStringField;
    qryGridPaiPadraoOBS_OFICIO: TStringField;
    qryGridPaiPadraoID_OFICIO_FK: TIntegerField;
    qryGridPaiPadraoSEQ_OFICIO: TIntegerField;
    btnEnviarResposta: TJvTransparentButton;
    btnReceberResposta: TJvTransparentButton;
    procedure btnEditarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure dbgGridPaiPadraoTitleClick(Column: TColumn);
    procedure dtePeriodoFimKeyPress(Sender: TObject; var Key: Char);
    procedure dtePeriodoIniKeyPress(Sender: TObject; var Key: Char);
    procedure edtNumControleKeyPress(Sender: TObject; var Key: Char);
    procedure edtNumProtocoloKeyPress(Sender: TObject; var Key: Char);
    procedure edtNumProcessoKeyPress(Sender: TObject; var Key: Char);
    procedure edtNumOficioKeyPress(Sender: TObject; var Key: Char);
    procedure edtAnoOficioKeyPress(Sender: TObject; var Key: Char);
    procedure edtPessoaOficioKeyPress(Sender: TObject; var Key: Char);
    procedure edtOrgaoOficioKeyPress(Sender: TObject; var Key: Char);
    procedure lcbFormaEnvioKeyPress(Sender: TObject; var Key: Char);
    procedure rgTipoOficioClick(Sender: TObject);
    procedure rgTipoOficioExit(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dbgGridPaiPadraoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure qryGridPaiPadraoAfterScroll(DataSet: TDataSet);
    procedure btnEnviarRespostaClick(Sender: TObject);
    procedure btnReceberRespostaClick(Sender: TObject);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }

    procedure AbrirOficios;
    procedure MontaQueryOficioControle;
    procedure MontaQueryOficioPeriodo;
  public
    { Public declarations }
  end;

var
  FFiltroOficio: TFFiltroOficio;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMOficio,
  UCadastroOficio, UOpcoesInclusaoOficio;

{ TFFiltroOficio }

procedure TFFiltroOficio.AbrirOficios;
begin
  if (Trim(edtNumControle.Text) = '') and
    (dtePeriodoIni.Date = 0) and
    (dtePeriodoFim.Date > 0) then
  begin
    Application.MessageBox('Selecione pelo menos o Per�odo de Recebimento/Envio ou o N� de Controle para realizar a busca.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);

    if dtePeriodoIni.CanFocus then
      dtePeriodoIni.SetFocus;

    Exit;
  end;

  qryGridPaiPadrao.Close;
  qryGridPaiPadrao.SQL.Clear;

  qryGridPaiPadrao.SQL.Text := 'SELECT O.ID_OFICIO AS ID, ' +
                               '       O.COD_OFICIO, ' +
                               '       O.ANO_OFICIO, ' +
                               '       O.TIPO_OFICIO, ' +
                               '       O.DATA_OFICIO, ' +
                               '       O.HORA_OFICIO, ' +
                               '       O.ID_FUNCIONARIO_FK, ' +
                               '       F.NOME_FUNCIONARIO, ' +
                               '       O.CAD_ID_USUARIO, ' +
                               '       O.DATA_CADASTRO, ' +
                               '       O.FLG_STATUS, ' +
                               '       O.STA_ID_USUARIO, ' +
                               '       O.DATA_STATUS, ' +
                               '       O.ASSUNTO_OFICIO, ' +
                               '       O.ID_OFICIO_PESSOA_FK, ' +
                               '       OP.NOME_OFICIO_PESSOA, ' +
                               '       (O.COD_OFICIO || ' + QuotedStr('/') + ' || O.ANO_OFICIO) AS NUM_OFICIO, ' +
                               '       O.NUM_PROCESSO, ' +
                               '       O.ID_OFICIO_ORGAO_FK, ' +
                               '       OO.NOME_OFICIO_ORGAO, ' +
                               '       O.ID_OFICIO_FORMAENVIO_FK, ' +
                               '       OFE.DESCR_OFICIO_FORMAENVIO, ' +
                               '       O.NUM_RASTREIO, ' +
                               '       O.NUM_PROTOCOLO, ' +
                               '       O.DATA_SITUACAO_AR, ' +
                               '       O.FLG_SITUACAO_AR, ' +
                               '       O.OBS_OFICIO, ' +
                               '       O.ID_OFICIO_FK, ' +
                               '       O.SEQ_OFICIO ' +
                               '  FROM OFICIO O ' +
                               '  LEFT JOIN FUNCIONARIO F ' +
                               '    ON O.ID_FUNCIONARIO_FK = F.ID_FUNCIONARIO ' +
                               '  LEFT JOIN OFICIO_PESSOA OP ' +
                               '    ON O.ID_OFICIO_PESSOA_FK = OP.ID_OFICIO_PESSOA ' +
                               '  LEFT JOIN OFICIO_ORGAO OO ' +
                               '    ON O.ID_OFICIO_ORGAO_FK = OO.ID_OFICIO_ORGAO ' +
                               '  LEFT JOIN OFICIO_FORMAENVIO OFE ' +
                               '    ON O.ID_OFICIO_FORMAENVIO_FK = OFE.ID_OFICIO_FORMAENVIO ';

  if Trim(edtNumControle.Text) <> '' then
    MontaQueryOficioControle;

  if (dtePeriodoIni.Date > 0) and
    (dtePeriodoFim.Date > 0) then
    MontaQueryOficioPeriodo;

  qryGridPaiPadrao.SQL.Add('ORDER BY O.ID_OFICIO DESC, ' +
                           '         O.DATA_OFICIO, ' +
                           '         O.HORA_OFICIO');

  qryGridPaiPadrao.Open;

  if edtNumControle.CanFocus then
    edtNumControle.SetFocus;
end;

procedure TFFiltroOficio.btnEditarClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao     := E;
  vgIdConsulta   := qryGridPaiPadrao.FieldByName('ID').AsInteger;
  vgOrigemFiltro := True;

  if not qryGridPaiPadrao.FieldByName('ID_OFICIO_FK').IsNull then
    vgOfc_IdAssoc := qryGridPaiPadrao.FieldByName('ID_OFICIO_FK').AsInteger;

  vgOfc_Codigo := qryGridPaiPadrao.Fields.FieldByName('COD_OFICIO').AsInteger;
  vgOfc_Ano    := qryGridPaiPadrao.Fields.FieldByName('ANO_OFICIO').AsInteger;
  vgOfc_Status := qryGridPaiPadrao.Fields.FieldByName('FLG_STATUS').AsString;

  try
    Application.CreateForm(TdmOficio, dmOficio);
    Application.CreateForm(TFCadastroOficio, FCadastroOficio);

    if qryGridPaiPadrao.FieldByName('TIPO_OFICIO').AsString = 'E' then  //Envio
      TpOfc := ENV
    else if qryGridPaiPadrao.FieldByName('TIPO_OFICIO').AsString = 'R' then  //Recebimento
      TpOfc := RCB;

    FCadastroOficio.ShowModal;
  finally
    FCadastroOficio.Free;
    FreeAndNil(dmOficio);

    vgOrigemCadastro := False;
  end;

  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroOficio.btnEnviarRespostaClick(Sender: TObject);
begin
  inherited;

  vgOperacao     := I;
  vgIdConsulta   := 0;
  vgOrigemFiltro := True;

  vgOfc_TpOfc   := 'R';
  vgOfc_IdAssoc := qryGridPaiPadrao.FieldByName('ID').AsInteger;

  try
    Application.CreateForm(TdmOficio, dmOficio);
    Application.CreateForm(TFCadastroOficio, FCadastroOficio);

    TpOfc := ENV;

    FCadastroOficio.ShowModal;
  finally
    FCadastroOficio.Free;
    FreeAndNil(dmOficio);
  end;

  btnFiltrar.Click;
end;

procedure TFFiltroOficio.btnExcluirClick(Sender: TObject);
var
  qryExclusao: TFDQuery;
begin
  inherited;

  if lPodeExcluir then
  begin
    qryExclusao := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
    Application.CreateForm(TdmOficio, dmOficio);

    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      { IMAGENS }
      dmOficio.cdsOficioImg_F.Close;
      dmOficio.cdsOficioImg_F.Params.ParamByName('ID_OFICIO').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
      dmOficio.cdsOficioImg_F.Open;

      dmOficio.cdsOficioImg_F.First;

      while not dmOficio.cdsOficioImg_F.Eof do
      begin
        if FileExists(vgConf_DiretorioImagens +
                      IntToStr(vgOfc_Ano) +
                      '\' + dmOficio.cdsOficioImg_F.FieldByName('NOME_FIXO').AsString +
                      dmOficio.cdsOficioImg_F.FieldByName('NOME_VARIAVEL').AsString +
                      '.jpg') then
        begin
          DeleteFile(vgConf_DiretorioImagens +
                      IntToStr(vgOfc_Ano) +
                     '\' + dmOficio.cdsOficioImg_F.FieldByName('NOME_FIXO').AsString +
                     dmOficio.cdsOficioImg_F.FieldByName('NOME_VARIAVEL').AsString +
                     '.jpg');
        end;

        dmOficio.cdsOficioImg_F.Next;
      end;

      with qryExclusao, SQL do
      begin
        Close;
        Clear;
        Text := 'DELETE FROM OFICIO_IMG WHERE ID_OFICIO_FK = :iIdOficio';
        Params.ParamByName('iIdOficio').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;
      end;

      { OFICIO }
      with qryExclusao, SQL do
      begin
        Close;
        Clear;
        Text := 'DELETE FROM OFICIO WHERE ID_OFICIO = :iIdOficio';
        Params.ParamByName('iIdOficio').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      GravarLog;
      Application.MessageBox('Of�cio exclu�do com sucesso!', 'Aviso', MB_OK)
    except
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Application.MessageBox('Erro na exclus�o do Of�cio.', 'Erro', MB_OK + MB_ICONERROR)
    end;

    FreeAndNil(dmOficio);
    FreeAndNil(qryExclusao);

    btnFiltrar.Click;
  end;
end;

procedure TFFiltroOficio.btnFiltrarClick(Sender: TObject);
begin
  AbrirOficios;

  inherited;
end;

procedure TFFiltroOficio.btnIncluirClick(Sender: TObject);
var
  iOpcao: Integer;  { 1 = Novo Envio / 2 = Envio (Resposta) / 3 = Novo Recebimento / 4 = Recebimento (Resposta) }
begin
  inherited;

{  iOpcao := 0;

  try
    Application.CreateForm(TFOpcoesInclusaoOficio, FOpcoesInclusaoOficio);
    FOpcoesInclusaoOficio.ShowModal;
  finally
    iOpcao := FOpcoesInclusaoOficio.iOp;
    FOpcoesInclusaoOficio.Free;
  end;

  if iOpcao = 0 then
    Exit;

  vgOperacao     := I;
  vgIdConsulta   := 0;
  vgOrigemFiltro := True;

  try
    Application.CreateForm(TdmOficio, dmOficio);
    Application.CreateForm(TFCadastroOficio, FCadastroOficio);

    if iOpcao in [1, 2] then  //Envio
      TpOfc := ENV
    else if iOpcao in [3, 4] then  //Recebimento
      TpOfc := RCB;

    FCadastroOficio.ShowModal;
  finally
    FCadastroOficio.Free;
    FreeAndNil(dmOficio);
  end;

  btnFiltrar.Click;  }
end;

procedure TFFiltroOficio.btnLimparClick(Sender: TObject);
begin
  edtNumControle.Clear;

  rgTipoOficio.ItemIndex := 0;
  dtePeriodoIni.Date := 0;
  dtePeriodoFim.Date := 0;
  edtNumProtocolo.Clear;
  edtNumProcesso.Clear;
  edtNumOficio.Clear;
  edtAnoOficio.Clear;

  edtPessoaOficio.Clear;
  edtOrgaoOficio.Clear;
  lcbFormaEnvio.KeyValue := Null;

  inherited;

  if edtNumControle.CanFocus then
    edtNumControle.SetFocus;
end;

procedure TFFiltroOficio.btnReceberRespostaClick(Sender: TObject);
begin
  inherited;

  vgOperacao     := I;
  vgIdConsulta   := 0;
  vgOrigemFiltro := True;

  vgOfc_TpOfc   := 'E';
  vgOfc_IdAssoc := qryGridPaiPadrao.FieldByName('ID').AsInteger;

  try
    Application.CreateForm(TdmOficio, dmOficio);
    Application.CreateForm(TFCadastroOficio, FCadastroOficio);

    TpOfc := RCB;

    FCadastroOficio.ShowModal;
  finally
    FCadastroOficio.Free;
    FreeAndNil(dmOficio);
  end;

  btnFiltrar.Click;
end;

procedure TFFiltroOficio.dbgGridPaiPadraoDblClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao     := C;
  vgIdConsulta   := qryGridPaiPadrao.FieldByName('ID').AsInteger;
  vgOrigemFiltro := True;

  if not qryGridPaiPadrao.FieldByName('ID_OFICIO_FK').IsNull then
    vgOfc_IdAssoc := qryGridPaiPadrao.FieldByName('ID_OFICIO_FK').AsInteger;

  vgOfc_Codigo := qryGridPaiPadrao.Fields.FieldByName('COD_OFICIO').AsInteger;
  vgOfc_Ano    := qryGridPaiPadrao.Fields.FieldByName('ANO_OFICIO').AsInteger;
  vgOfc_Status := qryGridPaiPadrao.Fields.FieldByName('FLG_STATUS').AsString;

  try
    Application.CreateForm(TdmOficio, dmOficio);
    Application.CreateForm(TFCadastroOficio, FCadastroOficio);

    if qryGridPaiPadrao.FieldByName('TIPO_OFICIO').AsString = 'E' then  //Envio
      TpOfc := ENV
    else if qryGridPaiPadrao.FieldByName('TIPO_OFICIO').AsString = 'R' then  //Recebimento
      TpOfc := RCB;

    FCadastroOficio.ShowModal;
  finally
    FCadastroOficio.Free;
    FreeAndNil(dmOficio);
  end;

  DefinirPosicaoGrid;
end;

procedure TFFiltroOficio.dbgGridPaiPadraoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Grid: TDBGrid;
  Row: Integer;
begin
  inherited;

  Grid := Sender as TDBGrid;
  Row  := Grid.DataSource.DataSet.RecNo;

  if gdSelected in State then
    Grid.Canvas.Brush.Color := clSkyBlue
  else
  begin
    if qryGridPaiPadrao.FieldByName('TIPO_OFICIO').AsString = 'E' then
      Grid.Canvas.Brush.Color := $00F5E1CF
    else if qryGridPaiPadrao.FieldByName('TIPO_OFICIO').AsString = 'R' then
      Grid.Canvas.Brush.Color := $00B9CBFF;
  end;

  Grid.DefaultDrawColumnCell(Rect, DataCol, Column, State);

  //Legenda
  if qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString = 'R' then  //Reservado
  begin
    dbgGridPaiPadrao.Canvas.Font.Color := clMaroon;
    dbgGridPaiPadrao.Canvas.FillRect(Rect);
    dbgGridPaiPadrao.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end
  else if qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString = 'U' then  //Usado
  begin
    dbgGridPaiPadrao.Canvas.Font.Color := clGreen;
    dbgGridPaiPadrao.Canvas.FillRect(Rect);
    dbgGridPaiPadrao.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TFFiltroOficio.dbgGridPaiPadraoTitleClick(Column: TColumn);
begin
  inherited;

  qryGridPaiPadrao.IndexFieldNames := Column.FieldName;
end;

procedure TFFiltroOficio.dtePeriodoFimKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    if dtePeriodoFim.Date = 0 then
    begin
      dtePeriodoIni.Date := 0;
      SelectNext(ActiveControl, True, True);
    end
    else
    begin
      if dtePeriodoIni.Date = 0 then
        dtePeriodoIni.Date := dtePeriodoFim.Date
      else if dtePeriodoFim.Date < dtePeriodoIni.Date then
        dtePeriodoFim.Date := dtePeriodoIni.Date;

      edtNumControle.Clear;

      SelectNext(ActiveControl, True, True);
    end;

    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dtePeriodoFim.Date = 0 then
    begin
      dtePeriodoIni.Date := 0;
      SelectNext(ActiveControl, True, True);
    end
    else
    begin
      if dtePeriodoIni.Date = 0 then
        dtePeriodoIni.Date := dtePeriodoFim.Date
      else if dtePeriodoFim.Date < dtePeriodoIni.Date then
        dtePeriodoFim.Date := dtePeriodoIni.Date;

      edtNumControle.Clear;

      btnFiltrar.Click;
    end;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroOficio.dtePeriodoIniKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    if dtePeriodoIni.Date = 0 then
      dtePeriodoFim.Date := 0
    else
    begin
      if (dtePeriodoFim.Date = 0) or
        (dtePeriodoFim.Date < dtePeriodoIni.Date) then
        dtePeriodoFim.Date := dtePeriodoIni.Date;

      edtNumControle.Clear;
    end;

    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dtePeriodoIni.Date = 0 then
    begin
      dtePeriodoFim.Date := 0;
      SelectNext(ActiveControl, True, True);
    end
    else
    begin
      if (dtePeriodoFim.Date = 0) or
        (dtePeriodoFim.Date < dtePeriodoIni.Date) then
        dtePeriodoFim.Date := dtePeriodoIni.Date;

      edtNumControle.Clear;

      btnFiltrar.Click;
    end;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroOficio.edtAnoOficioKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroOficio.edtNumControleKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    if Trim(edtNumControle.Text) <> '' then
    begin
      rgTipoOficio.ItemIndex := 0;
      dtePeriodoIni.Date := 0;
      dtePeriodoFim.Date := 0;
      edtNumProtocolo.Clear;
      edtNumProcesso.Clear;

      edtPessoaOficio.Clear;
      edtOrgaoOficio.Clear;
      lcbFormaEnvio.KeyValue := Null;
    end;

    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if Trim(edtNumControle.Text) = '' then
      SelectNext(ActiveControl, True, True)
    else
    begin
      rgTipoOficio.ItemIndex := 0;
      dtePeriodoIni.Date := 0;
      dtePeriodoFim.Date := 0;
      edtNumProtocolo.Clear;
      edtNumProcesso.Clear;

      edtPessoaOficio.Clear;
      edtOrgaoOficio.Clear;
      lcbFormaEnvio.KeyValue := Null;

      btnFiltrar.Click;
    end;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroOficio.edtNumOficioKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroOficio.edtNumProcessoKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroOficio.edtNumProtocoloKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroOficio.edtOrgaoOficioKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroOficio.edtPessoaOficioKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroOficio.FormShow(Sender: TObject);
begin
  edtNumControle.Clear;

  rgTipoOficio.ItemIndex := 0;
  dtePeriodoIni.Date := StartOfTheMonth(Date);
  dtePeriodoFim.Date := Date;
  edtNumProtocolo.Clear;
  edtNumProcesso.Clear;
  edtNumOficio.Clear;
  edtAnoOficio.Clear;

  edtPessoaOficio.Clear;
  edtOrgaoOficio.Clear;
  lcbFormaEnvio.KeyValue := Null;

  AbrirOficios;

  inherited;

  btnIncluir.Visible  := False;
  btnImprimir.Visible := False;

  if edtNumControle.CanFocus then
    edtNumControle.SetFocus;
end;

procedure TFFiltroOficio.GravarLog;
var
  sObservacao, sTipo: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    X:  //EXCLUSAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da exclus�o desse Of�cio:', '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(3, '', sObservacao);
    end;
    P:  //IMPRESSAO
    begin
      BS.GravarUsuarioLog(3, '', 'Somente impress�o de Lista de Of�cios');
    end;
    T:  //EXPORTACAO
    begin
      BS.GravarUsuarioLog(3, '', 'Somente exporta��o de Lista de Of�cios');
    end;
  end;
end;

procedure TFFiltroOficio.lcbFormaEnvioKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroOficio.MontaQueryOficioControle;
begin
  //CONTROLE
  qryGridPaiPadrao.SQL.Add(' WHERE O.ID_OFICIO = :ID_OFICIO ');

  qryGridPaiPadrao.Params.ParamByName('ID_OFICIO').Value := StrToInt(edtNumControle.Text);
end;

procedure TFFiltroOficio.MontaQueryOficioPeriodo;
begin
  //DATA DE ENVIO/RECEBIMENTO
  qryGridPaiPadrao.SQL.Add(' WHERE DATA_OFICIO BETWEEN :DATA_INI AND :DATA_FIM');

  qryGridPaiPadrao.Params.ParamByName('DATA_INI').Value := dtePeriodoIni.Date;
  qryGridPaiPadrao.Params.ParamByName('DATA_FIM').Value := dtePeriodoFim.Date;

  if rgTipoOficio.ItemIndex > 0 then
  begin
    qryGridPaiPadrao.SQL.Add(' AND O.TIPO_OFICIO = :TIPO_OFICIO ');

    if rgTipoOficio.ItemIndex = 1 then
      qryGridPaiPadrao.Params.ParamByName('TIPO_OFICIO').Value := 'E'
    else
      qryGridPaiPadrao.Params.ParamByName('TIPO_OFICIO').Value := 'R';
  end;

  //PROTOCOLO
  if Trim(edtNumProtocolo.Text) <> '' then
  begin
    qryGridPaiPadrao.SQL.Add(' AND O.NUM_PROTOCOLO = :NUM_PROTOCOLO ');
    qryGridPaiPadrao.Params.ParamByName('NUM_PROTOCOLO').Value := Trim(edtNumProtocolo.Text);
  end;

  //PROCESSO
  if Trim(edtNumProcesso.Text) <> '' then
  begin
    qryGridPaiPadrao.SQL.Add(' AND O.NUM_PROCESSO = :NUM_PROCESSO ');
    qryGridPaiPadrao.Params.ParamByName('NUM_PROCESSO').Value := Trim(edtNumProcesso.Text);
  end;

  //NUMERO DO OFICIO
  if Trim(edtNumOficio.Text) <> '' then
  begin
    qryGridPaiPadrao.SQL.Add(' AND O.COD_OFICIO = :COD_OFICIO ');
    qryGridPaiPadrao.Params.ParamByName('COD_OFICIO').Value := Trim(edtNumOficio.Text);
  end;

  //ANO DO OFICIO
  if Trim(edtAnoOficio.Text) <> '' then
  begin
    qryGridPaiPadrao.SQL.Add(' AND O.ANO_OFICIO = :ANO_OFICIO ');
    qryGridPaiPadrao.Params.ParamByName('ANO_OFICIO').Value := Trim(edtAnoOficio.Text);
  end;

  //DESTINATARIO/REMETENTE
  if Trim(edtPessoaOficio.Text) <> '' then
    qryGridPaiPadrao.SQL.Add(' AND UPPER(OP.NOME_OFICIO_PESSOA) LIKE ' +
                             QuotedStr('%' + Trim(UpperCase(edtPessoaOficio.Text)) + '%'));

  //ORGAO
  if Trim(edtOrgaoOficio.Text) <> '' then
    qryGridPaiPadrao.SQL.Add(' AND UPPER(OO.NOME_OFICIO_ORGAO) LIKE ' +
                             QuotedStr('%' + Trim(UpperCase(edtOrgaoOficio.Text)) + '%'));

  //SUBCATEGORIA
  if Trim(lcbFormaEnvio.Text) <> '' then
  begin
    qryGridPaiPadrao.SQL.Add(' AND O.ID_OFICIO_FORMAENVIO_FK = :ID_OFICIO_FORMAENVIO ');
    qryGridPaiPadrao.Params.ParamByName('ID_OFICIO_FORMAENVIO').Value := lcbFormaEnvio.KeyValue;
  end;
end;

function TFFiltroOficio.PodeExcluir(var Msg: String): Boolean;
var
  QryVerif: TFDQuery;
begin
  inherited;

  Result := True;

  QryVerif := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryVerif, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT * FROM OFICIO WHERE ID_OFICIO > :ID_OFICIO';
    Params.ParamByName('ID_OFICIO').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
    Open;

    if QryVerif.RecordCount > 0 then
      msg := 'N�o � poss�vel excluir o Of�cio informado, pois j� existe um subsequente cadastrado.';
  end;

  Result := (QryVerif.RecordCount = 0) and (qryGridPaiPadrao.RecordCount > 0);

  FreeAndNil(QryVerif);
end;

procedure TFFiltroOficio.qryGridPaiPadraoAfterScroll(DataSet: TDataSet);
begin
  inherited;

  VerificarPermissoes;
end;

procedure TFFiltroOficio.rgTipoOficioClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroOficio.rgTipoOficioExit(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroOficio.VerificarPermissoes;
begin
  inherited;

  btnIncluir.Enabled  := False;

  btnEnviarResposta.Enabled := (qryGridPaiPadrao.RecordCount > 0) and
                               ((qryGridPaiPadrao.FieldByName('TIPO_OFICIO').AsString = 'R') and
                                vgPrm_IncOficEnv);

  btnReceberResposta.Enabled := (qryGridPaiPadrao.RecordCount > 0) and
                                ((qryGridPaiPadrao.FieldByName('TIPO_OFICIO').AsString = 'E') and
                                 vgPrm_IncOficRec);

  btnEditar.Enabled := (qryGridPaiPadrao.RecordCount > 0) and
                       ((qryGridPaiPadrao.FieldByName('TIPO_OFICIO').AsString = 'E') and
                         vgPrm_EdOficEnv) or
                       ((qryGridPaiPadrao.FieldByName('TIPO_OFICIO').AsString = 'R') and
                         vgPrm_EdOficRec);

  btnExcluir.Enabled := (qryGridPaiPadrao.RecordCount > 0) and
                        ((qryGridPaiPadrao.FieldByName('TIPO_OFICIO').AsString = 'E') and
                          vgPrm_ExcOficEnv) or
                        ((qryGridPaiPadrao.FieldByName('TIPO_OFICIO').AsString = 'R') and
                          vgPrm_ExcOficRec);

  btnImprimir.Enabled := (qryGridPaiPadrao.RecordCount > 0) and
                         ((qryGridPaiPadrao.FieldByName('TIPO_OFICIO').AsString = 'E') and
                           vgPrm_ImpOficEnv) or
                         ((qryGridPaiPadrao.FieldByName('TIPO_OFICIO').AsString = 'R') and
                           vgPrm_ImpOficRec);
end;

end.
