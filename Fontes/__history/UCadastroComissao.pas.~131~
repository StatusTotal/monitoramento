{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroFechamentoComissao.pas
  Descricao:   Tela de cadastro de Fechamento de Comissoes de Funcionarios
  Author   :   Cristina
  Date:        15-dez-2016
  Last Update: 01-set-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroComissao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Datasnap.Provider, Datasnap.DBClient, JvBaseEdits, JvDBControls, Vcl.Mask,
  JvExMask, JvToolEdit, Vcl.DBCtrls, System.StrUtils, System.DateUtils;

type
  TFCadastroComissao = class(TFCadastroGeralPadrao)
    cdsComissao: TClientDataSet;
    dspComissao: TDataSetProvider;
    qryComissao: TFDQuery;
    cdsComissaoID_COMISSAO: TIntegerField;
    cdsComissaoID_FUNCIONARIO_FK: TIntegerField;
    cdsComissaoID_SISTEMA_FK: TIntegerField;
    cdsComissaoCOD_ADICIONAL: TIntegerField;
    cdsComissaoID_ORIGEM: TIntegerField;
    cdsComissaoTIPO_ORIGEM: TStringField;
    cdsComissaoPERCENT_COMISSAO: TBCDField;
    cdsComissaoVR_COMISSAO: TBCDField;
    cdsComissaoID_FECHAMENTO_SALARIO_FK: TIntegerField;
    cdsComissaoDATA_CADASTRO: TSQLTimeStampField;
    cdsComissaoCAD_ID_USUARIO: TIntegerField;
    cdsComissaoDATA_PAGAMENTO: TDateField;
    cdsComissaoPAG_ID_USUARIO: TIntegerField;
    cdsComissaoFLG_CANCELADO: TStringField;
    cdsComissaoDATA_CANCELAMENTO: TDateField;
    cdsComissaoCANCEL_ID_USUARIO: TIntegerField;
    cdsComissaoDATA_COMISSAO: TDateField;
    cdsComissaoID_ITEM_FK: TIntegerField;
    lblNomeColaborador: TLabel;
    lblDataComissao: TLabel;
    lblItem: TLabel;
    lblPercentagem: TLabel;
    lblValor: TLabel;
    lblDataPagamento: TLabel;
    lcbNomeColaborador: TDBLookupComboBox;
    lcbItem: TDBLookupComboBox;
    dteDataComissao: TJvDBDateEdit;
    dteDataPagamento: TJvDBDateEdit;
    cedPercentagem: TJvDBCalcEdit;
    cedValor: TJvDBCalcEdit;
    dsItem: TDataSource;
    dsColab: TDataSource;
    qryItem: TFDQuery;
    qryColab: TFDQuery;
    lblValorItem: TLabel;
    cedValorItem: TJvDBCalcEdit;
    cdsComissaoVR_COMISSAO_ANT: TCurrencyField;
    cdsComissaoVR_ITEM: TCurrencyField;
    procedure btnOkClick(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure cedPercentagemExit(Sender: TObject);
    procedure cedValorExit(Sender: TObject);
    procedure cedValorKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lcbItemExit(Sender: TObject);
    procedure cdsComissaoCalcFields(DataSet: TDataSet);
    procedure dteDataComissaoExit(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadastroComissao: TFCadastroComissao;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

procedure TFCadastroComissao.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroComissao.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroComissao.cdsComissaoCalcFields(DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with qryAux, SQL do
  begin
    //Tipo Despesa
    if cdsComissao.FieldByName('ID_ORIGEM').AsInteger > 0 then
    begin
      Close;
      Clear;

      Text := 'SELECT LD.VR_TOTAL ' +
              '  FROM LANCAMENTO_DET LD ' +
              ' INNER JOIN LANCAMENTO L ' +
              '    ON LD.COD_LANCAMENTO_FK = L.COD_LANCAMENTO ' +
              '   AND LD.ANO_LANCAMENTO_FK = L.ANO_LANCAMENTO ' +
              '   AND LD.ID_ITEM_FK = :ID_ITEM ' +
              '   AND L.ID_ORIGEM = :ID_ORIGEM ' +
              '   AND L.ID_NATUREZA_FK = :ID_NATUREZA';
      Params.ParamByName('ID_ITEM').AsInteger     := cdsComissao.FieldByName('ID_ITEM_FK').AsInteger;
      Params.ParamByName('ID_ORIGEM').AsInteger   := cdsComissao.FieldByName('ID_ORIGEM').AsInteger;

      case AnsiIndexStr(UpperCase(cdsComissao.FieldByName('TIPO_ORIGEM').AsString), ['R' , 'F', 'A']) of
        0: Params.ParamByName('ID_NATUREZA').AsInteger := 6;
        1: Params.ParamByName('ID_NATUREZA').AsInteger := 4;
        2: Params.ParamByName('ID_NATUREZA').AsInteger := 5;
      end;

      Open;

      cdsComissao.FieldByName('VR_ITEM').AsCurrency := qryAux.FieldByName('VR_TOTAL').AsCurrency;
    end;
  end;

  FreeAndNil(qryAux);
end;

procedure TFCadastroComissao.cedPercentagemExit(Sender: TObject);
begin
  inherited;

  if cdsComissao.State in [dsInsert, dsEdit] then
    cdsComissao.FieldByName('VR_COMISSAO').AsCurrency := ((cedValorItem.Value * cedPercentagem.Value)/100);
end;

procedure TFCadastroComissao.cedValorExit(Sender: TObject);
begin
  inherited;

  if cdsComissao.State in [dsInsert, dsEdit] then
    cdsComissao.FieldByName('PERCENT_COMISSAO').AsCurrency := ((cedValor.Value * 100) /
                                                               cedValorItem.Value);
end;

procedure TFCadastroComissao.cedValorKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    btnCancelar.Click;
end;

procedure TFCadastroComissao.DefinirTamanhoMaxCampos;
begin
  inherited;
  //
end;

procedure TFCadastroComissao.DesabilitarComponentes;
begin
  inherited;

  dteDataPagamento.Enabled := False;

  if vgOperacao = E then
  begin
    lcbNomeColaborador.Enabled := False;
    lcbItem.Enabled            := False;

    lblValorItem.Visible := (not cdsComissao.FieldByName('ID_ORIGEM').IsNull);
    cedValorItem.Visible := (not cdsComissao.FieldByName('ID_ORIGEM').IsNull);
    cedValorItem.Enabled := cdsComissao.FieldByName('ID_ORIGEM').IsNull;
  end;

  if vgOperacao = C then
  begin
    dteDataComissao.Enabled    := False;
    lcbNomeColaborador.Enabled := False;
    lcbItem.Enabled            := False;
    cedValorItem.Enabled       := fALSE;
    cedPercentagem.Enabled     := False;
    cedValor.Enabled           := False;
  end;
end;

procedure TFCadastroComissao.dteDataComissaoExit(Sender: TObject);
begin
  inherited;

  if cdsComissao.State in [dsInsert, dsEdit] then
  begin
    dmPrincipal.qryFechCx.Close;
    dmPrincipal.qryFechCx.Params.ParamByName('DATA').Value := dteDataComissao.Date;
    dmPrincipal.qryFechCx.Open;

    if dmPrincipal.qryFechCx.RecordCount > 0 then
      Application.MessageBox('O Caixa do Dia j� se encontra Fechado, portanto, os novos Lan�amentos de Comiss�o entrar�o com a data do dia seguinte. ',
                             'Aviso',
                             MB_OK + MB_ICONINFORMATION);

    if dmPrincipal.qryFechCx.RecordCount = 0 then
      cdsComissao.FieldByName('DATA_COMISSAO').AsDateTime := dteDataComissao.Date
    else
      cdsComissao.FieldByName('DATA_COMISSAO').AsDateTime := IncDay(dteDataComissao.Date, 1);
  end;
end;

procedure TFCadastroComissao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  cdsComissao.Cancel;

  vgOperacao     := VAZIO;
  vgIdConsulta   := 0;
  vgOrigemFiltro := False;

  inherited;
end;

procedure TFCadastroComissao.FormCreate(Sender: TObject);
begin
  inherited;

  qryColab.Close;
  qryColab.Open;

  qryItem.Close;
  qryItem.Open;
  qryItem.Edit;

  cdsComissao.Close;
  cdsComissao.Params.ParamByName('ID_COMISSAO').Value := vgIdConsulta;
  cdsComissao.Open;

  if vgOperacao = I then
  begin
    dmPrincipal.qryFechCx.Close;
    dmPrincipal.qryFechCx.Params.ParamByName('DATA').Value := Date;
    dmPrincipal.qryFechCx.Open;

    if dmPrincipal.qryFechCx.RecordCount > 0 then
      Application.MessageBox('O Caixa do Dia j� se encontra Fechado, portanto, os novos Lan�amentos de Comiss�o entrar�o com a data do dia seguinte. ',
                             'Aviso',
                             MB_OK + MB_ICONINFORMATION);

    cdsComissao.Append;

    if dmPrincipal.qryFechCx.RecordCount = 0 then
      cdsComissao.FieldByName('DATA_COMISSAO').AsDateTime := Date
    else
      cdsComissao.FieldByName('DATA_COMISSAO').AsDateTime := IncDay(Date, 1);

    cdsComissao.FieldByName('DATA_CADASTRO').AsDateTime := Now;
    cdsComissao.FieldByName('CAD_ID_USUARIO').Value     := vgUsu_Id;
    cdsComissao.FieldByName('DATA_QUITACAO').AsDateTime := Date;
    cdsComissao.FieldByName('FLG_CANCELADO').AsString   := 'N';
  end;

  if vgOperacao = E then
  begin
    cdsComissao.Edit;
    cdsComissao.FieldByName('VR_COMISSAO_ANT').AsCurrency := cdsComissao.FieldByName('VR_COMISSAO').AsCurrency;
  end;
end;

procedure TFCadastroComissao.FormShow(Sender: TObject);
begin
  inherited;

  if lcbNomeColaborador.CanFocus then
    lcbNomeColaborador.SetFocus;
end;

function TFCadastroComissao.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroComissao.GravarDadosGenerico(var Msg: String): Boolean;
var
  IdCli, IdCat, IdSubCat, Cod: Integer;
  Doc, Obs, NFantasia, RSocial, Descr: String;
  IdCom, CodLanc, AnoLanc, IdParc: Integer;
  sMsg, sIR, sCP, sLA: String;
  QryAuxS: TFDQuery;
  cValorDisp: Currency;
  Op: TOperacao;
  lContinuar: Boolean;
begin
  Result := True;
  Msg    := '';
  sIR    := '';
  sCP    := '';
  sLA    := '';
  Op     := vgOperacao;

  cValorDisp := 0;

  if vgOperacao = C then
    Exit;

  IdCom    := 0;
  CodLanc  := 0;
  AnoLanc  := 0;
  IdParc   := 0;
  IdCli    := 0;
  IdCom    := 0;
  IdCat    := 0;
  IdSubCat := 0;
  Cod      := 0;

  Doc       := '';
  Obs       := '';
  NFantasia := '';
  RSocial   := '';
  Descr     := '';

  lContinuar:= True;

  QryAuxS := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    { COMISSAO }
    if vgOperacao = I then
      IdCom := BS.ProximoId('ID_COMISSAO', 'COMISSAO')
    else
      IdCom := cdsComissao.FieldByName('ID_COMISSAO').AsInteger;

    cdsComissao.FieldByName('ID_COMISSAO').Value := IdCom;
    cdsComissao.Post;
    cdsComissao.ApplyUpdates(0);

    cdsComissao.Close;
    cdsComissao.Params.ParamByName('ID_COMISSAO').Value := IdCom;
    cdsComissao.Open;

    if cdsComissao.FieldByName('VR_COMISSAO_ANT').AsCurrency <> cdsComissao.FieldByName('VR_COMISSAO').AsCurrency then
      dmPrincipal.RecalcularFechamentoSalarios(cdsComissao.FieldByName('ID_FECHAMENTO_SALARIO_FK').AsInteger,
                                               cdsComissao.FieldByName('ID_FUNCIONARIO_FK').AsInteger,
                                               True, False, False);

    { ITEM }
    QryAuxS.Close;
    QryAuxS.SQL.Clear;
    QryAuxS.SQL.Text := 'UPDATE ITEM ' +
                        '   SET ULT_VALOR_CUSTO = :ULT_VALOR_CUSTO ' +
                        ' WHERE ID_ITEM = :ID_ITEM';
    QryAuxS.Params.ParamByName('ULT_VALOR_CUSTO').Value := cdsComissao.FieldByName('VR_ITEM').AsInteger;
    QryAuxS.Params.ParamByName('ID_ITEM').Value         := cdsComissao.FieldByName('ID_ITEM_FK').AsInteger;
    QryAuxS.ExecSQL;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    lContinuar := False;
  end;

  if lContinuar then
  begin
    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      { LANCAMENTO DE DESPESA }
      //Fornecedor
      Doc       := qryColab.FieldByName('CPF').AsString;
      Obs       := 'Cadastro realizado automaticamente em inclus�o de comiss�o.';
      NFantasia := qryColab.FieldByName('NOME_FUNCIONARIO').AsString;
      RSocial   := qryColab.FieldByName('NOME_FUNCIONARIO').AsString;

      dmPrincipal.RetornarClienteFornecedor('F',
                                            IdCli,
                                            Doc,
                                            Obs,
                                            NFantasia,
                                            RSocial);

      //Categoria
      IdCat := 0;
      Cod   := 1;  //Despesa
      Descr := 'FUNCION�RIOS';
      Obs   := 'Inclus�o AUTOM�TICA de Categoria de Despesa';

      dmPrincipal.RetornaCategoria(IdCat, Cod, Descr, Obs);

      //SubCategoria
      IdSubCat := 0;
      Cod      := 1;  //Despesa
      Descr    := NFantasia;
      Obs      := 'Inclus�o AUTOM�TICA de Subcategoria de Despesa';

      dmPrincipal.RetornaSubCategoria(IdSubCat, IdCat, Cod, Descr, Obs);

      dmPrincipal.InicializarComponenteLancamento;

      //Lancamento
      DadosLancamento := Lancamento.Create;

      if vgOperacao = I then
      begin
        dmPrincipal.RetornarClassificacaoNatureza(2, sIR, sCP, sLA);

        DadosLancamento.DataLancamento   := dteDataComissao.Date;
        DadosLancamento.TipoLancamento   := 'D';
        DadosLancamento.TipoCadastro     := 'A';
        DadosLancamento.IdCategoria      := IdCat;
        DadosLancamento.IdSubCategoria   := IdSubCat;
        DadosLancamento.IdCliFor         := IdCli;
        DadosLancamento.FlgIR            := sIR;
        DadosLancamento.FlgPessoal       := sCP;
        DadosLancamento.FlgFlutuante     := 'N';
        DadosLancamento.FlgAuxiliar      := sLA;
        Dadoslancamento.FlgForaFechCaixa := 'N';
        DadosLancamento.IdNatureza       := 2;  //COMISSAO
        DadosLancamento.QtdParcelas      := 1;
        DadosLancamento.CadIdUsuario     := vgUsu_Id;
        DadosLancamento.DataCadastro     := Now;
        DadosLancamento.FlgRecorrente    := 'N';
        DadosLancamento.FlgReplicado     := 'N';
        DadosLancamento.IdOrigem         := IdCom;
      end
      else
      if vgOperacao = E then
      begin
        QryAuxS.Close;
        QryAuxS.SQL.Clear;
        QryAuxS.SQL.Text := 'SELECT COD_LANCAMENTO, ANO_LANCAMENTO ' +
                            '  FROM LANCAMENTO ' +
                            ' WHERE ID_ORIGEM      = :ID_ORIGEM ' +
                            '   AND ID_NATUREZA_FK = :ID_NATUREZA';
        QryAuxS.Params.ParamByName('ID_ORIGEM').Value   := IdCom;
        QryAuxS.Params.ParamByName('ID_NATUREZA').Value := 2;
        QryAuxS.Open;

        DadosLancamento.CodLancamento := QryAuxS.FieldByName('COD_LANCAMENTO').AsInteger;
        DadosLancamento.AnoLancamento := QryAuxS.FieldByName('ANO_LANCAMENTO').AsInteger;
      end;

      if not cdsComissao.FieldByName('DATA_PAGAMENTO').IsNull then
      begin
        DadosLancamento.VlrTotalPago := cedValor.Value;
        DadosLancamento.FlgStatus    := 'G';
      end;

      DadosLancamento.VlrTotalPrev := cedValor.Value;
      DadosLancamento.Observacao   := 'Receita originada pela COMISS�O ' + IntToStr(IdCom) + '.';

      ListaLancamentos.Add(DadosLancamento);

      if vgOperacao = I then
        dmPrincipal.InsertLancamento(0)
      else if vgOperacao = E then
        dmPrincipal.UpdateLancamento(0);

      { PARCELAS }
      DadosLancamentoParc := LancamentoParc.Create;

      if vgOperacao = I then
      begin
        DadosLancamentoParc.NumParcela     := 1;
        DadosLancamentoParc.IdFormaPagto   := 6;  //Dinheiro
        DadosLancamentoParc.DataLancParc   := dteDataComissao.Date;
        DadosLancamentoParc.DataVencimento := Date;
        DadosLancamentoParc.CadIdUsuario   := vgUsu_Id;
        DadosLancamentoParc.DataCadastro   := Now;
      end;

      DadosLancamentoParc.VlrPrevisto := cedValor.Value;

      if not cdsComissao.FieldByName('DATA_PAGAMENTO').IsNull then
      begin
        DadosLancamentoParc.VlrPago        := cedValor.Value;
        DadosLancamentoParc.DataPagamento  := cdsComissao.FieldByName('DATA_PAGAMENTO').AsDateTime;
        DadosLancamentoParc.FlgStatus      := 'G';
        DadosLancamentoParc.PagIdUsuario   := vgUsu_Id;
      end;

      ListaLancamentoParcs.Add(DadosLancamentoParc);

      if vgOperacao = I then
        dmPrincipal.InsertLancamentoParc(0, 0)
      else if vgOperacao = E then
        dmPrincipal.UpdateLancamentoParc(0, 0);

      { LOG }
      if vgOperacao = I then
      begin
        with QryAuxS do
        begin
          QryAuxS.Close;
          QryAuxS.SQL.Clear;
          QryAuxS.SQL.Text := 'INSERT INTO USUARIO_LOG (ID_USUARIO_LOG, ID_USUARIO, ID_MODULO_FK, ' +
                              '                         TIPO_LANCAMENTO, CODIGO_LAN, ANO_LAN, ID_ORIGEM, ' +
                              '                         TABELA_ORIGEM, TIPO_ACAO,  OBS_USUARIO_LOG) ' +
                              '                 VALUES (:ID_USUARIO_LOG, :ID_USUARIO, :ID_MODULO, ' +
                              '                         :TIPO_LANCAMENTO, :CODIGO_LAN, :ANO_LAN, :ID_ORIGEM, ' +
                              '                         :TABELA_ORIGEM, :TIPO_ACAO,  :OBS_USUARIO_LOG)';

          QryAuxS.Params.ParamByName('ID_USUARIO_LOG').Value  := BS.ProximoId('ID_USUARIO_LOG', 'USUARIO_LOG');
          QryAuxS.Params.ParamByName('ID_USUARIO').Value      := vgUsu_Id;
          QryAuxS.Params.ParamByName('ID_MODULO').Value       := 80;
          QryAuxS.Params.ParamByName('TIPO_LANCAMENTO').Value := 'D';
          QryAuxS.Params.ParamByName('CODIGO_LAN').Value      := ListaLancamentos[0].CodLancamento;
          QryAuxS.Params.ParamByName('ANO_LAN').Value         := ListaLancamentos[0].AnoLancamento;
          QryAuxS.Params.ParamByName('ID_ORIGEM').Value       := Null;
          QryAuxS.Params.ParamByName('TABELA_ORIGEM').Value   := Null;
          QryAuxS.Params.ParamByName('TIPO_ACAO').Value       := 'I';
          QryAuxS.Params.ParamByName('OBS_USUARIO_LOG').Value := 'Inclus�o AUTOM�TICA de Despesa de COMISS�O.';

          QryAuxS.ExecSQL;
        end;
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      dmPrincipal.FinalizarComponenteLancamento;

      Msg := 'Dados da Comiss�o gravados com sucesso!';
    except
      on E: Exception do
      begin
        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Rollback;

        Result := False;
        Msg := 'Erro na grava��o do Dados da Comiss�o.';
      end;
    end;
  end;

  FreeAndNil(QryAuxS);
end;

procedure TFCadastroComissao.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta de Dados de Comiss�o',
                          vgIdConsulta, 'COMISSAO');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclus�o de Dados de Comiss�o',
                          vgIdConsulta, 'COMISSAO');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da edi��o desse Comiss�o:', '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de Comiss�o';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          vgIdConsulta, 'COMISSAO');
    end;
  end;
end;

procedure TFCadastroComissao.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroComissao.lcbItemExit(Sender: TObject);
begin
  inherited;

  if cdsComissao.State in [dsInsert, dsEdit] then
  begin
    if not qryItem.FieldByName('ULT_VALOR_CUSTO').IsNull then
      cdsComissao.FieldByName('VR_ITEM').AsCurrency := qryItem.FieldByName('ULT_VALOR_CUSTO').AsCurrency
    else
      cdsComissao.FieldByName('VR_ITEM').Value := Null;
  end;
end;

procedure TFCadastroComissao.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;

end;

function TFCadastroComissao.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if dteDataComissao.Date = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a DATA DA COMISS�O.';
    DadosMsgErro.Componente := dteDataComissao;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end
  else
  begin
    if dteDataComissao.Date > Date then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') A DATA DA COMISS�O n�o pode ser superior � Data de hoje.';
      DadosMsgErro.Componente := dteDataComissao;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;
  end;

  if Trim(lcbNomeColaborador.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o NOME DO COLABORADOR.';
    DadosMsgErro.Componente := lcbNomeColaborador;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(lcbItem.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o ITEM.';
    DadosMsgErro.Componente := lcbItem;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if cedValorItem.Value = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o VALOR DO ITEM.';
    DadosMsgErro.Componente := cedValorItem;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if cedPercentagem.Value = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a PORCENTAGEM DA COMISS�O.';
    DadosMsgErro.Componente := cedPercentagem;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if cedValor.Value = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o VALOR DA COMISS�O.';
    DadosMsgErro.Componente := cedValor;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroComissao.VerificarLayoutAto(var QtdErros: Integer): Boolean;
begin
  //
end;

end.
