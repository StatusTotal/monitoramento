{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroRelatorioRecibosCancelados.pas
  Descricao:   Filtro Relatorios de Recibos cancelados (R13 - R13DET)
  Author   :   Cristina
  Date:        01-jun-2017
  Last Update: 11-set-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroRelatorioRecibosCancelados;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroRelatorioPadrao, Vcl.StdCtrls,
  JvExControls, JvButton, JvTransparentButton, Vcl.ExtCtrls, Vcl.Mask, JvExMask,
  JvToolEdit, System.DateUtils, FireDAC.Stan.Param;

type
  TFFiltroRelatorioRecibosCancelados = class(TFFiltroRelatorioPadrao)
    lblPeriodo: TLabel;
    lblPeriodoInicio: TLabel;
    dteDataInicio: TJvDateEdit;
    lblPeriodoFim: TLabel;
    dteDataFim: TJvDateEdit;
    procedure btnLimparClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    procedure GravarLog; override;
    procedure GerarRelatorio(ImpDet: Boolean); override;
    function VerificarFiltro: Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroRelatorioRecibosCancelados: TFFiltroRelatorioRecibosCancelados;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMRelatorios;

{ TFFiltroRelatorioRecibosCancelados }

procedure TFFiltroRelatorioRecibosCancelados.btnLimparClick(Sender: TObject);
begin
  inherited;

  dteDataInicio.Date := StartOfTheYear(Date);
  dteDataFim.Date    := Date;

  chbTipoSintetico.Checked := True;
  chbTipoAnalitico.Checked := False;

  chbExportarPDF.Checked := False;

  chbExportarExcel.Enabled := vgPrm_ExpRelRcb;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioRecibosCancelados.btnVisualizarClick(
  Sender: TObject);
begin
  inherited;

  if not VerificarFiltro then
    Exit;

  //Sintetico
  dmRelatorios.qryRel13_RecCancel.Close;

  dmRelatorios.qryRel13_RecCancel.Params.ParamByName('DATA_INI').Value := (FormatDateTime('YYYY-MM-DD', dteDataInicio.Date) + ' 00:00:00');
  dmRelatorios.qryRel13_RecCancel.Params.ParamByName('DATA_FIM').Value := (FormatDateTime('YYYY-MM-DD', dteDataFim.Date) + ' 23:59:59');

  dmRelatorios.qryRel13_RecCancel.Open;

  if dmRelatorios.qryRel13_RecCancel.RecordCount = 0 then
  begin
    Application.MessageBox('N�o h� Recibos Cancelados para o per�odo informado.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
  end
  else
  begin
    if chbTipoSintetico.Checked then
      GerarRelatorio(False);

    if chbTipoAnalitico.Checked then
      GerarRelatorio(True);
  end;
end;

procedure TFFiltroRelatorioRecibosCancelados.FormShow(Sender: TObject);
begin
  inherited;

  dteDataInicio.Date := StartOfTheYear(Date);
  dteDataFim.Date    := Date;

  chbTipoSintetico.Checked := True;
  chbTipoAnalitico.Checked := False;

  chbExportarPDF.Checked := False;

  chbExportarExcel.Enabled := vgPrm_ExpRelRcb;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;

  chbExibirGrafico.Visible := False;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioRecibosCancelados.GerarRelatorio(ImpDet: Boolean);
begin
  inherited;

  try
    sTituloRelatorio    := 'RECIBOS CANCELADOS';
    sSubtituloRelatorio := 'Per�odo de ' + DateToStr(dteDataInicio.Date) + ' a ' + DateToStr(dteDataFim.Date);

    if ImpDet then
    begin
      dmRelatorios.DefinirRelatorio(R13DET);
      lExibeDetalhe := True;
    end
    else
    begin
      dmRelatorios.DefinirRelatorio(R13);
      lExibeDetalhe := False;
    end;

    dmRelatorios.ImprimirRelatorio(chbExportarPDF.Checked, chbExportarExcel.Checked);

    GravarLog;
  except
    Application.MessageBox('Erro ao gerar Relat�rio de Recibos Cancelados.',
                           'Erro',
                           MB_OK + MB_ICONERROR);
  end;
end;

procedure TFFiltroRelatorioRecibosCancelados.GravarLog;
begin
  inherited;

  BS.GravarUsuarioLog(50,
                      '',
                      'Impress�o de Relatorio de Recibos Cancelados (' + DateToStr(dDataRelatorio) +
                                                                   ' ' +
                                                                   TimeToStr(tHoraRelatorio) + ')',
                      0,
                      '');
end;

function TFFiltroRelatorioRecibosCancelados.VerificarFiltro: Boolean;
var
  Msg: String;
begin
  Result := True;

  if dteDataInicio.Date = 0 then
  begin
    Msg := '- Informe a DATA DE IN�CIO do Per�odo.';
    Result := False;
  end;

  if dteDataFim.Date = 0 then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe a DATA DE FIM do Per�odo.'
    else
      Msg := #13#10 + '- Informe a DATA DE FIM do Per�odo.';

    Result := False;
  end;

  if dteDataFim.Date < dteDataInicio.Date then
  begin
    if Trim(Msg) = '' then
      Msg := '- A DATA DE FIM do Per�odo n�o pode inferior � DATA DE IN�CIO do mesmo.'
    else
      Msg := #13#10 + '- A DATA DE FIM do Per�odo n�o pode ser inferior � DATA DE IN�CIO do mesmo.';

    Result := False;
  end;

  if (not chbTipoAnalitico.Checked) and
    (not chbTipoSintetico.Checked) then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe o TIPO DE LAN�AMENTO do Relat�rio de Recibos Cancelados.'
    else
      Msg := #13#10 + '- Informe o TIPO DE LAN�AMENTO do Relat�rio de Recibos Cancelados.';

    Result := False;
  end;

  if not Result then
    Application.MessageBox(PChar(':: ATEN��O ::' + #13#10 + #13#10 + Msg),
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
end;

end.
