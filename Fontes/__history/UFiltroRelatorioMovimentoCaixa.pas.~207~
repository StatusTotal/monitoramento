{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroRelatorioMovimentoCaixa.pas
  Descricao:   Filtro Relatorios de Movimento de Caixa (R09 - R09DET)
  Author   :   Cristina
  Date:        26-jan-2017
  Last Update: 10-jan-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroRelatorioMovimentoCaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroRelatorioPadrao, Vcl.StdCtrls,
  JvExControls, JvButton, JvTransparentButton, Vcl.ExtCtrls, Vcl.Mask, JvExMask,
  JvToolEdit, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, System.StrUtils, JvBaseEdits, JvDBControls,
  System.DateUtils;

type
  TFFiltroRelatorioMovimentoCaixa = class(TFFiltroRelatorioPadrao)
    lblData: TLabel;
    dteData: TJvDateEdit;
    lblVlrRealTContFinal: TLabel;
    cedVlrRealTContFinal: TJvCalcEdit;
    lblContaCorrente: TLabel;
    procedure btnLimparClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dteDataExit(Sender: TObject);
    procedure dteDataKeyPress(Sender: TObject; var Key: Char);
  protected
    procedure GravarLog; override;
    procedure GerarRelatorio(ImpDet: Boolean); override;
    function VerificarFiltro: Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }

    DataMovimento: TDatetime;

    procedure RecuperarValorContaCorrente;
  end;

var
  FFiltroRelatorioMovimentoCaixa: TFFiltroRelatorioMovimentoCaixa;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMRelatorios;

{ TFFiltroRelatorioMovimentoCaixa }

procedure TFFiltroRelatorioMovimentoCaixa.btnLimparClick(Sender: TObject);
begin
  inherited;

  if DataMovimento = 0 then
    dteData.Date := Date
  else
    dteData.Date := DataMovimento;

  RecuperarValorContaCorrente;

  chbExportarPDF.Checked := False;

  chbExportarExcel.Enabled := vgPrm_ExpMovCx or vgPrm_ExpRelMovCx;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;

  if dteData.CanFocus then
    dteData.SetFocus;
end;

procedure TFFiltroRelatorioMovimentoCaixa.btnVisualizarClick(Sender: TObject);
var
  QryLanc, QryRecPAnt: TFDQuery;
begin
  inherited;

  if not VerificarFiltro then
    Exit;

  dDataPerInicio := dteData.Date;
  dDataPerFim    := dteData.Date;

  cTotalPendentesAnterior := 0;
  cTotalDepositosSemBaixa := 0;

  QryLanc    := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
  QryRecPAnt := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  QryLanc.Close;
  QryLanc.SQL.Clear;
  QryLanc.SQL.Text := 'SELECT FIRST 1 LANCS.ID ' +
                      '  FROM (SELECT FIRST 1 ID_LANCAMENTO_PARC AS ID ' +
                      '          FROM LANCAMENTO_PARC ' +
                      '         WHERE DATA_LANCAMENTO_PARC BETWEEN :DATA_INI1 AND :DATA_FIM1 ' +
                      '            OR DATA_PAGAMENTO BETWEEN :DATA_INI2 AND :DATA_FIM2 ' +
                      '         UNION ' +
                      '        SELECT FIRST 1 ID_DEPOSITO_FLUTUANTE AS ID ' +
                      '          FROM DEPOSITO_FLUTUANTE ' +
                      '         WHERE FLG_STATUS = ' + QuotedStr('F') +
                      '           AND DATA_FLUTUANTE <= :DATA_FIM3) AS LANCS';
  QryLanc.Params.ParamByName('DATA_INI1').Value := dteData.Date;
  QryLanc.Params.ParamByName('DATA_FIM1').Value := dteData.Date;
  QryLanc.Params.ParamByName('DATA_INI2').Value := dteData.Date;
  QryLanc.Params.ParamByName('DATA_FIM2').Value := dteData.Date;
  QryLanc.Params.ParamByName('DATA_FIM3').Value := dteData.Date;
  QryLanc.Open;

  if QryLanc.RecordCount = 0 then
  begin
    Application.MessageBox('N�o h� Lan�amentos registrados.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
  end
  else
  begin
    dmGerencial.Processando(True, 'Gerando Relat�rio de Movimento de Caixa', True, 'Por favor, aguarde...');

    RecuperarValorContaCorrente;

    //PENDENTES DO FECHAMENTO ANTERIOR
    QryRecPAnt.Close;
    QryRecPAnt.SQL.Clear;

    QryRecPAnt.SQL.Text := 'SELECT SUM(LP.VR_PARCELA_PREV) AS TOTAL_PENDENTE ' +
                           '  FROM LANCAMENTO_PARC LP ' +
                           ' INNER JOIN LANCAMENTO L ' +
                           '    ON LP.COD_LANCAMENTO_FK = L.COD_LANCAMENTO ' +
                           '   AND LP.ANO_LANCAMENTO_FK = L.ANO_LANCAMENTO ' +
                           '  LEFT JOIN NATUREZA N ' +
                           '    ON L.ID_NATUREZA_FK = N.ID_NATUREZA ' +
                           ' WHERE L.FLG_CANCELADO = ' + QuotedStr('N') +
                           '   AND L.FLG_FORAFECHCAIXA = ' + QuotedStr('N') +
                           '   AND (LP.DATA_PAGAMENTO IS NULL ' +
                           '    OR LP.DATA_PAGAMENTO > :DATA_PEND1) ' +
                           '   AND EXISTS (SELECT NAT.ID_NATUREZA ' +
                           '                 FROM NATUREZA NAT ' +
                           '                WHERE NAT.ID_NATUREZA = L.ID_NATUREZA_FK ' +
                           '                  AND NAT.DESCR_NATUREZA <> ' + QuotedStr('SOBRA DE CAIXA') +
                           '                  AND NAT.DESCR_NATUREZA <> ' + QuotedStr('FALTA DE CAIXA') + ')' +
                           '   AND LP.FLG_STATUS <> ' + QuotedStr('C') +
                           '   AND L.TIPO_LANCAMENTO = ' + QuotedStr('R') +
                           '   AND LP.ID_FORMAPAGAMENTO_FK IN (2, 3, 4, 7) ' +
                           '   AND LP.DATA_LANCAMENTO_PARC < :DATA_PEND2';

    QryRecPAnt.Params.ParamByName('DATA_PEND1').Value := dteData.Date;
    QryRecPAnt.Params.ParamByName('DATA_PEND2').Value := dteData.Date;
    QryRecPAnt.Open;

    if not QryRecPAnt.FieldByName('TOTAL_PENDENTE').IsNull then
      cTotalPendentesAnterior := QryRecPAnt.FieldByName('TOTAL_PENDENTE').AsCurrency;

    { RECEITAS }
    //Importacoes
    dmRelatorios.qryRel09_Import.Close;
    dmRelatorios.qryRel09_Import.Params.ParamByName('DATA_INI').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 00:00:00');
    dmRelatorios.qryRel09_Import.Params.ParamByName('DATA_FIM').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 23:59:59');
    dmRelatorios.qryRel09_Import.Open;

    //Recebidos e Pendentes
    dmRelatorios.qryRel09_ImportTotal.Close;
    dmRelatorios.qryRel09_ImportTotal.Params.ParamByName('DATA_INIP1').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 00:00:00');
    dmRelatorios.qryRel09_ImportTotal.Params.ParamByName('DATA_FIMP1').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 23:59:59');
    dmRelatorios.qryRel09_ImportTotal.Params.ParamByName('DATA_INIP2').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 00:00:00');
    dmRelatorios.qryRel09_ImportTotal.Params.ParamByName('DATA_FIMP2').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 23:59:59');
    dmRelatorios.qryRel09_ImportTotal.Open;

    //Importacoes - Detalhamento
{    dmRelatorios.qryRel09_ImpDet.Close;
    dmRelatorios.qryRel09_ImpDet.Params.ParamByName('DATA_INI').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 00:00:00');
    dmRelatorios.qryRel09_ImpDet.Params.ParamByName('DATA_FIM').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 23:59:59');
    dmRelatorios.qryRel09_ImpDet.Open;  }

    //Outras Receitas
    dmRelatorios.qryRel09_ORec.Close;
    dmRelatorios.qryRel09_ORec.Params.ParamByName('DATA_INI').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 00:00:00');
    dmRelatorios.qryRel09_ORec.Params.ParamByName('DATA_FIM').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 23:59:59');
    dmRelatorios.qryRel09_ORec.Open;

    //Receitas para Repasses
    dmRelatorios.qryRel09_RecRep.Close;
    dmRelatorios.qryRel09_RecRep.Params.ParamByName('DATA_INIP').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 00:00:00');
    dmRelatorios.qryRel09_RecRep.Params.ParamByName('DATA_FIMP').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 23:59:59');
    dmRelatorios.qryRel09_RecRep.Open;

    //Receitas Oriundas de Servicos Anteriores
    dmRelatorios.qryRel09_ServAnt.Close;
    dmRelatorios.qryRel09_ServAnt.Params.ParamByName('DATA_INI1').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 00:00:00');
    dmRelatorios.qryRel09_ServAnt.Params.ParamByName('DATA_FIM').Value  := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 23:59:59');
    dmRelatorios.qryRel09_ServAnt.Params.ParamByName('DATA_INI2').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 00:00:00');
    dmRelatorios.qryRel09_ServAnt.Open;

    //Receitas Oriundas de Servicos Anteriores - Detalhamento
{    dmRelatorios.qryRel09_SAntDet.Close;
    dmRelatorios.qryRel09_SAntDet.Params.ParamByName('DATA_INI1').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 00:00:00');
    dmRelatorios.qryRel09_SAntDet.Params.ParamByName('DATA_FIM').Value  := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 23:59:59');
    dmRelatorios.qryRel09_SAntDet.Params.ParamByName('DATA_INI2').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 00:00:00');
    dmRelatorios.qryRel09_SAntDet.Open;  }

    { DESPESAS }
    //Despesas - Gerais
    dmRelatorios.qryRel09_DespG.Close;
    dmRelatorios.qryRel09_DespG.Params.ParamByName('DATA_INI').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 00:00:00');
    dmRelatorios.qryRel09_DespG.Params.ParamByName('DATA_FIM').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 23:59:59');
    dmRelatorios.qryRel09_DespG.Open;

    //Despesas - Descontos
    dmRelatorios.qryRel09_DespD.Close;
    dmRelatorios.qryRel09_DespD.Params.ParamByName('DATA_INI').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 00:00:00');
    dmRelatorios.qryRel09_DespD.Params.ParamByName('DATA_FIM').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 23:59:59');
    dmRelatorios.qryRel09_DespD.Open;

    //Despesas - Repasses
    dmRelatorios.qryRel09_DespR.Close;
    dmRelatorios.qryRel09_DespR.Params.ParamByName('DATA_INI').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 00:00:00');
    dmRelatorios.qryRel09_DespR.Params.ParamByName('DATA_FIM').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 23:59:59');
    dmRelatorios.qryRel09_DespR.Open;

    { OUTROS LANCAMENTOS }
    //Receitas Pendentes
    dmRelatorios.qryRel09_RecPend.Close;
    dmRelatorios.qryRel09_RecPend.Params.ParamByName('DATA_INI').Value  := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 23:59:59');
    dmRelatorios.qryRel09_RecPend.Params.ParamByName('DATA_FIM').Value  := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 23:59:59');
    dmRelatorios.qryRel09_RecPend.Open;

    //Depositos Sem Baixa
    dmRelatorios.qryRel09_DepoSBx.Close;
    dmRelatorios.qryRel09_DepoSBx.Params.ParamByName('DATA_FIM1').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 23:59:59');
    dmRelatorios.qryRel09_DepoSBx.Params.ParamByName('DATA_FIM2').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 23:59:59');
    dmRelatorios.qryRel09_DepoSBx.Params.ParamByName('DATA_FIM3').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 23:59:59');
    dmRelatorios.qryRel09_DepoSBx.Params.ParamByName('DATA_FIM4').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 23:59:59');
    dmRelatorios.qryRel09_DepoSBx.Open;

    cTotalDepositosSemBaixa := 0;

    dmRelatorios.qryRel09_DepoSBx.First;

    while not dmRelatorios.qryRel09_DepoSBx.Eof do
    begin
      cTotalDepositosSemBaixa := (cTotalDepositosSemBaixa + dmRelatorios.qryRel09_DepoSBx.FieldByName('VR_DEPOSITO_FLUTUANTE').AsCurrency);

      dmRelatorios.qryRel09_DepoSBx.Next;
    end;

    //Flutuantes Sem Baixa
    dmRelatorios.qryRel09_FlutSBx.Close;
    dmRelatorios.qryRel09_FlutSBx.Params.ParamByName('DATA_FIM1').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 23:59:59');
    dmRelatorios.qryRel09_FlutSBx.Params.ParamByName('DATA_FIM2').Value := (FormatDateTime('YYYY-MM-DD', dteData.Date) + ' 23:59:59');
    dmRelatorios.qryRel09_FlutSBx.Open;

    dmGerencial.Processando(False);

    GerarRelatorio(True);
  end;

  FreeAndNil(QryLanc);
  FreeAndNil(QryRecPAnt);
end;

procedure TFFiltroRelatorioMovimentoCaixa.dteDataExit(Sender: TObject);
begin
  inherited;

  RecuperarValorContaCorrente;
end;

procedure TFFiltroRelatorioMovimentoCaixa.dteDataKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    RecuperarValorContaCorrente;

    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    RecuperarValorContaCorrente;

    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroRelatorioMovimentoCaixa.FormCreate(Sender: TObject);
begin
  inherited;

  dDataPerInicio := 0;
  dDataPerFim    := 0;
end;

procedure TFFiltroRelatorioMovimentoCaixa.FormShow(Sender: TObject);
begin
  inherited;

  if DataMovimento = 0 then
    dteData.Date := Date
  else
    dteData.Date := DataMovimento;

  RecuperarValorContaCorrente;

  gbTipo.Visible := False;

  chbTipoSintetico.Visible := False;
  chbTipoAnalitico.Visible := False;

  chbExportarPDF.Checked := False;

  chbExportarExcel.Enabled := vgPrm_ExpMovCx or vgPrm_ExpRelMovCx;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;

  chbExibirGrafico.Visible := False;

  if dteData.CanFocus then
    dteData.SetFocus;
end;

procedure TFFiltroRelatorioMovimentoCaixa.GerarRelatorio(ImpDet: Boolean);
begin
  inherited;

  try
    sTituloRelatorio    := 'MOVIMENTO DE CAIXA';
    sSubtituloRelatorio := 'Dia ' + DateToStr(dteData.Date);

    if ImpDet then
    begin
      lExibeDetalhe := True;
      dmRelatorios.DefinirRelatorio(R09DET);
    end
    else
    begin
      lExibeDetalhe := False;
      dmRelatorios.DefinirRelatorio(R09);
    end;

    dmRelatorios.ImprimirRelatorio(chbExportarPDF.Checked, chbExportarExcel.Checked);

    GravarLog;
  except
    Application.MessageBox('Erro ao gerar Movimento de Caixa.',
                           'Erro',
                           MB_OK + MB_ICONERROR);
  end;
end;

procedure TFFiltroRelatorioMovimentoCaixa.GravarLog;
begin
  inherited;

  BS.GravarUsuarioLog(50,
                      '',
                      'Impress�o de Relatorio de Movimento de Caixa (' + DateToStr(dDataRelatorio) +
                                                                     ' ' +
                                                                     TimeToStr(tHoraRelatorio) + ')',
                      0,
                      '');
end;

procedure TFFiltroRelatorioMovimentoCaixa.RecuperarValorContaCorrente;
var
  QryCx: TFDQuery;
begin
  QryCx := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

  cTotalUltimoFechamento := 0;
  cContagemTotal         := 0;
  cContagemContaCorrente := 0;

  dDataUltimoFechamento := 0;

  sObservacaoFechamento := '';

  QryCx.Close;
  QryCx.SQL.Clear;

  QryCx.SQL.Text := 'SELECT CA.VR_TOTAL_ULT_FECH, ' +
                    '       (SELECT FIRST 1 CFF.DATA_FECHAMENTO ' +
                    '          FROM CAIXA_FECHAMENTO CFF ' +
                    '         WHERE CFF.FLG_CANCELADO = ' + QuotedStr('N') +
                    '           AND CFF.DATA_FECHAMENTO < CA.DATA_ABERTURA ' +
                    '        ORDER BY CFF.DATA_FECHAMENTO DESC) AS DATA_ULT_FECHAMENTO, ' +
                    '       (CASE WHEN (CF.ID_CAIXA_FECHAMENTO IS NULL) ' +
                    '             THEN CA.VR_NOTAS ' +
                    '             ELSE CF.VR_NOTAS ' +
                    '        END) AS CONTAGEM_NOTAS, ' +
                    '       (CASE WHEN (CF.ID_CAIXA_FECHAMENTO IS NULL) ' +
                    '             THEN CA.VR_MOEDAS ' +
                    '             ELSE CF.VR_MOEDAS ' +
                    '        END) AS CONTAGEM_MOEDAS, ' +
                    '       (CASE WHEN (CF.ID_CAIXA_FECHAMENTO IS NULL) ' +
                    '             THEN CA.VR_CONTACORRENTE ' +
                    '             ELSE CF.VR_CONTACORRENTE ' +
                    '        END) AS CONTA_CORRENTE, ' +
                    '       CF.VR_SALDO_DIA, ' +
                    '       CF.OBS_CAIXA_FECHAMENTO ' +
                    '  FROM CAIXA_ABERTURA CA ' +
                    '  LEFT JOIN CAIXA_FECHAMENTO CF ' +
                    '    ON CA.DATA_ABERTURA = CF.DATA_FECHAMENTO ' +
                    ' WHERE CA.DATA_ABERTURA = :DATA_ABERTURA ' +
                    '   AND CA.FLG_CANCELADO = ' + QuotedStr('N');

  QryCx.Params.ParamByName('DATA_ABERTURA').Value := dteData.Date;
  QryCx.Open;

  if QryCx.RecordCount = 0 then
  begin
    cedVlrRealTContFinal.Value := 0;

    lblContaCorrente.Visible := True;
    btnVisualizar.Enabled    := False;
  end
  else
  begin
    if QryCx.FieldByName('DATA_ULT_FECHAMENTO').IsNull then
      dDataUltimoFechamento := vgDataUltFech
    else
      dDataUltimoFechamento := QryCx.FieldByName('DATA_ULT_FECHAMENTO').AsDateTime;

    cContagemContaCorrente := QryCx.FieldByName('CONTA_CORRENTE').AsCurrency;

    cContagemTotal := (QryCx.FieldByName('CONTAGEM_NOTAS').AsCurrency +
                     QryCx.FieldByName('CONTAGEM_MOEDAS').AsCurrency);

    if not QryCx.FieldByName('VR_SALDO_DIA').IsNull then
      cedVlrRealTContFinal.Value := QryCx.FieldByName('VR_SALDO_DIA').AsCurrency;

    if not QryCx.FieldByName('OBS_CAIXA_FECHAMENTO').IsNull then
      sObservacaoFechamento := QryCx.FieldByName('OBS_CAIXA_FECHAMENTO').AsString;

    if QryCx.FieldByName('VR_TOTAL_ULT_FECH').IsNull then
      cTotalUltimoFechamento := 0
    else
      cTotalUltimoFechamento := QryCx.FieldByName('VR_TOTAL_ULT_FECH').AsCurrency;

    lblContaCorrente.Visible := False;
    btnVisualizar.Enabled    := True;
  end;

  FreeAndNil(QryCx);
end;

function TFFiltroRelatorioMovimentoCaixa.VerificarFiltro: Boolean;
var
  Msg: String;
begin
  Result := True;

  if dteData.Date = 0 then
  begin
    Msg := '- Informe a DATA DO FECHAMENTO.';
    Result := False;
  end;

  if not Result then
    Application.MessageBox(PChar(':: ATEN��O ::' + #13#10 + #13#10 + Msg),
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
end;

end.
