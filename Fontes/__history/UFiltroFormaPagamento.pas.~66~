{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroFormaPagamento.pas
  Descricao:   Filtro de Formas de Pagamento
  Author   :   Cristina
  Date:        04-mar-2016
  Last Update: 11-nov-2016  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroFormaPagamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.StdCtrls, frxClass, frxDBSet;

type
  TFFiltroFormaPagamento = class(TFFiltroSimplesPadrao)
    lblDescricao: TLabel;
    edtDescricao: TEdit;
    rgFlgAtiva: TRadioGroup;
    qryGridPaiPadraoID: TIntegerField;
    qryGridPaiPadraoDESCR_FORMAPAGAMENTO: TStringField;
    qryGridPaiPadraoMAX_PARCELAS: TIntegerField;
    qryGridPaiPadraoMIN_PARCELAS: TIntegerField;
    qryGridPaiPadraoPRAZO: TIntegerField;
    qryGridPaiPadraoPERC_TARIFA: TBCDField;
    qryGridPaiPadraoFLG_ATIVA: TStringField;
    qryGridPaiPadraoTIPO: TStringField;
    procedure edtDescricaoKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure rgFlgAtivaExit(Sender: TObject);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroFormaPagamento: TFFiltroFormaPagamento;

  IdFormaPagamento: Integer;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UCadastroFormaPagamento;

{ TFFiltroFormaPagamento }

procedure TFFiltroFormaPagamento.btnEditarClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao     := E;
  vgIdConsulta   := qryGridPaiPadrao.FieldByName('ID').AsInteger;
  vgOrigemFiltro := True;

  dmGerencial.CriarForm(TFCadastroFormaPagamento, FCadastroFormaPagamento);

  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroFormaPagamento.btnExcluirClick(Sender: TObject);
var
  qryExclusao: TFDQuery;
begin
  inherited;

  if lPodeExcluir then
  begin
    qryExclusao := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      with qryExclusao, SQL do
      begin
        Close;
        Clear;
        Text := 'DELETE FROM FORMAPAGAMENTO WHERE ID_FORMAPAGAMENTO = :ID_FORMAPAGAMENTO';
        Params.ParamByName('ID_FORMAPAGAMENTO').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      GravarLog;
      Application.MessageBox('Forma de Pagamento exclu�da com sucesso!', 'Aviso', MB_OK)
    except
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Application.MessageBox('Erro na exclus�o da Forma de Pagamento.', 'Erro', MB_OK + MB_ICONERROR)
    end;

    FreeAndNil(qryExclusao);

    btnFiltrar.Click;
  end;
end;

procedure TFFiltroFormaPagamento.btnFiltrarClick(Sender: TObject);
begin
  with qryGridPaiPadrao, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_FORMAPAGAMENTO AS ID, ' +
            '       DESCR_FORMAPAGAMENTO, ' +
            '       MAX_PARCELAS, ' +
            '       MIN_PARCELAS, ' +
            '       PRAZO, ' +
            '       PERC_TARIFA, ' +
            '       TIPO, ' +
            '       FLG_ATIVA ' +
            '  FROM FORMAPAGAMENTO ' +
            ' WHERE (1 = 1) ';

    if Trim(edtDescricao.Text) <> '' then
    begin
      Add(' AND CAST(UPPER(DESCR_FORMAPAGAMENTO) AS VARCHAR(250)CHARACTER SET ISO8859_1) COLLATE PT_BR CONTAINING CAST(:DESCR AS VARCHAR(250))');
      Params.ParamByName('DESCR').Value := Trim(edtDescricao.Text);
    end;

    if rgFlgAtiva.ItemIndex = 1 then
      Add(' AND FLG_ATIVA = ' + QuotedStr('S'))
    else if rgFlgAtiva.ItemIndex = 2 then
      Add(' AND FLG_ATIVA = ' + QuotedStr('N'));

    Add('ORDER BY DESCR_FORMAPAGAMENTO');
    Open;
  end;

  inherited;
end;

procedure TFFiltroFormaPagamento.btnIncluirClick(Sender: TObject);
begin
  inherited;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := True;

  dmGerencial.CriarForm(TFCadastroFormaPagamento, FCadastroFormaPagamento);

  btnFiltrar.Click;
end;

procedure TFFiltroFormaPagamento.btnLimparClick(Sender: TObject);
begin
  edtDescricao.Clear;
  rgFlgAtiva.ItemIndex := 0;

  inherited;

  if edtDescricao.CanFocus then
    edtDescricao.SetFocus;
end;

procedure TFFiltroFormaPagamento.dbgGridPaiPadraoDblClick(Sender: TObject);
var
  sTexto: String;
begin
  inherited;

  if vgOrigemCadastro then
  begin
    if qryGridPaiPadrao.RecordCount = 0 then
      IdFormaPagamento := 0
    else
    begin
      IdFormaPagamento := qryGridPaiPadrao.FieldByName('ID').AsInteger;

      sTexto := 'Confirma a sele��o de ' + qryGridPaiPadrao.FieldByName('DESCR_FORMAPAGAMENTO').AsString + '?';

      if Application.MessageBox(PChar(sTexto), 'Confirma��o', MB_YESNO + MB_ICONQUESTION) = ID_YES then
        Self.Close
      else
        IdFormaPagamento := 0;
    end;
  end
  else
  begin
    PegarPosicaoGrid;

    vgOperacao     := C;
    vgIdConsulta   := qryGridPaiPadrao.FieldByName('ID').AsInteger;
    vgOrigemFiltro := True;

    dmGerencial.CriarForm(TFCadastroFormaPagamento, FCadastroFormaPagamento);

    DefinirPosicaoGrid;
  end;
end;

procedure TFFiltroFormaPagamento.edtDescricaoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroFormaPagamento.FormCreate(Sender: TObject);
begin
  inherited;

  IdFormaPagamento := 0;
end;

procedure TFFiltroFormaPagamento.FormShow(Sender: TObject);
begin
  btnImprimir.Visible := False;

  with qryGridPaiPadrao, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_FORMAPAGAMENTO AS ID, ' +
            '       DESCR_FORMAPAGAMENTO, ' +
            '       MAX_PARCELAS, ' +
            '       MIN_PARCELAS, ' +
            '       PRAZO, ' +
            '       PERC_TARIFA, ' +
            '       TIPO, ' +
            '       FLG_ATIVA ' +
            '  FROM FORMAPAGAMENTO ' +
            'ORDER BY DESCR_FORMAPAGAMENTO';
    Open;
  end;

  ShowScrollBar(dbgGridPaiPadrao.Handle, SB_HORZ, False);

  inherited;

  if edtDescricao.CanFocus then
    edtDescricao.SetFocus;
end;

procedure TFFiltroFormaPagamento.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    X:  //EXCLUSAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da exclus�o desse Vale:', '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'VALE');
    end;
    P:  //IMPRESSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente impress�o de Lista de Vales',
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'VALE');
    end;
    T:  //EXPORTACAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente exporta��o de Lista de Vales',
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'VALE');
    end;
  end;
end;

function TFFiltroFormaPagamento.PodeExcluir(var Msg: String): Boolean;
var
  QryVerif: TFDQuery;
begin
  inherited;

  Result := True;

  QryVerif := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryVerif, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT * FROM LANCAMENTO_PARC WHERE ID_FORMAPAGAMENTO_FK = :ID_FORMAPAGAMENTO';
    Params.ParamByName('ID_FORMAPAGAMENTO').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
    Open;
  end;

  if QryVerif.RecordCount > 0 then
    msg := 'N�o � poss�vel excluir a Forma de Pagamento informada, pois a mesma j� possui registro no Sistema.';

  Result := (QryVerif.RecordCount = 0) and (qryGridPaiPadrao.RecordCount > 0);

  FreeAndNil(QryVerif);
end;

procedure TFFiltroFormaPagamento.rgFlgAtivaExit(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroFormaPagamento.VerificarPermissoes;
begin
  inherited;

  btnIncluir.Enabled  := (vgUsu_FlgSuporte = 'S');  //vgPrm_IncFPagto;
  btnEditar.Enabled   := (vgUsu_FlgSuporte = 'S'); //vgPrm_EdFPagto and (qryGridPaiPadrao.RecordCount > 0);
  btnExcluir.Enabled  := (vgUsu_FlgSuporte = 'S'); //vgPrm_ExcFPagto and (qryGridPaiPadrao.RecordCount > 0);
  btnImprimir.Enabled := False;
  btnExportar.Enabled := False;
end;

end.
