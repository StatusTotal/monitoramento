{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroRelatorioRecDespIR.pas
  Descricao:   Filtro Relatorios de Receitas e Despesas para o IR (R11 - R11DET)
  Author   :   Cristina
  Date:        03-mai-2017
  Last Update:
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroRelatorioRecDespIR;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroRelatorioPadrao, Vcl.StdCtrls,
  JvExControls, JvButton, JvTransparentButton, Vcl.ExtCtrls, Vcl.Mask, JvExMask,
  JvToolEdit, System.DateUtils;

type
  TFFiltroRelatorioRecDespIR = class(TFFiltroRelatorioPadrao)
    lblPeriodo: TLabel;
    lblPeriodoInicio: TLabel;
    dteDataInicio: TJvDateEdit;
    lblPeriodoFim: TLabel;
    dteDataFim: TJvDateEdit;
    rgTipoLancamento: TRadioGroup;
    procedure btnLimparClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    procedure GravarLog; override;
    procedure GerarRelatorio(ImpDet: Boolean); override;
    function VerificarFiltro: Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroRelatorioRecDespIR: TFFiltroRelatorioRecDespIR;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UDMRelatorios, UGDM, UVariaveisGlobais;

{ TFFiltroRelatorioRecDespIR }

procedure TFFiltroRelatorioRecDespIR.btnLimparClick(Sender: TObject);
begin
  inherited;

  dteDataInicio.Date := StartOfTheYear(Date);
  dteDataFim.Date    := EndOfTheYear(Date);

  rgTipoLancamento.ItemIndex := 0;

  chbTipoSintetico.Checked := True;
  chbTipoAnalitico.Checked := False;

  chbExportarPDF.Checked   := True;
  chbExportarExcel.Checked := False;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioRecDespIR.btnVisualizarClick(Sender: TObject);
begin
  inherited;

  if not VerificarFiltro then
    Exit;

  //Sintetico
  dmRelatorios.qryRel11_RecDespIR.Close;

  dmRelatorios.qryRel11_RecDespIR.Params.ParamByName('DATA_INI').Value := (FormatDateTime('YYYY-MM-DD', dteDataInicio.Date) + ' 00:00:00');
  dmRelatorios.qryRel11_RecDespIR.Params.ParamByName('DATA_FIM').Value := (FormatDateTime('YYYY-MM-DD', dteDataFim.Date) + ' 23:59:59');

  case rgTipoLancamento.ItemIndex of
    0:
    begin
      dmRelatorios.qryRel11_RecDespIR.Params.ParamByName('TP_LANC01').Value := 'R';
      dmRelatorios.qryRel11_RecDespIR.Params.ParamByName('TP_LANC02').Value := 'D';
    end;
    1:
    begin
      dmRelatorios.qryRel11_RecDespIR.Params.ParamByName('TP_LANC01').Value := 'R';
      dmRelatorios.qryRel11_RecDespIR.Params.ParamByName('TP_LANC02').Value := 'R';
    end;
    2:
    begin
      dmRelatorios.qryRel11_RecDespIR.Params.ParamByName('TP_LANC01').Value := 'D';
      dmRelatorios.qryRel11_RecDespIR.Params.ParamByName('TP_LANC02').Value := 'D';
    end;
  end;

  dmRelatorios.qryRel11_RecDespIR.Open;

  if dmRelatorios.qryRel11_RecDespIR.RecordCount = 0 then
  begin
    Application.MessageBox('N�o h� Lan�amentos registrados.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
  end
  else
  begin
    dmRelatorios.qryRel11_RecDespIR_DET.Close;
    dmRelatorios.qryRel11_RecDespIR_DET.Params.ParamByName('COD_LANCAMENTO').Value := dmRelatorios.qryRel11_RecDespIR.FieldByName('COD_LANCAMENTO').AsInteger;
    dmRelatorios.qryRel11_RecDespIR_DET.Params.ParamByName('ANO_LANCAMENTO').Value := dmRelatorios.qryRel11_RecDespIR.FieldByName('ANO_LANCAMENTO').AsInteger;
    dmRelatorios.qryRel11_RecDespIR_DET.Open;

    if chbTipoSintetico.Checked then
      GerarRelatorio(False);

    if chbTipoAnalitico.Checked then
      GerarRelatorio(True);
  end;
end;

procedure TFFiltroRelatorioRecDespIR.FormShow(Sender: TObject);
begin
  inherited;

  dteDataInicio.Date := StartOfTheYear(Date);
  dteDataFim.Date    := EndOfTheYear(Date);

  rgTipoLancamento.ItemIndex := 0;

  chbTipoSintetico.Checked := True;
  chbTipoAnalitico.Checked := False;

  chbExportarPDF.Checked   := True;
  chbExportarExcel.Checked := False;

  chbExibirGrafico.Visible := False;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioRecDespIR.GerarRelatorio(ImpDet: Boolean);
begin
  inherited;

  try
    sTituloRelatorio    := 'IMPOSTO DE RENDA';
    sSubtituloRelatorio := 'Per�odo de ' + DateToStr(dteDataInicio.Date) + ' a ' + DateToStr(dteDataFim.Date);

    if ImpDet then
      dmRelatorios.DefinirRelatorio(R11DET)
    else
      dmRelatorios.DefinirRelatorio(R11);

    lPodeCalcular := True;
    dmRelatorios.ImprimirRelatorio(chbExportarPDF.Checked, chbExportarExcel.Checked);

    GravarLog;
  except
    Application.MessageBox('Erro ao gerar Relat�rio de Imposto de Renda.',
                           'Erro',
                           MB_OK + MB_ICONERROR);
  end;
end;

procedure TFFiltroRelatorioRecDespIR.GravarLog;
begin
  inherited;

  BS.GravarUsuarioLog(50,
                      '',
                      'Impress�o de Relatorio de Imposto de Renda (' + DateToStr(dDataRelatorio) +
                                                                   ' ' +
                                                                   TimeToStr(tHoraRelatorio) + ')',
                      0,
                      '');
end;

function TFFiltroRelatorioRecDespIR.VerificarFiltro: Boolean;
var
  Msg: String;
begin
  Result := True;

  if dteDataInicio.Date = 0 then
  begin
    Msg := '- Informe a DATA DE IN�CIO do Per�odo.';
    Result := False;
  end;

  if dteDataFim.Date = 0 then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe a DATA DE FIM do Per�odo.'
    else
      Msg := #13#10 + '- Informe a DATA DE FIM do Per�odo.';

    Result := False;
  end;

  if dteDataFim.Date < dteDataInicio.Date then
  begin
    if Trim(Msg) = '' then
      Msg := '- A DATA DE FIM do Per�odo n�o pode inferior � DATA DE IN�CIO do mesmo.'
    else
      Msg := #13#10 + '- A DATA DE FIM do Per�odo n�o pode ser inferior � DATA DE IN�CIO do mesmo.';

    Result := False;
  end;

  if (not chbTipoAnalitico.Checked) and
    (not chbTipoSintetico.Checked) then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe o TIPO DE LAN�AMENTO do Relat�rio de Imposto de Renda.'
    else
      Msg := #13#10 + '- Informe o TIPO DE LAN�AMENTO do Relat�rio de Imposto de Renda.';

    Result := False;
  end;

  if not Result then
    Application.MessageBox(PChar(':: ATEN��O ::' + #13#10 + #13#10 + Msg),
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
end;

end.
