{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UDMCaixa.pas
  Descricao:   Data Module de Abertura e Fechamento de Caixa
  Author   :   Cristina
  Date:        23-dez-2016
  Last Update: 12-jan-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UDMCaixa;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.Provider,
  Datasnap.DBClient, System.DateUtils;

type
  TdmCaixa = class(TDataModule)
    qryDeposSBx: TFDQuery;
    qryFlutSBx: TFDQuery;
    dsDeposSBx: TDataSource;
    dsFlutSBx: TDataSource;
    dsImportDH: TDataSource;
    dsImportRep: TDataSource;
    dsRecAntCT: TDataSource;
    dsDespDesc: TDataSource;
    dsDespRepFlut: TDataSource;
    dsDespDH: TDataSource;
    qryImportDH: TFDQuery;
    qryImportRep: TFDQuery;
    qryRecAntCT: TFDQuery;
    qryDespDesc: TFDQuery;
    qryDespRepFlut: TFDQuery;
    qryDespDH: TFDQuery;
    dsImportDP: TDataSource;
    qryImportDP: TFDQuery;
    dsImportCT: TDataSource;
    qryImportCT: TFDQuery;
    dsImportCH: TDataSource;
    qryImportCH: TFDQuery;
    dsImportPR: TDataSource;
    dsImportFT: TDataSource;
    qryImportFT: TFDQuery;
    qryImportPR: TFDQuery;
    dsORecDH: TDataSource;
    qryORecDH: TFDQuery;
    dsORecDP: TDataSource;
    qryORecDP: TFDQuery;
    dsORecCT: TDataSource;
    qryORecCT: TFDQuery;
    dsORecCH: TDataSource;
    qryORecCH: TFDQuery;
    dsORecPR: TDataSource;
    qryORecPR: TFDQuery;
    dsORecFT: TDataSource;
    qryORecFT: TFDQuery;
    dsORecRep: TDataSource;
    qryORecRep: TFDQuery;
    dsRecAntCH: TDataSource;
    qryRecAntCH: TFDQuery;
    dsRecAntPR: TDataSource;
    qryRecAntPR: TFDQuery;
    dsRecAntFT: TDataSource;
    qryRecAntFT: TFDQuery;
    dsRecPendCT: TDataSource;
    qryRecPendCT: TFDQuery;
    dsRecPendCH: TDataSource;
    qryRecPendCH: TFDQuery;
    dsRecPendPR: TDataSource;
    qryRecPendPR: TFDQuery;
    dsRecPendFT: TDataSource;
    qryRecPendFT: TFDQuery;
    qryImportTotal: TFDQuery;
    qryORecTotal: TFDQuery;
    dsImportTotal: TDataSource;
    dsORecTotal: TDataSource;
    cdsFechCX_F: TClientDataSet;
    cdsFechCX_FID_CAIXA_FECHAMENTO: TIntegerField;
    cdsFechCX_FDATA_FECHAMENTO: TDateField;
    cdsFechCX_FHORA_FECHAMENTO: TTimeField;
    cdsFechCX_FVR_TOTAL_ULT_AB: TBCDField;
    cdsFechCX_FVR_RECIBO_IMPORTADO: TBCDField;
    cdsFechCX_FVR_IMPORT_DINHEIRO: TBCDField;
    cdsFechCX_FVR_IMPORT_DEPOSITO: TBCDField;
    cdsFechCX_FVR_IMPORT_CARTAO: TBCDField;
    cdsFechCX_FVR_IMPORT_CHEQUE: TBCDField;
    cdsFechCX_FVR_IMPORT_PROMISSORIA: TBCDField;
    cdsFechCX_FVR_IMPORT_FATURADO: TBCDField;
    cdsFechCX_FVR_IMPORT_REPASSES: TBCDField;
    cdsFechCX_FVR_OUTRASREC_TOTAL: TBCDField;
    cdsFechCX_FVR_OUTRASREC_DINHEIRO: TBCDField;
    cdsFechCX_FVR_OUTRASREC_DEPOSITO: TBCDField;
    cdsFechCX_FVR_OUTRASREC_CARTAO: TBCDField;
    cdsFechCX_FVR_OUTRASREC_CHEQUE: TBCDField;
    cdsFechCX_FVR_OUTRASREC_PROMISSORIA: TBCDField;
    cdsFechCX_FVR_OUTRASREC_FATURADO: TBCDField;
    cdsFechCX_FVR_OUTRASREC_REPASSES: TBCDField;
    cdsFechCX_FVR_SRVANT_CARTAO: TBCDField;
    cdsFechCX_FVR_SRVANT_CHEQUE: TBCDField;
    cdsFechCX_FVR_SRVANT_PROMISSORIA: TBCDField;
    cdsFechCX_FVR_SRVANT_FATURADO: TBCDField;
    cdsFechCX_FVR_RECPEND_CARTAO: TBCDField;
    cdsFechCX_FVR_RECPEND_CHEQUE: TBCDField;
    cdsFechCX_FVR_RECPEND_PROMISSORIA: TBCDField;
    cdsFechCX_FVR_RECPEND_FATURADO: TBCDField;
    cdsFechCX_FVR_FLUT_DEPOSITO: TBCDField;
    cdsFechCX_FVR_FLUT_REPASSE: TBCDField;
    cdsFechCX_FVR_FECH_ANTERIOR: TBCDField;
    cdsFechCX_FVR_TOTAL_RECEITA: TBCDField;
    cdsFechCX_FVR_TOTAL_DESPESA: TBCDField;
    cdsFechCX_FVR_SALDO_DIA: TBCDField;
    cdsFechCX_FVR_DESP_DINHEIRO: TBCDField;
    cdsFechCX_FVR_DESP_DESCONTO: TBCDField;
    cdsFechCX_FVR_DESP_REPASSE: TBCDField;
    cdsFechCX_FVR_NOTAS: TBCDField;
    cdsFechCX_FVR_MOEDAS: TBCDField;
    cdsFechCX_FVR_CONTACORRENTE: TBCDField;
    cdsFechCX_FVR_CONTAGEM_FINAL: TBCDField;
    cdsFechCX_FVR_DIFERENCA: TBCDField;
    cdsFechCX_FVR_TOTAL_FECHAMENTO: TBCDField;
    cdsFechCX_FVR_RECPEND_TOTAL: TCurrencyField;
    cdsFechCX_FVR_SALDO_GERAL: TCurrencyField;
    cdsFechCX_FOBS_CAIXA_FECHAMENTO: TStringField;
    cdsFechCX_FCAD_ID_USUARIO: TIntegerField;
    cdsFechCX_FFLG_CANCELADO: TStringField;
    cdsFechCX_FDATA_CANCELAMENTO: TDateField;
    cdsFechCX_FCANCEL_ID_USUARIO: TIntegerField;
    cdsFechCX_FNOME_USUARIO: TStringField;
    dspFechCX_F: TDataSetProvider;
    qryFechCX_F: TFDQuery;
    qryFechCXImg_F: TFDQuery;
    dspFechCXImg_F: TDataSetProvider;
    cdsFechCXImg_F: TClientDataSet;
    dsFechCX_F: TDataSource;
    dsFechCXImg_F: TDataSource;
    cdsFechCXImg_FID_CAIXA_FECHAMENTO_IMG: TIntegerField;
    cdsFechCXImg_FID_CAIXA_FECHAMENTO_FK: TIntegerField;
    cdsFechCXImg_FNOME_FIXO: TStringField;
    cdsFechCXImg_FNOME_VARIAVEL: TStringField;
    cdsFechCXImg_FEXTENSAO: TStringField;
    cdsFechCXImg_FNOVO: TBooleanField;
    cdsFechCXImg_FORIGEM_ANTIGA: TStringField;
    cdsFechCXImg_FNOME_V_ANTIGO: TStringField;
    cdsFechCXImg_FCAMINHO_COMPLETO: TStringField;
    cdsFechCX_FVR_SRVANT_TOTAL: TCurrencyField;
    cdsFechCX_FVR_FLUT_TOTAL: TCurrencyField;
    procedure cdsFechCXImg_FCalcFields(DataSet: TDataSet);
    procedure cdsFechCX_FVR_TOTAL_ULT_ABGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_RECIBO_IMPORTADOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_IMPORT_DINHEIROGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_IMPORT_DEPOSITOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_IMPORT_CARTAOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_IMPORT_CHEQUEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_IMPORT_PROMISSORIAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_IMPORT_FATURADOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_IMPORT_REPASSESGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_OUTRASREC_TOTALGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_OUTRASREC_DINHEIROGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_OUTRASREC_DEPOSITOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_OUTRASREC_CARTAOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_OUTRASREC_CHEQUEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_OUTRASREC_PROMISSORIAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_OUTRASREC_FATURADOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_OUTRASREC_REPASSESGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_SRVANT_CARTAOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_SRVANT_CHEQUEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_SRVANT_PROMISSORIAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_SRVANT_FATURADOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_RECPEND_CARTAOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_RECPEND_CHEQUEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_RECPEND_PROMISSORIAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_RECPEND_FATURADOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_FLUT_DEPOSITOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_FLUT_REPASSEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_FECH_ANTERIORGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_TOTAL_RECEITAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_TOTAL_DESPESAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_SALDO_DIAGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsFechCX_FVR_DESP_DINHEIROGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_DESP_DESCONTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_DESP_REPASSEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_NOTASGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsFechCX_FVR_MOEDASGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsFechCX_FVR_CONTACORRENTEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_CONTAGEM_FINALGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_DIFERENCAGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsFechCX_FVR_TOTAL_FECHAMENTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_RECPEND_TOTALGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsFechCX_FVR_SALDO_GERALGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsFechCX_FCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }

    procedure AbrirQrys(sDtIni, sDtFim: String);
  end;

var
  dmCaixa: TdmCaixa;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{$R *.dfm}

{ TdmCaixa }

procedure TdmCaixa.AbrirQrys(sDtIni, sDtFim: String);
begin
  { IMPORTACAO }
  //Total
  qryImportTotal.Close;
  qryImportTotal.Params.ParamByName('DATA_INIP').Value := sDtIni;
  qryImportTotal.Params.ParamByName('DATA_FIMP').Value := sDtFim;
  qryImportTotal.Open;

  //Pgto. Dinheiro
  qryImportDH.Close;
  qryImportDH.Params.ParamByName('DATA_INIP').Value := sDtIni;
  qryImportDH.Params.ParamByName('DATA_FIMP').Value := sDtFim;
  qryImportDH.Open;

  //Pgto. Deposito
  qryImportDP.Close;
  qryImportDP.Params.ParamByName('DATA_INIP').Value := sDtIni;
  qryImportDP.Params.ParamByName('DATA_FIMP').Value := sDtFim;
  qryImportDP.Open;

  //Pgto. Cartao
  qryImportCT.Close;
  qryImportCT.Params.ParamByName('DATA_INIP').Value := sDtIni;
  qryImportCT.Params.ParamByName('DATA_FIMP').Value := sDtFim;
  qryImportCT.Open;

  //Pgto. Cheque
  qryImportCH.Close;
  qryImportCH.Params.ParamByName('DATA_INIP').Value := sDtIni;
  qryImportCH.Params.ParamByName('DATA_FIMP').Value := sDtFim;
  qryImportCH.Open;

  //Pgto. Promissoria
  qryImportPR.Close;
  qryImportPR.Params.ParamByName('DATA_INIP').Value := sDtIni;
  qryImportPR.Params.ParamByName('DATA_FIMP').Value := sDtFim;
  qryImportPR.Open;

  //Pgto. Faturado
  qryImportFT.Close;
  qryImportFT.Params.ParamByName('DATA_INIP').Value := sDtIni;
  qryImportFT.Params.ParamByName('DATA_FIMP').Value := sDtFim;
  qryImportFT.Open;

  //Receitas para Repasses
  qryImportRep.Close;
  qryImportRep.Params.ParamByName('DATA_INIP').Value := sDtIni;
  qryImportRep.Params.ParamByName('DATA_FIMP').Value := sDtFim;
  qryImportRep.Open;

  { OUTRAS RECEITAS }
  //Total
  qryORecTotal.Close;
  qryORecTotal.Params.ParamByName('DATA_INIP').Value := sDtIni;
  qryORecTotal.Params.ParamByName('DATA_FIMP').Value := sDtFim;
  qryORecTotal.Open;

  //Pgto. Dinheiro
  qryORecDH.Close;
  qryORecDH.Params.ParamByName('DATA_INIP').Value := sDtIni;
  qryORecDH.Params.ParamByName('DATA_FIMP').Value := sDtFim;
  qryORecDH.Open;

  //Pgto. Deposito
  qryORecDP.Close;
  qryORecDP.Params.ParamByName('DATA_INIP').Value := sDtIni;
  qryORecDP.Params.ParamByName('DATA_FIMP').Value := sDtFim;
  qryORecDP.Open;

  //Pgto. Cartao
  qryORecCT.Close;
  qryORecCT.Params.ParamByName('DATA_INIP').Value := sDtIni;
  qryORecCT.Params.ParamByName('DATA_FIMP').Value := sDtFim;
  qryORecCT.Open;

  //Pgto. Cheque
  qryORecCH.Close;
  qryORecCH.Params.ParamByName('DATA_INIP').Value := sDtIni;
  qryORecCH.Params.ParamByName('DATA_FIMP').Value := sDtFim;
  qryORecCH.Open;

  //Pgto. Promissoria
  qryORecPR.Close;
  qryORecPR.Params.ParamByName('DATA_INIP').Value := sDtIni;
  qryORecPR.Params.ParamByName('DATA_FIMP').Value := sDtFim;
  qryORecPR.Open;

  //Pgto. Faturado
  qryORecFT.Close;
  qryORecFT.Params.ParamByName('DATA_INIP').Value := sDtIni;
  qryORecFT.Params.ParamByName('DATA_FIMP').Value := sDtFim;
  qryORecFT.Open;

  //Receitas para Repasses
  qryORecRep.Close;
  qryORecRep.Params.ParamByName('DATA_INIP').Value := sDtIni;
  qryORecRep.Params.ParamByName('DATA_FIMP').Value := sDtFim;
  qryORecRep.Open;

  { RECEITAS ORIUNDAS DE RECIBOS ANTERIORES }
  //Cartao
  qryRecAntCT.Close;
  qryRecAntCT.Params.ParamByName('DATA_INIP1').Value := sDtIni;
  qryRecAntCT.Params.ParamByName('DATA_FIMP').Value  := sDtFim;
  qryRecAntCT.Params.ParamByName('DATA_INIP2').Value := sDtIni;
  qryRecAntCT.Open;

  //Cheque
  qryRecAntCH.Close;
  qryRecAntCH.Params.ParamByName('DATA_INIP1').Value := sDtIni;
  qryRecAntCH.Params.ParamByName('DATA_FIMP').Value  := sDtFim;
  qryRecAntCH.Params.ParamByName('DATA_INIP2').Value := sDtIni;
  qryRecAntCH.Open;

  //Promissoria
  qryRecAntPR.Close;
  qryRecAntPR.Params.ParamByName('DATA_INIP1').Value := sDtIni;
  qryRecAntPR.Params.ParamByName('DATA_FIMP').Value  := sDtFim;
  qryRecAntPR.Params.ParamByName('DATA_INIP2').Value := sDtIni;
  qryRecAntPR.Open;

  //Faturado
  qryRecAntFT.Close;
  qryRecAntFT.Params.ParamByName('DATA_INIP1').Value := sDtIni;
  qryRecAntFT.Params.ParamByName('DATA_FIMP').Value  := sDtFim;
  qryRecAntFT.Params.ParamByName('DATA_INIP2').Value := sDtIni;
  qryRecAntFT.Open;

  { RECEITAS PENDENTES }
  //Cartao
  qryRecPendCT.Close;
  qryRecPendCT.Params.ParamByName('DATA_FIM1').Value  := sDtFim;
  qryRecPendCT.Params.ParamByName('DATA_FIM2').Value  := sDtFim;
  qryRecPendCT.Open;

  //Cheque
  qryRecPendCH.Close;
  qryRecPendCH.Params.ParamByName('DATA_FIM1').Value  := sDtFim;
  qryRecPendCH.Params.ParamByName('DATA_FIM2').Value  := sDtFim;
  qryRecPendCH.Open;

  //Promissoria
  qryRecPendPR.Close;
  qryRecPendPR.Params.ParamByName('DATA_FIM1').Value  := sDtFim;
  qryRecPendPR.Params.ParamByName('DATA_FIM2').Value  := sDtFim;
  qryRecPendPR.Open;

  //Faturado
  qryRecPendFT.Close;
  qryRecPendFT.Params.ParamByName('DATA_FIM1').Value  := sDtFim;
  qryRecPendFT.Params.ParamByName('DATA_FIM2').Value  := sDtFim;
  qryRecPendFT.Open;

  { RELACAO DE FLUTUANTES }
  //Depositos Sem Baixa
  qryDeposSBx.Close;
  qryDeposSBx.Params.ParamByName('DATA_FIM1').Value := sDtFim;
  qryDeposSBx.Params.ParamByName('DATA_FIM2').Value := sDtFim;
  qryDeposSBx.Params.ParamByName('DATA_FIM3').Value := sDtFim;
  qryDeposSBx.Open;

  //Receitas para Repasse
  qryFlutSBx.Close;
  qryFlutSBx.Params.ParamByName('DATA_FIM1').Value := sDtFim;
  qryFlutSBx.Params.ParamByName('DATA_FIM2').Value := sDtFim;
  qryFlutSBx.Open;

  { DESPESAS }
  //Dinheiro
  qryDespDH.Close;
  qryDespDH.Params.ParamByName('DATA_INIP').Value := sDtIni;
  qryDespDH.Params.ParamByName('DATA_FIMP').Value := sDtFim;
  qryDespDH.Open;

  //Descontos
  qryDespDesc.Close;
  qryDespDesc.Params.ParamByName('DATA_INIP').Value := sDtIni;
  qryDespDesc.Params.ParamByName('DATA_FIMP').Value := sDtFim;
  qryDespDesc.Open;

  //Repasses Flutuantes
  qryDespRepFlut.Close;
  qryDespRepFlut.Params.ParamByName('DATA_INIP').Value := sDtIni;
  qryDespRepFlut.Params.ParamByName('DATA_FIMP').Value := sDtFim;
  qryDespRepFlut.Open;
end;

procedure TdmCaixa.cdsFechCXImg_FCalcFields(DataSet: TDataSet);
begin
  cdsFechCXImg_F.FieldByName('CAMINHO_COMPLETO').AsString := vgConf_DiretorioImagens +
                                                             IntToStr(YearOf(cdsFechCX_F.FieldByName('DATA_FECHAMENTO').AsDateTime)) +
                                                             '\' +
                                                             cdsFechCXImg_F.FieldByName('NOME_FIXO').AsString +
                                                             cdsFechCXImg_F.FieldByName('NOME_VARIAVEL').AsString +
                                                             '.jpg';
end;

procedure TdmCaixa.cdsFechCX_FCalcFields(DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with qryAux, SQL do
  begin
    //Item
    if dmCaixa.cdsFechCX_F.FieldByName('CAD_ID_USUARIO').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT NOME ' +
              '  FROM USUARIO ' +
              ' WHERE ID_USUARIO = :ID_USUARIO';
      Params.ParamByName('ID_USUARIO').AsInteger := dmCaixa.cdsFechCX_F.FieldByName('CAD_ID_USUARIO').AsInteger;
      Open;

      dmCaixa.cdsFechCX_F.FieldByName('NOME_USUARIO').AsString := qryAux.FieldByName('NOME').AsString;
    end;
  end;

  FreeAndNil(qryAux);

  { TOTAL RECEITA ORIUNDAS SERVICO ANTERIOR }
  dmCaixa.cdsFechCX_F.FieldByName('VR_SRVANT_TOTAL').AsCurrency := (dmCaixa.cdsFechCX_F.FieldByName('VR_SRVANT_CARTAO').AsCurrency +
                                                                    dmCaixa.cdsFechCX_F.FieldByName('VR_SRVANT_CHEQUE').AsCurrency +
                                                                    dmCaixa.cdsFechCX_F.FieldByName('VR_SRVANT_PROMISSORIA').AsCurrency +
                                                                    dmCaixa.cdsFechCX_F.FieldByName('VR_SRVANT_FATURADO').AsCurrency);

  { TOTAL RELACOES DEPOSITOS E REPASSES }
  dmCaixa.cdsFechCX_F.FieldByName('VR_FLUT_TOTAL').AsCurrency := (dmCaixa.cdsFechCX_F.FieldByName('VR_FLUT_DEPOSITO').AsCurrency +
                                                                  dmCaixa.cdsFechCX_F.FieldByName('VR_FLUT_REPASSE').AsCurrency);

  { TOTAL RECEITA PENDENTE }
  dmCaixa.cdsFechCX_F.FieldByName('VR_RECPEND_TOTAL').AsCurrency := (dmCaixa.cdsFechCX_F.FieldByName('VR_RECPEND_CARTAO').AsCurrency +
                                                                     dmCaixa.cdsFechCX_F.FieldByName('VR_RECPEND_CHEQUE').AsCurrency +
                                                                     dmCaixa.cdsFechCX_F.FieldByName('VR_RECPEND_PROMISSORIA').AsCurrency +
                                                                     dmCaixa.cdsFechCX_F.FieldByName('VR_RECPEND_FATURADO').AsCurrency);

  { SALDO GERAL }
  dmCaixa.cdsFechCX_F.FieldByName('VR_SALDO_GERAL').AsCurrency := (dmCaixa.cdsFechCX_F.FieldByName('VR_TOTAL_FECHAMENTO').AsCurrency +
                                                                   dmCaixa.cdsFechCX_F.FieldByName('VR_RECPEND_TOTAL').AsCurrency);
end;

procedure TdmCaixa.cdsFechCX_FVR_CONTACORRENTEGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_CONTAGEM_FINALGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_DESP_DESCONTOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_DESP_DINHEIROGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_DESP_REPASSEGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_DIFERENCAGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_FECH_ANTERIORGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_FLUT_DEPOSITOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_FLUT_REPASSEGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_IMPORT_CARTAOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_IMPORT_CHEQUEGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_IMPORT_DEPOSITOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_IMPORT_DINHEIROGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_IMPORT_FATURADOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_IMPORT_PROMISSORIAGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_IMPORT_REPASSESGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_MOEDASGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_NOTASGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_OUTRASREC_CARTAOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_OUTRASREC_CHEQUEGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_OUTRASREC_DEPOSITOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_OUTRASREC_DINHEIROGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_OUTRASREC_FATURADOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_OUTRASREC_PROMISSORIAGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_OUTRASREC_REPASSESGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_OUTRASREC_TOTALGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_RECIBO_IMPORTADOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_RECPEND_CARTAOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_RECPEND_CHEQUEGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_RECPEND_FATURADOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_RECPEND_PROMISSORIAGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_RECPEND_TOTALGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_SALDO_DIAGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_SALDO_GERALGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_SRVANT_CARTAOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_SRVANT_CHEQUEGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_SRVANT_FATURADOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_SRVANT_PROMISSORIAGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_TOTAL_DESPESAGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_TOTAL_FECHAMENTOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_TOTAL_RECEITAGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmCaixa.cdsFechCX_FVR_TOTAL_ULT_ABGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

end.
