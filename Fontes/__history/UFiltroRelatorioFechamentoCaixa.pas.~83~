{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroRelatorioFechamentoCaixa.pas
  Descricao:   Filtro Relatorios de Fechamento de Caixa por Forma de Pagamento (R02 - R02DET)
  Author   :   Cristina
  Date:        13-set-2016
  Last Update: 23-dez-2016  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroRelatorioFechamentoCaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroRelatorioPadrao, Vcl.StdCtrls,
  JvExControls, JvButton, JvTransparentButton, Vcl.ExtCtrls, Vcl.Mask, JvExMask,
  JvToolEdit, System.DateUtils;

type
  TFFiltroRelatorioFechamentoCaixa = class(TFFiltroRelatorioPadrao)
    lblPeriodo: TLabel;
    lblPeriodoInicio: TLabel;
    dteDataInicio: TJvDateEdit;
    lblPeriodoFim: TLabel;
    dteDataFim: TJvDateEdit;
    gbExibirRelatorio: TGroupBox;
    chbPrevRealPend: TCheckBox;
    chbFirmas: TCheckBox;
    chbPagtoDinheiro: TCheckBox;
    chbPagtoCartao: TCheckBox;
    chbPagtoCheque: TCheckBox;
    chbPagtoDeposito: TCheckBox;
    chbPagtoFaturado: TCheckBox;
    chbPagtoPromissoria: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
    procedure chbTipoAnaliticoClick(Sender: TObject);
    procedure chbTipoAnaliticoExit(Sender: TObject);
  protected
    procedure GravarLog; override;
    procedure GerarRelatorio(ImpDet: Boolean); override;
    function VerificarFiltro: Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroRelatorioFechamentoCaixa: TFFiltroRelatorioFechamentoCaixa;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMRelatorios;

procedure TFFiltroRelatorioFechamentoCaixa.btnLimparClick(Sender: TObject);
begin
  inherited;

  dteDataInicio.Date := Date;
  dteDataFim.Date    := Date;

  chbTipoSintetico.Checked := True;
  chbTipoAnalitico.Checked := False;

  chbFirmas.Checked           := False;
  chbPagtoCheque.Checked      := False;
  chbPagtoFaturado.Checked    := False;
  chbPagtoPromissoria.Checked := False;
  chbPagtoCartao.Checked      := False;
  chbPagtoDinheiro.Checked    := False;
  chbPagtoDeposito.Checked    := False;

  chbFirmas.Enabled           := False;
  chbPagtoCheque.Enabled      := False;
  chbPagtoFaturado.Enabled    := False;
  chbPagtoPromissoria.Enabled := False;
  chbPagtoCartao.Enabled      := False;
  chbPagtoDinheiro.Enabled    := False;
  chbPagtoDeposito.Enabled    := False;

  chbPrevRealPend.Checked := True;
  chbPrevRealPend.Enabled := False;

  chbExportarPDF.Checked   := True;
  chbExportarExcel.Checked := True;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioFechamentoCaixa.btnVisualizarClick(Sender: TObject);
begin
  inherited;

  if not VerificarFiltro then
    Exit;

  //Previsto x Pendente
  dmRelatorios.qryRel02_PrevistoPendente.Close;
  dmRelatorios.qryRel02_PrevistoPendente.Params.ParamByName('DATA_INI01').Value := (FormatDateTime('YYYY-MM-DD', dteDataInicio.Date) + ' 00:00:00');
  dmRelatorios.qryRel02_PrevistoPendente.Params.ParamByName('DATA_FIM01').Value := (FormatDateTime('YYYY-MM-DD', dteDataFim.Date) + ' 23:59:59');
  dmRelatorios.qryRel02_PrevistoPendente.Params.ParamByName('DATA_INI03').Value := (FormatDateTime('YYYY-MM-DD', dteDataInicio.Date) + ' 00:00:00');
  dmRelatorios.qryRel02_PrevistoPendente.Params.ParamByName('DATA_FIM03').Value := (FormatDateTime('YYYY-MM-DD', dteDataFim.Date) + ' 23:59:59');
  dmRelatorios.qryRel02_PrevistoPendente.Open;

  //Realizado
  dmRelatorios.qryRel02_Realizado.Close;
  dmRelatorios.qryRel02_Realizado.Params.ParamByName('DATA_INI02').Value := (FormatDateTime('YYYY-MM-DD', dteDataInicio.Date) + ' 00:00:00');
  dmRelatorios.qryRel02_Realizado.Params.ParamByName('DATA_FIM02').Value := (FormatDateTime('YYYY-MM-DD', dteDataFim.Date) + ' 23:59:59');
  dmRelatorios.qryRel02_Realizado.Open;

  if dmRelatorios.qryRel02_Realizado.RecordCount = 0 then
  begin
    Application.MessageBox('N�o h� Lan�amentos registrados no per�odo informado.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
  end
  else
  begin
    if chbTipoSintetico.Checked then
      GerarRelatorio(False);

    if chbTipoAnalitico.Checked then
      GerarRelatorio(True);
  end;
end;

procedure TFFiltroRelatorioFechamentoCaixa.chbTipoAnaliticoClick(
  Sender: TObject);
begin
  inherited;

  chbFirmas.Checked           := chbTipoAnalitico.Checked;
  chbPagtoCheque.Checked      := chbTipoAnalitico.Checked;
  chbPagtoFaturado.Checked    := chbTipoAnalitico.Checked;
  chbPagtoPromissoria.Checked := chbTipoAnalitico.Checked;
  chbPagtoCartao.Checked      := chbTipoAnalitico.Checked;
  chbPagtoDinheiro.Checked    := chbTipoAnalitico.Checked;
  chbPagtoDeposito.Checked    := chbTipoAnalitico.Checked;

  chbFirmas.Enabled           := chbTipoAnalitico.Checked;
  chbPagtoCheque.Enabled      := chbTipoAnalitico.Checked;
  chbPagtoFaturado.Enabled    := chbTipoAnalitico.Checked;
  chbPagtoPromissoria.Enabled := chbTipoAnalitico.Checked;
  chbPagtoCartao.Enabled      := chbTipoAnalitico.Checked;
  chbPagtoDinheiro.Enabled    := chbTipoAnalitico.Checked;
  chbPagtoDeposito.Enabled    := chbTipoAnalitico.Checked;
end;

procedure TFFiltroRelatorioFechamentoCaixa.chbTipoAnaliticoExit(
  Sender: TObject);
begin
  inherited;

  chbFirmas.Checked           := chbTipoAnalitico.Checked;
  chbPagtoCheque.Checked      := chbTipoAnalitico.Checked;
  chbPagtoFaturado.Checked    := chbTipoAnalitico.Checked;
  chbPagtoPromissoria.Checked := chbTipoAnalitico.Checked;
  chbPagtoCartao.Checked      := chbTipoAnalitico.Checked;
  chbPagtoDinheiro.Checked    := chbTipoAnalitico.Checked;
  chbPagtoDeposito.Checked    := chbTipoAnalitico.Checked;

  chbFirmas.Enabled           := chbTipoAnalitico.Checked;
  chbPagtoCheque.Enabled      := chbTipoAnalitico.Checked;
  chbPagtoFaturado.Enabled    := chbTipoAnalitico.Checked;
  chbPagtoPromissoria.Enabled := chbTipoAnalitico.Checked;
  chbPagtoCartao.Enabled      := chbTipoAnalitico.Checked;
  chbPagtoDinheiro.Enabled    := chbTipoAnalitico.Checked;
  chbPagtoDeposito.Enabled    := chbTipoAnalitico.Checked;
end;

procedure TFFiltroRelatorioFechamentoCaixa.FormShow(Sender: TObject);
begin
  inherited;

  dteDataInicio.Date := Date;
  dteDataFim.Date    := Date;

  chbTipoSintetico.Checked := True;

  chbFirmas.Enabled           := False;
  chbPagtoCheque.Enabled      := False;
  chbPagtoFaturado.Enabled    := False;
  chbPagtoPromissoria.Enabled := False;
  chbPagtoCartao.Enabled      := False;
  chbPagtoDinheiro.Enabled    := False;
  chbPagtoDeposito.Enabled    := False;

  chbPrevRealPend.Checked := True;
  chbPrevRealPend.Enabled := False;

  chbExportarPDF.Checked   := True;
  chbExportarExcel.Checked := True;

  chbExibirGrafico.Visible := False;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioFechamentoCaixa.GerarRelatorio(ImpDet: Boolean);
begin
  inherited;

  try
    sTituloRelatorio    := 'FECHAMENTO DE CAIXA (por Forma de Pagamento)';
    sSubtituloRelatorio := 'Per�odo de ' + DateToStr(dteDataInicio.Date) + ' a ' + DateToStr(dteDataFim.Date);

    lExibirFirmas           := False;
    lExibirPagtoCartao      := False;
    lExibirPagtoCheque      := False;
    lExibirPagtoDinheiro    := False;
    lExibirPagtoFaturado    := False;
    lExibirPagtoDeposito    := False;
    lExibirPagtoPromissoria := False;

    if ImpDet then
    begin
      dmRelatorios.DefinirRelatorio(R02DET);

      //Firmas
      if chbFirmas.Checked then
      begin
        dmRelatorios.qryRel02_Firmas.Close;
        dmRelatorios.qryRel02_Firmas.Params.ParamByName('DATA_INI').Value := (FormatDateTime('YYYY-MM-DD', dteDataInicio.Date) + ' 00:00:00');
        dmRelatorios.qryRel02_Firmas.Params.ParamByName('DATA_FIM').Value := (FormatDateTime('YYYY-MM-DD', dteDataFim.Date) + ' 23:59:59');
        dmRelatorios.qryRel02_Firmas.Open;

        lExibirFirmas := True;
      end
      else
      begin
        dmRelatorios.qryRel02_Firmas.Close;
        dmRelatorios.qryRel02_Firmas.Params.ParamByName('DATA_INI').Value := Null;
        dmRelatorios.qryRel02_Firmas.Params.ParamByName('DATA_FIM').Value := Null;
        dmRelatorios.qryRel02_Firmas.Open;
      end;

      //Pagamento em Cheque
      if chbPagtoCheque.Checked then
      begin
        dmRelatorios.qryRel02_Cheque.Close;
        dmRelatorios.qryRel02_Cheque.Params.ParamByName('DATA_INI').Value := (FormatDateTime('YYYY-MM-DD', dteDataInicio.Date) + ' 00:00:00');
        dmRelatorios.qryRel02_Cheque.Params.ParamByName('DATA_FIM').Value := (FormatDateTime('YYYY-MM-DD', dteDataFim.Date) + ' 23:59:59');
        dmRelatorios.qryRel02_Cheque.Open;

        lExibirPagtoCheque := True;
      end
      else
      begin
        dmRelatorios.qryRel02_Cheque.Close;
        dmRelatorios.qryRel02_Cheque.Params.ParamByName('DATA_INI').Value := 0;
        dmRelatorios.qryRel02_Cheque.Params.ParamByName('DATA_FIM').Value := 0;
        dmRelatorios.qryRel02_Cheque.Open;
      end;

      //Pagamento por Deposito
      if chbPagtoDeposito.Checked then
      begin
        dmRelatorios.qryRel02_Deposito.Close;
        dmRelatorios.qryRel02_Deposito.Params.ParamByName('DATA_INI').Value := (FormatDateTime('YYYY-MM-DD', dteDataInicio.Date) + ' 00:00:00');
        dmRelatorios.qryRel02_Deposito.Params.ParamByName('DATA_FIM').Value := (FormatDateTime('YYYY-MM-DD', dteDataFim.Date) + ' 23:59:59');
        dmRelatorios.qryRel02_Deposito.Open;

        lExibirPagtoDeposito := True;
      end
      else
      begin
        dmRelatorios.qryRel02_Deposito.Close;
        dmRelatorios.qryRel02_Deposito.Params.ParamByName('DATA_INI').Value := 0;
        dmRelatorios.qryRel02_Deposito.Params.ParamByName('DATA_FIM').Value := 0;
        dmRelatorios.qryRel02_Deposito.Open;
      end;

      //Pagamento Faturado
      if chbPagtoFaturado.Checked then
      begin
        dmRelatorios.qryRel02_Faturado.Close;
        dmRelatorios.qryRel02_Faturado.Params.ParamByName('DATA_INI').Value := (FormatDateTime('YYYY-MM-DD', dteDataInicio.Date) + ' 00:00:00');
        dmRelatorios.qryRel02_Faturado.Params.ParamByName('DATA_FIM').Value := (FormatDateTime('YYYY-MM-DD', dteDataFim.Date) + ' 23:59:59');
        dmRelatorios.qryRel02_Faturado.Open;

        lExibirPagtoFaturado := True;
      end
      else
      begin
        dmRelatorios.qryRel02_Faturado.Close;
        dmRelatorios.qryRel02_Faturado.Params.ParamByName('DATA_INI').Value := 0;
        dmRelatorios.qryRel02_Faturado.Params.ParamByName('DATA_FIM').Value := 0;
        dmRelatorios.qryRel02_Faturado.Open;
      end;

      //Pagamento em Cartao
      if chbPagtoCartao.Checked then
      begin
        dmRelatorios.qryRel02_Cartao.Close;
        dmRelatorios.qryRel02_Cartao.Params.ParamByName('DATA_INI').Value := (FormatDateTime('YYYY-MM-DD', dteDataInicio.Date) + ' 00:00:00');
        dmRelatorios.qryRel02_Cartao.Params.ParamByName('DATA_FIM').Value := (FormatDateTime('YYYY-MM-DD', dteDataFim.Date) + ' 23:59:59');
        dmRelatorios.qryRel02_Cartao.Open;

        lExibirPagtoCartao := True;
      end
      else
      begin
        dmRelatorios.qryRel02_Cartao.Close;
        dmRelatorios.qryRel02_Cartao.Params.ParamByName('DATA_INI').Value := 0;
        dmRelatorios.qryRel02_Cartao.Params.ParamByName('DATA_FIM').Value := 0;
        dmRelatorios.qryRel02_Cartao.Open;
      end;

      //Pagamento em Dinheiro
      if chbPagtoDinheiro.Checked then
      begin
        dmRelatorios.qryRel02_Dinheiro.Close;
        dmRelatorios.qryRel02_Dinheiro.Params.ParamByName('DATA_INI').Value := (FormatDateTime('YYYY-MM-DD', dteDataInicio.Date) + ' 00:00:00');
        dmRelatorios.qryRel02_Dinheiro.Params.ParamByName('DATA_FIM').Value := (FormatDateTime('YYYY-MM-DD', dteDataFim.Date) + ' 23:59:59');
        dmRelatorios.qryRel02_Dinheiro.Open;

        lExibirPagtoDinheiro := True;
      end
      else
      begin
        dmRelatorios.qryRel02_Dinheiro.Close;
        dmRelatorios.qryRel02_Dinheiro.Params.ParamByName('DATA_INI').Value := 0;
        dmRelatorios.qryRel02_Dinheiro.Params.ParamByName('DATA_FIM').Value := 0;
        dmRelatorios.qryRel02_Dinheiro.Open;
      end;

      //Pagamento em Promissoria
      if chbPagtoPromissoria.Checked then
      begin
        dmRelatorios.qryRel02_Promissoria.Close;
        dmRelatorios.qryRel02_Promissoria.Params.ParamByName('DATA_INI').Value := (FormatDateTime('YYYY-MM-DD', dteDataInicio.Date) + ' 00:00:00');
        dmRelatorios.qryRel02_Promissoria.Params.ParamByName('DATA_FIM').Value := (FormatDateTime('YYYY-MM-DD', dteDataFim.Date) + ' 23:59:59');
        dmRelatorios.qryRel02_Promissoria.Open;

        lExibirPagtoPromissoria := True;
      end
      else
      begin
        dmRelatorios.qryRel02_Promissoria.Close;
        dmRelatorios.qryRel02_Promissoria.Params.ParamByName('DATA_INI').Value := 0;
        dmRelatorios.qryRel02_Promissoria.Params.ParamByName('DATA_FIM').Value := 0;
        dmRelatorios.qryRel02_Promissoria.Open;
      end;
    end
    else
      dmRelatorios.DefinirRelatorio(R02);

    dmRelatorios.ImprimirRelatorio(chbExportarPDF.Checked);

    GravarLog;
  except
    Application.MessageBox('Erro ao gerar Fechamento de Caixa.',
                           'Erro',
                           MB_OK + MB_ICONERROR);
  end;
end;

procedure TFFiltroRelatorioFechamentoCaixa.GravarLog;
begin
  inherited;

  BS.GravarUsuarioLog(50,
                      '',
                      'Impress�o de Relatorio de Fechamento de Caixa (' + DateToStr(dDataRelatorio) +
                                                                      ' ' +
                                                                      TimeToStr(tHoraRelatorio) + ')',
                      0,
                      '');
end;

function TFFiltroRelatorioFechamentoCaixa.VerificarFiltro: Boolean;
var
  Msg: String;
begin
  Result := True;

  if dteDataInicio.Date = 0 then
  begin
    Msg := '- Informe a DATA DE IN�CIO do Per�odo.';
    Result := False;
  end;

  if dteDataFim.Date = 0 then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe a DATA DE FIM do Per�odo.'
    else
      Msg := #13#10 + '- Informe a DATA DE FIM do Per�odo.';

    Result := False;
  end;

  if dteDataFim.Date < dteDataInicio.Date then
  begin
    if Trim(Msg) = '' then
      Msg := '- A DATA DE FIM do Per�odo n�o pode inferior � DATA DE IN�CIO do mesmo.'
    else
      Msg := #13#10 + '- A DATA DE FIM do Per�odo n�o pode ser inferior � DATA DE IN�CIO do mesmo.';

    Result := False;
  end;

  if (not chbTipoAnalitico.Checked) and
    (not chbTipoSintetico.Checked) then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe o TIPO do Extrato de Contas.'
    else
      Msg := #13#10 + '- Informe o TIPO do Relat�rio de Fechamento de Caixa.';

    Result := False;
  end;

  if chbTipoAnalitico.Checked then
  begin
    if (not chbFirmas.Checked) and
      (not chbPagtoCartao.Checked) and
      (not chbPagtoCheque.Checked) and
      (not chbPagtoDinheiro.Checked) and
      (not chbPagtoFaturado.Checked) and
      (not chbPagtoDeposito.Checked) and
      (not chbPagtoPromissoria.Checked) then
    begin
      if Trim(Msg) = '' then
        Msg := '- Informe o(s) detalhamento(s) a serem apresentados no Relat�rio.'
      else
        Msg := #13#10 + '- Informe o(s) detalhamento(s) a serem apresentados no Relat�rio.';

      Result := False;
    end;
  end;

  if not Result then
    Application.MessageBox(PChar(':: ATEN��O ::' + #13#10 + #13#10 + Msg),
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
end;

end.
