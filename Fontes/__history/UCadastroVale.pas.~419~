{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroVale.pas
  Descricao:   Tela de cadastro de Vale de Funcionario
  Author   :   Cristina
  Date:        27-mai-2016
  Last Update: 30-nov-2017 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroVale;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, Vcl.Graphics,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.DBClient, Datasnap.Provider,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, JvBaseEdits, JvDBControls,
  Vcl.Mask, JvExMask, JvToolEdit, Vcl.DBCtrls, System.DateUtils;

type
  TFCadastroVale = class(TFCadastroGeralPadrao)
    lblNomeFuncionario: TLabel;
    lblDescricao: TLabel;
    lblDataVale: TLabel;
    lblValor: TLabel;
    lblDataQuitacao: TLabel;
    lcbNomeFuncionario: TDBLookupComboBox;
    mmDescricao: TDBMemo;
    dteDataVale: TJvDBDateEdit;
    dteDataQuitacao: TJvDBDateEdit;
    cedValor: TJvDBCalcEdit;
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure dteDataQuitacaoKeyPress(Sender: TObject; var Key: Char);
    procedure dteDataValeExit(Sender: TObject);
    procedure dteDataValeKeyPress(Sender: TObject; var Key: Char);
    procedure dteDataQuitacaoExit(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }

    cVlrMaxVales,
    cVlrGastoVales: Currency;

    function CalcularValorVales: Boolean;
  public
    { Public declarations }
  end;

var
  FCadastroVale: TFCadastroVale;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMColaborador, UDMVale;

{ TFCadastroVale }

procedure TFCadastroVale.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroVale.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

function TFCadastroVale.CalcularValorVales: Boolean;
var
  QryMaxVale, QryVlrVale: TFDQuery;
  cValorTotalGasto: Currency;
begin
  Result := True;

  cVlrMaxVales     := 0;
  cVlrGastoVales   := 0;
  cValorTotalGasto := 0;

  //Valor Maximo de Vales
  QryMaxVale := dmGerencial.CriarFDQuery(nil, vgConGER);

  with QryMaxVale, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT VR_MAXIMO_VALE ' +
            '  FROM FUNCIONARIO ' +
            ' WHERE ID_FUNCIONARIO = :ID_FUNCIONARIO ';
    Params.ParamByName('ID_FUNCIONARIO').Value := lcbNomeFuncionario.KeyValue;
    Open;

    cVlrMaxVales := QryMaxVale.FieldByName('VR_MAXIMO_VALE').AsCurrency;
  end;

  //Valor ja gasto com Vales pelo Funcionario
  QryVlrVale := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryVlrVale, SQL do
  begin
    Close;
    Clear;

    Text := 'SELECT SUM(VR_VALE) AS VR_VALE ' +
            '  FROM VALE ' +
            ' WHERE ID_FUNCIONARIO_FK = :ID_FUNCIONARIO ' +
            '   AND ID_FECHAMENTO_SALARIO_FK IS NULL ' +
            '   AND FLG_CANCELADO = ' + QuotedStr('N');

    if vgOperacao = E then
      Add('   AND ID_VALE <> ' + IntToStr(vgIdConsulta));

    Add('GROUP BY ID_FUNCIONARIO_FK');

    Params.ParamByName('ID_FUNCIONARIO').Value := lcbNomeFuncionario.KeyValue;

    Open;

    cVlrGastoVales   := QryVlrVale.FieldByName('VR_VALE').AsCurrency;
    cValorTotalGasto := (QryVlrVale.FieldByName('VR_VALE').AsCurrency + cedValor.Value);
  end;

  Result := (cVlrMaxVales > 0) and (cVlrMaxVales >= cValorTotalGasto);

  FreeAndNil(QryVlrVale);
  FreeAndNil(QryMaxVale);
end;

procedure TFCadastroVale.DefinirTamanhoMaxCampos;
begin
  inherited;

  mmDescricao.MaxLength := 1000;
end;

procedure TFCadastroVale.DesabilitarComponentes;
begin
  inherited;

  if vgOperacao = E then
  begin
    lcbNomeFuncionario.Enabled := False;
    cedValor.Enabled           := dmVale.cdsVale.FieldByName('DATA_QUITACAO').IsNull;
  end;

  if vgOperacao = C then
  begin
    dteDataVale.Enabled        := False;
    lcbNomeFuncionario.Enabled := False;
    mmDescricao.Enabled        := False;
    cedValor.Enabled           := False;
    dteDataQuitacao.Enabled    := False;
  end;
end;

procedure TFCadastroVale.dteDataQuitacaoExit(Sender: TObject);
begin
  inherited;

  if dmVale.cdsVale.State in [dsInsert, dsEdit] then
  begin
    if dteDataQuitacao.Date < vgDataLancVigente then
      dmVale.cdsVale.FieldByName('DATA_QUITACAO').AsDateTime := vgDataLancVigente
    else
      dmVale.cdsVale.FieldByName('DATA_QUITACAO').AsDateTime := dteDataQuitacao.Date;
  end;
end;

procedure TFCadastroVale.dteDataQuitacaoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    if Trim(lcbNomeFuncionario.Text) = '' then
    begin
      Application.MessageBox('Por favor, informe o Nome do Colaborador.', 'Aviso', MB_OK + MB_ICONWARNING);

      if lcbNomeFuncionario.CanFocus then
        lcbNomeFuncionario.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if Trim(lcbNomeFuncionario.Text) = '' then
    begin
      Application.MessageBox('Por favor, informe o Nome do Colaborador.', 'Aviso', MB_OK + MB_ICONWARNING);

      if lcbNomeFuncionario.CanFocus then
        lcbNomeFuncionario.SetFocus;
    end
    else if dmVale.cdsVale.State in [dsInsert, dsEdit] then
    begin
      if dteDataQuitacao.Date < vgDataLancVigente then
        dmVale.cdsVale.FieldByName('DATA_QUITACAO').AsDateTime := vgDataLancVigente
      else
        dmVale.cdsVale.FieldByName('DATA_QUITACAO').AsDateTime := dteDataQuitacao.Date;

      btnOk.Click;
    end;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    btnCancelar.Click;
end;

procedure TFCadastroVale.dteDataValeExit(Sender: TObject);
begin
  inherited;

  if dmVale.cdsVale.State in [dsInsert, dsEdit] then
  begin
    if dteDataVale.Date < vgDataLancVigente then
      dmVale.cdsVale.FieldByName('DATA_VALE').AsDateTime := vgDataLancVigente
    else
      dmVale.cdsVale.FieldByName('DATA_VALE').AsDateTime := dteDataVale.Date;
  end;
end;

procedure TFCadastroVale.dteDataValeKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dmVale.cdsVale.State in [dsInsert, dsEdit] then
    begin
      if dteDataVale.Date < vgDataLancVigente then
        dmVale.cdsVale.FieldByName('DATA_VALE').AsDateTime := vgDataLancVigente
      else
        dmVale.cdsVale.FieldByName('DATA_VALE').AsDateTime := dteDataVale.Date;
    end;

    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    btnCancelar.Click;
end;

procedure TFCadastroVale.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;

  if lPodeCancelar then
  begin
    dmVale.cdsVale.Cancel;

    FreeAndNil(dmVale);

    vgOperacao     := VAZIO;
    vgIdConsulta   := 0;
    vgOrigemFiltro := False;
  end
  else
    Action := caNone;
end;

procedure TFCadastroVale.FormCreate(Sender: TObject);
begin
  inherited;

  dmVale.qryFuncionarios.Close;
  dmVale.qryFuncionarios.Open;

  dmVale.cdsVale.Close;
  dmVale.cdsVale.Params.ParamByName('ID_VALE').Value := vgIdConsulta;
  dmVale.cdsVale.Open;

  if vgOperacao = I then
  begin
    dmVale.cdsVale.Append;

    if Date < vgDataLancVigente then
      dmVale.cdsVale.FieldByName('DATA_VALE').AsDateTime := vgDataLancVigente
    else
      dmVale.cdsVale.FieldByName('DATA_VALE').AsDateTime := Date;

    dmVale.cdsVale.FieldByName('DATA_CADASTRO').AsDateTime := Now;
    dmVale.cdsVale.FieldByName('CAD_ID_USUARIO').Value     := vgUsu_Id;
    dmVale.cdsVale.FieldByName('DATA_QUITACAO').AsDateTime := Date;
    dmVale.cdsVale.FieldByName('FLG_CANCELADO').AsString   := 'N';
  end;

  if vgOperacao = E then
  begin
    dmVale.cdsVale.Edit;
    dmVale.cdsVale.FieldByName('VR_VALE_ANTERIOR').AsCurrency := dmVale.cdsVale.FieldByName('VR_VALE').AsCurrency;
  end;
end;

procedure TFCadastroVale.FormShow(Sender: TObject);
begin
  inherited;

  if lcbNomeFuncionario.CanFocus then
    lcbNomeFuncionario.SetFocus;
end;

function TFCadastroVale.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroVale.GravarDadosGenerico(var Msg: String): Boolean;
var
  IdForn, IdNat, IdCat, IdSubCat, Cod: Integer;
  Doc, Obs, NFantasia, RSocial, Descr: String;
  IdVale, CodLanc, AnoLanc, IdParc, IdItem: Integer;
  sMsg, sIR, sCP, sLA: String;
  QryAuxS: TFDQuery;
  cValorDisp: Currency;
  Op: TOperacao;
  lContinuar: Boolean;
begin
  Result := True;
  Msg    := '';
  sIR    := '';
  sCP    := '';
  sLA    := '';
  Op     := vgOperacao;

  cValorDisp := 0;

  if vgOperacao = C then
    Exit;

  if not CalcularValorVales then
  begin
    if cVlrMaxVales = 0 then
      sMsg := 'O VALOR M�XIMO PARA VALE cadastrado para esse Colaborador est� zerado.'
    else
    begin
      cValorDisp := (cVlrMaxVales - cVlrGastoVales);

      sMsg := 'VALOR M�XIMO PARA VALE: ' + CurrToStr(cVlrMaxVales) + #13#10 +
              'VALOR UTILIZADO (sem esse Vale): ' + CurrToStr(cVlrGastoVales) + #13#10 +
              'VALOR DISPON�VEL PARA VALE: ' + CurrToStr(cValorDisp) + #13#10 +
              'Por favor, informe um valor menor.';
    end;

    Application.MessageBox(PChar(sMsg), 'Aviso', MB_OK + MB_ICONWARNING);

    cedValor.Clear;

    Abort;
  end;

  IdVale   := 0;
  CodLanc  := 0;
  AnoLanc  := 0;
  IdParc   := 0;
  IdForn   := 0;
  IdItem   := 0;
  IdNat    := 0;
  IdCat    := 0;
  IdSubCat := 0;
  Cod      := 0;

  Doc       := '';
  Obs       := '';
  NFantasia := '';
  RSocial   := '';
  Descr     := '';

  lContinuar:= True;

  QryAuxS := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    { VALE }
    if vgOperacao = I then
      IdVale := BS.ProximoId('ID_VALE', 'VALE')
    else
      IdVale := dmVale.cdsVale.FieldByName('ID_VALE').AsInteger;

    dmVale.cdsVale.FieldByName('ID_VALE').Value := IdVale;

    if not dmVale.cdsVale.FieldByName('DATA_QUITACAO').IsNull then
      dmVale.cdsVale.FieldByName('QUIT_ID_USUARIO').Value := vgUsu_Id;

    dmVale.cdsVale.Post;
    dmVale.cdsVale.ApplyUpdates(0);

    dmVale.cdsVale.Close;
    dmVale.cdsVale.Params.ParamByName('ID_VALE').Value := IdVale;
    dmVale.cdsVale.Open;

    if dmVale.cdsVale.FieldByName('VR_VALE_ANTERIOR').AsCurrency <> dmVale.cdsVale.FieldByName('VR_VALE').AsCurrency then
      dmPrincipal.RecalcularFechamentoSalarios(dmVale.cdsVale.FieldByName('ID_FECHAMENTO_SALARIO_FK').AsInteger,
                                               dmVale.cdsVale.FieldByName('ID_FUNCIONARIO_FK').AsInteger,
                                               True, False, False);
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    lContinuar := False;
  end;

  if lContinuar then
  begin
    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      { LANCAMENTO DE DESPESA }
      //Fornecedor
      Doc       := dmVale.qryFuncionarios.FieldByName('CPF').AsString;
      Obs       := 'Cadastro realizado automaticamente em inclus�o de VALE.';
      NFantasia := dmVale.qryFuncionarios.FieldByName('NOME_FUNCIONARIO').AsString;
      RSocial   := dmVale.qryFuncionarios.FieldByName('NOME_FUNCIONARIO').AsString;

      dmPrincipal.RetornarClienteFornecedor('F',
                                            IdForn,
                                            Doc,
                                            Obs,
                                            NFantasia,
                                            RSocial);

      //Categoria
      IdNat := 3;
      IdCat := 0;
      Cod   := 1;  //Despesa
      Descr := 'COLABORADORES';
      Obs   := 'Inclus�o AUTOM�TICA de Categoria de Despesa';

      dmPrincipal.RetornaCategoria(IdCat, IdNat, Cod, Descr, Obs);

      //SubCategoria
      IdSubCat := 0;
      Cod      := 1;  //Despesa
      Descr    := NFantasia;
      Obs      := 'Inclus�o AUTOM�TICA de Subcategoria de Despesa';

      dmPrincipal.RetornaSubCategoria(IdSubCat, IdCat, Cod, Descr, Obs);

      dmPrincipal.InicializarComponenteLancamento;

      //Lancamento
      DadosLancamento := Lancamento.Create;

      if vgOperacao = I then
      begin
        dmPrincipal.RetornarClassificacaoNatureza(3, sIR, sCP, sLA);

        DadosLancamento.DataLancamento   := dteDataVale.Date;
        DadosLancamento.TipoLancamento   := 'D';
        DadosLancamento.TipoCadastro     := 'A';
        DadosLancamento.IdCategoria      := IdCat;
        DadosLancamento.IdSubCategoria   := IdSubCat;
        DadosLancamento.IdCliFor         := IdForn;
        DadosLancamento.FlgIR            := sIR;
        DadosLancamento.FlgPessoal       := sCP;
        DadosLancamento.FlgFlutuante     := 'N';
        DadosLancamento.FlgAuxiliar      := sLA;
        DadosLancamento.FlgForaFechCaixa := 'N';
        DadosLancamento.IdNatureza       := 3;  //VALE
        DadosLancamento.QtdParcelas      := 1;
        DadosLancamento.CadIdUsuario     := vgUsu_Id;
        DadosLancamento.DataCadastro     := Now;
        DadosLancamento.FlgRecorrente    := 'N';
        DadosLancamento.FlgReplicado     := 'N';
        DadosLancamento.IdOrigem         := IdVale;
      end
      else
      if vgOperacao = E then
      begin
        QryAuxS.Close;
        QryAuxS.SQL.Clear;
        QryAuxS.SQL.Text := 'SELECT COD_LANCAMENTO, ANO_LANCAMENTO ' +
                            '  FROM LANCAMENTO ' +
                            ' WHERE ID_ORIGEM      = :ID_ORIGEM ' +
                            '   AND ID_NATUREZA_FK = :ID_NATUREZA';
        QryAuxS.Params.ParamByName('ID_ORIGEM').Value   := IdVale;
        QryAuxS.Params.ParamByName('ID_NATUREZA').Value := 3;
        QryAuxS.Open;

        DadosLancamento.CodLancamento := QryAuxS.FieldByName('COD_LANCAMENTO').AsInteger;
        DadosLancamento.AnoLancamento := QryAuxS.FieldByName('ANO_LANCAMENTO').AsInteger;
      end;

      if not dmVale.cdsVale.FieldByName('DATA_QUITACAO').IsNull then
      begin
        DadosLancamento.VlrTotalPago := cedValor.Value;
        DadosLancamento.FlgStatus    := 'G';
      end;

      DadosLancamento.VlrTotalPrev := cedValor.Value;
      DadosLancamento.Observacao   := 'Despesa originada pelo VALE ' + IntToStr(IdVale) + '.' + #13#10 +
                                      Trim(mmDescricao.Text);

      ListaLancamentos.Add(DadosLancamento);

      if vgOperacao = I then
        dmPrincipal.InsertLancamento(0)
      else if vgOperacao = E then
        dmPrincipal.UpdateLancamento(0);

      { PARCELAS }
      DadosLancamentoParc := LancamentoParc.Create;

      if vgOperacao = I then
      begin
        DadosLancamentoParc.NumParcela     := 1;
        DadosLancamentoParc.IdFormaPagto   := 6;  //Dinheiro
        DadosLancamentoParc.DataLancParc   := dteDataVale.Date;
        DadosLancamentoParc.DataVencimento := dteDataVale.Date;
        DadosLancamentoParc.CadIdUsuario   := vgUsu_Id;
        DadosLancamentoParc.DataCadastro   := Now;
      end;

      DadosLancamentoParc.VlrPrevisto := cedValor.Value;

      if not dmVale.cdsVale.FieldByName('DATA_QUITACAO').IsNull then
      begin
        DadosLancamentoParc.VlrPago        := cedValor.Value;
        DadosLancamentoParc.DataPagamento  := dmVale.cdsVale.FieldByName('DATA_QUITACAO').AsDateTime;
        DadosLancamentoParc.FlgStatus      := 'G';
        DadosLancamentoParc.PagIdUsuario   := vgUsu_Id;
      end;

      ListaLancamentoParcs.Add(DadosLancamentoParc);

      if vgOperacao = I then
        dmPrincipal.InsertLancamentoParc(0, 0)
      else if vgOperacao = E then
        dmPrincipal.UpdateLancamentoParc(0, 0);

      { LOG }
      if vgOperacao = I then
      begin
        with QryAuxS do
        begin
          QryAuxS.Close;
          QryAuxS.SQL.Clear;
          QryAuxS.SQL.Text := 'INSERT INTO USUARIO_LOG (ID_USUARIO_LOG, ID_USUARIO, ID_MODULO_FK, ' +
                              '                         TIPO_LANCAMENTO, CODIGO_LAN, ANO_LAN, ID_ORIGEM, ' +
                              '                         TABELA_ORIGEM, TIPO_ACAO,  OBS_USUARIO_LOG) ' +
                              '                 VALUES (:ID_USUARIO_LOG, :ID_USUARIO, :ID_MODULO, ' +
                              '                         :TIPO_LANCAMENTO, :CODIGO_LAN, :ANO_LAN, :ID_ORIGEM, ' +
                              '                         :TABELA_ORIGEM, :TIPO_ACAO,  :OBS_USUARIO_LOG)';

          QryAuxS.Params.ParamByName('ID_USUARIO_LOG').Value  := BS.ProximoId('ID_USUARIO_LOG', 'USUARIO_LOG');
          QryAuxS.Params.ParamByName('ID_USUARIO').Value      := vgUsu_Id;
          QryAuxS.Params.ParamByName('ID_MODULO').Value       := 80;
          QryAuxS.Params.ParamByName('TIPO_LANCAMENTO').Value := 'D';
          QryAuxS.Params.ParamByName('CODIGO_LAN').Value      := ListaLancamentos[0].CodLancamento;
          QryAuxS.Params.ParamByName('ANO_LAN').Value         := ListaLancamentos[0].AnoLancamento;
          QryAuxS.Params.ParamByName('ID_ORIGEM').Value       := Null;
          QryAuxS.Params.ParamByName('TABELA_ORIGEM').Value   := Null;
          QryAuxS.Params.ParamByName('TIPO_ACAO').Value       := 'I';
          QryAuxS.Params.ParamByName('OBS_USUARIO_LOG').Value := 'Inclus�o AUTOM�TICA de Despesa de VALE.';

          QryAuxS.ExecSQL;
        end;
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      dmPrincipal.FinalizarComponenteLancamento;

      Msg := 'Dados do Vale gravados com sucesso!';
    except
      on E: Exception do
      begin
        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Rollback;

        Result := False;
        Msg := 'Erro na grava��o do Dados do Vale.';
      end;
    end;
  end;

  FreeAndNil(QryAuxS);
end;

procedure TFCadastroVale.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta de Dados de Vale',
                          vgIdConsulta, 'VALE');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclus�o de Dados de Vale',
                          vgIdConsulta, 'VALE');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da edi��o desse Vale:', '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de Vale';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          vgIdConsulta, 'VALE');
    end;
  end;
end;

procedure TFCadastroVale.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroVale.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

function TFCadastroVale.VerificarDadosGenerico(var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if dteDataVale.Date = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a DATA DO VALE.';
    DadosMsgErro.Componente := dteDataVale;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end
  else
  begin
    if dteDataVale.Date > Date then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') A DATA DO VALE n�o pode ser superior � Data de hoje.';
      DadosMsgErro.Componente := dteDataVale;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;

    if dteDataVale.Date < vgDataLancVigente then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') A DATA DO VALE n�o pode ser inferior a ' +
                                                          FormatDateTime('DD/MM/YYYY', vgDataLancVigente) +
                                                          ', pois os Caixas anteriores a essa data j� se encontram FECHADOS.';;
      DadosMsgErro.Componente := dteDataVale;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;
  end;

  if Trim(lcbNomeFuncionario.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o NOME DO COLABORADOR.';
    DadosMsgErro.Componente := lcbNomeFuncionario;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(mmDescricao.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a DESCRI��O DO VALE.';
    DadosMsgErro.Componente := mmDescricao;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if cedValor.Value = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o VALOR DO VALE.';
    DadosMsgErro.Componente := cedValor;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroVale.VerificarLayoutAto(var QtdErros: Integer): Boolean;
begin
  //
end;

end.
