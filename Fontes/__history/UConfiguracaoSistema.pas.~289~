{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UConfiguracaoSistema.pas
  Descricao:   Tela de configuracao dos parametros do Sistema
  Author   :   Cristina
  Date:        26-fev-2016
  Last Update: 10-nov-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UConfiguracaoSistema;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Datasnap.Provider, Datasnap.DBClient, Vcl.DBCtrls, Vcl.Mask, Vcl.Grids,
  Vcl.DBGrids, JvBaseDlg, JvSelectDirectory, Vcl.FileCtrl, System.StrUtils,
  JvExMask, JvToolEdit, JvMaskEdit, JvDBControls;

type
  TFConfiguracaoSistema = class(TFCadastroGeralPadrao)
    btnDispositivos: TJvTransparentButton;
    ntbConfigSistema: TNotebook;
    cbAmbiente: TDBComboBox;
    edtPathBancoDados: TDBEdit;
    edtPathImagens: TDBEdit;
    lblAmbiente: TLabel;
    lblPathBancoDados: TLabel;
    lblPathImagens: TLabel;
    lblFlgAtualizacao: TLabel;
    cbFlgAtualizacao: TDBComboBox;
    dbgDispositivos: TDBGrid;
    lcbTipoDisp: TDBLookupComboBox;
    cbFlgAtivoDisp: TDBComboBox;
    edtPathDisp: TDBEdit;
    lblTipoDisp: TLabel;
    lblFlgAtivoDisp: TLabel;
    lblPathDisp: TLabel;
    btnIncluirDisp: TJvTransparentButton;
    btnEditarDisp: TJvTransparentButton;
    btnExcluirDisp: TJvTransparentButton;
    btnConfirmarDisp: TJvTransparentButton;
    btnCancelarDisp: TJvTransparentButton;
    btnAbrirPathDisp: TJvTransparentButton;
    sldCaminho: TJvSelectDirectory;
    btnAbrirPathBD: TJvTransparentButton;
    btnAbrirPathImg: TJvTransparentButton;
    gbDiaPagamentoFunc: TGroupBox;
    rbtnUltimoDia: TRadioButton;
    rbtnPrimeiroDia: TRadioButton;
    rbtnSegundoDia: TRadioButton;
    rbtnTerceiroDia: TRadioButton;
    rbtnQuartoDia: TRadioButton;
    rbtnQuintoDia: TRadioButton;
    cbTrabalhaComissoes: TDBComboBox;
    lblTrabalhaComissoes: TLabel;
    lblPathRelatoriosPDF: TLabel;
    edtPathRelatoriosPDF: TDBEdit;
    btnAbrirPathRelPDF: TJvTransparentButton;
    lblVersaoSync: TLabel;
    edtVersaoSync: TDBEdit;
    lblIntervaloSync: TLabel;
    edtIntervaloSync: TDBEdit;
    lblPathCarneLeao: TLabel;
    edtPathCarneLeao: TDBEdit;
    btnAbrirPathCarneLeao: TJvTransparentButton;
    gbServentia: TGroupBox;
    lcbServentia: TDBLookupComboBox;
    lcbCidade: TDBLookupComboBox;
    lblCidade: TLabel;
    lcbUFEnd: TDBLookupComboBox;
    lblUFEnd: TLabel;
    edtEndereco: TDBEdit;
    lblEndereco: TLabel;
    edtServentia: TDBEdit;
    lblServentia: TLabel;
    lblCNPJ: TLabel;
    medCNPJ: TJvDBMaskEdit;
    lblNomeServentiaRelatorio: TLabel;
    edtNomeServentiaRelatorio: TDBEdit;
    gbExpedienteFDS: TGroupBox;
    chbExpedienteSabado: TDBCheckBox;
    chbExpedienteDomingo: TDBCheckBox;
    chbExpedienteFeriado: TDBCheckBox;
    lblFlgZerarNumOficio: TLabel;
    cbFlgZerarNumOficio: TDBComboBox;
    lblHorarioFechamento: TLabel;
    medHorarioFechamento: TJvDBMaskEdit;
    rgDiaFechamentoCaixa: TDBRadioGroup;
    procedure edtPathDispKeyPress(Sender: TObject; var Key: Char);
    procedure btnIncluirDispClick(Sender: TObject);
    procedure btnEditarDispClick(Sender: TObject);
    procedure btnExcluirDispClick(Sender: TObject);
    procedure btnCancelarDispClick(Sender: TObject);
    procedure btnConfirmarDispClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure dbgDispositivosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure edtServentiaExit(Sender: TObject);
    procedure lcbServentiaExit(Sender: TObject);
    procedure btnAbrirPathDispClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnDadosPrincipaisClick(Sender: TObject);
    procedure btnDispositivosClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAbrirPathImgClick(Sender: TObject);
    procedure btnAbrirPathBDClick(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure rbtnUltimoDiaClick(Sender: TObject);
    procedure rbtnPrimeiroDiaClick(Sender: TObject);
    procedure rbtnSegundoDiaClick(Sender: TObject);
    procedure rbtnTerceiroDiaClick(Sender: TObject);
    procedure rbtnQuartoDiaClick(Sender: TObject);
    procedure rbtnQuintoDiaClick(Sender: TObject);
    procedure btnAbrirPathRelPDFClick(Sender: TObject);
    procedure btnAbrirPathCarneLeaoClick(Sender: TObject);
    procedure lcbUFEndExit(Sender: TObject);
    procedure medCNPJKeyPress(Sender: TObject; var Key: Char);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }

    iIdIncDisp: Integer;

    procedure HabDesabBotoesCRUD(Inc, Ed, Exc: Boolean);
    procedure AbrirPath(Tipo: String);
    procedure SelecionaRadioButton(Tag: Integer);

    function VerificarDispositivo(var Msg: String; var QtdErros: Integer): Boolean;
  public
    { Public declarations }
  end;

var
  FConfiguracaoSistema: TFConfiguracaoSistema;

implementation

{$R *.dfm}

uses UDM, UGDM, UBibliotecaSistema, UVariaveisGlobais, UDMConfiguracaoSistema;

{ TFConfiguracaoSistema }

procedure TFConfiguracaoSistema.AbrirPath(Tipo: String);
begin
  try
    sldCaminho.Title := 'Selecione o caminho:';
    sldCaminho.InitialDir := 'C:/';
    sldCaminho.Options := sldCaminho.Options + [sdAllowCreate, sdPerformCreate];
    sldCaminho.ClassicDialog := False;  //Troca estilos

    if sldCaminho.Execute() then
    begin
      case AnsiIndexStr(UpperCase(Tipo), ['B', 'I', 'D', 'R', 'C']) of
        0: dmConfiguracaoSistema.cdsConfiguracao.FieldByName('DIR_DATABASE').AsString       := sldCaminho.Directory + '\MONITORAMENTO.FDB';
        1: dmConfiguracaoSistema.cdsConfiguracao.FieldByName('DIR_ARQ_IMAGENS').AsString    := sldCaminho.Directory + '\';
        2: dmConfiguracaoSistema.cdsDispositivo.FieldByName('DIR_DISPOSITIVO').AsString     := sldCaminho.Directory + '\';
        3: dmConfiguracaoSistema.cdsConfiguracao.FieldByName('DIR_ARQ_RELATORIOS').AsString := sldCaminho.Directory + '\';
        4: dmConfiguracaoSistema.cdsConfiguracao.FieldByName('DIR_ARQ_CARNELEAO').AsString  := sldCaminho.Directory + '\';
      end;
    end;
  except
    Application.MessageBox('Erro ao associar o caminho selecionado.', 'Erro', MB_OK + MB_ICONERROR);
  end;
end;

procedure TFConfiguracaoSistema.btnAbrirPathBDClick(Sender: TObject);
begin
  inherited;

  AbrirPath('B');
end;

procedure TFConfiguracaoSistema.btnAbrirPathDispClick(Sender: TObject);
begin
  inherited;

  AbrirPath('D');
end;

procedure TFConfiguracaoSistema.btnAbrirPathImgClick(Sender: TObject);
begin
  inherited;

  AbrirPath('I');
end;

procedure TFConfiguracaoSistema.btnAbrirPathRelPDFClick(Sender: TObject);
begin
  inherited;

  AbrirPath('R');
end;

procedure TFConfiguracaoSistema.btnCancelarClick(Sender: TObject);
begin
  inherited;

  Self.Close;
end;

procedure TFConfiguracaoSistema.btnCancelarDispClick(Sender: TObject);
begin
  inherited;

  dmConfiguracaoSistema.cdsDispositivo.Cancel;

  if dmConfiguracaoSistema.cdsDispositivo.RecordCount = 0 then
    HabDesabBotoesCRUD(True, False, False)
  else
    HabDesabBotoesCRUD(True, True, True);
end;

procedure TFConfiguracaoSistema.btnConfirmarDispClick(Sender: TObject);
var
  Msg: String;
  iCount, Qtd: Integer;
  lInserindo: Boolean;
begin
  inherited;

  lblMensagemErro.Caption := '  Mensagem:';
  lbMensagemErro.Color    := clWindow;
  lbMensagemErro.Clear;

  Msg    := '';
  iCount := 0;
  Qtd    := 0;
  lInserindo := False;

  if not VerificarDispositivo(Msg, Qtd) then
  begin
    case Qtd of
      0:
      begin
        lblMensagemErro.Caption := '  Mensagem:';
        lbMensagemErro.Color    := clWindow;
      end;
      1:
      begin
        lblMensagemErro.Caption := '  Durante a verifica��o dos dados, o seguinte erro foi encontrado:';
        lbMensagemErro.Color    := clInfoBk;
      end
      else
      begin
        lblMensagemErro.Caption := '  Durante a verifica��o dos dados, os seguintes erros foram encontrados:';
        lbMensagemErro.Color    := clInfoBk;
      end;
    end;

    for iCount := 0 to Pred(ListaMsgErro.Count) do
      lbMensagemErro.Items.Add(ListaMsgErro[iCount].Mensagem);
  end
  else
  begin
    if dmConfiguracaoSistema.cdsDispositivo.State = dsInsert then
    begin
      Inc(iIdIncDisp);
      dmConfiguracaoSistema.cdsDispositivo.FieldByName('ID_DISPOSITIVO').AsInteger := iIdIncDisp;

      lInserindo := True;
    end;

    dmConfiguracaoSistema.cdsDispositivo.Post;

    if lInserindo then
    begin
      if Application.MessageBox('Deseja incluir outro Dispositivo?', 'Confirma��o', MB_YESNO + MB_ICONQUESTION) = ID_YES then
        btnIncluirDisp.Click
      else
        HabDesabBotoesCRUD(True, True, True);
    end;
  end;
end;

procedure TFConfiguracaoSistema.btnDadosPrincipaisClick(Sender: TObject);
begin
  inherited;

  MarcarDesmarcarAbas(PRINC);
end;

procedure TFConfiguracaoSistema.btnDispositivosClick(Sender: TObject);
begin
  inherited;

  MarcarDesmarcarAbas(DISP);
end;

procedure TFConfiguracaoSistema.btnEditarDispClick(Sender: TObject);
begin
  inherited;

  if dmConfiguracaoSistema.cdsDispositivo.RecordCount > 0 then
  begin
    dmConfiguracaoSistema.cdsDispositivo.Edit;
    HabDesabBotoesCRUD(False, false, False);
  end;
end;

procedure TFConfiguracaoSistema.btnExcluirDispClick(Sender: TObject);
begin
  inherited;

  if dmConfiguracaoSistema.cdsDispositivo.RecordCount > 0 then
    dmConfiguracaoSistema.cdsDispositivo.Delete;
end;

procedure TFConfiguracaoSistema.btnIncluirDispClick(Sender: TObject);
begin
  inherited;

  dmConfiguracaoSistema.cdsDispositivo.Append;

  dmConfiguracaoSistema.cdsDispositivo.FieldByName('NOVO').AsBoolean     := True;
  dmConfiguracaoSistema.cdsDispositivo.FieldByName('FLG_ATIVO').AsString := 'S';

  HabDesabBotoesCRUD(False, False, False);

  if lcbTipoDisp.CanFocus then
    lcbTipoDisp.SetFocus;
end;

procedure TFConfiguracaoSistema.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFConfiguracaoSistema.dbgDispositivosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;

  AlternarCorGrid(Sender, Rect, DataCol, Column, State);
end;

procedure TFConfiguracaoSistema.edtPathDispKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dmConfiguracaoSistema.cdsDispositivo.State in [dsInsert, dsEdit] then
      btnConfirmarDisp.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFConfiguracaoSistema.edtServentiaExit(Sender: TObject);
begin
  inherited;

  if Trim(edtServentia.Text) <> '' then
    dmConfiguracaoSistema.cdsConfiguracao.FieldByName('COD_SERVENTIA').AsInteger := StrToInt(edtServentia.Text);
end;

procedure TFConfiguracaoSistema.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FreeAndNil(dmConfiguracaoSistema);

  vgOperacao   := VAZIO;
  vgIdConsulta := 0;

  inherited;
end;

procedure TFConfiguracaoSistema.FormCreate(Sender: TObject);
begin
  inherited;

  ShowScrollBar(dbgDispositivos.Handle, SB_HORZ, False);

  dmConfiguracaoSistema.qryServentia.Close;
  dmConfiguracaoSistema.qryServentia.Open;

  dmConfiguracaoSistema.qryUF.Close;
  dmConfiguracaoSistema.qryUF.Open;

  dmConfiguracaoSistema.cdsConfiguracao.Close;
  dmConfiguracaoSistema.cdsConfiguracao.Open;

  if vgOperacao = E then
  begin
    dmConfiguracaoSistema.qryCidade.Close;
    dmConfiguracaoSistema.qryCidade.Params.ParamByName('SUF').Value := Trim(lcbUFEnd.Text);
    dmConfiguracaoSistema.qryCidade.Open;

    dmConfiguracaoSistema.cdsConfiguracao.Edit;
  end;

  case dmConfiguracaoSistema.cdsConfiguracao.FieldByName('DIA_PAGAMENTOFUNC').AsInteger of
    0: rbtnUltimoDia.Checked   := True;
    1: rbtnPrimeiroDia.Checked := True;
    2: rbtnSegundoDia.Checked  := True;
    3: rbtnTerceiroDia.Checked := True;
    4: rbtnQuartoDia.Checked   := True;
    5: rbtnQuintoDia.Checked   := True;
  end;

  dmConfiguracaoSistema.qryTipoDispositivo.Close;
  dmConfiguracaoSistema.qryTipoDispositivo.Open;

  dmConfiguracaoSistema.cdsDispositivo.Close;
  dmConfiguracaoSistema.cdsDispositivo.Open;

  iIdIncDisp := dmConfiguracaoSistema.cdsDispositivo.RecordCount;
end;

procedure TFConfiguracaoSistema.FormShow(Sender: TObject);
begin
  inherited;

  edtVersaoSync.Enabled := False;

  if dmConfiguracaoSistema.cdsDispositivo.RecordCount > 0 then
    HabDesabBotoesCRUD(True, True, True)
  else
    HabDesabBotoesCRUD(True, False, False);

  btnDispositivos.Visible := False;

  MarcarDesmarcarAbas(PRINC);
end;

procedure TFConfiguracaoSistema.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtServentia.MaxLength              := 4;
  edtNomeServentiaRelatorio.MaxLength := 250;
  edtPathBancoDados.MaxLength         := 600;
  edtPathImagens.MaxLength            := 600;
  edtPathDisp.MaxLength               := 600;
  edtPathRelatoriosPDF.MaxLength      := 600;
end;

procedure TFConfiguracaoSistema.DesabilitarComponentes;
begin
  inherited;

  lcbCidade.Enabled := ((dmConfiguracaoSistema.qryCidade.FieldByName('MUNICIPIO').AsString) <> '');

  lcbTipoDisp.Enabled      := False;
  cbFlgAtivoDisp.Enabled   := False;
  edtPathDisp.Enabled      := False;
  btnAbrirPathDisp.Enabled := False;
end;

function TFConfiguracaoSistema.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFConfiguracaoSistema.GravarDadosGenerico(var Msg: String): Boolean;
var
  qryAtuSrvn: TFDQuery;
begin
  inherited;

  Result := True;
  Msg := '';

  if vgOperacao = C then
    Exit;

  qryAtuSrvn := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    { CONFIGURACOES }
    if dmConfiguracaoSistema.cdsConfiguracao.State in [dsInsert, dsEdit] then
    begin
      dmConfiguracaoSistema.cdsConfiguracao.FieldByName('FLG_BLOQUEIO_FECHAMENTO').AsString := 'N';
      dmConfiguracaoSistema.cdsConfiguracao.Post;
      dmConfiguracaoSistema.cdsConfiguracao.ApplyUpdates(0);
    end;

    { DISPOSITIVOS }
    dmConfiguracaoSistema.cdsDispositivo.First;

    while not dmConfiguracaoSistema.cdsDispositivo.Eof do
    begin
      if dmConfiguracaoSistema.cdsDispositivo.FieldByName('NOVO').AsBoolean then
      begin
        dmConfiguracaoSistema.cdsDispositivo.Edit;
        dmConfiguracaoSistema.cdsDispositivo.FieldByName('ID_DISPOSITIVO').AsInteger := BS.ProximoId('ID_DISPOSITIVO', 'DISPOSITIVO');
        dmConfiguracaoSistema.cdsDispositivo.Post;
      end;

      dmConfiguracaoSistema.cdsDispositivo.Next;
    end;

    dmConfiguracaoSistema.cdsDispositivo.ApplyUpdates(0);

    { SERVENTIA }
    if dmConfiguracaoSistema.qryServentia.State in [dsInsert, dsEdit] then
    begin
      dmConfiguracaoSistema.qryServentia.Post;
      dmConfiguracaoSistema.qryServentia.ApplyUpdates(0);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    vgConf_Atribuicao           := dmConfiguracaoSistema.cdsConfiguracao.FieldByName('ATRIBUICAO').AsInteger;
    vgConf_Ambiente             := dmConfiguracaoSistema.cdsConfiguracao.FieldByName('AMBIENTE').AsString;
    vgConf_CodServentia         := dmConfiguracaoSistema.cdsConfiguracao.FieldByName('COD_SERVENTIA').AsString;
    vgConf_NomeServentia        := dmConfiguracaoSistema.cdsConfiguracao.FieldByName('NOME_SERVENTIA_RELATORIO').AsString;
    vgConf_DiretorioDatabase    := dmConfiguracaoSistema.cdsConfiguracao.FieldByName('DIR_DATABASE').AsString;
    vgConf_DiretorioImagens     := dmConfiguracaoSistema.cdsConfiguracao.FieldByName('DIR_ARQ_IMAGENS').AsString;
    vgConf_DiretorioRelatorios  := dmConfiguracaoSistema.cdsConfiguracao.FieldByName('DIR_ARQ_RELATORIOS').AsString;
    vgConf_DiretorioCarneLeao   := dmConfiguracaoSistema.cdsConfiguracao.FieldByName('DIR_ARQ_CARNELEAO').AsString;
    vgConf_FlgExibeAtualizacao  := dmConfiguracaoSistema.cdsConfiguracao.FieldByName('FLG_EXB_ATUALIZACAO').AsString;
    vgConf_FlgTrabalhaComissao  := dmConfiguracaoSistema.cdsConfiguracao.FieldByName('FLG_COMISSAO').AsString;
    vgConf_FlgExpedienteSabado  := dmConfiguracaoSistema.cdsConfiguracao.FieldByName('FLG_EXPEDIENTESABADO').AsString;
    vgConf_FlgExpedienteDomingo := dmConfiguracaoSistema.cdsConfiguracao.FieldByName('FLG_EXPEDIENTEDOMINGO').AsString;
    vgConf_FlgExpedienteFeriado := dmConfiguracaoSistema.cdsConfiguracao.FieldByName('FLG_EXPEDIENTEFERIADO').AsString;
    vgConf_FlgZerarNumOficio    := dmConfiguracaoSistema.cdsConfiguracao.FieldByName('FLG_ZERARNUMOFICIO').AsString;
    vgConf_FlgBloqueiaFechPagto := dmConfiguracaoSistema.cdsConfiguracao.FieldByName('FLG_BLOQUEIO_FECHAMENTO').AsString;
    vgConf_DataIniImportacao    := dmConfiguracaoSistema.cdsConfiguracao.FieldByName('DATA_INI_IMPORTACAO').AsDateTime;
    vgConf_HoraFechamentoPadrao := dmConfiguracaoSistema.cdsConfiguracao.FieldByName('HORA_FECHAMENTOPADRAO').AsString;
    vgConf_DiaFechamentoCaixa   := dmConfiguracaoSistema.cdsConfiguracao.FieldByName('DIA_FECHAMENTOCAIXA').AsString;

    //SERVENTIA
    qryAtuSrvn.Close;
    qryAtuSrvn.SQL.Clear;
    qryAtuSrvn.SQL.Text := 'SELECT * ' +
                           '  FROM SERVENTIA ' +
                           ' WHERE COD_SERVENTIA = :CodServentia';
    qryAtuSrvn.Params.ParamByName('CodServentia').Value := vgConf_CodServentia;
    qryAtuSrvn.Open;

    vgSrvn_IdServentia       := qryAtuSrvn.FieldByName('ID_SERVENTIA').AsInteger;
    vgSrvn_CodServentia      := qryAtuSrvn.FieldByName('COD_SERVENTIA').AsString;
    vgSrvn_NomeServentia     := qryAtuSrvn.FieldByName('NOME_SERVENTIA').AsString;
    vgSrvn_EndServentia      := qryAtuSrvn.FieldByName('ENDERECO').AsString;
    vgSrvn_CidadeServentia   := qryAtuSrvn.FieldByName('CIDADE').AsString;
    vgSrvn_EstadoServentia   := qryAtuSrvn.FieldByName('UF').AsString;
    vgSrvn_CEPServentia      := qryAtuSrvn.FieldByName('CEP').AsString;
    vgSrvn_TelServentia      := qryAtuSrvn.FieldByName('TELEFONE').AsString;
    vgSrvn_EmailServentia    := qryAtuSrvn.FieldByName('EMAIL').AsString;
    vgSrvn_NomeTabeliao      := qryAtuSrvn.FieldByName('NOME_TABELIAO').AsString;
    vgSrvn_MatriculaTabeliao := qryAtuSrvn.FieldByName('MATRICULA_TABELIAO').AsString;
    vgSrvn_NomeTabeliaoSubst := qryAtuSrvn.FieldByName('NOME_SUBSTITUTO').AsString;
    vgSrvn_CNPJServentia     := qryAtuSrvn.FieldByName('CNPJ').AsString;
    vgSrvn_CodMunicipioServ  := qryAtuSrvn.FieldByName('COD_MUN').AsInteger;
    vgSrvn_TituloServentia   := qryAtuSrvn.FieldByName('TITULO').AsString;

    Msg := 'Configura��es gravadas com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na grava��o das Configura��es.';
  end;

  FreeAndNil(qryAtuSrvn);
end;

procedure TFConfiguracaoSistema.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    I, C:  //INCLUSAO /CONSULTA
    begin
      BS.GravarUsuarioLog(80, '', 'Somente consulta', vgIdConsulta, 'CONFIGURACAO');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da edi��o das Configura��es:', '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de Configura��o';

      BS.GravarUsuarioLog(80, '', sObservacao, vgIdConsulta, 'CONFIGURACAO');
    end;
  end;
end;

procedure TFConfiguracaoSistema.HabDesabBotoesCRUD(Inc, Ed,
  Exc: Boolean);
begin
  btnIncluirDisp.Enabled   := Inc;
  btnEditarDisp.Enabled    := Ed;
  btnExcluirDisp.Enabled   := Exc;
  btnConfirmarDisp.Enabled := not (Inc or Ed or Exc);
  btnCancelarDisp.Enabled  := not (Inc or Ed or Exc);
  lcbTipoDisp.Enabled      := not (Inc or Ed or Exc);
  cbFlgAtivoDisp.Enabled   := not (Inc or Ed or Exc);
  edtPathDisp.Enabled      := not (Inc or Ed or Exc);
  btnAbrirPathDisp.Enabled := not (Inc or Ed or Exc);
end;

procedure TFConfiguracaoSistema.btnAbrirPathCarneLeaoClick(Sender: TObject);
begin
  inherited;

  AbrirPath('C');
end;

procedure TFConfiguracaoSistema.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(ntbConfigSistema);
end;

procedure TFConfiguracaoSistema.lcbServentiaExit(Sender: TObject);
begin
  inherited;

  if lcbServentia.KeyValue > -1 then
    edtServentia.Text := dmConfiguracaoSistema.qryServentia.FieldByName('COD_SERVENTIA').AsString;
end;

procedure TFConfiguracaoSistema.lcbUFEndExit(Sender: TObject);
begin
  inherited;

  if Trim(lcbUFEnd.Text) <> '' then
  begin
    lcbCidade.Enabled := True;

    dmConfiguracaoSistema.qryCidade.Close;
    dmConfiguracaoSistema.qryCidade.Params.ParamByName('SUF').Value := Trim(lcbUFEnd.Text);
    dmConfiguracaoSistema.qryCidade.Open;

    if lcbCidade.CanFocus then
      lcbCidade.SetFocus;
  end
  else
    lcbCidade.Enabled := False;
end;

procedure TFConfiguracaoSistema.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;

  case Aba of
    PRINC:
    begin
      ntbConfigSistema.PageIndex := 0;

      if edtServentia.CanFocus then
        edtServentia.SetFocus;
    end;
    DISP:
    begin
      ntbConfigSistema.PageIndex := 1;

      if lcbTipoDisp.CanFocus then
        lcbTipoDisp.SetFocus;
    end;
  end;

  btnDadosPrincipais.Transparent := not (Aba = PRINC);
  btnDispositivos.Transparent    := not (Aba = DISP);
end;

procedure TFConfiguracaoSistema.medCNPJKeyPress(Sender: TObject; var Key: Char);
var
  sCNPJ: String;
begin
  inherited;

  sCNPJ := ReplaceStr(ReplaceStr(ReplaceStr(medCNPJ.Text, '.', ''), '/', ''), '-', '');

  if (Key = #9) or  //TAB
    (Key = #13) then  //ENTER
  begin
    if not dmGerencial.VerificarCNPJ(Trim(sCNPJ)) then
    begin
      Application.MessageBox('CNPJ inv�lido.', 'Erro', MB_OK + MB_ICONERROR);

      medCNPJ.Clear;

      if medCNPJ.CanFocus then
        medCNPJ.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFConfiguracaoSistema.rbtnPrimeiroDiaClick(Sender: TObject);
begin
  inherited;

  SelecionaRadioButton(rbtnPrimeiroDia.Tag);
end;

procedure TFConfiguracaoSistema.rbtnQuartoDiaClick(Sender: TObject);
begin
  inherited;

  SelecionaRadioButton(rbtnQuartoDia.Tag);
end;

procedure TFConfiguracaoSistema.rbtnQuintoDiaClick(Sender: TObject);
begin
  inherited;

  SelecionaRadioButton(rbtnQuintoDia.Tag);
end;

procedure TFConfiguracaoSistema.rbtnSegundoDiaClick(Sender: TObject);
begin
  inherited;

  SelecionaRadioButton(rbtnSegundoDia.Tag);
end;

procedure TFConfiguracaoSistema.rbtnTerceiroDiaClick(Sender: TObject);
begin
  inherited;

  SelecionaRadioButton(rbtnTerceiroDia.Tag);
end;

procedure TFConfiguracaoSistema.rbtnUltimoDiaClick(Sender: TObject);
begin
  inherited;

  SelecionaRadioButton(rbtnUltimoDia.Tag);
end;

procedure TFConfiguracaoSistema.SelecionaRadioButton(Tag: Integer);
var
  iDia: Integer;
begin
  case Tag of
    10: iDia := 0;
    11: iDia := 1;
    12: iDia := 2;
    13: iDia := 3;
    14: iDia := 4;
    15: iDia := 5;
  end;

  rbtnUltimoDia.Checked   := (iDia = 0);
  rbtnPrimeiroDia.Checked := (iDia = 1);
  rbtnSegundoDia.Checked  := (iDia = 2);
  rbtnTerceiroDia.Checked := (iDia = 3);
  rbtnQuartoDia.Checked   := (iDia = 4);
  rbtnQuintoDia.Checked   := (iDia = 5);

  if dmConfiguracaoSistema.cdsConfiguracao.State in [dsInsert, dsEdit] then
    dmConfiguracaoSistema.cdsConfiguracao.FieldByName('DIA_PAGAMENTOFUNC').AsInteger := iDia;
end;

function TFConfiguracaoSistema.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
var
  sCNPJ: String;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  { CONFIGURACAO }
  if Trim(edtServentia.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher a SERVENTIA.';
    DadosMsgErro.Componente := edtServentia;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtEndereco.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher o ENDERE�O DA SERVENTIA.';
    DadosMsgErro.Componente := edtEndereco;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(lcbUFEnd.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher a UF DA SERVENTIA.';
    DadosMsgErro.Componente := lcbUFEnd;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(lcbCidade.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher a CIDADE DA SERVENTIA.';
    DadosMsgErro.Componente := lcbCidade;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  sCNPJ := ReplaceStr(ReplaceStr(ReplaceStr(medCNPJ.Text, '.', ''), '/', ''), '-', '');

  if Trim(sCNPJ) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher o CNPJ DA SERVENTIA.';
    DadosMsgErro.Componente := medCNPJ;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtNomeServentiaRelatorio.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher o NOME DA SERVENTIA PARA OS RELAT�RIOS.';
    DadosMsgErro.Componente := edtNomeServentiaRelatorio;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(cbAmbiente.Text) = ''  then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o AMBIENTE.';
    DadosMsgErro.Componente := cbAmbiente;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtPathBancoDados.Text) = ''  then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o CAMINHO DO BANCO DE DADOS.';
    DadosMsgErro.Componente := edtPathBancoDados;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtPathImagens.Text) = ''  then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o CAMINHO DA PASTA DE IMAGENS.';
    DadosMsgErro.Componente := edtPathImagens;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end
  else
  begin
    if Copy(Trim(edtPathImagens.Text), Length(edtPathImagens.Text), 1) <> '\' then
      edtPathImagens.Text := (edtPathImagens.Text + '\');
  end;

  if Trim(edtPathRelatoriosPDF.Text) = ''  then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o CAMINHO DA PASTA DE RELAT�RIOS (.PDF).';
    DadosMsgErro.Componente := edtPathRelatoriosPDF;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end
  else
  begin
    if Copy(Trim(edtPathRelatoriosPDF.Text), Length(edtPathRelatoriosPDF.Text), 1) <> '\' then
      edtPathRelatoriosPDF.Text := (edtPathRelatoriosPDF.Text + '\');
  end;

  if Trim(cbTrabalhaComissoes.Text) = ''  then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar se a Serventia trabalha ou n�o com COMISS�ES.';
    DadosMsgErro.Componente := cbTrabalhaComissoes;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(cbFlgAtualizacao.Text) = ''  then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher se deseja VISUALIZAR AS ATUALIZA��ES.';
    DadosMsgErro.Componente := cbFlgAtualizacao;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(ReplaceStr(medHorarioFechamento.Text, ':', '')) = ''  then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta o Hor�rio de Fechamento de Caixa.';
    DadosMsgErro.Componente := medHorarioFechamento;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFConfiguracaoSistema.VerificarDispositivo(var Msg: String;
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  Msg      := '';
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if Trim(lcbTipoDisp.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher o TIPO DO DISPOSITIVO. (Aba: Dispositivos)';
    DadosMsgErro.Componente := lcbTipoDisp;
    DadosMsgErro.PageIndex  := 1;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(cbFlgAtivoDisp.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta informar se o DISPOSITIVO est� ou n�o ATIVO. (Aba: Dispositivos)';
    DadosMsgErro.Componente := cbFlgAtivoDisp;
    DadosMsgErro.PageIndex  := 1;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtPathDisp.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher o CAMINHO DO DISPOSITIVO. (Aba: Dispositivos)';
    DadosMsgErro.Componente := edtPathDisp;
    DadosMsgErro.PageIndex  := 1;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if QtdErros > 0 then
    Result := False;
end;

function TFConfiguracaoSistema.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

end.
