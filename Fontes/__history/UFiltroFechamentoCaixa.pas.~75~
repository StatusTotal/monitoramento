{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroFechamentoCaixa.pas
  Descricao:   Tela de filtro de Fechamento de Caixa
  Author   :   Cristina
  Date:        26-dez-2016
  Last Update: 11-out-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroFechamentoCaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Mask, JvExMask, JvToolEdit, System.DateUtils;

type
  TFFiltroFechamentoCaixa = class(TFFiltroSimplesPadrao)
    lblPeriodoFechamento: TLabel;
    dteIniPeriodoFechamento: TJvDateEdit;
    Label3: TLabel;
    dteFimPeriodoFechamento: TJvDateEdit;
    rgSituacao: TRadioGroup;
    qryGridPaiPadraoID: TIntegerField;
    qryGridPaiPadraoDATA_FECHAMENTO: TDateField;
    qryGridPaiPadraoHORA_FECHAMENTO: TTimeField;
    qryGridPaiPadraoVR_TOTAL_ULT_AB: TBCDField;
    qryGridPaiPadraoVR_RECIBO_IMPORTADO: TBCDField;
    qryGridPaiPadraoVR_IMPORT_DINHEIRO: TBCDField;
    qryGridPaiPadraoVR_IMPORT_DEPOSITO: TBCDField;
    qryGridPaiPadraoVR_IMPORT_CARTAO: TBCDField;
    qryGridPaiPadraoVR_IMPORT_CHEQUE: TBCDField;
    qryGridPaiPadraoVR_IMPORT_PROMISSORIA: TBCDField;
    qryGridPaiPadraoVR_IMPORT_FATURADO: TBCDField;
    qryGridPaiPadraoVR_IMPORT_REPASSES: TBCDField;
    qryGridPaiPadraoVR_OUTRASREC_TOTAL: TBCDField;
    qryGridPaiPadraoVR_OUTRASREC_DINHEIRO: TBCDField;
    qryGridPaiPadraoVR_OUTRASREC_DEPOSITO: TBCDField;
    qryGridPaiPadraoVR_OUTRASREC_CARTAO: TBCDField;
    qryGridPaiPadraoVR_OUTRASREC_CHEQUE: TBCDField;
    qryGridPaiPadraoVR_OUTRASREC_PROMISSORIA: TBCDField;
    qryGridPaiPadraoVR_OUTRASREC_FATURADO: TBCDField;
    qryGridPaiPadraoVR_OUTRASREC_REPASSES: TBCDField;
    qryGridPaiPadraoVR_SRVANT_CARTAO: TBCDField;
    qryGridPaiPadraoVR_SRVANT_CHEQUE: TBCDField;
    qryGridPaiPadraoVR_SRVANT_PROMISSORIA: TBCDField;
    qryGridPaiPadraoVR_SRVANT_FATURADO: TBCDField;
    qryGridPaiPadraoVR_RECPEND_CARTAO: TBCDField;
    qryGridPaiPadraoVR_RECPEND_CHEQUE: TBCDField;
    qryGridPaiPadraoVR_RECPEND_PROMISSORIA: TBCDField;
    qryGridPaiPadraoVR_RECPEND_FATURADO: TBCDField;
    qryGridPaiPadraoVR_FLUT_DEPOSITO: TBCDField;
    qryGridPaiPadraoVR_FLUT_REPASSE: TBCDField;
    qryGridPaiPadraoVR_FECH_ANTERIOR: TBCDField;
    qryGridPaiPadraoVR_TOTAL_RECEITA: TBCDField;
    qryGridPaiPadraoVR_TOTAL_DESPESA: TBCDField;
    qryGridPaiPadraoVR_SALDO_DIA: TBCDField;
    qryGridPaiPadraoVR_DESP_DINHEIRO: TBCDField;
    qryGridPaiPadraoVR_DESP_DESCONTO: TBCDField;
    qryGridPaiPadraoVR_DESP_REPASSE: TBCDField;
    qryGridPaiPadraoVR_NOTAS: TBCDField;
    qryGridPaiPadraoVR_MOEDAS: TBCDField;
    qryGridPaiPadraoVR_CONTACORRENTE: TBCDField;
    qryGridPaiPadraoVR_CONTAGEM_FINAL: TBCDField;
    qryGridPaiPadraoVR_TOTAL_FECHAMENTO: TBCDField;
    qryGridPaiPadraoVR_DIFERENCA: TBCDField;
    qryGridPaiPadraoOBS_CAIXA_FECHAMENTO: TStringField;
    qryGridPaiPadraoCAD_ID_USUARIO: TIntegerField;
    qryGridPaiPadraoFLG_CANCELADO: TStringField;
    qryGridPaiPadraoDATA_CANCELAMENTO: TDateField;
    qryGridPaiPadraoCANCEL_ID_USUARIO: TIntegerField;
    qryGridPaiPadraoNOME_USUARIO: TStringField;
    procedure btnEditarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure dbgGridPaiPadraoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgGridPaiPadraoTitleClick(Column: TColumn);
    procedure dteFimPeriodoFechamentoKeyPress(Sender: TObject; var Key: Char);
    procedure dteIniPeriodoFechamentoKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryGridPaiPadraoCalcFields(DataSet: TDataSet);
    procedure rgSituacaoClick(Sender: TObject);
    procedure rgSituacaoExit(Sender: TObject);
    procedure qryGridPaiPadraoVR_TOTAL_ULT_ABGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_RECIBO_IMPORTADOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_IMPORT_DINHEIROGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_IMPORT_DEPOSITOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_IMPORT_CARTAOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_IMPORT_CHEQUEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_IMPORT_PROMISSORIAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_IMPORT_FATURADOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_IMPORT_REPASSESGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_OUTRASREC_TOTALGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_OUTRASREC_DINHEIROGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_OUTRASREC_DEPOSITOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_OUTRASREC_CARTAOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_OUTRASREC_CHEQUEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_OUTRASREC_PROMISSORIAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_OUTRASREC_FATURADOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_OUTRASREC_REPASSESGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_SRVANT_CARTAOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_SRVANT_CHEQUEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_SRVANT_PROMISSORIAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_SRVANT_FATURADOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_RECPEND_CARTAOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_RECPEND_CHEQUEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_RECPEND_PROMISSORIAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_RECPEND_FATURADOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_FLUT_DEPOSITOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_FLUT_REPASSEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_FECH_ANTERIORGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_TOTAL_RECEITAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_TOTAL_DESPESAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_SALDO_DIAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_DESP_DINHEIROGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_DESP_DESCONTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_DESP_REPASSEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_NOTASGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_MOEDASGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_CONTACORRENTEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_CONTAGEM_FINALGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_TOTAL_FECHAMENTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_DIFERENCAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure btnImprimirClick(Sender: TObject);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroFechamentoCaixa: TFFiltroFechamentoCaixa;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMCaixa,
  UCadastroFechamentoCaixa, UOpcoesAcaoFechCaixaRel;

{ TFFiltroFechamentoCaixa }

procedure TFFiltroFechamentoCaixa.btnEditarClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  if qryGridPaiPadrao.FieldByName('DATA_FECHAMENTO').AsDateTime <> Date then
    vgOperacao := C
  else
    vgOperacao := E;

  vgIdConsulta := qryGridPaiPadrao.FieldByName('ID').AsInteger;

  Application.CreateForm(TdmCaixa, dmCaixa);
  dmGerencial.CriarForm(TFCadastroFechamentoCaixa, FCadastroFechamentoCaixa);

  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroFechamentoCaixa.btnExcluirClick(Sender: TObject);
var
  iIdUsu: Integer;
  qryExclusao: TFDQuery;
  sNomeUsu, sSenhaUsu: String;
  lCancelado: Boolean;
begin
  inherited;

  lCancelado := False;

  if lPodeExcluir then
  begin
    if Application.MessageBox(PChar('Para excluir esse Fechamento de Caixa ser� preciso informar LOGIN ' +
                                    'e SENHA de Autorizador. ' + #13#10 +
                                    'Deseja prosseguir?'), 'Aviso', MB_YESNO + MB_ICONQUESTION) = ID_YES then
    begin
      iIdUsu    := 0;
      sNomeUsu  := '';
      sSenhaUsu := '';

      repeat
        repeat
          if not InputQuery('LOGIN', 'Por favor, informe o LOGIN do Autorizador:', sNomeUsu) then
            lCancelado := True;
        until (Trim(sNomeUsu) <> '') or lCancelado;

        if not lCancelado then
        begin
          repeat
            if not InputQuery('SENHA', #31'Por favor, informe a SENHA do Autorizador:', sSenhaUsu) then
              lCancelado := True;
          until (Trim(sSenhaUsu) <> '') or lCancelado;
        end;
      until (dmGerencial.VerificarAutorizacao(iIdUsu, sNomeUsu, sSenhaUsu) = True) or lCancelado;

      if lCancelado then
        Exit;
    end
    else
      Exit;

    qryExclusao := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      with qryExclusao, SQL do
      begin
        Close;
        Clear;
        Text := 'UPDATE CAIXA_FECHAMENTO ' +
                '   SET FLG_CANCELADO     = :FLG_CANCELADO, ' +
                '       DATA_CANCELAMENTO = :DATA_CANCELAMENTO, ' +
                '       CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO ' +
                ' WHERE ID_CAIXA_FECHAMENTO = :ID_CAIXA_FECHAMENTO';
        Params.ParamByName('FLG_CANCELADO').Value       := 'S';
        Params.ParamByName('DATA_CANCELAMENTO').Value   := Date;
        Params.ParamByName('CANCEL_ID_USUARIO').Value   := vgUsu_Id;
        Params.ParamByName('ID_CAIXA_FECHAMENTO').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      GravarLog;
      Application.MessageBox('Fechamento de Caixa exclu�da com sucesso!', 'Aviso', MB_OK)
    except
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Application.MessageBox('Erro na exclus�o da Fechamento de Caixa.', 'Erro', MB_OK + MB_ICONERROR)
    end;

    FreeAndNil(qryExclusao);

    btnFiltrar.Click;
  end;
end;

procedure TFFiltroFechamentoCaixa.btnFiltrarClick(Sender: TObject);
begin
  qryGridPaiPadrao.Close;
  qryGridPaiPadrao.Params.ParamByName('DATA_INI').Value := dteIniPeriodoFechamento.Date;
  qryGridPaiPadrao.Params.ParamByName('DATA_FIM').Value := dteFimPeriodoFechamento.Date;

  case rgSituacao.ItemIndex of
    0:
    begin
      qryGridPaiPadrao.Params.ParamByName('SIT1').Value := 'N';
      qryGridPaiPadrao.Params.ParamByName('SIT2').Value := 'S';
    end;
    1:
    begin
      qryGridPaiPadrao.Params.ParamByName('SIT1').Value := 'N';
      qryGridPaiPadrao.Params.ParamByName('SIT2').Value := 'N';
    end;
    2:
    begin
      qryGridPaiPadrao.Params.ParamByName('SIT1').Value := 'S';
      qryGridPaiPadrao.Params.ParamByName('SIT2').Value := 'S';
    end;
  end;

  qryGridPaiPadrao.Open;

  inherited;
end;

procedure TFFiltroFechamentoCaixa.btnImprimirClick(Sender: TObject);
begin
  inherited;

  dmGerencial.CriarForm(TFOpcoesAcaoFechCaixaRel, FOpcoesAcaoFechCaixaRel);
end;

procedure TFFiltroFechamentoCaixa.btnIncluirClick(Sender: TObject);
begin
  inherited;

  Application.CreateForm(TdmCaixa, dmCaixa);

  //Recupera o Valor Total da Ultima Operacao
  dmCaixa.qryUltOperacao.Close;
  dmCaixa.qryUltOperacao.Open;

  if dmCaixa.qryUltOperacao.RecordCount > 0 then
  begin
    if dmCaixa.qryUltOperacao.FieldByName('TIPO').AsString = 'F' then
    begin
      Application.MessageBox('O CAIXA j� se encontra fechado.',
                             'Aviso',
                             MB_OK + MB_ICONWARNING);
      FreeAndNil(dmCaixa);
      Exit;
    end;
  end
  else
  begin
    Application.MessageBox('N�o � poss�vel realizar o Fechamento do CAIXA, pois o mesmo nunca foi aberto.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
    FreeAndNil(dmCaixa);
    Exit;
  end;

  vgOperacao   := I;
  vgIdConsulta := 0;

  dmGerencial.CriarForm(TFCadastroFechamentoCaixa, FCadastroFechamentoCaixa);

  btnFiltrar.Click;
end;

procedure TFFiltroFechamentoCaixa.btnLimparClick(Sender: TObject);
begin
  inherited;

  dteIniPeriodoFechamento.Date := StartOfTheMonth(Date);
  dteFimPeriodoFechamento.Date := EndOfTheMonth(Date);

  rgSituacao.ItemIndex := 1;

  inherited;

  if dteIniPeriodoFechamento.CanFocus then
    dteIniPeriodoFechamento.SetFocus;
end;

procedure TFFiltroFechamentoCaixa.dbgGridPaiPadraoDblClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao   := C;
  vgIdConsulta := qryGridPaiPadrao.FieldByName('ID').AsInteger;

  Application.CreateForm(TdmCaixa, dmCaixa);
  dmGerencial.CriarForm(TFCadastroFechamentoCaixa, FCadastroFechamentoCaixa);

  DefinirPosicaoGrid;
end;

procedure TFFiltroFechamentoCaixa.dbgGridPaiPadraoDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  Grid: TDBGrid;
begin
  inherited;

  Grid := Sender as TDBGrid;

  if Grid.DataSource.DataSet.FieldByName('FLG_CANCELADO').AsString = 'S' then
  begin
    Grid.Canvas.Font.Color := clGray;
    Grid.Canvas.FillRect(Rect);
    Grid.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TFFiltroFechamentoCaixa.dbgGridPaiPadraoTitleClick(Column: TColumn);
begin
  inherited;

  qryGridPaiPadrao.IndexFieldNames := Column.FieldName;
end;

procedure TFFiltroFechamentoCaixa.dteFimPeriodoFechamentoKeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
     if dteFimPeriodoFechamento.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteFimPeriodoFechamento.CanFocus then
        dteFimPeriodoFechamento.SetFocus;
    end
    else if dteIniPeriodoFechamento.Date > dteFimPeriodoFechamento.Date then
    begin
      Application.MessageBox('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso', MB_OK + MB_ICONWARNING);

      dteFimPeriodoFechamento.Clear;

      if dteFimPeriodoFechamento.CanFocus then
        dteFimPeriodoFechamento.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dteFimPeriodoFechamento.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteFimPeriodoFechamento.CanFocus then
        dteFimPeriodoFechamento.SetFocus;
    end
    else if dteIniPeriodoFechamento.Date > dteFimPeriodoFechamento.Date then
    begin
      Application.MessageBox('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso', MB_OK + MB_ICONWARNING);

      dteFimPeriodoFechamento.Clear;

      if dteFimPeriodoFechamento.CanFocus then
        dteFimPeriodoFechamento.SetFocus;
    end
    else
      btnFiltrar.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroFechamentoCaixa.dteIniPeriodoFechamentoKeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
     if dteIniPeriodoFechamento.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteIniPeriodoFechamento.CanFocus then
        dteIniPeriodoFechamento.SetFocus;
    end
    else if dteIniPeriodoFechamento.Date > dteFimPeriodoFechamento.Date then
    begin
      Application.MessageBox('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso', MB_OK + MB_ICONWARNING);

      dteIniPeriodoFechamento.Clear;

      if dteIniPeriodoFechamento.CanFocus then
        dteIniPeriodoFechamento.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dteIniPeriodoFechamento.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteIniPeriodoFechamento.CanFocus then
        dteIniPeriodoFechamento.SetFocus;
    end
    else if dteIniPeriodoFechamento.Date > dteFimPeriodoFechamento.Date then
    begin
      Application.MessageBox('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso', MB_OK + MB_ICONWARNING);

      dteIniPeriodoFechamento.Clear;

      if dteIniPeriodoFechamento.CanFocus then
        dteIniPeriodoFechamento.SetFocus;
    end
    else
      btnFiltrar.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroFechamentoCaixa.FormCreate(Sender: TObject);
begin
  inherited;

  dteIniPeriodoFechamento.Date := StartOfTheMonth(Date);
  dteFimPeriodoFechamento.Date := EndOfTheMonth(Date);

  btnFiltrar.Click;
end;

procedure TFFiltroFechamentoCaixa.FormShow(Sender: TObject);
begin
  inherited;

  if dteIniPeriodoFechamento.CanFocus then
    dteIniPeriodoFechamento.SetFocus;
end;

procedure TFFiltroFechamentoCaixa.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    X:  //EXCLUSAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da exclus�o desse Fechamento de Caixa:', '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'CAIXA_FECHAMENTO');
    end;
    P:  //IMPRESSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente impress�o de Lista de Fechamentos de Caixa',
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'CAIXA_FECHAMENTO');
    end;
    T:  //EXPORTACAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente exporta��o de Lista de Fechamentos de Caixa',
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'CAIXA_FECHAMENTO');
    end;
  end;
end;

function TFFiltroFechamentoCaixa.PodeExcluir(var Msg: String): Boolean;
begin
  Result := True;

  if qryGridPaiPadrao.FieldByName('FLG_CANCELADO').AsString = 'S' then
    msg := 'N�o � poss�vel excluir o Fechamento de Caixa informado, pois o mesmo j� foi cancelado.';

  Result := (qryGridPaiPadrao.FieldByName('FLG_CANCELADO').AsString = 'N') and
            (qryGridPaiPadrao.RecordCount > 0);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoCalcFields(DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  inherited;

  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with qryAux, SQL do
  begin
    //Item
    if qryGridPaiPadrao.FieldByName('CAD_ID_USUARIO').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT NOME ' +
              '  FROM USUARIO ' +
              ' WHERE ID_USUARIO = :ID_USUARIO';
      Params.ParamByName('ID_USUARIO').AsInteger := qryGridPaiPadrao.FieldByName('CAD_ID_USUARIO').AsInteger;
      Open;

      qryGridPaiPadrao.FieldByName('NOME_USUARIO').AsString := qryAux.FieldByName('NOME').AsString;
    end;
  end;

  FreeAndNil(qryAux);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_CONTACORRENTEGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_CONTAGEM_FINALGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_DESP_DESCONTOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_DESP_DINHEIROGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_DESP_REPASSEGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_DIFERENCAGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_FECH_ANTERIORGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_FLUT_DEPOSITOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_FLUT_REPASSEGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_IMPORT_CARTAOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_IMPORT_CHEQUEGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_IMPORT_DEPOSITOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_IMPORT_DINHEIROGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_IMPORT_FATURADOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_IMPORT_PROMISSORIAGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_IMPORT_REPASSESGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_MOEDASGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_NOTASGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_OUTRASREC_CARTAOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_OUTRASREC_CHEQUEGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_OUTRASREC_DEPOSITOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_OUTRASREC_DINHEIROGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_OUTRASREC_FATURADOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_OUTRASREC_PROMISSORIAGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_OUTRASREC_REPASSESGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_OUTRASREC_TOTALGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_RECIBO_IMPORTADOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_RECPEND_CARTAOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_RECPEND_CHEQUEGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_RECPEND_FATURADOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_RECPEND_PROMISSORIAGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_SALDO_DIAGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_SRVANT_CARTAOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_SRVANT_CHEQUEGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_SRVANT_FATURADOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_SRVANT_PROMISSORIAGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_TOTAL_DESPESAGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_TOTAL_FECHAMENTOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_TOTAL_RECEITAGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.qryGridPaiPadraoVR_TOTAL_ULT_ABGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroFechamentoCaixa.rgSituacaoClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroFechamentoCaixa.rgSituacaoExit(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroFechamentoCaixa.VerificarPermissoes;
begin
  inherited;

  btnIncluir.Enabled  := vgPrm_IncFecCx;
  btnEditar.Enabled   := vgPrm_EdFecCx and (qryGridPaiPadrao.RecordCount > 0);
  btnExcluir.Enabled  := vgPrm_ExcFecCx and (qryGridPaiPadrao.RecordCount > 0);
  btnImprimir.Enabled := vgPrm_ImpFecCx and (qryGridPaiPadrao.RecordCount > 0);
end;

end.
