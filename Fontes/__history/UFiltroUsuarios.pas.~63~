unit UFiltroUsuarios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Vcl.StdCtrls, JvMaskEdit, Vcl.Mask, JvExMask, JvToolEdit;

type
  TFFiltroUsuarios = class(TFFiltroSimplesPadrao)
    edtNomeUsu: TEdit;
    lblNomeFiltro: TLabel;
    edtMatriculaUsu: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    dteCadastroUsu: TJvDateEdit;
    Label3: TLabel;
    medCPFUsu: TJvMaskEdit;
    cbSexoUsu: TComboBox;
    Label4: TLabel;
    Label5: TLabel;
    edtQualificacaoUsu: TEdit;
    Label6: TLabel;
    rgStatusUsu: TRadioGroup;
    cbFuncionarioUsu: TComboBox;
    qryGridPaiPadraoID: TIntegerField;
    qryGridPaiPadraoID_USUARIO: TIntegerField;
    qryGridPaiPadraoNOME: TStringField;
    qryGridPaiPadraoCPF: TStringField;
    qryGridPaiPadraoSENHA: TStringField;
    qryGridPaiPadraoQUALIFICACAO: TStringField;
    qryGridPaiPadraoFLG_MASTER: TStringField;
    qryGridPaiPadraoMATRICULA: TStringField;
    qryGridPaiPadraoFLG_SUPORTE: TStringField;
    qryGridPaiPadraoFLG_VISUALIZOU_ATU: TStringField;
    qryGridPaiPadraoFLG_SEXO: TStringField;
    qryGridPaiPadraoID_FUNCIONARIO_FK: TIntegerField;
    qryGridPaiPadraoFLG_ATIVO: TStringField;
    qryGridPaiPadraoDATA_CADASTRO: TDateField;
    qryGridPaiPadraoDATA_INATIVACAO: TDateField;
    qryGridPaiPadraoNOME_FUNCIONARIO: TStringField;
    procedure btnIncluirClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
  protected
    procedure VerificarPermissoes; override;
    function PodeExcluir(msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroUsuarios: TFFiltroUsuarios;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UVariaveisGlobais, UCadastroUsuario, UGDM;

procedure TFFiltroUsuarios.btnEditarClick(Sender: TObject);
begin
  inherited;

  vgOperacao  := E;
  vgIdCosulta := qryGridPaiPadrao.FieldByName('ID').AsInteger;
  BS.CriarForm(TFCadastroUsuario, FCadastroUsuario);
end;

procedure TFFiltroUsuarios.btnExcluirClick(Sender: TObject);
var
  qryExclusao: TFDQuery;
begin
  inherited;

  if lPodeExcluir then
  begin
    qryExclusao := BS.CriarFDQuery(nil, vgConGER);

    with qryExclusao, SQL do
    begin
      Close;
      Clear;
      Text := 'DELETE FROM USUARIO WHERE ID_USUARIO = :iIdUsuario';
      Params.ParamByName('iIdUsuario').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
      ExecSQL;
    end;

    FreeAndNil(qryExclusao);
  end;
end;

procedure TFFiltroUsuarios.btnFiltrarClick(Sender: TObject);
var
  sCPF: String;
begin
  inherited;
  sCPF := '';

  with qryGridPaiPadrao, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT U.ID_USUARIO AS ID, ' +
            '       ' + QuotedStr('') + ' AS NOME_FUNCIONARIO, ' +
            '       U.* ' +
            '  FROM USUARIO U ' +
            ' WHERE U.FLG_SUPORTE = ' + QuotedStr('N');

    if Trim(edtNomeUsu.Text) <> '' then
      Add(' AND NOME LIKE ' + QuotedStr('%' + Trim(edtNomeUsu.Text) + '%'));

    sCPF := StringReplace(StringReplace(Trim(edtNomeUsu.Text), '.', '', [rfReplaceAll, rfIgnoreCase]), '-', '', [rfReplaceAll, rfIgnoreCase]);

    if sCPF <> '' then
      Add(' AND REPLACE(REPLACE(CPF, ' + QuotedStr('.') + ', ' + QuotedStr('') + '), ' + QuotedStr('-') + ', ' + QuotedStr('') + ') = ' + QuotedStr(sCPF));

    if dteCadastroUsu.Date <> 0 then
      Add(' AND DATA_CADASTRO = ' + QuotedStr(Trim(dteCadastroUsu.Text)));

    if Trim(edtMatriculaUsu.Text) <> '' then
      Add(' AND MATRICULA = ' + QuotedStr(Trim(edtMatriculaUsu.Text)));

    if Trim(cbSexoUsu.Text) <> '' then
      Add(' AND FLG_SEXO = ' + QuotedStr(Trim(cbSexoUsu.Text)));

    if Trim(cbFuncionarioUsu.Text) <> '' then
      Add(' AND ID_FUNCIONARIO_FK = ' + IntToStr(cbFuncionarioUsu.ItemIndex + 1));

    if Trim(edtQualificacaoUsu.Text) <> '' then
      Add(' AND QUALIFICACAO LIKE ' + QuotedStr('%' + Trim(edtQualificacaoUsu.Text) + '%'));

    if rgStatusUsu.ItemIndex = 1 then
      Add(' AND FLG_ATIVO = ' + QuotedStr('S'))
    else if rgStatusUsu.ItemIndex = 2 then
      Add(' AND FLG_ATIVO = ' + QuotedStr('N'));

    Add('ORDER BY U.NOME');
    Open;
  end;
end;

procedure TFFiltroUsuarios.btnIncluirClick(Sender: TObject);
begin
  inherited;
  vgOperacao  := I;
  vgIdCosulta := 0;
  BS.CriarForm(TFCadastroUsuario, FCadastroUsuario);
end;

procedure TFFiltroUsuarios.btnLimparClick(Sender: TObject);
begin
  inherited;

  edtNomeUsu.Clear;
  medCPFUsu.Clear;
  dteCadastroUsu.Clear;
  edtMatriculaUsu.Clear;
  cbSexoUsu.ItemIndex := -1;
  cbFuncionarioUsu.ItemIndex := -1;
  edtQualificacaoUsu.Clear;
  rgStatusUsu.ItemIndex := 0;

  if edtNomeUsu.CanFocus then
    edtNomeUsu.SetFocus;
end;

procedure TFFiltroUsuarios.FormCreate(Sender: TObject);
begin
  inherited;

  with qryGridPaiPadrao, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT U.ID_USUARIO AS ID, ' +
            '       ' + QuotedStr('') + ' AS NOME_FUNCIONARIO, ' +
            '       U.* ' +
            '  FROM USUARIO U ' +
            ' WHERE U.FLG_SUPORTE = ' + QuotedStr('N')  +
            'ORDER BY U.NOME';
    Open;
  end;
end;

procedure TFFiltroUsuarios.FormShow(Sender: TObject);
begin
  inherited;

  BS.MontarComboBox(cbFuncionarioUsu, 'NOME_FUNCIONARIO', 'FUNCIONARIO', '', '', 'ID_FUNCIONARIO');
end;

function TFFiltroUsuarios.PodeExcluir(msg: String): Boolean;
var
  QryVerif: TFDQuery;
begin
  inherited;

  Result := True;

  QryVerif := BS.CriarFDQuery(nil, vgConSISTEMA);

  with QryVerif, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT * FROM USUARIO_LOG WHERE ID_USUARIO = :iIdUsuario';
    Params.ParamByName('iIdUsuario').Value := qryGridPaiPadrao.FieldByName('ID').AsInteger;
    Open;
  end;

  if QryVerif.RecordCount > 0 then
    msg := 'N�o � poss�vel excluir o Usu�rio informado, pois o mesmo j� possui hist�rico de a��es no sistema.';

  Result := (QryVerif.RecordCount = 0);

  FreeAndNil(QryVerif);
end;

procedure TFFiltroUsuarios.VerificarPermissoes;
begin
  inherited;
{
  btnIncluir.Enabled  :=  and (qryGridPaiPadrao.RecordCount > 0);
  btnEditar.Enabled   :=  and (qryGridPaiPadrao.RecordCount > 0);
  btnExcluir.Enabled  :=  and (qryGridPaiPadrao.RecordCount > 0);
  btnImprimir.Enabled :=  and (qryGridPaiPadrao.RecordCount > 0);
  btnExportar.Enabled :=  and (qryGridPaiPadrao.RecordCount > 0);
}
end;

end.
