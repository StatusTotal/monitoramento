{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UOpcoesInclusaoOficio.pas
  Descricao:   Tela de Opcoes de Inclusao de Oficio
  Author   :   Cristina
  Date:        23-jan-2017
  Last Update:
------------------------------------------------------------------------------------------------------------------------}

unit UOpcoesInclusaoOficio;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UOpcoesAcaoPadrao, JvExControls,
  JvButton, JvTransparentButton, Vcl.ExtCtrls, System.Actions, Vcl.ActnList,
  Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFOpcoesInclusaoOficio = class(TFOpcoesAcaoPadrao)
    btnOpcao4: TJvTransparentButton;
    btnOpcao3: TJvTransparentButton;
    acmBotoes: TActionManager;
    actIncluirNovoRecebimento: TAction;
    actIncluirNovoEnvio: TAction;
    actIncluirRespostaEnvio: TAction;
    actIncluirRespostaRecebimento: TAction;
    procedure FormShow(Sender: TObject);
    procedure actIncluirNovoEnvioExecute(Sender: TObject);
    procedure actIncluirNovoRecebimentoExecute(Sender: TObject);
    procedure actIncluirRespostaRecebimentoExecute(Sender: TObject);
    procedure actIncluirRespostaEnvioExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    iOp: Integer;
  end;

var
  FOpcoesInclusaoOficio: TFOpcoesInclusaoOficio;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UListaOficios;

procedure TFOpcoesInclusaoOficio.actIncluirNovoEnvioExecute(Sender: TObject);
begin
  inherited;

  iOp := 1;
  Self.Close;
end;

procedure TFOpcoesInclusaoOficio.actIncluirNovoRecebimentoExecute(
  Sender: TObject);
begin
  inherited;

  iOp := 3;
  Self.Close;
end;

procedure TFOpcoesInclusaoOficio.actIncluirRespostaEnvioExecute(
  Sender: TObject);
begin
  inherited;

  iOp := 2;
  vgOfc_TpOfc := 'E';

  dmGerencial.CriarForm(TFListaOficios, FListaOficios);

  vgOfc_TpOfc := '';
  Self.Close;
end;

procedure TFOpcoesInclusaoOficio.actIncluirRespostaRecebimentoExecute(
  Sender: TObject);
begin
  inherited;

  iOp := 4;
  vgOfc_TpOfc := 'R';

  dmGerencial.CriarForm(TFListaOficios, FListaOficios);

  vgOfc_TpOfc := '';
  Self.Close;
end;

procedure TFOpcoesInclusaoOficio.FormShow(Sender: TObject);
var
  QryVerif: TFDQuery;
begin
  inherited;

  iOp := 0;

  actIncluirNovoEnvio.Enabled  := vgPrm_IncOficEnv;  //Oficio Enviado
  actIncluirNovoRecebimento.Enabled  := vgPrm_IncOficRec;  //Oficio Recebido

  QryVerif := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  //Enviados
  QryVerif.Close;
  QryVerif.SQL.Clear;
  QryVerif.SQL.Text := 'SELECT ID_OFICIO ' +
                       '  FROM OFICIO ' +
                       ' WHERE TIPO_OFICIO = ' + QuotedStr('E');
  QryVerif.Open;

  actIncluirRespostaEnvio.Enabled := vgPrm_IncOficEnv and
                                     (QryVerif.RecordCount > 0);  //Oficio Enviado (Resposta)

  //Recebidos
  QryVerif.Close;
  QryVerif.SQL.Clear;
  QryVerif.SQL.Text := 'SELECT ID_OFICIO ' +
                       '  FROM OFICIO ' +
                       ' WHERE TIPO_OFICIO = ' + QuotedStr('R');
  QryVerif.Open;

  actIncluirRespostaRecebimento.Enabled := vgPrm_IncOficRec and
                                           (QryVerif.RecordCount > 0);  //Oficio Recebido (Resposta)

  FreeAndNil(QryVerif);
end;

end.
