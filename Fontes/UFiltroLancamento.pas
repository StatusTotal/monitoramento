{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroLancamento.pas
  Descricao:   Filtro de Lancamentos de Despesas e Receitas
  Author   :   Cristina
  Date:        12-abr-2016
  Last Update: 09-jan-2018 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroLancamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroDuploPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, JvExExtCtrls, JvNetscapeSplitter, System.StrUtils, UDM,
  Vcl.StdCtrls, Vcl.Mask, JvExMask, JvToolEdit, Vcl.DBCtrls, System.DateUtils,
  JvComponentBase, JvDBGridExport, System.Win.ComObj, Vcl.OleCtrls, Vcl.OleCtnrs,
  Vcl.OleServer, ExcelXP, JvRadioGroup, frxClass, frxDBSet;

type
  TFFiltroLancamento = class(TFFiltroDuploPadrao)
    gbLegenda: TGroupBox;
    spLegendaAVencer: TShape;
    lblLegendaAVencer: TLabel;
    lblLegendaSituacaoLanc: TLabel;
    lblLegendaVenceHoje: TLabel;
    spLegendaVenceHoje: TShape;
    lblLegendaVencido: TLabel;
    spLegendaVencido: TShape;
    lblLegendaPago: TLabel;
    spLegendaPago: TShape;
    lblLegendaCancelado: TLabel;
    spLegendaCancelado: TShape;
    dtePeriodoParcIni: TJvDateEdit;
    dtePeriodoParcFim: TJvDateEdit;
    lblPeriodoParc: TLabel;
    Label1: TLabel;
    lblCategoria: TLabel;
    lblSubcategoria: TLabel;
    qryGridFilhoPadraoID_LANCAMENTO_PARC: TIntegerField;
    qryGridFilhoPadraoCOD_LANCAMENTO_FK: TIntegerField;
    qryGridFilhoPadraoANO_LANCAMENTO_FK: TIntegerField;
    qryGridFilhoPadraoNUM_PARCELA: TIntegerField;
    qryGridFilhoPadraoVR_PARCELA_PREV: TBCDField;
    qryGridFilhoPadraoVR_PARCELA_JUROS: TBCDField;
    qryGridFilhoPadraoVR_PARCELA_PAGO: TBCDField;
    qryGridFilhoPadraoFLG_STATUS: TStringField;
    qryGridFilhoPadraoCANCEL_ID_USUARIO: TIntegerField;
    qryGridFilhoPadraoPAG_ID_USUARIO: TIntegerField;
    qryGridFilhoPadraoOBS_LANCAMENTO_PARC: TStringField;
    qryGridFilhoPadraoCAD_ID_USUARIO: TIntegerField;
    rgTipoLancamento: TRadioGroup;
    gbSituacao: TGroupBox;
    chbCanceladoLanc: TCheckBox;
    chbAPagar: TCheckBox;
    chbPaga: TCheckBox;
    qryGridPaiPadraoCOD_LANCAMENTO: TIntegerField;
    qryGridPaiPadraoANO_LANCAMENTO: TIntegerField;
    qryGridPaiPadraoTIPO_LANCAMENTO: TStringField;
    qryGridPaiPadraoID_CATEGORIA_DESPREC_FK: TIntegerField;
    qryGridPaiPadraoID_SUBCATEGORIA_DESPREC_FK: TIntegerField;
    qryGridPaiPadraoFLG_IMPOSTORENDA: TStringField;
    qryGridPaiPadraoFLG_PESSOAL: TStringField;
    qryGridPaiPadraoFLG_AUXILIAR: TStringField;
    qryGridPaiPadraoFLG_REAL: TStringField;
    qryGridPaiPadraoQTD_PARCELAS: TIntegerField;
    qryGridPaiPadraoVR_TOTAL_PREV: TBCDField;
    qryGridPaiPadraoVR_TOTAL_JUROS: TBCDField;
    qryGridPaiPadraoVR_TOTAL_PAGO: TBCDField;
    qryGridPaiPadraoCAD_ID_USUARIO: TIntegerField;
    qryGridPaiPadraoFLG_CANCELADO: TStringField;
    qryGridPaiPadraoCANCEL_ID_USUARIO: TIntegerField;
    qryGridPaiPadraoFLG_RECORRENTE: TStringField;
    qryGridPaiPadraoNUM_RECIBO: TIntegerField;
    qryGridPaiPadraoOBSERVACAO: TStringField;
    qryGridPaiPadraoDESCR_CATEGORIA_DESPREC: TStringField;
    qryGridPaiPadraoDESCR_SUBCATEGORIA_DESPREC: TStringField;
    lcbCategoria: TDBLookupComboBox;
    qryCategoria: TFDQuery;
    dsCategoria: TDataSource;
    dsSubCategoria: TDataSource;
    qrySubCategoria: TFDQuery;
    lcbSubCategoria: TDBLookupComboBox;
    qryGridFilhoPadraoAGENCIA: TStringField;
    qryGridFilhoPadraoCONTA: TStringField;
    qryGridFilhoPadraoID_BANCO_FK: TIntegerField;
    qryGridPaiPadraoFLG_REPLICADO: TStringField;
    lblNatureza: TLabel;
    qryGridPaiPadraoTIPO_CADASTRO: TStringField;
    qryGridPaiPadraoID_ORIGEM: TIntegerField;
    qryGridPaiPadraoID_SISTEMA_FK: TIntegerField;
    qryGridPaiPadraoID_NATUREZA_FK: TIntegerField;
    qryGridPaiPadraoDESCR_NATUREZA: TStringField;
    qryGridPaiPadraoVR_TOTAL_DESCONTO: TBCDField;
    qryGridFilhoPadraoVR_PARCELA_DESCONTO: TBCDField;
    qryGridDetalhe: TFDQuery;
    dsGridDetalhe: TDataSource;
    qryGridDetalheID_LANCAMENTO_DET: TIntegerField;
    qryGridDetalheCOD_LANCAMENTO_FK: TIntegerField;
    qryGridDetalheANO_LANCAMENTO_FK: TIntegerField;
    qryGridDetalheID_ITEM_FK: TIntegerField;
    qryGridDetalheVR_UNITARIO: TBCDField;
    qryGridDetalheQUANTIDADE: TIntegerField;
    qryGridDetalheVR_TOTAL: TBCDField;
    qryGridDetalheSELO_ORIGEM: TStringField;
    qryGridDetalheALEATORIO_ORIGEM: TStringField;
    qryGridDetalheTIPO_COBRANCA: TStringField;
    qryGridDetalheCOD_ADICIONAL: TIntegerField;
    qryGridDetalheEMOLUMENTOS: TBCDField;
    qryGridDetalheFETJ: TBCDField;
    qryGridDetalheFUNDPERJ: TBCDField;
    qryGridDetalheFUNPERJ: TBCDField;
    qryGridDetalheFUNARPEN: TBCDField;
    qryGridDetalhePMCMV: TBCDField;
    qryGridDetalheISS: TBCDField;
    qryGridDetalheFLG_CANCELADO: TStringField;
    qryGridDetalheDESCR_ITEM: TStringField;
    lblNumRecibo: TLabel;
    edtNumRecibo: TEdit;
    pnlGridDetalhe: TPanel;
    dbgGridDetalhe: TDBGrid;
    qryGridPaiPadraoFLG_FLUTUANTE: TStringField;
    gbClassificacao: TGroupBox;
    chbFlgIR: TCheckBox;
    chbFlgLivroAuxiliar: TCheckBox;
    chbFlgContasPessoais: TCheckBox;
    chbFlgFlutuante: TCheckBox;
    qryGridFilhoPadraoNUM_CHEQUE: TStringField;
    cbTipoCobranca: TComboBox;
    lblTipoCobranca: TLabel;
    GroupBox1: TGroupBox;
    lblCodigoLanc: TLabel;
    lblAnoLanc: TLabel;
    edtCodigoLanc: TEdit;
    edtAnoLanc: TEdit;
    edtNumProtocolo: TEdit;
    lblNumProtocolo: TLabel;
    qryGridPaiPadraoID_CLIENTE_FORNECEDOR_FK: TIntegerField;
    lblItem: TLabel;
    qryGridFilhoPadraoID_FORMAPAGAMENTO_FK: TIntegerField;
    qryGridPaiPadraoFLG_STATUS: TStringField;
    qryGridFilhoPadraoDATA_LANCAMENTO_PARC: TDateField;
    qryGridFilhoPadraoDATA_VENCIMENTO: TDateField;
    qryGridFilhoPadraoDATA_CANCELAMENTO: TDateField;
    qryGridFilhoPadraoDATA_PAGAMENTO: TDateField;
    qryGridFilhoPadraoDATA_CADASTRO: TDateField;
    qryGridPaiPadraoDATA_CADASTRO: TSQLTimeStampField;
    qryGridPaiPadraoDATA_CANCELAMENTO: TDateField;
    qryGridPaiPadraoDATA_LANCAMENTO: TDateField;
    qryGridDetalheMUTUA: TBCDField;
    qryGridDetalheACOTERJ: TBCDField;
    qryGridDetalheNUM_PROTOCOLO: TIntegerField;
    qryGridPaiPadraoID_CARNELEAO_EQUIVALENCIA_FK: TIntegerField;
    Panel1: TPanel;
    mmObsLanc: TDBMemo;
    JvNetscapeSplitter1: TJvNetscapeSplitter;
    edtItem: TEdit;
    lblObervacao: TLabel;
    edtNatureza: TEdit;
    chbFlgRecorrente: TCheckBox;
    dbgxExport: TJvDBGridExcelExport;
    lcbFPagto: TDBLookupComboBox;
    lblFPagto: TLabel;
    qryFPagto: TFDQuery;
    dsFPagto: TDataSource;
    edtNomeCliFor: TEdit;
    lblNomeCliFor: TLabel;
    qryGridPaiPadraoNOME_FANTASIA: TStringField;
    rgTipoDataPer: TJvRadioGroup;
    qryGridPaiPadraoFLG_FORAFECHCAIXA: TStringField;
    chbFlgForaFechCaixa: TCheckBox;
    qryGridFilhoPadraoCOD_BANCO: TStringField;
    qryGridFilhoPadraoAG_CONTA: TStringField;
    qryGridFilhoPadraoNUM_DOCUMENTO: TStringField;
    rgOrdenacao: TRadioGroup;
    qryGridFilhoPadraoTP_LANC: TStringField;
    qryGridFilhoPadraoID_FORMAPAGAMENTO_ORIG_FK: TIntegerField;
    qryGridFilhoPadraoDESCR_FORMAPAGAMENTO: TStringField;
    btnRealizarPagamento: TJvTransparentButton;
    btnAlterarNatureza: TJvTransparentButton;
    qryGridFilhoPadraoNUM_COD_DEPOSITO: TStringField;
    procedure btnIncluirClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure dbgGridFilhoPadraoDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtCodigoLancKeyPress(Sender: TObject; var Key: Char);
    procedure edtAnoLancKeyPress(Sender: TObject; var Key: Char);
    procedure dtePeriodoParcIniKeyPress(Sender: TObject; var Key: Char);
    procedure dtePeriodoParcFimKeyPress(Sender: TObject; var Key: Char);
    procedure chbAPagarKeyPress(Sender: TObject; var Key: Char);
    procedure chbPagaKeyPress(Sender: TObject; var Key: Char);
    procedure chbCanceladoLancKeyPress(Sender: TObject; var Key: Char);
    procedure chbAutomaticaKeyPress(Sender: TObject; var Key: Char);
    procedure chbManualKeyPress(Sender: TObject; var Key: Char);
    procedure rgTipoLancamentoClick(Sender: TObject);
    procedure rgTipoLancamentoExit(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure dbgGridPaiPadraoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgGridFilhoPadraoDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure qryGridPaiPadraoAfterScroll(DataSet: TDataSet);
    procedure dbgGridPaiPadraoTitleClick(Column: TColumn);
    procedure dbgGridFilhoPadraoTitleClick(Column: TColumn);
    procedure lcbCategoriaKeyPress(Sender: TObject; var Key: Char);
    procedure lcbSubCategoriaKeyPress(Sender: TObject; var Key: Char);
    procedure qryCategoriaAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure qryGridPaiPadraoVR_TOTAL_PREVGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_TOTAL_JUROSGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_TOTAL_PAGOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridFilhoPadraoVR_PARCELA_PREVGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridFilhoPadraoVR_PARCELA_JUROSGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridFilhoPadraoVR_PARCELA_MULTAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridFilhoPadraoVR_PARCELA_PAGOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure lcbCategoriaExit(Sender: TObject);
    procedure qryGridFilhoPadraoVR_PARCELA_DESCONTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryGridPaiPadraoVR_TOTAL_DESCONTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure dbgGridDetalheDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgGridDetalheDblClick(Sender: TObject);
    procedure edtNumReciboKeyPress(Sender: TObject; var Key: Char);
    procedure dbgGridDetalheTitleClick(Column: TColumn);
    procedure qryGridDetalheVR_UNITARIOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryGridDetalheVR_TOTALGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure chbFlgIRKeyPress(Sender: TObject; var Key: Char);
    procedure chbFlgLivroAuxiliarKeyPress(Sender: TObject; var Key: Char);
    procedure chbFlgContasPessoaisKeyPress(Sender: TObject; var Key: Char);
    procedure chbFlgFlutuanteKeyPress(Sender: TObject; var Key: Char);
    procedure edtNumIdentDepositoKeyPress(Sender: TObject; var Key: Char);
    procedure cbTipoCobrancaKeyPress(Sender: TObject; var Key: Char);
    procedure edtNumProtocoloKeyPress(Sender: TObject; var Key: Char);
    procedure edtItemKeyPress(Sender: TObject; var Key: Char);
    procedure edtNaturezaKeyPress(Sender: TObject; var Key: Char);
    procedure chbFlgRecorrenteKeyPress(Sender: TObject; var Key: Char);
    procedure chbCanceladoLancClick(Sender: TObject);
    procedure chbPagaClick(Sender: TObject);
    procedure chbAPagarClick(Sender: TObject);
    procedure chbFlgRecorrenteClick(Sender: TObject);
    procedure chbFlgFlutuanteClick(Sender: TObject);
    procedure chbFlgContasPessoaisClick(Sender: TObject);
    procedure chbFlgLivroAuxiliarClick(Sender: TObject);
    procedure chbFlgIRClick(Sender: TObject);
    procedure btnExportarClick(Sender: TObject);
    procedure lcbFPagtoKeyPress(Sender: TObject; var Key: Char);
    procedure edtNomeCliForKeyPress(Sender: TObject; var Key: Char);
    procedure rgTipoDataPerExit(Sender: TObject);
    procedure rgTipoDataPerClick(Sender: TObject);
    procedure chbFlgForaFechCaixaClick(Sender: TObject);
    procedure chbFlgForaFechCaixaKeyPress(Sender: TObject; var Key: Char);
    procedure btnImprimirClick(Sender: TObject);
    procedure rgOrdenacaoClick(Sender: TObject);
    procedure qryGridFilhoPadraoCalcFields(DataSet: TDataSet);
    procedure btnRealizarPagamentoClick(Sender: TObject);
    procedure btnAlterarNaturezaClick(Sender: TObject);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }

    procedure AbrirLancamentos;
    procedure MontaQueryLancamentoCodigoAno;
    procedure MontaQueryLancamentoPeriodo;
    procedure AbrirCadastro(SomentePagamento: Boolean);
  public
    { Public declarations }
  end;

var
  FFiltroLancamento: TFFiltroLancamento;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UGDM, UVariaveisGlobais, UDMLancamento,
  UCadastroLancamento, UOpcoesInclusaoLanc, UDMRelatorios,
  UFiltroRelatorioLancamentosF, UPagamentoParcelaLancamento,
  UAlteracaoNaturezaLancamento;

{ TFFiltroLancamento }

procedure TFFiltroLancamento.AbrirCadastro(SomentePagamento: Boolean);
begin
  PegarPosicaoGrid;

  vgOrigemFiltro := True;
  vgLanc_Codigo  := qryGridPaiPadrao.FieldByName('COD_LANCAMENTO').AsInteger;
  vgLanc_Ano     := qryGridPaiPadrao.FieldByName('ANO_LANCAMENTO').AsInteger;
  vgLanc_TpLanc  := qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString;

  try
    Application.CreateForm(TdmLancamento, dmLancamento);
    Application.CreateForm(TFCadastroLancamento, FCadastroLancamento);

    if qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString = 'D' then
      FCadastroLancamento.TipoLanc := DESP
    else
      FCadastroLancamento.TipoLanc := REC;

    FCadastroLancamento.lSomentePagamento := SomentePagamento;

    FCadastroLancamento.ShowModal;
  finally
    FreeAndNil(dmLancamento);
    FCadastroLancamento.Free;
  end;

  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroLancamento.MontaQueryLancamentoCodigoAno;
begin
  //CODIGO e ANO
  qryGridPaiPadrao.SQL.Add(' WHERE L.COD_LANCAMENTO = :COD_LANCAMENTO ' +
                           '   AND L.ANO_LANCAMENTO = :ANO_LANCAMENTO ');

  qryGridPaiPadrao.Params.ParamByName('COD_LANCAMENTO').Value := StrToInt(edtCodigoLanc.Text);
  qryGridPaiPadrao.Params.ParamByName('ANO_LANCAMENTO').Value := StrToInt(edtAnoLanc.Text);
end;

procedure TFFiltroLancamento.MontaQueryLancamentoPeriodo;
begin
  //DATA DE LANCAMENTO
  qryGridPaiPadrao.SQL.Add(' WHERE EXISTS(SELECT LP.ID_LANCAMENTO_PARC ' +
                           '              FROM LANCAMENTO_PARC LP ' +
                           '             WHERE LP.COD_LANCAMENTO_FK = L.COD_LANCAMENTO ' +
                           '               AND LP.ANO_LANCAMENTO_FK = L.ANO_LANCAMENTO ');

  case rgTipoDataPer.ItemIndex of
    0:  //Lancamento
      qryGridPaiPadrao.SQL.Add(' AND LP.DATA_LANCAMENTO_PARC BETWEEN :DATA_INI AND :DATA_FIM)');
    1:  //Vencimento
      qryGridPaiPadrao.SQL.Add(' AND LP.DATA_VENCIMENTO BETWEEN :DATA_INI AND :DATA_FIM)');
    2:  //Pagamento
      qryGridPaiPadrao.SQL.Add(' AND LP.DATA_PAGAMENTO BETWEEN :DATA_INI AND :DATA_FIM)');
  end;

  qryGridPaiPadrao.Params.ParamByName('DATA_INI').Value := dtePeriodoParcIni.Date;
  qryGridPaiPadrao.Params.ParamByName('DATA_FIM').Value := dtePeriodoParcFim.Date;

  if rgTipoLancamento.ItemIndex > 0 then
  begin
    qryGridPaiPadrao.SQL.Add(' AND L.TIPO_LANCAMENTO = :TIPO_LANCAMENTO ');

    if rgTipoLancamento.ItemIndex = 1 then
      qryGridPaiPadrao.Params.ParamByName('TIPO_LANCAMENTO').Value := 'D'
    else
      qryGridPaiPadrao.Params.ParamByName('TIPO_LANCAMENTO').Value := 'R';
  end;

  //RECIBO
  if Trim(edtNumRecibo.Text) <> '' then
  begin
    qryGridPaiPadrao.SQL.Add(' AND L.NUM_RECIBO = :NUM_RECIBO ');
    qryGridPaiPadrao.Params.ParamByName('NUM_RECIBO').Value := Trim(edtNumRecibo.Text);
  end;

  //PROTOCOLO
  if Trim(edtNumProtocolo.Text) <> '' then
  begin
    qryGridPaiPadrao.SQL.Add(' AND EXISTS(SELECT LD.NUM_PROTOCOLO ' +
                             '              FROM LANCAMENTO_DET LD ' +
                             '             WHERE LD.COD_LANCAMENTO_FK = L.COD_LANCAMENTO ' +
                             '               AND LD.ANO_LANCAMENTO_FK = L.ANO_LANCAMENTO ' +
                             '               AND LD.NUM_PROTOCOLO = :NUM_PROTOCOLO)');

    qryGridPaiPadrao.Params.ParamByName('NUM_PROTOCOLO').Value := Trim(edtNumProtocolo.Text);
  end;

  //TIPO DE COBRANCA
  if Trim(cbTipoCobranca.Text) <> '' then
  begin
    qryGridPaiPadrao.SQL.Add(' AND EXISTS(SELECT LD.NUM_PROTOCOLO ' +
                             '              FROM LANCAMENTO_DET LD ' +
                             '             WHERE LD.COD_LANCAMENTO_FK = L.COD_LANCAMENTO ' +
                             '               AND LD.ANO_LANCAMENTO_FK = L.ANO_LANCAMENTO ' +
                             '               AND LD.TIPO_COBRANCA = :TIPO_COBRANCA)');

    qryGridPaiPadrao.Params.ParamByName('TIPO_COBRANCA').Value := Trim(cbTipoCobranca.Text);
  end;

  //ITEM
  if Trim(edtItem.Text) <> '' then
  begin
    qryGridPaiPadrao.SQL.Add(' AND EXISTS(SELECT LD.NUM_PROTOCOLO ' +
                             '              FROM LANCAMENTO_DET LD ' +
                             '             INNER JOIN ITEM I ' +
                             '                ON LD.ID_ITEM_FK = I.ID_ITEM ' +
                             '             WHERE LD.COD_LANCAMENTO_FK = L.COD_LANCAMENTO ' +
                             '               AND LD.ANO_LANCAMENTO_FK = L.ANO_LANCAMENTO ' +
                             '               AND UPPER(I.DESCR_ITEM) LIKE ' + QuotedStr('%' + Trim(UpperCase(edtItem.Text)) + '%') + ')');
  end;

  //CLASSIFICACAO
  //Imposto de Renda
  if chbFlgIR.Checked then
  begin
    qryGridPaiPadrao.SQL.Add(' AND L.FLG_IMPOSTORENDA = :FLG_IMPOSTORENDA ');
    qryGridPaiPadrao.Params.ParamByName('FLG_IMPOSTORENDA').Value := 'S';
  end;

  //Livro Auxiliar
  if chbFlgLivroAuxiliar.Checked then
  begin
    qryGridPaiPadrao.SQL.Add(' AND L.FLG_AUXILIAR = :FLG_AUXILIAR ');
    qryGridPaiPadrao.Params.ParamByName('FLG_AUXILIAR').Value := 'S';
  end;

  //Contas Pessoais
  if chbFlgContasPessoais.Checked then
  begin
    qryGridPaiPadrao.SQL.Add(' AND L.FLG_PESSOAL = :FLG_PESSOAL ');
    qryGridPaiPadrao.Params.ParamByName('FLG_PESSOAL').Value := 'S';
  end;

  //Flutuantes
  if chbFlgFlutuante.Checked then
  begin
    qryGridPaiPadrao.SQL.Add(' AND L.FLG_FLUTUANTE = :FLG_FLUTUANTE ');
    qryGridPaiPadrao.Params.ParamByName('FLG_FLUTUANTE').Value := 'S';
  end;

  //Fora do fechamento de Caixa
  if chbFlgForaFechCaixa.Checked then
  begin
    qryGridPaiPadrao.SQL.Add(' AND L.FLG_FORAFECHCAIXA = :FLG_FORAFECHCAIXA ');
    qryGridPaiPadrao.Params.ParamByName('FLG_FORAFECHCAIXA').Value := 'S';
  end;

  //Recorrentes
  if chbFlgRecorrente.Checked then
  begin
    qryGridPaiPadrao.SQL.Add(' AND L.FLG_RECORRENTE = :FLG_RECORRENTE ');
    qryGridPaiPadrao.Params.ParamByName('FLG_RECORRENTE').Value := 'S';
  end;

  //NATUREZA
  if Trim(edtNatureza.Text) <> '' then
    qryGridPaiPadrao.SQL.Add(' AND UPPER(N.DESCR_NATUREZA) LIKE ' + QuotedStr('%' + Trim(UpperCase(edtNatureza.Text)) + '%'));

  //CATEGORIA
  if Trim(lcbCategoria.Text) <> '' then
  begin
    qryGridPaiPadrao.SQL.Add(' AND L.ID_CATEGORIA_DESPREC_FK = :ID_CATEGORIA_DESPREC ');
    qryGridPaiPadrao.Params.ParamByName('ID_CATEGORIA_DESPREC').Value := lcbCategoria.KeyValue;
  end;

  //SUBCATEGORIA
  if Trim(lcbSubCategoria.Text) <> '' then
  begin
    qryGridPaiPadrao.SQL.Add(' AND L.ID_SUBCATEGORIA_DESPREC_FK = :ID_SUBCATEGORIA_DESPREC ');
    qryGridPaiPadrao.Params.ParamByName('ID_SUBCATEGORIA_DESPREC').Value := lcbSubcategoria.KeyValue;
  end;

  //CLIENTE - FORNECEDOR
  if Trim(edtNomeCliFor.Text) <> '' then
    qryGridPaiPadrao.SQL.Add(' AND UPPER(CF.NOME_FANTASIA) LIKE ' + QuotedStr('%' + Trim(UpperCase(edtNomeCliFor.Text)) + '%'));

  //SITUACAO
  //Cancelada
  if chbCanceladoLanc.Checked then
  begin
    qryGridPaiPadrao.SQL.Add(' AND L.FLG_CANCELADO = :FLG_CANCELADO ');
    qryGridPaiPadrao.Params.ParamByName('FLG_CANCELADO').Value := 'S';
  end
  else
  begin
    //A Pagar
    if chbAPagar.Checked then
    begin
      //A Pagar e Paga
      if chbPaga.Checked then
      begin
        qryGridPaiPadrao.SQL.Add(' AND (L.FLG_STATUS = :FLG_STATUS1 ' +
                                 '  OR  L.FLG_STATUS = :FLG_STATUS2)');

        qryGridPaiPadrao.Params.ParamByName('FLG_STATUS1').Value := 'P';
        qryGridPaiPadrao.Params.ParamByName('FLG_STATUS2').Value := 'G';
      end
      else  //Somente A Pagar
      begin
        qryGridPaiPadrao.SQL.Add(' AND L.FLG_STATUS = :FLG_STATUS ');
        qryGridPaiPadrao.Params.ParamByName('FLG_STATUS').Value := 'P';
      end;
    end
    else
    begin
      //Somente Paga
      if chbPaga.Checked then
      begin
        qryGridPaiPadrao.SQL.Add(' AND L.FLG_STATUS = :FLG_STATUS ');
        qryGridPaiPadrao.Params.ParamByName('FLG_STATUS').Value := 'G';
      end;
    end;

    qryGridPaiPadrao.SQL.Add(' AND L.FLG_CANCELADO = :FLG_CANCELADO ');
    qryGridPaiPadrao.Params.ParamByName('FLG_CANCELADO').Value := 'N';
  end;

  //FORMA DE PAGAMENTO
  if Trim(lcbFPagto.Text) <> '' then
  begin
    qryGridPaiPadrao.SQL.Add(' AND EXISTS(SELECT LP.ID_FORMAPAGAMENTO_FK ' +
                             '              FROM LANCAMENTO_PARC LP ' +
                             '             WHERE LP.COD_LANCAMENTO_FK = L.COD_LANCAMENTO ' +
                             '               AND LP.ANO_LANCAMENTO_FK = L.ANO_LANCAMENTO ' +
                             '               AND LP.ID_FORMAPAGAMENTO_FK = :ID_FORMAPAGAMENTO)');

    qryGridPaiPadrao.Params.ParamByName('ID_FORMAPAGAMENTO').Value := lcbFPagto.KeyValue;
  end;
end;

procedure TFFiltroLancamento.AbrirLancamentos;
var
  sOrdem: String;
begin
  sOrdem := '';

  if (Trim(edtCodigoLanc.Text) = '') and
    (Trim(edtAnoLanc.Text) = '') and
    (dtePeriodoParcIni.Date = 0) and
    (dtePeriodoParcFim.Date > 0) then
  begin
    Application.MessageBox('Selecione pelo menos o Per�odo ou o C�digo e o Ano do Vencimento para realizar a busca.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);

    if dtePeriodoParcIni.CanFocus then
      dtePeriodoParcIni.SetFocus;

    Exit;
  end;

  qryGridPaiPadrao.Close;
  qryGridPaiPadrao.SQL.Clear;

  qryGridPaiPadrao.SQL.Text := 'SELECT DISTINCT L.*, ' +
                               '       C.DESCR_CATEGORIA_DESPREC, ' +
                               '       S.DESCR_SUBCATEGORIA_DESPREC, ' +
                               '       N.DESCR_NATUREZA, ' +
                               '       CF.NOME_FANTASIA ' +
                               '  FROM LANCAMENTO L ' +
                               '  LEFT JOIN CATEGORIA_DESPREC C ' +
                               '    ON L.ID_CATEGORIA_DESPREC_FK = C.ID_CATEGORIA_DESPREC ' +
                               '  LEFT JOIN SUBCATEGORIA_DESPREC S ' +
                               '    ON L.ID_SUBCATEGORIA_DESPREC_FK = S.ID_SUBCATEGORIA_DESPREC ' +
                               '  LEFT JOIN NATUREZA N ' +
                               '    ON L.ID_NATUREZA_FK = N.ID_NATUREZA ' +
                               '  LEFT JOIN CLIENTE_FORNECEDOR CF ' +
                               '    ON L.ID_CLIENTE_FORNECEDOR_FK = CF.ID_CLIENTE_FORNECEDOR';

  if (Trim(edtCodigoLanc.Text) <> '') and
    (Trim(edtAnoLanc.Text) <> '') then
    MontaQueryLancamentoCodigoAno;

  if (dtePeriodoParcIni.Date > 0) and
    (dtePeriodoParcFim.Date > 0) then
    MontaQueryLancamentoPeriodo;

  sOrdem := IfThen((rgOrdenacao.ItemIndex = 0), 'ASC', 'DESC');

  qryGridPaiPadrao.SQL.Add('ORDER BY L.DATA_LANCAMENTO ' + sOrdem + ', ' +
                           '         L.NUM_RECIBO ASC, ' +
                           '         L.COD_LANCAMENTO ' + sOrdem + ', ' +
                           '         L.ANO_LANCAMENTO ' + sOrdem);

  qryGridPaiPadrao.Open;
end;

procedure TFFiltroLancamento.btnAlterarNaturezaClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao       := E;
  vgOrigemCadastro := False;
  vgOrigemFiltro   := True;
  vgLanc_Codigo    := qryGridPaiPadrao.FieldByName('COD_LANCAMENTO').AsInteger;
  vgLanc_Ano       := qryGridPaiPadrao.FieldByName('ANO_LANCAMENTO').AsInteger;
  vgLanc_TpLanc    := qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString;

  dmGerencial.CriarForm(TFAlteracaoNaturezaLancamento, FAlteracaoNaturezaLancamento);

  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroLancamento.btnEditarClick(Sender: TObject);
begin
  inherited;

  if (Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) = 'SAL�RIO') or
    (Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) = 'COMISS�O') or
    (Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) = 'VALE') or
    (Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) = 'OUTROS LAN�AMENTOS COM COLABORADOR') then
  begin
    Application.MessageBox(PChar('Esse Lan�amento pode ser alterado somente na Origem.'),
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
    vgOperacao := C;
  end
  else if (qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString = 'G') and
    (not vgPrm_EdLancPago) then
  begin
    Application.MessageBox(PChar('N�o � poss�vel editar esse Lan�amento, pois o mesmo j� est� PAGO.'),
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
    vgOperacao := C;
  end
  else
    vgOperacao := E;

  AbrirCadastro(False);
end;

procedure TFFiltroLancamento.btnExcluirClick(Sender: TObject);
var
  qryExclusao: TFDQuery;
  sTipo, sNomeUsu, sSenhaUsu, sTexto: String;
  iIdUsu: Integer;
  lCancelado: Boolean;
begin
  inherited;

  lCancelado := False;

  sTipo := IfThen((qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString = 'D'), 'Despesa', 'Receita');

  if lPodeExcluir then
  begin
    if (qryGridPaiPadrao.FieldByName('TIPO_CADASTRO').AsString = 'A') or
      (qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString = 'G') then  //Origem Automatica
    begin
      sTexto := '';

      if qryGridPaiPadrao.FieldByName('TIPO_CADASTRO').AsString = 'A' then
        sTexto := ' foi gerada automaticamente ';

      if qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString = 'G' then
        sTexto := ' j� se encontra paga ';

      if Application.MessageBox(PChar('Essa ' + sTipo + sTexto + 'e ser� preciso informar LOGIN ' +
                                      'e SENHA de Autorizador para exclu�-la. ' + #13#10 + 'Deseja prosseguir?'), 'Aviso', MB_YESNO + MB_ICONQUESTION) = ID_YES then
      begin
        iIdUsu    := 0;
        sNomeUsu  := '';
        sSenhaUsu := '';

        repeat
          repeat
            if not InputQuery('LOGIN', 'Por favor, informe o LOGIN do Autorizador:', sNomeUsu) then
              lCancelado := True;
          until (Trim(sNomeUsu) <> '') or lCancelado;

          if not lCancelado then
          begin
            repeat
              if not InputQuery('SENHA', #31'Por favor, informe a SENHA do Autorizador:', sSenhaUsu) then
                lCancelado := True;
            until (Trim(sSenhaUsu) <> '') or lCancelado;
          end;
        until (dmGerencial.VerificarAutorizacao(iIdUsu, sNomeUsu, sSenhaUsu) = True) or lCancelado;
      end
      else
        lCancelado := True;
    end;

    if not lCancelado then
    begin
      qryExclusao := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

      try
        if vgConSISTEMA.Connected then
          vgConSISTEMA.StartTransaction;

        //Lancamento
        with qryExclusao, SQL do
        begin
          Close;
          Clear;

          Text := 'UPDATE LANCAMENTO ' +
                  '   SET FLG_CANCELADO     = :FLG_CANCELADO, ' +
                  '       DATA_CANCELAMENTO = :DATA_CANCELAMENTO, ' +
                  '       CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO ' +
                  ' WHERE COD_LANCAMENTO  = :COD_LANCAMENTO ' +
                  '   AND ANO_LANCAMENTO  = :ANO_LANCAMENTO ' +
                  '   AND TIPO_LANCAMENTO = :TIPO_LANCAMENTO';

          Params.ParamByName('FLG_CANCELADO').Value      := 'S';
          Params.ParamByName('DATA_CANCELAMENTO').Value  := Now;
          Params.ParamByName('CANCEL_ID_USUARIO').Value  := vgUsu_Id;
          Params.ParamByName('COD_LANCAMENTO').Value     := qryGridPaiPadrao.FieldByName('COD_LANCAMENTO').AsInteger;
          Params.ParamByName('ANO_LANCAMENTO').Value     := qryGridPaiPadrao.FieldByName('ANO_LANCAMENTO').AsInteger;
          Params.ParamByName('TIPO_LANCAMENTO').Value    := qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString;
          ExecSQL;
        end;

        //Detalhe
        with qryExclusao, SQL do
        begin
          Close;
          Clear;

          Text := 'UPDATE LANCAMENTO_DET ' +
                  '   SET FLG_CANCELADO = :FLG_CANCELADO ' +
                  ' WHERE COD_LANCAMENTO_FK  = :COD_LANCAMENTO ' +
                  '   AND ANO_LANCAMENTO_FK  = :ANO_LANCAMENTO';

          Params.ParamByName('FLG_CANCELADO').Value  := 'S';
          Params.ParamByName('COD_LANCAMENTO').Value := qryGridPaiPadrao.FieldByName('COD_LANCAMENTO').AsInteger;
          Params.ParamByName('ANO_LANCAMENTO').Value := qryGridPaiPadrao.FieldByName('ANO_LANCAMENTO').AsInteger;
          ExecSQL;
        end;

        //Parcela
        with qryExclusao, SQL do
        begin
          Close;
          Clear;

          Text := 'UPDATE LANCAMENTO_PARC ' +
                  '   SET FLG_STATUS         = :FLG_STATUS, ' +
                  '       DATA_CANCELAMENTO  = :DATA_CANCELAMENTO, ' +
                  '       CANCEL_ID_USUARIO  = :CANCEL_ID_USUARIO ' +
                  ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                  '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO';

          Params.ParamByName('FLG_STATUS').Value        := 'C';
          Params.ParamByName('DATA_CANCELAMENTO').Value := Now;
          Params.ParamByName('CANCEL_ID_USUARIO').Value := vgUsu_Id;
          Params.ParamByName('COD_LANCAMENTO').Value    := qryGridPaiPadrao.FieldByName('COD_LANCAMENTO').AsInteger;
          Params.ParamByName('ANO_LANCAMENTO').Value    := qryGridPaiPadrao.FieldByName('ANO_LANCAMENTO').AsInteger;
          ExecSQL;
        end;

        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Commit;

        GravarLog;
        Application.MessageBox(PChar(Trim(sTipo) + ' exclu�da com sucesso!'), 'Aviso', MB_OK)
      except
        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Rollback;

        Application.MessageBox(PChar('Erro na exclus�o da ' + Trim(sTipo) + '.'), 'Erro', MB_OK + MB_ICONERROR)
      end;

      FreeAndNil(qryExclusao);

      btnFiltrar.Click;
    end;
  end;
end;

procedure TFFiltroLancamento.btnExportarClick(Sender: TObject);
begin
  inherited;

  vgOperacao := X;

  try
    dbgxExport.Grid        := dbgGridPaiPadrao;
    dbgxExport.FileName    := vgConf_DiretorioRelatorios +
                              IntToStr(YearOf(Now)) + '\' +
                              'LstLancs_' +
                              dmGerencial.PegarNumeroTexto(dtePeriodoParcIni.Text) + '_a_' +
                              dmGerencial.PegarNumeroTexto(dtePeriodoParcFim.Text);
    dbgxExport.Orientation := woLandscape;
    dbgxExport.AutoFit     := True;
    dbgxExport.Silent      := False;
    dbgxExport.Visible     := True;
    dbgxExport.Close       := scNever;
    dbgxExport.ExportGrid;

    Application.MessageBox('Lista exportada com sucesso!',
                           'Sucesso',
                           MB_OK);
  except
    //
  end;

  vgOperacao := VAZIO;
end;

procedure TFFiltroLancamento.btnFiltrarClick(Sender: TObject);
begin
  inherited;

  AbrirLancamentos;

  if dbgGridPaiPadrao.CanFocus then
    dbgGridPaiPadrao.SetFocus;
end;

procedure TFFiltroLancamento.btnImprimirClick(Sender: TObject);
var
  sListaLancs: String;
  iPos: Integer;
begin
  inherited;

  sListaLancs := '';

  qryGridPaiPadrao.DisableControls;

  iPos := qryGridPaiPadrao.RecNo;

  qryGridPaiPadrao.First;

  while not qryGridPaiPadrao.Eof do
  begin
    if Trim(sListaLancs) = '' then
      sListaLancs := QuotedStr(qryGridPaiPadrao.FieldByName('COD_LANCAMENTO').AsString + '/' +
                               qryGridPaiPadrao.FieldByName('ANO_LANCAMENTO').AsString)
    else
      sListaLancs := sListaLancs + ', ' + QuotedStr(qryGridPaiPadrao.FieldByName('COD_LANCAMENTO').AsString + '/' +
                                                    qryGridPaiPadrao.FieldByName('ANO_LANCAMENTO').AsString);

    qryGridPaiPadrao.Next;
  end;

  qryGridPaiPadrao.EnableControls;

  qryGridPaiPadrao.RecNo := iPos;

  vgOperacao := P;
  vgOrigemCadastro := False;

  Application.CreateForm(TdmRelatorios, dmRelatorios);

  try
    Application.CreateForm(TFFiltroRelatorioLancamentosF, FFiltroRelatorioLancamentosF);
    FFiltroRelatorioLancamentosF.dDataInicio  := dtePeriodoParcIni.Date;
    FFiltroRelatorioLancamentosF.dDataFim     := dtePeriodoParcFim.Date;

    FFiltroRelatorioLancamentosF.sLstLancamentos := sListaLancs;

     case rgTipoLancamento.ItemIndex of
      0:
      begin
        FFiltroRelatorioLancamentosF.sTipoLanc1 := 'R';
        FFiltroRelatorioLancamentosF.sTipoLanc2 := 'D';
      end;
      1:
      begin
        FFiltroRelatorioLancamentosF.sTipoLanc1 := 'D';
        FFiltroRelatorioLancamentosF.sTipoLanc2 := 'D';
      end;
      2:
      begin
        FFiltroRelatorioLancamentosF.sTipoLanc1 := 'R';
        FFiltroRelatorioLancamentosF.sTipoLanc2 := 'R';
      end;
    end;

    FFiltroRelatorioLancamentosF.ShowModal;
  finally
    FFiltroRelatorioLancamentosF.Free;
  end;

  FreeAndNil(dmRelatorios);

  qryGridPaiPadrao.EnableControls;
  qryGridDetalhe.EnableControls;
  qryGridFilhoPadrao.EnableControls;

  qryGridPaiPadrao.First;
end;

procedure TFFiltroLancamento.btnIncluirClick(Sender: TObject);
begin
  inherited;

  dmGerencial.CriarForm(TFOpcoesInclusaoLanc, FOpcoesInclusaoLanc);
  btnFiltrar.Click;
end;

procedure TFFiltroLancamento.btnLimparClick(Sender: TObject);
begin
  inherited;

  rgTipoLancamento.ItemIndex := 0;

  dtePeriodoParcIni.Date := StartOfTheMonth(Date);
  dtePeriodoParcFim.Date := Date;

  edtNumRecibo.Clear;
  edtNumProtocolo.Clear;
  cbTipoCobranca.ItemIndex     := -1;
  chbFlgIR.Checked             := False;
  chbFlgLivroAuxiliar.Checked  := False;
  chbFlgContasPessoais.Checked := False;
  chbFlgFlutuante.Checked      := False;
  chbFlgForaFechCaixa.Checked  := False;

  edtItem.Clear;
  edtNatureza.Clear;
  lcbCategoria.KeyValue    := Null;
  lcbSubcategoria.KeyValue := Null;
  lcbSubCategoria.Enabled  := False;
  chbAPagar.Checked        := True;
  chbPaga.Checked          := True;
  chbCanceladoLanc.Checked := False;

  lcbFPagto.KeyValue := -1;

  chbFlgRecorrente.Checked := False;

  rgOrdenacao.ItemIndex := 1;

  edtCodigoLanc.Clear;
  edtAnoLanc.Clear;

  btnFiltrar.Click;
end;

procedure TFFiltroLancamento.btnRealizarPagamentoClick(Sender: TObject);
begin
  inherited;

  if (Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) = 'SAL�RIO') or
    (Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) = 'COMISS�O') or
    (Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) = 'VALE') or
    (Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) = 'OUTROS LAN�AMENTOS COM COLABORADOR') then
    Application.MessageBox(PChar('Esse Lan�amento pode ser alterado somente na Origem.'),
                           'Aviso',
                           MB_OK + MB_ICONWARNING)
  else
  begin
    PegarPosicaoGrid;

    vgOperacao       := E;
    vgOrigemCadastro := False;
    vgLanc_Codigo    := qryGridPaiPadrao.FieldByName('COD_LANCAMENTO').AsInteger;
    vgLanc_Ano       := qryGridPaiPadrao.FieldByName('ANO_LANCAMENTO').AsInteger;
    vgLanc_TpLanc    := qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString;

    try
      Application.CreateForm(TFPagamentoParcelaLancamento, FPagamentoParcelaLancamento);

      FPagamentoParcelaLancamento.IdParcela := qryGridFilhoPadrao.FieldByName('ID_LANCAMENTO_PARC').AsInteger;

      FPagamentoParcelaLancamento.ShowModal;
    finally
      FPagamentoParcelaLancamento.Free;
    end;

    btnFiltrar.Click;

    DefinirPosicaoGrid;
  end;
end;

procedure TFFiltroLancamento.cbTipoCobrancaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLancamento.chbAPagarClick(Sender: TObject);
begin
  inherited;

  chbCanceladoLanc.Checked := False;

  btnFiltrar.Click;
end;

procedure TFFiltroLancamento.chbAPagarKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLancamento.chbAutomaticaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLancamento.chbCanceladoLancClick(Sender: TObject);
begin
  inherited;

  chbAPagar.Checked := False;
  chbPaga.Checked   := False;

  btnFiltrar.Click;
end;

procedure TFFiltroLancamento.chbCanceladoLancKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLancamento.chbFlgContasPessoaisClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroLancamento.chbFlgContasPessoaisKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLancamento.chbFlgFlutuanteClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroLancamento.chbFlgFlutuanteKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLancamento.chbFlgIRClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroLancamento.chbFlgIRKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLancamento.chbFlgLivroAuxiliarClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroLancamento.chbFlgLivroAuxiliarKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLancamento.chbManualKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLancamento.chbPagaClick(Sender: TObject);
begin
  inherited;

  chbCanceladoLanc.Checked := False;

  btnFiltrar.Click;
end;

procedure TFFiltroLancamento.chbPagaKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLancamento.chbFlgForaFechCaixaClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroLancamento.chbFlgForaFechCaixaKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLancamento.chbFlgRecorrenteClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroLancamento.chbFlgRecorrenteKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLancamento.dbgGridDetalheDblClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao    := C;
  vgLanc_Codigo := qryGridPaiPadrao.FieldByName('COD_LANCAMENTO').AsInteger;
  vgLanc_Ano    := qryGridPaiPadrao.FieldByName('ANO_LANCAMENTO').AsInteger;
  vgLanc_TpLanc := qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString;

  try
    Application.CreateForm(TdmLancamento, dmLancamento);
    Application.CreateForm(TFCadastroLancamento, FCadastroLancamento);

    if qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString = 'D' then
      FCadastroLancamento.TipoLanc := DESP
    else
      FCadastroLancamento.TipoLanc := REC;

    FCadastroLancamento.ShowModal;
  finally
    FreeAndNil(dmLancamento);
    FCadastroLancamento.Free;
  end;

  DefinirPosicaoGrid;
end;

procedure TFFiltroLancamento.dbgGridDetalheDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Grid: TDBGrid;
  Row: Integer;
begin
  inherited;

  Grid := Sender as TDBGrid;
  Row  := Grid.DataSource.DataSet.RecNo;

  if gdSelected in State then
    Grid.Canvas.Brush.Color := clSkyBlue
  else
  begin
    if Odd(Row) then
      Grid.Canvas.Brush.Color := $FFFFF0
    else
      Grid.Canvas.Brush.Color := $D3D3D3;
  end;

  Grid.DefaultDrawColumnCell(Rect, DataCol, Column, State);

  if qryGridDetalhe.FieldByName('FLG_CANCELADO').AsString = 'S' then
  begin
    dbgGridDetalhe.Canvas.Font.Color := clGray;
    dbgGridDetalhe.Canvas.FillRect(Rect);
    dbgGridDetalhe.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TFFiltroLancamento.dbgGridDetalheTitleClick(Column: TColumn);
begin
  inherited;

  qryGridDetalhe.IndexFieldNames := Column.FieldName;
end;

procedure TFFiltroLancamento.dbgGridFilhoPadraoDblClick(Sender: TObject);
begin
  inherited;

  vgOperacao := C;
  AbrirCadastro(False);
end;

procedure TFFiltroLancamento.dbgGridFilhoPadraoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Grid: TDBGrid;
  Row: Integer;
begin
  inherited;

  Grid := Sender as TDBGrid;
  Row  := Grid.DataSource.DataSet.RecNo;

  if gdSelected in State then
    Grid.Canvas.Brush.Color := clSkyBlue
  else
  begin
    if Odd(Row) then
      Grid.Canvas.Brush.Color := $FFFFF0
    else
      Grid.Canvas.Brush.Color := $D3D3D3;
  end;

  Grid.DefaultDrawColumnCell(Rect, DataCol, Column, State);

  //Legenda
  if qryGridFilhoPadrao.FieldByName('FLG_STATUS').AsString = 'C' then  //Cancelada
  begin
    dbgGridFilhoPadrao.Canvas.Font.Color := clGray;
    dbgGridFilhoPadrao.Canvas.FillRect(Rect);
    dbgGridFilhoPadrao.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end
  else if qryGridFilhoPadrao.FieldByName('FLG_STATUS').AsString = 'P' then  //Pendente (A Pagar)
  begin
    if qryGridFilhoPadrao.FieldByName('DATA_VENCIMENTO').AsDateTime > Date then  //A Vencer
    begin
      dbgGridFilhoPadrao.Canvas.Font.Color := clBlack;
      dbgGridFilhoPadrao.Canvas.FillRect(Rect);
      dbgGridFilhoPadrao.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end
    else if qryGridFilhoPadrao.FieldByName('DATA_VENCIMENTO').AsDateTime = Date then  //Vence Hoje
    begin
      dbgGridFilhoPadrao.Canvas.Font.Color := clGreen;
      dbgGridFilhoPadrao.Canvas.Font.Style := [fsBold];
      dbgGridFilhoPadrao.Canvas.FillRect(Rect);
      dbgGridFilhoPadrao.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end
    else if qryGridFilhoPadrao.FieldByName('DATA_VENCIMENTO').AsDateTime < Date then  //Vencido
    begin
      dbgGridFilhoPadrao.Canvas.Font.Color := clMaroon;
      dbgGridFilhoPadrao.Canvas.Font.Style := [fsBold];
      dbgGridFilhoPadrao.Canvas.FillRect(Rect);
      dbgGridFilhoPadrao.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;
  end
  else if qryGridFilhoPadrao.FieldByName('FLG_STATUS').AsString = 'G' then  //Paga
  begin
    dbgGridFilhoPadrao.Canvas.Font.Color := clNavy;
    dbgGridFilhoPadrao.Canvas.FillRect(Rect);
    dbgGridFilhoPadrao.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TFFiltroLancamento.dbgGridFilhoPadraoTitleClick(Column: TColumn);
begin
  inherited;

  qryGridFilhoPadrao.IndexFieldNames := Column.FieldName;
end;

procedure TFFiltroLancamento.dbgGridPaiPadraoDblClick(Sender: TObject);
begin
  inherited;

  vgOperacao := C;
  AbrirCadastro(False);
end;

procedure TFFiltroLancamento.dbgGridPaiPadraoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Grid: TDBGrid;
  Row: Integer;
begin
  inherited;

  Grid := Sender as TDBGrid;
  Row  := Grid.DataSource.DataSet.RecNo;

  if gdSelected in State then
    Grid.Canvas.Brush.Color := clSkyBlue
  else
  begin
    if qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString = 'R' then
      Grid.Canvas.Brush.Color := $00DFFFDF
    else if qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString = 'D' then
      Grid.Canvas.Brush.Color := $00DCD9FF;
  end;

  Grid.DefaultDrawColumnCell(Rect, DataCol, Column, State);

  //Legenda
  if qryGridPaiPadrao.FieldByName('FLG_CANCELADO').AsString = 'S' then
  begin
    dbgGridPaiPadrao.Canvas.Font.Color := clGray;
    dbgGridPaiPadrao.Canvas.FillRect(Rect);
    dbgGridPaiPadrao.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end
  else if qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString = 'P' then  //Pendente (A Pagar)
  begin
    dbgGridPaiPadrao.Canvas.Font.Color := clBlack;
    dbgGridPaiPadrao.Canvas.FillRect(Rect);
    dbgGridPaiPadrao.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end
  else if qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString = 'G' then  //Paga
  begin
    dbgGridPaiPadrao.Canvas.Font.Color := clNavy;
    dbgGridPaiPadrao.Canvas.FillRect(Rect);
    dbgGridPaiPadrao.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TFFiltroLancamento.dbgGridPaiPadraoTitleClick(Column: TColumn);
begin
  inherited;

  qryGridPaiPadrao.IndexFieldNames := Column.FieldName;
end;

procedure TFFiltroLancamento.lcbFPagtoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLancamento.dtePeriodoParcFimKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    if dtePeriodoParcFim.Date = 0 then
    begin
      dtePeriodoParcIni.Date := 0;
      SelectNext(ActiveControl, True, True);
    end
    else
    begin
      if dtePeriodoParcIni.Date = 0 then
        dtePeriodoParcIni.Date := dtePeriodoParcFim.Date
      else if dtePeriodoParcFim.Date < dtePeriodoParcIni.Date then
        dtePeriodoParcFim.Date := dtePeriodoParcIni.Date;

      edtCodigoLanc.Clear;
      edtAnoLanc.Clear;

      SelectNext(ActiveControl, True, True);
    end;

    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dtePeriodoParcFim.Date = 0 then
    begin
      dtePeriodoParcIni.Date := 0;
      SelectNext(ActiveControl, True, True);
    end
    else
    begin
      if dtePeriodoParcIni.Date = 0 then
        dtePeriodoParcIni.Date := dtePeriodoParcFim.Date
      else if dtePeriodoParcFim.Date < dtePeriodoParcIni.Date then
        dtePeriodoParcFim.Date := dtePeriodoParcIni.Date;

      edtCodigoLanc.Clear;
      edtAnoLanc.Clear;

      btnFiltrar.Click;
    end;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroLancamento.dtePeriodoParcIniKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    if dtePeriodoParcIni.Date = 0 then
      dtePeriodoParcFim.Date := 0
    else
    begin
      if (dtePeriodoParcFim.Date = 0) or
        (dtePeriodoParcFim.Date < dtePeriodoParcIni.Date) then
        dtePeriodoParcFim.Date := dtePeriodoParcIni.Date;

      edtCodigoLanc.Clear;
      edtAnoLanc.Clear;
    end;

    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dtePeriodoParcIni.Date = 0 then
    begin
      dtePeriodoParcFim.Date := 0;
      SelectNext(ActiveControl, True, True);
    end
    else
    begin
      if (dtePeriodoParcFim.Date = 0) or
        (dtePeriodoParcFim.Date < dtePeriodoParcIni.Date) then
        dtePeriodoParcFim.Date := dtePeriodoParcIni.Date;

      edtCodigoLanc.Clear;
      edtAnoLanc.Clear;

      btnFiltrar.Click;
    end;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroLancamento.edtAnoLancKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    if Trim(edtCodigoLanc.Text) = '' then
      edtAnoLanc.Clear
    else
    begin
      rgTipoLancamento.ItemIndex := 0;
      dtePeriodoParcIni.Date     := 0;
      dtePeriodoParcFim.Date     := 0;
      edtNumRecibo.Clear;
      edtNumProtocolo.Clear;
      cbTipoCobranca.ItemIndex     := -1;
      chbFlgIR.Checked             := False;
      chbFlgLivroAuxiliar.Checked  := False;
      chbFlgContasPessoais.Checked := False;
      chbFlgFlutuante.Checked      := False;
      chbFlgForaFechCaixa.Checked  := False;

      edtItem.Clear;
      edtNatureza.Clear;
      lcbCategoria.KeyValue    := Null;
      lcbSubcategoria.KeyValue := Null;
      lcbSubCategoria.Enabled  := False;
      chbAPagar.Checked        := True;
      chbPaga.Checked          := True;
      chbCanceladoLanc.Checked := False;
    end;

    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if Trim(edtCodigoLanc.Text) = '' then
    begin
      edtAnoLanc.Clear;
      SelectNext(ActiveControl, True, True);
    end
    else
    begin
      rgTipoLancamento.ItemIndex := 0;
      dtePeriodoParcIni.Date     := 0;
      dtePeriodoParcFim.Date     := 0;
      edtNumRecibo.Clear;
      edtNumProtocolo.Clear;
      cbTipoCobranca.ItemIndex     := -1;
      chbFlgIR.Checked             := False;
      chbFlgLivroAuxiliar.Checked  := False;
      chbFlgContasPessoais.Checked := False;
      chbFlgFlutuante.Checked      := False;
      chbFlgForaFechCaixa.Checked  := False;

      edtItem.Clear;
      edtNatureza.Clear;
      lcbCategoria.KeyValue    := Null;
      lcbSubcategoria.KeyValue := Null;
      lcbSubCategoria.Enabled  := False;
      chbAPagar.Checked        := True;
      chbPaga.Checked          := True;
      chbCanceladoLanc.Checked := False;

      if Trim(edtAnoLanc.Text) = '' then
        SelectNext(ActiveControl, True, True)
      else
        btnFiltrar.Click;
    end;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroLancamento.edtCodigoLancKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    if Trim(edtCodigoLanc.Text) = '' then
      edtAnoLanc.Clear
    else
    begin
      rgTipoLancamento.ItemIndex := 0;
      dtePeriodoParcIni.Date     := 0;
      dtePeriodoParcFim.Date     := 0;
      edtNumRecibo.Clear;
      edtNumProtocolo.Clear;
      cbTipoCobranca.ItemIndex     := -1;
      chbFlgIR.Checked             := False;
      chbFlgLivroAuxiliar.Checked  := False;
      chbFlgContasPessoais.Checked := False;
      chbFlgFlutuante.Checked      := False;
      chbFlgForaFechCaixa.Checked  := False;

      edtItem.Clear;
      edtNatureza.Clear;
      lcbCategoria.KeyValue    := Null;
      lcbSubcategoria.KeyValue := Null;
      lcbSubCategoria.Enabled  := False;
      chbAPagar.Checked        := True;
      chbPaga.Checked          := True;
      chbCanceladoLanc.Checked := False;
    end;

    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if Trim(edtCodigoLanc.Text) = '' then
    begin
      edtAnoLanc.Clear;
      SelectNext(ActiveControl, True, True);
    end
    else
    begin
      rgTipoLancamento.ItemIndex := 0;
      dtePeriodoParcIni.Date     := 0;
      dtePeriodoParcFim.Date     := 0;
      edtNumRecibo.Clear;
      edtNumProtocolo.Clear;
      cbTipoCobranca.ItemIndex     := -1;
      chbFlgIR.Checked             := False;
      chbFlgLivroAuxiliar.Checked  := False;
      chbFlgContasPessoais.Checked := False;
      chbFlgFlutuante.Checked      := False;
      chbFlgForaFechCaixa.Checked  := False;

      edtItem.Clear;
      edtNatureza.Clear;
      lcbCategoria.KeyValue    := Null;
      lcbSubcategoria.KeyValue := Null;
      lcbSubCategoria.Enabled  := False;
      chbAPagar.Checked        := True;
      chbPaga.Checked          := True;
      chbCanceladoLanc.Checked := False;

      if Trim(edtAnoLanc.Text) = '' then
        SelectNext(ActiveControl, True, True)
      else
        btnFiltrar.Click;
    end;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroLancamento.edtItemKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLancamento.edtNaturezaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLancamento.edtNomeCliForKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLancamento.edtNumIdentDepositoKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLancamento.edtNumProtocoloKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLancamento.edtNumReciboKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLancamento.FormCreate(Sender: TObject);
begin
  inherited;

  { Despesas Recorrentes }
  dmPrincipal.AtualizarLancamentosColaboradorRecorrentes;
  dmPrincipal.AtualizarLancamentosRecorrentes;

  qryCategoria.Close;
  qryCategoria.Open;

  qrySubCategoria.Close;
  qrySubCategoria.Params.ParamByName('ID_CATEGORIA_DESPREC').Value := qryCategoria.FieldByName('ID_CATEGORIA_DESPREC').AsInteger;
  qrySubCategoria.Params.ParamByName('TIPO').Value                 := qryCategoria.FieldByName('TIPO').AsString;
  qrySubCategoria.Open;

  qryFPagto.Close;
  qryFPagto.Open;
end;

procedure TFFiltroLancamento.FormShow(Sender: TObject);
begin
  rgTipoLancamento.ItemIndex := 0;

  dtePeriodoParcIni.Date := StartOfTheMonth(Date);
  dtePeriodoParcFim.Date := Date;

  edtNumRecibo.Clear;
  edtNumProtocolo.Clear;
  cbTipoCobranca.ItemIndex     := -1;
  chbFlgIR.Checked             := False;
  chbFlgLivroAuxiliar.Checked  := False;
  chbFlgContasPessoais.Checked := False;
  chbFlgFlutuante.Checked      := False;
  chbFlgForaFechCaixa.Checked  := False;

  edtItem.Clear;
  edtNatureza.Clear;
  lcbCategoria.KeyValue    := Null;
  lcbSubcategoria.KeyValue := Null;
  lcbSubCategoria.Enabled  := False;
  chbAPagar.Checked        := True;
  chbPaga.Checked          := True;
  chbCanceladoLanc.Checked := False;

  edtCodigoLanc.Clear;
  edtAnoLanc.Clear;

  mmObsLanc.ReadOnly := True;

  AbrirLancamentos;

  inherited;

  ShowScrollBar(dbgGridDetalhe.Handle, SB_HORZ, False);

  if dtePeriodoParcIni.CanFocus then
    dtePeriodoParcIni.SetFocus;
end;

procedure TFFiltroLancamento.GravarLog;
var
  sObservacao, sTipo: String;
begin
  inherited;

  sObservacao := '';

  sTipo := IfThen((qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString = 'D'), 'Despesa', 'Receita');

  case vgOperacao of
    X:  //EXCLUSAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da exclus�o dessa ' + sTipo + ':', '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(2, '', sObservacao);
    end;
    P:  //IMPRESSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente impress�o de Lista de ' + sTipo);
    end;
    T:  //EXPORTACAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente exporta��o de Lista de ' + sTipo);
    end;
  end;
end;

procedure TFFiltroLancamento.lcbCategoriaExit(Sender: TObject);
begin
  inherited;

  qrySubCategoria.Close;
  qrySubCategoria.Params.ParamByName('ID_CATEGORIA_DESPREC').Value := qryCategoria.FieldByName('ID_CATEGORIA_DESPREC').AsInteger;
  qrySubCategoria.Params.ParamByName('TIPO').Value                 := qryCategoria.FieldByName('TIPO').AsString;
  qrySubCategoria.Open;

  if qrySubCategoria.RecordCount > 0 then
    lcbSubCategoria.Enabled := True;
end;

procedure TFFiltroLancamento.lcbCategoriaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLancamento.lcbSubCategoriaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

function TFFiltroLancamento.PodeExcluir(var Msg: String): Boolean;
var
  QryVerif: TFDQuery;
begin
  inherited;

  Result := (qryGridPaiPadrao.RecordCount > 0);

  if (Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) = 'SAL�RIO') or
    (Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) = 'COMISS�O') or
    (Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) = 'VALE') or
    (Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) = 'FIRMAS ANTIGO (MOVIMENTO)') or
    (Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) = 'FIRMAS NOVO (SERVICOS)') or
    (Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) = 'RECIBO (PAGAMENTOS)') or
    (Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) = 'OUTROS LAN�AMENTOS COM COLABORADOR') then
  begin
    if qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString = 'D' then
      Msg := 'A Despesa informada somente pode ser alterada/cancelada na Origem.'
    else
      Msg := 'A Receita informada somente pode ser alterada/cancelada na Origem.';

    Result := False;
  end
  else
  begin
    QryVerif := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

    QryVerif.Close;
    QryVerif.SQL.Clear;
    QryVerif.SQL.Text := 'SELECT ID_BAIXA_LANC_DERIVADO ' +
                         '  FROM BAIXA_LANC_DERIVADO ' +
                         ' WHERE DERIV_COD_LANCAMENTO_FK = ' + qryGridPaiPadrao.FieldByName('COD_LANCAMENTO').AsString +
                         '   AND DERIV_ANO_LANCAMENTO_FK = ' + qryGridPaiPadrao.FieldByName('ANO_LANCAMENTO').AsString;
    QryVerif.Open;

    if QryVerif.RecordCount > 0 then
    begin
      if qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString = 'D' then
        Msg := 'A Despesa informada n�o pode ser cancelada pois foi gerada de uma Baixa de Flutuante.'
      else
        Msg := 'A Receita informada n�o pode ser cancelada pois foi gerada de uma Baixa de Flutuante.';
    end;

    Result := (QryVerif.RecordCount = 0);

    FreeAndNil(QryVerif);
  end;
end;

procedure TFFiltroLancamento.qryCategoriaAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qrySubCategoria.Close;
  qrySubCategoria.Params.ParamByName('ID_CATEGORIA_DESPREC').Value := qryCategoria.FieldByName('ID_CATEGORIA_DESPREC').AsInteger;
  qrySubCategoria.Params.ParamByName('TIPO').Value                 := qryCategoria.FieldByName('TIPO').AsString;
  qrySubCategoria.Open;
end;

procedure TFFiltroLancamento.qryGridDetalheVR_TOTALGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroLancamento.qryGridDetalheVR_UNITARIOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroLancamento.qryGridFilhoPadraoCalcFields(DataSet: TDataSet);
begin
  inherited;

  qryGridFilhoPadrao.FieldByName('TP_LANC').AsString := qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString;
end;

procedure TFFiltroLancamento.qryGridFilhoPadraoVR_PARCELA_DESCONTOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroLancamento.qryGridFilhoPadraoVR_PARCELA_JUROSGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroLancamento.qryGridFilhoPadraoVR_PARCELA_MULTAGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroLancamento.qryGridFilhoPadraoVR_PARCELA_PAGOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroLancamento.qryGridFilhoPadraoVR_PARCELA_PREVGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroLancamento.qryGridPaiPadraoAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryGridDetalhe.Close;
  qryGridDetalhe.Params.ParamByName('COD_LANCAMENTO').Value := qryGridPaiPadrao.FieldByName('COD_LANCAMENTO').AsInteger;
  qryGridDetalhe.Params.ParamByName('ANO_LANCAMENTO').Value := qryGridPaiPadrao.FieldByName('ANO_LANCAMENTO').AsInteger;
  qryGridDetalhe.Open;

  qryGridFilhoPadrao.Close;
  qryGridFilhoPadrao.Params.ParamByName('COD_LANCAMENTO').Value := qryGridPaiPadrao.FieldByName('COD_LANCAMENTO').AsInteger;
  qryGridFilhoPadrao.Params.ParamByName('ANO_LANCAMENTO').Value := qryGridPaiPadrao.FieldByName('ANO_LANCAMENTO').AsInteger;
  qryGridFilhoPadrao.Open;

  VerificarPermissoes;
end;

procedure TFFiltroLancamento.qryGridPaiPadraoVR_TOTAL_DESCONTOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroLancamento.qryGridPaiPadraoVR_TOTAL_JUROSGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroLancamento.qryGridPaiPadraoVR_TOTAL_PAGOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroLancamento.qryGridPaiPadraoVR_TOTAL_PREVGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('R$ ###,###,##0.00', Sender.AsFloat);
end;

procedure TFFiltroLancamento.rgOrdenacaoClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroLancamento.rgTipoDataPerClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroLancamento.rgTipoDataPerExit(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroLancamento.rgTipoLancamentoClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroLancamento.rgTipoLancamentoExit(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroLancamento.VerificarPermissoes;
begin
  inherited;

  btnIncluir.Enabled  := vgPrm_IncDesp or vgPrm_IncRec;

  btnEditar.Enabled   := (qryGridPaiPadrao.RecordCount > 0) and
                         (((qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString = 'D') and
                            vgPrm_EdDesp) or
                          ((qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString = 'R') and
                            vgPrm_EdRec));

  btnExcluir.Enabled  := (qryGridPaiPadrao.RecordCount > 0) and
                         (((qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString = 'D') and
                            vgPrm_ExcDesp) or
                          ((qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString = 'R') and
                            vgPrm_ExcRec));

  btnRealizarPagamento.Enabled := (qryGridPaiPadrao.RecordCount > 0) and
                                  (((qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString = 'D') and
                                     vgPrm_EdDesp) or
                                   ((qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString = 'R') and
                                     vgPrm_EdRec)) and
                                  ((Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) <> 'SAL�RIO') and
                                   (Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) <> 'COMISS�O') and
                                   (Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) <> 'VALE') and
                                   (Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) <> 'OUTROS LAN�AMENTOS COM COLABORADOR')) and
                                  (Trim(qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString) = 'P');

  btnImprimir.Enabled := ((qryGridPaiPadrao.RecordCount > 0) and
                          (((qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString = 'D') and
                             vgPrm_ImpDesp) or
                           ((qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString = 'R') and
                             vgPrm_ImpRec))) and
                         ((Trim(edtCodigoLanc.Text) = '') and
                          (Trim(edtAnoLanc.Text) = ''));

  if (vgUsu_FlgMaster = 'S') or
  (vgUsu_FlgSuporte = 'S') then
    btnAlterarNatureza.Visible := (qryGridPaiPadrao.RecordCount > 0) and
                                  (((qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString = 'D') and
                                    vgPrm_EdDesp) or
                                   ((qryGridPaiPadrao.FieldByName('TIPO_LANCAMENTO').AsString = 'R') and
                                    vgPrm_EdRec)) and
                                  vgPrm_EdLancPago and
                                  ((qryGridPaiPadrao.FieldByName('FLG_STATUS').AsString = 'G') and
                                   (qryGridPaiPadrao.FieldByName('FLG_CANCELADO').AsString = 'N')) and
                                  ((Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) <> 'SAL�RIO') and
                                   (Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) <> 'COMISS�O') and
                                   (Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) <> 'VALE') and
                                   (Trim(qryGridPaiPadrao.FieldByName('DESCR_NATUREZA').AsString) <> 'OUTROS LAN�AMENTOS COM COLABORADOR'))
  else
    btnAlterarNatureza.Visible := False;
end;

end.
