{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UDM.pas
  Descricao:   Data Module Principal
  Author   :   Cristina
  Date:        08-jan-2016
  Last Update: 20-mar-2019 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UDM;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Phys.FBDef, FireDAC.Phys.IBBase, FireDAC.Phys.FB, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.Comp.UI, System.IniFiles,
  Vcl.Forms, Vcl.Controls, Winapi.Windows, Winapi.Messages, System.DateUtils,
  System.Variants, Vcl.Dialogs, JvBaseDlg, JvProgressDialog, Datasnap.DBClient,
  Datasnap.Provider, System.Generics.Collections, System.StrUtils, Winapi.TlHelp32;

type
  //Relatorios
  TRel = (R01, R01DET,  { Extrato de Contas }
          R02, R02DET,  { Fechamento de Caixa por Forma de Pagamento }
          R03, R03DET,  { Fechamento de Caixa por Tipo de Ato }
          R04, R04DET,  { Flutuantes }
          R05, R05DET,  { Recibo }
          R06, R06DET,  { Repasses }
          R07, R07DET,  { Atos }
          R08, R08DET,  { Contas a Receber x Contas a Pagar }
          R09, R09DET,  { Movimento de Caixa }
          R10,          { Diferencas do Fechamento de Caixa }
          R11, R11DET,  { Receitas e Despesas para o IR }
          R12,          { Despesas com Colaboradores }
          R13, R13DET,  { Recibos Cancelados }
          R14, R14DET,  { Filtro de Lancamentos }
          R15,          { Log de Usuario / Auditoria de Tabelas }

          R100,R100DET, {RELATORIO DE LOTE FOLHA}
          R101,R101DET, {RELATORIO DE FOLHA DE SEGURANÇA}
          R102,R102DET, {RELATORIOS DE LOTE DE ETIQUETA}
          R103,R103DET  {RELATORIO DE ETIQUETAS});
  { CLIENTE - FORNECEDOR }
  TTipoClienteFornecedor = (CLIE, FORN);  { CLIE = Cliente / FORN = Fornecedor }

  TTipoProdutoServico = (PROD, SERV);  { PROD = Produto / SERV = Servico}

  TTipoLancamento = (DESP, REC, NONE);  { DESP = Despesa / REC = Receita / NONE = Nenhum }

  TTipoOficio = (ENV, RCB, NHM);  { ENV = Enviado / RCB = Recebido / NHM = Nenhum }

  { LANCAMENTO }
  TDadosClasseLancamento = class(TObject)
  public
    CodLancamento:     Integer;
    AnoLancamento:     Integer;
    DataLancamento:    TDate;
    TipoLancamento:    String;
    TipoCadastro:      String;
    IdCategoria:       Integer;
    DescrCategoria:    String;
    IdSubCategoria:    Integer;
    DescrSubCategoria: String;
    IdNatureza:        Integer;
    DescrNatureza:     String;
    IdCliFor:          Integer;
    NomeCliFor:        String;
    FlgIR:             String;
    FlgPessoal:        String;
    FlgAuxiliar:       String;
    FlgReal:           String;
    FlgFlutuante:      String;
    FlgForaFechCaixa:  String;
    QtdParcelas:       Integer;
    VlrTotalPrev:      Currency;
    VlrTotalJuros:     Currency;
    VlrTotalDesconto:  Currency;
    VlrTotalPago:      Currency;
    CadIdUsuario:      Integer;
    DataCadastro:      TDate;
    FlgStatus:         String;
    FlgCancelado:      String;
    DataCancelamento:  TDate;
    CancelIdUsuario:   Integer;
    FlgRecorrente:     String;
    FlgReplicado:      String;
    NumRecibo:         Integer;
    IdOrigem:          Integer;
    IdSistema:         Integer;
    NomeSistema:       String;
    Observacao:        String;
    IdEquivalencia:    Integer;

    constructor Create(); virtual;
    destructor  Destroy; override;
  end;

  TClasseLancamento = class(TDadosClasseLancamento)
  public
    CodLancamento:     Integer;
    AnoLancamento:     Integer;
    DataLancamento:    TDate;
    TipoLancamento:    String;
    TipoCadastro:      String;
    IdCategoria:       Integer;
    DescrCategoria:    String;
    IdSubCategoria:    Integer;
    DescrSubCategoria: String;
    IdNatureza:        Integer;
    DescrNatureza:     String;
    IdCliFor:          Integer;
    NomeCliFor:        String;
    FlgIR:             String;
    FlgPessoal:        String;
    FlgAuxiliar:       String;
    FlgForaFechCaixa:  String;
    FlgReal:           String;
    FlgFlutuante:      String;
    QtdParcelas:       Integer;
    VlrTotalPrev:      Currency;
    VlrTotalJuros:     Currency;
    VlrTotalDesconto:  Currency;
    VlrTotalPago:      Currency;
    CadIdUsuario:      Integer;
    DataCadastro:      TDate;
    FlgStatus:         String;
    FlgCancelado:      String;
    DataCancelamento:  TDate;
    CancelIdUsuario:   Integer;
    FlgRecorrente:     String;
    FlgReplicado:      String;
    NumRecibo:         Integer;
    IdOrigem:          Integer;
    DescrTipoOrigem:   String;
    IdSistema:         Integer;
    NomeSistema:       String;
    Observacao:        String;
    IdEquivalencia:    Integer;
  end;

  TLancamento = class of TDadosClasseLancamento;

  { LANCAMENTO - DETALHE }
  TDadosClasseLancamentoDet = class(TObject)
  public
    IdLancamentoDet:    Integer;
    CodLancamento:      Integer;
    AnoLancamento:      Integer;
    IdItem:             Integer;
    DescrItem:          String;
    VlrUnitario:        Currency;
    Quantidade:         Integer;
    VlrTotal:           Currency;
    SeloOrigem:         String;
    AleatorioOrigem:    String;
    TipoCobranca:       String;
    CodAdicional:       Integer;
    Emolumentos:        Currency;
    FETJ:               Currency;
    FUNDPERJ:           Currency;
    FUNPERJ:            Currency;
    FUNARPEN:           Currency;
    PMCMV:              Currency;
    ISS:                Currency;
    Mutua:              Currency;
    Acoterj:            Currency;
    NumProtocolo:       Integer;
    FlgCancelado:       String;

    constructor Create(); virtual;
    destructor  Destroy; override;
  end;

  TClasseLancamentoDet = class(TDadosClasseLancamentoDet)
  public
    IdLancamentoDet:    Integer;
    CodLancamento:      Integer;
    AnoLancamento:      Integer;
    IdItem:             Integer;
    DescrItem:          String;
    VlrUnitario:        Currency;
    Quantidade:         Integer;
    VlrTotal:           Currency;
    SeloOrigem:         String;
    AleatorioOrigem:    String;
    TipoCobranca:       String;
    CodAdicional:       Integer;
    Emolumentos:        Currency;
    FETJ:               Currency;
    FUNDPERJ:           Currency;
    FUNPERJ:            Currency;
    FUNARPEN:           Currency;
    PMCMV:              Currency;
    ISS:                Currency;
    Mutua:              Currency;
    Acoterj:            Currency;
    NumProtocolo:       Integer;
    FlgCancelado:       String;
  end;

  TLancamentoDet = class of TDadosClasseLancamentoDet;

  { LACAMENTO - PARCELA }
  TDadosClasseLancamentoParc = class(TObject)
  public
    IdLancamentoParc: Integer;
    CodLancamento:    Integer;
    AnoLancamento:    Integer;
    DataLancParc:     TDate;
    NumParcela:       Integer;
    DataVencimento:   TDate;
    VlrPrevisto:      Currency;
    VlrJuros:         Currency;
    VlrDesconto:      Currency;
    VlrPago:          Currency;
    IdFormaPagtoOrig: Integer;
    IdFormaPagto:     Integer;
    Agencia:          String;
    Conta:            String;
    IdBanco:          Integer;
    NumCheque:        String;
    NumCodDeposito:   String;
    FlgStatus:        String;
    CadIdUsuario:     Integer;
    DataCadastro:     TDate;
    CancelIdUsuario:  Integer;
    DataCancelamento: TDate;
    PagIdUsuario:     Integer;
    DataPagamento:    TDate;
    Observacao:       String;

    constructor Create(); virtual;
    destructor  Destroy; override;
  end;

  TClasseLancamentoParc = class(TDadosClasseLancamentoParc)
  public
    IdLancamentoParc: Integer;
    CodLancamento:    Integer;
    AnoLancamento:    Integer;
    DataLancParc:     TDate;
    NumParcela:       Integer;
    DataVencimento:   TDate;
    VlrPrevisto:      Currency;
    VlrJuros:         Currency;
    VlrDesconto:      Currency;
    VlrPago:          Currency;
    IdFormaPagtoOrig: Integer;
    IdFormaPagto:     Integer;
    Agencia:          String;
    Conta:            String;
    IdBanco:          Integer;
    NumCheque:        String;
    NumCodDeposito:   String;
    FlgStatus:        String;
    CadIdUsuario:     Integer;
    DataCadastro:     TDate;
    CancelIdUsuario:  Integer;
    DataCancelamento: TDate;
    PagIdUsuario:     Integer;
    DataPagamento:    TDate;
    Observacao:       String;
  end;

  TLancamentoParc = class of TDadosClasseLancamentoParc;

  TdmPrincipal = class(TDataModule)
    conSISTEMA: TFDConnection;
    FDPhysFBDriverLink1: TFDPhysFBDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    qryVersao_Layout: TFDQuery;
    qryConfiguracao: TFDQuery;
    dsVersao_Layout: TDataSource;
    dsConfiguracao: TDataSource;
    qryAtualizacao: TFDQuery;
    dsAtualizacao: TDataSource;
    dsModulo: TDataSource;
    qryModulo: TFDQuery;
    qryAtualizacaoDet: TFDQuery;
    dsAtualizacaoDet: TDataSource;
    dsPermissao: TDataSource;
    qryPermissao: TFDQuery;
    qryRecomecaTermo: TFDQuery;
    dsRecomecaTermo: TDataSource;
    dsTermoAtual: TDataSource;
    qryTermoAtual: TFDQuery;
    dsDespesas: TDataSource;
    qryDespesas: TFDQuery;
    pdlgDespesas: TJvProgressDialog;
    dsAuxLanc: TDataSource;
    qryAuxLanc: TFDQuery;
    dspAuxLanc: TDataSetProvider;
    cdsAuxLanc: TClientDataSet;
    cdsAuxLancCOD_LANCAMENTO: TIntegerField;
    cdsAuxLancANO_LANCAMENTO: TIntegerField;
    cdsAuxLancTIPO_LANCAMENTO: TStringField;
    cdsAuxLancID_CLIENTE_FORNECEDOR_FK: TIntegerField;
    cdsAuxLancID_CATEGORIA_DESPREC_FK: TIntegerField;
    cdsAuxLancID_SUBCATEGORIA_DESPREC_FK: TIntegerField;
    cdsAuxLancFLG_IMPOSTORENDA: TStringField;
    cdsAuxLancFLG_PESSOAL: TStringField;
    cdsAuxLancFLG_AUXILIAR: TStringField;
    cdsAuxLancFLG_REAL: TStringField;
    cdsAuxLancQTD_PARCELAS: TIntegerField;
    cdsAuxLancVR_TOTAL_PREV: TBCDField;
    cdsAuxLancVR_TOTAL_JUROS: TBCDField;
    cdsAuxLancVR_TOTAL_PAGO: TBCDField;
    cdsAuxLancCAD_ID_USUARIO: TIntegerField;
    cdsAuxLancFLG_STATUS: TStringField;
    cdsAuxLancFLG_CANCELADO: TStringField;
    cdsAuxLancCANCEL_ID_USUARIO: TIntegerField;
    cdsAuxLancFLG_RECORRENTE: TStringField;
    cdsAuxLancFLG_REPLICADO: TStringField;
    cdsAuxLancNUM_RECIBO: TIntegerField;
    cdsAuxLancOBSERVACAO: TStringField;
    cdsAuxLancTIPO_CADASTRO: TStringField;
    cdsAuxLancID_ORIGEM: TIntegerField;
    cdsAuxLancID_SISTEMA_FK: TIntegerField;
    cdsAuxLancID_NATUREZA_FK: TIntegerField;
    qryHistoricoItem: TFDQuery;
    dspHistoricoItem: TDataSetProvider;
    cdsHistoricoItem: TClientDataSet;
    dsHistoricoItem: TDataSource;
    cdsHistoricoItemID_HISTORICO_ITEM: TIntegerField;
    cdsHistoricoItemID_ITEM_FK: TIntegerField;
    cdsHistoricoItemID_USUARIO: TIntegerField;
    cdsHistoricoItemQUANTIDADE: TIntegerField;
    cdsHistoricoItemVR_UNITARIO: TBCDField;
    cdsHistoricoItemID_TIPO_HISTORICO_FK: TIntegerField;
    cdsHistoricoItemFLG_TIPO_HISTORICO: TStringField;
    cdsHistoricoItemDATA_HISTORICO: TSQLTimeStampField;
    cdsAuxLancVR_TOTAL_DESCONTO: TBCDField;
    dsVales: TDataSource;
    qryVales: TFDQuery;
    dsComissoes: TDataSource;
    qryComissoes: TFDQuery;
    dspComissoes: TDataSetProvider;
    dspVales: TDataSetProvider;
    cdsComissoes: TClientDataSet;
    cdsComissoesID_COMISSAO: TIntegerField;
    cdsComissoesID_FUNCIONARIO_FK: TIntegerField;
    cdsComissoesID_SISTEMA_FK: TIntegerField;
    cdsComissoesCOD_ADICIONAL: TIntegerField;
    cdsComissoesTIPO_ORIGEM: TStringField;
    cdsComissoesPERCENT_COMISSAO: TBCDField;
    cdsComissoesVR_COMISSAO: TBCDField;
    cdsComissoesCAD_ID_USUARIO: TIntegerField;
    cdsComissoesDATA_PAGAMENTO: TDateField;
    cdsComissoesPAG_ID_USUARIO: TIntegerField;
    cdsComissoesNOMESISTEMA: TStringField;
    cdsComissoesID_ORIGEM: TIntegerField;
    cdsComissoesID_FECHAMENTO_SALARIO_FK: TIntegerField;
    cdsComissoesFLG_CANCELADO: TStringField;
    cdsComissoesDATA_CANCELAMENTO: TDateField;
    cdsComissoesCANCEL_ID_USUARIO: TIntegerField;
    cdsVales: TClientDataSet;
    cdsValesID_VALE: TIntegerField;
    cdsValesID_FUNCIONARIO_FK: TIntegerField;
    cdsValesVR_VALE: TBCDField;
    cdsValesDESCR_VALE: TStringField;
    cdsValesCAD_ID_USUARIO: TIntegerField;
    cdsValesDATA_QUITACAO: TDateField;
    cdsValesQUIT_ID_USUARIO: TIntegerField;
    cdsValesFLG_CANCELADO: TStringField;
    cdsValesDATA_CANCELAMENTO: TDateField;
    cdsValesCANCEL_ID_USUARIO: TIntegerField;
    cdsValesID_FECHAMENTO_SALARIO_FK: TIntegerField;
    qryOutrasDesp: TFDQuery;
    dspOutrasDesp: TDataSetProvider;
    cdsOutrasDesp: TClientDataSet;
    cdsOutrasDespID_OUTRADESPESA_FUNC: TIntegerField;
    cdsOutrasDespID_FUNCIONARIO_FK: TIntegerField;
    cdsOutrasDespID_TIPO_OUTRADESP_FUNC_FK: TIntegerField;
    cdsOutrasDespVR_OUTRADESPESA_FUNC: TBCDField;
    cdsOutrasDespDESCR_OUTRADESPESA_FUNC: TStringField;
    cdsOutrasDespCAD_ID_USUARIO: TIntegerField;
    cdsOutrasDespDATA_QUITACAO: TDateField;
    cdsOutrasDespQUIT_ID_USUARIO: TIntegerField;
    cdsOutrasDespFLG_CANCELADO: TStringField;
    cdsOutrasDespDATA_CANCELAMENTO: TDateField;
    cdsOutrasDespCANCEL_ID_USUARIO: TIntegerField;
    cdsOutrasDespID_FECHAMENTO_SALARIO_FK: TIntegerField;
    dsOutrasDesp: TDataSource;
    dsComissoesPendentes: TDataSource;
    qryComissoesPendentes: TFDQuery;
    dspComissoesPendentes: TDataSetProvider;
    cdsComissoesPendentes: TClientDataSet;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    IntegerField5: TIntegerField;
    StringField1: TStringField;
    BCDField1: TBCDField;
    BCDField2: TBCDField;
    IntegerField6: TIntegerField;
    DateField2: TDateField;
    IntegerField7: TIntegerField;
    StringField3: TStringField;
    IntegerField8: TIntegerField;
    IntegerField9: TIntegerField;
    StringField4: TStringField;
    DateField3: TDateField;
    IntegerField10: TIntegerField;
    cdsValesDATA_VALE: TDateField;
    cdsValesDATA_CADASTRO: TSQLTimeStampField;
    cdsOutrasDespDATA_OUTRADESPESA_FUNC: TDateField;
    cdsOutrasDespDATA_CADASTRO: TSQLTimeStampField;
    cdsComissoesDATA_COMISSAO: TDateField;
    cdsComissoesDATA_CADASTRO: TSQLTimeStampField;
    cdsComissoesPendentesDATA_COMISSAO: TDateField;
    cdsComissoesPendentesDATA_CADASTRO: TSQLTimeStampField;
    cdsAuxLancFLG_FLUTUANTE: TStringField;
    cdsAuxLancDATA_CADASTRO: TSQLTimeStampField;
    cdsAuxLancDATA_LANCAMENTO: TDateField;
    cdsAuxLancDATA_CANCELAMENTO: TDateField;
    cdsAuxLancID_CARNELEAO_EQUIVALENCIA_FK: TIntegerField;
    cdsComissoesPendentesID_ITEM_FK: TIntegerField;
    cdsComissoesPendentesDESCR_ITEM: TStringField;
    cdsComissoesDESCR_ITEM: TStringField;
    cdsComissoesID_ITEM_FK: TIntegerField;
    cdsOutrasDespFLG_MODALIDADE: TStringField;
    cdsAuxLancFLG_FORAFECHCAIXA: TStringField;
    qryRemetente: TFDQuery;
    dspRemetente: TDataSetProvider;
    cdsRemetente: TClientDataSet;
    dsRemetente: TDataSource;
    dsDestinatario: TDataSource;
    qryDestinatario: TFDQuery;
    dspDestinatario: TDataSetProvider;
    cdsDestinatario: TClientDataSet;
    cdsDestinatarioID_EMAIL_DESTINATARIO: TIntegerField;
    cdsDestinatarioNOME_DESTINATARIO: TStringField;
    cdsDestinatarioEMAIL_DESTINATARIO: TStringField;
    cdsDestinatarioFLG_REL_FECHAMENTO: TStringField;
    cdsDestinatarioFLG_ATIVO: TStringField;
    cdsDestinatarioNOVO: TBooleanField;
    cdsRemetenteID_EMAIL_REMETENTE: TIntegerField;
    cdsRemetenteNOME_REMETENTE: TStringField;
    cdsRemetenteEMAIL_REMETENTE: TStringField;
    cdsRemetenteSENHA_REMETENTE: TStringField;
    cdsRemetenteSERVIDOR_SMTP: TStringField;
    cdsRemetentePORTA_SMTP: TIntegerField;
    cdsRemetenteFLG_SSL_SMTP: TStringField;
    cdsRemetenteFLG_ATIVO: TStringField;
    cdsRemetenteUSERNAME_REMETENTE: TStringField;
    cdsRemetenteFLG_TLS_SMTP: TStringField;
    cdsRemetenteFLG_AUTENTICACAO: TStringField;
    dspDespesas: TDataSetProvider;
    cdsDespesas: TClientDataSet;
    cdsDespesasSELECIONADO: TBooleanField;
    cdsDespesasCOD_LANCAMENTO: TIntegerField;
    cdsDespesasANO_LANCAMENTO: TIntegerField;
    cdsDespesasLANC: TStringField;
    cdsDespesasTIPO_LANCAMENTO: TStringField;
    cdsDespesasDESCR_CATEGORIA_DESPREC: TStringField;
    cdsDespesasDESCR_SUBCATEGORIA_DESPREC: TStringField;
    cdsDespesasDESCR_NATUREZA: TStringField;
    cdsDespesasID_LANCAMENTO_PARC: TIntegerField;
    cdsDespesasNUM_PARCELA: TStringField;
    cdsDespesasDATA_VENCIMENTO: TDateField;
    cdsDespesasVR_PARCELA_PREV: TBCDField;
    cdsDespesasDESCR_FORMAPAGAMENTO: TStringField;
    cdsDespesasFLG_STATUS: TStringField;
    cdsDespesasNUM_RECIBO: TIntegerField;
    cdsDespesasID_NATUREZA_FK: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsComissoesCalcFields(DataSet: TDataSet);
    procedure cdsComissoesVR_COMISSAOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsOutrasDespVR_OUTRADESPESA_FUNCGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsValesVR_VALEGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryDespesasVR_PARCELA_PREVGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsComissoesPendentesCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure RetornarGenerico(Tipo: String; var Id, IdFK, Codigo: Integer;
      var Descricao, Observacao: String);
  public
    { Public declarations }

    procedure AbrirRelatorio(NumRelatorio: Integer; RCPN: Boolean = False; DataMovimento: TDatetime = 0);
    procedure ApontarFechamentoAgregadas(Mes, Ano, IdFechamento, IdFuncionario: Integer);
    procedure AtualizarLancamentosColaboradorRecorrentes;
    procedure AtualizarLancamentosRecorrentes;
    procedure CancelarImportacoesDuplicadas(dDataImportacao: TDatetime);
    procedure FinalizarComponenteLancamento;
    procedure InicializarComponenteLancamento;
    procedure InsertLancamento(Indice: Integer);
    procedure InsertLancamentoDet(Indice, IndiceL: Integer);
    procedure InsertLancamentoParc(Indice, IndiceL: Integer);
    procedure RecalcularFechamentoSalarios(IdFechamento, IdFuncionario: Integer;
                                           Vale, Comissao, OutrasDespesas: Boolean);
    procedure RetornaCargo(var Id, Codigo: Integer; var Descricao,
                           Observacao: String);
    procedure RetornaCategoria(var Id, IdCategoria, Codigo: Integer; var Descricao,
                               Observacao: String);
    procedure RetornaNatureza(var Id, Codigo: Integer; var Descricao,
                              Observacao: String);
    procedure RetornarClienteFornecedor(Tipo: String; var Id: Integer;
                                        var Documento, Observacao, NomeFantasia,
                                        RazaoSocial: String);
    procedure RetornarFuncionario(var Id, IdCargo: Integer; var Nome, CPF,
                                  Observacao: String);
    procedure RetornarItem(Tipo: String; var Id: Integer;
      var Descricao, Abreviacao, Observacao, UMedida: String; var Medida: Real);
    procedure RetornaMesAnoPagamentoSal(var Mes, Ano: Integer);
    procedure RetornaSubCategoria(var Id, IdSubCategoria, Codigo: Integer;
                                  var Descricao, Observacao: String);
    procedure RetornarUsuario(var Id, IdFuncionario: Integer; var Nome, CPF,
      Matricula, Cargo, Senha, Observacao: String);
    procedure RetornarClassificacaoNatureza(IdNat: Integer; var IR, CP, LA: String );

    procedure UpdateLancamento(Indice: Integer);
    procedure UpdateLancamentoDet(Indice, IndiceL: Integer);
    procedure UpdateLancamentoParc(Indice, IndiceL: Integer);

    procedure VerificarSituacaoCaixa;

    function AtualizarEstoqueProduto(IdItem: Integer; UltValor: Currency): String;
    function CalcularDataPagamentoFunc: TDateTime;
    function ExisteCadastroComissaoF: Boolean;
    function ExistemComissoesPendentes: Boolean;
    function ExistemReceitasPeriodo: Boolean;
    function FechamentoComissoesPendente: Boolean;
    function FechamentoPagamentosPendente: Boolean;
    function GerarDataDespesaRecorrente(Data: TDateTime): TDateTime;
    function ProcessoSistemaAtivo(NomeExe: String): Boolean;
    function RetornaEquivalenciaCarneLeao(IdCat, IdSubCat, IdNat: Integer): Integer;
  end;

var
  dmPrincipal: TdmPrincipal;

  iQtdParc,
  iQtdTot: Integer;

  sNomeRelatorio: String;

  { LANCAMENTO }
  Lancamento: TLancamento;
  DadosLancamento: TDadosClasseLancamento;
  ListaLancamentos: TList<TDadosClasseLancamento>;

  { LANCAMENTO - DETALHE }
  LancamentoDet: TLancamentoDet;
  DadosLancamentoDet: TDadosClasseLancamentoDet;
  ListaLancamentoDets: TList<TDadosClasseLancamentoDet>;

  { LANCAMENTO - PARCELA }
  LancamentoParc: TLancamentoParc;
  DadosLancamentoParc: TDadosClasseLancamentoParc;
  ListaLancamentoParcs: TList<TDadosClasseLancamentoParc>;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses UVariaveisGlobais, ULogin, UBibliotecaSistema, UPrincipal, UGDM,
  UDMRelatorios, UFiltroRelatorioDespesasColaboradores,
  UFiltroRelatorioFlutuantes, UFiltroRelatorioMovimentoCaixa,
  UFiltroRelatorioDiferencasFechamentoCaixa, UFiltroRelatorioLote,
  UFiltroRelatorioFolha, UFiltroRelatorioLoteEtiqueta,
  UFiltroRelatorioEtiquetas;

{$R *.dfm}

procedure TdmPrincipal.AbrirRelatorio(NumRelatorio: Integer; RCPN: Boolean; DataMovimento: TDatetime);
begin
  case NumRelatorio of
    4:  //Flutuantes
    begin
      vgOperacao := P;
      vgOrigemCadastro := False;
      Application.CreateForm(TdmRelatorios, dmRelatorios);
      dmGerencial.CriarForm(TFFiltroRelatorioFlutuantes, FFiltroRelatorioFlutuantes);
      FreeAndNil(dmRelatorios);
    end;
    9:  //Movimento de Caixa
    begin
      vgOperacao := P;
      vgOrigemCadastro := False;
      Application.CreateForm(TdmRelatorios, dmRelatorios);

      if DataMovimento = 0 then
        dmGerencial.CriarForm(TFFiltroRelatorioMovimentoCaixa, FFiltroRelatorioMovimentoCaixa)
      else
      begin
        try
          Application.CreateForm(TFFiltroRelatorioMovimentoCaixa, FFiltroRelatorioMovimentoCaixa);
          FFiltroRelatorioMovimentoCaixa.DataMovimento := DataMovimento;
          FFiltroRelatorioMovimentoCaixa.ShowModal;
        finally
          FFiltroRelatorioMovimentoCaixa.Free;
        end;
      end;

      FreeAndNil(dmRelatorios);
    end;
    10:  //Diferencas do Fechamento de Caixa
    begin
      vgOperacao := P;
      vgOrigemCadastro := False;
      Application.CreateForm(TdmRelatorios, dmRelatorios);
      dmGerencial.CriarForm(TFFiltroRelatorioDiferencasFechamentoCaixa, FFiltroRelatorioDiferencasFechamentoCaixa);
      FreeAndNil(dmRelatorios);
    end;
    12:  //Despesas com Colaboradores
    begin
      vgOperacao := P;
      vgOrigemCadastro := False;
      Application.CreateForm(TdmRelatorios, dmRelatorios);
      dmGerencial.CriarForm(TFFiltroRelatorioDespesasColaboradores, FFiltroRelatorioDespesasColaboradores);
      FreeAndNil(dmRelatorios);
    end;
    100:  //Lotes de Folhas
    begin
      vgOperacao := P;
      vgOrigemCadastro := False;

      try
        Application.CreateForm(TdmRelatorios, dmRelatorios);
        Application.CreateForm(TFFiltroRelatorioLote, FFiltroRelatorioLote);
        FFiltroRelatorioLote.lRCPN := RCPN;
        FFiltroRelatorioLote.ShowModal;
      finally
        FFiltroRelatorioLote.Free;
        FreeAndNil(dmRelatorios);
      end;
    end;
    101:  //Folhas de Seguranca
    begin
      vgOperacao := P;
      vgOrigemCadastro := False;

      try
        Application.CreateForm(TdmRelatorios, dmRelatorios);
        Application.CreateForm(TFFiltroRelatorioFolha, FFiltroRelatorioFolha);
        FFiltroRelatorioFolha.lRCPN := RCPN;
        FFiltroRelatorioFolha.ShowModal;
      finally
        FFiltroRelatorioFolha.Free;
        FreeAndNil(dmRelatorios);
      end;
    end;
    102:  //Lotes de Etiquetas
    begin
      vgOperacao := P;
      vgOrigemCadastro := False;
      Application.CreateForm(TdmRelatorios, dmRelatorios);
      dmGerencial.CriarForm(TFFiltroRelatorioLoteEtiqueta, FFiltroRelatorioLoteEtiqueta);
      FreeAndNil(dmRelatorios);
    end;
    103:  //Etiquetas
    begin
      vgOperacao := P;
      vgOrigemCadastro := False;
      Application.CreateForm(TdmRelatorios, dmRelatorios);
      dmGerencial.CriarForm(TFFiltroRelatorioEtiquetas, FFiltroRelatorioEtiquetas);
      FreeAndNil(dmRelatorios);
    end;
  end;
end;

procedure TdmPrincipal.ApontarFechamentoAgregadas(Mes, Ano, IdFechamento,
  IdFuncionario: Integer);
begin
  //Informar o Id do Fechamento nas tabelas agregadas
  //Comissoes
  dmPrincipal.cdsComissoes.Close;
  dmPrincipal.cdsComissoes.Params.ParamByName('MES_REFERENCIA').Value := Mes;
  dmPrincipal.cdsComissoes.Params.ParamByName('ANO_REFERENCIA').Value := Ano;
  dmPrincipal.cdsComissoes.Params.ParamByName('ID_FUNC01').Value      := IdFuncionario;
  dmPrincipal.cdsComissoes.Params.ParamByName('ID_FUNC02').Value      := IdFuncionario;
  dmPrincipal.cdsComissoes.Open;

  dmPrincipal.cdsComissoes.First;

  while not dmPrincipal.cdsComissoes.Eof do
  begin
    dmPrincipal.cdsComissoes.Edit;

    if IdFechamento > 0 then
    begin
      dmPrincipal.cdsComissoes.FieldByName('ID_FECHAMENTO_SALARIO_FK').AsInteger := IdFechamento;
      dmPrincipal.cdsComissoes.FieldByName('PAG_ID_USUARIO').AsInteger           := vgUsu_Id;
      dmPrincipal.cdsComissoes.FieldByName('DATA_PAGAMENTO').AsDateTime          := Date;
    end
    else
    begin
      dmPrincipal.cdsComissoes.FieldByName('ID_FECHAMENTO_SALARIO_FK').Value := Null;
      dmPrincipal.cdsComissoes.FieldByName('PAG_ID_USUARIO').Value           := Null;
      dmPrincipal.cdsComissoes.FieldByName('DATA_PAGAMENTO').Value           := Null;
    end;

    dmPrincipal.cdsComissoes.Post;

    dmPrincipal.cdsComissoes.Next;
  end;

  dmPrincipal.cdsComissoes.ApplyUpdates(0);

  //Outras Despesas
  dmPrincipal.cdsOutrasDesp.Close;
  dmPrincipal.cdsOutrasDesp.Params.ParamByName('MES_REFERENCIA').Value := Mes;
  dmPrincipal.cdsOutrasDesp.Params.ParamByName('ANO_REFERENCIA').Value := Ano;
  dmPrincipal.cdsOutrasDesp.Params.ParamByName('ID_FUNCIONARIO').Value := IdFuncionario;
  dmPrincipal.cdsOutrasDesp.Open;

  dmPrincipal.cdsOutrasDesp.First;

  while not dmPrincipal.cdsOutrasDesp.Eof do
  begin
    dmPrincipal.cdsOutrasDesp.Edit;

    if IdFechamento > 0 then
      dmPrincipal.cdsOutrasDesp.FieldByName('ID_FECHAMENTO_SALARIO_FK').AsInteger := IdFechamento
    else
      dmPrincipal.cdsOutrasDesp.FieldByName('ID_FECHAMENTO_SALARIO_FK').Value     := Null;

    dmPrincipal.cdsOutrasDesp.Post;

    dmPrincipal.cdsOutrasDesp.Next;
  end;

  dmPrincipal.cdsOutrasDesp.ApplyUpdates(0);

  //Vales
  dmPrincipal.cdsVales.Close;
  dmPrincipal.cdsVales.Params.ParamByName('MES_REFERENCIA').Value := Mes;
  dmPrincipal.cdsVales.Params.ParamByName('ANO_REFERENCIA').Value := Ano;
  dmPrincipal.cdsVales.Params.ParamByName('ID_FUNCIONARIO').Value := IdFuncionario;
  dmPrincipal.cdsVales.Open;

  dmPrincipal.cdsVales.First;

  while not dmPrincipal.cdsVales.Eof do
  begin
    dmPrincipal.cdsVales.Edit;

    if IdFechamento > 0 then
      dmPrincipal.cdsVales.FieldByName('ID_FECHAMENTO_SALARIO_FK').AsInteger := IdFechamento
    else
      dmPrincipal.cdsVales.FieldByName('ID_FECHAMENTO_SALARIO_FK').Value     := Null;

    dmPrincipal.cdsVales.Post;

    dmPrincipal.cdsVales.Next;
  end;

  dmPrincipal.cdsVales.ApplyUpdates(0);
end;

procedure TdmPrincipal.AtualizarLancamentosColaboradorRecorrentes;
var
  QryOD, QryDesp, QryAuxOD, QryAuxDet, QryAuxParc, QryAuxLog: TFDQuery;
  IdOldOD, IdOD, IdDetalhe, IdParcela, NumParc, k, m: Integer;
  lAtualizar, lContinuar: Boolean;
  Msg, sQtd: String;
begin
  //Essa rotina serve para gerar os Lançamentos Recorrentes com Colaboradores automaticamente
  sQtd := '';
  lAtualizar := False;

  IdOldOD   := 0;
  IdOD      := 0;
  IdDetalhe := 0;
  IdParcela := 0;
  NumParc   := 0;

  QryOD      := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
  QryDesp    := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
  QryAuxOD   := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
  QryAuxDet  := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
  QryAuxParc := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
  QryAuxLog  := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryDesp, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT DATA_ULT_ATULANCOLAB FROM CONFIGURACAO';
    Open;

    lAtualizar := (QryDesp.FieldByName('DATA_ULT_ATULANCOLAB').AsDateTime < Date);

    if lAtualizar then
    begin
      { Pesquisa todas as Lancamentos de Colaboradores Recorrentes que nao
        possuem replica no mes atual }
      QryOD.Close;
      QryOD.SQL.Clear;
      QryOD.SQL.Text := 'SELECT OD.* ' +
                        '  FROM OUTRADESPESA_FUNC OD ' +
                        ' WHERE OD.FLG_RECORRENTE = ' + QuotedStr('S') +
                        '   AND OD.FLG_REPLICADO = ' + QuotedStr('N') +
                        '   AND OD.FLG_CANCELADO = ' + QuotedStr('N') +
                        '   AND EXTRACT (MONTH FROM OD.DATA_CADASTRO) = :MES_CADASTRO ' +
                        '   AND EXTRACT (YEAR FROM OD.DATA_CADASTRO) = :ANO_CADASTRO ' +
                        '   AND OD.FLG_CANCELADO = ' + QuotedStr('N') +
                        'ORDER BY OD.ID_OUTRADESPESA_FUNC';

      if MonthOf(Date) = 1 then
      begin
        QryOD.Params.ParamByName('MES_CADASTRO').Value  := 12;
        QryOD.Params.ParamByName('ANO_CADASTRO').Value  := (YearOf(Date) - 1);
      end
      else
      begin
        QryOD.Params.ParamByName('MES_CADASTRO').Value  := (MonthOf(Date) - 1);
        QryOD.Params.ParamByName('ANO_CADASTRO').Value  := YearOf(Date);
      end;

      QryOD.Open;
      QryOD.Last;

      sQtd := IntToStr(QryOD.RecordCount);

      lAtualizar := QryOD.RecordCount > 0;
    end;

    if lAtualizar then
    begin
      pdlgDespesas.Min := 0;
      pdlgDespesas.Show;
      pdlgDespesas.Caption := 'Atualização de LANÇAMENTOS RECORRENTES COM COLABORADOR';
      pdlgDespesas.Max := QryOD.RecordCount;

      QryOD.First;

      IdOD := BS.ProximoId('ID_OUTRADESPESA_FUNC', 'OUTRADESPESA_FUNC');

      while not QryOD.Eof do
      begin
        lContinuar := True;

        IdOldOD := QryOD.FieldByName('ID_OUTRADESPESA_FUNC').AsInteger;

        //Copia a Outra Despesa
        try
          if vgConSISTEMA.Connected then
            vgConSISTEMA.StartTransaction;

          QryAuxOD.Close;
          QryAuxOD.SQL.Clear;
          QryAuxOD.SQL.Text := 'INSERT INTO OUTRADESPESA_FUNC (ID_OUTRADESPESA_FUNC, ' +
                               '                               ID_FUNCIONARIO_FK, ' +
                               '                               ID_TIPO_OUTRADESP_FUNC_FK, ' +
                               '                               DATA_OUTRADESPESA_FUNC, ' +
                               '                               VR_OUTRADESPESA_FUNC, ' +
                               '                               DESCR_OUTRADESPESA_FUNC, ' +
                               '                               CAD_ID_USUARIO, ' +
                               '                               DATA_QUITACAO, ' +
                               '                               QUIT_ID_USUARIO, ' +
                               '                               FLG_CANCELADO, ' +
                               '                               FLG_MODALIDADE, ' +
                               '                               FLG_RECORRENTE) ' +
                               '                       VALUES (:ID_OUTRADESPESA_FUNC, ' +
                               '                               :ID_FUNCIONARIO, ' +
                               '                               :ID_TIPO_OUTRADESP_FUNC, ' +
                               '                               :DATA_OUTRADESPESA_FUNC, ' +
                               '                               :VR_OUTRADESPESA_FUNC, ' +
                               '                               :DESCR_OUTRADESPESA_FUNC, ' +
                               '                               :CAD_ID_USUARIO, ' +
                               '                               :DATA_QUITACAO, ' +
                               '                               :QUIT_ID_USUARIO, ' +
                               '                               :FLG_CANCELADO, ' +
                               '                               :FLG_MODALIDADE, ' +
                               '                               :FLG_RECORRENTE)';
          QryAuxOD.Params.ParamByName('ID_OUTRADESPESA_FUNC').Value    := IdOD;
          QryAuxOD.Params.ParamByName('ID_FUNCIONARIO').Value          := QryOD.FieldByName('ID_FUNCIONARIO_FK').AsInteger;
          QryAuxOD.Params.ParamByName('ID_TIPO_OUTRADESP_FUNC').Value  := QryOD.FieldByName('ID_TIPO_OUTRADESP_FUNC_FK').AsInteger;
          QryAuxOD.Params.ParamByName('DATA_OUTRADESPESA_FUNC').Value  := GerarDataDespesaRecorrente(QryOD.FieldByName('DATA_OUTRADESPESA_FUNC').AsDateTime);
          QryAuxOD.Params.ParamByName('VR_OUTRADESPESA_FUNC').Value    := QryOD.FieldByName('VR_OUTRADESPESA_FUNC').AsCurrency;
          QryAuxOD.Params.ParamByName('DESCR_OUTRADESPESA_FUNC').Value := QryOD.FieldByName('DESCR_OUTRADESPESA_FUNC').AsString;
          QryAuxOD.Params.ParamByName('CAD_ID_USUARIO').Value          := vgUsu_Id;
          QryAuxOD.Params.ParamByName('DATA_QUITACAO').Value           := GerarDataDespesaRecorrente(QryOD.FieldByName('DATA_QUITACAO').AsDateTime);
          QryAuxOD.Params.ParamByName('QUIT_ID_USUARIO').Value         := vgUsu_Id;
          QryAuxOD.Params.ParamByName('FLG_CANCELADO').Value           := 'N';
          QryAuxOD.Params.ParamByName('FLG_MODALIDADE').Value          := QryOD.FieldByName('FLG_MODALIDADE').AsString;
          QryAuxOD.Params.ParamByName('FLG_RECORRENTE').Value          := 'S';
          QryAuxOD.ExecSQL;

          if vgConSISTEMA.InTransaction then
            vgConSISTEMA.Commit;
        except
          on E: Exception do
          begin
            if vgConSISTEMA.InTransaction then
              vgConSISTEMA.Rollback;

            lContinuar := False;

            if Trim(Msg) = '' then
              Msg := E.Message
            else
               Msg := Msg + #13#10 + E.Message;
          end;
        end;

        Inc(IdOD);

        pdlgDespesas.Text := 'Atualizando ' + IntToStr(QryOD.RecNo) + ' de ' + sQtd + ' Lançamentos Recorrentes com Colaborador...';
        pdlgDespesas.Position := QryOD.RecNo;

        if lContinuar then
        begin
          //Lancamento atrelado a tabela OUTRA_DESPESA_FUNC
          QryAuxOD.Close;
          QryAuxOD.SQL.Clear;
          QryAuxOD.SQL.Text := 'SELECT L.* ' +
                               '  FROM LANCAMENTO L ' +
                               ' WHERE L.FLG_CANCELADO = ' + QuotedStr('N') +
                               '   AND L.ID_NATUREZA_FK = 7 ' +
                               '   AND L.ID_ORIGEM = :ID_ORIGEM ' +
                               '   AND (SELECT LP.DATA_PAGAMENTO ' +
                               '          FROM LANCAMENTO_PARC LP ' +
                               '         WHERE LP.COD_LANCAMENTO_FK = L.COD_LANCAMENTO ' +
                               '           AND LP.ANO_LANCAMENTO_FK = L.ANO_LANCAMENTO) <> ' + QuotedStr('1899-12-31') +
                               'ORDER BY L.COD_LANCAMENTO, L.ANO_LANCAMENTO';
          QryAuxOD.Params.ParamByName('ID_ORIGEM').Value := IdOldOD;  //OUTROS LANCAMENTOS COM COLABORADOR
          QryAuxOD.Open;

          QryAuxOD.First;

          vgLanc_Codigo := dmGerencial.ProximoCodigoLancamento;
          vgLanc_Ano := YearOf(Date);

          IdDetalhe := BS.ProximoId('ID_LANCAMENTO_DET', 'LANCAMENTO_DET');
          IdParcela := BS.ProximoId('ID_LANCAMENTO_PARC', 'LANCAMENTO_PARC');

          while not QryAuxOD.Eof do
          begin
            try
              if vgConSISTEMA.Connected then
                vgConSISTEMA.StartTransaction;

              if vgConGER.Connected then
                vgConGER.StartTransaction;

              InicializarComponenteLancamento;

              //Copia o Lancamento
              { LANCAMENTO }
              DadosLancamento := Lancamento.Create;

              DadosLancamento.TipoLancamento     := QryAuxOD.FieldByName('TIPO_LANCAMENTO').AsString;
              DadosLancamento.TipoCadastro       := 'A';

              if QryAuxOD.FieldByName('ID_CLIENTE_FORNECEDOR_FK').IsNull then
                DadosLancamento.IdCliFor         := 0
              else
                DadosLancamento.IdCliFor         := QryAuxOD.FieldByName('ID_CLIENTE_FORNECEDOR_FK').AsInteger;

                if QryAuxOD.FieldByName('ID_CATEGORIA_DESPREC_FK').IsNull then
                  DadosLancamento.IdCategoria    := 0
                else
                  DadosLancamento.IdCategoria    := QryAuxOD.FieldByName('ID_CATEGORIA_DESPREC_FK').AsInteger;

                if QryAuxOD.FieldByName('ID_SUBCATEGORIA_DESPREC_FK').IsNull then
                  DadosLancamento.IdSubCategoria := 0
                else
                  DadosLancamento.IdSubCategoria := QryAuxOD.FieldByName('ID_SUBCATEGORIA_DESPREC_FK').AsInteger;

              DadosLancamento.FlgIR              := QryAuxOD.FieldByName('FLG_IMPOSTORENDA').AsString;
              DadosLancamento.FlgPessoal         := QryAuxOD.FieldByName('FLG_PESSOAL').AsString;
              DadosLancamento.FlgFlutuante       := QryAuxOD.FieldByName('FLG_FLUTUANTE').AsString;
              DadosLancamento.FlgAuxiliar        := QryAuxOD.FieldByName('FLG_AUXILIAR').AsString;
              DadosLancamento.FlgForaFechCaixa   := QryAuxOD.FieldByName('FLG_FORAFECHCAIXA').AsString;
              DadosLancamento.IdNatureza         := QryAuxOD.FieldByName('ID_NATUREZA_FK').AsInteger;
              DadosLancamento.QtdParcelas        := QryAuxOD.FieldByName('QTD_PARCELAS').AsInteger;
              DadosLancamento.VlrTotalPrev       := QryAuxOD.FieldByName('VR_TOTAL_PREV').AsCurrency;
              DadosLancamento.VlrTotalJuros      := 0;
              DadosLancamento.VlrTotalDesconto   := 0;
              DadosLancamento.VlrTotalPago       := 0;
              DadosLancamento.CadIdUsuario       := vgUsu_Id;
              DadosLancamento.DataCadastro       := Now;
              DadosLancamento.DataLancamento     := GerarDataDespesaRecorrente(QryAuxOD.FieldByName('DATA_LANCAMENTO').AsDateTime);
              DadosLancamento.FlgRecorrente      := 'S';
              DadosLancamento.FlgReplicado       := 'N';
              DadosLancamento.IdEquivalencia     := QryAuxOD.FieldByName('ID_CARNELEAO_EQUIVALENCIA_FK').AsInteger;
              DadosLancamento.Observacao         := QryAuxOD.FieldByName('OBSERVACAO').AsString;

              ListaLancamentos.Add(DadosLancamento);

              dmPrincipal.InsertLancamento(0);

              vgLanc_Codigo := ListaLancamentos[0].CodLancamento;
              vgLanc_Ano    := ListaLancamentos[0].AnoLancamento;

              { DETALHE }
              QryAuxDet.Close;
              QryAuxDet.SQL.Clear;

              QryAuxDet.SQL.Text := 'SELECT * FROM LANCAMENTO_DET ' +
                                     '  WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                                     '    AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ' +
                                     '    AND FLG_CANCELADO = ' + QuotedStr('N') +
                                     'ORDER BY ID_LANCAMENTO_DET';

              QryAuxDet.Params.ParamByName('COD_LANCAMENTO').Value := QryAuxOD.FieldByName('COD_LANCAMENTO').AsInteger;
              QryAuxDet.Params.ParamByName('ANO_LANCAMENTO').Value := QryAuxOD.FieldByName('ANO_LANCAMENTO').AsInteger;
              QryAuxDet.Open;

              QryAuxDet.First;
              m := 0;

              while not QryAuxDet.Eof do
              begin
                DadosLancamentoDet := LancamentoDet.Create;

                DadosLancamentoDet.IdLancamentoDet   := IdDetalhe;
                DadosLancamentoDet.IdItem            := QryAuxDet.FieldByName('ID_ITEM_FK').AsInteger;
                DadosLancamentoDet.Quantidade        := QryAuxDet.FieldByName('QUANTIDADE').AsInteger;
                DadosLancamentoDet.VlrUnitario       := QryAuxDet.FieldByName('VR_UNITARIO').AsCurrency;
                DadosLancamentoDet.VlrTotal          := QryAuxDet.FieldByName('VR_TOTAL').AsCurrency;

                if Trim(QryAuxDet.FieldByName('SELO_ORIGEM').AsString) <> '' then
                  DadosLancamentoDet.SeloOrigem      := QryAuxDet.FieldByName('SELO_ORIGEM').AsString;

                if Trim(QryAuxDet.FieldByName('ALEATORIO_ORIGEM').AsString) <> '' then
                  DadosLancamentoDet.AleatorioOrigem := QryAuxDet.FieldByName('ALEATORIO_ORIGEM').AsString;

                if Trim(QryAuxDet.FieldByName('TIPO_COBRANCA').AsString) <> '' then
                  DadosLancamentoDet.TipoCobranca    := QryAuxDet.FieldByName('TIPO_COBRANCA').AsString;

                if not QryAuxDet.FieldByName('COD_ADICIONAL').IsNull then
                  DadosLancamentoDet.CodAdicional    := QryAuxDet.FieldByName('COD_ADICIONAL').AsInteger;

                if not QryAuxDet.FieldByName('EMOLUMENTOS').IsNull then
                  DadosLancamentoDet.Emolumentos     := QryAuxDet.FieldByName('EMOLUMENTOS').AsCurrency;

                if not QryAuxDet.FieldByName('FETJ').IsNull then
                  DadosLancamentoDet.FETJ            := QryAuxDet.FieldByName('FETJ').AsCurrency;

                if not QryAuxDet.FieldByName('FUNDPERJ').IsNull then
                  DadosLancamentoDet.FUNDPERJ        := QryAuxDet.FieldByName('FUNDPERJ').AsCurrency;

                if not QryAuxDet.FieldByName('FUNPERJ').IsNull then
                  DadosLancamentoDet.FUNPERJ         := QryAuxDet.FieldByName('FUNPERJ').AsCurrency;

                if not QryAuxDet.FieldByName('FUNARPEN').IsNull then
                  DadosLancamentoDet.FUNARPEN        := QryAuxDet.FieldByName('FUNARPEN').AsCurrency;

                if not QryAuxDet.FieldByName('PMCMV').IsNull then
                  DadosLancamentoDet.PMCMV           := QryAuxDet.FieldByName('PMCMV').AsCurrency;

                if not QryAuxDet.FieldByName('ISS').IsNull then
                  DadosLancamentoDet.ISS             := QryAuxDet.FieldByName('ISS').AsCurrency;

                if not QryAuxDet.FieldByName('MUTUA').IsNull then
                  DadosLancamentoDet.Mutua           := QryAuxDet.FieldByName('MUTUA').AsCurrency;

                if not QryAuxDet.FieldByName('ACOTERJ').IsNull then
                  DadosLancamentoDet.Acoterj         := QryAuxDet.FieldByName('ACOTERJ').AsCurrency;

                if not QryAuxDet.FieldByName('NUM_PROTOCOLO').IsNull then
                  DadosLancamentoDet.NumProtocolo    := QryAuxDet.FieldByName('NUM_PROTOCOLO').AsInteger;

                ListaLancamentoDets.Add(DadosLancamentoDet);

                dmPrincipal.InsertLancamentoDet(m, 0);

                IdDetalhe := ListaLancamentoDets[m].IdLancamentoDet;

                Inc(IdDetalhe);
                Inc(m);

                QryAuxDet.Next;
              end;

              { PARCELAS }
              QryAuxParc.Close;
              QryAuxParc.SQL.Clear;

              QryAuxParc.SQL.Text := 'SELECT * FROM LANCAMENTO_PARC ' +
                                     '  WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                                     '    AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ' +
                                     '    AND FLG_STATUS <> ' + QuotedStr('C') +
                                     'ORDER BY ID_LANCAMENTO_PARC';

              QryAuxParc.Params.ParamByName('COD_LANCAMENTO').Value := QryAuxOD.FieldByName('COD_LANCAMENTO').AsInteger;
              QryAuxParc.Params.ParamByName('ANO_LANCAMENTO').Value := QryAuxOD.FieldByName('ANO_LANCAMENTO').AsInteger;
              QryAuxParc.Open;

              NumParc   := 1;

              QryAuxParc.First;
              k := 0;

              while not QryAuxParc.Eof do
              begin
                DadosLancamentoParc := LancamentoParc.Create;

                DadosLancamentoParc.IdLancamentoParc := IdParcela;
                DadosLancamentoParc.IdFormaPagto     := QryAuxParc.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger;
                DadosLancamentoParc.DataLancParc     := GerarDataDespesaRecorrente(QryAuxParc.FieldByName('DATA_LANCAMENTO_PARC').AsDateTime);
                DadosLancamentoParc.NumParcela       := NumParc;
                DadosLancamentoParc.DataVencimento   := GerarDataDespesaRecorrente(QryAuxParc.FieldByName('DATA_VENCIMENTO').AsDateTime);
                DadosLancamentoParc.VlrPrevisto      := QryAuxParc.FieldByName('VR_PARCELA_PREV').AsCurrency;
                DadosLancamentoParc.Observacao       := QryAuxParc.FieldByName('OBS_LANCAMENTO_PARC').AsString;
                DadosLancamentoParc.CadIdUsuario     := vgUsu_Id;
                DadosLancamentoParc.DataCadastro     := Now;

                ListaLancamentoParcs.Add(DadosLancamentoParc);

                dmPrincipal.InsertLancamentoParc(k, 0);

                IdParcela := ListaLancamentoParcs[k].IdLancamentoParc;

                Inc(IdParcela);
                Inc(NumParc);
                Inc(k);

                QryAuxParc.Next;
              end;

              { LOG }
              QryAuxLog.Close;
              QryAuxLog.SQL.Clear;
              QryAuxLog.SQL.Text := 'INSERT INTO USUARIO_LOG (ID_USUARIO_LOG, ID_USUARIO, ID_MODULO_FK, ' +
                                    '                         TIPO_LANCAMENTO, CODIGO_LAN, ANO_LAN, ID_ORIGEM, ' +
                                    '                         TABELA_ORIGEM, TIPO_ACAO, OBS_USUARIO_LOG) ' +
                                    '                 VALUES (:ID_USUARIO_LOG, :ID_USUARIO, :ID_MODULO, ' +
                                    '                         :TIPO_LANCAMENTO, :CODIGO_LAN, :ANO_LAN, :ID_ORIGEM, ' +
                                    '                         :TABELA_ORIGEM, :TIPO_ACAO, :OBS_USUARIO_LOG)';

              QryAuxLog.Params.ParamByName('ID_USUARIO_LOG').Value  := BS.ProximoId('ID_USUARIO_LOG', 'USUARIO_LOG');
              QryAuxLog.Params.ParamByName('ID_USUARIO').Value      := vgUsu_Id;
              QryAuxLog.Params.ParamByName('ID_MODULO').Value       := 2;
              QryAuxLog.Params.ParamByName('TIPO_LANCAMENTO').Value := 'D';
              QryAuxLog.Params.ParamByName('CODIGO_LAN').Value      := vgLanc_Codigo;
              QryAuxLog.Params.ParamByName('ANO_LAN').Value         := vgLanc_Ano;
              QryAuxLog.Params.ParamByName('ID_ORIGEM').Value       := Null;
              QryAuxLog.Params.ParamByName('TABELA_ORIGEM').Value   := Null;
              QryAuxLog.Params.ParamByName('TIPO_ACAO').Value       := 'I';
              QryAuxLog.Params.ParamByName('OBS_USUARIO_LOG').Value := 'Inclusão automática de Lançamentos Recorrentes.';

              QryAuxLog.ExecSQL;

              FinalizarComponenteLancamento;

              if vgConGER.InTransaction then
                vgConGER.Commit;

              if vgConSISTEMA.InTransaction then
                vgConSISTEMA.Commit;
            except
              on E: Exception do
              begin
                if vgConGER.InTransaction then
                  vgConGER.Rollback;

                if vgConSISTEMA.InTransaction then
                  vgConSISTEMA.Rollback;

                lContinuar := False;

                if Trim(Msg) = '' then
                  Msg := E.Message
                else
                   Msg := Msg + #13#10 + E.Message;
              end
            end;

            QryAuxOD.Next;
          end;
        end;

        { ATUALIZA FLG_REPLICADO DA DESPESA ORIGINAL }
        if lContinuar then
        begin
          try
            if vgConSISTEMA.Connected then
              vgConSISTEMA.StartTransaction;

            QryAuxOD.Close;
            QryAuxOD.SQL.Clear;
            QryAuxOD.SQL.Text := 'UPDATE OUTRADESPESA_FUNC ' +
                                 '   SET FLG_REPLICADO = :FLG_REPLICADO ' +
                                 ' WHERE ID_OUTRADESPESA_FUNC = :ID_OUTRADESPESA_FUNC';
            QryAuxOD.Params.ParamByName('FLG_REPLICADO').Value        := 'S';
            QryAuxOD.Params.ParamByName('ID_OUTRADESPESA_FUNC').Value := IdOldOD;
            QryAuxOD.ExecSQL;

            if vgConSISTEMA.InTransaction then
              vgConSISTEMA.Commit;
          except
            on E: Exception do
            begin
              if vgConSISTEMA.InTransaction then
                vgConSISTEMA.Rollback;

              lContinuar := False;

              if Trim(Msg) = '' then
                Msg := E.Message
              else
                 Msg := Msg + #13#10 + E.Message;
            end;
          end;
        end;

        QryOD.Next;
      end;

      if lContinuar then
      begin
        try
          if vgConSISTEMA.Connected then
            vgConSISTEMA.StartTransaction;

          Close;
          Clear;
          Text := 'UPDATE CONFIGURACAO SET DATA_ULT_ATULANCOLAB = :DATA_ULT_ATULANCOLAB';
          Params.ParamByName('DATA_ULT_ATULANCOLAB').Value := Date;
          ExecSQL;

          if vgConSISTEMA.InTransaction then
            vgConSISTEMA.Commit;
        except
          on E: Exception do
          begin
            if vgConSISTEMA.InTransaction then
              vgConSISTEMA.Rollback;

            lContinuar := False;

            if Trim(Msg) = '' then
              Msg := E.Message
            else
               Msg := Msg + #13#10 + E.Message;
          end;
        end;
      end
      else
      begin
        Msg := 'Erro ao atualizar as LANÇAMENTOS RECORRENTES. ' + #13#10 +
               'Por favor, entre em contato com o Suporte. ' + #13#10 +
               Msg;

        GravarLogErro(Msg);

        Application.MessageBox(PChar(Msg), 'Erro', MB_OK + MB_ICONERROR);
      end;
    end;
  end;

  pdlgDespesas.Hide;

  FreeAndNil(QryAuxLog);
  FreeAndNil(QryAuxDet);
  FreeAndNil(QryAuxParc);
  FreeAndNil(QryAuxOD);
  FreeAndNil(QryDesp);
  FreeAndNil(QryOD);
end;

procedure TdmPrincipal.AtualizarLancamentosRecorrentes;
var
  QryDesp, QryAuxDet, QryAuxParc, QryAuxLog: TFDQuery;
  IdDetalhe, IdParcela, NumParc, k, m: Integer;
  lAtualizar: Boolean;
  Msg, sQtd: String;
begin
  //Essa rotina serve para gerar os Lançamentos Recorrentes automaticamente
  sQtd := '';
  lAtualizar := False;

  IdDetalhe := 0;
  IdParcela := 0;
  NumParc   := 0;

  QryDesp    := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
  QryAuxDet  := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
  QryAuxParc := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
  QryAuxLog  := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryDesp, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT DATA_ULT_ATUDESPESAS FROM CONFIGURACAO';
    Open;

    lAtualizar := (QryDesp.FieldByName('DATA_ULT_ATUDESPESAS').AsDateTime < Date);

    if lAtualizar then
    begin
      { Pesquisa todas as Despesas Recorrentes que nao possuem replica
        no mes atual (exceto Lancamentos de Colaboradores) }
      cdsAuxLanc.Close;

      if MonthOf(Date) = 1 then
      begin
        cdsAuxLanc.Params.ParamByName('MES_CADASTRO').Value  := 12;
        cdsAuxLanc.Params.ParamByName('ANO_CADASTRO').Value  := (YearOf(Date) - 1);
      end
      else
      begin
        cdsAuxLanc.Params.ParamByName('MES_CADASTRO').Value  := (MonthOf(Date) - 1);
        cdsAuxLanc.Params.ParamByName('ANO_CADASTRO').Value  := YearOf(Date);
      end;

      cdsAuxLanc.Open;
      cdsAuxLanc.Last;

      sQtd := IntToStr(cdsAuxLanc.RecordCount);

      lAtualizar := cdsAuxLanc.RecordCount > 0;
    end;

    if lAtualizar then
    begin
      pdlgDespesas.Min := 0;
      pdlgDespesas.Show;
      pdlgDespesas.Caption := 'Atualização de LANÇAMENTOS RECORRENTES';
      pdlgDespesas.Max := cdsAuxLanc.RecordCount;

      try
        if vgConSISTEMA.Connected then
          vgConSISTEMA.StartTransaction;

        if vgConGER.Connected then
          vgConGER.StartTransaction;

        cdsAuxLanc.First;

        vgLanc_Codigo := dmGerencial.ProximoCodigoLancamento;
        vgLanc_Ano := YearOf(Date);

        IdDetalhe := BS.ProximoId('ID_LANCAMENTO_DET', 'LANCAMENTO_DET');
        IdParcela := BS.ProximoId('ID_LANCAMENTO_PARC', 'LANCAMENTO_PARC');

        while not cdsAuxLanc.Eof do
        begin
          InicializarComponenteLancamento;

          pdlgDespesas.Text := 'Atualizando ' + IntToStr(cdsAuxLanc.RecNo) + ' de ' + sQtd + ' Lançamentos Recorrentes...';
          pdlgDespesas.Position := cdsAuxLanc.RecNo;

          //Copia as Despesas
          { LANCAMENTO }
          DadosLancamento := Lancamento.Create;

          DadosLancamento.TipoLancamento     := cdsAuxLanc.FieldByName('TIPO_LANCAMENTO').AsString;
          DadosLancamento.TipoCadastro       := 'A';

          if cdsAuxLanc.FieldByName('ID_CLIENTE_FORNECEDOR_FK').IsNull then
            DadosLancamento.IdCliFor         := 0
          else
            DadosLancamento.IdCliFor         := cdsAuxLanc.FieldByName('ID_CLIENTE_FORNECEDOR_FK').AsInteger;

            if cdsAuxLanc.FieldByName('ID_CATEGORIA_DESPREC_FK').IsNull then
              DadosLancamento.IdCategoria    := 0
            else
              DadosLancamento.IdCategoria    := cdsAuxLanc.FieldByName('ID_CATEGORIA_DESPREC_FK').AsInteger;

            if cdsAuxLanc.FieldByName('ID_SUBCATEGORIA_DESPREC_FK').IsNull then
              DadosLancamento.IdSubCategoria := 0
            else
              DadosLancamento.IdSubCategoria := cdsAuxLanc.FieldByName('ID_SUBCATEGORIA_DESPREC_FK').AsInteger;

          DadosLancamento.FlgIR              := cdsAuxLanc.FieldByName('FLG_IMPOSTORENDA').AsString;
          DadosLancamento.FlgPessoal         := cdsAuxLanc.FieldByName('FLG_PESSOAL').AsString;
          DadosLancamento.FlgFlutuante       := cdsAuxLanc.FieldByName('FLG_FLUTUANTE').AsString;
          DadosLancamento.FlgAuxiliar        := cdsAuxLanc.FieldByName('FLG_AUXILIAR').AsString;
          DadosLancamento.FlgForaFechCaixa   := cdsAuxLanc.FieldByName('FLG_FORAFECHCAIXA').AsString;
          DadosLancamento.IdNatureza         := cdsAuxLanc.FieldByName('ID_NATUREZA_FK').AsInteger;
          DadosLancamento.QtdParcelas        := cdsAuxLanc.FieldByName('QTD_PARCELAS').AsInteger;
          DadosLancamento.VlrTotalPrev       := cdsAuxLanc.FieldByName('VR_TOTAL_PREV').AsCurrency;
          DadosLancamento.VlrTotalJuros      := 0;
          DadosLancamento.VlrTotalDesconto   := 0;
          DadosLancamento.VlrTotalPago       := 0;
          DadosLancamento.CadIdUsuario       := vgUsu_Id;
          DadosLancamento.DataCadastro       := Now;
          DadosLancamento.DataLancamento     := GerarDataDespesaRecorrente(cdsAuxLanc.FieldByName('DATA_LANCAMENTO').AsDateTime);
          DadosLancamento.FlgRecorrente      := 'S';
          DadosLancamento.FlgReplicado       := 'N';
          DadosLancamento.IdEquivalencia     := cdsAuxLanc.FieldByName('ID_CARNELEAO_EQUIVALENCIA_FK').AsInteger;
          DadosLancamento.Observacao         := cdsAuxLanc.FieldByName('OBSERVACAO').AsString;

          ListaLancamentos.Add(DadosLancamento);

          dmPrincipal.InsertLancamento(0);

          vgLanc_Codigo := ListaLancamentos[0].CodLancamento;
          vgLanc_Ano    := ListaLancamentos[0].AnoLancamento;

          { DETALHE }
          QryAuxDet.Close;
          QryAuxDet.SQL.Clear;

          QryAuxDet.SQL.Text := 'SELECT * FROM LANCAMENTO_DET ' +
                                 '  WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                                 '    AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ' +
                                 '    AND FLG_CANCELADO = ' + QuotedStr('N') +
                                 'ORDER BY ID_LANCAMENTO_DET';

          QryAuxDet.Params.ParamByName('COD_LANCAMENTO').Value := cdsAuxLanc.FieldByName('COD_LANCAMENTO').AsInteger;
          QryAuxDet.Params.ParamByName('ANO_LANCAMENTO').Value := cdsAuxLanc.FieldByName('ANO_LANCAMENTO').AsInteger;
          QryAuxDet.Open;

          QryAuxDet.First;
          m := 0;

          while not QryAuxDet.Eof do
          begin
            DadosLancamentoDet := LancamentoDet.Create;

            DadosLancamentoDet.IdLancamentoDet   := IdDetalhe;
            DadosLancamentoDet.IdItem            := QryAuxDet.FieldByName('ID_ITEM_FK').AsInteger;
            DadosLancamentoDet.Quantidade        := QryAuxDet.FieldByName('QUANTIDADE').AsInteger;
            DadosLancamentoDet.VlrUnitario       := QryAuxDet.FieldByName('VR_UNITARIO').AsCurrency;
            DadosLancamentoDet.VlrTotal          := QryAuxDet.FieldByName('VR_TOTAL').AsCurrency;

            if Trim(QryAuxDet.FieldByName('SELO_ORIGEM').AsString) <> '' then
              DadosLancamentoDet.SeloOrigem      := QryAuxDet.FieldByName('SELO_ORIGEM').AsString;

            if Trim(QryAuxDet.FieldByName('ALEATORIO_ORIGEM').AsString) <> '' then
              DadosLancamentoDet.AleatorioOrigem := QryAuxDet.FieldByName('ALEATORIO_ORIGEM').AsString;

            if Trim(QryAuxDet.FieldByName('TIPO_COBRANCA').AsString) <> '' then
              DadosLancamentoDet.TipoCobranca    := QryAuxDet.FieldByName('TIPO_COBRANCA').AsString;

            if not QryAuxDet.FieldByName('COD_ADICIONAL').IsNull then
              DadosLancamentoDet.CodAdicional    := QryAuxDet.FieldByName('COD_ADICIONAL').AsInteger;

            if not QryAuxDet.FieldByName('EMOLUMENTOS').IsNull then
              DadosLancamentoDet.Emolumentos     := QryAuxDet.FieldByName('EMOLUMENTOS').AsCurrency;

            if not QryAuxDet.FieldByName('FETJ').IsNull then
              DadosLancamentoDet.FETJ            := QryAuxDet.FieldByName('FETJ').AsCurrency;

            if not QryAuxDet.FieldByName('FUNDPERJ').IsNull then
              DadosLancamentoDet.FUNDPERJ        := QryAuxDet.FieldByName('FUNDPERJ').AsCurrency;

            if not QryAuxDet.FieldByName('FUNPERJ').IsNull then
              DadosLancamentoDet.FUNPERJ         := QryAuxDet.FieldByName('FUNPERJ').AsCurrency;

            if not QryAuxDet.FieldByName('FUNARPEN').IsNull then
              DadosLancamentoDet.FUNARPEN        := QryAuxDet.FieldByName('FUNARPEN').AsCurrency;

            if not QryAuxDet.FieldByName('PMCMV').IsNull then
              DadosLancamentoDet.PMCMV           := QryAuxDet.FieldByName('PMCMV').AsCurrency;

            if not QryAuxDet.FieldByName('ISS').IsNull then
              DadosLancamentoDet.ISS             := QryAuxDet.FieldByName('ISS').AsCurrency;

            if not QryAuxDet.FieldByName('MUTUA').IsNull then
              DadosLancamentoDet.Mutua           := QryAuxDet.FieldByName('MUTUA').AsCurrency;

            if not QryAuxDet.FieldByName('ACOTERJ').IsNull then
              DadosLancamentoDet.Acoterj         := QryAuxDet.FieldByName('ACOTERJ').AsCurrency;

            if not QryAuxDet.FieldByName('NUM_PROTOCOLO').IsNull then
              DadosLancamentoDet.NumProtocolo    := QryAuxDet.FieldByName('NUM_PROTOCOLO').AsInteger;

            ListaLancamentoDets.Add(DadosLancamentoDet);

            dmPrincipal.InsertLancamentoDet(m, 0);

            IdDetalhe := ListaLancamentoDets[m].IdLancamentoDet;

            Inc(IdDetalhe);
            Inc(m);

            QryAuxDet.Next;
          end;

          { PARCELAS }
          QryAuxParc.Close;
          QryAuxParc.SQL.Clear;

          QryAuxParc.SQL.Text := 'SELECT * FROM LANCAMENTO_PARC ' +
                                 '  WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                                 '    AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ' +
                                 '    AND FLG_STATUS <> ' + QuotedStr('C') +
                                 'ORDER BY ID_LANCAMENTO_PARC';

          QryAuxParc.Params.ParamByName('COD_LANCAMENTO').Value := cdsAuxLanc.FieldByName('COD_LANCAMENTO').AsInteger;
          QryAuxParc.Params.ParamByName('ANO_LANCAMENTO').Value := cdsAuxLanc.FieldByName('ANO_LANCAMENTO').AsInteger;
          QryAuxParc.Open;

          NumParc   := 1;

          QryAuxParc.First;
          k := 0;

          while not QryAuxParc.Eof do
          begin
            DadosLancamentoParc := LancamentoParc.Create;

            DadosLancamentoParc.IdLancamentoParc := IdParcela;
            DadosLancamentoParc.IdFormaPagto     := QryAuxParc.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger;
            DadosLancamentoParc.DataLancParc     := GerarDataDespesaRecorrente(QryAuxParc.FieldByName('DATA_LANCAMENTO_PARC').AsDateTime);
            DadosLancamentoParc.NumParcela       := NumParc;
            DadosLancamentoParc.DataVencimento   := GerarDataDespesaRecorrente(QryAuxParc.FieldByName('DATA_VENCIMENTO').AsDateTime);
            DadosLancamentoParc.VlrPrevisto      := QryAuxParc.FieldByName('VR_PARCELA_PREV').AsCurrency;
            DadosLancamentoParc.Observacao       := QryAuxParc.FieldByName('OBS_LANCAMENTO_PARC').AsString;
            DadosLancamentoParc.CadIdUsuario     := vgUsu_Id;
            DadosLancamentoParc.DataCadastro     := Now;

            ListaLancamentoParcs.Add(DadosLancamentoParc);

            dmPrincipal.InsertLancamentoParc(k, 0);

            IdParcela := ListaLancamentoParcs[k].IdLancamentoParc;

            Inc(IdParcela);
            Inc(NumParc);
            Inc(k);

            QryAuxParc.Next;
          end;

          { LOG }
          QryAuxLog.Close;
          QryAuxLog.SQL.Clear;
          QryAuxLog.SQL.Text := 'INSERT INTO USUARIO_LOG (ID_USUARIO_LOG, ID_USUARIO, ID_MODULO_FK, ' +
                                '                         TIPO_LANCAMENTO, CODIGO_LAN, ANO_LAN, ID_ORIGEM, ' +
                                '                         TABELA_ORIGEM, TIPO_ACAO, OBS_USUARIO_LOG) ' +
                                '                 VALUES (:ID_USUARIO_LOG, :ID_USUARIO, :ID_MODULO, ' +
                                '                         :TIPO_LANCAMENTO, :CODIGO_LAN, :ANO_LAN, :ID_ORIGEM, ' +
                                '                         :TABELA_ORIGEM, :TIPO_ACAO, :OBS_USUARIO_LOG)';

          QryAuxLog.Params.ParamByName('ID_USUARIO_LOG').Value  := BS.ProximoId('ID_USUARIO_LOG', 'USUARIO_LOG');
          QryAuxLog.Params.ParamByName('ID_USUARIO').Value      := vgUsu_Id;
          QryAuxLog.Params.ParamByName('ID_MODULO').Value       := 2;
          QryAuxLog.Params.ParamByName('TIPO_LANCAMENTO').Value := 'D';
          QryAuxLog.Params.ParamByName('CODIGO_LAN').Value      := vgLanc_Codigo;
          QryAuxLog.Params.ParamByName('ANO_LAN').Value         := vgLanc_Ano;
          QryAuxLog.Params.ParamByName('ID_ORIGEM').Value       := Null;
          QryAuxLog.Params.ParamByName('TABELA_ORIGEM').Value   := Null;
          QryAuxLog.Params.ParamByName('TIPO_ACAO').Value       := 'I';
          QryAuxLog.Params.ParamByName('OBS_USUARIO_LOG').Value := 'Inclusão automática de Lançamentos Recorrentes.';

          QryAuxLog.ExecSQL;

          { ATUALIZA FLG_REPLICADO DA DESPESA ORIGINAL }
          cdsAuxLanc.Edit;
          cdsAuxLanc.FieldByName('FLG_REPLICADO').AsString := 'S';
          cdsAuxLanc.Post;

          FinalizarComponenteLancamento;

          cdsAuxLanc.Next;
        end;

        cdsAuxLanc.ApplyUpdates(0);

        Close;
        Clear;
        Text := 'UPDATE CONFIGURACAO SET DATA_ULT_ATUDESPESAS = :DATA_ULT_ATUDESPESAS';
        Params.ParamByName('DATA_ULT_ATUDESPESAS').Value := Date;
        ExecSQL;

        if vgConGER.InTransaction then
          vgConGER.Commit;

        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Commit;
      except
        on E: Exception do
        begin
          if vgConGER.InTransaction then
            vgConGER.Rollback;

          if vgConSISTEMA.InTransaction then
            vgConSISTEMA.Rollback;

          GravarLogErro(E.Message);

          Msg := 'Erro ao atualizar as LANÇAMENTOS RECORRENTES. ' + #13#10 +
                 'Por favor, entre em contato com o Suporte. ' + #13#10 +
                 E.Message;
          Application.MessageBox(PChar(Msg), 'Erro', MB_OK + MB_ICONERROR);
        end;
      end;
    end;
  end;

  pdlgDespesas.Hide;

  FreeAndNil(QryAuxLog);
  FreeAndNil(QryAuxDet);
  FreeAndNil(QryAuxParc);
  FreeAndNil(QryDesp);
end;

function TdmPrincipal.AtualizarEstoqueProduto(IdItem: Integer; UltValor: Currency): String;
var
  Qtd: Integer;
  QryAux: TFDQuery;
begin
  Result := '';

  Qtd := 0;

  QryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  //Atualiza Estoque Atual do Item
  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    QryAux.Close;
    QryAux.SQL.Clear;
    QryAux.SQL.Text := 'SELECT I.ID_ITEM AS ID, ' +
                       '       (SELECT SUM(HI1.QUANTIDADE) AS ENTRADAS ' +
                       '          FROM HISTORICO_ITEM HI1 ' +
                       '         WHERE HI1.ID_ITEM_FK = I.ID_ITEM ' +
                       '           AND HI1.FLG_TIPO_HISTORICO = ' + QuotedStr('E') +
                       '        GROUP BY HI1.ID_ITEM_FK) AS TOT_ENTRADAS, ' +
                       '       (SELECT SUM(HI2.QUANTIDADE) AS SAIDAS ' +
                       '          FROM HISTORICO_ITEM HI2 ' +
                       '         WHERE HI2.ID_ITEM_FK = I.ID_ITEM ' +
                       '           AND HI2.FLG_TIPO_HISTORICO = ' + QuotedStr('S') +
                       '        GROUP BY HI2.ID_ITEM_FK) AS TOT_SAIDAS ' +
                       '  FROM ITEM I ' +
                       ' WHERE I.ID_ITEM = :ID_ITEM ' +
                       'GROUP BY ID_ITEM';
    QryAux.Params.ParamByName('ID_ITEM').Value := IdItem;
    QryAux.Open;

    //Calculo do Estoque
    Qtd := (QryAux.FieldByName('TOT_ENTRADAS').AsInteger - QryAux.FieldByName('TOT_SAIDAS').AsInteger);

    QryAux.Close;
    QryAux.SQL.Clear;
    QryAux.SQL.Text := 'UPDATE ITEM ' +
                       '   SET ESTOQUE_ATUAL   = :ESTOQUE_ATUAL ';

    if UltValor >= 0 then
    begin
      QryAux.SQL.Add(', ULT_VALOR_CUSTO = :ULT_VALOR_CUSTO ');

      QryAux.Params.ParamByName('ULT_VALOR_CUSTO').Value := UltValor;
    end;

    QryAux.SQL.Add(' WHERE ID_ITEM = :ID_ITEM');

    QryAux.Params.ParamByName('ESTOQUE_ATUAL').Value := Qtd;
    QryAux.Params.ParamByName('ID_ITEM').Value       := IdItem;
    QryAux.ExecSQL;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := '';
  end;

  FreeAndNil(QryAux);
end;

function TdmPrincipal.CalcularDataPagamentoFunc: TDateTime;
var
  QryConfig: TFDQuery;
  j: Integer;
  dUltDataPagto, dtProxDataPagto: TDateTime;
begin
  { Essa funcao calcula a Data do Pagamento dos funcionarios de acordo com o
    Dia Util base cadastrado da tabela de Configuracao }
  j := 0;

  dUltDataPagto   := 0;
  dtProxDataPagto := 0;

  QryConfig := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryConfig, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT DIA_PAGAMENTOFUNC FROM CONFIGURACAO';
    Open;
  end;

  if QryConfig.FieldByName('DIA_PAGAMENTOFUNC').AsInteger = 0 then  //Ultimo Dia Util
  begin
    dUltDataPagto := EndOfTheMonth(Date);

    if DayOfWeek(dUltDataPagto) = 6 then  //Sabado
      dUltDataPagto := IncDay(dUltDataPagto, -1)
    else if DayOfWeek(dUltDataPagto) = 7 then  //Domingo
      dUltDataPagto := IncDay(dUltDataPagto, -2);
  end
  else
  begin  //1º, 2º, 3º, 4º ou 5º Dia Util
    for j := 1 to QryConfig.FieldByName('DIA_PAGAMENTOFUNC').AsInteger do
    begin
      if j = 1 then
        dUltDataPagto := StartOfTheMonth(Date)
      else
        dUltDataPagto := IncDay(dUltDataPagto);

      if DayOfWeek(dUltDataPagto) = 5 then  //Sabado
        dUltDataPagto := IncDay(dUltDataPagto, 2)
      else if DayOfWeek(dUltDataPagto) = 6 then  //Domingo
        dUltDataPagto := IncDay(dUltDataPagto, 1);
    end;
  end;

  if Date > dUltDataPagto then
  begin
    dtProxDataPagto := IncMonth(Date, 1);

    if QryConfig.FieldByName('DIA_PAGAMENTOFUNC').AsInteger = 0 then  //Ultimo Dia Util
    begin
      dUltDataPagto := EndOfTheMonth(Date);

      if DayOfWeek(dUltDataPagto) = 5 then  //Sabado
        dUltDataPagto := IncDay(dUltDataPagto, -1)
      else if DayOfWeek(dUltDataPagto) = 6 then  //Domingo
        dUltDataPagto := IncDay(dUltDataPagto, -2);
    end
    else
    begin  //1º, 2º, 3º, 4º ou 5º Dia Util
      for j := 1 to QryConfig.FieldByName('DIA_PAGAMENTOFUNC').AsInteger do
      begin
        if j = 1 then
          dtProxDataPagto := StartOfTheMonth(dtProxDataPagto)
        else
          dtProxDataPagto := IncDay(dtProxDataPagto);

        if DayOfWeek(dtProxDataPagto) = 6 then  //Sabado
          dtProxDataPagto := IncDay(dtProxDataPagto, 2)
        else if DayOfWeek(dtProxDataPagto) = 7 then  //Domingo
          dtProxDataPagto := IncDay(dtProxDataPagto, 1);
      end;
    end;
  end
  else
    dtProxDataPagto := dUltDataPagto;

  Result := dtProxDataPagto;

  FreeAndNil(QryConfig);
end;

procedure TdmPrincipal.CancelarImportacoesDuplicadas(dDataImportacao: TDatetime);
var
  QryDupl, QryLanc, QryUp: TFDQuery;
  iUltIdFirmas, iUltIdRecibo, iUltIdReciboF: Integer;
begin
  iUltIdFirmas  := 0;
  iUltIdRecibo  := 0;
  iUltIdReciboF := 0;

  try
    QryDupl := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
    QryLanc := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
    QryUp   := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    QryDupl.Close;
    QryDupl.SQL.Clear;

    QryDupl.SQL.Text := 'SELECT L.NUM_RECIBO, ' +
                        '       L.ID_ORIGEM, ' +
                        '       L.DATA_LANCAMENTO, ' +
                        '       L.TIPO_LANCAMENTO, ' +
                        '       L.ID_SISTEMA_FK, ' +
                        '       L.ID_NATUREZA_FK ' +
                        '  FROM LANCAMENTO L ' +
                        ' INNER JOIN LANCAMENTO_PARC LP ' +
                        '    ON L.COD_LANCAMENTO = LP.COD_LANCAMENTO_FK ' +
                        '   AND L.ANO_LANCAMENTO = LP.ANO_LANCAMENTO_FK ' +
                        ' WHERE L.TIPO_LANCAMENTO = ' + QuotedStr('R') +
                        '   AND L.ID_ORIGEM IS NOT NULL ' +
                        '   AND L.DATA_LANCAMENTO >= :DATA_LANCAMENTO ' +
                        'GROUP BY L.NUM_RECIBO, ' +
                        '         L.ID_ORIGEM, ' +
                        '         L.DATA_LANCAMENTO, ' +
                        '         L.TIPO_LANCAMENTO, ' +
                        '         L.ID_SISTEMA_FK, ' +
                        '         L.ID_NATUREZA_FK ' +
                        'HAVING COUNT(L.ID_ORIGEM) > 1 ' +
                        'ORDER BY L.ID_NATUREZA_FK ASC, ' +
                        '         L.ID_SISTEMA_FK ASC, ' +
                        '         L.ID_ORIGEM ASC, ' +
                        '         L.NUM_RECIBO ASC';

    QryDupl.Params.ParamByName('DATA_LANCAMENTO').Value := dDataImportacao;
    QryDupl.Open;

    QryDupl.First;

    while not QryDupl.Eof do
    begin
      if QryDupl.FieldByName('ID_NATUREZA_FK').AsInteger = 4 then  //Firmas
      begin
        if iUltIdFirmas = 0 then
          iUltIdFirmas := QryDupl.FieldByName('ID_ORIGEM').AsInteger;
      end
      else
      begin
        if QryDupl.FieldByName('ID_SISTEMA_FK').AsInteger = 1 then  //Firmas
        begin
          if iUltIdReciboF = 0 then
            iUltIdReciboF := QryDupl.FieldByName('ID_ORIGEM').AsInteger;
        end
        else
        begin
          if iUltIdRecibo = 0 then
            iUltIdRecibo := QryDupl.FieldByName('ID_ORIGEM').AsInteger;  //Demais sistemas
        end;
      end;

      QryLanc.Close;
      QryLanc.SQL.Clear;

      QryLanc.SQL.Text := 'SELECT FIRST 1 L.COD_LANCAMENTO, ' +
                          '       L.ANO_LANCAMENTO ' +
                          '  FROM LANCAMENTO L ' +
                          ' WHERE L.NUM_RECIBO = :NUM_RECIBO ' +
                          '   AND L.ID_ORIGEM = :ID_ORIGEM ' +
                          '   AND L.DATA_LANCAMENTO = :DATA_LANCAMENTO ' +
                          '   AND L.TIPO_LANCAMENTO = :TIPO_LANCAMENTO';

      QryLanc.Params.ParamByName('NUM_RECIBO').Value      := QryDupl.FieldByName('NUM_RECIBO').AsInteger;
      QryLanc.Params.ParamByName('ID_ORIGEM').Value       := QryDupl.FieldByName('ID_ORIGEM').AsInteger;
      QryLanc.Params.ParamByName('DATA_LANCAMENTO').Value := QryDupl.FieldByName('DATA_LANCAMENTO').AsDateTime;
      QryLanc.Params.ParamByName('TIPO_LANCAMENTO').Value := QryDupl.FieldByName('TIPO_LANCAMENTO').AsString;
      QryLanc.Open;

      if QryLanc.RecordCount > 0 then
      begin
        if vgConSISTEMA.Connected then
          vgConSISTEMA.StartTransaction;

        QryUp.Close;
        QryUp.SQL.Clear;

        QryUp.SQL.Text := 'UPDATE LANCAMENTO_DET ' +
                          '   SET FLG_CANCELADO = ' + QuotedStr('S') +
                          ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                          '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO';

        QryUp.Params.ParamByName('COD_LANCAMENTO').Value := QryLanc.FieldByName('COD_LANCAMENTO').AsInteger;
        QryUp.Params.ParamByName('ANO_LANCAMENTO').Value := QryLanc.FieldByName('ANO_LANCAMENTO').AsInteger;
        QryUp.ExecSQL;

        QryUp.Close;
        QryUp.SQL.Clear;

        QryUp.SQL.Text := 'UPDATE LANCAMENTO_PARC ' +
                          '   SET FLG_STATUS = ' + QuotedStr('C') + ', ' +
                          '       DATA_CANCELAMENTO = CURRENT_DATE, ' +
                          '       CANCEL_ID_USUARIO = :ID_USUARIO ' +
                          ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                          '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO';

        QryUp.Params.ParamByName('COD_LANCAMENTO').Value := QryLanc.FieldByName('COD_LANCAMENTO').AsInteger;
        QryUp.Params.ParamByName('ANO_LANCAMENTO').Value := QryLanc.FieldByName('ANO_LANCAMENTO').AsInteger;
        QryUp.Params.ParamByName('ID_USUARIO').Value     := vgUsu_Id;
        QryUp.ExecSQL;

        QryUp.Close;
        QryUp.SQL.Clear;

        QryUp.SQL.Text := 'UPDATE LANCAMENTO ' +
                          '   SET FLG_CANCELADO = ' + QuotedStr('S') + ', ' +
                          '       DATA_CANCELAMENTO = CURRENT_DATE, ' +
                          '       CANCEL_ID_USUARIO = :ID_USUARIO ' +
                          ' WHERE COD_LANCAMENTO = :COD_LANCAMENTO ' +
                          '   AND ANO_LANCAMENTO = :ANO_LANCAMENTO';

        QryUp.Params.ParamByName('COD_LANCAMENTO').Value := QryLanc.FieldByName('COD_LANCAMENTO').AsInteger;
        QryUp.Params.ParamByName('ANO_LANCAMENTO').Value := QryLanc.FieldByName('ANO_LANCAMENTO').AsInteger;
        QryUp.Params.ParamByName('ID_USUARIO').Value     := vgUsu_Id;
        QryUp.ExecSQL;
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      QryDupl.Next;
    end;

    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    QryUp.Close;
    QryUp.SQL.Clear;

    QryUp.SQL.Text := 'UPDATE CONFIGURACAO ' +
                      '   SET ID_USUATUALIZAREC = NULL, ' +
                      '       ID_USUATUALIZAFIR = NULL, ' +
                      '       ID_ULTRECIBOATUAL_T = IIF(ID_ULTRECIBOATUAL_T > 0, (CASE WHEN (:ULT_ID_RECIBO > 0) THEN :ULT_ID_RECIBO ELSE 0 END), 0), ' +
                      '       ID_ULTRECIBOATUAL_B = IIF(ID_ULTRECIBOATUAL_B > 0, (CASE WHEN (:ULT_ID_RECIBO_F > 0) THEN :ULT_ID_RECIBO_F ELSE 0 END), 0), ' +
                      '       ID_ULTRECIBOATUALF = IIF(ID_ULTRECIBOATUALF > 0, (CASE WHEN (:ULT_ID_FIRMAS > 0) THEN :ULT_ID_FIRMAS ELSE 0 END), 0)';

    QryUp.Params.ParamByName('ULT_ID_RECIBO').Value   := iUltIdRecibo;
    QryUp.Params.ParamByName('ULT_ID_RECIBO_F').Value := iUltIdReciboF;
    QryUp.Params.ParamByName('ULT_ID_FIRMAS').Value   := iUltIdFirmas;
    QryUp.ExecSQL;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    FreeAndNil(QryDupl);
    FreeAndNil(QryLanc);
    FreeAndNil(QryUp);

    Application.MessageBox('Duplicidades nas importações canceladas com sucesso!.',
                           'Sucesso',
                           MB_OK);
  except
    on E: Exception do
    begin
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      FreeAndNil(QryDupl);
      FreeAndNil(QryLanc);
      FreeAndNil(QryUp);

      Application.MessageBox('Erro ao cancelar duplicidades nas importações.',
                             'Erro',
                             MB_OK + MB_ICONERROR);
    end;
  end;
end;

procedure TdmPrincipal.cdsComissoesCalcFields(DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with qryAux, SQL do
  begin
    //Sistema
    if cdsComissoes.FieldByName('ID_SISTEMA_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT * ' +
              '  FROM SISTEMA ' +
              ' WHERE ID_SISTEMA = :ID_SISTEMA';
      Params.ParamByName('ID_SISTEMA').AsInteger := cdsComissoes.FieldByName('ID_SISTEMA_FK').AsInteger;
      Open;

      cdsComissoes.FieldByName('NOMESISTEMA').AsString := qryAux.FieldByName('NOME_SISTEMA').AsString;
    end;

    //Tipo Ato
    if cdsComissoes.FieldByName('ID_ITEM_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT ID_ITEM, UPPER(DESCR_ITEM) AS DESCR_ITEM ' +
              '  FROM ITEM ' +
              ' WHERE ID_ITEM = :ID_ITEM';
      Params.ParamByName('ID_ITEM').AsInteger := cdsComissoes.FieldByName('ID_ITEM_FK').AsInteger;
      Open;

      cdsComissoes.FieldByName('DESCR_ITEM').AsString := qryAux.FieldByName('DESCR_ITEM').AsString;
    end;
  end;

  FreeAndNil(qryAux);
end;

procedure TdmPrincipal.cdsComissoesPendentesCalcFields(DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with qryAux, SQL do
  begin
    //Sistema
    if cdsComissoesPendentes.FieldByName('ID_SISTEMA_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT * ' +
              '  FROM SISTEMA ' +
              ' WHERE ID_SISTEMA = :ID_SISTEMA';
      Params.ParamByName('ID_SISTEMA').AsInteger := cdsComissoesPendentes.FieldByName('ID_SISTEMA_FK').AsInteger;
      Open;

      cdsComissoesPendentes.FieldByName('NOMESISTEMA').AsString := qryAux.FieldByName('NOME_SISTEMA').AsString;
    end;

    //Tipo Ato
    if cdsComissoesPendentes.FieldByName('ID_ITEM_FK').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT ID_ITEM, UPPER(DESCR_ITEM) AS DESCR_ITEM ' +
              '  FROM ITEM ' +
              ' WHERE ID_ITEM = :ID_ITEM';
      Params.ParamByName('ID_ITEM').AsInteger := cdsComissoesPendentes.FieldByName('ID_ITEM_FK').AsInteger;
      Open;

      cdsComissoesPendentes.FieldByName('DESCR_ITEM').AsString := qryAux.FieldByName('DESCR_ITEM').AsString;
    end;
  end;

  FreeAndNil(qryAux);
end;

procedure TdmPrincipal.cdsComissoesVR_COMISSAOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmPrincipal.cdsOutrasDespVR_OUTRADESPESA_FUNCGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmPrincipal.cdsValesVR_VALEGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmPrincipal.DataModuleCreate(Sender: TObject);
var
  Ini: TIniFile;
  s, sServ, sDB, Msg, Modulos: String;
  lAcaoOk: Boolean;
begin
  lAtualizandoVersao := False;

  vgCorSistema := $000F5CFF;

  vgNomeSistema := 'MONITORAMENTO';

  vgUsaTotalFirmas := False;
  vgUsaTotalRecibo := False;

  //Verifica se existe atualizacao disponivel
  dmGerencial.AtualizaExecutavelSistema('www\cartorios\StatusTotal\Monitoramento',
                                        'MONITORAMENTO.exe',
                                        'MONITORAMENTO-update.exe',
                                        1);

  vgAtS_VersaoSistema := dmGerencial.RetornarVersaoSistema;  { SEMPRE ATUALIZAR O NUMERO DA VERSAO DO SISTEMA AQUI }

  lAcaoOk := False;

  Msg     := '';
  s       := '';
  sServ   := '';
  sDB     := '';
  Modulos := '';

  Ini := TIniFile.Create(vgDirExec + 'Config.ini');

  try
    { BANCO DE DADOS DE MONITORAMENTO }
    conSISTEMA.Close;

    sDB := Ini.ReadString('DB', 'PATH', '*');
    conSISTEMA.Params.Values['Database'] := dmGerencial.DecryptStr(sDB);

    sServ := Ini.ReadString('DB', 'SERV', '*');
    conSISTEMA.Params.Values['Server'] := dmGerencial.DecryptStr(sServ);  //localhost OU o nome do servidor OU o IP do servidor

    conSISTEMA.Params.Values['Protocol']     := 'Remote';  //Local ou Remote
    conSISTEMA.Params.Values['SQLDialect']   := '3';
    conSISTEMA.Params.Values['CharacterSet'] := 'ISO8859_1';

    conSISTEMA.Connected := True;

    { Preencher as variaveis do Sistema }
    //GERAL
    vgConSISTEMA    := conSISTEMA;
    vgConSISTEMAAux := conSISTEMA;

    s := Ini.ReadString('MD', 'TBS', '*');

    Modulos := dmGerencial.DecryptStr(s);

    { FOLERG (quando nao houver habilitacao para o modulo, substituir por asterisco)
      F = Financeiro / O = Oficios / L = Folhas de Seguranca / E = Etiquetas /
      R = Relatorios / G = Configuracoes }

    //MODULO
    vgMod_Financeiro    := Copy(Modulos, 1, 1) = 'F';
    vgMod_Oficios       := Copy(Modulos, 2, 1) = 'O';
    vgMod_Folhas        := Copy(Modulos, 3, 1) = 'L';
    vgMod_Etiquetas     := Copy(Modulos, 4, 1) = 'E';
    vgMod_Relatorios    := Copy(Modulos, 5, 1) = 'R';
    vgMod_Configuracoes := Copy(Modulos, 6, 1) = 'G';

    //CLIENTE USA O SISTEMA TOTAL RECIBO
    vgUsaTotalRecibo := (Trim(Ini.ReadString('DBR', 'SERV', '')) <> '*');

    //CLIENTE USA O SISTEMA TOTAL FIRMAS
    vgUsaTotalFirmas := (Trim(Ini.ReadString('DBF', 'SERV', '')) <> '*') or
                        (Trim(Ini.ReadString('DBFA', 'SERV', '')) <> '*');

    lAcaoOk := True;
  except
    on E: Exception do
    begin
      GravarLogErro(E.Message);

      Msg := 'Erro ao tentar conectar ao Banco de Dados MONITORAMENTO. ' + #13#10 +
             'Por favor, entre em contato com o Suporte. ' + #13#10 +
             E.Message;
      Application.MessageBox(PChar(Msg), 'Erro', MB_OK + MB_ICONERROR);
      Ini.Free;
      Application.Terminate;
    end;
  end;

  if lAcaoOk then
  begin
    if FileExists('C:\FNMASTOTAL') then
    begin
      { Preencher as variaveis do Sistema }
      //USUARIO
      vgUsu_Id                := 1;
      vgUsu_Nome              := 'SUPORTE';
      vgUsu_CPF               := '999.999.999-99';
      vgUsu_Qualificacao      := 'SUPORTE';
      vgUsu_FlgMaster         := 'S';
      vgUsu_FlgOficial        := 'N';
      vgUsu_Matricula         := '';
      vgUsu_FlgSuporte        := 'S';
      vgUsu_FlgVisualizouAtu  := 'S';
      vgUsu_FlgSexo           := 'I';
      vgUsu_IdFuncionario     := 0;
      vgUsu_FlgAtivo          := 'S';
      vgUsu_FlgAssinaNegativa := 'S';
      vgUsu_DataCadastro      := StrToDate('01/01/2016');
      vgUsu_DataInativacao    := 0;

      vgLoginOk := True;
    end
    else
    begin
      try
        Application.CreateForm(TFLogin, FLogin);
        FLogin.sNomeSistema := 'MONITORAMENTO';
        FLogin.ShowModal;
      finally
        FLogin.Free;
      end;
    end;
  end;

  if vgLoginOk then
  begin
    //Verifica no banco de dados qual a ultima versao cadastrada do sistema
    qryAtualizacao.Close;
    qryAtualizacao.Open;

    BS.DeterminarVersaoSistema;

    //Preenche as variaveis globais do sistema
    BS.PreencherVariaveisSistema;

    //Abre a tela principal do Sistema
    dmGerencial.CriarForm(TFPrincipal, FPrincipal);
  end
  else
    Application.Terminate;
end;

function TdmPrincipal.ExisteCadastroComissaoF: Boolean;
var
  QryC: TFDQuery;
begin
  //Verifica se Existe algum cadastro de regra de comissao para funcionario
  Result := False;

  QryC := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  QryC.Close;
  QryC.SQL.Clear;
  QryC.SQL.Text := 'SELECT FIRST 1 ID_FUNCIONARIO_COMISSAO ' +
                   '  FROM FUNCIONARIO_COMISSAO';
  QryC.Open;

  Result := (QryC.RecordCount > 0);

  FreeAndNil(QryC);
end;

function TdmPrincipal.ExistemComissoesPendentes: Boolean;
var
  iMes, iAno: Integer;
begin
  //Verifica se existem Comissoes pendentes ate o mes de referencia
  Result := False;

  iMes := 0;
  iAno := 0;

  RetornaMesAnoPagamentoSal(iMes, iAno);

  //Comissoes nao pagas
  cdsComissoesPendentes.Close;
  cdsComissoesPendentes.Params.ParamByName('MES_REFERENCIA').Value := iMes;
  cdsComissoesPendentes.Params.ParamByName('ANO_REFERENCIA').Value := iAno;
  cdsComissoesPendentes.Params.ParamByName('ID_FUNC01').Value      := 0;
  cdsComissoesPendentes.Params.ParamByName('ID_FUNC02').Value      := 0;
  cdsComissoesPendentes.Open;

  Result := (cdsComissoesPendentes.RecordCount > 0);
end;

function TdmPrincipal.ExistemReceitasPeriodo: Boolean;
var
  iMes, iAno: Integer;
  QryComP: TFDQuery;
begin
  //Verifica se foram recebidas Receitas no mes de referencia
  Result := False;

  iMes := 0;
  iAno := 0;

  RetornaMesAnoPagamentoSal(iMes, iAno);

  QryComP := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  QryComP.Close;
  QryComP.SQL.Clear;

  QryComP.SQL.Text := 'SELECT COUNT(COD_LANCAMENTO) AS QTD ' +
                      '  FROM LANCAMENTO ' +
                       'WHERE EXTRACT(MONTH FROM DATA_CADASTRO) = :MES_REFERENCIA ' +
                      '   AND EXTRACT(YEAR FROM DATA_CADASTRO)  = :ANO_REFERENCIA';

  QryComP.Params.ParamByName('MES_REFERENCIA').Value := iMes;
  QryComP.Params.ParamByName('ANO_REFERENCIA').Value := iAno;
  QryComP.Open;

  Result := (QryComP.FieldByName('QTD').AsInteger > 0);
end;

function TdmPrincipal.FechamentoComissoesPendente: Boolean;
var
  iMes, iAno: Integer;
  QryComP: TFDQuery;
begin
  //Verifica se o Fechamento de Comissoes foi ou nao realizado
  Result := False;

  iMes := 0;
  iAno := 0;

  RetornaMesAnoPagamentoSal(iMes, iAno);

  QryComP := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  QryComP.Close;
  QryComP.SQL.Clear;

  QryComP.SQL.Text := 'SELECT COUNT(ID_COMISSAO) QTD ' +
                      '  FROM COMISSAO ' +
                      ' WHERE ID_FUNCIONARIO_FK = (CASE WHEN (:ID_FUNC01 = 0) ' +
                      '                                 THEN ID_FUNCIONARIO_FK ' +
                      '                                 ELSE :ID_FUNC02 ' +
                      '                            END) ' +
                      '   AND DATA_PAGAMENTO IS NULL ' +
                      '   AND EXTRACT(MONTH FROM DATA_COMISSAO) = :MES_REFERENCIA ' +
                      '   AND EXTRACT(YEAR FROM DATA_COMISSAO)  = :ANO_REFERENCIA';

  QryComP.Params.ParamByName('MES_REFERENCIA').Value := iMes;
  QryComP.Params.ParamByName('ANO_REFERENCIA').Value := iAno;
  QryComP.Params.ParamByName('ID_FUNC01').Value      := 0;
  QryComP.Params.ParamByName('ID_FUNC02').Value      := 0;
  QryComP.Open;

  Result := (QryComP.FieldByName('QTD').AsInteger > 0);
end;

function TdmPrincipal.FechamentoPagamentosPendente: Boolean;
var
  iMes, iAno: Integer;
  QryPagtoP: TFDQuery;
begin
  //Verifica se o Fechamento de Comissoes foi ou nao realizado
  Result := False;

  iMes := 0;
  iAno := 0;

  RetornaMesAnoPagamentoSal(iMes, iAno);

  QryPagtoP := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  QryPagtoP.Close;
  QryPagtoP.SQL.Clear;

  QryPagtoP.SQL.Text := 'SELECT COUNT(ID_FECHAMENTO_SALARIO) QTD ' +
                        '  FROM FECHAMENTO_SALARIO ' +
                        ' WHERE MES_REFERENCIA = :MES_REFERENCIA ' +
                        '   AND ANO_REFERENCIA = :ANO_REFERENCIA';

  QryPagtoP.Params.ParamByName('MES_REFERENCIA').Value := iMes;
  QryPagtoP.Params.ParamByName('ANO_REFERENCIA').Value := iAno;
  QryPagtoP.Open;

  Result := (QryPagtoP.FieldByName('QTD').AsInteger = 0);
end;

procedure TdmPrincipal.FinalizarComponenteLancamento;
begin
  FreeAndNil(ListaLancamentos);
  FreeAndNil(DadosLancamento);

  FreeAndNil(ListaLancamentoDets);
  FreeAndNil(DadosLancamentoDet);

  FreeAndNil(ListaLancamentoParcs);
  FreeAndNil(DadosLancamentoParc);
end;

function TdmPrincipal.GerarDataDespesaRecorrente(Data: TDateTime): TDateTime;
var
  Dia, Mes, Ano: Word;
  iDia, iMes, iAno: Integer;
  sData: String;
  dData: TDatetime;
begin
  iDia := 0;
  iMes := 0;
  iAno := 0;

  DecodeDate(Data, Ano, Mes, Dia);

  case Mes of
    1:  //Janeiro para Fevereiro
    begin
      iMes := (Mes + 1);
      iAno := Ano;

      if Dia > 28 then
      begin
        if Dia = 29 then
        begin
          if IsLeapYear(Ano) then
            iDia := Dia
          else
            iDia := 28;
        end
        else
          iDia := 28;
      end
      else
        iDia := Dia;
    end;
    12:  //Dezembro para Janeiro
    begin
      iDia := Dia;
      iMes := 1;
      iAno := (Ano + 1);
    end;
    2, 4, 6,  //Fevereiro para Marco - Abril para Maio - Junho para Julho
    7, 9, 11:  //Julho para Agosto - Setembro para Outubro - Novembro para Dezembro
    begin
      iDia := Dia;
      iMes := (Mes + 1);
      iAno := Ano;
    end;
    3, 5, 8, 10:  //Marco para Abril - Maio para Junho - Agosto para Setembro - Outubro para Novembro
    begin
      if Dia = 31 then
        iDia := 30
      else
        iDia := Dia;

      iMes := (Mes + 1);
      iAno := Ano;
    end;
  end;

  sData := dmGerencial.CompletaString(IntToStr(iDia), '0', 'E', 2) +
           '/' +
           dmGerencial.CompletaString(IntToStr(iMes), '0', 'E', 2) +
           '/' +
           dmGerencial.CompletaString(IntToStr(iAno), '0', 'E', 4);

  dData := VarToDateTime(sData);

  if dData < vgDataLancVigente then
  begin
    if Trim(vgSituacaoCaixa) = 'N' then
      Result := dData
    else
      Result := vgDataLancVigente;
  end
  else
    Result := dData;
end;

procedure TdmPrincipal.InicializarComponenteLancamento;
begin
  { LANCAMENTO }
  Lancamento       := TClasseLancamento;
  ListaLancamentos := TList<TDadosClasseLancamento>.Create;

  { LANCAMENTO - DETALHE }
  LancamentoDet       := TClasseLancamentoDet;
  ListaLancamentoDets := TList<TDadosClasseLancamentoDet>.Create;

  { LANCAMENTO - PARCELA }
  LancamentoParc       := TClasseLancamentoParc;
  ListaLancamentoParcs := TList<TDadosClasseLancamentoParc>.Create;
end;

procedure TdmPrincipal.InsertLancamento(Indice: Integer);
var
  QryLanc: TFDQuery;
begin
  QryLanc := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryLanc, SQL do
  begin
    { LANCAMENTO }
    Close;
    Clear;

    Text := 'INSERT INTO LANCAMENTO (COD_LANCAMENTO, ANO_LANCAMENTO, DATA_LANCAMENTO, TIPO_LANCAMENTO, ' +
            '                        TIPO_CADASTRO, ID_CLIENTE_FORNECEDOR_FK, ID_CATEGORIA_DESPREC_FK, ' +
            '                        ID_SUBCATEGORIA_DESPREC_FK, FLG_IMPOSTORENDA, FLG_FLUTUANTE, ' +
            '                        FLG_PESSOAL, FLG_AUXILIAR, FLG_FORAFECHCAIXA, FLG_REAL, ' +
            '                        ID_NATUREZA_FK, QTD_PARCELAS, ' +
            '                        VR_TOTAL_PREV, VR_TOTAL_JUROS, VR_TOTAL_DESCONTO, ' +
            '                        VR_TOTAL_PAGO, CAD_ID_USUARIO, DATA_CADASTRO, FLG_STATUS, ' +
            '                        FLG_CANCELADO, DATA_CANCELAMENTO, CANCEL_ID_USUARIO, ' +
            '                        FLG_RECORRENTE, FLG_REPLICADO, NUM_RECIBO, ID_ORIGEM, ' +
            '                        ID_SISTEMA_FK, OBSERVACAO, ID_CARNELEAO_EQUIVALENCIA_FK) ' +
            '                VALUES (:COD_LANCAMENTO, :ANO_LANCAMENTO, :DATA_LANCAMENTO, :TIPO_LANCAMENTO, ' +
            '                        :TIPO_CADASTRO, :ID_CLIENTE_FORNECEDOR, :ID_CATEGORIA_DESPREC, ' +
            '                        :ID_SUBCATEGORIA_DESPREC, :FLG_IMPOSTORENDA, :FLG_FLUTUANTE, ' +
            '                        :FLG_PESSOAL, :FLG_AUXILIAR, :FLG_FORAFECHCAIXA, :FLG_REAL, ' +
            '                        :ID_NATUREZA, :QTD_PARCELAS, ' +
            '                        :VR_TOTAL_PREV, :VR_TOTAL_JUROS, :VR_TOTAL_DESCONTO, ' +
            '                        :VR_TOTAL_PAGO, :CAD_ID_USUARIO, :DATA_CADASTRO, :FLG_STATUS, ' +
            '                        :FLG_CANCELADO, :DATA_CANCELAMENTO, :CANCEL_ID_USUARIO, ' +
            '                        :FLG_RECORRENTE, :FLG_REPLICADO, :NUM_RECIBO, :ID_ORIGEM, ' +
            '                        :ID_SISTEMA, :OBSERVACAO, :ID_CARNELEAO_EQUIVALENCIA)';

    if ListaLancamentos[Indice].CodLancamento = 0 then
    begin
      ListaLancamentos[Indice].CodLancamento := dmGerencial.ProximoCodigoLancamento;
      ListaLancamentos[Indice].AnoLancamento := YearOf(Date);
    end;

    Params.ParamByName('COD_LANCAMENTO').AsInteger            := ListaLancamentos[Indice].CodLancamento;
    Params.ParamByName('ANO_LANCAMENTO').AsInteger            := ListaLancamentos[Indice].AnoLancamento;
    Params.ParamByName('DATA_LANCAMENTO').AsDateTime          := ListaLancamentos[Indice].DataLancamento;
    Params.ParamByName('TIPO_LANCAMENTO').AsString            := ListaLancamentos[Indice].TipoLancamento;
    Params.ParamByName('TIPO_CADASTRO').AsString              := ListaLancamentos[Indice].TipoCadastro;
    Params.ParamByName('ID_NATUREZA').AsInteger               := ListaLancamentos[Indice].IdNatureza;
    Params.ParamByName('QTD_PARCELAS').AsInteger              := ListaLancamentos[Indice].QtdParcelas;
    Params.ParamByName('CAD_ID_USUARIO').AsInteger            := ListaLancamentos[Indice].CadIdUsuario;
    Params.ParamByName('DATA_CADASTRO').AsDateTime            := ListaLancamentos[Indice].DataCadastro;
    Params.ParamByName('FLG_STATUS').AsString                 := ListaLancamentos[Indice].FlgStatus;
    Params.ParamByName('FLG_CANCELADO').AsString              := ListaLancamentos[Indice].FlgCancelado;
    Params.ParamByName('FLG_RECORRENTE').AsString             := ListaLancamentos[Indice].FlgRecorrente;
    Params.ParamByName('FLG_REPLICADO').AsString              := ListaLancamentos[Indice].FlgReplicado;
    Params.ParamByName('VR_TOTAL_PREV').AsCurrency            := ListaLancamentos[Indice].VlrTotalPrev;
    Params.ParamByName('VR_TOTAL_JUROS').AsCurrency           := ListaLancamentos[Indice].VlrTotalJuros;
    Params.ParamByName('VR_TOTAL_DESCONTO').AsCurrency        := ListaLancamentos[Indice].VlrTotalDesconto;
    Params.ParamByName('VR_TOTAL_PAGO').AsCurrency            := ListaLancamentos[Indice].VlrTotalPago;

    if ListaLancamentos[Indice].IdCliFor <> 0 then
      Params.ParamByName('ID_CLIENTE_FORNECEDOR').AsInteger   := ListaLancamentos[Indice].IdCliFor;

    if ListaLancamentos[Indice].IdCategoria <> 0 then
      Params.ParamByName('ID_CATEGORIA_DESPREC').AsInteger    := ListaLancamentos[Indice].IdCategoria;

    if ListaLancamentos[Indice].IdSubCategoria <> 0 then
      Params.ParamByName('ID_SUBCATEGORIA_DESPREC').AsInteger := ListaLancamentos[Indice].IdSubCategoria;

    if ListaLancamentos[Indice].FlgIR <> '' then
      Params.ParamByName('FLG_IMPOSTORENDA').AsString         := ListaLancamentos[Indice].FlgIR;

    if ListaLancamentos[Indice].FlgPessoal <> '' then
      Params.ParamByName('FLG_PESSOAL').AsString              := ListaLancamentos[Indice].FlgPessoal;

    if ListaLancamentos[Indice].FlgAuxiliar <> '' then
      Params.ParamByName('FLG_AUXILIAR').AsString             := ListaLancamentos[Indice].FlgAuxiliar;

    if ListaLancamentos[Indice].FlgForaFechCaixa <> '' then
      Params.ParamByName('FLG_FORAFECHCAIXA').AsString        := ListaLancamentos[Indice].FlgForaFechCaixa;

    if ListaLancamentos[Indice].FlgReal <> '' then
      Params.ParamByName('FLG_REAL').AsString                 := ListaLancamentos[Indice].FlgReal;

    if ListaLancamentos[Indice].FlgFlutuante <> '' then
      Params.ParamByName('FLG_FLUTUANTE').AsString            := ListaLancamentos[Indice].FlgFlutuante;

    if ListaLancamentos[Indice].DataCancelamento <> 0 then
      Params.ParamByName('DATA_CANCELAMENTO').AsDateTime      := ListaLancamentos[Indice].DataCancelamento;

    if ListaLancamentos[Indice].CancelIdUsuario <> 0 then
      Params.ParamByName('CANCEL_ID_USUARIO').AsInteger       := ListaLancamentos[Indice].CancelIdUsuario;

    if ListaLancamentos[Indice].NumRecibo > -1 then
      Params.ParamByName('NUM_RECIBO').AsInteger              := ListaLancamentos[Indice].NumRecibo;

    if ListaLancamentos[Indice].IdOrigem <> 0 then
      Params.ParamByName('ID_ORIGEM').AsInteger               := ListaLancamentos[Indice].IdOrigem;

    if ListaLancamentos[Indice].IdSistema <> 0 then
      Params.ParamByName('ID_SISTEMA').AsInteger              := ListaLancamentos[Indice].IdSistema;

    if ListaLancamentos[Indice].Observacao <> '' then
      Params.ParamByName('OBSERVACAO').AsString               := ListaLancamentos[Indice].Observacao;

    if ListaLancamentos[Indice].IdEquivalencia <> 0 then
      Params.ParamByName('ID_CARNELEAO_EQUIVALENCIA').AsInteger := ListaLancamentos[Indice].IdEquivalencia;

    try
      ExecSQL;
    except
      ListaLancamentos[Indice].CodLancamento := dmGerencial.ProximoCodigoLancamento;
      InsertLancamento(Indice);
    end;
  end;

  FreeAndNil(QryLanc);
end;

procedure TdmPrincipal.InsertLancamentoDet(Indice, IndiceL: Integer);
var
  QryLancD: TFDQuery;
begin
  QryLancD := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryLancD, SQL do
  begin
    { LANCAMENTO - DETALHE }
    Close;
    Clear;

    Text := 'INSERT INTO LANCAMENTO_DET (ID_LANCAMENTO_DET, COD_LANCAMENTO_FK, ANO_LANCAMENTO_FK, ' +
            '                            ID_ITEM_FK, VR_UNITARIO, QUANTIDADE, ' +
            '                            VR_TOTAL, SELO_ORIGEM, ALEATORIO_ORIGEM, ' +
            '                            TIPO_COBRANCA, COD_ADICIONAL, EMOLUMENTOS, ' +
            '                            FETJ, FUNDPERJ, FUNPERJ, FUNARPEN, PMCMV, ' +
            '                            ISS, MUTUA, ACOTERJ, ' +
            '                            NUM_PROTOCOLO, FLG_CANCELADO) ' +
            '                    VALUES (:ID_LANCAMENTO_DET, :COD_LANCAMENTO, :ANO_LANCAMENTO, ' +
            '                            :ID_ITEM, :VR_UNITARIO, :QUANTIDADE, ' +
            '                            :VR_TOTAL, :SELO_ORIGEM, :ALEATORIO_ORIGEM, ' +
            '                            :TIPO_COBRANCA, :COD_ADICIONAL, :EMOLUMENTOS, ' +
            '                            :FETJ, :FUNDPERJ, :FUNPERJ, :FUNARPEN, :PMCMV, ' +
            '                            :ISS, :MUTUA, :ACOTERJ, ' +
            '                            :NUM_PROTOCOLO, :FLG_CANCELADO)';

    if ListaLancamentoDets[Indice].IdLancamentoDet = 0 then
      ListaLancamentoDets[Indice].IdLancamentoDet  := BS.ProximoId('ID_LANCAMENTO_DET', 'LANCAMENTO_DET');

    ListaLancamentoDets[Indice].CodLancamento      := ListaLancamentos[IndiceL].CodLancamento;
    ListaLancamentoDets[Indice].AnoLancamento      := ListaLancamentos[IndiceL].AnoLancamento;

    Params.ParamByName('ID_LANCAMENTO_DET').Value  := ListaLancamentoDets[Indice].IdLancamentoDet;
    Params.ParamByName('COD_LANCAMENTO').Value     := ListaLancamentoDets[Indice].CodLancamento;
    Params.ParamByName('ANO_LANCAMENTO').Value     := ListaLancamentoDets[Indice].AnoLancamento;
    Params.ParamByName('VR_UNITARIO').Value        := ListaLancamentoDets[Indice].VlrUnitario;
    Params.ParamByName('VR_TOTAL').Value           := ListaLancamentoDets[Indice].VlrTotal;
    Params.ParamByName('EMOLUMENTOS').Value        := ListaLancamentoDets[Indice].Emolumentos;
    Params.ParamByName('FETJ').Value               := ListaLancamentoDets[Indice].FETJ;
    Params.ParamByName('FUNDPERJ').Value           := ListaLancamentoDets[Indice].FUNDPERJ;
    Params.ParamByName('FUNPERJ').Value            := ListaLancamentoDets[Indice].FUNPERJ;
    Params.ParamByName('FUNARPEN').Value           := ListaLancamentoDets[Indice].FUNARPEN;
    Params.ParamByName('PMCMV').Value              := ListaLancamentoDets[Indice].PMCMV;
    Params.ParamByName('ISS').Value                := ListaLancamentoDets[Indice].ISS;
    Params.ParamByName('MUTUA').Value              := ListaLancamentoDets[Indice].Mutua;
    Params.ParamByName('ACOTERJ').Value            := ListaLancamentoDets[Indice].Acoterj;

    if ListaLancamentoDets[Indice].IdItem <> 0 then
      Params.ParamByName('ID_ITEM').Value          := ListaLancamentoDets[Indice].IdItem;

    if ListaLancamentoDets[Indice].Quantidade <> 0 then
      Params.ParamByName('QUANTIDADE').Value       := ListaLancamentoDets[Indice].Quantidade;

    if ListaLancamentoDets[Indice].SeloOrigem <> '' then
      Params.ParamByName('SELO_ORIGEM').Value      := ListaLancamentoDets[Indice].SeloOrigem;

    if ListaLancamentoDets[Indice].AleatorioOrigem <> '' then
      Params.ParamByName('ALEATORIO_ORIGEM').Value := ListaLancamentoDets[Indice].AleatorioOrigem;

    if ListaLancamentoDets[Indice].TipoCobranca <> '' then
      Params.ParamByName('TIPO_COBRANCA').Value    := ListaLancamentoDets[Indice].TipoCobranca;

    if ListaLancamentoDets[Indice].CodAdicional <> 0 then
      Params.ParamByName('COD_ADICIONAL').Value    := ListaLancamentoDets[Indice].CodAdicional;

    if ListaLancamentoDets[Indice].NumProtocolo <> 0 then
      Params.ParamByName('NUM_PROTOCOLO').Value    := ListaLancamentoDets[Indice].NumProtocolo;

    if ListaLancamentoDets[Indice].FlgCancelado <> '' then
      Params.ParamByName('FLG_CANCELADO').Value    := ListaLancamentoDets[Indice].FlgCancelado;

    try
      ExecSQL;
    except
      ListaLancamentoDets[Indice].IdLancamentoDet := BS.ProximoId('ID_LANCAMENTO_DET', 'LANCAMENTO_DET');
      InsertLancamentoDet(Indice, IndiceL);
    end;
  end;

  FreeAndNil(QryLancD);
end;

procedure TdmPrincipal.InsertLancamentoParc(Indice, IndiceL: Integer);
var
  QryLancP: TFDQuery;
begin
  QryLancP := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryLancP, SQL do
  begin
    { LANCAMENTO - PARCELA }
    Close;
    Clear;

    Text := 'INSERT INTO LANCAMENTO_PARC (ID_LANCAMENTO_PARC, COD_LANCAMENTO_FK, ANO_LANCAMENTO_FK, ' +
            '                             NUM_PARCELA, DATA_VENCIMENTO, VR_PARCELA_PREV, ID_FORMAPAGAMENTO_ORIG_FK, ' +
            '                             VR_PARCELA_JUROS, VR_PARCELA_DESCONTO, VR_PARCELA_PAGO, ID_FORMAPAGAMENTO_FK, ' +
            '                             AGENCIA, CONTA, ID_BANCO_FK, NUM_CHEQUE, FLG_STATUS, DATA_CANCELAMENTO, ' +
            '                             CANCEL_ID_USUARIO, DATA_PAGAMENTO, NUM_COD_DEPOSITO, ' +
            '                             PAG_ID_USUARIO, OBS_LANCAMENTO_PARC, CAD_ID_USUARIO, ' +
            '                             DATA_CADASTRO, DATA_LANCAMENTO_PARC) ' +
            '                     VALUES (:ID_LANCAMENTO_PARC, :COD_LANCAMENTO, :ANO_LANCAMENTO, ' +
            '                             :NUM_PARCELA, :DATA_VENCIMENTO, :VR_PARCELA_PREV, :ID_FORMAPAGAMENTO_ORIG, ' +
            '                             :VR_PARCELA_JUROS, :VR_PARCELA_DESCONTO, :VR_PARCELA_PAGO, :ID_FORMAPAGAMENTO, ' +
            '                             :AGENCIA, :CONTA, :ID_BANCO, :NUM_CHEQUE, :FLG_STATUS, :DATA_CANCELAMENTO, ' +
            '                             :CANCEL_ID_USUARIO, :DATA_PAGAMENTO, :NUM_COD_DEPOSITO, ' +
            '                             :PAG_ID_USUARIO, :OBS_LANCAMENTO_PARC, :CAD_ID_USUARIO, ' +
            '                             :DATA_CADASTRO, :DATA_LANCAMENTO_PARC)';

    if ListaLancamentoParcs[Indice].IdLancamentoParc = 0 then
      ListaLancamentoParcs[Indice].IdLancamentoParc := BS.ProximoId('ID_LANCAMENTO_PARC', 'LANCAMENTO_PARC');

    ListaLancamentoParcs[Indice].CodLancamento := ListaLancamentos[IndiceL].CodLancamento;
    ListaLancamentoParcs[Indice].AnoLancamento := ListaLancamentos[IndiceL].AnoLancamento;

    Params.ParamByName('ID_LANCAMENTO_PARC').AsInteger     := ListaLancamentoParcs[Indice].IdLancamentoParc;
    Params.ParamByName('COD_LANCAMENTO').AsInteger         := ListaLancamentoParcs[Indice].CodLancamento;
    Params.ParamByName('ANO_LANCAMENTO').AsInteger         := ListaLancamentoParcs[Indice].AnoLancamento;
    Params.ParamByName('VR_PARCELA_PREV').AsCurrency       := ListaLancamentoParcs[Indice].VlrPrevisto;
    Params.ParamByName('VR_PARCELA_JUROS').AsCurrency      := ListaLancamentoParcs[Indice].VlrJuros;
    Params.ParamByName('VR_PARCELA_DESCONTO').AsCurrency   := ListaLancamentoParcs[Indice].VlrDesconto;
    Params.ParamByName('VR_PARCELA_PAGO').AsCurrency       := ListaLancamentoParcs[Indice].VlrPago;

    if ListaLancamentoParcs[Indice].DataLancParc <> 0 then
      Params.ParamByName('DATA_LANCAMENTO_PARC').AsDate    := ListaLancamentoParcs[Indice].DataLancParc;

    if ListaLancamentoParcs[Indice].NumParcela <> 0 then
      Params.ParamByName('NUM_PARCELA').AsInteger          := ListaLancamentoParcs[Indice].NumParcela;

    if ListaLancamentoParcs[Indice].DataVencimento <> 0 then
      Params.ParamByName('DATA_VENCIMENTO').AsDate         := ListaLancamentoParcs[Indice].DataVencimento;

    if ListaLancamentoParcs[Indice].IdFormaPagto <> 0 then
      Params.ParamByName('ID_FORMAPAGAMENTO_ORIG').AsInteger := ListaLancamentoParcs[Indice].IdFormaPagto;

    if ListaLancamentoParcs[Indice].IdFormaPagto <> 0 then
      Params.ParamByName('ID_FORMAPAGAMENTO').AsInteger    := ListaLancamentoParcs[Indice].IdFormaPagto;

    if ListaLancamentoParcs[Indice].Agencia <> '' then
      Params.ParamByName('AGENCIA').AsString               := ListaLancamentoParcs[Indice].Agencia;

    if ListaLancamentoParcs[Indice].Conta <> '' then
      Params.ParamByName('CONTA').AsString                 := ListaLancamentoParcs[Indice].Conta;

    if ListaLancamentoParcs[Indice].IdBanco <> 0 then
      Params.ParamByName('ID_BANCO').AsInteger             := ListaLancamentoParcs[Indice].IdBanco;

    if ListaLancamentoParcs[Indice].NumCheque <> '' then
      Params.ParamByName('NUM_CHEQUE').AsString            := ListaLancamentoParcs[Indice].NumCheque;

    if ListaLancamentoParcs[Indice].NumCodDeposito <> '' then
      Params.ParamByName('NUM_COD_DEPOSITO').AsString      := ListaLancamentoParcs[Indice].NumCodDeposito;

    if ListaLancamentoParcs[Indice].FlgStatus <> '' then
      Params.ParamByName('FLG_STATUS').AsString            := ListaLancamentoParcs[Indice].FlgStatus;

    if ListaLancamentoParcs[Indice].DataCancelamento <> 0 then
      Params.ParamByName('DATA_CANCELAMENTO').AsDateTime   := ListaLancamentoParcs[Indice].DataCancelamento;

    if ListaLancamentoParcs[Indice].CancelIdUsuario <> 0 then
      Params.ParamByName('CANCEL_ID_USUARIO').AsInteger    := ListaLancamentoParcs[Indice].CancelIdUsuario;

    if ListaLancamentoParcs[Indice].DataPagamento <> 0 then
      Params.ParamByName('DATA_PAGAMENTO').AsDateTime      := ListaLancamentoParcs[Indice].DataPagamento;

    if ListaLancamentoParcs[Indice].PagIdUsuario <> 0 then
      Params.ParamByName('PAG_ID_USUARIO').AsInteger       := ListaLancamentoParcs[Indice].PagIdUsuario;

    if ListaLancamentoParcs[Indice].CadIdUsuario <> 0 then
      Params.ParamByName('CAD_ID_USUARIO').AsInteger       := ListaLancamentoParcs[Indice].CadIdUsuario;

    if ListaLancamentoParcs[Indice].DataCadastro <> 0 then
      Params.ParamByName('DATA_CADASTRO').AsDate           := ListaLancamentoParcs[Indice].DataCadastro;

    if ListaLancamentoParcs[Indice].Observacao <> '' then
      Params.ParamByName('OBS_LANCAMENTO_PARC').AsString   := ListaLancamentoParcs[Indice].Observacao;

    try
      ExecSQL;
    except
      ListaLancamentoParcs[Indice].IdLancamentoParc := BS.ProximoId('ID_LANCAMENTO_PARC', 'LANCAMENTO_PARC');
      InsertLancamentoParc(Indice, IndiceL);
    end;
  end;

  FreeAndNil(QryLancP);
end;

function TdmPrincipal.ProcessoSistemaAtivo(NomeExe: String): Boolean;
var
  Name: String;
  Proc: PROCESSENTRY32;
  HSnap: HWND;
  Looper: BOOL;
begin
  Result := False;

  Proc.dwSize := SizeOf(Proc);
  HSnap := CreateToolhelp32Snapshot(TH32CS_SNAPALL, 0);
  Looper := Process32First(HSnap, Proc);

  while Integer(Looper) <> 0 do
  begin
    Name := ExtractFileName(Proc.szExeFile);

    if NomeExe = Name then
      Result := True;

    Looper := Process32Next(HSnap, Proc);
  end;

  CloseHandle(HSnap);
end;

procedure TdmPrincipal.qryDespesasVR_PARCELA_PREVGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TdmPrincipal.RecalcularFechamentoSalarios(IdFechamento, IdFuncionario: Integer;
  Vale, Comissao, OutrasDespesas: Boolean);
var
  QryA: TFDQuery;
  cTotalBase, cTotalV, cTotalC, cTotalO: Currency;
  IdItem: Integer;
  Descr, Abrev, Obs, UMed: String;
  Med: Real;
begin
  cTotalBase := 0;
  cTotalV    := 0;
  cTotalC    := 0;
  cTotalO    := 0;
  IdItem     := 0;
  Med        := 0;

  Descr := '';
  Abrev := '';
  Obs   := '';
  UMed  := '';

  QryA := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  //TOTAL DE SALARIOS
  QryA.Close;
  QryA.SQL.Clear;
  QryA.SQL.Text := 'SELECT SUM(VR_SALARIO_BASE) AS TOT_SALARIOS ' +
                   '  FROM SALARIO ' +
                   ' WHERE ID_FECHAMENTO_SALARIO_FK = :ID_FECHAMENTO_SALARIO ' +
                   '   AND ID_FUNCIONARIO_FK = :ID_FUNCIONARIO ';
  QryA.Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := IdFechamento;
  QryA.Params.ParamByName('ID_FUNCIONARIO').Value        := IdFuncionario;
  QryA.Open;

  cTotalBase := QryA.FieldByName('TOT_SALARIOS').AsCurrency;

  //VALE
  QryA.Close;
  QryA.SQL.Clear;
  QryA.SQL.Text := 'SELECT SUM(VR_VALE) AS TOT_VALE ' +
                   '  FROM VALE ' +
                   ' WHERE ID_FECHAMENTO_SALARIO_FK = :ID_FECHAMENTO_SALARIO ' +
                   '   AND ID_FUNCIONARIO_FK = :ID_FUNCIONARIO ' +
                   '   AND DATA_QUITACAO IS NOT NULL ' +
                   '   AND FLG_CANCELADO = ' + QuotedStr('N');
  QryA.Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := IdFechamento;
  QryA.Params.ParamByName('ID_FUNCIONARIO').Value        := IdFuncionario;
  QryA.Open;

  cTotalV := QryA.FieldByName('TOT_VALE').AsCurrency;

  //COMISSAO
  QryA.Close;
  QryA.SQL.Clear;
  QryA.SQL.Text := 'SELECT SUM(VR_COMISSAO) AS TOT_COMISSAO ' +
                   '  FROM COMISSAO ' +
                   ' WHERE ID_FECHAMENTO_SALARIO_FK = :ID_FECHAMENTO_SALARIO ' +
                   '   AND ID_FUNCIONARIO_FK = :ID_FUNCIONARIO ' +
                   '   AND DATA_PAGAMENTO IS NULL ' +
                   '   AND FLG_CANCELADO = ' + QuotedStr('N');
  QryA.Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := IdFechamento;
  QryA.Params.ParamByName('ID_FUNCIONARIO').Value        := IdFuncionario;
  QryA.Open;

  cTotalC := QryA.FieldByName('TOT_COMISSAO').AsCurrency;

  //OUTRA DESPESA
  QryA.Close;
  QryA.SQL.Clear;
  QryA.SQL.Text := 'SELECT SUM((CASE WHEN (FLG_MODALIDADE = ' + QuotedStr('A') + ') ' +
                   '                 THEN CAST(VR_OUTRADESPESA_FUNC AS DECIMAL(10,2)) ' +
                   '                 ELSE CAST(-VR_OUTRADESPESA_FUNC AS DECIMAL(10,2)) ' +
                   '            END)) AS TOT_ODESP ' +
                   '  FROM OUTRADESPESA_FUNC ' +
                   ' WHERE ID_FECHAMENTO_SALARIO_FK = :ID_FECHAMENTO_SALARIO ' +
                   '   AND FLG_MODALIDADE <> ' + QuotedStr('L') +
                   '   AND ID_FUNCIONARIO_FK = :ID_FUNCIONARIO ' +
                   '   AND DATA_QUITACAO IS NOT NULL ' +
                   '   AND FLG_CANCELADO = ' + QuotedStr('N');
  QryA.Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := IdFechamento;
  QryA.Params.ParamByName('ID_FUNCIONARIO').Value        := IdFuncionario;
  QryA.Open;

  cTotalO := QryA.FieldByName('TOT_ODESP').AsCurrency;

  { ATUALIZACAO DE SALARIO }
  QryA.Close;
  QryA.SQL.Clear;

  QryA.SQL.Text := 'UPDATE SALARIO SET ';

  if Vale then
  begin
    QryA.SQL.Add(' VR_TOTAL_VALE = :VR_TOTAL_VALE ');
    QryA.Params.ParamByName('VR_TOTAL_VALE').Value := cTotalV;
  end;

  if Comissao then
  begin
    if Vale then
      QryA.SQL.Add(', VR_TOTAL_COMISSAO = :VR_TOTAL_COMISSAO ')
    else
      QryA.SQL.Add(' VR_TOTAL_COMISSAO = :VR_TOTAL_COMISSAO ');

    QryA.Params.ParamByName('VR_TOTAL_COMISSAO').Value := cTotalC;
  end;

  if OutrasDespesas then
  begin
    if Vale or Comissao then
      QryA.SQL.Add(', VR_TOTAL_OUTRADESPESA = :VR_TOTAL_OUTRADESPESA ')
    else
      QryA.SQL.Add(' VR_TOTAL_OUTRADESPESA = :VR_TOTAL_OUTRADESPESA ');

    QryA.Params.ParamByName('VR_TOTAL_OUTRADESPESA').Value := cTotalO;
  end;

  QryA.SQL.Add(' WHERE ID_FECHAMENTO_SALARIO_FK = :ID_FECHAMENTO_SALARIO ' +
               '   AND ID_FUNCIONARIO_FK        = :ID_FUNCIONARIO');

  QryA.Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := IdFechamento;
  QryA.Params.ParamByName('ID_FUNCIONARIO').Value        := IdFuncionario;

  QryA.ExecSQL;

  { ATUALIZAR LANCAMENTO DE SALARIO }
  QryA.Close;
  QryA.SQL.Clear;
  QryA.SQL.Text := 'SELECT L.COD_LANCAMENTO, L.ANO_LANCAMENTO ' +
                   '  FROM LANCAMENTO L ' +
                   ' INNER JOIN SALARIO S ' +
                   '    ON L.ID_ORIGEM = S.ID_SALARIO ' +
                   '   AND S.ID_FECHAMENTO_SALARIO_FK = :ID_FECHAMENTO_SALARIO ' +
                   ' WHERE L.ID_NATUREZA_FK = 1';
  QryA.Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := IdFechamento;
  QryA.Open;

  //Inicializa os componentes de Lancamento
  dmPrincipal.InicializarComponenteLancamento;

  //Lancamento
  DadosLancamento := Lancamento.Create;

  DadosLancamento.CodLancamento    := QryA.FieldByName('COD_LANCAMENTO').AsInteger;
  DadosLancamento.AnoLancamento    := QryA.FieldByName('ANO_LANCAMENTO').AsInteger;
  DadosLancamento.VlrTotalPrev     := (cTotalBase + cTotalC + cTotalO);
  DadosLancamento.VlrTotalDesconto := cTotalV;

  ListaLancamentos.Add(DadosLancamento);

  dmPrincipal.UpdateLancamento(0);

  //Lancamento - Detalhe
  //Item
  IdItem := 0;
  Med    := 1.00;
  Descr  := 'COMISSÕES';
  Abrev  := 'COMISSÕES';
  Obs    := 'Cadastro realizado automaticamente em alteração de SALÁRIO.';
  UMed   := 'Unidade(s)';

  RetornarItem('', IdItem, Descr, Abrev,
               Obs, UMed, Med);

  //Comissoes
  DadosLancamentoDet := LancamentoDet.Create;

  DadosLancamentoDet.IdItem      := IdItem;
  DadosLancamentoDet.VlrUnitario := cTotalC;
  DadosLancamentoDet.VlrTotal    := cTotalC;

  ListaLancamentoDets.Add(DadosLancamentoDet);

  dmPrincipal.UpdateLancamentoDet(0, 0);

  //Lancamento - Parcela
  DadosLancamentoParc := LancamentoParc.Create;

  DadosLancamentoParc.VlrPrevisto := (cTotalBase + cTotalC + cTotalO);
  DadosLancamentoParc.VlrDesconto := cTotalV;

  ListaLancamentoParcs.Add(DadosLancamentoParc);

  dmPrincipal.UpdateLancamentoParc(0, 0);

  //Destroi os componentes de Lancamento
  dmPrincipal.FinalizarComponenteLancamento;

  { RECALCULO DE FECHAMENTO DE SALARIO }
  //VALE
  if Vale then
  begin
    QryA.Close;
    QryA.SQL.Clear;
    QryA.SQL.Text := 'SELECT SUM(VR_VALE) AS TOT_VALE ' +
                     '  FROM VALE ' +
                     ' WHERE ID_FECHAMENTO_SALARIO_FK = :ID_FECHAMENTO_SALARIO ' +
                     '   AND DATA_QUITACAO IS NOT NULL ' +
                     '   AND FLG_CANCELADO = ' + QuotedStr('N');
    QryA.Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := IdFechamento;
    QryA.Open;

    cTotalV := QryA.FieldByName('TOT_VALE').AsCurrency;
  end;

  //COMISSAO
  if Comissao then
  begin
    QryA.Close;
    QryA.SQL.Clear;
    QryA.SQL.Text := 'SELECT SUM(VR_COMISSAO) AS TOT_COMISSAO ' +
                     '  FROM COMISSAO ' +
                     ' WHERE ID_FECHAMENTO_SALARIO_FK = :ID_FECHAMENTO_SALARIO ' +
                     '   AND DATA_PAGAMENTO IS NULL ' +
                     '   AND FLG_CANCELADO = ' + QuotedStr('N');
    QryA.Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := IdFechamento;
    QryA.Open;

    cTotalC := QryA.FieldByName('TOT_COMISSAO').AsCurrency;
  end;

  //OUTRA DESPESA
  if OutrasDespesas then
  begin
    QryA.Close;
    QryA.SQL.Clear;
    QryA.SQL.Text := 'SELECT SUM((CASE WHEN (FLG_MODALIDADE = ' + QuotedStr('A') + ') ' +
                     '                 THEN CAST(VR_OUTRADESPESA_FUNC AS DECIMAL(10,2)) ' +
                     '                 ELSE CAST(-VR_OUTRADESPESA_FUNC AS DECIMAL(10,2)) ' +
                     '            END)) AS TOT_ODESP ' +
                     '  FROM OUTRADESPESA_FUNC ' +
                     ' WHERE ID_FECHAMENTO_SALARIO_FK = :ID_FECHAMENTO_SALARIO ' +
                     '   AND FLG_MODALIDADE <> ' + QuotedStr('L') +
                     '   AND DATA_QUITACAO IS NOT NULL ' +
                     '   AND FLG_CANCELADO = ' + QuotedStr('N');
    QryA.Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := IdFechamento;
    QryA.Open;

    cTotalO := QryA.FieldByName('TOT_ODESP').AsCurrency;
  end;

  { ATUALIZACAO DE FECHAMENTO }
  QryA.Close;
  QryA.SQL.Clear;
  QryA.SQL.Text := 'UPDATE FECHAMENTO_SALARIO ' +
                   '   SET VR_TOTAL_BASE         = :VR_TOTAL_BASE, ' +
                   '       VR_TOTAL_COMISSAO     = :VR_TOTAL_COMISSAO, ' +
                   '       VR_TOTAL_OUTRADESPESA = :VR_TOTAL_OUTRADESPESA, ' +
                   '       VR_TOTAL_VALE         = :VR_TOTAL_VALE, ' +
                   '       VR_TOTAL_APAGAR       = :VR_TOTAL_APAGAR ' +
                   ' WHERE ID_FECHAMENTO_SALARIO = :ID_FECHAMENTO_SALARIO';
  QryA.Params.ParamByName('VR_TOTAL_BASE').Value         := cTotalBase;
  QryA.Params.ParamByName('VR_TOTAL_COMISSAO').Value     := cTotalC;
  QryA.Params.ParamByName('VR_TOTAL_OUTRADESPESA').Value := cTotalO;
  QryA.Params.ParamByName('VR_TOTAL_VALE').Value         := cTotalV;
  QryA.Params.ParamByName('VR_TOTAL_APAGAR').Value       := (cTotalBase - (cTotalC + cTotalO + cTotalV));
  QryA.Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := IdFechamento;
  QryA.ExecSQL;

  FreeAndNil(QryA);
end;

procedure TdmPrincipal.RetornaCargo(var Id, Codigo: Integer; var Descricao,
  Observacao: String);
var
  Zero: Integer;
begin
  Zero := 0;
  RetornarGenerico('CARGO', Id, Zero, Codigo, Descricao, Observacao);
end;

procedure TdmPrincipal.RetornaCategoria(var Id, IdCategoria, Codigo: Integer; var Descricao,
  Observacao: String);
begin
  RetornarGenerico('CATEGORIA_DESPREC', Id, IdCategoria, Codigo, Descricao, Observacao);
end;

function TdmPrincipal.RetornaEquivalenciaCarneLeao(IdCat, IdSubCat,
  IdNat: Integer): Integer;
var
  QryEquiv: TFDQuery;
begin
  Result := 0;

  QryEquiv := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

  QryEquiv.Close;
  QryEquiv.SQL.Clear;

  QryEquiv.SQL.Text := 'SELECT ID_CARNELEAO_EQUIVALENCIA ' +
                       '  FROM CARNELEAO_EQUIVALENCIA ' +
                       ' WHERE FLG_CANCELADO = ' + QuotedStr('N') +
                       '   AND M_ID_NATUREZA_FK = ' + IntToStr(IdNat);

  if IdCat > 0 then
    QryEquiv.SQL.Add(' AND M_ID_CATEGORIA_DESPREC_FK = ' + IntToStr(IdCat));

  if IdSubCat > 0 then
    QryEquiv.SQL.Add(' AND M_ID_SUBCATEGORIA_DESPREC_FK = ' + IntToStr(IdSubCat));

  QryEquiv.Open;

  if QryEquiv.RecordCount > 0 then
    Result := QryEquiv.FieldByName('ID_CARNELEAO_EQUIVALENCIA').AsInteger;

  FreeAndNil(QryEquiv);
end;

procedure TdmPrincipal.RetornaMesAnoPagamentoSal(var Mes, Ano: Integer);
var
  QryC: TFDQuery;
  dData: TDateTime;
begin
  //Retorna o Mes e o Ano de Referencia para realizacao de pagamento de Funcionarios
  Mes  := 0;
  Ano  := 0;
  dData := 0;

  QryC := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

  QryC.Close;
  QryC.SQL.Clear;
  QryC.SQL.Text := 'SELECT DIA_PAGAMENTOFUNC FROM CONFIGURACAO';
  QryC.Open;

  dData := CalcularDataPagamentoFunc;

  if QryC.FieldByName('DIA_PAGAMENTOFUNC').AsInteger = 0 then  //Ultimo Dia Util
  begin
    Mes := MonthOf(dData);
    Ano := YearOf(dData);
  end
  else
  begin  //1º, 2º, 3º, 4º ou 5º Dia Util
    Mes := (MonthOf(dData) - 1);

    if Mes = 12 then
      Ano := (YearOf(dData) - 1)
    else
      Ano := YearOf(dData);
  end;

  FreeAndNil(QryC);
end;

procedure TdmPrincipal.RetornaNatureza(var Id, Codigo: Integer; var Descricao,
  Observacao: String);
var
  Zero: Integer;
begin
  Zero := 0;
  RetornarGenerico('NATUREZA', Id, Zero, Codigo, Descricao, Observacao);
end;

procedure TdmPrincipal.RetornarClassificacaoNatureza(IdNat: Integer; var IR, CP, LA: String);
var
  QryNat: TFDQuery;
begin
  QryNat := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  QryNat.Close;
  QryNat.SQL.Clear;
  QryNat.SQL.Text := 'SELECT FLG_IMPOSTORENDA, FLG_PESSOAL, FLG_AUXILIAR ' +
                     '  FROM NATUREZA ' +
                     ' WHERE ID_NATUREZA = :ID_NATUREZA';
  QryNat.Params.ParamByName('ID_NATUREZA').Value := IdNat;
  QryNat.Open;

  IR := QryNat.FieldByName('FLG_IMPOSTORENDA').AsString;
  CP := QryNat.FieldByName('FLG_PESSOAL').AsString;
  LA := QryNat.FieldByName('FLG_AUXILIAR').AsString;

  FreeAndNil(QryNat);
end;

procedure TdmPrincipal.RetornarClienteFornecedor(Tipo: String; var Id: Integer;
  var Documento, Observacao, NomeFantasia, RazaoSocial: String);
var
  QryCliForn, QryI: TFDQuery;
  Op: TOperacao;
  sCPF_CNPJ: String;
begin
  QryCliForn := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

  //Consulta
  QryCliForn.Close;
  QryCliForn.SQL.Clear;
  QryCliForn.SQL.Text := 'SELECT ID_CLIENTE_FORNECEDOR, ' +
                         '       CPF_CNPJ, ' +
                         '       NOME_FANTASIA, ' +
                         '       RAZAO_SOCIAL ' +
                         '  FROM CLIENTE_FORNECEDOR ';

  if Trim(Documento) <> '' then
  begin
    if Length(dmGerencial.PegarNumeroTexto(Documento)) = 11 then
    begin
      QryCliForn.SQL.Add(' WHERE DOCUMENTO = :DOCUMENTO ' +
                         '   AND REPLACE(REPLACE(CPF_CNPJ, ' +
                                                 QuotedStr('-') + ', ' +
                                                 QuotedStr('') + '), ' +
                                         QuotedStr('.') + ', ' +
                                         QuotedStr('') + ') = :CPF_CNPJ ');

      QryCliForn.Params.ParamByName('CPF_CNPJ').Value  := dmGerencial.PegarNumeroTexto(Documento);
    end
    else if Length(dmGerencial.PegarNumeroTexto(Documento)) = 14 then
    begin
      QryCliForn.SQL.Add(' WHERE DOCUMENTO = :DOCUMENTO ' +
                         '   AND REPLACE(REPLACE(REPLACE(CPF_CNPJ, ' +
                                                         QuotedStr('-') + ', ' +
                                                         QuotedStr('') + '), ' +
                                                 QuotedStr('.') + ', ' +
                                                 QuotedStr('') + '), ' +
                                         QuotedStr('/') + ', ' +
                                         QuotedStr('') + ') = :CPF_CNPJ ');

      QryCliForn.Params.ParamByName('CPF_CNPJ').Value  := dmGerencial.PegarNumeroTexto(Documento);
    end
    else
      QryCliForn.SQL.Add(' WHERE DOCUMENTO = :DOCUMENTO ');

    QryCliForn.Params.ParamByName('DOCUMENTO').Value := Documento;
  end
  else
  begin
    QryCliForn.SQL.Add(' WHERE RAZAO_SOCIAL  = :RAZAO_SOCIAL ' +
                    '   AND NOME_FANTASIA = :NOME_FANTASIA');
    QryCliForn.Params.ParamByName('RAZAO_SOCIAL').Value  := RazaoSocial;
    QryCliForn.Params.ParamByName('NOME_FANTASIA').Value := NomeFantasia;
  end;

  QryCliForn.SQL.Add(' AND TIPO_CLIENTE_FORNECEDOR = :TIPO_CLIENTE_FORNECEDOR');
  QryCliForn.Params.ParamByName('TIPO_CLIENTE_FORNECEDOR').Value := Tipo;

  QryCliForn.Open;

  if QryCliForn.RecordCount = 0 then
  begin
    try
      if vgConSISTEMAAux.Connected then
        vgConSISTEMAAux.StartTransaction;

      //Inclusao
      QryI := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

      QryI.Close;
      QryI.SQL.Clear;
      QryI.SQL.Text := 'INSERT INTO CLIENTE_FORNECEDOR (ID_CLIENTE_FORNECEDOR, NOME_FANTASIA, RAZAO_SOCIAL, ' +
                       '                                CPF_CNPJ, DOCUMENTO, DATA_CADASTRO, CAD_ID_USUARIO, ' +
                       '                                OBSERVACAO, FLG_ATIVO, FLG_BLOQUEADO, ' +
                       '                                TIPO_CLIENTE_FORNECEDOR, TIPO_PESSOA) ' +
                       '                        VALUES (:ID_CLIENTE_FORNECEDOR, :NOME_FANTASIA, :RAZAO_SOCIAL, ' +
                       '                                :CPF_CNPJ, :DOCUMENTO, :DATA_CADASTRO, :CAD_ID_USUARIO, ' +
                       '                                :OBSERVACAO, :FLG_ATIVO, :FLG_BLOQUEADO, ' +
                       '                                :TIPO_CLIENTE_FORNECEDOR, :TIPO_PESSOA)' ;

      if Id = 0 then
        Id := BS.ProximoId('ID_CLIENTE_FORNECEDOR', 'CLIENTE_FORNECEDOR');

      QryI.Params.ParamByName('ID_CLIENTE_FORNECEDOR').Value   := Id;
      QryI.Params.ParamByName('NOME_FANTASIA').Value           := NomeFantasia;
      QryI.Params.ParamByName('RAZAO_SOCIAL').Value            := RazaoSocial;
      QryI.Params.ParamByName('DOCUMENTO').Value               := Documento;
      QryI.Params.ParamByName('DATA_CADASTRO').Value           := Date;
      QryI.Params.ParamByName('CAD_ID_USUARIO').Value          := vgUsu_Id;
      QryI.Params.ParamByName('OBSERVACAO').Value              := Observacao;
      QryI.Params.ParamByName('FLG_ATIVO').Value               := 'S';
      QryI.Params.ParamByName('FLG_BLOQUEADO').Value           := 'N';
      QryI.Params.ParamByName('TIPO_CLIENTE_FORNECEDOR').Value := Tipo;

      if Length(dmGerencial.PegarNumeroTexto(Documento)) = 11 then
      begin
        sCPF_CNPJ := Copy(dmGerencial.PegarNumeroTexto(Documento), 1, 3) + '.' +
                     Copy(dmGerencial.PegarNumeroTexto(Documento), 4, 3) + '.' +
                     Copy(dmGerencial.PegarNumeroTexto(Documento), 7, 3) + '-' +
                     Copy(dmGerencial.PegarNumeroTexto(Documento), 10, 2);

        QryI.Params.ParamByName('TIPO_PESSOA').Value            := 'F';
      end
      else if Length(dmGerencial.PegarNumeroTexto(Documento)) = 14 then
      begin
        sCPF_CNPJ := Copy(dmGerencial.PegarNumeroTexto(Documento), 1, 2) + '.' +
                     Copy(dmGerencial.PegarNumeroTexto(Documento), 3, 3) + '.' +
                     Copy(dmGerencial.PegarNumeroTexto(Documento), 6, 3) + '/' +
                     Copy(dmGerencial.PegarNumeroTexto(Documento), 9, 4) + '-' +
                     Copy(dmGerencial.PegarNumeroTexto(Documento), 13, 2);

        QryI.Params.ParamByName('TIPO_PESSOA').Value            := 'J';
      end
      else
        sCPF_CNPJ := '';

      QryI.Params.ParamByName('CPF_CNPJ').Value                 := sCPF_CNPJ;

      QryI.ExecSQL;

      Op := vgOperacao;
      vgOperacao := I;

      if Trim(Tipo) = 'C' then
        BS.GravarUsuarioLog(80, '', 'Inclusão AUTOMÁTICA de Cliente',
                            Id, 'CLIENTE_FORNECEDOR')
      else if Trim(Tipo) = 'F' then
        BS.GravarUsuarioLog(80, '', 'Inclusão AUTOMÁTICA de Fornecedor',
                            Id, 'CLIENTE_FORNECEDOR');
      vgOperacao := Op;

      FreeAndNil(QryI);

      if vgConSISTEMAAux.InTransaction then
        vgConSISTEMAAux.Commit;
    except
      if vgConSISTEMAAux.InTransaction then
        vgConSISTEMAAux.Rollback;
    end;
  end
  else
  begin
    Id           := QryCliForn.FieldByName('ID_CLIENTE_FORNECEDOR').AsInteger;
    Documento    := QryCliForn.FieldByName('CPF_CNPJ').AsString;
    NomeFantasia := QryCliForn.FieldByName('NOME_FANTASIA').AsString;
    RazaoSocial  := QryCliForn.FieldByName('RAZAO_SOCIAL').AsString;
  end;

  FreeAndNil(QryCliForn);
end;

procedure TdmPrincipal.RetornarFuncionario(var Id, IdCargo: Integer; var Nome,
  CPF, Observacao: String);
var
  QryFun, QryI, QryIMon: TFDQuery;
  Op: TOperacao;
  sCPF: String;
begin
  QryFun := dmGerencial.CriarFDQuery(nil, vgConGERAux);

  //Consulta
  QryFun.Close;
  QryFun.SQL.Clear;
  QryFun.SQL.Text := 'SELECT ID_FUNCIONARIO, CPF, NOME_FUNCIONARIO ' +
                     '  FROM FUNCIONARIO ';

  if Trim(CPF) <> '' then
  begin
    QryFun.SQL.Add(' WHERE REPLACE(REPLACE(CPF, ' +
                                           QuotedStr('-') + ', ' +
                                           QuotedStr('') + '), ' +
                                   QuotedStr('.') + ', ' +
                                   QuotedStr('') + ') = :CPF ');
    QryFun.Params.ParamByName('CPF').Value  := dmGerencial.PegarNumeroTexto(CPF);
  end
  else if Trim(Nome) <> '' then
  begin
    QryFun.SQL.Add(' WHERE NOME_FUNCIONARIO = :NOME_FUNCIONARIO');
    QryFun.Params.ParamByName('NOME_FUNCIONARIO').Value := Nome;
  end;

  QryFun.Open;

  if QryFun.RecordCount = 0 then
  begin
    //Gerencial
    try
      if vgConGERAux.Connected then
        vgConGERAux.StartTransaction;

      //Inclusao
      QryI := dmGerencial.CriarFDQuery(nil, vgConGERAux);

      QryI.Close;
      QryI.SQL.Clear;
      QryI.SQL.Text := 'INSERT INTO FUNCIONARIO (ID_FUNCIONARIO, NOME_FUNCIONARIO, ' +
                       '                         CPF, ID_CARGO_FK, DATA_ADMISSAO, ' +
                       '                         DATA_CADASTRO, FLG_ATIVO) ' +
                       '                 VALUES (:ID_FUNCIONARIO, :NOME_FUNCIONARIO, ' +
                       '                         :CPF, :ID_CARGO, :DATA_ADMISSAO, ' +
                       '                         :DATA_CADASTRO, :FLG_ATIVO)' ;

      sCPF := Copy(dmGerencial.PegarNumeroTexto(CPF), 1, 3) + '.' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 4, 3) + '.' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 7, 3) + '-' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 10, 2);

      if Id = 0 then
        Id := BS.ProximoId('ID_FUNCIONARIO', 'FUNCIONARIO');

      QryI.Params.ParamByName('ID_FUNCIONARIO').Value   := Id;
      QryI.Params.ParamByName('NOME_FUNCIONARIO').Value := Nome;
      QryI.Params.ParamByName('CPF').Value              := sCPF;
      QryI.Params.ParamByName('ID_CARGO').Value         := IdCargo;
      QryI.Params.ParamByName('DATA_ADMISSAO').Value    := Null;
      QryI.Params.ParamByName('DATA_CADASTRO').Value    := Date;

      if Trim(Nome) = 'FUNCIONÁRIO' then
        QryI.Params.ParamByName('FLG_ATIVO').Value      := 'N'
      else
        QryI.Params.ParamByName('FLG_ATIVO').Value      := 'S';

      QryI.ExecSQL;

      Op := vgOperacao;
      vgOperacao := I;

      BS.GravarUsuarioLog(80, '', Observacao,
                          Id, 'FUNCIONARIO');
      vgOperacao := Op;

      FreeAndNil(QryI);

      if vgConGERAux.InTransaction then
        vgConGERAux.Commit;
    except
      if vgConGERAux.InTransaction then
        vgConGERAux.Rollback;
    end;

    //Monitoramento
    try
      if vgConSISTEMAAux.Connected then
        vgConSISTEMAAux.StartTransaction;

      //Inclusao
      QryIMon := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

      QryIMon.Close;
      QryIMon.SQL.Clear;
      QryIMon.SQL.Text := 'INSERT INTO FUNCIONARIO (ID_FUNCIONARIO, NOME_FUNCIONARIO, ' +
                       '                         CPF, ID_CARGO_FK, DATA_ADMISSAO, ' +
                       '                         DATA_CADASTRO, FLG_ATIVO) ' +
                       '                 VALUES (:ID_FUNCIONARIO, :NOME_FUNCIONARIO, ' +
                       '                         :CPF, :ID_CARGO, :DATA_ADMISSAO, ' +
                       '                         :DATA_CADASTRO, :FLG_ATIVO)' ;

      sCPF := Copy(dmGerencial.PegarNumeroTexto(CPF), 1, 3) + '.' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 4, 3) + '.' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 7, 3) + '-' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 10, 2);

      QryIMon.Params.ParamByName('ID_FUNCIONARIO').Value   := Id;
      QryIMon.Params.ParamByName('NOME_FUNCIONARIO').Value := Nome;
      QryIMon.Params.ParamByName('CPF').Value              := sCPF;
      QryIMon.Params.ParamByName('ID_CARGO').Value         := IdCargo;
      QryIMon.Params.ParamByName('DATA_ADMISSAO').Value    := Null;
      QryIMon.Params.ParamByName('DATA_CADASTRO').Value    := Date;

      if Trim(Nome) = 'FUNCIONÁRIO' then
        QryIMon.Params.ParamByName('FLG_ATIVO').Value      := 'N'
      else
        QryIMon.Params.ParamByName('FLG_ATIVO').Value      := 'S';

      QryIMon.ExecSQL;

      Op := vgOperacao;
      vgOperacao := I;

      BS.GravarUsuarioLog(80, '', Observacao,
                          Id, 'FUNCIONARIO');
      vgOperacao := Op;

      FreeAndNil(QryIMon);

      if vgConSISTEMAAux.InTransaction then
        vgConSISTEMAAux.Commit;
    except
      if vgConSISTEMAAux.InTransaction then
        vgConSISTEMAAux.Rollback;
    end;
  end
  else
  begin
    Id   := QryFun.FieldByName('ID_FUNCIONARIO').AsInteger;
    CPF  := QryFun.FieldByName('CPF').AsString;
    Nome := QryFun.FieldByName('NOME_FUNCIONARIO').AsString;
  end;

  FreeAndNil(QryFun);
end;

procedure TdmPrincipal.RetornarGenerico(Tipo: String; var Id, IdFK, Codigo: Integer;
  var Descricao, Observacao: String);
var
  QryGen, QryI, QryIMon: TFDQuery;
  Op: TOperacao;
  sCPF_CNPJ: String;
begin
  if Trim(Tipo) = 'CARGO' then
    QryGen := dmGerencial.CriarFDQuery(nil, vgConGERAux)
  else
    QryGen := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

  //Consulta
  QryGen.Close;
  QryGen.SQL.Clear;

  case AnsiIndexStr(UpperCase(Tipo), ['CARGO',
                                      'CATEGORIA_DESPREC',
                                      'SUBCATEGORIA_DESPREC',
                                      'NATUREZA']) of
    0:  { CARGO }
    begin
      QryGen.SQL.Text := 'SELECT * ' +
                       '  FROM CARGO ';

      if Codigo > 0 then
      begin
        QryGen.SQL.Add(' WHERE COD_CARGO = :COD_CARGO');
        QryGen.Params.ParamByName('COD_CARGO').Value := Codigo;
      end
      else if Trim(Descricao) <> '' then
      begin
        QryGen.SQL.Add(' WHERE DESCR_CARGO = :DESCR_CARGO');
        QryGen.Params.ParamByName('DESCR_CARGO').Value := Descricao;
      end;
    end;
    1:  { CATEGORIA }
    begin
      QryGen.SQL.Text := 'SELECT * ' +
                       '  FROM CATEGORIA_DESPREC ' +
                       ' WHERE TIPO = :TIPO ';

      if Codigo = 1 then
        QryGen.Params.ParamByName('TIPO').Value := 'D'  //Despesa
      else if Codigo = 2 then
        QryGen.Params.ParamByName('TIPO').Value := 'R';  //Receita

      QryGen.SQL.Add(' AND DESCR_CATEGORIA_DESPREC = :DESCR_CATEGORIA_DESPREC ' +
                     ' AND ID_NATUREZA_FK    = :ID_NATUREZA');
      QryGen.Params.ParamByName('DESCR_CATEGORIA_DESPREC').Value := Descricao;
      QryGen.Params.ParamByName('ID_NATUREZA').Value             := IdFK;
    end;
    2:  { SUBCATEGORIA }
    begin
      QryGen.SQL.Text := 'SELECT * ' +
                       '  FROM SUBCATEGORIA_DESPREC ' +
                       ' WHERE TIPO = :TIPO ';

      if Codigo = 1 then
        QryGen.Params.ParamByName('TIPO').Value := 'D'  //Despesa
      else if Codigo = 2 then
        QryGen.Params.ParamByName('TIPO').Value := 'R';  //Receita

      QryGen.SQL.Add(' AND DESCR_SUBCATEGORIA_DESPREC = :DESCR_SUBCATEGORIA_DESPREC ' +
                     ' AND ID_CATEGORIA_DESPREC_FK    = :ID_CATEGORIA_DESPREC');

      QryGen.Params.ParamByName('DESCR_SUBCATEGORIA_DESPREC').Value := Descricao;
      QryGen.Params.ParamByName('ID_CATEGORIA_DESPREC').Value       := IdFK;
    end;
    3:  { NATUREZA }
    begin
      QryGen.SQL.Text := 'SELECT * ' +
                       '  FROM NATUREZA ' +
                       ' WHERE TIPO_NATUREZA = :TIPO_NATUREZA ' +
                       '   AND DESCR_NATUREZA = :DESCR_NATUREZA';

      if Codigo = 1 then
        QryGen.Params.ParamByName('TIPO_NATUREZA').Value := 'D'  //Despesa
      else if Codigo = 2 then
        QryGen.Params.ParamByName('TIPO_NATUREZA').Value := 'R';  //Receita

      QryGen.Params.ParamByName('DESCR_NATUREZA').Value := Descricao;
    end;
  end;

  QryGen.Open;

  if QryGen.RecordCount = 0 then
  begin
    try
      if Trim(Tipo) = 'CARGO' then
      begin
        if vgConGERAux.Connected then
          vgConGERAux.StartTransaction;

        if vgConSISTEMAAux.Connected then
          vgConSISTEMAAux.StartTransaction;

        QryI := dmGerencial.CriarFDQuery(nil, vgConGERAux);
      end
      else
      begin
        if vgConSISTEMAAux.Connected then
          vgConSISTEMAAux.StartTransaction;

        QryI := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);
      end;

      //Inclusao
      QryI.Close;
      QryI.SQL.Clear;

      case AnsiIndexStr(UpperCase(Tipo), ['CARGO',
                                          'CATEGORIA_DESPREC',
                                          'SUBCATEGORIA_DESPREC',
                                          'NATUREZA']) of
        0:  { CARGO }
        begin
          QryI.SQL.Text := 'INSERT INTO CARGO (ID_CARGO, DESCR_CARGO, COD_CARGO) ' +
                           '           VALUES (:ID_CARGO, :DESCR_CARGO, :COD_CARGO)' ;

          if Id = 0 then
            Id := BS.ProximoId('ID_CARGO', 'CARGO');

          QryI.Params.ParamByName('ID_CARGO').Value    := Id;
          QryI.Params.ParamByName('DESCR_CARGO').Value := Descricao;
          QryI.Params.ParamByName('COD_CARGO').Value   := Codigo;
        end;
        1:  { CATEGORIA }
        begin
          QryI.SQL.Text := 'INSERT INTO CATEGORIA_DESPREC (ID_CATEGORIA_DESPREC, ' +
                           '                               DESCR_CATEGORIA_DESPREC, ' +
                           '                               ID_NATUREZA_FK, ' +
                           '                               TIPO) ' +
                           '           VALUES (:ID_CATEGORIA_DESPREC, ' +
                           '                   :DESCR_CATEGORIA_DESPREC, ' +
                           '                   :ID_NATUREZA, ' +
                           '                   :TIPO)' ;

          if Id = 0 then
            Id := BS.ProximoId('ID_CATEGORIA_DESPREC', 'CATEGORIA_DESPREC');

          QryI.Params.ParamByName('ID_CATEGORIA_DESPREC').Value    := Id;
          QryI.Params.ParamByName('DESCR_CATEGORIA_DESPREC').Value := Descricao;
          QryI.Params.ParamByName('ID_NATUREZA').Value             := IdFK;
          QryI.Params.ParamByName('TIPO').Value                    := IfThen((Codigo = 1), 'D', 'R');
        end;
        2:  { SUBCATEGORIA }
        begin
          QryI.SQL.Text := 'INSERT INTO SUBCATEGORIA_DESPREC (ID_SUBCATEGORIA_DESPREC, ' +
                           '                                  DESCR_SUBCATEGORIA_DESPREC, ' +
                           '                                  ID_CATEGORIA_DESPREC_FK, ' +
                           '                                  TIPO) ' +
                           '                          VALUES (:ID_SUBCATEGORIA_DESPREC, ' +
                           '                                  :DESCR_SUBCATEGORIA_DESPREC, ' +
                           '                                  :ID_CATEGORIA_DESPREC, ' +
                           '                                  :TIPO)' ;

          if Id = 0 then
            Id := BS.ProximoId('ID_SUBCATEGORIA_DESPREC', 'SUBCATEGORIA_DESPREC');

          QryI.Params.ParamByName('ID_SUBCATEGORIA_DESPREC').Value    := Id;
          QryI.Params.ParamByName('DESCR_SUBCATEGORIA_DESPREC').Value := Descricao;
          QryI.Params.ParamByName('ID_CATEGORIA_DESPREC').Value       := IdFK;
          QryI.Params.ParamByName('TIPO').Value                       := IfThen((Codigo = 1), 'D', 'R');
        end;
        3:  { NATUREZA }
        begin
          QryI.SQL.Text := 'INSERT INTO NATUREZA (ID_NATUREZA, DESCR_NATUREZA, ' +
                           '                      TIPO_NATUREZA, FLG_EDITAVEL) ' +
                           '              VALUES (:ID_NATUREZA, :DESCR_NATUREZA, ' +
                           '                      :TIPO_NATUREZA, :FLG_EDITAVEL)' ;

          if Id = 0 then
            Id := BS.ProximoId('ID_NATUREZA', 'NATUREZA');

          QryI.Params.ParamByName('ID_NATUREZA').Value    := Id;
          QryI.Params.ParamByName('DESCR_NATUREZA').Value := Descricao;
          QryI.Params.ParamByName('TIPO_NATUREZA').Value  := IfThen((Codigo = 1), 'D', 'R');
          QryI.Params.ParamByName('FLG_EDITAVEL').Value   := 'S';
        end;
      end;

      QryI.ExecSQL;

      if Trim(Tipo) = 'CARGO' then
      begin
        QryIMon := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

        QryIMon.Close;
        QryIMon.SQL.Clear;
        QryIMon.SQL.Text := 'INSERT INTO CARGO (ID_CARGO, DESCR_CARGO, COD_CARGO) ' +
                            '           VALUES (:ID_CARGO, :DESCR_CARGO, :COD_CARGO)';
        QryIMon.Params.ParamByName('ID_CARGO').Value    := Id;
        QryIMon.Params.ParamByName('DESCR_CARGO').Value := Descricao;
        QryIMon.Params.ParamByName('COD_CARGO').Value   := Codigo;

        QryIMon.ExecSQL;

        FreeAndNil(QryIMon);
      end;

      Op := vgOperacao;
      vgOperacao := I;

      BS.GravarUsuarioLog(80, '', Observacao, Id, Tipo);
      vgOperacao := Op;

      FreeAndNil(QryI);

      if Trim(Tipo) = 'CARGO' then
      begin
        if vgConGERAux.InTransaction then
          vgConGERAux.Commit;

        if vgConSISTEMAAux.InTransaction then
          vgConSISTEMAAux.Commit;
      end
      else
      begin
        if vgConSISTEMAAux.InTransaction then
          vgConSISTEMAAux.Commit;
      end;
    except
      if Trim(Tipo) = 'CARGO' then
      begin
        if vgConGERAux.InTransaction then
          vgConGERAux.Rollback;

        if vgConSISTEMAAux.InTransaction then
          vgConSISTEMAAux.Rollback;
      end
      else
      begin
        if vgConSISTEMAAux.InTransaction then
          vgConSISTEMAAux.Rollback;
      end;
    end;
  end
  else
  begin
    case AnsiIndexStr(UpperCase(Tipo), ['CARGO',
                                        'CATEGORIA_DESPREC',
                                        'SUBCATEGORIA_DESPREC',
                                        'NATUREZA']) of
      0:  { CARGO }
      begin
        Id        := QryGen.FieldByName('ID_CARGO').AsInteger;
        Codigo    := QryGen.FieldByName('COD_CARGO').AsInteger;
        Descricao := QryGen.FieldByName('DESCR_CARGO').AsString;
      end;
      1:  { CATEGORIA }
      begin
        Id        := QryGen.FieldByName('ID_CATEGORIA_DESPREC').AsInteger;
        Descricao := QryGen.FieldByName('DESCR_CATEGORIA_DESPREC').AsString;
      end;
      2:  { SUBCATEGORIA }
      begin
        Id        := QryGen.FieldByName('ID_SUBCATEGORIA_DESPREC').AsInteger;
        Descricao := QryGen.FieldByName('DESCR_SUBCATEGORIA_DESPREC').AsString;
      end;
      3:  { NATUREZA }
      begin
        Id        := QryGen.FieldByName('ID_NATUREZA').AsInteger;
        Descricao := QryGen.FieldByName('DESCR_NATUREZA').AsString;
      end;
    end;
  end;

  FreeAndNil(QryGen);
end;

procedure TdmPrincipal.RetornarItem(Tipo: String; var Id: Integer;
  var Descricao, Abreviacao, Observacao, UMedida: String; var Medida: Real);
var
  QryItem, QryI: TFDQuery;
  Op: TOperacao;
begin
  QryItem := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

  //Consulta
  QryItem.Close;
  QryItem.SQL.Clear;

  QryItem.SQL.Text := 'SELECT ID_ITEM, ' +
                      '       DESCR_ITEM, ' +
                      '       ABREV_ITEM' +
                      '  FROM ITEM ' +
                      ' WHERE DESCR_ITEM = :DESCR_ITEM';

  QryItem.Params.ParamByName('DESCR_ITEM').Value := Descricao;

  if Trim(Tipo) = '' then
    QryItem.SQL.Add(' AND TIPO_ITEM IS NULL')
  else
  begin
    QryItem.SQL.Add(' AND TIPO_ITEM = :TIPO_ITEM');
    QryItem.Params.ParamByName('TIPO_ITEM').Value := Tipo;
  end;

  QryItem.Open;

  if QryItem.RecordCount = 0 then
  begin
    try
      if vgConSISTEMAAux.Connected then
        vgConSISTEMAAux.StartTransaction;

      //Inclusao
      QryI := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

      QryI.Close;
      QryI.SQL.Clear;
      QryI.SQL.Text := 'INSERT INTO ITEM (ID_ITEM, TIPO_ITEM, DESCR_ITEM, ' +
                       '                  ABREV_ITEM, ULT_VALOR_CUSTO, ' +
                       '                  ESTOQUE_ATUAL, MEDIDA_PADRAO, ' +
                       '                  DESCR_UNIDADEMEDIDA, DATA_CADASTRO, ' +
                       '                  OBS_ITEM, FLG_ATIVO, DATA_INATIVACAO) ' +
                       '          VALUES (:ID_ITEM, :TIPO_ITEM, :DESCR_ITEM, ' +
                       '                  :ABREV_ITEM, :ULT_VALOR_CUSTO, ' +
                       '                  :ESTOQUE_ATUAL, :MEDIDA_PADRAO, ' +
                       '                  :DESCR_UNIDADEMEDIDA, :DATA_CADASTRO, ' +
                       '                  :OBS_ITEM, :FLG_ATIVO, :DATA_INATIVACAO)' ;

      if Id = 0 then
        Id := BS.ProximoId('ID_ITEM', 'ITEM');

      QryI.Params.ParamByName('ID_ITEM').Value             := Id;
      QryI.Params.ParamByName('TIPO_ITEM').Value           := Tipo;
      QryI.Params.ParamByName('DESCR_ITEM').Value          := Descricao;

      if Trim(Abreviacao) = '' then
        QryI.Params.ParamByName('ABREV_ITEM').Value        := Null
      else
        QryI.Params.ParamByName('ABREV_ITEM').Value        := Abreviacao;

      QryI.Params.ParamByName('ULT_VALOR_CUSTO').Value     := Null;
      QryI.Params.ParamByName('ESTOQUE_ATUAL').Value       := 0;
      QryI.Params.ParamByName('MEDIDA_PADRAO').Value       := Medida;
      QryI.Params.ParamByName('DESCR_UNIDADEMEDIDA').Value := UMedida;
      QryI.Params.ParamByName('DATA_CADASTRO').Value       := Date;
      QryI.Params.ParamByName('OBS_ITEM').Value            := Observacao;
      QryI.Params.ParamByName('FLG_ATIVO').Value           := 'S';
      QryI.Params.ParamByName('DATA_INATIVACAO').Value     := Null;

      QryI.ExecSQL;

      Op := vgOperacao;
      vgOperacao := I;
      BS.GravarUsuarioLog(80, '', 'Inclusão AUTOMÁTICA de Item',
                          Id, 'ITEM');
      vgOperacao := Op;

      FreeAndNil(QryI);

      if vgConSISTEMAAux.InTransaction then
        vgConSISTEMAAux.Commit;
    except
      if vgConSISTEMAAux.InTransaction then
        vgConSISTEMAAux.Rollback;
    end;
  end
  else
  begin
    Id         := QryItem.FieldByName('ID_ITEM').AsInteger;
    Descricao  := QryItem.FieldByName('DESCR_ITEM').AsString;
    Abreviacao := QryItem.FieldByName('ABREV_ITEM').AsString;
  end;

  FreeAndNil(QryItem);
end;

procedure TdmPrincipal.RetornarUsuario(var Id, IdFuncionario: Integer; var Nome,
  CPF, Matricula, Cargo, Senha, Observacao: String);
var
  QryUsu, QryI, QryIMon: TFDQuery;
  Op: TOperacao;
  sCPF: String;
begin
  QryUsu := dmGerencial.CriarFDQuery(nil, vgConGERAux);

  //Consulta
  QryUsu.Close;
  QryUsu.SQL.Clear;
  QryUsu.SQL.Text := 'SELECT * ' +
                     '  FROM USUARIO ';

  if Trim(CPF) <> '' then
  begin
    QryUsu.SQL.Add(' WHERE REPLACE(REPLACE(CPF, ' +
                                           QuotedStr('-') + ', ' +
                                           QuotedStr('') + '), ' +
                                   QuotedStr('.') + ', ' +
                                   QuotedStr('') + ') = :CPF ');
    QryUsu.Params.ParamByName('CPF').Value  := dmGerencial.PegarNumeroTexto(CPF);
  end
  else if Trim(Nome) <> '' then
  begin
    QryUsu.SQL.Add(' WHERE NOME = :NOME');
    QryUsu.Params.ParamByName('NOME').Value := Nome;
  end;

  QryUsu.Open;

  if QryUsu.RecordCount = 0 then
  begin
    //Gerencial
    try
      if vgConGERAux.Connected then
        vgConGERAux.StartTransaction;

      //Inclusao
      QryI := dmGerencial.CriarFDQuery(nil, vgConGERAux);

      QryI.Close;
      QryI.SQL.Clear;
      QryI.SQL.Text := 'INSERT INTO USUARIO (ID_USUARIO, NOME, CPF, SENHA, QUALIFICACAO, ' +
                       '                     MATRICULA, ID_FUNCIONARIO_FK, FLG_MASTER, ' +
                       '                     FLG_SUPORTE, FLG_VISUALIZOU_ATU, FLG_ATIVO, ' +
                       '                     DATA_CADASTRO) ' +
                       '             VALUES (:ID_USUARIO, :NOME, :CPF, :SENHA, :QUALIFICACAO, ' +
                       '                     :MATRICULA, :ID_FUNCIONARIO, :FLG_MASTER, ' +
                       '                     :FLG_SUPORTE, :FLG_VISUALIZOU_ATU, :FLG_ATIVO, ' +
                       '                     :DATA_CADASTRO)' ;

      sCPF := Copy(dmGerencial.PegarNumeroTexto(CPF), 1, 3) + '.' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 4, 3) + '.' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 7, 3) + '-' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 10, 2);

      if Id = 0 then
        Id := BS.ProximoId('ID_USUARIO', 'USUARIO');

      QryI.Params.ParamByName('ID_USUARIO').Value         := Id;
      QryI.Params.ParamByName('NOME').Value               := Nome;
      QryI.Params.ParamByName('CPF').Value                := sCPF;
      QryI.Params.ParamByName('SENHA').Value              := Senha;
      QryI.Params.ParamByName('QUALIFICACAO').Value       := Cargo;
      QryI.Params.ParamByName('MATRICULA').Value          := Matricula;
      QryI.Params.ParamByName('ID_FUNCIONARIO').Value     := IdFuncionario;
      QryI.Params.ParamByName('FLG_MASTER').Value         := 'N';
      QryI.Params.ParamByName('FLG_SUPORTE').Value        := 'N';
      QryI.Params.ParamByName('FLG_VISUALIZOU_ATU').Value := 'S';
      QryI.Params.ParamByName('FLG_ATIVO').Value          := 'S';
      QryI.Params.ParamByName('DATA_CADASTRO').Value      := Date;
      QryI.ExecSQL;

      Op := vgOperacao;
      vgOperacao := I;

      BS.GravarUsuarioLog(80, '', Observacao,
                          Id, 'USUARIO');
      vgOperacao := Op;

      FreeAndNil(QryI);

      if vgConGERAux.InTransaction then
        vgConGERAux.Commit;
    except
      if vgConGERAux.InTransaction then
        vgConGERAux.Rollback;
    end;

    //Monitoramento
    try
      if vgConSISTEMAAux.Connected then
        vgConSISTEMAAux.StartTransaction;

      //Inclusao
      QryIMon := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

      QryIMon.Close;
      QryIMon.SQL.Clear;
      QryIMon.SQL.Text := 'INSERT INTO USUARIO (ID_USUARIO, NOME, CPF, SENHA, QUALIFICACAO, ' +
                       '                     MATRICULA, ID_FUNCIONARIO_FK, FLG_MASTER, ' +
                       '                     FLG_SUPORTE, FLG_VISUALIZOU_ATU, FLG_ATIVO, ' +
                       '                     DATA_CADASTRO) ' +
                       '             VALUES (:ID_USUARIO, :NOME, :CPF, :SENHA, :QUALIFICACAO, ' +
                       '                     :MATRICULA, :ID_FUNCIONARIO, :FLG_MASTER, ' +
                       '                     :FLG_SUPORTE, :FLG_VISUALIZOU_ATU, :FLG_ATIVO, ' +
                       '                     :DATA_CADASTRO)' ;

      sCPF := Copy(dmGerencial.PegarNumeroTexto(CPF), 1, 3) + '.' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 4, 3) + '.' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 7, 3) + '-' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 10, 2);

      QryIMon.Params.ParamByName('ID_USUARIO').Value         := Id;
      QryIMon.Params.ParamByName('NOME').Value               := Nome;
      QryIMon.Params.ParamByName('CPF').Value                := sCPF;
      QryIMon.Params.ParamByName('SENHA').Value              := Senha;
      QryIMon.Params.ParamByName('QUALIFICACAO').Value       := Cargo;
      QryIMon.Params.ParamByName('MATRICULA').Value          := Matricula;
      QryIMon.Params.ParamByName('ID_FUNCIONARIO').Value     := IdFuncionario;
      QryIMon.Params.ParamByName('FLG_MASTER').Value         := 'N';
      QryIMon.Params.ParamByName('FLG_SUPORTE').Value        := 'N';
      QryIMon.Params.ParamByName('FLG_VISUALIZOU_ATU').Value := 'S';
      QryIMon.Params.ParamByName('FLG_ATIVO').Value          := 'S';
      QryIMon.Params.ParamByName('DATA_CADASTRO').Value      := Date;
      QryIMon.ExecSQL;

      Op := vgOperacao;
      vgOperacao := I;

      BS.GravarUsuarioLog(80, '', Observacao,
                          Id, 'USUARIO');
      vgOperacao := Op;

      FreeAndNil(QryIMon);

      if vgConSISTEMAAux.InTransaction then
        vgConSISTEMAAux.Commit;
    except
      if vgConSISTEMAAux.InTransaction then
        vgConSISTEMAAux.Rollback;
    end;
  end
  else
  begin
    Id            := QryUsu.FieldByName('ID_USUARIO').AsInteger;
    IdFuncionario := QryUsu.FieldByName('ID_FUNCIONARIO_FK').AsInteger;
    CPF           := QryUsu.FieldByName('CPF').AsString;
    Nome          := QryUsu.FieldByName('NOME').AsString;
    Matricula     := QryUsu.FieldByName('MATRICULA').AsString;
    Cargo         := QryUsu.FieldByName('QUALIFICACAO').AsString;
    Senha         := QryUsu.FieldByName('SENHA').AsString;
  end;

  FreeAndNil(QryUsu);
end;

procedure TdmPrincipal.RetornaSubCategoria(var Id, IdSubCategoria, Codigo: Integer;
  var Descricao, Observacao: String);
begin
  RetornarGenerico('SUBCATEGORIA_DESPREC', Id, IdSubCategoria, Codigo, Descricao, Observacao);
end;

procedure TdmPrincipal.UpdateLancamento(Indice: Integer);
var
  QryLanc: TFDQuery;
  sQry: String;
begin
  QryLanc := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  sQry   := '';

  with QryLanc, SQL do
  begin
    { LANCAMENTO }
    Close;
    Clear;

    Text := 'UPDATE LANCAMENTO SET ';

    if ListaLancamentos[Indice].DataLancamento <> 0 then
      sQry := 'DATA_LANCAMENTO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentos[Indice].DataLancamento));

    if ListaLancamentos[Indice].IdCategoria <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'ID_CATEGORIA_DESPREC_FK = ' + IntToStr(ListaLancamentos[Indice].IdCategoria)
      else
        sQry := sQry + ', ID_CATEGORIA_DESPREC_FK = ' + IntToStr(ListaLancamentos[Indice].IdCategoria);
    end;

    if ListaLancamentos[Indice].IdSubCategoria <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'ID_SUBCATEGORIA_DESPREC_FK = ' + IntToStr(ListaLancamentos[Indice].IdSubCategoria)
      else
        sQry := sQry + ', ID_SUBCATEGORIA_DESPREC_FK = ' + IntToStr(ListaLancamentos[Indice].IdSubCategoria);
    end;

    if ListaLancamentos[Indice].FlgIR <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'FLG_IMPOSTORENDA = ' + QuotedStr(ListaLancamentos[Indice].FlgIR)
      else
        sQry := sQry + ', FLG_IMPOSTORENDA = ' + QuotedStr(ListaLancamentos[Indice].FlgIR);
    end;

    if ListaLancamentos[Indice].FlgPessoal <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'FLG_PESSOAL = ' + QuotedStr(ListaLancamentos[Indice].FlgPessoal)
      else
        sQry := sQry + ', FLG_PESSOAL = ' + QuotedStr(ListaLancamentos[Indice].FlgPessoal);
    end;

    if ListaLancamentos[Indice].FlgAuxiliar <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'FLG_AUXILIAR = ' + QuotedStr(ListaLancamentos[Indice].FlgAuxiliar)
      else
        sQry := sQry + ', FLG_AUXILIAR = ' + QuotedStr(ListaLancamentos[Indice].FlgAuxiliar);
    end;

    if ListaLancamentos[Indice].FlgForaFechCaixa <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'FLG_FORAFECHCAIXA = ' + QuotedStr(ListaLancamentos[Indice].FlgForaFechCaixa)
      else
        sQry := sQry + ', FLG_FORAFECHCAIXA = ' + QuotedStr(ListaLancamentos[Indice].FlgForaFechCaixa);
    end;

    if ListaLancamentos[Indice].IdNatureza <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'ID_NATUREZA_FK = ' + IntToStr(ListaLancamentos[Indice].IdNatureza)
      else
        sQry := sQry + ', ID_NATUREZA_FK = ' + IntToStr(ListaLancamentos[Indice].IdNatureza);
    end;

    if ListaLancamentos[Indice].QtdParcelas <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'QTD_PARCELAS = ' + IntToStr(ListaLancamentos[Indice].QtdParcelas)
      else
        sQry := sQry + ', QTD_PARCELAS = ' + IntToStr(ListaLancamentos[Indice].QtdParcelas);
    end;

    if ListaLancamentos[Indice].VlrTotalPrev <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'VR_TOTAL_PREV = :VR_TOTAL_PREV '
      else
        sQry := sQry + ', VR_TOTAL_PREV = :VR_TOTAL_PREV ';
    end;

    if ListaLancamentos[Indice].VlrTotalJuros <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'VR_TOTAL_JUROS = :VR_TOTAL_JUROS '
      else
        sQry := sQry + ', VR_TOTAL_JUROS = :VR_TOTAL_JUROS ';
    end;

    if ListaLancamentos[Indice].VlrTotalDesconto <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'VR_TOTAL_DESCONTO = :VR_TOTAL_DESCONTO '
      else
        sQry := sQry + ', VR_TOTAL_DESCONTO = :VR_TOTAL_DESCONTO ';
    end;

    if ListaLancamentos[Indice].VlrTotalPago <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'VR_TOTAL_PAGO = :VR_TOTAL_PAGO '
      else
        sQry := sQry + ', VR_TOTAL_PAGO = :VR_TOTAL_PAGO ';
    end;

    if ListaLancamentos[Indice].CadIdUsuario <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'CAD_ID_USUARIO = ' + IntToStr(ListaLancamentos[Indice].CadIdUsuario)
      else
        sQry := sQry + ', CAD_ID_USUARIO = ' + IntToStr(ListaLancamentos[Indice].CadIdUsuario);
    end;

    if ListaLancamentos[Indice].DataCadastro <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'DATA_CADASTRO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD hh:mm:ss', ListaLancamentos[Indice].DataCadastro))
      else
        sQry := sQry + ', DATA_CADASTRO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD hh:mm:ss', ListaLancamentos[Indice].DataCadastro));
    end;

    if ListaLancamentos[Indice].FlgStatus <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'FLG_STATUS = ' + QuotedStr(ListaLancamentos[Indice].FlgStatus)
      else
        sQry := sQry + ', FLG_STATUS = ' + QuotedStr(ListaLancamentos[Indice].FlgStatus);
    end;

    if ListaLancamentos[Indice].FlgCancelado <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'FLG_CANCELADO = ' + QuotedStr(ListaLancamentos[Indice].FlgCancelado)
      else
        sQry := sQry + ', FLG_CANCELADO = ' + QuotedStr(ListaLancamentos[Indice].FlgCancelado);
    end;

    if ListaLancamentos[Indice].DataCancelamento <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'DATA_CANCELAMENTO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentos[Indice].DataCancelamento))
      else
        sQry := sQry + ', DATA_CANCELAMENTO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentos[Indice].DataCancelamento));
    end;

    if ListaLancamentos[Indice].CancelIdUsuario <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'CANCEL_ID_USUARIO = ' + IntToStr(ListaLancamentos[Indice].CancelIdUsuario)
      else
        sQry := sQry + ', CANCEL_ID_USUARIO = ' + IntToStr(ListaLancamentos[Indice].CancelIdUsuario);
    end;

    if ListaLancamentos[Indice].FlgRecorrente <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'FLG_RECORRENTE = ' + QuotedStr(ListaLancamentos[Indice].FlgRecorrente)
      else
        sQry := sQry + ', FLG_RECORRENTE = ' + QuotedStr(ListaLancamentos[Indice].FlgRecorrente);
    end;

    if ListaLancamentos[Indice].FlgReplicado <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'FLG_REPLICADO = ' + QuotedStr(ListaLancamentos[Indice].FlgReplicado)
      else
        sQry := sQry + ', FLG_REPLICADO = ' + QuotedStr(ListaLancamentos[Indice].FlgReplicado);
    end;

    if ListaLancamentos[Indice].NumRecibo > -1 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'NUM_RECIBO = ' + IntToStr(ListaLancamentos[Indice].NumRecibo)
      else
        sQry := sQry + ', NUM_RECIBO = ' + IntToStr(ListaLancamentos[Indice].NumRecibo);
    end;

    if ListaLancamentos[Indice].Observacao <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'OBSERVACAO = ' + QuotedStr(ListaLancamentos[Indice].Observacao)
      else
        sQry := sQry + ', OBSERVACAO = ' + QuotedStr(ListaLancamentos[Indice].Observacao);
    end;

    if ListaLancamentos[Indice].IdEquivalencia <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'ID_CARNELEAO_EQUIVALENCIA_FK = ' + IntToStr(ListaLancamentos[Indice].IdEquivalencia)
      else
        sQry := sQry + ', ID_CARNELEAO_EQUIVALENCIA_FK = ' + IntToStr(ListaLancamentos[Indice].IdEquivalencia);
    end;

    if Trim(sQry) = '' then
      Exit;

    Add(sQry +
        ' WHERE COD_LANCAMENTO = :COD_LANCAMENTO ' +
        '   AND ANO_LANCAMENTO = :ANO_LANCAMENTO');

    if ListaLancamentos[Indice].VlrTotalPrev <> 0 then
      Params.ParamByName('VR_TOTAL_PREV').Value := dmGerencial.NoRound((ListaLancamentos[Indice].VlrTotalPrev), 2);

    if ListaLancamentos[Indice].VlrTotalJuros <> 0 then
      Params.ParamByName('VR_TOTAL_JUROS').Value := dmGerencial.NoRound((ListaLancamentos[Indice].VlrTotalJuros), 2);

    if ListaLancamentos[Indice].VlrTotalDesconto <> 0 then
      Params.ParamByName('VR_TOTAL_DESCONTO').Value := dmGerencial.NoRound((ListaLancamentos[Indice].VlrTotalDesconto), 2);

    if ListaLancamentos[Indice].VlrTotalPago <> 0 then
      Params.ParamByName('VR_TOTAL_PAGO').Value := dmGerencial.NoRound((ListaLancamentos[Indice].VlrTotalPago), 2);

    Params.ParamByName('COD_LANCAMENTO').Value := ListaLancamentos[Indice].CodLancamento;
    Params.ParamByName('ANO_LANCAMENTO').Value := ListaLancamentos[Indice].AnoLancamento;

    ExecSQL;
  end;

  FreeAndNil(QryLanc);
end;

procedure TdmPrincipal.UpdateLancamentoDet(Indice, IndiceL: Integer);
var
  QryLancD: TFDQuery;
  sQry: String;
begin
  QryLancD := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  sQry   := '';

  with QryLancD, SQL do
  begin
    { LANCAMENTO - DETALHE }
    Close;
    Clear;

    Text := 'UPDATE LANCAMENTO_DET SET ';

    if ListaLancamentoDets[Indice].VlrUnitario <> 0 then
      sQry := 'VR_UNITARIO = :VR_UNITARIO ';

    if ListaLancamentoDets[Indice].Quantidade <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'QUANTIDADE = ' + CurrToStr(ListaLancamentoDets[Indice].Quantidade)
      else
        sQry := sQry + ', QUANTIDADE = ' + CurrToStr(ListaLancamentoDets[Indice].Quantidade);
    end;

    if ListaLancamentoDets[Indice].VlrTotal <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'VR_TOTAL = :VR_TOTAL '
      else
        sQry := sQry + ', VR_TOTAL = :VR_TOTAL ';
    end;

    if ListaLancamentoDets[Indice].FlgCancelado <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'FLG_CANCELADO = ' + QuotedStr(ListaLancamentoDets[Indice].FlgCancelado)
      else
        sQry := sQry + ', FLG_CANCELADO = ' + QuotedStr(ListaLancamentoDets[Indice].FlgCancelado);
    end;

    if Trim(sQry) = '' then
      Exit;

    Add(sQry +
        ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
        '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ');

    if ListaLancamentoDets[Indice].VlrUnitario <> 0 then
      Params.ParamByName('VR_UNITARIO').Value := dmGerencial.NoRound((ListaLancamentoDets[Indice].VlrUnitario), 2);

    if ListaLancamentoDets[Indice].VlrTotal <> 0 then
      Params.ParamByName('VR_TOTAL').Value := dmGerencial.NoRound((ListaLancamentoDets[Indice].VlrTotal), 2);

    Params.ParamByName('COD_LANCAMENTO').Value := ListaLancamentos[IndiceL].CodLancamento;
    Params.ParamByName('ANO_LANCAMENTO').Value := ListaLancamentos[IndiceL].AnoLancamento;

    if ListaLancamentoDets[Indice].IdItem > 0 then
    begin
      Add(' AND ID_ITEM_FK = :ID_ITEM');
      Params.ParamByName('ID_ITEM').Value      := ListaLancamentoDets[Indice].IdItem;
    end;

    if ListaLancamentoDets[Indice].IdLancamentoDet > 0 then
    begin
      Add(' AND ID_LANCAMENTO_DET = :ID_LANCAMENTO_DET');
      Params.ParamByName('ID_LANCAMENTO_DET').Value := ListaLancamentoDets[Indice].IdLancamentoDet;
    end;

    ExecSQL;
  end;

  FreeAndNil(QryLancD);
end;

procedure TdmPrincipal.UpdateLancamentoParc(Indice, IndiceL: Integer);
var
  QryLancP: TFDQuery;
  sQry: String;
begin
  QryLancP := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  sQry := '';

  with QryLancP, SQL do
  begin
    { LANCAMENTO - PARCELA }
    Close;
    Clear;

    Text := 'UPDATE LANCAMENTO_PARC SET ';

    if ListaLancamentoParcs[Indice].NumParcela <> 0 then
      sQry := 'NUM_PARCELA = ' + IntToStr(ListaLancamentoParcs[Indice].NumParcela);

    if ListaLancamentoParcs[Indice].IdFormaPagto <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'ID_CLIENTE_FORNECEDOR_FK = ' + IntToStr(ListaLancamentoParcs[Indice].IdFormaPagto)
      else
        sQry := sQry + ', ID_CLIENTE_FORNECEDOR_FK = ' + IntToStr(ListaLancamentoParcs[Indice].IdFormaPagto);
    end;

    if ListaLancamentoParcs[Indice].DataLancParc <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'DATA_LANCAMENTO_PARC = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentoParcs[Indice].DataLancParc))
      else
        sQry := sQry + ', DATA_LANCAMENTO_PARC = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentoParcs[Indice].DataLancParc));
    end;

    if ListaLancamentoParcs[Indice].DataVencimento <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'DATA_VENCIMENTO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentoParcs[Indice].DataVencimento))
      else
        sQry := sQry + ', DATA_VENCIMENTO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentoParcs[Indice].DataVencimento));
    end;

    if ListaLancamentoParcs[Indice].VlrPrevisto <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'VR_PARCELA_PREV = :VR_PARCELA_PREV '
      else
        sQry := sQry + ', VR_PARCELA_PREV = :VR_PARCELA_PREV ';
    end;

    if ListaLancamentoParcs[Indice].VlrJuros <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'VR_PARCELA_JUROS = :VR_PARCELA_JUROS '
      else
        sQry := sQry + ', VR_PARCELA_JUROS = :VR_PARCELA_JUROS ';
    end;

    if ListaLancamentoParcs[Indice].VlrDesconto <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'VR_PARCELA_DESCONTO = :VR_PARCELA_DESCONTO '
      else
        sQry := sQry + ', VR_PARCELA_DESCONTO = :VR_PARCELA_DESCONTO ';
    end;

    if ListaLancamentoParcs[Indice].VlrPago <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'VR_PARCELA_PAGO = :VR_PARCELA_PAGO '
      else
        sQry := sQry + ', VR_PARCELA_PAGO = :VR_PARCELA_PAGO ';
    end;

    if ListaLancamentoParcs[Indice].Agencia <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'AGENCIA = ' + QuotedStr(ListaLancamentoParcs[Indice].Agencia)
      else
        sQry := sQry + ', AGENCIA = ' + QuotedStr(ListaLancamentoParcs[Indice].Agencia);
    end;

    if ListaLancamentoParcs[Indice].Conta <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'CONTA = ' + QuotedStr(ListaLancamentoParcs[Indice].Conta)
      else
        sQry := sQry + ', FLG_ICONTAMPOSTORENDA = ' + QuotedStr(ListaLancamentoParcs[Indice].Conta);
    end;

    if ListaLancamentoParcs[Indice].IdBanco <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'ID_BANCO_FK = ' + IntToStr(ListaLancamentoParcs[Indice].IdBanco)
      else
        sQry := sQry + ', ID_BANCO_FK = ' + IntToStr(ListaLancamentoParcs[Indice].IdBanco);
    end;

    if ListaLancamentoParcs[Indice].NumCheque <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'NUM_CHEQUE = ' + QuotedStr(ListaLancamentoParcs[Indice].NumCheque)
      else
        sQry := sQry + ', NUM_CHEQUE = ' + QuotedStr(ListaLancamentoParcs[Indice].NumCheque);
    end;

    if ListaLancamentoParcs[Indice].NumCodDeposito <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'NUM_COD_DEPOSITO = ' + QuotedStr(ListaLancamentoParcs[Indice].NumCodDeposito)
      else
        sQry := sQry + ', NUM_COD_DEPOSITO = ' + QuotedStr(ListaLancamentoParcs[Indice].NumCodDeposito);
    end;

    if ListaLancamentoParcs[Indice].FlgStatus <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'FLG_STATUS = ' + QuotedStr(ListaLancamentoParcs[Indice].FlgStatus)
      else
        sQry := sQry + ', FLG_STATUS = ' + QuotedStr(ListaLancamentoParcs[Indice].FlgStatus);
    end;

    if ListaLancamentoParcs[Indice].DataCancelamento <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'DATA_CANCELAMENTO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentoParcs[Indice].DataCancelamento))
      else
        sQry := sQry + ', DATA_CANCELAMENTO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentoParcs[Indice].DataCancelamento));

      sQry := sQry + ', DATA_PAGAMENTO = NULL';
      sQry := sQry + ', PAG_ID_USUARIO = NULL';
    end;

    if ListaLancamentoParcs[Indice].CancelIdUsuario <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'CANCEL_ID_USUARIO = ' + IntToStr(ListaLancamentoParcs[Indice].CancelIdUsuario)
      else
        sQry := sQry + ', CANCEL_ID_USUARIO = ' + IntToStr(ListaLancamentoParcs[Indice].CancelIdUsuario);
    end;

    if ListaLancamentoParcs[Indice].DataPagamento <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'DATA_PAGAMENTO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentoParcs[Indice].DataPagamento))
      else
        sQry := sQry + ', DATA_PAGAMENTO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentoParcs[Indice].DataPagamento));
    end;

    if ListaLancamentoParcs[Indice].PagIdUsuario <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'PAG_ID_USUARIO = ' + IntToStr(ListaLancamentoParcs[Indice].PagIdUsuario)
      else
        sQry := sQry + ', PAG_ID_USUARIO = ' + IntToStr(ListaLancamentoParcs[Indice].PagIdUsuario);
    end;

    if ListaLancamentoParcs[Indice].Observacao <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'OBS_LANCAMENTO_PARC = ' + QuotedStr(ListaLancamentoParcs[Indice].Observacao)
      else
        sQry := sQry + ', OBS_LANCAMENTO_PARC = ' + QuotedStr(ListaLancamentoParcs[Indice].Observacao);
    end;

    if ListaLancamentoParcs[Indice].DataCadastro <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'DATA_CADASTRO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentoParcs[Indice].DataCadastro))
      else
        sQry := sQry + ', DATA_CADASTRO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentoParcs[Indice].DataCadastro));
    end;

    if ListaLancamentoParcs[Indice].CadIdUsuario <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'CAD_ID_USUARIO = ' + IntToStr(ListaLancamentoParcs[Indice].CadIdUsuario)
      else
        sQry := sQry + ', CAD_ID_USUARIO = ' + IntToStr(ListaLancamentoParcs[Indice].CadIdUsuario);
    end;

    if Trim(sQry) = '' then
      Exit;

    Add(sQry +
        ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
        '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ');

    if ListaLancamentoParcs[Indice].VlrPrevisto <> 0 then
      Params.ParamByName('VR_PARCELA_PREV').Value := dmGerencial.NoRound((ListaLancamentoParcs[Indice].VlrPrevisto), 2);

    if ListaLancamentoParcs[Indice].VlrJuros <> 0 then
      Params.ParamByName('VR_PARCELA_JUROS').Value := dmGerencial.NoRound((ListaLancamentoParcs[Indice].VlrJuros), 2);

    if ListaLancamentoParcs[Indice].VlrDesconto <> 0 then
      Params.ParamByName('VR_PARCELA_DESCONTO').Value := dmGerencial.NoRound((ListaLancamentoParcs[Indice].VlrDesconto), 2);

    if ListaLancamentoParcs[Indice].VlrPago <> 0 then
      Params.ParamByName('VR_PARCELA_PAGO').Value := dmGerencial.NoRound((ListaLancamentoParcs[Indice].VlrPago), 2);

    Params.ParamByName('COD_LANCAMENTO').Value := ListaLancamentos[IndiceL].CodLancamento;
    Params.ParamByName('ANO_LANCAMENTO').Value := ListaLancamentos[IndiceL].AnoLancamento;

    if ListaLancamentoParcs[Indice].IdLancamentoParc > 0 then
    begin
      Add(' AND ID_LANCAMENTO_PARC = :ID_LANCAMENTO_PARC');
      Params.ParamByName('ID_LANCAMENTO_PARC').Value := ListaLancamentoParcs[Indice].IdLancamentoParc;
    end;

    ExecSQL;
  end;

  FreeAndNil(QryLancP);
end;

procedure TdmPrincipal.VerificarSituacaoCaixa;
var
  QryCx: TFDQuery;
begin
  //Atualiza as variaveis de Configuracao do Sistema
  dmGerencial.AtualizarVariaveisConfiguracaoSistema;

  //CAIXA
  vgSituacaoCaixa := '';

  vgDataUltAb   := 0;
  vgHoraUltAb   := 0;
  vgDataProxAb  := 0;
  vgVlrNotasAb  := 0;
  vgVlrMoedasAb := 0;
  vgVlrContaAb  := 0;
  vgVlrTotalAb  := 0;
  vgIdMovAb     := 0;

  vgDataUltFech   := 0;
  vgHoraUltFech   := 0;
  vgDataProxFech  := 0;
  vgVlrNotasFech  := 0;
  vgVlrMoedasFech := 0;
  vgVlrContaFech  := 0;
  vgVlrTotalFech  := 0;
  vgIdMovFech     := 0;

  vgDataLancVigente := 0;

  QryCx := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  //Verifica a Situacao atual do Caixa
  QryCx.Close;
  QryCx.SQL.Clear;
  QryCx.SQL.Text := 'SELECT FIRST 1 CX.* ' +
                    '  FROM (SELECT A.DATA_ABERTURA AS DATA, ' +
                    '               A.HORA_ABERTURA AS HORA, ' +
                    '               ' + QuotedStr('A') + ' AS TIPO ' +
                    '          FROM CAIXA_ABERTURA A ' +
                    '         WHERE FLG_CANCELADO = ' + QuotedStr('N') +
                    '        UNION ' +
                    '        SELECT F.DATA_FECHAMENTO AS DATA, ' +
                    '               F.HORA_FECHAMENTO AS HORA, ' +
                    '               ' + QuotedStr('F') + ' AS TIPO ' +
                    '          FROM CAIXA_FECHAMENTO F ' +
                    '         WHERE FLG_CANCELADO = ' + QuotedStr('N') + ') CX ' +
                    'ORDER BY CX.DATA DESC, ' +
                    '         CX.HORA DESC';
  QryCx.Open;

  vgSituacaoCaixa := IfThen(QryCx.FieldByName('TIPO').IsNull,
                            'N',
                            QryCx.FieldByName('TIPO').AsString);

  //Abertura de Caixa
  QryCx.Close;
  QryCx.SQL.Clear;
  QryCx.SQL.Text := 'SELECT A.VR_TOTAL_ABERTURA AS VR_TOTAL, ' +
                    '       A.DATA_ABERTURA AS DATA, ' +
                    '       A.HORA_ABERTURA AS HORA, ' +
                    '       A.VR_NOTAS, ' +
                    '       A.VR_MOEDAS, ' +
                    '       A.VR_CONTACORRENTE, ' +
                    '       (SELECT M.ID_MOVIMENTO ' +
                    '          FROM MOVIMENTO M ' +
                    '         WHERE M.ID_ORIGEM = A.ID_CAIXA_ABERTURA ' +
                    '           AND M.TIPO_ORIGEM = ' + QuotedStr('A') + ') ID_ULT_MOV ' +
                    '  FROM CAIXA_ABERTURA A ' +
                    ' WHERE A.FLG_CANCELADO = ' + QuotedStr('N') +
                    '   AND A.DATA_ABERTURA = (SELECT MAX(CA.DATA_ABERTURA) ' +
                    '                            FROM CAIXA_ABERTURA CA ' +
                    '                           WHERE CA.FLG_CANCELADO = ' + QuotedStr('N') + ')';
  QryCx.Open;

  if QryCx.RecordCount > 0 then
  begin
    vgDataUltAb     := QryCx.FieldByName('DATA').AsDateTime;
    vgHoraUltAb     := StrToTime(QryCx.FieldByName('HORA').AsString);
    vgVlrNotasAb    := QryCx.FieldByName('VR_NOTAS').AsFloat;
    vgVlrMoedasAb   := QryCx.FieldByName('VR_MOEDAS').AsFloat;
    vgVlrContaAb    := QryCx.FieldByName('VR_CONTACORRENTE').AsFloat;
    vgVlrTotalAb    := QryCx.FieldByName('VR_TOTAL').AsFloat;
    vgIdMovAb       := QryCx.FieldByName('ID_ULT_MOV').AsInteger;
  end;

  //Fechamento de Caixa
  QryCx.Close;
  QryCx.SQL.Clear;
  QryCx.SQL.Text := 'SELECT F.VR_TOTAL_FECHAMENTO AS VR_TOTAL, ' +
                    '       F.DATA_FECHAMENTO AS DATA, ' +
                    '       F.HORA_FECHAMENTO AS HORA, ' +
                    '       F.VR_NOTAS, ' +
                    '       F.VR_MOEDAS, ' +
                    '       F.VR_CONTACORRENTE, ' +
                    '       (SELECT M.ID_MOVIMENTO ' +
                    '          FROM MOVIMENTO M ' +
                    '         WHERE M.ID_ORIGEM = F.ID_CAIXA_FECHAMENTO ' +
                    '           AND M.TIPO_ORIGEM = ' + QuotedStr('F') + ') ID_ULT_MOV ' +
                    '  FROM CAIXA_FECHAMENTO F ' +
                    ' WHERE F.FLG_CANCELADO = ' + QuotedStr('N') +
                    '   AND F.DATA_FECHAMENTO = (SELECT MAX(CF.DATA_FECHAMENTO) ' +
                    '                              FROM CAIXA_FECHAMENTO CF ' +
                    '                             WHERE CF.FLG_CANCELADO = ' + QuotedStr('N') + ')';
  QryCx.Open;

  if QryCx.RecordCount > 0 then
  begin
    vgDataUltFech   := QryCx.FieldByName('DATA').AsDateTime;
    vgHoraUltFech   := StrToTime(QryCx.FieldByName('HORA').AsString);
    vgVlrNotasFech  := QryCx.FieldByName('VR_NOTAS').AsFloat;
    vgVlrMoedasFech := QryCx.FieldByName('VR_MOEDAS').AsFloat;
    vgVlrContaFech  := QryCx.FieldByName('VR_CONTACORRENTE').AsFloat;
    vgVlrTotalFech  := QryCx.FieldByName('VR_TOTAL').AsFloat;
    vgIdMovAb       := QryCx.FieldByName('ID_ULT_MOV').AsInteger;
  end;

  case AnsiIndexStr(UpperCase(vgSituacaoCaixa), ['N', 'A', 'F']) of
    0:
    begin
      if vgConf_DataIniImportacao = 0 then
      begin
        vgDataProxAb      := Date;  //Data prevista
        vgDataProxFech    := Date;  //Data prevista
        vgDataLancVigente := Date;  //Data prevista
      end
      else
      begin
        vgDataProxAb      := vgConf_DataIniImportacao;  //Data prevista
        vgDataProxFech    := vgConf_DataIniImportacao;  //Data prevista
        vgDataLancVigente := vgConf_DataIniImportacao;  //Data prevista
      end;
    end;
    1:
    begin
      vgDataProxAb      := dmGerencial.ProximoDiaUtil(vgDataUltAb);  //Data prevista
      vgDataProxFech    := vgDataUltAb;
      vgDataLancVigente := vgDataUltAb;  //Data prevista
    end;
    2:
    begin
      vgDataProxAb      := dmGerencial.ProximoDiaUtil(vgDataUltAb);  //Data prevista
      vgDataProxFech    := dmGerencial.ProximoDiaUtil(vgDataUltFech);  //Data prevista
      vgDataLancVigente := vgDataProxFech;  //Data prevista
    end;
  end;

  FreeAndNil(QryCx);
end;

{ TDadosClasseLancamento }

constructor TDadosClasseLancamento.Create;
begin
  CodLancamento     := 0;
  AnoLancamento     := 0;
  DataLancamento    := 0;
  TipoLancamento    := '';
  TipoCadastro      := '';
  IdCategoria       := 0;
  DescrCategoria    := '';
  IdSubCategoria    := 0;
  DescrSubCategoria := '';
  IdNatureza        := 0;
  DescrNatureza     := '';
  IdCliFor          := 0;
  NomeCliFor        := '';
  FlgIR             := '';
  FlgPessoal        := '';
  FlgAuxiliar       := '';
  FlgForaFechCaixa  := '';
  FlgReal           := 'S';
  FlgFlutuante      := '';
  QtdParcelas       := 0;
  VlrTotalPrev      := 0;
  VlrTotalJuros     := 0;
  VlrTotalDesconto  := 0;
  VlrTotalPago      := 0;
  CadIdUsuario      := 0;
  DataCadastro      := 0;
  FlgStatus         := 'P';
  FlgCancelado      := 'N';
  DataCancelamento  := 0;
  CancelIdUsuario   := 0;
  FlgRecorrente     := '';
  FlgReplicado      := '';
  NumRecibo         := -1;
  IdOrigem          := 0;
  IdSistema         := 0;
  NomeSistema       := '';
  Observacao        := '';
  IdEquivalencia    := 0;
end;

destructor TDadosClasseLancamento.Destroy;
begin

  inherited;
end;

{ TDadosClasseLancamentoDet }

constructor TDadosClasseLancamentoDet.Create;
begin
  IdLancamentoDet    := 0;
  CodLancamento      := 0;
  AnoLancamento      := 0;
  IdItem             := 0;
  DescrItem          := '';
  VlrUnitario        := 0;
  Quantidade         := 0;
  VlrTotal           := 0;
  SeloOrigem         := '';
  AleatorioOrigem    := '';
  TipoCobranca       := '';
  CodAdicional       := 0;
  Emolumentos        := 0;
  FETJ               := 0;
  FUNDPERJ           := 0;
  FUNPERJ            := 0;
  FUNARPEN           := 0;
  PMCMV              := 0;
  ISS                := 0;
  Mutua              := 0;
  Acoterj            := 0;
  NumProtocolo       := 0;
  FlgCancelado       := 'N';
end;

destructor TDadosClasseLancamentoDet.Destroy;
begin

  inherited;
end;

{ TDadosClasseLancamentoParc }

constructor TDadosClasseLancamentoParc.Create;
begin
  IdLancamentoParc := 0;
  CodLancamento    := 0;
  AnoLancamento    := 0;
  DataLancParc     := 0;
  NumParcela       := 0;
  DataVencimento   := 0;
  VlrPrevisto      := 0;
  VlrJuros         := 0;
  VlrDesconto      := 0;
  VlrPago          := 0;
  IdFormaPagtoOrig := 0;
  IdFormaPagto     := 0;
  Agencia          := '';
  Conta            := '';
  IdBanco          := 0;
  NumCheque        := '';
  NumCodDeposito   := '';
  FlgStatus        := 'P';
  CadIdUsuario     := 0;
  DataCadastro     := 0;
  CancelIdUsuario  := 0;
  DataCancelamento := 0;
  PagIdUsuario     := 0;
  DataPagamento    := 0;
  Observacao       := '';
end;

destructor TDadosClasseLancamentoParc.Destroy;
begin

  inherited;
end;

end.
