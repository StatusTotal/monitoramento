{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroFechamentoSalario.pas
  Descricao:   Tela de Cadastro de Fechamento de Salario dos Funcionarios
  Author   :   Cristina
  Date:        03-jun-2016
  Last Update: 23-out-2018 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroFechamentoSalario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Grids, Vcl.DBGrids, Vcl.DBCtrls, Vcl.Mask, JvBaseEdits, JvDBControls,
  JvExMask, JvToolEdit, JvExStdCtrls, JvCombobox, JvDBCombobox,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.DBClient, Datasnap.Provider,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.DateUtils, JvExDBGrids,
  JvDBGrid, System.Generics.Collections, JvMaskEdit;

type
  { FUNCIONARIO }
  TDadosClasseFuncionario = class(TObject)
  public
    IdFuncionario   : Integer;
    NomeFuncionario : String;
    VrSalario       : Currency;

    constructor Create(); virtual;
    destructor  Destroy; override;
  end;

  TClasseFuncionario = class(TDadosClasseFuncionario)
  public
    IdFuncionario   : Integer;
    NomeFuncionario : String;
    VrSalario       : Currency;
  end;

  TFuncionario = class of TDadosClasseFuncionario;

  TFCadastroFechamentoSalario = class(TFCadastroGeralPadrao)
    lblMesReferencia: TLabel;
    lblAnoReferencia: TLabel;
    lblDataPrevisaoPagto: TLabel;
    lblMesPagamento: TLabel;
    lblNomeFuncionario: TLabel;
    edtAnoReferencia: TDBEdit;
    edtAnoPagamento: TDBEdit;
    lblAnoPagamento: TLabel;
    lblDataRealizacaoPagto: TLabel;
    lblVlrSalarioBase: TLabel;
    lblVlrTotalComissao: TLabel;
    lblVlrTotalVale: TLabel;
    lblVlrSalarioAPagar: TLabel;
    dteDataPrevisaoPagto: TJvDBDateEdit;
    dteDataRealizacaoPagto: TJvDBDateEdit;
    edtNomeFuncionario: TDBEdit;
    lblComissoes: TLabel;
    lblVales: TLabel;
    dbgComissoes: TDBGrid;
    dbgVales: TDBGrid;
    cbMesReferencia: TJvDBComboBox;
    cbMesPagamento: TJvDBComboBox;
    dbgFuncionarios: TJvDBGrid;
    Shape1: TShape;
    btnIncluirFunc: TJvTransparentButton;
    btnExcluirFunc: TJvTransparentButton;
    lblOutrasDespesas: TLabel;
    dbgOutrasDespesas: TDBGrid;
    lblVlrTotalOutrasDesp: TLabel;
    dsComissoes: TDataSource;
    dsOutrasDesp: TDataSource;
    dsVales: TDataSource;
    cedVlrSalarioBase: TJvDBCalcEdit;
    cedVlrTotalComissao: TJvDBCalcEdit;
    cedVlrTotalOutrasDesp: TJvDBCalcEdit;
    cedVlrTotalVale: TJvDBCalcEdit;
    cedVlrSalarioAPagar: TJvDBCalcEdit;
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure dbgComissoesDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgValesDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure dbgFuncionariosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnIncluirFuncClick(Sender: TObject);
    procedure btnExcluirFuncClick(Sender: TObject);
    procedure dbgOutrasDespesasDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure dteDataPrevisaoPagtoExit(Sender: TObject);
    procedure dteDataPrevisaoPagtoKeyPress(Sender: TObject; var Key: Char);
    procedure dteDataRealizacaoPagtoExit(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }

    iIdSalario,
    iIdFuncionario: Integer;

    dDataVenc: TDateTime;

    procedure IncluirLancamento;
    procedure AtualizarLancamento;
  public
    { Public declarations }

    procedure InicializarComponenteFuncionario;
    procedure FinalizarComponenteFuncionario;
  end;

var
  FCadastroFechamentoSalario: TFCadastroFechamentoSalario;

  { FUNCIONARIO }
  Funcionario: TFuncionario;
  DadosFuncionario: TDadosClasseFuncionario;
  ListaFuncionarios: TList<TDadosClasseFuncionario>;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMFechamentoSalario,
  UListaFuncionarios;

{ TFCadastroPagamentoFuncionario }

procedure TFCadastroFechamentoSalario.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroFechamentoSalario.btnExcluirFuncClick(Sender: TObject);
begin
  inherited;

  if dmFechamentoSalario.cdsSalario.RecordCount > 0 then
  begin
    if dmFechamentoSalario.cdsSalario.FieldByName('PRE_CADASTRADO').AsBoolean then
      dmFechamentoSalario.cdsSalario.FieldByName('EXCLUIDO').AsBoolean
    else
      dmFechamentoSalario.cdsSalario.Delete;
  end;
end;

procedure TFCadastroFechamentoSalario.btnIncluirFuncClick(Sender: TObject);
var
  j: Integer;
begin
  inherited;

  FCadastroFechamentoSalario.FinalizarComponenteFuncionario;
  FCadastroFechamentoSalario.InicializarComponenteFuncionario;

  if dmFechamentoSalario.cdsSalario.RecordCount > 0 then
  begin
    dmFechamentoSalario.cdsSalario.First;

    while not dmFechamentoSalario.cdsSalario.Eof do
    begin
      DadosFuncionario := Funcionario.Create;
      DadosFuncionario.IdFuncionario   := dmFechamentoSalario.cdsSalario.FieldByName('ID_FUNCIONARIO_FK').AsInteger;
      DadosFuncionario.NomeFuncionario := dmFechamentoSalario.cdsSalario.FieldByName('FUNCIONARIO').AsString;
      DadosFuncionario.VrSalario       := dmFechamentoSalario.cdsSalario.FieldByName('VR_SALARIO_BASE').AsCurrency;
      ListaFuncionarios.Add(DadosFuncionario);

      dmFechamentoSalario.cdsSalario.Next;
    end;
  end;

  dmGerencial.CriarForm(TFListaFuncionarios, FListaFuncionarios);

  for j := 0 to Pred(ListaFuncionarios.Count) do
  begin
    Inc(iIdSalario);

    dmFechamentoSalario.cdsSalario.Append;
    dmFechamentoSalario.cdsSalario.FieldByName('NOVO').AsBoolean               := True;
    dmFechamentoSalario.cdsSalario.FieldByName('ID_SALARIO').AsInteger         := (iIdSalario + 900000);
    dmFechamentoSalario.cdsSalario.FieldByName('ID_FUNCIONARIO_FK').AsInteger  := ListaFuncionarios[j].IdFuncionario;
    dmFechamentoSalario.cdsSalario.FieldByName('FUNCIONARIO').AsString         := ListaFuncionarios[j].NomeFuncionario;
    dmFechamentoSalario.cdsSalario.FieldByName('VR_SALARIO_BASE').AsCurrency   := ListaFuncionarios[j].VrSalario;
    dmFechamentoSalario.cdsSalario.Post;

    dmFechamentoSalario.CalcularValoresTotais(ListaFuncionarios[j].IdFuncionario);

    dmFechamentoSalario.cdsSalario.Edit;
    dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_COMISSAO').AsCurrency     := cTotalComissoes;
    dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_OUTRADESPESA').AsCurrency := ((cTotalOutrasDespesasA * -1) - (cTotalOutrasDespesasD * -1));
    dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_OD').AsCurrency           := Abs((cTotalOutrasDespesasA * -1) - (cTotalOutrasDespesasD * -1));
    dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_OD_A').AsCurrency         := cTotalOutrasDespesasA;
    dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_OD_D').AsCurrency         := cTotalOutrasDespesasD;
    dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_VALE').AsCurrency         := cTotalVales;
    dmFechamentoSalario.cdsSalario.FieldByName('VR_SALARIO_APAGAR').AsCurrency     := (ListaFuncionarios[j].VrSalario -
                                                                                       cTotalComissoes -
                                                                                       ((cTotalOutrasDespesasA * -1) -
                                                                                        (cTotalOutrasDespesasD * -1)) -
                                                                                       cTotalVales);
    dmFechamentoSalario.cdsSalario.Post;
  end;
end;

procedure TFCadastroFechamentoSalario.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroFechamentoSalario.AtualizarLancamento;
var
  Indice, IdItem: Integer;
  QryAux: TFDQuery;
  Descr, Abrev, Obs, UMed: String;
  Med: Real;
  iMes, iAno: Integer;
begin
  Indice := 0;
  IdItem := 0;
  Med    := 0;

  Descr := '';
  Abrev := '';
  Obs   := '';
  UMed  := '';

  iMes := 0;
  iAno := 0;

  QryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  QryAux.Close;
  QryAux.SQL.Clear;
  QryAux.SQL.Text := 'SELECT COD_LANCAMENTO, ANO_LANCAMENTO, FLG_STATUS ' +
                     '  FROM LANCAMENTO ' +
                     ' WHERE ID_ORIGEM      = :ID_ORIGEM ' +
                     '   AND ID_NATUREZA_FK = 1';
  QryAux.Params.ParamByName('ID_ORIGEM').Value := dmFechamentoSalario.cdsSalario.FieldByName('ID_SALARIO').AsInteger;
  QryAux.Open;

  { LANCAMENTO }
  DadosLancamento := Lancamento.Create;

  DadosLancamento.CodLancamento    := QryAux.FieldByName('COD_LANCAMENTO').AsInteger;
  DadosLancamento.AnoLancamento    := QryAux.FieldByName('ANO_LANCAMENTO').AsInteger;
  DadosLancamento.DataLancamento   := dmFechamentoSalario.cdsFechamento.FieldByName('DATA_PREV_PAGAMENTO').AsDateTime;
  DadosLancamento.FlgStatus        := QryAux.FieldByName('FLG_STATUS').AsString;
  DadosLancamento.VlrTotalPrev     := (dmFechamentoSalario.cdsSalario.FieldByName('VR_SALARIO_BASE').AsCurrency +
                                       dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_COMISSAO').AsCurrency -
                                       dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_OUTRADESPESA').AsCurrency);
  DadosLancamento.VlrTotalDesconto := dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_VALE').AsCurrency;

  if dmFechamentoSalario.cdsSalario.FieldByName('EXCLUIDO').AsBoolean then
  begin
    DadosLancamento.FlgCancelado     := 'S';
    DadosLancamento.DataCancelamento := Now;
    DadosLancamento.CancelIdUsuario  := vgUsu_Id;
  end;

  ListaLancamentos.Add(DadosLancamento);

  dmPrincipal.UpdateLancamento(0);

  { LANCAMENTO - DETALHE }
  //Item
  IdItem := 0;
  Med    := 1.00;
  Descr  := 'SAL�RIO';
  Abrev  := 'SAL�RIO';
  Obs    := 'Cadastro realizado automaticamente em altera��o de SAL�RIO.';
  UMed   := 'Unidade(s)';

  dmPrincipal.RetornarItem('', IdItem, Descr, Abrev,
                           Obs, UMed, Med);

  //Salario
  DadosLancamentoDet := LancamentoDet.Create;

  DadosLancamentoDet.CodLancamento := QryAux.FieldByName('COD_LANCAMENTO').AsInteger;
  DadosLancamentoDet.AnoLancamento := QryAux.FieldByName('ANO_LANCAMENTO').AsInteger;
  DadosLancamentoDet.IdItem        := IdItem;
  DadosLancamentoDet.VlrUnitario   := dmFechamentoSalario.cdsSalario.FieldByName('VR_SALARIO_BASE').AsCurrency;
  DadosLancamentoDet.VlrTotal      := dmFechamentoSalario.cdsSalario.FieldByName('VR_SALARIO_BASE').AsCurrency;

  if dmFechamentoSalario.cdsSalario.FieldByName('EXCLUIDO').AsBoolean then
    DadosLancamentoDet.FlgCancelado := 'S';

  ListaLancamentoDets.Add(DadosLancamentoDet);

  dmPrincipal.UpdateLancamentoDet(Indice, 0);

  //Item
  IdItem := 0;
  Med    := 1.00;
  Descr  := 'COMISS�ES';
  Abrev  := 'COMISS�ES';
  Obs    := 'Cadastro realizado automaticamente em altera��o de SAL�RIO.';
  UMed   := 'Unidade(s)';

  dmPrincipal.RetornarItem('', IdItem, Descr, Abrev,
                           Obs, UMed, Med);

  //Comissoes
  Inc(Indice);

  DadosLancamentoDet := LancamentoDet.Create;

  DadosLancamentoDet.CodLancamento := QryAux.FieldByName('COD_LANCAMENTO').AsInteger;
  DadosLancamentoDet.AnoLancamento := QryAux.FieldByName('ANO_LANCAMENTO').AsInteger;
  DadosLancamentoDet.IdItem        := IdItem;
  DadosLancamentoDet.VlrUnitario   := dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_COMISSAO').AsCurrency;
  DadosLancamentoDet.VlrTotal      := dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_COMISSAO').AsCurrency;

  if dmFechamentoSalario.cdsSalario.FieldByName('EXCLUIDO').AsBoolean then
    DadosLancamentoDet.FlgCancelado := 'S';

  ListaLancamentoDets.Add(DadosLancamentoDet);

  dmPrincipal.UpdateLancamentoDet(Indice, 0);

  //Item
  IdItem := 0;
  Med    := 1.00;
  Descr  := 'OUTROS LAN�AMENTOS (Acr�scimos)';
  Abrev  := 'OUTROS LAN�AMENTOS (Acr�scimos)';
  Obs    := 'Cadastro realizado automaticamente em altera��o de SAL�RIO.';
  UMed   := 'Unidade(s)';

  dmPrincipal.RetornarItem('', IdItem, Descr, Abrev,
                           Obs, UMed, Med);

  //Outras Despesas - Acrescimos
  Inc(Indice);

  DadosLancamentoDet := LancamentoDet.Create;

  DadosLancamentoDet.CodLancamento := QryAux.FieldByName('COD_LANCAMENTO').AsInteger;
  DadosLancamentoDet.AnoLancamento := QryAux.FieldByName('ANO_LANCAMENTO').AsInteger;
  DadosLancamentoDet.IdItem        := IdItem;
  DadosLancamentoDet.VlrUnitario   := dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_OD_A').AsCurrency;
  DadosLancamentoDet.VlrTotal      := dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_OD_A').AsCurrency;

  if dmFechamentoSalario.cdsSalario.FieldByName('EXCLUIDO').AsBoolean then
    DadosLancamentoDet.FlgCancelado := 'S';

  ListaLancamentoDets.Add(DadosLancamentoDet);

  dmPrincipal.UpdateLancamentoDet(Indice, 0);

  //Item
  IdItem := 0;
  Med    := 1.00;
  Descr  := 'OUTROS LAN�AMENTOS (Decr�scimos)';
  Abrev  := 'OUTROS LAN�AMENTOS (Decr�scimos)';
  Obs    := 'Cadastro realizado automaticamente em altera��o de SAL�RIO.';
  UMed   := 'Unidade(s)';

  dmPrincipal.RetornarItem('', IdItem, Descr, Abrev,
                           Obs, UMed, Med);

  //Outras Despesas - Decrescimos
  Inc(Indice);

  DadosLancamentoDet := LancamentoDet.Create;

  DadosLancamentoDet.CodLancamento := QryAux.FieldByName('COD_LANCAMENTO').AsInteger;
  DadosLancamentoDet.AnoLancamento := QryAux.FieldByName('ANO_LANCAMENTO').AsInteger;
  DadosLancamentoDet.IdItem        := IdItem;
  DadosLancamentoDet.VlrUnitario   := dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_OD_D').AsCurrency;
  DadosLancamentoDet.VlrTotal      := dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_OD_D').AsCurrency;

  if dmFechamentoSalario.cdsSalario.FieldByName('EXCLUIDO').AsBoolean then
    DadosLancamentoDet.FlgCancelado := 'S';

  ListaLancamentoDets.Add(DadosLancamentoDet);

  dmPrincipal.UpdateLancamentoDet(Indice, 0);

  //Item
  IdItem := 0;
  Med    := 1.00;
  Descr  := 'VALES';
  Abrev  := 'VALES';
  Obs    := 'Cadastro realizado automaticamente em altera��o de SAL�RIO.';
  UMed   := 'Unidade(s)';

  dmPrincipal.RetornarItem('', IdItem, Descr, Abrev,
                           Obs, UMed, Med);

  //Vales
  Inc(Indice);

  DadosLancamentoDet := LancamentoDet.Create;

  DadosLancamentoDet.CodLancamento := QryAux.FieldByName('COD_LANCAMENTO').AsInteger;
  DadosLancamentoDet.AnoLancamento := QryAux.FieldByName('ANO_LANCAMENTO').AsInteger;
  DadosLancamentoDet.IdItem        := IdItem;
  DadosLancamentoDet.VlrUnitario   := dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_VALE').AsCurrency;
  DadosLancamentoDet.VlrTotal      := dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_VALE').AsCurrency;

  if dmFechamentoSalario.cdsSalario.FieldByName('EXCLUIDO').AsBoolean then
    DadosLancamentoDet.FlgCancelado := 'S';

  ListaLancamentoDets.Add(DadosLancamentoDet);

  dmPrincipal.UpdateLancamentoDet(Indice, 0);

  { LANCAMENTO - PARCELA }
  DadosLancamentoParc := LancamentoParc.Create;

  DadosLancamentoParc.DataLancParc   := dmFechamentoSalario.cdsFechamento.FieldByName('DATA_PREV_PAGAMENTO').AsDateTime;
  DadosLancamentoParc.DataVencimento := dmFechamentoSalario.cdsFechamento.FieldByName('DATA_PREV_PAGAMENTO').AsDateTime;
  DadosLancamentoParc.VlrPrevisto    := (dmFechamentoSalario.cdsSalario.FieldByName('VR_SALARIO_BASE').AsCurrency +
                                          dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_COMISSAO').AsCurrency -
                                          dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_OUTRADESPESA').AsCurrency);
  DadosLancamentoParc.VlrDesconto    := dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_VALE').AsCurrency;

  if dmFechamentoSalario.cdsSalario.FieldByName('EXCLUIDO').AsBoolean then
  begin
    DadosLancamentoParc.FlgStatus        := 'C';
    DadosLancamentoParc.DataCancelamento := Now;
    DadosLancamentoParc.CancelIdUsuario  := vgUsu_Id;
  end
  else
  begin
    DadosLancamentoParc.FlgStatus := QryAux.FieldByName('FLG_STATUS').AsString;

    if not dmFechamentoSalario.cdsFechamento.FieldByName('DATA_REAL_PAGAMENTO').IsNull then
      DadosLancamentoParc.DataPagamento := dmFechamentoSalario.cdsFechamento.FieldByName('DATA_REAL_PAGAMENTO').AsDateTime;
  end;

  ListaLancamentoParcs.Add(DadosLancamentoParc);

  dmPrincipal.UpdateLancamentoParc(0, 0);

  //Verifica Mes e Ano de Referencia
  if iMesRef < Integer(MonthOf(Date)) then
  begin
    if iMesRef = 12 then
    begin
      iMes := 1;
      iAno := (iAnoRef + 1);
    end
    else
    begin
      iMes := (iMesRef + 1);
      iAno := iAnoRef;
    end;
  end
  else
  begin
    iMes := iMesRef;
    iAno := iAnoRef;
  end;

  //Retirar o Id do Fechamento nas tabelas agregadas
  if dmFechamentoSalario.cdsSalario.FieldByName('EXCLUIDO').AsBoolean then
    dmPrincipal.ApontarFechamentoAgregadas(iMes,
                                           iAno,
                                           0,
                                           dmFechamentoSalario.cdsSalario.FieldByName('ID_FUNCIONARIO_FK').AsInteger);

  FreeAndNil(QryAux);
end;

procedure TFCadastroFechamentoSalario.dbgComissoesDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;

  AlternarCorGrid(Sender, Rect, DataCol, Column, State);
end;

procedure TFCadastroFechamentoSalario.dbgValesDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;

  AlternarCorGrid(Sender, Rect, DataCol, Column, State);
end;

procedure TFCadastroFechamentoSalario.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtAnoReferencia.MaxLength := 4;
  edtAnoPagamento.MaxLength  := 4;
end;

procedure TFCadastroFechamentoSalario.DesabilitarComponentes;
begin
  inherited;

  cbMesReferencia.Enabled  := (vgOperacao <> C);
  edtAnoReferencia.Enabled := (vgOperacao <> C);

  btnIncluirFunc.Enabled := (vgOperacao <> C);
  btnExcluirFunc.Enabled := (vgOperacao <> C);

  dteDataPrevisaoPagto.Enabled := (vgOperacao <> C);
  cbMesPagamento.Enabled       := (vgOperacao <> C);
  edtAnoPagamento.Enabled      := (vgOperacao <> C);

  edtNomeFuncionario.Enabled := False;

  dteDataRealizacaoPagto.Enabled := (vgOperacao = E) and
                                    ((not dmFechamentoSalario.cdsFechamento.FieldByName('DATA_REAL_PAGAMENTO').IsNull) and
                                     vgPrm_EdRealFecSal);
  cedVlrSalarioBase.Enabled      := False;
  cedVlrTotalComissao.Enabled    := False;
  cedVlrTotalOutrasDesp.Enabled  := False;
  cedVlrTotalVale.Enabled        := False;
  cedVlrSalarioAPagar.Enabled    := False;

  dbgComissoes.ReadOnly      := True;
  dbgOutrasDespesas.ReadOnly := True;
  dbgVales.ReadOnly          := True;
end;

procedure TFCadastroFechamentoSalario.dteDataPrevisaoPagtoExit(Sender: TObject);
begin
  inherited;

  if dmFechamentoSalario.cdsFechamento.State in [dsInsert, dsEdit] then
  begin
    if dteDataPrevisaoPagto.Date = 0 then
    begin
      dmFechamentoSalario.cdsFechamento.FieldByName('MES_PAGAMENTO').Value := Null;
      dmFechamentoSalario.cdsFechamento.FieldByName('ANO_PAGAMENTO').Value := Null;
    end
    else
    begin
      if dteDataPrevisaoPagto.Date < vgDataLancVigente then
      begin
        dmFechamentoSalario.cdsFechamento.FieldByName('DATA_PREV_PAGAMENTO').AsDateTime := vgDataLancVigente;
        dmFechamentoSalario.cdsFechamento.FieldByName('MES_PAGAMENTO').AsInteger        := MonthOf(vgDataLancVigente);
        dmFechamentoSalario.cdsFechamento.FieldByName('ANO_PAGAMENTO').AsInteger        := YearOf(vgDataLancVigente);
      end
      else
      begin
        dmFechamentoSalario.cdsFechamento.FieldByName('DATA_PREV_PAGAMENTO').AsDateTime := dteDataPrevisaoPagto.Date;
        dmFechamentoSalario.cdsFechamento.FieldByName('MES_PAGAMENTO').AsInteger        := MonthOf(dteDataPrevisaoPagto.Date);
        dmFechamentoSalario.cdsFechamento.FieldByName('ANO_PAGAMENTO').AsInteger        := YearOf(dteDataPrevisaoPagto.Date);
      end;
    end;
  end;
end;

procedure TFCadastroFechamentoSalario.dteDataPrevisaoPagtoKeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dmFechamentoSalario.cdsFechamento.State in [dsInsert, dsEdit] then
    begin
      if dteDataPrevisaoPagto.Date = 0 then
      begin
        dmFechamentoSalario.cdsFechamento.FieldByName('MES_PAGAMENTO').Value := Null;
        dmFechamentoSalario.cdsFechamento.FieldByName('ANO_PAGAMENTO').Value := Null;
      end
      else
      begin
        if dteDataPrevisaoPagto.Date < vgDataLancVigente then
        begin
          dmFechamentoSalario.cdsFechamento.FieldByName('DATA_PREV_PAGAMENTO').AsDateTime := vgDataLancVigente;
          dmFechamentoSalario.cdsFechamento.FieldByName('MES_PAGAMENTO').AsInteger        := MonthOf(vgDataLancVigente);
          dmFechamentoSalario.cdsFechamento.FieldByName('ANO_PAGAMENTO').AsInteger        := YearOf(vgDataLancVigente);
        end
        else
        begin
          dmFechamentoSalario.cdsFechamento.FieldByName('DATA_PREV_PAGAMENTO').AsDateTime := dteDataPrevisaoPagto.Date;
          dmFechamentoSalario.cdsFechamento.FieldByName('MES_PAGAMENTO').AsInteger        := MonthOf(dteDataPrevisaoPagto.Date);
          dmFechamentoSalario.cdsFechamento.FieldByName('ANO_PAGAMENTO').AsInteger        := YearOf(dteDataPrevisaoPagto.Date);
        end;
      end;
    end;

    btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    btnCancelar.Click;
end;

procedure TFCadastroFechamentoSalario.dteDataRealizacaoPagtoExit(
  Sender: TObject);
var
  iIdUsu: Integer;
  sNomeUsu, sSenhaUsu: String;
  lCancelado: Boolean;
begin
  inherited;

  lCancelado := False;

  if (vgOperacao = E) and vgPrm_EdRealFecSal then
  begin
    if dmFechamentoSalario.cdsFechamento.FieldByName('DATA_REAL_PAGAMENTO').AsDateTime <>
      dmFechamentoSalario.cdsFechamento.FieldByName('DATA_REAL_PAGAMENTO_ANT').AsDateTime then
    begin
      if Application.MessageBox(PChar('Os Pagamentos j� foram Realizados e ser� preciso informar LOGIN ' +
                                      'e SENHA de Autorizador para alterar a Data de Realiza��o. ' + #13#10 +
                                      'Deseja prosseguir?'), 'Aviso', MB_YESNO + MB_ICONQUESTION) = ID_YES then
      begin
        iIdUsu    := 0;
        sNomeUsu  := '';
        sSenhaUsu := '';

        repeat
          repeat
            if not InputQuery('LOGIN', 'Por favor, informe o LOGIN do Autorizador:', sNomeUsu) then
              lCancelado := True;
          until (Trim(sNomeUsu) <> '') or lCancelado;

          if not lCancelado then
          begin
            repeat
              if not InputQuery('SENHA', #31'Por favor, informe a SENHA do Autorizador:', sSenhaUsu) then
                lCancelado := True;
            until (Trim(sSenhaUsu) <> '') or lCancelado;
          end;
        until (dmGerencial.VerificarAutorizacao(iIdUsu, sNomeUsu, sSenhaUsu) = True) or lCancelado;

        if not lCancelado then
          BS.GravarUsuarioLog(2, '', ('O usu�rio mudou a Data de Realiza��o dos Pagamentos de ' +
                                      dmFechamentoSalario.cdsFechamento.FieldByName('DATA_REAL_PAGAMENTO_ANT').AsString +
                                      ' para ' +
                                      dmFechamentoSalario.cdsFechamento.FieldByName('DATA_REAL_PAGAMENTO').AsString))
        else
        begin
          if not (dmFechamentoSalario.cdsFechamento.State in [dsInsert, dsEdit]) then
            dmFechamentoSalario.cdsFechamento.Edit;

          dmFechamentoSalario.cdsFechamento.FieldByName('DATA_REAL_PAGAMENTO').AsDateTime := dmFechamentoSalario.cdsFechamento.FieldByName('DATA_REAL_PAGAMENTO_ANT').AsDateTime;
        end;
      end
      else
      begin
        if not (dmFechamentoSalario.cdsFechamento.State in [dsInsert, dsEdit]) then
          dmFechamentoSalario.cdsFechamento.Edit;

        dmFechamentoSalario.cdsFechamento.FieldByName('DATA_REAL_PAGAMENTO').AsDateTime := dmFechamentoSalario.cdsFechamento.FieldByName('DATA_REAL_PAGAMENTO_ANT').AsDateTime;
      end;

      if dteDataRealizacaoPagto.Date = 0 then
      begin
        dmFechamentoSalario.cdsFechamento.FieldByName('MES_PAGAMENTO').Value := Null;
        dmFechamentoSalario.cdsFechamento.FieldByName('ANO_PAGAMENTO').Value := Null;
      end
      else
      begin
        dmFechamentoSalario.cdsFechamento.FieldByName('MES_PAGAMENTO').AsInteger := MonthOf(dteDataRealizacaoPagto.Date);
        dmFechamentoSalario.cdsFechamento.FieldByName('ANO_PAGAMENTO').AsInteger := YearOf(dteDataRealizacaoPagto.Date);
      end;
    end;
  end;
end;

procedure TFCadastroFechamentoSalario.FinalizarComponenteFuncionario;
begin
  FreeAndNil(ListaFuncionarios);
end;

procedure TFCadastroFechamentoSalario.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  if lPodeCancelar then
  begin
    FinalizarComponenteFuncionario;

    dmFechamentoSalario.cdsFechamento.Cancel;
    dmFechamentoSalario.cdsSalario.Cancel;

    dmFechamentoSalario.cdsFechamento.Close;
    dmFechamentoSalario.cdsSalario.Close;

    FreeAndNil(dmFechamentoSalario);

    vgOperacao     := VAZIO;
    vgIdConsulta   := 0;
    vgOrigemFiltro := False;
  end
  else
    Action := caNone;
end;

procedure TFCadastroFechamentoSalario.FormCreate(Sender: TObject);
begin
  inherited;

  InicializarComponenteFuncionario;

  iIdSalario     := 0;
  iIdFuncionario := 0;

  if vgOperacao = I then
    dDataVenc := dmPrincipal.CalcularDataPagamentoFunc;

  cTotalComissoes       := 0;
  cTotalVales           := 0;
  cTotalOutrasDespesasD := 0;
  cTotalOutrasDespesasA := 0;

  iMesRef := 0;
  iAnoRef := 0;

  dmPrincipal.RetornaMesAnoPagamentoSal(iMesRef, iAnoRef);
end;

procedure TFCadastroFechamentoSalario.FormShow(Sender: TObject);
var
  iIdSalPago: Integer;
begin
  with dmFechamentoSalario do
  begin

    //FECHAMENTO DE SALARIOS
    cdsFechamento.Close;
    cdsFechamento.Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := vgIdConsulta;
    cdsFechamento.Open;

    //SALARIO
    cdsSalario.Close;
    cdsSalario.Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := vgIdConsulta;
    cdsSalario.Open;

    //TABELAS AGREGADAS
    //Comissao
    dmPrincipal.cdsComissoes.Close;
    dmPrincipal.cdsComissoes.Params.ParamByName('MES_REFERENCIA').Value := iMesRef;
    dmPrincipal.cdsComissoes.Params.ParamByName('ANO_REFERENCIA').Value := iAnoRef;
    dmPrincipal.cdsComissoes.Params.ParamByName('ID_FUNC01').Value      := cdsSalario.FieldByName('ID_FUNCIONARIO_FK').AsInteger;
    dmPrincipal.cdsComissoes.Params.ParamByName('ID_FUNC02').Value      := cdsSalario.FieldByName('ID_FUNCIONARIO_FK').AsInteger;
    dmPrincipal.cdsComissoes.Open;

    //Vale
    dmPrincipal.cdsVales.Close;
    dmPrincipal.cdsVales.Params.ParamByName('MES_REFERENCIA').Value := iMesRef;
    dmPrincipal.cdsVales.Params.ParamByName('ANO_REFERENCIA').Value := iAnoRef;
    dmPrincipal.cdsVales.Params.ParamByName('ID_FUNCIONARIO').Value := cdsSalario.FieldByName('ID_FUNCIONARIO_FK').AsInteger;
    dmPrincipal.cdsVales.Open;

    //Outra Despesa
    dmPrincipal.cdsOutrasDesp.Close;
    dmPrincipal.cdsOutrasDesp.Params.ParamByName('MES_REFERENCIA').Value := iMesRef;
    dmPrincipal.cdsOutrasDesp.Params.ParamByName('ANO_REFERENCIA').Value := iAnoRef;
    dmPrincipal.cdsOutrasDesp.Params.ParamByName('ID_FUNCIONARIO').Value := cdsSalario.FieldByName('ID_FUNCIONARIO_FK').AsInteger;
    dmPrincipal.cdsOutrasDesp.Open;

    if vgOperacao = I then
    begin
      //FECHAMENTO DE SALARIOS
      cdsFechamento.Append;

      if dDataVenc < vgDataLancVigente then
      begin
        cdsFechamento.FieldByName('DATA_PREV_PAGAMENTO').AsDateTime := vgDataLancVigente;
        cdsFechamento.FieldByName('MES_PAGAMENTO').AsInteger        := MonthOf(vgDataLancVigente);
        cdsFechamento.FieldByName('ANO_PAGAMENTO').AsInteger        := YearOf(vgDataLancVigente);
      end
      else
      begin
        cdsFechamento.FieldByName('DATA_PREV_PAGAMENTO').AsDateTime := dDataVenc;
        cdsFechamento.FieldByName('MES_PAGAMENTO').AsInteger        := MonthOf(dDataVenc);
        cdsFechamento.FieldByName('ANO_PAGAMENTO').AsInteger        := YearOf(dDataVenc);
      end;

      cdsFechamento.FieldByName('PREV_ID_USUARIO').AsInteger      := vgUsu_Id;
      cdsFechamento.FieldByName('FLG_STATUS').AsString            := 'P';
      cdsFechamento.FieldByName('MES_REFERENCIA').AsInteger       := iMesRef;
      cdsFechamento.FieldByName('ANO_REFERENCIA').AsInteger       := iAnoRef;

      btnIncluirFunc.Click;
    end
    else if vgOperacao = E then
    begin
      //FECHAMENTO DE SALARIOS
      dDataVenc := cdsFechamento.FieldByName('DATA_PREV_PAGAMENTO').AsDateTime;

      cdsFechamento.Edit;

      if not cdsFechamento.FieldByName('DATA_REAL_PAGAMENTO').IsNull then
        cdsFechamento.FieldByName('DATA_REAL_PAGAMENTO_ANT').AsDateTime := cdsFechamento.FieldByName('DATA_REAL_PAGAMENTO').AsDateTime;

      //SALARIO
      cdsSalario.First;

      while not cdsSalario.Eof do
      begin
        cdsSalario.Edit;
        cdsSalario.FieldByName('PRE_CADASTRADO').AsBoolean := True;
        cdsSalario.Post;

        cdsSalario.Next;
      end;

      iIdSalario := cdsSalario.RecordCount;
    end;
  end;

  inherited;

  dmFechamentoSalario.cdsSalario.First;

  if edtAnoReferencia.CanFocus then
    edtAnoReferencia.SetFocus;
end;

function TFCadastroFechamentoSalario.GravarDadosAto(
  var Msg: String): Boolean;
begin
  //
end;

function TFCadastroFechamentoSalario.GravarDadosGenerico(
  var Msg: String): Boolean;
var
  IdFech, IdSal, IdItem: Integer;
  cTotalBase, cTotalComissao, cTotalODespesa, cTotalVale, cTotalAPagar: Currency;
  Op: TOperacao;
  lItemNovo: Boolean;
  iMes, iAno: Integer;
  lContinuar: Boolean;
  QryAux, QryEdAux: TFDQuery;
begin
  Result := True;
  Msg    := '';

  cTotalBase     := 0;
  cTotalComissao := 0;
  cTotalODespesa := 0;
  cTotalVale     := 0;
  cTotalAPagar   := 0;

  IdFech := 0;
  IdSal  := 0;
  IdItem := 0;

  iMes := 0;
  iAno := 0;

  lContinuar := True;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    { FECHAMENTO DE SALARIOS }
    if vgOperacao = I then
    begin
      IdFech := BS.ProximoId('ID_FECHAMENTO_SALARIO', 'FECHAMENTO_SALARIO');
      dmFechamentoSalario.cdsFechamento.FieldByName('ID_FECHAMENTO_SALARIO').AsInteger := IdFech;
    end
    else
      IdFech := dmFechamentoSalario.cdsFechamento.FieldByName('ID_FECHAMENTO_SALARIO').AsInteger;

    dmFechamentoSalario.cdsFechamento.FieldByName('ID_FECHAMENTO_SALARIO').AsInteger := IdFech;

    if dmFechamentoSalario.cdsFechamento.State in [dsInsert, dsEdit]  then
    begin
      dmFechamentoSalario.cdsFechamento.Post;
      dmFechamentoSalario.cdsFechamento.ApplyUpdates(0);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;
  except
    on E: Exception do
    begin
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Result := False;
      Msg := 'Erro na grava��o do Dados do Fechamento de Sal�rios.';

      lContinuar := False;
    end;
  end;

  if lContinuar then
  begin
    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      { SALARIO }
      if dmFechamentoSalario.cdsSalario.RecordCount > 0 then
      begin
        dmFechamentoSalario.cdsSalario.First;
        IdSal := BS.ProximoId('ID_SALARIO', 'SALARIO');

        while not dmFechamentoSalario.cdsSalario.Eof do
        begin
          dmPrincipal.InicializarComponenteLancamento;

          if dmFechamentoSalario.cdsSalario.FieldByName('NOVO').AsBoolean then
          begin
            dmFechamentoSalario.cdsSalario.Edit;
            dmFechamentoSalario.cdsSalario.FieldByName('ID_SALARIO').AsInteger               := IdSal;
            dmFechamentoSalario.cdsSalario.FieldByName('ID_FECHAMENTO_SALARIO_FK').AsInteger := IdFech;
            dmFechamentoSalario.cdsSalario.Post;

            IncluirLancamento;

            Inc(IdSal);
          end
          else
          begin
            AtualizarLancamento;

            if dmFechamentoSalario.cdsSalario.FieldByName('EXCLUIDO').AsBoolean then
              dmFechamentoSalario.cdsSalario.Delete;
          end;

          dmPrincipal.FinalizarComponenteLancamento;

          //Caso a Data de Realizacao do Pagamento anterior esteja Diferente da Atual
          if (vgOperacao = E) and (vgPrm_EdRealFecSal) then
          begin
            if dmFechamentoSalario.cdsFechamento.FieldByName('DATA_REAL_PAGAMENTO').AsDateTime <>
              dmFechamentoSalario.cdsFechamento.FieldByName('DATA_REAL_PAGAMENTO_ANT').AsDateTime then
            begin
              QryAux   := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
              QryEdAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

              //Lancamento (Consulta)
              QryAux.Close;
              QryAux.SQL.Clear;
              QryAux.SQL.Text := 'SELECT COD_LANCAMENTO, ANO_LANCAMENTO ' +
                                   '  FROM LANCAMENTO ' +
                                   ' WHERE ID_ORIGEM      = :ID_ORIGEM ' +
                                   '   AND ID_NATUREZA_FK = :ID_NATUREZA';
              QryAux.Params.ParamByName('ID_ORIGEM').Value   := dmFechamentoSalario.cdsSalario.FieldByName('ID_SALARIO').AsInteger;
              QryAux.Params.ParamByName('ID_NATUREZA').Value := 1;  //SALARIO
              QryAux.Open;

              //Parcela do Lancamento
              QryEdAux.Close;
              QryEdAux.SQL.Clear;

              QryEdAux.SQL.Text := 'UPDATE LANCAMENTO_PARC ' +
                                   '   SET DATA_PAGAMENTO  = :DATA_PAGAMENTO ' +
                                   ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                                   '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ';

              QryEdAux.Params.ParamByName('DATA_PAGAMENTO').Value  := dmFechamentoSalario.cdsFechamento.FieldByName('DATA_REAL_PAGAMENTO').AsDateTime;
              QryEdAux.Params.ParamByName('COD_LANCAMENTO').Value  := QryAux.FieldByName('COD_LANCAMENTO').AsInteger;
              QryEdAux.Params.ParamByName('ANO_LANCAMENTO').Value  := QryAux.FieldByName('ANO_LANCAMENTO').AsInteger;

              QryEdAux.ExecSQL;

              FreeAndNil(QryAux);
              FreeAndNil(QryEdAux);
            end;
          end;

          dmFechamentoSalario.cdsSalario.Next;
        end;
      end;

      if dmFechamentoSalario.cdsSalario.RecordCount > 0 then
          dmFechamentoSalario.cdsSalario.ApplyUpdates(0);

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      Msg := 'Dados do Fechamento de Sal�rios gravados com sucesso!';
    except
      on E: Exception do
      begin
        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Rollback;

        Result := False;
        lContinuar := False;

        Msg := 'Erro na grava��o dos Dados do Fechamento de Sal�rios.';
      end;
    end;
  end;

  if lContinuar then
  begin
    try
      //Acertar Valor do Fechamento
      dmFechamentoSalario.cdsSalario.Close;
      dmFechamentoSalario.cdsSalario.Params.ParamByName('ID_FECHAMENTO_SALARIO').Value := IdFech;
      dmFechamentoSalario.cdsSalario.Open;

      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      //Verifica Mes e Ano de Referencia
      if iMesRef < Integer(MonthOf(Date)) then
      begin
        if iMesRef = 12 then
        begin
          iMes := 1;
          iAno := (iAnoRef + 1);
        end
        else
        begin
          iMes := (iMesRef + 1);
          iAno := iAnoRef;
        end;
      end
      else
      begin
        iMes := iMesRef;
        iAno := iAnoRef;
      end;

      dmFechamentoSalario.cdsSalario.First;

      while not dmFechamentoSalario.cdsSalario.Eof do
      begin
        //Calculo dos Totais
        cTotalBase     := (cTotalBase + dmFechamentoSalario.cdsSalario.FieldByName('VR_SALARIO_BASE').AsCurrency);
        cTotalComissao := (cTotalComissao + dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_COMISSAO').AsCurrency);
        cTotalODespesa := (cTotalODespesa + dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_OUTRADESPESA').AsCurrency);
        cTotalVale     := (cTotalVale + dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_VALE').AsCurrency);
        cTotalAPagar   := (cTotalAPagar + dmFechamentoSalario.cdsSalario.FieldByName('VR_SALARIO_APAGAR').AsCurrency);

        //Informar o Id do Fechamento nas tabelas agregadas
        dmPrincipal.ApontarFechamentoAgregadas(iMes,
                                               iAno,
                                               IdFech,
                                               dmFechamentoSalario.cdsSalario.FieldByName('ID_FUNCIONARIO_FK').AsInteger);

        dmFechamentoSalario.cdsSalario.Next;
      end;

      dmFechamentoSalario.cdsFechamento.Edit;
      dmFechamentoSalario.cdsFechamento.FieldByName('VR_TOTAL_BASE').AsCurrency         := cTotalBase;
      dmFechamentoSalario.cdsFechamento.FieldByName('VR_TOTAL_COMISSAO').AsCurrency     := cTotalComissao;
      dmFechamentoSalario.cdsFechamento.FieldByName('VR_TOTAL_OUTRADESPESA').AsCurrency := cTotalODespesa;
      dmFechamentoSalario.cdsFechamento.FieldByName('VR_TOTAL_VALE').AsCurrency         := cTotalVale;
      dmFechamentoSalario.cdsFechamento.FieldByName('VR_TOTAL_APAGAR').AsCurrency       := cTotalAPagar;
      dmFechamentoSalario.cdsFechamento.Post;
      dmFechamentoSalario.cdsFechamento.ApplyUpdates(0);

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      Msg := 'Dados do Fechamento de Sal�rios gravados com sucesso!';
    except
      on E: Exception do
      begin
        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Rollback;

        Result := False;
        Msg := 'Erro na grava��o dos Dados do Fechamento de Sal�rios.';
      end;
    end;
  end;

  if not lContinuar then
  begin
    Result := False;
    Msg := 'Erro na grava��o dos Dados do Fechamento de Sal�rios.';
  end;
end;

procedure TFCadastroFechamentoSalario.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta do Fechamento de Sal�rios',
                          vgIdConsulta, 'FECHAMENTO_SALARIO');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclus�o do Fechamento de Sal�rios',
                          vgIdConsulta, 'FECHAMENTO_SALARIO');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da edi��o desse Fechamento de Sal�rios:', '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de Fechamento de Sal�rios';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          vgIdConsulta, 'FECHAMENTO_SALARIO');
    end;
  end;
end;

procedure TFCadastroFechamentoSalario.IncluirLancamento;
var
  IdItem, IdDet, Indice, IdForn, IdNat, IdCat, IdSubCat, Cod: Integer;
  Doc, Descr, Abrev, Obs, UMed, NFantasia, RSocial: String;
  Med: Real;
  QryAuxS: TFDQuery;
  sIR, sCP, sLA: String;
begin
  IdItem   := 0;
  IdDet    := 0;
  Indice   := 0;
  IdForn   := 0;
  IdNat    := 0;
  IdCat    := 0;
  IdSubCat := 0;
  Cod      := 0;

  Doc       := '';
  Descr     := '';
  Abrev     := '';
  Obs       := '';
  NFantasia := '';
  RSocial   := '';
  UMed      := '';
  sIR       := '';
  sCP       := '';
  sLA       := '';

  Med := 0;

  QryAuxS := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  { LANCAMENTO }
  dmPrincipal.RetornarClassificacaoNatureza(1, sIR, sCP, sLA);

  //Fornecedor
  Doc       := dmFechamentoSalario.cdsSalario.FieldByName('CPF').AsString;
  Obs       := 'Cadastro realizado automaticamente em inclus�o de SAL�RIO.';
  NFantasia := dmFechamentoSalario.cdsSalario.FieldByName('FUNCIONARIO').AsString;
  RSocial   := dmFechamentoSalario.cdsSalario.FieldByName('FUNCIONARIO').AsString;

  dmPrincipal.RetornarClienteFornecedor('F',
                                        IdForn,
                                        Doc,
                                        Obs,
                                        NFantasia,
                                        RSocial);

  //Categoria
  IdNat := 1;
  IdCat := 0;
  Cod   := 1;  //Despesa
  Descr := 'COLABORADORES';
  Obs   := 'Inclus�o AUTOM�TICA de Categoria de Despesa';

  dmPrincipal.RetornaCategoria(IdCat, IdNat, Cod, Descr, Obs);

  //SubCategoria
  IdSubCat := 0;
  Cod      := 1;  //Despesa
  Descr    := NFantasia;
  Obs      := 'Inclus�o AUTOM�TICA de Subcategoria de Despesa';

  dmPrincipal.RetornaSubCategoria(IdSubCat, IdCat, Cod, Descr, Obs);

  DadosLancamento := Lancamento.Create;

  DadosLancamento.DataLancamento   := dteDataPrevisaoPagto.Date;
  DadosLancamento.TipoLancamento   := 'D';
  DadosLancamento.TipoCadastro     := 'A';
  DadosLancamento.IdCategoria      := IdCat;
  DadosLancamento.IdSubCategoria   := IdSubCat;
  DadosLancamento.IdCliFor         := IdForn;
  DadosLancamento.FlgIR            := sIR;
  DadosLancamento.FlgPessoal       := sCP;
  DadosLancamento.FlgFlutuante     := 'N';
  DadosLancamento.FlgAuxiliar      := sLA;
  DadosLancamento.FlgForaFechCaixa := 'N';
  DadosLancamento.IdNatureza       := 1;  //SALARIO
  DadosLancamento.QtdParcelas      := 1;
  DadosLancamento.CadIdUsuario     := vgUsu_Id;
  DadosLancamento.DataCadastro     := Now;
  DadosLancamento.FlgRecorrente    := 'N';
  DadosLancamento.FlgReplicado     := 'N';
  DadosLancamento.IdOrigem         := dmFechamentoSalario.cdsSalario.FieldByName('ID_SALARIO').AsInteger;
  DadosLancamento.VlrTotalPrev     := (dmFechamentoSalario.cdsSalario.FieldByName('VR_SALARIO_BASE').AsCurrency +
                                       dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_COMISSAO').AsCurrency -
                                       dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_OUTRADESPESA').AsCurrency);
  DadosLancamento.VlrTotalDesconto := dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_VALE').AsCurrency;
  DadosLancamento.Observacao       := 'Despesa originada pelo SAL�RIO';

  ListaLancamentos.Add(DadosLancamento);

  dmPrincipal.InsertLancamento(0);

  { DETALHE }
  //SALARIO
  //Item - Salario
//  IdItem := BS.ProximoId('ID_ITEM', 'ITEM');

  IdItem := 0;

  Descr := 'SAL�RIO';
  Abrev := 'SAL�RIO';
  Obs   := 'Cadastro realizado automaticamente em inclus�o de SAL�RIO.';
  UMed  := 'Unidade(s)';
  Med   := 1.00;

  dmPrincipal.RetornarItem('S', IdItem, Descr, Abrev,
                           Obs, UMed, Med);

  //Detalhe - Salario
  IdDet := BS.ProximoId('ID_LANCAMENTO_DET', 'LANCAMENTO_DET');

  DadosLancamentoDet := LancamentoDet.Create;

  DadosLancamentoDet.IdLancamentoDet := IdDet;
  DadosLancamentoDet.IdItem          := IdItem;
  DadosLancamentoDet.DescrItem       := Descr;
  DadosLancamentoDet.VlrUnitario     := dmFechamentoSalario.cdsSalario.FieldByName('VR_SALARIO_BASE').AsCurrency;
  DadosLancamentoDet.Quantidade      := 1;
  DadosLancamentoDet.VlrTotal        := dmFechamentoSalario.cdsSalario.FieldByName('VR_SALARIO_BASE').AsCurrency;
  DadosLancamentoDet.FlgCancelado    := 'N';

  ListaLancamentoDets.Add(DadosLancamentoDet);

  dmPrincipal.InsertLancamentoDet(Indice, 0);

  //COMISSOES
  if not dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_COMISSAO').IsNull then
  begin
    //Item - Comissao
//    Inc(IdItem);

    IdItem := 0;

    Descr := 'COMISS�ES';
    Abrev := 'COMISS�ES';
    Obs   := 'Cadastro realizado automaticamente em inclus�o de SAL�RIO.';
    UMed  := 'Unidade(s)';
    Med   := 1.00;

    dmPrincipal.RetornarItem('S', IdItem, Descr, Abrev,
                             Obs, UMed, Med);

    //Detalhe - Comissao
    Inc(IdDet);
    Inc(Indice);

    DadosLancamentoDet := LancamentoDet.Create;

    DadosLancamentoDet.IdLancamentoDet := IdDet;
    DadosLancamentoDet.IdItem          := IdItem;
    DadosLancamentoDet.DescrItem       := Descr;
    DadosLancamentoDet.VlrUnitario     := dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_COMISSAO').AsCurrency;
    DadosLancamentoDet.Quantidade      := 1;
    DadosLancamentoDet.VlrTotal        := dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_COMISSAO').AsCurrency;
    DadosLancamentoDet.FlgCancelado    := 'N';

    ListaLancamentoDets.Add(DadosLancamentoDet);

    dmPrincipal.InsertLancamentoDet(Indice, 0);
  end;

  //OUTRAS DESPESAS
  if not dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_OUTRADESPESA').IsNull then
  begin
    //Item - Outra Despesa (Acrescimos)
//    Inc(IdItem);
    Inc(Indice);

    IdItem := 0;

    Descr := 'OUTROS LAN�AMENTOS (Acr�scimos)';
    Abrev := 'OUTROS LAN�AMENTOS (Acr�scimos)';
    Obs   := 'Cadastro realizado automaticamente em inclus�o de SAL�RIO.';
    UMed  := 'Unidade(s)';
    Med   := 1.00;

    dmPrincipal.RetornarItem('S', IdItem, Descr, Abrev,
                             Obs, UMed, Med);

    //Detalhe - Outra Despesa
    Inc(IdDet);

    DadosLancamentoDet := LancamentoDet.Create;

    DadosLancamentoDet.IdLancamentoDet := IdDet;
    DadosLancamentoDet.IdItem          := IdItem;
    DadosLancamentoDet.DescrItem       := Descr;
    DadosLancamentoDet.VlrUnitario     := dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_OD_A').AsCurrency;
    DadosLancamentoDet.Quantidade      := 1;
    DadosLancamentoDet.VlrTotal        := dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_OD_A').AsCurrency;
    DadosLancamentoDet.FlgCancelado    := 'N';

    ListaLancamentoDets.Add(DadosLancamentoDet);

    dmPrincipal.InsertLancamentoDet(Indice, 0);

    //Item - Outra Despesa (Decrescimos)
//    Inc(IdItem);
    Inc(Indice);

    IdItem := 0;

    Descr := 'OUTROS LAN�AMENTOS (Decr�scimos)';
    Abrev := 'OUTROS LAN�AMENTOS (Decr�scimos)';
    Obs   := 'Cadastro realizado automaticamente em inclus�o de SAL�RIO.';
    UMed  := 'Unidade(s)';
    Med   := 1.00;

    dmPrincipal.RetornarItem('S', IdItem, Descr, Abrev,
                             Obs, UMed, Med);

    //Detalhe - Outra Despesa
    Inc(IdDet);

    DadosLancamentoDet := LancamentoDet.Create;

    DadosLancamentoDet.IdLancamentoDet := IdDet;
    DadosLancamentoDet.IdItem          := IdItem;
    DadosLancamentoDet.DescrItem       := Descr;
    DadosLancamentoDet.VlrUnitario     := dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_OD_D').AsCurrency;
    DadosLancamentoDet.Quantidade      := 1;
    DadosLancamentoDet.VlrTotal        := dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_OD_D').AsCurrency;
    DadosLancamentoDet.FlgCancelado    := 'N';

    ListaLancamentoDets.Add(DadosLancamentoDet);

    dmPrincipal.InsertLancamentoDet(Indice, 0);
  end;

  //VALES
  if not dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_VALE').IsNull then
  begin
    //Item - Vale
//    Inc(IdItem);

    IdItem := 0;

    Descr := 'VALES';
    Abrev := 'VALES';
    Obs   := 'Cadastro realizado automaticamente em inclus�o de SAL�RIO.';
    UMed  := 'Unidade(s)';
    Med   := 1.00;

    dmPrincipal.RetornarItem('S', IdItem, Descr, Abrev,
                             Obs, UMed, Med);

    //Detalhe - Vale
    Inc(IdDet);
    Inc(Indice);

    DadosLancamentoDet := LancamentoDet.Create;

    DadosLancamentoDet.IdLancamentoDet := IdDet;
    DadosLancamentoDet.IdItem          := IdItem;
    DadosLancamentoDet.DescrItem       := Descr;
    DadosLancamentoDet.VlrUnitario     := dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_VALE').AsCurrency;
    DadosLancamentoDet.Quantidade      := 1;
    DadosLancamentoDet.VlrTotal        := dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_VALE').AsCurrency;
    DadosLancamentoDet.FlgCancelado    := 'N';

    ListaLancamentoDets.Add(DadosLancamentoDet);

    dmPrincipal.InsertLancamentoDet(Indice, 0);
  end;

  { PARCELA }
  DadosLancamentoParc := LancamentoParc.Create;

  DadosLancamentoParc.NumParcela     := 1;
  DadosLancamentoParc.IdFormaPagto   := 6;  //Dinheiro
  DadosLancamentoParc.DataLancParc   := dteDataPrevisaoPagto.Date;
  DadosLancamentoParc.DataVencimento := dteDataPrevisaoPagto.Date;
  DadosLancamentoParc.CadIdUsuario   := vgUsu_Id;
  DadosLancamentoParc.DataCadastro   := Now;
  DadosLancamentoParc.VlrPrevisto    := (dmFechamentoSalario.cdsSalario.FieldByName('VR_SALARIO_BASE').AsCurrency +
                                         dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_COMISSAO').AsCurrency -
                                         dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_OUTRADESPESA').AsCurrency);
  DadosLancamentoParc.VlrDesconto    := dmFechamentoSalario.cdsSalario.FieldByName('VR_TOTAL_VALE').AsCurrency;

  ListaLancamentoParcs.Add(DadosLancamentoParc);

  dmPrincipal.InsertLancamentoParc(0, 0);

  { LOG }
  with QryAuxS do
  begin
    QryAuxS.Close;
    QryAuxS.SQL.Clear;
    QryAuxS.SQL.Text := 'INSERT INTO USUARIO_LOG (ID_USUARIO_LOG, ID_USUARIO, ID_MODULO_FK, ' +
                        '                         TIPO_LANCAMENTO, CODIGO_LAN, ANO_LAN, ID_ORIGEM, ' +
                        '                         TABELA_ORIGEM, TIPO_ACAO, OBS_USUARIO_LOG) ' +
                        '                 VALUES (:ID_USUARIO_LOG, :ID_USUARIO, :ID_MODULO, ' +
                        '                         :TIPO_LANCAMENTO, :CODIGO_LAN, :ANO_LAN, :ID_ORIGEM, ' +
                        '                         :TABELA_ORIGEM, :TIPO_ACAO, :OBS_USUARIO_LOG)';

    QryAuxS.Params.ParamByName('ID_USUARIO_LOG').Value  := BS.ProximoId('ID_USUARIO_LOG', 'USUARIO_LOG');
    QryAuxS.Params.ParamByName('ID_USUARIO').Value      := vgUsu_Id;
    QryAuxS.Params.ParamByName('ID_MODULO').Value       := 80;
    QryAuxS.Params.ParamByName('TIPO_LANCAMENTO').Value := 'D';
    QryAuxS.Params.ParamByName('CODIGO_LAN').Value      := ListaLancamentos[0].CodLancamento;
    QryAuxS.Params.ParamByName('ANO_LAN').Value         := ListaLancamentos[0].AnoLancamento;
    QryAuxS.Params.ParamByName('ID_ORIGEM').Value       := Null;
    QryAuxS.Params.ParamByName('TABELA_ORIGEM').Value   := Null;
    QryAuxS.Params.ParamByName('TIPO_ACAO').Value       := 'I';
    QryAuxS.Params.ParamByName('OBS_USUARIO_LOG').Value := 'Inclus�o AUTOM�TICA de Despesa de DE SAL�RIO.';

    QryAuxS.ExecSQL;
  end;

  FreeAndNil(QryAuxS);
end;

procedure TFCadastroFechamentoSalario.InicializarComponenteFuncionario;
begin
  //FUNCIONARIO
  Funcionario       := TClasseFuncionario;
  ListaFuncionarios := TList<TDadosClasseFuncionario>.Create;
end;

procedure TFCadastroFechamentoSalario.dbgFuncionariosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;

  AlternarCorGrid(Sender, Rect, DataCol, Column, State);
end;

procedure TFCadastroFechamentoSalario.dbgOutrasDespesasDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;

  AlternarCorGrid(Sender, Rect, DataCol, Column, State);
end;

procedure TFCadastroFechamentoSalario.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroFechamentoSalario.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

function TFCadastroFechamentoSalario.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if dteDataPrevisaoPagto.Date = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a DATA DE PREVIS�O DO PAGAMENTO.';
    DadosMsgErro.Componente := dteDataPrevisaoPagto;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(cbMesPagamento.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher o M�S DE PAGAMENTO.';
    DadosMsgErro.Componente := cbMesPagamento;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtAnoPagamento.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a ANO DE PAGAMENTO.';
    DadosMsgErro.Componente := edtAnoPagamento;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end
  else
  begin
    if StrToInt(edtAnoPagamento.Text) <> YearOf(dteDataPrevisaoPagto.Date) then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') ANO DE PAGAMENTO est� divergente da DE PREVIS�O DO PAGAMENTO.';
      DadosMsgErro.Componente := edtAnoPagamento;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end
  end;

  if not dmFechamentoSalario.cdsFechamento.FieldByName('PAG_ID_USUARIO').IsNull then
  begin
    if (vgOperacao = E) and
      (dteDataRealizacaoPagto.Date = 0) then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta preencher a DATA DE REALIZA��O DO PAGAMENTO.';
      DadosMsgErro.Componente := dteDataPrevisaoPagto;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroFechamentoSalario.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

{ TDadosClasseFuncionario }

constructor TDadosClasseFuncionario.Create;
begin
  IdFuncionario   := 0;
  NomeFuncionario := '';
  VrSalario       := 0;
end;

destructor TDadosClasseFuncionario.Destroy;
begin

  inherited;
end;

end.
