{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroRelatorioExtratoContas.pas
  Descricao:   Filtro Relatorios de Extrato de Contas (R01 - R01DET)
  Author   :   Cristina
  Date:        29-dez-2016
  Last Update: 11-out-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroRelatorioExtratoContas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroRelatorioPadrao, Vcl.StdCtrls,
  JvExControls, JvButton, JvTransparentButton, Vcl.ExtCtrls, Vcl.Mask, JvExMask,
  JvToolEdit, System.DateUtils, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Grids, Vcl.DBGrids, JvBaseEdits,
  JvDBControls;

type
  TFFiltroRelatorioExtratoContas = class(TFFiltroRelatorioPadrao)
    lblPeriodo: TLabel;
    lblPeriodoInicio: TLabel;
    lblPeriodoFim: TLabel;
    dteDataInicio: TJvDateEdit;
    dteDataFim: TJvDateEdit;
    lblSaldoInicial: TLabel;
    cedSaldoInicial: TJvCalcEdit;
    procedure FormShow(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
    procedure dteDataInicioExit(Sender: TObject);
  protected
    procedure GravarLog; override;
    procedure GerarRelatorio(ImpDet: Boolean); override;
    function VerificarFiltro: Boolean; override;
  private
    { Private declarations }

    procedure RetornarSaldoInicial;
  public
    { Public declarations }
  end;

var
  FFiltroRelatorioExtratoContas: TFFiltroRelatorioExtratoContas;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UDMRelatorios, UGDM, UVariaveisGlobais;

{ TFFiltroRelatorioExtratoContas }

procedure TFFiltroRelatorioExtratoContas.btnLimparClick(Sender: TObject);
begin
  inherited;

  dteDataInicio.Date := Date;
  dteDataFim.Date    := Date;

  cSaldoInicial := 0;

  chbTipoSintetico.Checked := True;
  chbTipoAnalitico.Checked := False;

  chbExportarPDF.Checked := False;

  chbExportarExcel.Enabled := vgPrm_ExpRelExtCnt;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioExtratoContas.btnVisualizarClick(Sender: TObject);
var
  SaldoParcial: Currency;
  Data: TDateTime;
begin
  inherited;

  if not VerificarFiltro then
    Exit;

  cSaldoInicial := cedSaldoInicial.Value;

  //Despesas x Receitas
  dmRelatorios.qryRel01_RecDesp.Close;
  dmRelatorios.qryRel01_RecDesp.Params.ParamByName('DATA_INI').Value := (FormatDateTime('YYYY-MM-DD', dteDataInicio.Date) + ' 00:00:00');
  dmRelatorios.qryRel01_RecDesp.Params.ParamByName('DATA_FIM').Value := (FormatDateTime('YYYY-MM-DD', dteDataFim.Date) + ' 23:59:59');
  dmRelatorios.qryRel01_RecDesp.Open;

  if (cedSaldoInicial.EditText = '') and
    (dmRelatorios.qryRel01_RecDesp.RecordCount = 0) then
  begin
    Application.MessageBox('N�o h� Lan�amentos registrados.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
  end
  else
  begin
    dmRelatorios.qryRel01_RecDesp.First;

    Data := dmRelatorios.qryRel01_RecDesp.FieldByName('DATA_PAGAMENTO').AsDateTime;

    while not dmRelatorios.qryRel01_RecDesp.Eof do
    begin
      if dmRelatorios.qryRel01_RecDesp.FieldByName('TIPO_LANCAMENTO').AsString = 'R' then
      begin
        if Data = dmRelatorios.qryRel01_RecDesp.FieldByName('DATA_PAGAMENTO').AsDateTime then
          SaldoParcial := (SaldoParcial + dmRelatorios.qryRel01_RecDesp.FieldByName('VR_PARCELA_PAGO').AsCurrency)
        else
        begin
          SaldoParcial := (dmRelatorios.qryRel01_RecDesp.FieldByName('VR_SALDO_DIA_INI').AsCurrency +
                           dmRelatorios.qryRel01_RecDesp.FieldByName('VR_PARCELA_PAGO').AsCurrency);
        end;
      end
      else if dmRelatorios.qryRel01_RecDesp.FieldByName('TIPO_LANCAMENTO').AsString = 'D' then
      begin
        if Data = dmRelatorios.qryRel01_RecDesp.FieldByName('DATA_PAGAMENTO').AsDateTime then
          SaldoParcial := (SaldoParcial - dmRelatorios.qryRel01_RecDesp.FieldByName('VR_PARCELA_PAGO').AsCurrency)
        else
        begin
          SaldoParcial := (dmRelatorios.qryRel01_RecDesp.FieldByName('VR_SALDO_DIA_INI').AsCurrency -
                           dmRelatorios.qryRel01_RecDesp.FieldByName('VR_PARCELA_PAGO').AsCurrency);
        end;
      end;

      dmRelatorios.qryRel01_RecDesp.Edit;
      dmRelatorios.qryRel01_RecDesp.FieldByName('VR_SALDO_PARCIAL').AsCurrency := SaldoParcial;
      dmRelatorios.qryRel01_RecDesp.Post;

      Data := dmRelatorios.qryRel01_RecDesp.FieldByName('DATA_PAGAMENTO').AsDateTime;

      dmRelatorios.qryRel01_RecDesp.Next;
    end;

    dmRelatorios.qryRel01_Totais.Close;
    dmRelatorios.qryRel01_Totais.Params.ParamByName('DATA_INI').Value := dteDataInicio.Date;
    dmRelatorios.qryRel01_Totais.Params.ParamByName('DATA_FIM').Value := dteDataFim.Date;
    dmRelatorios.qryRel01_Totais.Params.ParamByName('TP_LANC1').Value := 'R';
    dmRelatorios.qryRel01_Totais.Params.ParamByName('TP_LANC2').Value := 'D';
    dmRelatorios.qryRel01_Totais.Open;

    if chbTipoSintetico.Checked then
      GerarRelatorio(False);

    if chbTipoAnalitico.Checked then
      GerarRelatorio(True);
  end;
end;

procedure TFFiltroRelatorioExtratoContas.dteDataInicioExit(Sender: TObject);
begin
  inherited;

  if dteDataInicio.Date <> 0 then
    RetornarSaldoInicial;
end;

procedure TFFiltroRelatorioExtratoContas.FormShow(Sender: TObject);
begin
  inherited;

  dteDataInicio.Date := Date;
  dteDataFim.Date    := Date;

  cSaldoInicial := 0;

  cedSaldoInicial.ReadOnly := True;

  chbTipoSintetico.Checked := True;
  chbTipoAnalitico.Checked := False;

  chbExportarPDF.Checked := False;

  chbExportarExcel.Enabled := vgPrm_ExpRelExtCnt;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;

  chbExibirGrafico.Visible := False;

  if dteDataInicio.CanFocus then
    dteDataInicio.SetFocus;
end;

procedure TFFiltroRelatorioExtratoContas.GerarRelatorio(ImpDet: Boolean);
begin
  inherited;

  try
    sTituloRelatorio    := 'EXTRATO DE CONTAS';
    sSubtituloRelatorio := 'Per�odo de ' + DateToStr(dteDataInicio.Date) + ' a ' + DateToStr(dteDataFim.Date);

    if ImpDet then
      dmRelatorios.DefinirRelatorio(R01DET)
    else
      dmRelatorios.DefinirRelatorio(R01);

    dmRelatorios.ImprimirRelatorio(chbExportarPDF.Checked, chbExportarExcel.Checked);

    GravarLog;
  except
    Application.MessageBox('Erro ao gerar Extrato de Contas.',
                           'Erro',
                           MB_OK + MB_ICONERROR);
  end;
end;

procedure TFFiltroRelatorioExtratoContas.GravarLog;
begin
  inherited;

  BS.GravarUsuarioLog(50,
                      '',
                      'Impress�o de Relatorio de Extrato de Contas (' + DateToStr(dDataRelatorio) +
                                                                        ' ' +
                                                                        TimeToStr(tHoraRelatorio) + ')',
                      0,
                      '');
end;

procedure TFFiltroRelatorioExtratoContas.RetornarSaldoInicial;
begin

  cedSaldoInicial.Value := 0;

  dmRelatorios.qryRel01_SaldoInicial.Close;
  dmRelatorios.qryRel01_SaldoInicial.Params.ParamByName('DATA').Value := dteDataInicio.Date;
  dmRelatorios.qryRel01_SaldoInicial.Open;

  cedSaldoInicial.Value := dmRelatorios.qryRel01_SaldoInicial.FieldByName('VR_SALDO_DIA').AsCurrency;
end;

function TFFiltroRelatorioExtratoContas.VerificarFiltro: Boolean;
var
  Msg: String;
begin
  Result := True;

  if dteDataInicio.Date = 0 then
  begin
    Msg := '- Informe a DATA DE IN�CIO do Per�odo.';
    Result := False;
  end;

  if dteDataFim.Date = 0 then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe a DATA DE FIM do Per�odo.'
    else
      Msg := #13#10 + '- Informe a DATA DE FIM do Per�odo.';

    Result := False;
  end;

  if dteDataFim.Date < dteDataInicio.Date then
  begin
    if Trim(Msg) = '' then
      Msg := '- A DATA DE FIM do Per�odo n�o pode inferior � DATA DE IN�CIO do mesmo.'
    else
      Msg := #13#10 + '- A DATA DE FIM do Per�odo n�o pode ser inferior � DATA DE IN�CIO do mesmo.';

    Result := False;
  end;

  if cedSaldoInicial.EditText = '' then
  begin
    if Trim(Msg) = '' then
      Msg := '- O SALDO INICIAL deve ser informado (ainda que 0 (zero)).'
    else
      Msg := #13#10 + '- O SALDO INICIAL deve ser informado (ainda que 0 (zero)).';

    Result := False;
  end;

  if (not chbTipoAnalitico.Checked) and
    (not chbTipoSintetico.Checked) then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe o TIPO do Relat�rio de Extrato de Contas.'
    else
      Msg := #13#10 + '- Informe o TIPO do Relat�rio de Extrato de Contas.';

    Result := False;
  end;

  if not Result then
    Application.MessageBox(PChar(':: ATEN��O ::' + #13#10 + #13#10 + Msg),
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
end;

end.
