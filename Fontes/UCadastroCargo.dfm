inherited FCadastroCargo: TFCadastroCargo
  Caption = 'Cadastro de Cargo'
  ClientHeight = 124
  ClientWidth = 528
  ExplicitWidth = 534
  ExplicitHeight = 153
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 522
    Height = 118
    ExplicitWidth = 522
    ExplicitHeight = 118
    inherited pnlDados: TPanel
      Width = 408
      Height = 112
      ExplicitWidth = 408
      ExplicitHeight = 112
      object lblDescricaoCargo: TLabel [0]
        Left = 5
        Top = 5
        Width = 51
        Height = 14
        Caption = 'Descri'#231#227'o'
      end
      inherited pnlMsgErro: TPanel
        Top = 51
        Width = 408
        ExplicitTop = 51
        ExplicitWidth = 408
        inherited lblMensagemErro: TLabel
          Width = 408
        end
        inherited lbMensagemErro: TListBox
          Width = 408
          OnDblClick = lbMensagemErroDblClick
          ExplicitWidth = 408
        end
      end
      object edtDescricaoCargo: TDBEdit
        Left = 5
        Top = 21
        Width = 400
        Height = 22
        CharCase = ecUpperCase
        Color = 16114127
        DataField = 'DESCR_CARGO'
        DataSource = dsCadastro
        TabOrder = 1
        OnKeyPress = edtDescricaoCargoKeyPress
      end
    end
    inherited pnlMenu: TPanel
      Height = 112
      ExplicitHeight = 112
      inherited btnDadosPrincipais: TJvTransparentButton
        Height = 20
        Visible = False
        ExplicitWidth = 102
        ExplicitHeight = 20
      end
      inherited btnOk: TJvTransparentButton
        Top = 22
        ExplicitTop = 22
      end
      inherited btnCancelar: TJvTransparentButton
        Top = 67
        ExplicitTop = 67
      end
    end
  end
  inherited dsCadastro: TDataSource
    DataSet = cdsCargo
    Left = 470
    Top = 70
  end
  object qryCargo: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM CARGO'
      ' WHERE ID_CARGO = :ID_CARGO')
    Left = 320
    Top = 70
    ParamData = <
      item
        Position = 1
        Name = 'ID_CARGO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspCargo: TDataSetProvider
    DataSet = qryCargo
    Left = 368
    Top = 70
  end
  object cdsCargo: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_CARGO'
        ParamType = ptInput
      end>
    ProviderName = 'dspCargo'
    Left = 417
    Top = 70
    object cdsCargoID_CARGO: TIntegerField
      FieldName = 'ID_CARGO'
      Required = True
    end
    object cdsCargoDESCR_CARGO: TStringField
      FieldName = 'DESCR_CARGO'
      Size = 250
    end
    object cdsCargoCOD_CARGO: TIntegerField
      FieldName = 'COD_CARGO'
    end
  end
end
