{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroRelatorioLote.pas
  Descricao:   Filtro de Relatorio de Lotes de Folhas de Seguranca e RCPN
  Author   :   Pedro
  Date:        19-jun-2017
  Last Update: 11-out-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroRelatorioLote;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils,System.DateUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroRelatorioPadrao, Vcl.StdCtrls,
  JvExControls, JvButton, JvTransparentButton, Vcl.ExtCtrls, sGroupBox,
  UDMRelatorios, Vcl.Mask, sMaskEdit, sCustomComboEdit, sToolEdit, UDM,
  UBibliotecaSistema, UGDM;

type
  TFFiltroRelatorioLote = class(TFFiltroRelatorioPadrao)
    rgstatus: TsRadioGroup;
    dtInicial: TsDateEdit;
    dtFinal: TsDateEdit;
    procedure btnVisualizarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    procedure GravarLog; override;
    procedure GerarRelatorio(ImpDet: Boolean); override;
    function  VerificarFiltro: Boolean; override;
  private
    { Private declarations }
    lPodeCalcular:Boolean;
  public
    { Public declarations }

    lRCPN: Boolean;
  end;

var
  FFiltroRelatorioLote: TFFiltroRelatorioLote;

implementation

{$R *.dfm}

uses UVariaveisGlobais;

procedure TFFiltroRelatorioLote.btnLimparClick(Sender: TObject);
begin
  inherited;

  dtInicial.Date :=StartOfTheMonth(Date);
  dtFinal.Date:=EndOfTheMonth(Date);
  rgstatus.ItemIndex :=0;

  chbExportarPDF.Checked := False;

  chbExportarExcel.Enabled := vgPrm_ExpLtFlsSeg or vgPrm_ExpLtFlsRCPN;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;
end;

procedure TFFiltroRelatorioLote.btnVisualizarClick(Sender: TObject);
begin
  inherited;

  if not VerificarFiltro then
    Exit;

  dmRelatorios.FDLoteFolha.Close;
  if lRCPN then
    dmRelatorios.FDLoteFolha.Params.ParamByName('RCPN').Value :='S'
      else dmRelatorios.FDLoteFolha.Params.ParamByName('RCPN').Value :='N';

  case rgstatus.ItemIndex of
    0: dmRelatorios.FDLoteFolha.Params.ParamByName('ST').Value :='*';
    1: dmRelatorios.FDLoteFolha.Params.ParamByName('ST').Value :='N';
    2: dmRelatorios.FDLoteFolha.Params.ParamByName('ST').Value :='U';
    3: dmRelatorios.FDLoteFolha.Params.ParamByName('ST').Value :='C';
  end;

  if (dtInicial.Date<> 0) and (dtFinal.Date <> 0) then
  begin
    dmRelatorios.FDLoteFolha.Params.ParamByName('D1').Value := dtInicial.Date;
    dmRelatorios.FDLoteFolha.Params.ParamByName('D2').Value := dtFinal.Date;
  end
  else
  begin
    dmRelatorios.FDLoteFolha.Params.ParamByName('D1').Value := '12/30/1899';
    dmRelatorios.FDLoteFolha.Params.ParamByName('D2').Value := '12/30/1899';
  end;

  dmRelatorios.FDLoteFolha.Open;

  if not dmRelatorios.FDLoteFolha.IsEmpty then
  begin
    if chbTipoSintetico.Checked then
       GerarRelatorio(False);

    if chbTipoAnalitico.Checked then
       GerarRelatorio(True);
  end;

end;

procedure TFFiltroRelatorioLote.FormCreate(Sender: TObject);
begin
  inherited;

  if not lRCPN then
    Self.Caption := 'Relat�rio de Lote de Folha de Seguran�a'
  else
    Self.Caption := 'Relat�rio de Lote de Folha de Seguran�a de RCPN';

  dtInicial.Date :=StartOfTheMonth(Date);
  dtFinal.Date:=EndOfTheMonth(Date);
  rgstatus.ItemIndex :=0;
end;

procedure TFFiltroRelatorioLote.FormShow(Sender: TObject);
begin
  inherited;

  chbExportarExcel.Enabled := vgPrm_ExpLtFlsSeg or vgPrm_ExpLtFlsRCPN;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;
end;

procedure TFFiltroRelatorioLote.GerarRelatorio(ImpDet: Boolean);
begin
  inherited;
    try
    sTituloRelatorio    := 'LOTE DE FOLHA DE SEGURAN�A';
    sSubtituloRelatorio := 'Per�odo: ' + DateToStr(dtInicial.Date)+' at� '+DateToStr(dtFinal.Date);

    if ImpDet then
    begin
      lExibeDetalhe := True;
      dmRelatorios.DefinirRelatorio(R100DET);
    end
    else
    begin
      lExibeDetalhe := False;
      dmRelatorios.DefinirRelatorio(R100);
    end;

    lPodeCalcular := True;
    dmRelatorios.ImprimirRelatorio(chbExportarPDF.Checked, chbExportarExcel.Checked);

    GravarLog;
  except
    Application.MessageBox('Erro ao gerar Lote de Folha de Seguran�a.',
                           'Erro',
                           MB_OK + MB_ICONERROR);
  end;

end;

procedure TFFiltroRelatorioLote.GravarLog;
begin
  inherited;
  BS.GravarUsuarioLog(4,
                      '',
                      'Impress�o de Relatorio de Lote de Seguran�a (' + DateToStr(dDataRelatorio) +
                                                                     ' ' +
                                                                     TimeToStr(tHoraRelatorio) + ')',
                      0,
                      '');
end;

function TFFiltroRelatorioLote.VerificarFiltro: Boolean;
var
  Msg: String;
begin
  Result := True;

  if dtInicial.Date = 0 then
  begin
    Msg := '- Informe a DATA DE IN�CIO do Per�odo.';
    Result := False;
  end;

  if dtFinal.Date = 0 then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe a DATA DE FIM do Per�odo.'
    else
      Msg := #13#10 + '- Informe a DATA DE FIM do Per�odo.';

    Result := False;
  end;

  if dtFinal.Date < dtInicial.Date then
  begin
    if Trim(Msg) = '' then
      Msg := '- A DATA DE FIM do Per�odo n�o pode inferior � DATA DE IN�CIO do mesmo.'
    else
      Msg := #13#10 + '- A DATA DE FIM do Per�odo n�o pode ser inferior � DATA DE IN�CIO do mesmo.';

    Result := False;
  end;

  if (not chbTipoAnalitico.Checked) and
    (not chbTipoSintetico.Checked) then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe o TIPO do Relat�rio de Lotes de Folhas de Seguran�a.'
    else
      Msg := #13#10 + '- Informe o TIPO do Relat�rio de Lotes de Folhas de Seguran�a.';

    Result := False;
  end;

  if not Result then
    Application.MessageBox(PChar(':: ATEN��O ::' + #13#10 + #13#10 + Msg),
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
end;

end.
