{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroRelatorioFolha.pas
  Descricao:   Filtro de Relatorio de Folhas de Seguranca
  Author   :   Pedro
  Date:        19-jun-2017
  Last Update: 11-out-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroRelatorioFolha;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils,System.DateUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroRelatorioPadrao, Vcl.StdCtrls,
  JvExControls, JvButton, JvTransparentButton, Vcl.ExtCtrls, sGroupBox,
  UDMRelatorios, Vcl.Mask, sMaskEdit, sCustomComboEdit, sToolEdit, UDM,
  UBibliotecaSistema, UGDM;

type
  TFFiltroRelatorioFolha = class(TFFiltroRelatorioPadrao)
    rgstatus: TsRadioGroup;
    dtInicial: TsDateEdit;
    dtFinal: TsDateEdit;
    rgSistema: TsRadioGroup;
    procedure btnVisualizarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    procedure GravarLog; override;
    procedure GerarRelatorio(ImpDet: Boolean); override;
    function  VerificarFiltro: Boolean; override;
  private
    { Private declarations }
    lPodeCalcular:Boolean;
  public
    { Public declarations }

    lRCPN: Boolean;
  end;

var
  FFiltroRelatorioFolha: TFFiltroRelatorioFolha;

implementation

{$R *.dfm}

uses UVariaveisGlobais;

procedure TFFiltroRelatorioFolha.btnLimparClick(Sender: TObject);
begin
  inherited;

  dtInicial.Date :=StartOfTheMonth(Date);
  dtFinal.Date:=EndOfTheMonth(Date);
  rgstatus.ItemIndex :=0;

  chbExportarPDF.Checked := False;

  chbExportarExcel.Enabled := vgPrm_ExpFlsSeg or vgPrm_ExpFlsRCPN;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;
end;

procedure TFFiltroRelatorioFolha.btnVisualizarClick(Sender: TObject);
begin
  inherited;

  if not VerificarFiltro then
    Exit;

  dmRelatorios.FDFolha.Close;

  if lRCPN then
    dmRelatorios.FDFolha.Params.ParamByName('RCPN').Value := 'S'
  else
    dmRelatorios.FDFolha.Params.ParamByName('RCPN').Value := 'N';

  case rgstatus.ItemIndex of
    0: dmRelatorios.FDFolha.Params.ParamByName('ST').Value :='*';
    1: dmRelatorios.FDFolha.Params.ParamByName('ST').Value :='U';
    2: dmRelatorios.FDFolha.Params.ParamByName('ST').Value :='N';
    3: dmRelatorios.FDFolha.Params.ParamByName('ST').Value :='C';
  end;

  case rgSistema.ItemIndex of
    0: dmRelatorios.FDFolha.Params.ParamByName('SISTEMA').Value :=dmGerencial.Replicar('*',15);
    1: dmRelatorios.FDFolha.Params.ParamByName('SISTEMA').Value :='%'+'FIRMAS'+'%';
    2: dmRelatorios.FDFolha.Params.ParamByName('SISTEMA').Value :='%'+'NOTAS'+'%';
    3: dmRelatorios.FDFolha.Params.ParamByName('SISTEMA').Value :='%'+'RCPN'+'%';
    4: dmRelatorios.FDFolha.Params.ParamByName('SISTEMA').Value :='%'+'PROTESTO'+'%';
    5: dmRelatorios.FDFolha.Params.ParamByName('SISTEMA').Value :='%'+'RGI'+'%';
    6: dmRelatorios.FDFolha.Params.ParamByName('SISTEMA').Value :='%'+'RTD'+'%';
    7: dmRelatorios.FDFolha.Params.ParamByName('SISTEMA').Value :='%'+'RCPJ'+'%';
  end;

  if (dtInicial.Date<> 0) and (dtFinal.Date <> 0) then
  begin
    dmRelatorios.FDFolha.Params.ParamByName('D1').Value := dtInicial.Date;
    dmRelatorios.FDFolha.Params.ParamByName('D2').Value := dtFinal.Date;
  end
  else
  begin
    dmRelatorios.FDFolha.Params.ParamByName('D1').Value := '12/30/1899';
    dmRelatorios.FDFolha.Params.ParamByName('D2').Value := '12/30/1899';
  end;

  dmRelatorios.FDFolha.Open;

  if not dmRelatorios.FDFolha.IsEmpty then
  begin
    if chbTipoSintetico.Checked then
       GerarRelatorio(False);

    if chbTipoAnalitico.Checked then
       GerarRelatorio(True);
  end;
end;

procedure TFFiltroRelatorioFolha.FormCreate(Sender: TObject);
begin
  inherited;

  if not lRCPN then
  begin
    Self.Caption := 'Relat�rio Folha de Seguran�a';
    rgSistema.Items.Clear;
    rgSistema.Items.Add('TODOS');
    rgSistema.Items.Add('FIRMAS');
    rgSistema.Items.Add('NOTAS');
    rgSistema.Items.Add('RGI');
    rgSistema.Items.Add('PROTESTO');
    rgSistema.Items.Add('RTD');
    rgSistema.Items.Add('RCPJ');
    rgSistema.ItemIndex:=0;
  end
  else
  begin
    Self.Caption := 'Relat�rio Folha de Seguran�a de Rcpn';
    rgSistema.Items.Clear;
    rgSistema.Items.Add('RCPN');
    rgSistema.ItemIndex:=0;
    rgSistema.Visible:=False;
    Self.Height:=235;
  end;


  dtInicial.Date :=StartOfTheMonth(Date);
  dtFinal.Date:=EndOfTheMonth(Date);
  rgstatus.ItemIndex :=0;
end;

procedure TFFiltroRelatorioFolha.FormShow(Sender: TObject);
begin
  inherited;

  chbExportarExcel.Enabled := vgPrm_ExpFlsSeg or vgPrm_ExpFlsRCPN;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;
end;

procedure TFFiltroRelatorioFolha.GerarRelatorio(ImpDet: Boolean);
begin
  inherited;
    try
    sTituloRelatorio    := 'FOLHA DE SEGURAN�A';
    sSubtituloRelatorio := 'Per�odo: ' + DateToStr(dtInicial.Date)+' at� '+DateToStr(dtFinal.Date);

    if ImpDet then
    begin
      lExibeDetalhe := True;
      dmRelatorios.DefinirRelatorio(R101DET);
    end
    else
    begin
      lExibeDetalhe := False;
      dmRelatorios.DefinirRelatorio(R101);
    end;

    lPodeCalcular := True;
    dmRelatorios.ImprimirRelatorio(chbExportarPDF.Checked, chbExportarExcel.Checked);

    GravarLog;
  except
    Application.MessageBox('Erro ao gerar Folha de Seguran�a.',
                           'Erro',
                           MB_OK + MB_ICONERROR);
  end;

end;

procedure TFFiltroRelatorioFolha.GravarLog;
begin
  inherited;
  BS.GravarUsuarioLog(4,
                      '',
                      'Impress�o de Relatorio de Folha de Seguran�a (' + DateToStr(dDataRelatorio) +
                                                                     ' ' +
                                                                     TimeToStr(tHoraRelatorio) + ')',
                      0,
                      '');
end;

function TFFiltroRelatorioFolha.VerificarFiltro: Boolean;
var
  Msg: String;
begin
  Result := True;

  if dtInicial.Date = 0 then
  begin
    Msg := '- Informe a DATA DE IN�CIO do Per�odo.';
    Result := False;
  end;

  if dtFinal.Date = 0 then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe a DATA DE FIM do Per�odo.'
    else
      Msg := #13#10 + '- Informe a DATA DE FIM do Per�odo.';

    Result := False;
  end;

  if dtFinal.Date < dtInicial.Date then
  begin
    if Trim(Msg) = '' then
      Msg := '- A DATA DE FIM do Per�odo n�o pode inferior � DATA DE IN�CIO do mesmo.'
    else
      Msg := #13#10 + '- A DATA DE FIM do Per�odo n�o pode ser inferior � DATA DE IN�CIO do mesmo.';

    Result := False;
  end;

  if (not chbTipoAnalitico.Checked) and
    (not chbTipoSintetico.Checked) then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe o TIPO do Relat�rio de Folhas de Seguran�a.'
    else
      Msg := #13#10 + '- Informe o TIPO do Relat�rio de Folhas de Seguran�a.';

    Result := False;
  end;

  if not Result then
    Application.MessageBox(PChar(':: ATEN��O ::' + #13#10 + #13#10 + Msg),
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
end;

end.
