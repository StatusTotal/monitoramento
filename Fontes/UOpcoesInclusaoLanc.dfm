inherited FOpcoesInclusaoLanc: TFOpcoesInclusaoLanc
  Caption = 'FOpcoesInclusaoLanc'
  ClientWidth = 725
  ExplicitWidth = 725
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlOpcoes: TPanel
    Width = 719
    inherited pnlBotoes: TPanel
      Width = 713
      inherited btnOpcao2: TJvTransparentButton
        Action = actDespesasMenu
        DropDownMenu = ppmDespesa
        Images.ActiveImage = dmGerencial.Im32
        Images.ActiveIndex = 6
        Images.DisabledImage = dmGerencial.Im32Dis
        Images.DisabledIndex = 6
      end
      inherited btnOpcao1: TJvTransparentButton
        Action = actReceitasMenu
        DropDownMenu = ppmReceita
        Images.ActiveImage = dmGerencial.Im32
        Images.ActiveIndex = 30
        Images.DisabledImage = dmGerencial.Im32Dis
        Images.DisabledIndex = 30
      end
    end
  end
  object acmBotoes: TActionManager
    DisabledImages = dmGerencial.Im32
    Images = dmGerencial.Im32
    Left = 198
    Top = 22
    StyleName = 'Platform Default'
    object actReceitasMenu: TAction
      Category = 'Receita'
      Caption = '&Receita'
      ImageIndex = 30
      OnExecute = actReceitasMenuExecute
    end
    object actIncluirReceita: TAction
      Category = 'Receita'
      Caption = '&Lan'#231'ar Receita'
      ImageIndex = 30
      OnExecute = actIncluirReceitaExecute
    end
    object actDespesasMenu: TAction
      Category = 'Despesa'
      Caption = '&Despesa'
      ImageIndex = 6
      OnExecute = actDespesasMenuExecute
    end
    object actIncluirDespesa: TAction
      Category = 'Despesa'
      Caption = '&Lan'#231'ar Despesa'
      ImageIndex = 6
      OnExecute = actIncluirDespesaExecute
    end
    object actIncluirRepasseMenu: TAction
      Category = 'Despesa'
      Caption = '&Repasse de Receitas'
      ImageIndex = 56
      OnExecute = actIncluirRepasseMenuExecute
    end
    object actIncluirRepasseVincDesp: TAction
      Category = 'Despesa'
      Caption = '&Vincular '#224' Despesa'
      ImageIndex = 58
      OnExecute = actIncluirRepasseVincDespExecute
    end
    object actIncluirRepasseGerarNDesp: TAction
      Category = 'Despesa'
      Caption = '&Gerar Nova Despesa'
      ImageIndex = 59
      OnExecute = actIncluirRepasseGerarNDespExecute
    end
    object actDepositoMenu: TAction
      Category = 'Receita'
      Caption = 'Dep'#243'sito'
      ImageIndex = 56
      OnExecute = actDepositoMenuExecute
    end
    object actIncluirDeposito: TAction
      Category = 'Receita'
      Caption = 'Cadastro'
      ImageIndex = 56
      OnExecute = actIncluirDepositoExecute
    end
    object actIncluirReceitaVincDepo: TAction
      Category = 'Receita'
      Caption = 'Vincular Receitas e Dep'#243'sitos'
      ImageIndex = 58
      OnExecute = actIncluirReceitaVincDepoExecute
    end
  end
  object ppmReceita: TJvPopupMenu
    ImageMargin.Left = 0
    ImageMargin.Top = 0
    ImageMargin.Right = 0
    ImageMargin.Bottom = 0
    ImageSize.Height = 0
    ImageSize.Width = 0
    Left = 294
    Top = 22
    object LanarReceita1: TMenuItem
      Action = actIncluirReceita
    end
    object Depsito1: TMenuItem
      Action = actDepositoMenu
      object Cadastro1: TMenuItem
        Action = actIncluirDeposito
      end
      object VincularReceitaseDepsitos1: TMenuItem
        Action = actIncluirReceitaVincDepo
      end
    end
  end
  object ppmDespesa: TJvPopupMenu
    ImageMargin.Left = 0
    ImageMargin.Top = 0
    ImageMargin.Right = 0
    ImageMargin.Bottom = 0
    ImageSize.Height = 0
    ImageSize.Width = 0
    Left = 382
    Top = 22
    object LanarDespesa1: TMenuItem
      Action = actIncluirDespesa
    end
    object RepassedeReceita1: TMenuItem
      Action = actIncluirRepasseMenu
      object VincularDespesa1: TMenuItem
        Action = actIncluirRepasseVincDesp
      end
      object GerarNovaDespesa1: TMenuItem
        Action = actIncluirRepasseGerarNDesp
      end
    end
  end
end
