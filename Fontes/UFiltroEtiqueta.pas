{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroEtiqueta.pas
  Descricao:   Filtro de Etiquetas
  Author   :   Pedro
  Date:        19-jun-2017
  Last Update: 08-nov-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroEtiqueta;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls,System.DateUtils,sBitBtn, Vcl.Mask, sMaskEdit, sCustomComboEdit,
  sToolEdit, Vcl.StdCtrls, sGroupBox, sComboBox, sEdit, Datasnap.DBClient,
  Datasnap.Provider, JvComponentBase, JvDBGridExport, UCadastroEtiqueta,
  System.StrUtils;

type
  TFFiltroEtiqueta = class(TFFiltroSimplesPadrao)
    edpesquisa: TsEdit;
    rgStatus: TsRadioGroup;
    edInicio: TsDateEdit;
    edFim: TsDateEdit;
    dbgxExport: TJvDBGridExcelExport;
    cbFiltro: TsComboBox;
    qryGridPaiPadraoID: TIntegerField;
    qryGridPaiPadraoID_LOTEETIQUETA_FK: TIntegerField;
    qryGridPaiPadraoORDEM: TIntegerField;
    qryGridPaiPadraoLETRA: TStringField;
    qryGridPaiPadraoNUMERO: TIntegerField;
    qryGridPaiPadraoALEATORIO: TStringField;
    qryGridPaiPadraoNUM_ETIQUETA: TStringField;
    qryGridPaiPadraoDATA_UTILIZACAO: TDateField;
    qryGridPaiPadraoID_SISTEMA_FK: TIntegerField;
    qryGridPaiPadraoFLG_SITUACAO: TStringField;
    qryGridPaiPadraoID_FUNCIONARIO_FK: TIntegerField;
    qryGridPaiPadraoID_NATUREZAATO_FK: TIntegerField;
    qryGridPaiPadraoOBS_ETIQUETA: TStringField;
    qryGridPaiPadraoDATA_CADASTRO: TSQLTimeStampField;
    qryGridPaiPadraoCAD_ID_USUARIO: TIntegerField;
    qryGridPaiPadraoDATA_CANCELAMENTO: TDateField;
    qryGridPaiPadraoCANCEL_ID_USUARIO: TIntegerField;
    qryGridPaiPadraoDATA_COMPRA: TDateField;
    qryGridPaiPadraoNOME_SISTEMA: TStringField;
    qryGridPaiPadraoNOME_FUNCIONARIO: TStringField;
    qryGridPaiPadraoSELO: TStringField;
    qryGridPaiPadraoSITUACAO: TStringField;
    procedure btnIncluirClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
//    procedure btnExportarClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure dbgGridPaiPadraoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure cbFiltroChange(Sender: TObject);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure cbFiltroKeyPress(Sender: TObject; var Key: Char);
    procedure edInicioKeyPress(Sender: TObject; var Key: Char);
    procedure edFimKeyPress(Sender: TObject; var Key: Char);
    procedure edpesquisaKeyPress(Sender: TObject; var Key: Char);
    procedure rgStatusClick(Sender: TObject);
    procedure rgStatusExit(Sender: TObject);
    procedure qryGridPaiPadraoCalcFields(DataSet: TDataSet);
    procedure btnImprimirClick(Sender: TObject);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }

    procedure AbrirLista;
  public
    { Public declarations }
  end;

var
  FFiltroEtiqueta: TFFiltroEtiqueta;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMEtiqueta;

procedure TFFiltroEtiqueta.FormCreate(Sender: TObject);
begin
  edInicio.Date := StartOfTheMonth(Date);
  edFim.Date    := EndOfTheMonth(Date);

  cbFiltro.ItemIndex:=0;

  btnFiltrarClick(Sender);
end;

procedure TFFiltroEtiqueta.VerificarPermissoes;
begin
  inherited;

  btnEditar.Enabled   := vgPrm_EdEtq and (qryGridPaiPadrao.RecordCount > 0);
  btnExcluir.Enabled  := vgPrm_ExcEtq and (qryGridPaiPadrao.RecordCount > 0);
  btnImprimir.Enabled := vgPrm_ImpEtq and (qryGridPaiPadrao.RecordCount > 0);
end;

function TFFiltroEtiqueta.PodeExcluir(var Msg: String): Boolean;
begin
  Result := True;

  if qryGridPaiPadrao.FieldByName('FLG_SITUACAO').AsString = 'C' then
    msg := 'N�o � poss�vel cancelar a etiqueta, pois o mesmo j� foi cancelado.';

  Result := (qryGridPaiPadrao.FieldByName('FLG_SITUACAO').AsString <> 'C') and
            (qryGridPaiPadrao.RecordCount > 0);
end;

procedure TFFiltroEtiqueta.qryGridPaiPadraoCalcFields(DataSet: TDataSet);
begin
  inherited;

  case AnsiIndexStr(UpperCase(qryGridPaiPadrao.FieldByName('FLG_SITUACAO').AsString), ['U', 'N', 'C']) of
    0: qryGridPaiPadrao.FieldByName('SITUACAO').AsString := 'Utilizado';
    1: qryGridPaiPadrao.FieldByName('SITUACAO').AsString := 'N�o Utilizado';
    2: qryGridPaiPadrao.FieldByName('SITUACAO').AsString := 'Cancelado';
  end;

  qryGridPaiPadrao.FieldByName('SELO').AsString := qryGridPaiPadraoLETRA.AsString +
                                                   qryGridPaiPadraoNUMERO.AsString + ' ' +
                                                   qryGridPaiPadraoALEATORIO.AsString;
end;

procedure TFFiltroEtiqueta.rgStatusClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroEtiqueta.rgStatusExit(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroEtiqueta.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    X:  //EXCLUSAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo do cancelamento dessa Etiqueta:', '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(5, '', sObservacao,
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'ETIQUETA');
    end;
    P:  //IMPRESSAO
    begin
      BS.GravarUsuarioLog(5, '', 'Somente Gerais da Etiqueta.',
                          qryGridPaiPadrao.FieldByName('ID').AsInteger,
                          'ETIQUETA');
    end;
  end;
end;

procedure TFFiltroEtiqueta.AbrirLista;
begin
  qryGridPaiPadrao.Close;
  qryGridPaiPadrao.SQL.Clear;
  qryGridPaiPadrao.SQL.Text := 'SELECT E.ID_ETIQUETA AS ID, ' +
                               '       E.ID_LOTEETIQUETA_FK, ' +
                               '       E.ORDEM, ' +
                               '       E.LETRA, ' +
                               '       E.NUMERO, ' +
                               '       E.ALEATORIO, ' +
                               '       E.NUM_ETIQUETA, ' +
                               '       E.DATA_UTILIZACAO, ' +
                               '       E.ID_SISTEMA_FK, ' +
                               '       E.FLG_SITUACAO, ' +
                               '       E.ID_FUNCIONARIO_FK, ' +
                               '       E.ID_NATUREZAATO_FK, ' +
                               '       E.OBS_ETIQUETA, ' +
                               '       E.DATA_CADASTRO, ' +
                               '       E.CAD_ID_USUARIO, ' +
                               '       E.DATA_CANCELAMENTO, ' +
                               '       E.CANCEL_ID_USUARIO, ' +
                               '       L.DATA_COMPRA, ' +
                               '       S.NOME_SISTEMA, ' +
                               '       FUNC.NOME_FUNCIONARIO ' +
                               '  FROM ETIQUETA E ' +
                               ' INNER JOIN LOTEETIQUETA L ' +
                               '    ON E.ID_LOTEETIQUETA_FK = L.ID_LOTEETIQUETA ' +
                               '  LEFT JOIN FUNCIONARIO FUNC ' +
                               '    ON E.ID_FUNCIONARIO_FK = FUNC.ID_FUNCIONARIO ' +
                               '  LEFT JOIN NATUREZAATO N ' +
                               '    ON E.ID_NATUREZAATO_FK = N.ID_NATUREZAATO ' +
                               '  LEFT JOIN SISTEMA S ' +
                               '    ON E.ID_SISTEMA_FK = S.ID_SISTEMA ';

  case rgStatus.ItemIndex of
    0: qryGridPaiPadrao.SQL.Add(' WHERE E.FLG_SITUACAO = E.FLG_SITUACAO ');
    1: qryGridPaiPadrao.SQL.Add(' WHERE E.FLG_SITUACAO = ' + QuotedStr('U'));
    2: qryGridPaiPadrao.SQL.Add(' WHERE E.FLG_SITUACAO = ' + QuotedStr('N'));
    3: qryGridPaiPadrao.SQL.Add(' WHERE E.FLG_SITUACAO = ' + QuotedStr('C'));
  end;

  case cbFiltro.ItemIndex of
    0: //DATA COMPRA
    begin
      qryGridPaiPadrao.SQL.Add(' AND L.DATA_COMPRA BETWEEN :DT_COMP1 AND :DT_COMP2 ');
      qryGridPaiPadrao.Params.ParamByName('DT_COMP1').Value := edInicio.Date;
      qryGridPaiPadrao.Params.ParamByName('DT_COMP2').Value := edFim.Date;

      if Trim(edpesquisa.Text) <> '' then
      begin
        qryGridPaiPadrao.SQL.Add(' AND (UPPER(E.LETRA||CAST(E.NUMERO AS VARCHAR(5))||E.ALEATORIO) LIKE (' + '%' + UpperCase(edpesquisa.Text) + '%) ');
        qryGridPaiPadrao.SQL.Add('  OR UPPER(E.NUM_ETIQUETA) LIKE (' + '%' + UpperCase(edpesquisa.Text) + '%) ');
        qryGridPaiPadrao.SQL.Add('  OR UPPER(S.NOME_SISTEMA) LIKE (' + '%' + UpperCase(edpesquisa.Text) + '%) ');
        qryGridPaiPadrao.SQL.Add('  OR UPPER(N.DESCR_NATUREZAATO) LIKE (' + '%' + UpperCase(edpesquisa.Text) + '%) ');
        qryGridPaiPadrao.SQL.Add('  OR UPPER(FUNC.NOME_FUNCIONARIO) LIKE (' + '%' + UpperCase(edpesquisa.Text) + '%)) ');
      end;
    end;
    1: //DATA DE UTILIZADO
    begin
      qryGridPaiPadrao.SQL.Add(' AND E.DATA_UTILIZACAO BETWEEN :DT_UT1 AND :DT_UT2 ');
      qryGridPaiPadrao.Params.ParamByName('DT_UT1').Value := edInicio.Date;
      qryGridPaiPadrao.Params.ParamByName('DT_UT2').Value := edFim.Date;

      if Trim(edpesquisa.Text) <> '' then
      begin
        qryGridPaiPadrao.SQL.Add(' AND (UPPER(E.LETRA||CAST(E.NUMERO AS VARCHAR(5))||E.ALEATORIO) LIKE (' + '%' + UpperCase(edpesquisa.Text) + '%) ');
        qryGridPaiPadrao.SQL.Add('  OR UPPER(E.NUM_ETIQUETA) LIKE (' + '%' + UpperCase(edpesquisa.Text) + '%) ');
        qryGridPaiPadrao.SQL.Add('  OR UPPER(S.NOME_SISTEMA) LIKE (' + '%' + UpperCase(edpesquisa.Text) + '%) ');
        qryGridPaiPadrao.SQL.Add('  OR UPPER(N.DESCR_NATUREZAATO) LIKE (' + '%' + UpperCase(edpesquisa.Text) + '%) ');
        qryGridPaiPadrao.SQL.Add('  OR UPPER(FUNC.NOME_FUNCIONARIO) LIKE (' + '%' + UpperCase(edpesquisa.Text) + '%)) ');
      end;
    end;
    2://SELO
    begin
      if Trim(edpesquisa.Text) <> '' then
        qryGridPaiPadrao.SQL.Add(' AND UPPER(E.LETRA||CAST(E.NUMERO AS VARCHAR(5))||E.ALEATORIO) LIKE (' + '%' + UpperCase(edpesquisa.Text) + '%) ');
    end;
    3://NUMERA��O FOLHA
    begin
      if Trim(edpesquisa.Text) <> '' then
        qryGridPaiPadrao.SQL.Add(' AND UPPER(E.NUM_ETIQUETA) LIKE (' + '%' + UpperCase(edpesquisa.Text) + '%) ');
    end;
    4://SISTEMA
    begin
      if Trim(edpesquisa.Text) <> '' then
        qryGridPaiPadrao.SQL.Add(' AND UPPER(S.NOME_SISTEMA) LIKE (' + '%' + UpperCase(edpesquisa.Text) + '%) ');
    end;
    5://NATUREZA
    begin
      if Trim(edpesquisa.Text) <> '' then
        qryGridPaiPadrao.SQL.Add(' AND UPPER(N.DESCR_NATUREZAATO) LIKE (' + '%' + UpperCase(edpesquisa.Text) + '%) ');
    end;
    6://FUNCIONARIO
    begin
      if Trim(edpesquisa.Text) <> '' then
        qryGridPaiPadrao.SQL.Add(' AND UPPER(FUNC.NOME_FUNCIONARIO) LIKE (' + '%' + UpperCase(edpesquisa.Text) + '%) ');
    end;
  end;

  qryGridPaiPadrao.SQL.Add(' ORDER BY L.ID_LOTEETIQUETA, '+
                           '          E.ORDEM');

  qryGridPaiPadrao.Open;
end;

procedure TFFiltroEtiqueta.btnEditarClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  if qryGridPaiPadrao.FieldByName('FLG_SITUACAO').AsString = 'C' then
    vgOperacao := C
  else
    vgOperacao := E;

  vgIdConsulta := qryGridPaiPadrao.FieldByName('ID').AsInteger;

  Application.CreateForm(TdmEtiqueta, dmEtiqueta);
  dmGerencial.CriarForm(TFCadastroEtiqueta, FCadastroEtiqueta);
  FreeAndNil(dmEtiqueta);

  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroEtiqueta.btnExcluirClick(Sender: TObject);
var
  qryExclusao: TFDQuery;
begin
  inherited;

  if lPodeExcluir then
  begin
    qryExclusao := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      with qryExclusao, SQL do
      begin
        Close;
        Clear;
        Text := 'UPDATE ETIQUETA ' +
                '   SET FLG_SITUACAO = ' + QuotedStr('C') + ', ' +
                '       DATA_CANCELAMENTO = :DATA_CANCELAMENTO, ' +
                '       CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO ' +
                ' WHERE ID_ETIQUETA = :ID_ETIQUETA';
        Params.ParamByName('DATA_CANCELAMENTO').Value := Date;
        Params.ParamByName('CANCEL_ID_USUARIO').Value := vgUsu_Id;
        Params.ParamByName('ID_ETIQUETA').Value       := qryGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      GravarLog;
      Application.MessageBox('Etiqueta de Seguran�a Cancelada com sucesso!', 'Aviso', MB_OK)
    except
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Application.MessageBox('Erro no cancelamento da Etiqueta.', 'Erro', MB_OK + MB_ICONERROR)
    end;

    FreeAndNil(qryExclusao);

    btnFiltrar.Click;
  end;
end;

{procedure TFFiltroEtiqueta.btnExportarClick(Sender: TObject);
begin
  inherited;
  try
    dbgxExport.Grid        := dbgGridPaiPadrao;
    dbgxExport.FileName    := vgConf_DiretorioRelatorios +
                              IntToStr(YearOf(Now)) + '\' +
                              'EtiquetadeSeguranca' +
                              dmGerencial.PegarNumeroTexto(edInicio.Text) + '_a_' +
                              dmGerencial.PegarNumeroTexto(edFim.Text);
    dbgxExport.Orientation := woLandscape;
    dbgxExport.AutoFit     := True;
    dbgxExport.Silent      := False;
    dbgxExport.Visible     := True;
    dbgxExport.Close       := scNever;
    dbgxExport.ExportGrid;

    Application.MessageBox('Lista exportada com sucesso!',
                           'Sucesso',
                           MB_OK);
  except
    dmGerencial.Aviso('N�o foi poss�vel gerar o arquivo. Por favor, tente novamente!');
  end;
end; }

procedure TFFiltroEtiqueta.btnFiltrarClick(Sender: TObject);
begin
  AbrirLista;

  inherited;
end;

procedure TFFiltroEtiqueta.btnImprimirClick(Sender: TObject);
begin
  inherited;

  dmPrincipal.AbrirRelatorio(103);
end;

procedure TFFiltroEtiqueta.btnIncluirClick(Sender: TObject);
begin
  inherited;
  //
end;

procedure TFFiltroEtiqueta.btnLimparClick(Sender: TObject);
begin
  inherited;

  edInicio.Date := StartOfTheMonth(Date);
  edFim.Date := EndOfTheMonth(Date);

  edpesquisa.Text := '';
  rgStatus.ItemIndex    := 0;

  inherited;

  if edInicio.CanFocus then
    edInicio.SetFocus;
end;

procedure TFFiltroEtiqueta.cbFiltroChange(Sender: TObject);
begin
  inherited;
  if (cbFiltro.ItemIndex <> 0) and (cbFiltro.ItemIndex <> 1) then
  begin
    edInicio.Enabled := False;
    edFim.Enabled    := False;
    edInicio.Color   := clSilver;
    edFim.Color      := clSilver;
  end
  else
  begin
    edInicio.Enabled := True;
    edFim.Enabled    := True;
    edInicio.Color   := clWindow;
    edFim.Color      := clWindow;
  end;
end;

procedure TFFiltroEtiqueta.cbFiltroKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroEtiqueta.dbgGridPaiPadraoDblClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao     := C;
  vgIdConsulta   := qryGridPaiPadrao.FieldByName('ID').AsInteger;
  vgOrigemFiltro := True;

  Application.CreateForm(TdmEtiqueta, dmEtiqueta);
  dmGerencial.CriarForm(TFCadastroEtiqueta, FCadastroEtiqueta);
  FreeAndNil(dmEtiqueta);

  DefinirPosicaoGrid;
end;

procedure TFFiltroEtiqueta.dbgGridPaiPadraoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;

  dbgGridPaiPadrao := Sender as TDBGrid;

  if dbgGridPaiPadrao.DataSource.DataSet.FieldByName('FLG_SITUACAO').AsString = 'C' then
  begin
    dbgGridPaiPadrao.Canvas.Font.Color := clGray;
    dbgGridPaiPadrao.Canvas.FillRect(Rect);
    dbgGridPaiPadrao.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TFFiltroEtiqueta.edFimKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroEtiqueta.edInicioKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroEtiqueta.edpesquisaKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroEtiqueta.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #27 then Close;
  inherited;
end;

procedure TFFiltroEtiqueta.FormShow(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;

  btnIncluir.Visible := False;

  ShowScrollBar(dbgGridPaiPadrao.Handle, SB_HORZ, False);

  if edInicio.CanFocus then
    edInicio.SetFocus;
end;

end.
