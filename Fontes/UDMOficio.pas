{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UDMOficio.pas
  Descricao:   Data Module de Oficios Recebidos e Enviados
  Author   :   Cristina
  Date:        12-jan-2017
  Last Update: 06-fev-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UDMOficio;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TdmOficio = class(TDataModule)
    qryOficio_F: TFDQuery;
    qryOfcPes: TFDQuery;
    qryOfcFrmEnv: TFDQuery;
    qryOfcOrgao: TFDQuery;
    qryOficioImg_F: TFDQuery;
    dspOficio_F: TDataSetProvider;
    cdsOficio_F: TClientDataSet;
    dsOficio_F: TDataSource;
    dspOficioImg_F: TDataSetProvider;
    cdsOficioImg_F: TClientDataSet;
    dsOficioImg_F: TDataSource;
    dsOfcPes: TDataSource;
    dsOfcFrmEnv: TDataSource;
    dsOfcOrgao: TDataSource;
    cdsOficioImg_FID_OFICIO_IMG: TIntegerField;
    cdsOficioImg_FID_OFICIO_FK: TIntegerField;
    cdsOficioImg_FNOME_FIXO: TStringField;
    cdsOficioImg_FNOME_VARIAVEL: TStringField;
    cdsOficioImg_FEXTENSAO: TStringField;
    qryOfcAssoc: TFDQuery;
    dsOfcAssoc: TDataSource;
    cdsOficioImg_FNOVO: TBooleanField;
    cdsOficioImg_FORIGEM_ANTIGA: TStringField;
    cdsOficioImg_FNOME_V_ANTIGO: TStringField;
    cdsOficioImg_FCAMINHO_COMPLETO: TStringField;
    cdsOficio_FID_OFICIO: TIntegerField;
    cdsOficio_FCOD_OFICIO: TIntegerField;
    cdsOficio_FANO_OFICIO: TIntegerField;
    cdsOficio_FTIPO_OFICIO: TStringField;
    cdsOficio_FDATA_OFICIO: TDateField;
    cdsOficio_FHORA_OFICIO: TTimeField;
    cdsOficio_FID_FUNCIONARIO_FK: TIntegerField;
    cdsOficio_FCAD_ID_USUARIO: TIntegerField;
    cdsOficio_FDATA_CADASTRO: TSQLTimeStampField;
    cdsOficio_FFLG_STATUS: TStringField;
    cdsOficio_FSTA_ID_USUARIO: TIntegerField;
    cdsOficio_FDATA_STATUS: TSQLTimeStampField;
    cdsOficio_FASSUNTO_OFICIO: TStringField;
    cdsOficio_FID_OFICIO_PESSOA_FK: TIntegerField;
    cdsOficio_FNUM_PROCESSO: TStringField;
    cdsOficio_FID_OFICIO_ORGAO_FK: TIntegerField;
    cdsOficio_FID_OFICIO_FORMAENVIO_FK: TIntegerField;
    cdsOficio_FNUM_RASTREIO: TStringField;
    cdsOficio_FNUM_PROTOCOLO: TStringField;
    cdsOficio_FDATA_SITUACAO_AR: TDateField;
    cdsOficio_FFLG_SITUACAO_AR: TStringField;
    cdsOficio_FOBS_OFICIO: TStringField;
    cdsOficio_FID_OFICIO_FK: TIntegerField;
    cdsOficio_FSEQ_OFICIO: TIntegerField;
    qryFuncionarios: TFDQuery;
    dsFuncionarios: TDataSource;
    procedure cdsOficioImg_FCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmOficio: TdmOficio;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{$R *.dfm}

procedure TdmOficio.cdsOficioImg_FCalcFields(DataSet: TDataSet);
begin
  cdsOficioImg_F.FieldByName('CAMINHO_COMPLETO').AsString := vgConf_DiretorioImagens +
                                                             IntToStr(vgOfc_Ano) +
                                                             '\' +
                                                             cdsOficioImg_F.FieldByName('NOME_FIXO').AsString +
                                                             cdsOficioImg_F.FieldByName('NOME_VARIAVEL').AsString + '.' +
                                                             cdsOficioImg_F.FieldByName('EXTENSAO').AsString;
end;

end.
