inherited FCadastroUsuarioPermissao: TFCadastroUsuarioPermissao
  Caption = 'Cadastro de Permiss'#227'o do Usu'#225'rio'
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    inherited pnlDados: TPanel
      inherited pnlMsgErro: TPanel
        inherited lbMensagemErro: TListBox
          OnDblClick = lbMensagemErroDblClick
        end
      end
      object gbUsuario: TGroupBox
        Left = 0
        Top = 0
        Width = 674
        Height = 46
        Align = alTop
        Caption = 'Usu'#225'rio'
        TabOrder = 1
        object edtUsuario: TEdit
          Left = 5
          Top = 18
          Width = 302
          Height = 22
          Color = 16114127
          Enabled = False
          MaxLength = 25
          TabOrder = 0
          OnKeyPress = FormKeyPress
        end
        object chbSelecionarTodas: TCheckBox
          Left = 573
          Top = 23
          Width = 92
          Height = 17
          Hint = 'Clique para selecionar todas as op'#231#245'es'
          Caption = 'Marcar Todas'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = chbSelecionarTodasClick
        end
      end
      object ntbUsuarioPermissao: TNotebook
        Left = 0
        Top = 46
        Width = 674
        Height = 452
        Align = alClient
        PageIndex = 4
        TabOrder = 2
        object TPage
          Left = 0
          Top = 0
          Caption = 'Financeiro'
          object chlbFinanceiro: TCheckListBox
            Left = 0
            Top = 0
            Width = 674
            Height = 452
            Align = alClient
            Columns = 2
            ItemHeight = 14
            TabOrder = 0
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'Oficios'
          object chlbOficios: TCheckListBox
            Left = 0
            Top = 0
            Width = 674
            Height = 452
            Align = alClient
            ItemHeight = 14
            TabOrder = 0
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'Folhas'
          object chlbFolhas: TCheckListBox
            Left = 0
            Top = 0
            Width = 674
            Height = 452
            Align = alClient
            ItemHeight = 14
            TabOrder = 0
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'Etiquetas'
          object chlbEtiquetas: TCheckListBox
            Left = 0
            Top = 0
            Width = 674
            Height = 452
            Align = alClient
            ItemHeight = 14
            TabOrder = 0
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'Relatorios'
          object chlbRelatorios: TCheckListBox
            Left = 0
            Top = 0
            Width = 674
            Height = 452
            Align = alClient
            Columns = 1
            ItemHeight = 14
            TabOrder = 0
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'Configuracoes'
          object chlbConfiguracoes: TCheckListBox
            Left = 0
            Top = 0
            Width = 674
            Height = 452
            Align = alClient
            ItemHeight = 14
            TabOrder = 0
          end
        end
      end
    end
    inherited pnlMenu: TPanel
      inherited btnDadosPrincipais: TJvTransparentButton
        Caption = '&Financeiro'
        OnClick = btnDadosPrincipaisClick
      end
      object btnConfiguracoes: TJvTransparentButton
        Left = 0
        Top = 200
        Width = 102
        Height = 40
        Align = alTop
        BorderWidth = 0
        Caption = 'Confi&gura'#231#245'es'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 6316128
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        WordWrap = True
        OnClick = btnConfiguracoesClick
        NumGlyphs = 2
        ExplicitTop = 86
      end
      object btnRelatorios: TJvTransparentButton
        Left = 0
        Top = 160
        Width = 102
        Height = 40
        Align = alTop
        BorderWidth = 0
        Caption = '&Relat'#243'rios'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 6316128
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        WordWrap = True
        OnClick = btnRelatoriosClick
        NumGlyphs = 2
        ExplicitTop = 86
      end
      object btnOficios: TJvTransparentButton
        Left = 0
        Top = 40
        Width = 102
        Height = 40
        Align = alTop
        BorderWidth = 0
        Caption = 'Of'#237'c&ios'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 6316128
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        WordWrap = True
        OnClick = btnOficiosClick
        NumGlyphs = 2
        ExplicitTop = 86
      end
      object btnFolhas: TJvTransparentButton
        Left = 0
        Top = 80
        Width = 102
        Height = 40
        Align = alTop
        BorderWidth = 0
        Caption = 'Fol&lhas'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 6316128
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        WordWrap = True
        OnClick = btnFolhasClick
        NumGlyphs = 2
        ExplicitTop = 86
      end
      object btnEtiquetas: TJvTransparentButton
        Left = 0
        Top = 120
        Width = 102
        Height = 40
        Align = alTop
        BorderWidth = 0
        Caption = '&Etiquetas'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 6316128
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        WordWrap = True
        OnClick = btnEtiquetasClick
        NumGlyphs = 2
        ExplicitTop = 126
      end
    end
  end
  inherited dsCadastro: TDataSource
    Left = 38
    Top = 390
  end
end
