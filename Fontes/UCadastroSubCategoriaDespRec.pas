{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroSubCategoriaDespRec.pas
  Descricao:   Formulario de cadastro de Subcategorias de Despesas ou Receitas
  Author   :   Cristina
  Date:        04-mar-2016
  Last Update: 23-out-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroSubCategoriaDespRec;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Vcl.DBCtrls, Vcl.Mask, Datasnap.DBClient, Datasnap.Provider;

type
  TFCadastroSubCategoriaDespRec = class(TFCadastroGeralPadrao)
    lblNomeSubCategoria: TLabel;
    edtNomeSubCategoria: TDBEdit;
    rgTipo: TDBRadioGroup;
    qrySubCategoria: TFDQuery;
    dspSubCategoria: TDataSetProvider;
    cdsSubCategoria: TClientDataSet;
    lblNomeCategoria: TLabel;
    lcbNomeCategoria: TDBLookupComboBox;
    dsCategoria: TDataSource;
    qryCategoria: TFDQuery;
    cdsSubCategoriaID_SUBCATEGORIA_DESPREC: TIntegerField;
    cdsSubCategoriaID_CATEGORIA_DESPREC_FK: TIntegerField;
    cdsSubCategoriaTIPO: TStringField;
    cdsSubCategoriaDESCR_SUBCATEGORIA_DESPREC: TStringField;
    lblNatureza: TLabel;
    edtNatureza: TEdit;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtNomeSubCategoriaKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnOkClick(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure lcbNomeCategoriaClick(Sender: TObject);
    procedure lcbNomeCategoriaExit(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
    sTipo: String;
  public
    { Public declarations }

    IdNovaSubCategoria: Integer;
  end;

var
  FCadastroSubCategoriaDespRec: TFCadastroSubCategoriaDespRec;

  TipoSubCat: String;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{ TFCadastroCategoriaDespRec }

procedure TFCadastroSubCategoriaDespRec.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroSubCategoriaDespRec.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroSubCategoriaDespRec.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtNomeSubCategoria.MaxLength := 250;
end;

procedure TFCadastroSubCategoriaDespRec.DesabilitarComponentes;
begin
  inherited;

  btnDadosPrincipais.Visible := False;
end;

procedure TFCadastroSubCategoriaDespRec.edtNomeSubCategoriaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroSubCategoriaDespRec.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  if lPodeCancelar then
  begin
    vgOperacao     := VAZIO;
    vgIdConsulta   := 0;
    vgOrigemFiltro := False;
  end
  else
    Action := caNone;
end;

procedure TFCadastroSubCategoriaDespRec.FormCreate(Sender: TObject);
begin
  inherited;

  IdNovaSubCategoria := 0;
  sTipo := '';
end;

procedure TFCadastroSubCategoriaDespRec.FormShow(Sender: TObject);
begin
  inherited;

  qryCategoria.Close;
  qryCategoria.Params.ParamByName('TIPO').Value := TipoSubCat;
  qryCategoria.Open;

  cdsSubCategoria.Close;
  cdsSubCategoria.Params.ParamByName('ID_SUBCATEGORIA_DESPREC').Value := vgIdConsulta;
  cdsSubCategoria.Open;

  if vgOperacao = I then
    edtNatureza.Text := ''
  else
    edtNatureza.Text := qryCategoria.FieldByName('DESCR_NATUREZA').AsString;

  edtNatureza.ReadOnly := True;

  if vgOperacao = I then
    cdsSubCategoria.Append;

  if vgOperacao = E then
    cdsSubCategoria.Edit;

  if vgOperacao = C then
    pnlDados.Enabled := False;

  if TipoSubCat = 'D' then
  begin
    rgTipo.ItemIndex := 0;
    sTipo := 'Despesa';
  end
  else if TipoSubCat = 'R' then
  begin
    rgTipo.ItemIndex := 1;
    sTipo := 'Receita';
  end;

  if lcbNomeCategoria.CanFocus then
    lcbNomeCategoria.SetFocus;
end;

function TFCadastroSubCategoriaDespRec.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroSubCategoriaDespRec.GravarDadosGenerico(
  var Msg: String): Boolean;
begin
  Result := True;
  Msg := '';

  if vgOperacao = C then
    Exit;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    if cdsSubCategoria.State in [dsInsert, dsEdit] then
    begin
      if vgOperacao = I then
      begin
        cdsSubCategoria.FieldByName('ID_SUBCATEGORIA_DESPREC').AsInteger := BS.ProximoId('ID_SUBCATEGORIA_DESPREC', 'SUBCATEGORIA_DESPREC');

        if vgOrigemCadastro then
          IdNovaSubCategoria := cdsSubCategoria.FieldByName('ID_SUBCATEGORIA_DESPREC').AsInteger;

        if rgTipo.ItemIndex = 0 then
          cdsSubCategoria.FieldByName('TIPO').AsString := 'D'
        else if rgTipo.ItemIndex = 1 then
          cdsSubCategoria.FieldByName('TIPO').AsString := 'R';
      end;

      cdsSubCategoria.Post;
      cdsSubCategoria.ApplyUpdates(0);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    Msg := 'Subcategoria de ' + sTipo + ' gravada com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na grava��o da Subcategoria de ' + sTipo + '.';
  end;
end;

procedure TFCadastroSubCategoriaDespRec.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta de Subcategoria de ' + sTipo,
                          vgIdConsulta, 'SUBCATEGORIA_DESPREC');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclus�o de Subcategoria de ' + sTipo,
                          vgIdConsulta, 'SUBCATEGORIA_DESPREC');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO',
                                  'Por favor, indique o motivo da edi��o dessa Subcategoria de ' + sTipo + ':',
                                  '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de Subcategoria de ' + sTipo;

      BS.GravarUsuarioLog(2, '', sObservacao,
                          vgIdConsulta, 'SUBCATEGORIA_DESPREC');
    end;
  end;
end;

procedure TFCadastroSubCategoriaDespRec.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroSubCategoriaDespRec.lcbNomeCategoriaClick(Sender: TObject);
begin
  inherited;

  edtNatureza.Text := qryCategoria.FieldByName('DESCR_NATUREZA').AsString;
end;

procedure TFCadastroSubCategoriaDespRec.lcbNomeCategoriaExit(Sender: TObject);
begin
  inherited;

  edtNatureza.Text := qryCategoria.FieldByName('DESCR_NATUREZA').AsString;
end;

procedure TFCadastroSubCategoriaDespRec.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

function TFCadastroSubCategoriaDespRec.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if Trim(lcbNomeCategoria.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher o NOME DA CATEGORIA DA ' + UpperCase(sTipo) + '.';
    DadosMsgErro.Componente := lcbNomeCategoria;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(edtNomeSubCategoria.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher o NOME DA SUBCATEGORIA DA ' + UpperCase(sTipo) + '.';
    DadosMsgErro.Componente := edtNomeSubCategoria;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroSubCategoriaDespRec.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

end.
