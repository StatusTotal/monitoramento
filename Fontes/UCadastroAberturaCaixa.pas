{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroAberturaCaixa.pas
  Descricao:   Tela de cadastro de Abertura de Caixa
  Author   :   Cristina
  Date:        23-dez-2016
  Last Update: 23-out-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroAberturaCaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  JvBaseEdits, JvDBControls, JvMaskEdit, Vcl.Mask, JvExMask, JvToolEdit,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.DBClient, Datasnap.Provider,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.DBCtrls, System.DateUtils,
  JvExExtCtrls, JvExtComponent, JvPanel, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, dxSkinMetropolis,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, cxTextEdit,
  cxMaskEdit, cxSpinEdit, cxTimeEdit, cxDBEdit, System.StrUtils;

type
  TFCadastroAberturaCaixa = class(TFCadastroGeralPadrao)
    lblNomeUsuarioResponsavel: TLabel;
    lblDataAbertura: TLabel;
    lblHoraAbertura: TLabel;
    lblVlrTotalUltFechamento: TLabel;
    lblVlrTotalAbertura: TLabel;
    dteDataAbertura: TJvDBDateEdit;
    cedVlrTotalAbertura: TJvDBCalcEdit;
    cedVlrTotalUltFechamento: TJvDBCalcEdit;
    qryAberturaCX: TFDQuery;
    dspAberturaCX: TDataSetProvider;
    cdsAberturaCX: TClientDataSet;
    cdsAberturaCXID_CAIXA_ABERTURA: TIntegerField;
    cdsAberturaCXDATA_ABERTURA: TDateField;
    cdsAberturaCXHORA_ABERTURA: TTimeField;
    cdsAberturaCXVR_NOTAS: TBCDField;
    cdsAberturaCXVR_MOEDAS: TBCDField;
    cdsAberturaCXVR_TOTAL_ABERTURA: TBCDField;
    cdsAberturaCXVR_TOTAL_ULT_FECH: TBCDField;
    cdsAberturaCXNOME_USUARIO: TStringField;
    edtNomeUsuarioResponsavel: TDBEdit;
    lblVlrContaCorrente: TLabel;
    cedVlrContaCorrente: TJvDBCalcEdit;
    lblDataCancelamento: TLabel;
    dteDataCancelamento: TJvDBDateEdit;
    cdsAberturaCXCAD_ID_USUARIO: TIntegerField;
    cdsAberturaCXFLG_CANCELADO: TStringField;
    cdsAberturaCXDATA_CANCELAMENTO: TDateField;
    cdsAberturaCXCANCEL_ID_USUARIO: TIntegerField;
    mmCupomAbertura: TMemo;
    pnlAberturaCancelada: TJvPanel;
    gbDinheiro: TGroupBox;
    lblVlrTotalNotas: TLabel;
    lblVlrTotalMoedas: TLabel;
    cedVlrTotalMoedas: TJvDBCalcEdit;
    cedVlrTotalNotas: TJvDBCalcEdit;
    cdsAberturaCXVR_CONTACORRENTE: TBCDField;
    mmObservacoes: TDBMemo;
    lblObservacoes: TLabel;
    cdsAberturaCXOBS_CAIXA_ABERTURA: TStringField;
    lblEmAtraso: TLabel;
    medHoraAbertura: TJvDBMaskEdit;
    cdsAberturaCXFLG_ATRASO_NOTIFICADO: TStringField;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cdsAberturaCXCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure cdsAberturaCXVR_NOTASGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsAberturaCXVR_MOEDASGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsAberturaCXVR_CONTA_CORRENTEGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsAberturaCXVR_TOTAL_ABERTURAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsAberturaCXVR_TOTAL_ULT_FECHGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cedVlrTotalMoedasExit(Sender: TObject);
    procedure cedVlrTotalNotasExit(Sender: TObject);
    procedure cedVlrContaCorrenteExit(Sender: TObject);
    procedure cedVlrTotalAberturaKeyPress(Sender: TObject; var Key: Char);
    procedure mmObservacoesKeyPress(Sender: TObject; var Key: Char);
    procedure dteDataAberturaExit(Sender: TObject);
    procedure dteDataAberturaKeyPress(Sender: TObject; var Key: Char);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }

    procedure CalcularTotalAbertura;
    procedure MontarMemo;
  public
    { Public declarations }
  end;

var
  FCadastroAberturaCaixa: TFCadastroAberturaCaixa;

  dDataAbertura: TDatetime;

  function DataAberturaInformada: Boolean;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMCaixa, USolicitaData;

{ TFCadastroAberturaCaixa }

function DataAberturaInformada: Boolean;
var
  QryFer: TFDQuery;
  dDataTeste: TDatetime;
  sData, sNovaData, sMensagem: String;
  lCancelado: Boolean;
begin
  Result := True;

  dDataAbertura := vgDataProxAb;
  sData         := FormatDateTime('DD/MM/YYYY', dDataAbertura);
  sNovaData     := FormatDateTime('DD/MM/YYYY', dDataAbertura);
  sMensagem     := '';
  lCancelado    := False;

  if vgOperacao = I then
  begin
    QryFer := dmGerencial.CriarFDQuery(nil, vgConGERAux);

    QryFer.Close;
    QryFer.SQL.Clear;

    QryFer.SQL.Text := 'SELECT ID_FERIADO ' +
                       '  FROM FERIADO ' +
                       ' WHERE DIA_FERIADO = :DIA_FERIADO ' +
                       '   AND MES_FERIADO = :MES_FERIADO ' +
                       '   AND ANO_FERIADO = :ANO_FERIADO';

    QryFer.Params.ParamByName('DIA_FERIADO').Value := DayOf(dDataAbertura);
    QryFer.Params.ParamByName('MES_FERIADO').Value := MonthOf(dDataAbertura);
    QryFer.Params.ParamByName('ANO_FERIADO').Value := YearOf(dDataAbertura);

    QryFer.Open;

    if QryFer.RecordCount > 0 then
      sMensagem := 'O dia ' + sNovaData + ' � um Feriado. Caso deseje realizar a Abertura do Caixa em uma data posterior a essa, por favor informe a nova Data de Abertura:'
    else if DayOfWeek(dDataAbertura) = 7 then
      sMensagem := 'O dia ' + sNovaData + ' � um S�bado. Caso deseje realizar a Abertura do Caixa em uma data posterior a essa, por favor informe a nova Data de Abertura:'
    else if DayOfWeek(dDataAbertura) = 1 then
      sMensagem := 'O dia ' + sNovaData + ' � um Domingo. Caso deseje realizar a Abertura do Caixa em uma data posterior a essa, por favor informe a nova Data de Abertura:';

    if Trim(sMensagem) <> '' then
    begin
    //Solicita a Data de Realizacao dos Pagamentos
      repeat
        try
          Application.CreateForm(TFSolicitaData, FSolicitaData);

          FSolicitaData.sCaptionLabel       := sMensagem;
          FSolicitaData.lblTitulo.Font.Size := 10;
          FSolicitaData.lVerifCaixaVigente  := True;

          FSolicitaData.ShowModal;
        finally
          if FSolicitaData.lCancelado then
            sData := FormatDateTime('DD/MM/YYYY', dDataAbertura)
          else
          begin
            try
              dDataTeste := FSolicitaData.dDataSolicitada;
              sData := FormatDateTime('DD/MM/YYYY', dDataTeste);
            except
              sData := FormatDateTime('DD/MM/YYYY', dDataAbertura);
            end;
          end;

          FSolicitaData.Free;
        end;
      until (Trim(sData) <> '');
    end;

    if Trim(sData) <> '' then
    begin
      sData := ReplaceStr(sData, '/', '');
      sData := Trim(ReplaceStr(sData, '-', ''));
      sNovaData := Copy(sData, 1, 2) + '/' +
                   Copy(sData, 3, 2) + '/' +
                   Copy(sData, 5, 4)
    end;

     dDataAbertura := StrToDate(sNovaData);

    FreeAndNil(QryFer);
  end;
end;

procedure TFCadastroAberturaCaixa.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroAberturaCaixa.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroAberturaCaixa.CalcularTotalAbertura;
var
  cVlrNotas: Currency;
begin
  cVlrNotas := Trunc(cedVlrTotalNotas.Value);

  cedVlrTotalAbertura.Value := (cVlrNotas +
                                cedVlrTotalMoedas.Value +
                                cedVlrContaCorrente.Value);

  MontarMemo;
end;

procedure TFCadastroAberturaCaixa.cdsAberturaCXCalcFields(DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  qryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with qryAux, SQL do
  begin
    //Item
    if cdsAberturaCX.FieldByName('CAD_ID_USUARIO').AsInteger > 0 then
    begin
      Close;
      Clear;
      Text := 'SELECT NOME ' +
              '  FROM USUARIO ' +
              ' WHERE ID_USUARIO = :ID_USUARIO';
      Params.ParamByName('ID_USUARIO').AsInteger := cdsAberturaCX.FieldByName('CAD_ID_USUARIO').AsInteger;
      Open;

      cdsAberturaCX.FieldByName('NOME_USUARIO').AsString := qryAux.FieldByName('NOME').AsString;
    end;
  end;

  FreeAndNil(qryAux);
end;

procedure TFCadastroAberturaCaixa.cdsAberturaCXVR_CONTA_CORRENTEGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFCadastroAberturaCaixa.cdsAberturaCXVR_MOEDASGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFCadastroAberturaCaixa.cdsAberturaCXVR_NOTASGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFCadastroAberturaCaixa.cdsAberturaCXVR_TOTAL_ABERTURAGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFCadastroAberturaCaixa.cdsAberturaCXVR_TOTAL_ULT_FECHGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFCadastroAberturaCaixa.cedVlrContaCorrenteExit(Sender: TObject);
begin
  inherited;

  CalcularTotalAbertura;
end;

procedure TFCadastroAberturaCaixa.cedVlrTotalAberturaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    CalcularTotalAbertura;
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    CalcularTotalAbertura;
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    btnCancelar.Click;
end;

procedure TFCadastroAberturaCaixa.cedVlrTotalMoedasExit(Sender: TObject);
begin
  inherited;

  CalcularTotalAbertura;
end;

procedure TFCadastroAberturaCaixa.cedVlrTotalNotasExit(Sender: TObject);
begin
  inherited;

  cedVlrTotalNotas.Value := Trunc(cedVlrTotalNotas.Value);

  CalcularTotalAbertura;
end;

procedure TFCadastroAberturaCaixa.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtNomeUsuarioResponsavel.MaxLength := 30;
end;

procedure TFCadastroAberturaCaixa.DesabilitarComponentes;
begin
  inherited;

  edtNomeUsuarioResponsavel.ReadOnly := True;
  dteDataAbertura.ReadOnly           := (vgOperacao in [C, E]);
  medHoraAbertura.ReadOnly           := (vgOperacao in [C, E]);
  cedVlrTotalUltFechamento.ReadOnly  := True;
  cedVlrTotalAbertura.ReadOnly       := True;
  dteDataCancelamento.ReadOnly       := True;

  mmCupomAbertura.ReadOnly := True;

  lblDataCancelamento.Visible := (vgOperacao <> I) and
                                 (cdsAberturaCX.FieldByName('FLG_CANCELADO').AsString = 'S');
  dteDataCancelamento.Visible := (vgOperacao <> I) and
                                 (cdsAberturaCX.FieldByName('FLG_CANCELADO').AsString = 'S');

  if vgOperacao = C then
  begin
    edtNomeUsuarioResponsavel.Enabled := False;
    dteDataAbertura.Enabled           := False;
    medHoraAbertura.Enabled           := False;
    cedVlrTotalAbertura.Enabled       := False;
    cedVlrTotalUltFechamento.Enabled  := False;
    dteDataCancelamento.Enabled       := False;

    cedVlrTotalNotas.Enabled    := False;
    cedVlrTotalMoedas.Enabled   := False;
    cedVlrContaCorrente.Enabled := False;
    mmObservacoes.Enabled       := False;
  end;
end;

procedure TFCadastroAberturaCaixa.dteDataAberturaExit(Sender: TObject);
begin
  inherited;

  if (vgOperacao = I) and
    (cdsAberturaCX.State in [dsInsert, dsEdit]) then
  begin
    if Trim(vgSituacaoCaixa) <> 'N' then
    begin
      if (dteDataAbertura.Date < dDataAbertura) or
        (dteDataAbertura.Date > Date) then
        dteDataAbertura.Date := dDataAbertura;
    end;
  end;
end;

procedure TFCadastroAberturaCaixa.dteDataAberturaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if (vgOperacao = I) and
      (dmCaixa.cdsFechCX_F.State in [dsInsert, dsEdit]) then
    begin
      if Trim(vgSituacaoCaixa) <> 'N' then
      begin
        if (dteDataAbertura.Date < dDataAbertura) or
          (dteDataAbertura.Date > Date) then
          dteDataAbertura.Date := dDataAbertura;
      end;
    end;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroAberturaCaixa.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  if lPodeCancelar then
  begin
    cdsAberturaCX.Cancel;
    cdsAberturaCX.Close;

    vgOperacao     := VAZIO;
    vgIdConsulta   := 0;
    vgOrigemFiltro := False;
  end
  else
    Action := caNone;
end;

procedure TFCadastroAberturaCaixa.FormShow(Sender: TObject);
begin
  inherited;

  cdsAberturaCX.Close;
  cdsAberturaCX.Params.ParamByName('ID_CAIXA_ABERTURA').Value := vgIdConsulta;
  cdsAberturaCX.Open;

  if vgOperacao = I then
  begin
    cdsAberturaCX.Append;
    cdsAberturaCX.FieldByName('CAD_ID_USUARIO').AsInteger     := vgUsu_Id;
    cdsAberturaCX.FieldByName('VR_TOTAL_ABERTURA').AsCurrency := 0;
    cdsAberturaCX.FieldByName('FLG_CANCELADO').AsString       := 'N';
    cdsAberturaCX.FieldByName('DATA_ABERTURA').AsDateTime     := dDataAbertura;
    cdsAberturaCX.FieldByName('VR_TOTAL_ULT_FECH').AsCurrency := vgVlrTotalFech;
    cdsAberturaCX.FieldByName('VR_NOTAS').AsCurrency          := vgVlrNotasFech;
    cdsAberturaCX.FieldByName('VR_MOEDAS').AsCurrency         := vgVlrMoedasFech;
    cdsAberturaCX.FieldByName('VR_CONTACORRENTE').AsCurrency  := vgVlrContaFech;

    if dDataAbertura < Date then
    begin
      lblEmAtraso.Visible := True;

      cdsAberturaCX.FieldByName('HORA_ABERTURA').AsString := '08:00:00';

      if Trim(vgSituacaoCaixa) = 'N' then
        lblEmAtraso.Caption := '1� Abertura de Caixa'
      else
        lblEmAtraso.Caption := '�ltimo Fechamento em:' + #13#10 + FormatDateTime('DD/MM/YYYY', vgDataUltFech);
    end
    else
    begin
      lblEmAtraso.Visible := False;

      cdsAberturaCX.FieldByName('HORA_ABERTURA').AsString := TimeToStr(Now);
    end;
  end;

  if vgOperacao = E then
  begin
    lblEmAtraso.Visible := False;
    cdsAberturaCX.Edit;
  end;

  if vgOperacao = C then
    lblEmAtraso.Visible := False;

  pnlAberturaCancelada.Visible := (vgOperacao <> I) and
                                  (cdsAberturaCX.FieldByName('FLG_CANCELADO').AsString = 'S');

  if cdsAberturaCX.FieldByName('FLG_CANCELADO').AsString = 'S' then
    mmCupomAbertura.Font.Color := cl3DDkShadow
  else
    mmCupomAbertura.Font.Color := clWindowText;

  MontarMemo;

  if Trim(vgSituacaoCaixa) = 'N' then
    lblVlrTotalUltFechamento.Caption := 'Vlr. Cx. Inicial'
  else
    lblVlrTotalUltFechamento.Caption := 'Tot. �lt. Fech.';
    
  if cedVlrTotalNotas.CanFocus then
    cedVlrTotalNotas.SetFocus;
end;

function TFCadastroAberturaCaixa.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroAberturaCaixa.GravarDadosGenerico(var Msg: String): Boolean;
begin
  Result := True;
  Msg := '';

  if vgOperacao = C then
    Exit;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    if cdsAberturaCX.State in [dsInsert, dsEdit] then
    begin
      if vgOperacao = I then
      begin
        cdsAberturaCX.FieldByName('ID_CAIXA_ABERTURA').AsInteger := BS.ProximoId('ID_CAIXA_ABERTURA', 'CAIXA_ABERTURA');

        if Trim(cdsAberturaCX.FieldByName('HORA_ABERTURA').AsString) = '' then
          cdsAberturaCX.FieldByName('HORA_ABERTURA').AsString    := TimeToStr(Now);
      end;

      cdsAberturaCX.Post;
      cdsAberturaCX.ApplyUpdates(0);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    Msg := 'Abertura de Caixa gravada com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na grava��o da Abertura de Caixa.';
  end;
end;

procedure TFCadastroAberturaCaixa.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta de Dados de Abertura de Caixa',
                          vgIdConsulta, 'CAIXA_ABERTURA');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclus�o de Dados de Abertura de Caixa',
                          vgIdConsulta, 'CAIXA_ABERTURA');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da edi��o dessa Abertura de Caixa:', '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de Abertura de Caixa';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          vgIdConsulta, 'CAIXA_ABERTURA');
    end;
  end;
end;

procedure TFCadastroAberturaCaixa.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroAberturaCaixa.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

procedure TFCadastroAberturaCaixa.mmObservacoesKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    btnCancelar.Click;
end;

procedure TFCadastroAberturaCaixa.MontarMemo;
begin
  mmCupomAbertura.Lines.Clear;

  mmCupomAbertura.Lines.Add('================================================');
  mmCupomAbertura.Lines.Add('               ABERTURA DE CAIXA                ');
  mmCupomAbertura.Lines.Add('================================================');
  mmCupomAbertura.Lines.Add('Data: ' + dmGerencial.CompletaString(DateToStr(dteDataAbertura.Date),
                                                                  '0',
                                                                  'E',
                                                                  10) +
                            '                  Hora: ' + dmGerencial.CompletaString(medHoraAbertura.Text,
                                                                                   '0',
                                                                                   'E',
                                                                                   8));
  mmCupomAbertura.Lines.Add('Usu. Respons�vel: ' + dmGerencial.CompletaString(edtNomeUsuarioResponsavel.Text,
                                                                              ' ',
                                                                              'E',
                                                                              30));
  mmCupomAbertura.Lines.Add('------------------------------------------------');
  mmCupomAbertura.Lines.Add('Descri��o                             Valor (R$)');
  mmCupomAbertura.Lines.Add('------------------------------------------------');
  mmCupomAbertura.Lines.Add('Notas:                              ' + dmGerencial.CompletaString(FormatCurr(',0.00', cedVlrTotalNotas.Value),
                                                                                                ' ',
                                                                                                'E',
                                                                                                12));
  mmCupomAbertura.Lines.Add('Moedas:                             ' + dmGerencial.CompletaString(FormatCurr(',0.00', cedVlrTotalMoedas.Value),
                                                                                                ' ',
                                                                                                'E',
                                                                                                12));
  mmCupomAbertura.Lines.Add('Conta Corrente:                     ' + dmGerencial.CompletaString(FormatCurr(',0.00', cedVlrContaCorrente.Value),
                                                                                                ' ',
                                                                                                'E',
                                                                                                12));
  mmCupomAbertura.Lines.Add('                                                ');
  mmCupomAbertura.Lines.Add('TOTAIS                                          ');
  mmCupomAbertura.Lines.Add('Abertura                            ' + dmGerencial.CompletaString(FormatCurr(',0.00', cedVlrTotalAbertura.Value),
                                                                                                ' ',
                                                                                                'E',
                                                                                                12));
  mmCupomAbertura.Lines.Add('                                                ');
  mmCupomAbertura.Lines.Add('�ltimo Fechamento                   ' + dmGerencial.CompletaString(FormatCurr(',0.00', cedVlrTotalUltFechamento.Value),
                                                                                                ' ',
                                                                                                'E',
                                                                                                12));
  mmCupomAbertura.Lines.Add('================================================');
end;

function TFCadastroAberturaCaixa.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if dteDataAbertura.Date = 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente a DATA DA ABERTURA.';
    DadosMsgErro.Componente := dteDataAbertura;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if (Trim(medHoraAbertura.Text) = '') or
    (Trim(medHoraAbertura.Text) = ':  :') then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente a HORA DA ABERTURA.';
    DadosMsgErro.Componente := medHoraAbertura;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if (dteDataAbertura.Date > 0) and
    (Trim(medHoraAbertura.Text) <> '') and
    (Trim(medHoraAbertura.Text) <> ':  :') then
  begin
    if (VarToDateTime(FormatDateTime('DD-MM-YYYY', vgDataUltFech) +
                      ' ' +
                      TimeToStr(vgHoraUltFech)) >= VarToDateTime(FormatDateTime('DD-MM-YYYY', dteDataAbertura.Date) +
                                                                 ' ' +
                                                                 Trim(medHoraAbertura.Text))) then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') A DATA E HORA DA ABERTURA devem ser maiores que a Data e Hora do �ltimo Fechamento (' + FormatDateTime('YYYY-MM-DD', vgDataUltFech) +
                                                                                                                                                                    ' ' +
                                                                                                                                                                    TimeToStr(vgHoraUltFech) + ').';
      DadosMsgErro.Componente := medHoraAbertura;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;
  end;

  if cedVlrTotalNotas.Value < 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o VALOR TOTAL DAS NOTAS.';
    DadosMsgErro.Componente := cedVlrTotalNotas;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if cedVlrTotalMoedas.Value < 0 then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o VALOR TOTAL DAS MOEDAS.';
    DadosMsgErro.Componente := cedVlrTotalMoedas;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroAberturaCaixa.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

end.
