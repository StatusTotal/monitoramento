object dmVale: TdmVale
  OldCreateOrder = False
  Height = 224
  Width = 233
  object qryVale: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM VALE'
      ' WHERE ID_VALE = :ID_VALE')
    Left = 30
    Top = 14
    ParamData = <
      item
        Position = 1
        Name = 'ID_VALE'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspVale: TDataSetProvider
    DataSet = qryVale
    Left = 30
    Top = 62
  end
  object cdsVale: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_VALE'
        ParamType = ptInput
      end>
    ProviderName = 'dspVale'
    Left = 31
    Top = 110
    object cdsValeID_VALE: TIntegerField
      FieldName = 'ID_VALE'
      Required = True
    end
    object cdsValeID_FUNCIONARIO_FK: TIntegerField
      FieldName = 'ID_FUNCIONARIO_FK'
    end
    object cdsValeDATA_VALE: TDateField
      FieldName = 'DATA_VALE'
    end
    object cdsValeVR_VALE: TBCDField
      FieldName = 'VR_VALE'
      OnGetText = cdsValeVR_VALEGetText
      Precision = 18
      Size = 2
    end
    object cdsValeDESCR_VALE: TStringField
      FieldName = 'DESCR_VALE'
      Size = 1000
    end
    object cdsValeDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsValeCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsValeDATA_QUITACAO: TDateField
      FieldName = 'DATA_QUITACAO'
    end
    object cdsValeQUIT_ID_USUARIO: TIntegerField
      FieldName = 'QUIT_ID_USUARIO'
    end
    object cdsValeFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsValeDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsValeCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsValeID_FECHAMENTO_SALARIO_FK: TIntegerField
      FieldName = 'ID_FECHAMENTO_SALARIO_FK'
    end
    object cdsValeVR_VALE_ANTERIOR: TCurrencyField
      FieldKind = fkInternalCalc
      FieldName = 'VR_VALE_ANTERIOR'
      OnGetText = cdsValeVR_VALE_ANTERIORGetText
    end
  end
  object dsVale: TDataSource
    DataSet = cdsVale
    Left = 31
    Top = 158
  end
  object qryFuncionarios: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT ID_FUNCIONARIO, NOME_FUNCIONARIO, CPF'
      '  FROM FUNCIONARIO'
      ' WHERE NOME_FUNCIONARIO <> '#39'FUNCION'#193'RIO'#39
      'ORDER BY NOME_FUNCIONARIO')
    Left = 156
    Top = 19
  end
  object dsFuncionarios: TDataSource
    DataSet = qryFuncionarios
    Left = 156
    Top = 67
  end
end
