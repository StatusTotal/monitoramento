{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroRelatorioFinDespRec.pas
  Descricao:   Filtro Relatorio Financeiro de Despesas x Receitas
  Author   :   Cristina
  Date:        12-ago-2016
  Last Update: 17-ago-2016
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroRelatorioFinDespRec;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroRelatorioPadrao,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, frxClass, frxDBSet, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.StdCtrls, Vcl.CheckLst,
  JvExControls, JvButton, JvTransparentButton, Vcl.ExtCtrls;

type
  TFFiltroRelatorioFinDespRec = class(TFFiltroRelatorioPadrao)
    fdsFirmas: TfrxDBDataset;
    qryFirmas: TFDQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroRelatorioFinDespRec: TFFiltroRelatorioFinDespRec;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

end.
