inherited FFiltroComissoes: TFFiltroComissoes
  Caption = 'Filtro de Comiss'#245'es'
  ClientHeight = 394
  ClientWidth = 905
  OnCreate = FormCreate
  ExplicitWidth = 911
  ExplicitHeight = 423
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 899
    Height = 388
    ExplicitWidth = 899
    ExplicitHeight = 388
    inherited pnlGrid: TPanel
      Top = 97
      Width = 815
      ExplicitTop = 97
      ExplicitWidth = 815
      inherited pnlGridPai: TPanel
        Width = 815
        ExplicitWidth = 815
        inherited dbgGridPaiPadrao: TDBGrid
          Width = 815
          OnTitleClick = dbgGridPaiPadraoTitleClick
          Columns = <
            item
              Expanded = False
              FieldName = 'ID'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'ID_FUNCIONARIO_FK'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'ID_SISTEMA_FK'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'COD_ADICIONAL'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'ID_ORIGEM'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'TIPO_ORIGEM'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'ID_FECHAMENTO_SALARIO_FK'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'DATA_CADASTRO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'CAD_ID_USUARIO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'PAG_ID_USUARIO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'FLG_CANCELADO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'CANCEL_ID_USUARIO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'DATA_COMISSAO'
              Title.Caption = 'Dt. Comiss'#227'o'
              Width = 85
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_ITEM_FK'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'NOMEFUNCIONARIO'
              Title.Caption = 'Nome Colaborador'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DESCR_ITEM'
              Title.Caption = 'Item'
              Width = 180
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'PERCENT_COMISSAO'
              Title.Alignment = taCenter
              Title.Caption = '%'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VR_COMISSAO'
              Title.Alignment = taRightJustify
              Title.Caption = 'Vlr. Comiss'#227'o'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATA_PAGAMENTO'
              Title.Caption = 'Dt. Pagto.'
              Width = 85
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATA_CANCELAMENTO'
              Title.Caption = 'Dt. Cancel.'
              Width = 85
              Visible = True
            end>
        end
      end
    end
    inherited pnlFiltro: TPanel
      Width = 899
      Height = 94
      ExplicitWidth = 899
      ExplicitHeight = 94
      inherited btnFiltrar: TJvTransparentButton
        Left = 813
        Top = 64
        ExplicitLeft = 813
        ExplicitTop = 64
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 813
        Top = 34
        ExplicitLeft = 813
        ExplicitTop = 34
      end
      object lblNomeFuncionario: TLabel
        Left = 81
        Top = 52
        Width = 100
        Height = 14
        Caption = 'Nome Colaborador'
      end
      object lblPeriodoCadastro: TLabel
        Left = 81
        Top = 8
        Width = 110
        Height = 14
        Caption = 'Per'#237'odo de Cadastro'
      end
      object Label3: TLabel
        Left = 187
        Top = 27
        Width = 6
        Height = 14
        Caption = 'a'
      end
      object lcbNomeFuncionario: TDBLookupComboBox
        Left = 81
        Top = 68
        Width = 401
        Height = 22
        Color = 16114127
        KeyField = 'ID_FUNCIONARIO'
        ListField = 'NOME_FUNCIONARIO'
        ListSource = dsFuncionarios
        NullValueKey = 16460
        TabOrder = 2
        OnKeyPress = lcbNomeFuncionarioKeyPress
      end
      object dteIniPeriodoCadastro: TJvDateEdit
        Left = 81
        Top = 24
        Width = 100
        Height = 22
        Color = 16114127
        ShowNullDate = False
        TabOrder = 0
        OnKeyPress = dteIniPeriodoCadastroKeyPress
      end
      object dteFimPeriodoCadastro: TJvDateEdit
        Left = 199
        Top = 24
        Width = 100
        Height = 22
        Color = 16114127
        ShowNullDate = False
        TabOrder = 1
        OnKeyPress = dteFimPeriodoCadastroKeyPress
      end
      object rgSituacaoVale: TRadioGroup
        Left = 488
        Top = 49
        Width = 281
        Height = 41
        Caption = 'Situa'#231#227'o'
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Todas'
          'N'#227'o quitado'
          'Quitado')
        TabOrder = 3
        OnExit = rgSituacaoValeExit
      end
    end
    inherited pnlMenu: TPanel
      Top = 97
      ExplicitTop = 97
    end
  end
  inherited dsGridPaiPadrao: TDataSource
    DataSet = cdsGridPaiPadrao
    Left = 820
    Top = 299
  end
  inherited qryGridPaiPadrao: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT ID_COMISSAO AS ID,'
      '       ID_FUNCIONARIO_FK,'
      '       ID_SISTEMA_FK,'
      '       COD_ADICIONAL,'
      '       ID_ORIGEM,'
      '       TIPO_ORIGEM,'
      '       PERCENT_COMISSAO,'
      '       VR_COMISSAO,'
      '       ID_FECHAMENTO_SALARIO_FK,'
      '       DATA_CADASTRO,'
      '       CAD_ID_USUARIO,'
      '       DATA_PAGAMENTO,'
      '       PAG_ID_USUARIO,'
      '       FLG_CANCELADO,'
      '       DATA_CANCELAMENTO,'
      '       CANCEL_ID_USUARIO,'
      '       DATA_COMISSAO,'
      '       ID_ITEM_FK'
      '  FROM COMISSAO'
      
        ' WHERE ID_FUNCIONARIO_FK = (CASE WHEN (:ID_FUNC01 = 0) THEN ID_F' +
        'UNCIONARIO_FK ELSE :ID_FUNC02 END)'
      '   AND DATA_COMISSAO BETWEEN :DATA_INI AND :DATA_FIM'
      'ORDER BY ID_FUNCIONARIO_FK ASC, DATA_COMISSAO DESC')
    Left = 820
    Top = 151
    ParamData = <
      item
        Position = 1
        Name = 'ID_FUNC01'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'ID_FUNC02'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 3
        Name = 'DATA_INI'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Position = 4
        Name = 'DATA_FIM'
        DataType = ftDate
        ParamType = ptInput
      end>
  end
  object qryFuncionarios: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT ID_FUNCIONARIO, NOME_FUNCIONARIO'
      '  FROM FUNCIONARIO'
      'ORDER BY NOME_FUNCIONARIO')
    Left = 132
    Top = 255
  end
  object dsFuncionarios: TDataSource
    DataSet = qryFuncionarios
    Left = 132
    Top = 303
  end
  object dspGridPaiPadrao: TDataSetProvider
    DataSet = qryGridPaiPadrao
    Left = 820
    Top = 199
  end
  object cdsGridPaiPadrao: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_FUNC01'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID_FUNC02'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'DATA_INI'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'DATA_FIM'
        ParamType = ptInput
      end>
    ProviderName = 'dspGridPaiPadrao'
    OnCalcFields = cdsGridPaiPadraoCalcFields
    Left = 820
    Top = 247
    object cdsGridPaiPadraoID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object cdsGridPaiPadraoID_FUNCIONARIO_FK: TIntegerField
      FieldName = 'ID_FUNCIONARIO_FK'
    end
    object cdsGridPaiPadraoID_SISTEMA_FK: TIntegerField
      FieldName = 'ID_SISTEMA_FK'
    end
    object cdsGridPaiPadraoCOD_ADICIONAL: TIntegerField
      FieldName = 'COD_ADICIONAL'
    end
    object cdsGridPaiPadraoID_ORIGEM: TIntegerField
      FieldName = 'ID_ORIGEM'
    end
    object cdsGridPaiPadraoTIPO_ORIGEM: TStringField
      FieldName = 'TIPO_ORIGEM'
      FixedChar = True
      Size = 1
    end
    object cdsGridPaiPadraoPERCENT_COMISSAO: TBCDField
      FieldName = 'PERCENT_COMISSAO'
      OnGetText = cdsGridPaiPadraoPERCENT_COMISSAOGetText
      Precision = 18
      Size = 2
    end
    object cdsGridPaiPadraoVR_COMISSAO: TBCDField
      FieldName = 'VR_COMISSAO'
      OnGetText = cdsGridPaiPadraoVR_COMISSAOGetText
      Precision = 18
      Size = 2
    end
    object cdsGridPaiPadraoID_FECHAMENTO_SALARIO_FK: TIntegerField
      FieldName = 'ID_FECHAMENTO_SALARIO_FK'
    end
    object cdsGridPaiPadraoDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsGridPaiPadraoCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsGridPaiPadraoDATA_PAGAMENTO: TDateField
      FieldName = 'DATA_PAGAMENTO'
    end
    object cdsGridPaiPadraoPAG_ID_USUARIO: TIntegerField
      FieldName = 'PAG_ID_USUARIO'
    end
    object cdsGridPaiPadraoFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsGridPaiPadraoDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsGridPaiPadraoCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsGridPaiPadraoDATA_COMISSAO: TDateField
      FieldName = 'DATA_COMISSAO'
    end
    object cdsGridPaiPadraoID_ITEM_FK: TIntegerField
      FieldName = 'ID_ITEM_FK'
    end
    object cdsGridPaiPadraoNOMEFUNCIONARIO: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEFUNCIONARIO'
      Size = 250
    end
    object cdsGridPaiPadraoDESCR_ITEM: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCR_ITEM'
      Size = 250
    end
  end
end
