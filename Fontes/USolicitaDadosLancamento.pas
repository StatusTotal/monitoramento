{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   USolicitaDadosLancamento.pas
  Descricao:   Tela padrao de solicitacao de Categoria e SubCategoria de Lancamento
  Author   :   Cristina
  Date:        03-jan-2017
  Last Update: 01-dez-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit USolicitaDadosLancamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Mask,
  JvExMask, JvToolEdit, JvExControls, JvButton, JvTransparentButton, Vcl.DBCtrls,
  System.StrUtils, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFSolicitaDadosLancamento = class(TForm)
    pnlPrincipal: TPanel;
    lblTitulo: TLabel;
    pnlBotoes: TPanel;
    btnOk: TJvTransparentButton;
    btnCancelar: TJvTransparentButton;
    lblCategoria: TLabel;
    lcbCategoria: TDBLookupComboBox;
    btnIncluirCategoria: TJvTransparentButton;
    lblSubCategoria: TLabel;
    lcbSubCategoria: TDBLookupComboBox;
    btnIncluirSubCategoria: TJvTransparentButton;
    qryCategoria: TFDQuery;
    dsCategoria: TDataSource;
    qrySubCategoria: TFDQuery;
    dsSubCategoria: TDataSource;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure btnIncluirCategoriaClick(Sender: TObject);
    procedure btnIncluirSubCategoriaClick(Sender: TObject);
    procedure lcbCategoriaClick(Sender: TObject);
    procedure lcbCategoriaExit(Sender: TObject);
    procedure lcbCategoriaKeyPress(Sender: TObject; var Key: Char);
    procedure lcbSubCategoriaKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
    iIdCategoria,
    iIdSubCategoria: Integer;

    sCaptionLabel: String;
  end;

var
  FSolicitaDadosLancamento: TFSolicitaDadosLancamento;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais,
  UCadastroCategoriaDespRec, UCadastroSubCategoriaDespRec;

procedure TFSolicitaDadosLancamento.btnCancelarClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TFSolicitaDadosLancamento.btnIncluirCategoriaClick(Sender: TObject);
var
  Op: TOperacao;
  IdCons: Integer;
  OrigF, OrigC: Boolean;
begin
  inherited;

  Op     := vgOperacao;
  IdCons := vgIdConsulta;
  OrigF  := vgOrigemFiltro;
  OrigC  := vgOrigemCadastro;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := False;
  vgOrigemCadastro := True;

  try
    Application.CreateForm(TFCadastroCategoriaDespRec, FCadastroCategoriaDespRec);

    TipoCat := 'D';

    FCadastroCategoriaDespRec.ShowModal;
  finally
    qryCategoria.Close;
    qryCategoria.Params.ParamByName('TIPO_CAT').Value := 'D';
    qryCategoria.Open;

    if FCadastroCategoriaDespRec.IdNovaCategoria <> 0 then
      lcbCategoria.KeyValue := FCadastroCategoriaDespRec.IdNovaCategoria;

    FCadastroCategoriaDespRec.Free;
  end;

  vgOperacao       := Op;
  vgIdConsulta     := IdCons;
  vgOrigemFiltro   := OrigF;
  vgOrigemCadastro := OrigC;

  if Trim(lcbCategoria.Text) <> '' then
  begin
    lcbSubCategoria.Enabled        := True;
    btnIncluirSubCategoria.Enabled := True;

    if qryCategoria.RecordCount > 0 then
    begin
      qrySubCategoria.Close;
      qrySubCategoria.Params.ParamByName('ID_CAT').Value      := qryCategoria.FieldByName('ID_CATEGORIA_DESPREC').AsInteger;
      qrySubCategoria.Params.ParamByName('TIPO_SUBCAT').Value := 'D';
      qrySubCategoria.Open;

      if qrySubCategoria.RecordCount > 0 then
        lcbSubCategoria.Enabled := True
      else
        lcbSubCategoria.Enabled := False;
    end;

    if lcbSubCategoria.CanFocus then
      lcbSubCategoria.SetFocus;
  end;
end;

procedure TFSolicitaDadosLancamento.btnIncluirSubCategoriaClick(
  Sender: TObject);
var
  Op: TOperacao;
  IdCons: Integer;
  OrigF, OrigC: Boolean;
begin
  inherited;

  Op     := vgOperacao;
  IdCons := vgIdConsulta;
  OrigF  := vgOrigemFiltro;
  OrigC  := vgOrigemCadastro;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := False;
  vgOrigemCadastro := True;

  try
    Application.CreateForm(TFCadastroSubCategoriaDespRec, FCadastroSubCategoriaDespRec);

    TipoSubCat := 'D';

    FCadastroSubCategoriaDespRec.ShowModal;
  finally
    if qryCategoria.RecordCount > 0 then
    begin
      qrySubCategoria.Close;
      qrySubCategoria.Params.ParamByName('ID_CAT').Value      := qryCategoria.FieldByName('ID_CATEGORIA_DESPREC').AsInteger;
      qrySubCategoria.Params.ParamByName('TIPO_SUBCAT').Value := 'D';
      qrySubCategoria.Open;

      if qrySubCategoria.RecordCount > 0 then
        lcbSubCategoria.Enabled := True
      else
        lcbSubCategoria.Enabled := False;
    end;

    if FCadastroSubCategoriaDespRec.IdNovaSubCategoria <> 0 then
      lcbSubCategoria.KeyValue := FCadastroSubCategoriaDespRec.IdNovaSubCategoria;

    FCadastroSubCategoriaDespRec.Free;
  end;

  vgOperacao       := Op;
  vgIdConsulta     := IdCons;
  vgOrigemFiltro   := OrigF;
  vgOrigemCadastro := OrigC;
end;

procedure TFSolicitaDadosLancamento.btnOkClick(Sender: TObject);
begin
  iIdCategoria    := 0;
  iIdSubCategoria := 0;

  if (Trim(lcbCategoria.Text) = '') and
    (Trim(lcbSubCategoria.Text) = '') then
  begin
    if Application.MessageBox('N�o h� Categoria ou SubCategoria selecionada. Deseja cancelar informa��o dos dados?',
                              'Confirma��o',
                              MB_YESNO + MB_ICONQUESTION) = ID_YES then
      Self.Close;
  end
  else
  begin
    if (Trim(lcbCategoria.Text) <> '') or
      (Trim(lcbSubCategoria.Text) <> '') then
    begin
      if Trim(lcbCategoria.Text) <> '' then
        iIdCategoria := lcbCategoria.KeyValue;

      if Trim(lcbSubCategoria.Text) <> '' then
        iIdSubCategoria := lcbSubCategoria.KeyValue;

      Self.Close;
    end;
  end;
end;

procedure TFSolicitaDadosLancamento.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFSolicitaDadosLancamento.FormShow(Sender: TObject);
begin
  lblTitulo.Caption := sCaptionLabel;

  qryCategoria.Close;
  qryCategoria.Params.ParamByName('TIPO_CAT').Value := 'D';
  qryCategoria.Open;
end;

procedure TFSolicitaDadosLancamento.lcbCategoriaClick(Sender: TObject);
begin
  if qryCategoria.RecordCount > 0 then
  begin
    qrySubCategoria.Close;
    qrySubCategoria.Params.ParamByName('ID_CAT').Value      := qryCategoria.FieldByName('ID_CATEGORIA_DESPREC').AsInteger;
    qrySubCategoria.Params.ParamByName('TIPO_SUBCAT').Value := 'D';
    qrySubCategoria.Open;

    if qrySubCategoria.RecordCount > 0 then
      lcbSubCategoria.Enabled := True
    else
      lcbSubCategoria.Enabled := False;
  end;
end;

procedure TFSolicitaDadosLancamento.lcbCategoriaExit(Sender: TObject);
begin
  if qryCategoria.RecordCount > 0 then
  begin
    qrySubCategoria.Close;
    qrySubCategoria.Params.ParamByName('ID_CAT').Value      := qryCategoria.FieldByName('ID_CATEGORIA_DESPREC').AsInteger;
    qrySubCategoria.Params.ParamByName('TIPO_SUBCAT').Value := 'D';
    qrySubCategoria.Open;

    if qrySubCategoria.RecordCount > 0 then
      lcbSubCategoria.Enabled := True
    else
      lcbSubCategoria.Enabled := False;
  end;
end;

procedure TFSolicitaDadosLancamento.lcbCategoriaKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if lcbSubCategoria.Enabled then
      SelectNext(ActiveControl, True, True)
    else
      btnOk.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFSolicitaDadosLancamento.lcbSubCategoriaKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

end.
