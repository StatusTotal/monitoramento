{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroLoteEtiquetaa.pas
  Descricao:   Filtro de Lotes de Etiquetas
  Author   :   Pedro
  Date:        19-jun-2017
  Last Update: 11-out-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroLoteEtiqueta;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls,System.DateUtils,sBitBtn, Vcl.Mask, sMaskEdit, sCustomComboEdit,
  sToolEdit, Vcl.StdCtrls, sGroupBox, sComboBox, sEdit, Datasnap.DBClient,
  Datasnap.Provider, JvComponentBase, JvDBGridExport, UCadastroLoteEtiqueta,
  System.StrUtils;

type
  TFFiltroLoteEtiqueta = class(TFFiltroSimplesPadrao)
    edpesquisa: TsEdit;
    rgStatus: TsRadioGroup;
    edInicio: TsDateEdit;
    edFim: TsDateEdit;
    dspGridPaiPadrao: TDataSetProvider;
    cdsGridPaiPadrao: TClientDataSet;
    dbgxExport: TJvDBGridExcelExport;
    cdsGridPaiPadraoID: TIntegerField;
    cdsGridPaiPadraoNUM_LOTE: TStringField;
    cdsGridPaiPadraoLETRA: TStringField;
    cdsGridPaiPadraoNUM_INICIAL: TStringField;
    cdsGridPaiPadraoNUM_FINAL: TStringField;
    cdsGridPaiPadraoQTD_ETIQUETAS: TIntegerField;
    cdsGridPaiPadraoMASCARA: TStringField;
    cdsGridPaiPadraoFLG_SITUACAO: TStringField;
    cdsGridPaiPadraoDATA_CADASTRO: TSQLTimeStampField;
    cdsGridPaiPadraoCAD_ID_USUARIO: TIntegerField;
    cdsGridPaiPadraoDATA_COMPRA: TDateField;
    cdsGridPaiPadraoDATA_CANCELAMENTO: TDateField;
    cdsGridPaiPadraoCANCEL_ID_USUARIO: TIntegerField;
    cdsGridPaiPadraoSITUACAO: TStringField;
    procedure btnIncluirClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
//    procedure btnExportarClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure cdsGridPaiPadraoCalcFields(DataSet: TDataSet);
    procedure dbgGridPaiPadraoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure edInicioKeyPress(Sender: TObject; var Key: Char);
    procedure edFimKeyPress(Sender: TObject; var Key: Char);
    procedure edpesquisaKeyPress(Sender: TObject; var Key: Char);
    procedure rgStatusClick(Sender: TObject);
    procedure rgStatusExit(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroLoteEtiqueta: TFFiltroLoteEtiqueta;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UDMEtiqueta;

procedure TFFiltroLoteEtiqueta.FormCreate(Sender: TObject);
begin
  edInicio.Date := StartOfTheMonth(Date);
  edFim.Date    := EndOfTheMonth(Date);

  btnFiltrarClick(Sender);
end;

procedure TFFiltroLoteEtiqueta.VerificarPermissoes;
begin
  inherited;

  btnIncluir.Enabled  := vgPrm_IncLtEtq;
  btnEditar.Enabled   := vgPrm_EdLtEtq and (cdsGridPaiPadrao.RecordCount > 0);
  btnExcluir.Enabled  := vgPrm_ExcLtEtq and (cdsGridPaiPadrao.RecordCount > 0);
  btnImprimir.Enabled := vgPrm_ImpLtEtq and (cdsGridPaiPadrao.RecordCount > 0);
end;

function TFFiltroLoteEtiqueta.PodeExcluir(var Msg: String): Boolean;
begin
  Result := True;

  if cdsGridPaiPadrao.FieldByName('FLG_SITUACAO').AsString = 'C' then
    msg := 'N�o � poss�vel cancelar o lote, pois o mesmo j� foi cancelado.';

  Result := (cdsGridPaiPadrao.FieldByName('FLG_SITUACAO').AsString <> 'C') and
            (cdsGridPaiPadrao.RecordCount > 0);
end;

procedure TFFiltroLoteEtiqueta.rgStatusClick(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroLoteEtiqueta.rgStatusExit(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroLoteEtiqueta.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    X:  //EXCLUSAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo do cancelamento desse Lote de Etiquetas:', '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(5, '', sObservacao,
                          cdsGridPaiPadrao.FieldByName('ID').AsInteger,
                          'LOTEETIQUETA');
    end;
    P:  //IMPRESSAO
    begin
      BS.GravarUsuarioLog(5, '', 'Somente Gerais do Lote.',
                          cdsGridPaiPadrao.FieldByName('ID').AsInteger,
                          'LOTEETIQUETA');
    end;
  end;
end;

procedure TFFiltroLoteEtiqueta.btnEditarClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  if cdsGridPaiPadrao.FieldByName('FLG_SITUACAO').AsString = 'C' then
    vgOperacao := C
  else
    vgOperacao := E;

  vgIdConsulta := cdsGridPaiPadrao.FieldByName('ID').AsInteger;
  vgOrigemFiltro := True;

  Application.CreateForm(TdmEtiqueta, dmEtiqueta);
  dmGerencial.CriarForm(TFCadastroLoteEtiqueta, FCadastroLoteEtiqueta);
  FreeAndNil(dmEtiqueta);

  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroLoteEtiqueta.btnExcluirClick(Sender: TObject);
var
  qryExclusao: TFDQuery;
begin
  inherited;

  if lPodeExcluir then
  begin
    qryExclusao := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      with qryExclusao, SQL do
      begin
        Close;
        Clear;
        Text := 'UPDATE ETIQUETA ' +
                '   SET FLG_SITUACAO = ' + QuotedStr('C') + ', ' +
                '       DATA_CANCELAMENTO = :DATA_CANCELAMENTO, ' +
                '       CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO ' +
                ' WHERE ID_LOTEETIQUETA_FK = :ID_LOTEETIQUETA';
        Params.ParamByName('DATA_CANCELAMENTO').Value := Date;
        Params.ParamByName('CANCEL_ID_USUARIO').Value := vgUsu_Id;
        Params.ParamByName('ID_LOTEETIQUETA').Value   := cdsGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;
      end;

      with qryExclusao, SQL do
      begin
        Close;
        Clear;
        Text := 'UPDATE LOTEETIQUETA ' +
                '   SET FLG_SITUACAO = ' + QuotedStr('C') + ', ' +
                '       DATA_CANCELAMENTO = :DATA_CANCELAMENTO, ' +
                '       CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO ' +
                ' WHERE ID_LOTEETIQUETA = :ID_LOTEETIQUETA';
        Params.ParamByName('DATA_CANCELAMENTO').Value := Date;
        Params.ParamByName('CANCEL_ID_USUARIO').Value := vgUsu_Id;
        Params.ParamByName('ID_LOTEETIQUETA').Value   := cdsGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      GravarLog;
      Application.MessageBox('Lote Cancelado com sucesso!', 'Aviso', MB_OK)
    except
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Application.MessageBox('Erro no cancelamento do Lote.', 'Erro', MB_OK + MB_ICONERROR)
    end;

    FreeAndNil(qryExclusao);

    btnFiltrar.Click;
  end;
end;

{procedure TFFiltroLoteEtiqueta.btnExportarClick(Sender: TObject);
begin
  inherited;
  try
    dbgxExport.Grid        := dbgGridPaiPadrao;
    dbgxExport.FileName    := vgConf_DiretorioRelatorios +
                              IntToStr(YearOf(Now)) + '\' +
                              'EstoqueLotedeEtiqueta' +
                              dmGerencial.PegarNumeroTexto(edInicio.Text) + '_a_' +
                              dmGerencial.PegarNumeroTexto(edFim.Text);
    dbgxExport.Orientation := woLandscape;
    dbgxExport.AutoFit     := True;
    dbgxExport.Silent      := False;
    dbgxExport.Visible     := True;
    dbgxExport.Close       := scNever;
    dbgxExport.ExportGrid;

    Application.MessageBox('Lista exportada com sucesso!',
                           'Sucesso',
                           MB_OK);
  except
    dmGerencial.Aviso('N�o foi poss�vel gerar o arquivo. Por favor, tente novamente!');
  end;
end;  }

procedure TFFiltroLoteEtiqueta.btnFiltrarClick(Sender: TObject);
begin
  cdsGridPaiPadrao.Close;

  cdsGridPaiPadrao.Params.ParamByName('DT_COMP1').Value := FormatDateTime('YYYY-MM-DD', edInicio.Date);
  cdsGridPaiPadrao.Params.ParamByName('DT_COMP2').Value := FormatDateTime('YYYY-MM-DD', edFim.Date);

  if Trim(edpesquisa.Text) <> '' then
  begin
    cdsGridPaiPadrao.Params.ParamByName('NUM_LOTE1').Value := '%' + edpesquisa.Text + '%';
    cdsGridPaiPadrao.Params.ParamByName('NUM_LOTE2').Value := '%' + edpesquisa.Text + '%';
  end
  else
  begin
    cdsGridPaiPadrao.Params.ParamByName('NUM_LOTE1').Value := dmGerencial.Replicar('*', 15);
    cdsGridPaiPadrao.Params.ParamByName('NUM_LOTE2').Value := dmGerencial.Replicar('*', 15);
  end;

  case rgStatus.ItemIndex of
    0:
    begin
      cdsGridPaiPadrao.Params.ParamByName('FLG_SIT1').Value := '*';
      cdsGridPaiPadrao.Params.ParamByName('FLG_SIT2').Value := '*';
    end;
    1:
    begin
      cdsGridPaiPadrao.Params.ParamByName('FLG_SIT1').Value := 'U';
      cdsGridPaiPadrao.Params.ParamByName('FLG_SIT2').Value := 'U';
    end;
    2:
    begin
      cdsGridPaiPadrao.Params.ParamByName('FLG_SIT1').Value := 'N';
      cdsGridPaiPadrao.Params.ParamByName('FLG_SIT2').Value := 'N';
    end;
    3:
    begin
      cdsGridPaiPadrao.Params.ParamByName('FLG_SIT1').Value := 'C';
      cdsGridPaiPadrao.Params.ParamByName('FLG_SIT2').Value := 'C';
    end;
  end;

  cdsGridPaiPadrao.Open;

  inherited;
end;

procedure TFFiltroLoteEtiqueta.btnImprimirClick(Sender: TObject);
begin
  inherited;

  dmPrincipal.AbrirRelatorio(102);
end;

procedure TFFiltroLoteEtiqueta.btnIncluirClick(Sender: TObject);
begin
  inherited;

  vgOperacao     := I;
  vgIdConsulta   := 0;
  vgOrigemFiltro := True;

  Application.CreateForm(TdmEtiqueta, dmEtiqueta);
  dmGerencial.CriarForm(TFCadastroLoteEtiqueta, FCadastroLoteEtiqueta);
  FreeAndNil(dmEtiqueta);

  btnFiltrar.Click;
end;

procedure TFFiltroLoteEtiqueta.btnLimparClick(Sender: TObject);
begin
  inherited;

  edInicio.Date := StartOfTheMonth(Date);
  edFim.Date    := EndOfTheMonth(Date);

  edpesquisa.Clear;
  rgStatus.ItemIndex := 0;

  inherited;

  if edInicio.CanFocus then
    edInicio.SetFocus;
end;

procedure TFFiltroLoteEtiqueta.cdsGridPaiPadraoCalcFields(DataSet: TDataSet);
begin
  inherited;

  case AnsiIndexStr(UpperCase(cdsGridPaiPadrao.FieldByName('FLG_SITUACAO').AsString), ['U', 'N', 'C']) of
    0: cdsGridPaiPadrao.FieldByName('SITUACAO').AsString := 'Utilizado';
    1: cdsGridPaiPadrao.FieldByName('SITUACAO').AsString := 'N�o Utilizado';
    2: cdsGridPaiPadrao.FieldByName('SITUACAO').AsString := 'Cancelado';
  end;
end;

procedure TFFiltroLoteEtiqueta.dbgGridPaiPadraoDblClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao     := C;
  vgIdConsulta   := cdsGridPaiPadrao.FieldByName('ID').AsInteger;
  vgOrigemFiltro := True;

  Application.CreateForm(TdmEtiqueta, dmEtiqueta);
  dmGerencial.CriarForm(TFCadastroLoteEtiqueta, FCadastroLoteEtiqueta);
  FreeAndNil(dmEtiqueta);

  DefinirPosicaoGrid;
end;

procedure TFFiltroLoteEtiqueta.dbgGridPaiPadraoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;

  dbgGridPaiPadrao := Sender as TDBGrid;

  if dbgGridPaiPadrao.DataSource.DataSet.FieldByName('FLG_SITUACAO').AsString = 'C' then
  begin
    dbgGridPaiPadrao.Canvas.Font.Color := clGray;
    dbgGridPaiPadrao.Canvas.FillRect(Rect);
    dbgGridPaiPadrao.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TFFiltroLoteEtiqueta.edFimKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLoteEtiqueta.edInicioKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLoteEtiqueta.edpesquisaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

procedure TFFiltroLoteEtiqueta.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #27 then Close;
  inherited;
end;

procedure TFFiltroLoteEtiqueta.FormShow(Sender: TObject);
begin
  btnFiltrar.Click;

  ShowScrollBar(dbgGridPaiPadrao.Handle, SB_HORZ, False);

  inherited;

  if edInicio.CanFocus then
    edInicio.SetFocus;
end;

end.
