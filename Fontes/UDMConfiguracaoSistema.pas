{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UDMConfiguracaoSistema.pas
  Descricao:   Data Module da Tela de configuracao dos parametros do Sistema
  Author   :   Cristina
  Date:        26-fev-2016
  Last Update: 01-dez-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UDMConfiguracaoSistema;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TdmConfiguracaoSistema = class(TDataModule)
    qryConfiguracao: TFDQuery;
    dspConfiguracao: TDataSetProvider;
    cdsConfiguracao: TClientDataSet;
    qryDispositivo: TFDQuery;
    dspDispositivo: TDataSetProvider;
    dsDispositivo: TDataSource;
    cdsDispositivo: TClientDataSet;
    dsConfiguracao: TDataSource;
    dsServentia: TDataSource;
    dsTipoDispositivo: TDataSource;
    qryTipoDispositivo: TFDQuery;
    qryServentia: TFDQuery;
    qryTipoDispositivoID_TIPO_DISPOSITIVO: TIntegerField;
    qryTipoDispositivoDESCR_TIPO_DISPOSITIVO: TStringField;
    qryServentiaCOD_SERVENTIA: TStringField;
    qryServentiaNOME_SERVENTIA: TStringField;
    cdsDispositivoID_DISPOSITIVO: TIntegerField;
    cdsDispositivoID_TIPO_DISPOSITIVO_FK: TIntegerField;
    cdsDispositivoDIR_DISPOSITIVO: TStringField;
    cdsDispositivoFLG_ATIVO: TStringField;
    cdsDispositivoDESCR_TIPO_DISPOSITIVO: TStringField;
    cdsDispositivoNOVO: TBooleanField;
    cdsConfiguracaoINTERVALO_SYNC: TIntegerField;
    cdsConfiguracaoATRIBUICAO: TIntegerField;
    cdsConfiguracaoAMBIENTE: TStringField;
    cdsConfiguracaoCOD_SERVENTIA: TStringField;
    cdsConfiguracaoDIR_DATABASE: TStringField;
    cdsConfiguracaoDIR_ARQ_IMAGENS: TStringField;
    cdsConfiguracaoDIR_ARQ_RELATORIOS: TStringField;
    cdsConfiguracaoFLG_EXB_ATUALIZACAO: TStringField;
    cdsConfiguracaoDATA_ULT_ATUDESPESAS: TDateField;
    cdsConfiguracaoID_USUATUALIZAREC: TIntegerField;
    cdsConfiguracaoID_ULTRECIBOATUALF: TLargeintField;
    cdsConfiguracaoID_USUATUALIZAFIR: TIntegerField;
    cdsConfiguracaoID_ULTRECIBOATUALF_A: TIntegerField;
    cdsConfiguracaoID_USUATUALIZAFIR_A: TIntegerField;
    cdsConfiguracaoDIA_PAGAMENTOFUNC: TIntegerField;
    cdsConfiguracaoFLG_COMISSAO: TStringField;
    cdsConfiguracaoVERSAO_SYNC: TStringField;
    cdsConfiguracaoID_ULTRECIBOATUAL_T: TIntegerField;
    cdsConfiguracaoID_ULTRECIBOATUAL_B: TIntegerField;
    cdsConfiguracaoDATA_INI_IMPORTACAO: TDateField;
    cdsConfiguracaoDIR_ARQ_CARNELEAO: TStringField;
    qryUF: TFDQuery;
    dsUF: TDataSource;
    dsCidade: TDataSource;
    qryCidade: TFDQuery;
    cdsConfiguracaoENDERECO: TStringField;
    cdsConfiguracaoCIDADE: TStringField;
    cdsConfiguracaoUF: TStringField;
    qryServentiaENDERECO: TStringField;
    qryServentiaCIDADE: TStringField;
    qryServentiaUF: TStringField;
    qryServentiaCNPJ: TStringField;
    cdsConfiguracaoNOME_SERVENTIA_RELATORIO: TStringField;
    cdsConfiguracaoFLG_BLOQUEIO_FECHAMENTO: TStringField;
    cdsConfiguracaoFLG_EXPEDIENTESABADO: TStringField;
    cdsConfiguracaoFLG_EXPEDIENTEDOMINGO: TStringField;
    cdsConfiguracaoFLG_SITUACAOCAIXA: TStringField;
    cdsConfiguracaoFLG_EXPEDIENTEFERIADO: TStringField;
    cdsConfiguracaoFLG_ZERARNUMOFICIO: TStringField;
    cdsConfiguracaoHORA_FECHAMENTOPADRAO: TTimeField;
    cdsConfiguracaoDIA_FECHAMENTOCAIXA: TStringField;
    cdsConfiguracaoDATA_ULT_ATULANCOLAB: TDateField;
    procedure cdsDispositivoCalcFields(DataSet: TDataSet);
    procedure cdsConfiguracaoINTERVALO_SYNCGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmConfiguracaoSistema: TdmConfiguracaoSistema;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses UDM, UGDM, UBibliotecaSistema, UVariaveisGlobais;

{$R *.dfm}

procedure TdmConfiguracaoSistema.cdsConfiguracaoINTERVALO_SYNCGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := IntToStr(dmGerencial.PegarNumero(Sender.AsString));
end;

procedure TdmConfiguracaoSistema.cdsDispositivoCalcFields(DataSet: TDataSet);
var
  QryNomeDisp: TFDQuery;
begin
  QryNomeDisp := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryNomeDisp, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT DESCR_TIPO_DISPOSITIVO FROM TIPO_DISPOSITIVO ' +
            ' WHERE ID_TIPO_DISPOSITIVO = :ID_TIPO_DISPOSITIVO';
    Params.ParamByName('ID_TIPO_DISPOSITIVO').Value := cdsDispositivo.FieldByName('ID_TIPO_DISPOSITIVO_FK').AsInteger;
    Open;

    cdsDispositivo.FieldByName('DESCR_TIPO_DISPOSITIVO').AsString := QryNomeDisp.FieldByName('DESCR_TIPO_DISPOSITIVO').AsString;
  end;

  FreeAndNil(QryNomeDisp);
end;

end.
