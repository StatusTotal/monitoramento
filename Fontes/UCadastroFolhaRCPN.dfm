inherited FCadastroFolhaRCPN: TFCadastroFolhaRCPN
  Caption = 'Cadastro de Folha de Seguran'#231'a de RCPN'
  ClientHeight = 302
  ClientWidth = 585
  ExplicitWidth = 591
  ExplicitHeight = 331
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 579
    Height = 296
    ExplicitWidth = 579
    ExplicitHeight = 296
    inherited pnlDados: TPanel
      Width = 465
      Height = 290
      ExplicitWidth = 465
      ExplicitHeight = 290
      object lblNomeFuncionario: TLabel [0]
        Left = 7
        Top = 48
        Width = 100
        Height = 14
        Caption = 'Nome Colaborador'
      end
      inherited pnlMsgErro: TPanel
        Top = 229
        Width = 465
        TabOrder = 7
        ExplicitTop = 229
        ExplicitWidth = 465
        inherited lblMensagemErro: TLabel
          Width = 465
        end
        inherited lbMensagemErro: TListBox
          Width = 465
          OnDblClick = lbMensagemErroDblClick
          ExplicitWidth = 465
        end
      end
      object edDataPratica: TsDBDateEdit
        Left = 142
        Top = 20
        Width = 121
        Height = 21
        AutoSize = False
        EditMask = '!99/99/9999;1; '
        MaxLength = 10
        TabOrder = 1
        Text = '  /  /    '
        OnKeyPress = FormKeyPress
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Data Pr'#225'tica'
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        DataField = 'DATA_UTILIZACAO'
        DataSource = dsCadastro
      end
      object edFolha: TsDBEdit
        Left = 7
        Top = 20
        Width = 129
        Height = 22
        CharCase = ecUpperCase
        DataField = 'NUM_FOLHA'
        DataSource = dsCadastro
        ReadOnly = True
        TabOrder = 0
        OnKeyPress = FormKeyPress
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Numera'#231#227'o Folha'
        BoundLabel.Layout = sclTopLeft
      end
      object edLetra: TsDBEdit
        Left = 269
        Top = 20
        Width = 56
        Height = 22
        CharCase = ecUpperCase
        DataField = 'LETRA'
        DataSource = dsCadastro
        TabOrder = 2
        OnKeyPress = FormKeyPress
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Letra'
        BoundLabel.Layout = sclTopLeft
      end
      object edNumero: TsDBEdit
        Left = 331
        Top = 20
        Width = 65
        Height = 22
        CharCase = ecUpperCase
        DataField = 'NUMERO'
        DataSource = dsCadastro
        MaxLength = 5
        TabOrder = 3
        OnKeyPress = FormKeyPress
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'N'#250'mero'
        BoundLabel.Layout = sclTopLeft
      end
      object edAleatorio: TsDBEdit
        Left = 404
        Top = 20
        Width = 57
        Height = 22
        CharCase = ecUpperCase
        DataField = 'ALEATORIO'
        DataSource = dsCadastro
        TabOrder = 4
        OnKeyPress = FormKeyPress
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Aleat'#243'rio'
        BoundLabel.Layout = sclTopLeft
      end
      object MMObs: TsDBMemo
        Left = 0
        Top = 108
        Width = 454
        Height = 115
        DataField = 'OBS_FOLHASEGURANCA'
        DataSource = dsCadastro
        TabOrder = 6
        OnKeyPress = MMObsKeyPress
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Observa'#231#227'o do Ato'
        BoundLabel.Layout = sclTopLeft
        CharCase = ecUpperCase
      end
      object lcbNomeFuncionario: TDBLookupComboBox
        Left = 7
        Top = 64
        Width = 256
        Height = 22
        Color = 16114127
        DataField = 'ID_FUNCIONARIO_FK'
        DataSource = dsCadastro
        KeyField = 'ID_FUNCIONARIO'
        ListField = 'NOME_FUNCIONARIO'
        ListSource = dmFolhaSeguranca.dsFuncionarios
        NullValueKey = 16460
        TabOrder = 5
        OnKeyPress = FormKeyPress
      end
    end
    inherited pnlMenu: TPanel
      Height = 290
      ExplicitHeight = 290
      inherited btnOk: TJvTransparentButton
        Top = 200
        ExplicitTop = 74
      end
      inherited btnCancelar: TJvTransparentButton
        Top = 245
        ExplicitTop = 119
      end
    end
  end
  inherited dsCadastro: TDataSource
    DataSet = dmFolhaSeguranca.cdsFolha
    Left = 518
    Top = 243
  end
end
