inherited FListaProdutoServico: TFListaProdutoServico
  Caption = 'Lista de Produto/Servi'#231'o'
  ClientWidth = 535
  OnCreate = FormCreate
  ExplicitWidth = 541
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 529
    ExplicitWidth = 529
    inherited pnlGrid: TPanel
      Top = 84
      Width = 523
      Height = 359
      ExplicitTop = 84
      ExplicitWidth = 523
      ExplicitHeight = 359
      inherited dbgGridListaPadrao: TDBGrid
        Width = 523
        Height = 359
        OnDblClick = dbgGridListaPadraoDblClick
        OnTitleClick = dbgGridListaPadraoTitleClick
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'ID'
            Title.Alignment = taCenter
            Title.Caption = '#'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TIPO_ITEM'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DESCR_ITEM'
            Title.Caption = 'Descri'#231#227'o'
            Width = 450
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ABREV_ITEM'
            Title.Caption = 'Abrevia'#231#227'o'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'FLG_ATIVO'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DATA_INATIVACAO'
            Visible = False
          end>
      end
    end
    inherited pnlFiltro: TPanel
      Width = 529
      Height = 81
      ExplicitWidth = 529
      ExplicitHeight = 81
      inherited btnFiltrar: TJvTransparentButton
        Left = 443
        Top = 50
        OnClick = btnFiltrarClick
        ExplicitLeft = 443
        ExplicitTop = 50
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 443
        Top = 20
        ExplicitLeft = 443
        ExplicitTop = 20
      end
      object lblDescricao: TLabel
        Left = 3
        Top = 37
        Width = 51
        Height = 14
        Caption = 'Descri'#231#227'o'
      end
      object lblSelecione: TLabel
        Left = 3
        Top = 8
        Width = 57
        Height = 14
        Caption = 'Selecione'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edtDescricao: TEdit
        Left = 3
        Top = 54
        Width = 434
        Height = 22
        Color = 16114127
        TabOrder = 0
        OnKeyPress = edtDescricaoKeyPress
      end
    end
  end
  inherited dsGridListaPadrao: TDataSource
    Left = 452
  end
  inherited qryGridListaPadrao: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT ID_ITEM AS ID,'
      '       TIPO_ITEM,'
      '       DESCR_ITEM,'
      '       ABREV_ITEM,'
      '       FLG_ATIVO,'
      '       DATA_INATIVACAO'
      '  FROM ITEM')
    Left = 452
    object qryGridListaPadraoID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID_ITEM'
      Required = True
    end
    object qryGridListaPadraoTIPO_ITEM: TStringField
      FieldName = 'TIPO_ITEM'
      Origin = 'TIPO_ITEM'
      FixedChar = True
      Size = 1
    end
    object qryGridListaPadraoDESCR_ITEM: TStringField
      FieldName = 'DESCR_ITEM'
      Origin = 'DESCR_ITEM'
      Size = 250
    end
    object qryGridListaPadraoABREV_ITEM: TStringField
      FieldName = 'ABREV_ITEM'
      Origin = 'ABREV_ITEM'
      Size = 150
    end
    object qryGridListaPadraoFLG_ATIVO: TStringField
      FieldName = 'FLG_ATIVO'
      Origin = 'FLG_ATIVO'
      FixedChar = True
      Size = 1
    end
    object qryGridListaPadraoDATA_INATIVACAO: TDateField
      FieldName = 'DATA_INATIVACAO'
      Origin = 'DATA_INATIVACAO'
    end
  end
end
