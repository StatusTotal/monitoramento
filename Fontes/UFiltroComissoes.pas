{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroComissoes.pas
  Descricao:   Tela de filtro de Comissoes de Funcionarios
  Author   :   Cristina
  Date:        13-dez-2016
  Last Update: 11-out-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroComissoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Mask, JvExMask, JvToolEdit, Vcl.DBCtrls,
  Datasnap.DBClient, Datasnap.Provider, System.DateUtils;

type
  TFFiltroComissoes = class(TFFiltroSimplesPadrao)
    qryFuncionarios: TFDQuery;
    dsFuncionarios: TDataSource;
    lblNomeFuncionario: TLabel;
    lblPeriodoCadastro: TLabel;
    Label3: TLabel;
    lcbNomeFuncionario: TDBLookupComboBox;
    dteIniPeriodoCadastro: TJvDateEdit;
    dteFimPeriodoCadastro: TJvDateEdit;
    rgSituacaoVale: TRadioGroup;
    dspGridPaiPadrao: TDataSetProvider;
    cdsGridPaiPadrao: TClientDataSet;
    cdsGridPaiPadraoID: TIntegerField;
    cdsGridPaiPadraoID_FUNCIONARIO_FK: TIntegerField;
    cdsGridPaiPadraoID_SISTEMA_FK: TIntegerField;
    cdsGridPaiPadraoCOD_ADICIONAL: TIntegerField;
    cdsGridPaiPadraoID_ORIGEM: TIntegerField;
    cdsGridPaiPadraoTIPO_ORIGEM: TStringField;
    cdsGridPaiPadraoPERCENT_COMISSAO: TBCDField;
    cdsGridPaiPadraoVR_COMISSAO: TBCDField;
    cdsGridPaiPadraoID_FECHAMENTO_SALARIO_FK: TIntegerField;
    cdsGridPaiPadraoDATA_CADASTRO: TSQLTimeStampField;
    cdsGridPaiPadraoCAD_ID_USUARIO: TIntegerField;
    cdsGridPaiPadraoDATA_PAGAMENTO: TDateField;
    cdsGridPaiPadraoPAG_ID_USUARIO: TIntegerField;
    cdsGridPaiPadraoFLG_CANCELADO: TStringField;
    cdsGridPaiPadraoDATA_CANCELAMENTO: TDateField;
    cdsGridPaiPadraoCANCEL_ID_USUARIO: TIntegerField;
    cdsGridPaiPadraoDATA_COMISSAO: TDateField;
    cdsGridPaiPadraoID_ITEM_FK: TIntegerField;
    cdsGridPaiPadraoNOMEFUNCIONARIO: TStringField;
    cdsGridPaiPadraoDESCR_ITEM: TStringField;
    procedure btnEditarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure cdsGridPaiPadraoVR_COMISSAOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure cdsGridPaiPadraoCalcFields(DataSet: TDataSet);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure dbgGridPaiPadraoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgGridPaiPadraoTitleClick(Column: TColumn);
    procedure dteIniPeriodoCadastroKeyPress(Sender: TObject; var Key: Char);
    procedure dteFimPeriodoCadastroKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lcbNomeFuncionarioKeyPress(Sender: TObject; var Key: Char);
    procedure rgSituacaoValeExit(Sender: TObject);
    procedure cdsGridPaiPadraoPERCENT_COMISSAOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFiltroComissoes: TFFiltroComissoes;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UCadastroComissao,
  UCadastroFechamentoComissao;

{ TFFiltroComissoes }

procedure TFFiltroComissoes.btnEditarClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  if cdsGridPaiPadrao.FieldByName('FLG_CANCELADO').AsString = 'S' then
    vgOperacao := C
  else if (not cdsGridPaiPadrao.FieldByName('DATA_PAGAMENTO').IsNull) and
    (not cdsGridPaiPadrao.FieldByName('ID_FECHAMENTO_SALARIO_FK').IsNull) then
  begin
    Application.MessageBox('Essa Comiss�o j� foi paga e n�o pode ser alterada.', 'Aviso', MB_OK + MB_ICONWARNING);
    vgOperacao := C;
  end
  else
    vgOperacao   := E;

  vgIdConsulta := cdsGridPaiPadrao.FieldByName('ID').AsInteger;

  dmGerencial.CriarForm(TFCadastroComissao, FCadastroComissao);
  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroComissoes.btnExcluirClick(Sender: TObject);
var
  qryExclusao: TFDQuery;
  CodLanc, AnoLanc: Integer;
begin
  inherited;

  if lPodeExcluir then
  begin
    CodLanc := 0;
    AnoLanc := 0;

    qryExclusao := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      with qryExclusao, SQL do
      begin
        //Lancamento (Consulta)
        Close;
        Clear;
        Text := 'SELECT COD_LANCAMENTO, ANO_LANCAMENTO ' +
                '  FROM LANCAMENTO' +
                ' WHERE ID_ORIGEM      = :ID_ORIGEM ' +
                '   AND ID_NATUREZA_FK = :ID_NATUREZA';
        Params.ParamByName('ID_ORIGEM').Value   := cdsGridPaiPadrao.FieldByName('ID').AsInteger;
        Params.ParamByName('ID_NATUREZA').Value := 2;  //COMISSAO
        Open;

        CodLanc := qryExclusao.FieldByName('COD_LANCAMENTO').AsInteger;
        AnoLanc := qryExclusao.FieldByName('ANO_LANCAMENTO').AsInteger;

        //Parcela Lancamento
        Close;
        Clear;

        Text := 'UPDATE LANCAMENTO_PARC ' +
                '                   SET FLG_STATUS        = :FLG_STATUS, ' +
                '                       DATA_CANCELAMENTO = :DATA_CANCELAMENTO, ' +
                '                       CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO ' +
                ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO';

        Params.ParamByName('FLG_STATUS').Value        := 'C';
        Params.ParamByName('DATA_CANCELAMENTO').Value := Now;
        Params.ParamByName('CANCEL_ID_USUARIO').Value := vgUsu_Id;
        Params.ParamByName('COD_LANCAMENTO').Value    := CodLanc;
        Params.ParamByName('ANO_LANCAMENTO').Value    := AnoLanc;
        ExecSQL;

        //Lancamento
        Close;
        Clear;

        Text := 'UPDATE LANCAMENTO ' +
                '              SET FLG_CANCELADO     = :FLG_CANCELADO, ' +
                '                  DATA_CANCELAMENTO = :DATA_CANCELAMENTO, ' +
                '                  CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO ' +
                ' WHERE COD_LANCAMENTO = :COD_LANCAMENTO ' +
                '   AND ANO_LANCAMENTO = :ANO_LANCAMENTO';

        Params.ParamByName('FLG_CANCELADO').Value     := 'S';
        Params.ParamByName('DATA_CANCELAMENTO').Value := Now;
        Params.ParamByName('CANCEL_ID_USUARIO').Value := vgUsu_Id;
        Params.ParamByName('COD_LANCAMENTO').Value    := CodLanc;
        Params.ParamByName('ANO_LANCAMENTO').Value    := AnoLanc;
        ExecSQL;

        //Vale
        Close;
        Clear;
        Text := 'UPDATE COMISSAO ' +
                '        SET FLG_CANCELADO     = :FLG_CANCELADO, ' +
                '            DATA_CANCELAMENTO = :DATA_CANCELAMENTO, ' +
                '            CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO ' +
                ' WHERE ID_COMISSAO_FK = :ID_COMISSAO';
        Params.ParamByName('FLG_CANCELADO').Value     := 'S';
        Params.ParamByName('DATA_CANCELAMENTO').Value := Date;
        Params.ParamByName('CANCEL_ID_USUARIO').Value := vgUsu_Id;
        Params.ParamByName('ID_COMISSAO').Value       := cdsGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;

        dmPrincipal.RecalcularFechamentoSalarios(cdsGridPaiPadrao.FieldByName('ID_FECHAMENTO_SALARIO_FK').AsInteger,
                                                 cdsGridPaiPadrao.FieldByName('ID_FUNCIONARIO_FK').AsInteger,
                                                 False, False, True);
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      GravarLog;
      Application.MessageBox('Comiss�o exclu�da com sucesso!', 'Aviso', MB_OK)
    except
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Application.MessageBox('Erro na exclus�o da Comiss�o.', 'Erro', MB_OK + MB_ICONERROR)
    end;

    FreeAndNil(qryExclusao);

    btnFiltrar.Click;
  end;
end;

procedure TFFiltroComissoes.btnFiltrarClick(Sender: TObject);
begin
  cdsGridPaiPadrao.Close;

  if Trim(lcbNomeFuncionario.Text) <> '' then
  begin
    cdsGridPaiPadrao.Params.ParamByName('ID_FUNC01').Value := lcbNomeFuncionario.KeyValue;
    cdsGridPaiPadrao.Params.ParamByName('ID_FUNC02').Value := lcbNomeFuncionario.KeyValue;
  end
  else
  begin
    cdsGridPaiPadrao.Params.ParamByName('ID_FUNC01').Value := 0;
    cdsGridPaiPadrao.Params.ParamByName('ID_FUNC02').Value := 0;
  end;

  cdsGridPaiPadrao.Params.ParamByName('DATA_INI').Value := FormatDateTime('YYYY-MM-DD', dteIniPeriodoCadastro.Date);
  cdsGridPaiPadrao.Params.ParamByName('DATA_FIM').Value := FormatDateTime('YYYY-MM-DD', dteFimPeriodoCadastro.Date);

  cdsGridPaiPadrao.Open;

  inherited;
end;

procedure TFFiltroComissoes.btnIncluirClick(Sender: TObject);
begin
  inherited;

  vgOperacao   := I;
  vgIdConsulta := 0;

  dmGerencial.CriarForm(TFCadastroFechamentoComissao, FCadastroFechamentoComissao);
  btnFiltrar.Click;
end;

procedure TFFiltroComissoes.btnLimparClick(Sender: TObject);
begin
  inherited;

  dteIniPeriodoCadastro.Date := StartOfTheMonth(Date);
  dteFimPeriodoCadastro.Date := EndOfTheMonth(Date);

  lcbNomeFuncionario.KeyValue := Null;
  rgSituacaoVale.ItemIndex    := 0;

  inherited;

  if dteIniPeriodoCadastro.CanFocus then
    dteIniPeriodoCadastro.SetFocus;
end;

procedure TFFiltroComissoes.cdsGridPaiPadraoCalcFields(DataSet: TDataSet);
var
  qryAuxG, qryAuxS: TFDQuery;
begin
  qryAuxG := dmGerencial.CriarFDQuery(nil, vgConGER);
  qryAuxS := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  //Funcionario
  with qryAuxG, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_FUNCIONARIO, UPPER(NOME_FUNCIONARIO) AS NOME_FUNCIONARIO ' +
            '  FROM FUNCIONARIO ' +
            ' WHERE ID_FUNCIONARIO = :ID_FUNCIONARIO';
    Params.ParamByName('ID_FUNCIONARIO').AsInteger := cdsGridPaiPadrao.FieldByName('ID_FUNCIONARIO_FK').AsInteger;
    Open;
  end;

  if qryAuxG.RecordCount > 0 then
    cdsGridPaiPadrao.FieldByName('NOMEFUNCIONARIO').AsString := qryAuxG.FieldByName('NOME_FUNCIONARIO').AsString;

  //Item
  with qryAuxS, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_ITEM, UPPER(DESCR_ITEM) AS DESCR_ITEM ' +
            '  FROM ITEM ' +
            ' WHERE ID_ITEM = :ID_ITEM';
    Params.ParamByName('ID_ITEM').AsInteger := cdsGridPaiPadrao.FieldByName('ID_ITEM_FK').AsInteger;
    Open;
  end;

  if qryAuxS.RecordCount > 0 then
    cdsGridPaiPadrao.FieldByName('DESCR_ITEM').AsString := qryAuxS.FieldByName('DESCR_ITEM').AsString;

  FreeAndNil(qryAuxG);
  FreeAndNil(qryAuxS);
end;

procedure TFFiltroComissoes.cdsGridPaiPadraoPERCENT_COMISSAOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFFiltroComissoes.cdsGridPaiPadraoVR_COMISSAOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFFiltroComissoes.dbgGridPaiPadraoDblClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao   := C;
  vgIdConsulta := cdsGridPaiPadrao.FieldByName('ID').AsInteger;

  dmGerencial.CriarForm(TFCadastroComissao, FCadastroComissao);

  DefinirPosicaoGrid;
end;

procedure TFFiltroComissoes.dbgGridPaiPadraoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Grid: TDBGrid;
begin
  inherited;

  Grid := Sender as TDBGrid;

  if Grid.DataSource.DataSet.FieldByName('FLG_CANCELADO').AsString = 'S' then
  begin
    Grid.Canvas.Font.Color := clGray;
    Grid.Canvas.FillRect(Rect);
    Grid.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TFFiltroComissoes.dbgGridPaiPadraoTitleClick(Column: TColumn);
begin
  inherited;

  cdsGridPaiPadrao.IndexFieldNames := Column.FieldName;
end;

procedure TFFiltroComissoes.dteFimPeriodoCadastroKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
     if dteFimPeriodoCadastro.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteFimPeriodoCadastro.CanFocus then
        dteFimPeriodoCadastro.SetFocus;
    end
    else if dteIniPeriodoCadastro.Date > dteFimPeriodoCadastro.Date then
    begin
      Application.MessageBox('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso', MB_OK + MB_ICONWARNING);

      dteFimPeriodoCadastro.Clear;

      if dteFimPeriodoCadastro.CanFocus then
        dteFimPeriodoCadastro.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dteFimPeriodoCadastro.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteFimPeriodoCadastro.CanFocus then
        dteFimPeriodoCadastro.SetFocus;
    end
    else if dteIniPeriodoCadastro.Date > dteFimPeriodoCadastro.Date then
    begin
      Application.MessageBox('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso', MB_OK + MB_ICONWARNING);

      dteFimPeriodoCadastro.Clear;

      if dteFimPeriodoCadastro.CanFocus then
        dteFimPeriodoCadastro.SetFocus;
    end
    else
      btnFiltrar.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroComissoes.dteIniPeriodoCadastroKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
     if dteIniPeriodoCadastro.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteIniPeriodoCadastro.CanFocus then
        dteIniPeriodoCadastro.SetFocus;
    end
    else if dteIniPeriodoCadastro.Date > dteFimPeriodoCadastro.Date then
    begin
      Application.MessageBox('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso', MB_OK + MB_ICONWARNING);

      dteIniPeriodoCadastro.Clear;

      if dteIniPeriodoCadastro.CanFocus then
        dteIniPeriodoCadastro.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dteIniPeriodoCadastro.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteIniPeriodoCadastro.CanFocus then
        dteIniPeriodoCadastro.SetFocus;
    end
    else if dteIniPeriodoCadastro.Date > dteFimPeriodoCadastro.Date then
    begin
      Application.MessageBox('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso', MB_OK + MB_ICONWARNING);

      dteIniPeriodoCadastro.Clear;

      if dteIniPeriodoCadastro.CanFocus then
        dteIniPeriodoCadastro.SetFocus;
    end
    else
      btnFiltrar.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroComissoes.FormCreate(Sender: TObject);
begin
  inherited;

  qryFuncionarios.Close;
  qryFuncionarios.Open;

  dteIniPeriodoCadastro.Date := StartOfTheMonth(Date);
  dteFimPeriodoCadastro.Date := EndOfTheMonth(Date);

  btnFiltrar.Click;
end;

procedure TFFiltroComissoes.FormShow(Sender: TObject);
begin
  inherited;

  btnImprimir.Visible := False;

  ShowScrollBar(dbgGridPaiPadrao.Handle, SB_HORZ, False);

  if dteIniPeriodoCadastro.CanFocus then
    dteIniPeriodoCadastro.SetFocus;
end;

procedure TFFiltroComissoes.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    X:  //EXCLUSAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da exclus�o desse Fechamento de Comiss�es:', '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          cdsGridPaiPadrao.FieldByName('ID').AsInteger,
                          'COMISSAO');
    end;
    P:  //IMPRESSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente impress�o de Dados de Fechamento de Comiss�es',
                          cdsGridPaiPadrao.FieldByName('ID').AsInteger,
                          'COMISSAO');
    end;
    T:  //EXPORTACAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente exporta��o de Dados de Fechamento de Comiss�es',
                          cdsGridPaiPadrao.FieldByName('ID').AsInteger,
                          'COMISSAO');
    end;
  end;
end;

procedure TFFiltroComissoes.lcbNomeFuncionarioKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

function TFFiltroComissoes.PodeExcluir(var Msg: String): Boolean;
begin
  Result := True;

  if not cdsGridPaiPadrao.FieldByName('DATA_PAGAMENTO').IsNull then
    msg := 'N�o � poss�vel excluir a Comiss�o informada, pois a mesma j� foi paga.'
  else if cdsGridPaiPadrao.FieldByName('FLG_CANCELADO').AsString = 'S' then
    msg := 'N�o � poss�vel excluir a Comiss�o informada, pois a mesma j� foi cancelada.';

  Result := cdsGridPaiPadrao.FieldByName('DATA_PAGAMENTO').IsNull and
            (cdsGridPaiPadrao.FieldByName('FLG_CANCELADO').AsString = 'N') and
            (cdsGridPaiPadrao.RecordCount > 0);
end;

procedure TFFiltroComissoes.rgSituacaoValeExit(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroComissoes.VerificarPermissoes;
begin
  inherited;

  btnIncluir.Enabled  := vgPrm_RealFecCom;
  btnEditar.Enabled   := vgPrm_RealFecCom and (cdsGridPaiPadrao.RecordCount > 0);
  btnExcluir.Enabled  := vgPrm_RealFecCom and (cdsGridPaiPadrao.RecordCount > 0);
  btnImprimir.Enabled := vgPrm_ImpFecCom and (cdsGridPaiPadrao.RecordCount > 0);
end;

end.
