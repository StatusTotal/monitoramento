{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroOficioOrgao.pas
  Descricao:   Formulario de cadastro de Orgao do Oficio
  Author   :   Cristina
  Date:        23-jan-2017
  Last Update: 23-out-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroOficioOrgao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Mask, Vcl.DBCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Datasnap.DBClient,
  Datasnap.Provider, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFCadastroOficioOrgao = class(TFCadastroGeralPadrao)
    lblNome: TLabel;
    edtNome: TDBEdit;
    qryOfcOrgao: TFDQuery;
    dspOfcOrgao: TDataSetProvider;
    cdsOfcOrgao: TClientDataSet;
    cdsOfcOrgaoID_OFICIO_ORGAO: TIntegerField;
    cdsOfcOrgaoNOME_OFICIO_ORGAO: TStringField;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure edtNomeKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }

    IdNovoOrgao: Integer;
  end;

var
  FCadastroOficioOrgao: TFCadastroOficioOrgao;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

{ TFCadastroOficioOrgao }

procedure TFCadastroOficioOrgao.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroOficioOrgao.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroOficioOrgao.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtNome.MaxLength := 250;
end;

procedure TFCadastroOficioOrgao.DesabilitarComponentes;
begin
  inherited;

  btnDadosPrincipais.Visible := False;
end;

procedure TFCadastroOficioOrgao.edtNomeKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if Trim(edtNome.Text) <> '' then
      btnOk.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroOficioOrgao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  if lPodeCancelar then
  begin
    vgOperacao     := VAZIO;
    vgIdConsulta   := 0;
    vgOrigemFiltro := False;
  end
  else
    Action := caNone;
end;

procedure TFCadastroOficioOrgao.FormCreate(Sender: TObject);
begin
  inherited;

  IdNovoOrgao := 0;
end;

procedure TFCadastroOficioOrgao.FormShow(Sender: TObject);
begin
  inherited;

  cdsOfcOrgao.Close;
  cdsOfcOrgao.Params.ParamByName('ID_OFICIO_ORGAO').Value := vgIdConsulta;
  cdsOfcOrgao.Open;

  if vgOperacao = I then
    cdsOfcOrgao.Append;

  if vgOperacao = E then
    cdsOfcOrgao.Edit;

  if vgOperacao = C then
    pnlDados.Enabled := False;

  if edtNome.CanFocus then
    edtNome.SetFocus;
end;

function TFCadastroOficioOrgao.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroOficioOrgao.GravarDadosGenerico(var Msg: String): Boolean;
begin
  Result := True;
  Msg := '';

  if vgOperacao = C then
    Exit;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    if cdsOfcOrgao.State in [dsInsert, dsEdit] then
    begin
      if vgOperacao = I then
      begin
        cdsOfcOrgao.FieldByName('ID_OFICIO_ORGAO').AsInteger := BS.ProximoId('ID_OFICIO_ORGAO', 'OFICIO_ORGAO');

        if vgOrigemCadastro then
          IdNovoOrgao := cdsOfcOrgao.FieldByName('ID_OFICIO_ORGAO').AsInteger;
      end;

      cdsOfcOrgao.Post;
      cdsOfcOrgao.ApplyUpdates(0);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    Msg := '�rg�o de Of�cio gravado com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na grava��o do �rg�o de Of�cio.';
  end;
end;

procedure TFCadastroOficioOrgao.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(3, '', 'Somente consulta de �rg�o de Of�cio',
                          vgIdConsulta, 'OFICIO_ORGAO');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(3, '', 'Somente inclus�o de �rg�o de Of�cio',
                          vgIdConsulta, 'OFICIO_ORGAO');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO',
                                  'Por favor, indique o motivo da edi��o desse �rg�o de Of�cio:',
                                  '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de �rg�o de Of�cio';

      BS.GravarUsuarioLog(3, '', sObservacao,
                          vgIdConsulta, 'OFICIO_ORGAO');
    end;
  end;
end;

procedure TFCadastroOficioOrgao.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroOficioOrgao.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

function TFCadastroOficioOrgao.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if Trim(edtNome.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher o NOME DO �RG�O DO OF�CIO.';
    DadosMsgErro.Componente := edtNome;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroOficioOrgao.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

end.
