inherited FOpcoesInclusaoOficio: TFOpcoesInclusaoOficio
  Caption = 'FOpcoesInclusaoOficio'
  ClientWidth = 299
  OnShow = FormShow
  ExplicitWidth = 299
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlOpcoes: TPanel
    Width = 293
    ExplicitWidth = 293
    inherited pnlBotoes: TPanel
      Width = 287
      ExplicitWidth = 287
      inherited btnOpcao2: TJvTransparentButton
        Action = actIncluirRespostaEnvio
        Font.Color = clNavy
        Spacing = 4
        WordWrap = True
        Images.ActiveImage = dmGerencial.Im32
        Images.ActiveIndex = 36
        Images.DisabledImage = dmGerencial.Im32Dis
        Images.DisabledIndex = 36
        ExplicitLeft = 64
        ExplicitHeight = 72
      end
      inherited btnOpcao1: TJvTransparentButton
        Action = actIncluirNovoEnvio
        Font.Color = clNavy
        Spacing = 4
        WordWrap = True
        Images.ActiveImage = dmGerencial.Im32
        Images.ActiveIndex = 28
        Images.DisabledImage = dmGerencial.Im32Dis
        Images.DisabledIndex = 28
      end
      object btnOpcao4: TJvTransparentButton
        Left = 216
        Top = 0
        Width = 72
        Height = 72
        Action = actIncluirRespostaRecebimento
        Align = alLeft
        BorderWidth = 0
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 23526
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        Spacing = 4
        TextAlign = ttaBottom
        Transparent = False
        WordWrap = True
        NumGlyphs = 2
        Images.ActiveImage = dmGerencial.Im32
        Images.ActiveIndex = 36
        Images.DisabledImage = dmGerencial.Im32Dis
        Images.DisabledIndex = 36
        ExplicitLeft = 222
      end
      object btnOpcao3: TJvTransparentButton
        Left = 144
        Top = 0
        Width = 72
        Height = 72
        Action = actIncluirNovoRecebimento
        Align = alLeft
        BorderWidth = 0
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 23526
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        Spacing = 4
        TextAlign = ttaBottom
        Transparent = False
        WordWrap = True
        NumGlyphs = 2
        Images.ActiveImage = dmGerencial.Im32
        Images.ActiveIndex = 29
        Images.DisabledImage = dmGerencial.Im32Dis
        Images.DisabledIndex = 29
        ExplicitLeft = 0
        ExplicitHeight = 451
      end
    end
  end
  object acmBotoes: TActionManager
    DisabledImages = dmGerencial.Im32
    Images = dmGerencial.Im32
    Left = 134
    Top = 22
    StyleName = 'Platform Default'
    object actIncluirNovoRecebimento: TAction
      Category = 'Recebimento'
      Caption = '&Receber Novo Of'#237'cio'
      Hint = 'Cadastro de novo Of'#237'cio recebido por esta Serventia'
      ImageIndex = 29
      OnExecute = actIncluirNovoRecebimentoExecute
    end
    object actIncluirNovoEnvio: TAction
      Category = 'Envio'
      Caption = '&Enviar Novo Of'#237'cio'
      Hint = 'Cadastro de novo Of'#237'cio a ser enviado por esta Serventia'
      ImageIndex = 28
      OnExecute = actIncluirNovoEnvioExecute
    end
    object actIncluirRespostaEnvio: TAction
      Category = 'Envio'
      Caption = 'Enviar Resposta'
      Hint = 
        'Cadastro de Of'#237'cio a ser enviado como resposta a Of'#237'cio recebido' +
        ' previamente por esta Serventia.'
      ImageIndex = 36
      OnExecute = actIncluirRespostaEnvioExecute
    end
    object actIncluirRespostaRecebimento: TAction
      Category = 'Recebimento'
      Caption = 'Receber Resposta'
      Hint = 
        'Cadastro de Of'#237'cio recebido como resposta a Of'#237'cio enviado anter' +
        'iormente por esta Serventia'
      ImageIndex = 36
      OnExecute = actIncluirRespostaRecebimentoExecute
    end
  end
end
