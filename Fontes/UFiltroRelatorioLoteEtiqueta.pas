{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroRelatorioLoteEtiqueta.pas
  Descricao:   Filtro de Relatorio de Lotes de Etiquetas de Seguranca
  Author   :   Pedro
  Date:        19-jun-2017
  Last Update: 11-out-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroRelatorioLoteEtiqueta;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils,System.DateUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroRelatorioPadrao, Vcl.StdCtrls,
  JvExControls, JvButton, JvTransparentButton, Vcl.ExtCtrls, sGroupBox,
  UDMRelatorios, Vcl.Mask, sMaskEdit, sCustomComboEdit, sToolEdit, UDM,
  UBibliotecaSistema, UGDM;

type
  TFFiltroRelatorioLoteEtiqueta = class(TFFiltroRelatorioPadrao)
    rgstatus: TsRadioGroup;
    dtInicial: TsDateEdit;
    dtFinal: TsDateEdit;
    procedure btnVisualizarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    procedure GravarLog; override;
    procedure GerarRelatorio(ImpDet: Boolean); override;
    function  VerificarFiltro: Boolean; override;
  private
    { Private declarations }
    lPodeCalcular:Boolean;
  public
    { Public declarations }
  end;

var
  FFiltroRelatorioLoteEtiqueta: TFFiltroRelatorioLoteEtiqueta;

implementation

{$R *.dfm}

uses UVariaveisGlobais;

procedure TFFiltroRelatorioLoteEtiqueta.btnLimparClick(Sender: TObject);
begin
  inherited;

  dtInicial.Date :=StartOfTheMonth(Date);
  dtFinal.Date:=EndOfTheMonth(Date);
  rgstatus.ItemIndex :=0;

  chbExportarPDF.Checked := False;

  chbExportarExcel.Enabled := vgPrm_ExpLtEtq;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;
end;

procedure TFFiltroRelatorioLoteEtiqueta.btnVisualizarClick(Sender: TObject);
begin
  inherited;

  if not VerificarFiltro then
    Exit;

  dmRelatorios.FDLoteFolha.Close;

  case rgstatus.ItemIndex of
    0: dmRelatorios.FDLoteEtiqueta.Params.ParamByName('ST').Value :='*';
    1: dmRelatorios.FDLoteEtiqueta.Params.ParamByName('ST').Value :='N';
    2: dmRelatorios.FDLoteEtiqueta.Params.ParamByName('ST').Value :='U';
    3: dmRelatorios.FDLoteEtiqueta.Params.ParamByName('ST').Value :='C';
  end;

  if (dtInicial.Date<> 0) and (dtFinal.Date <> 0) then
  begin
    dmRelatorios.FDLoteEtiqueta.Params.ParamByName('D1').Value := dtInicial.Date;
    dmRelatorios.FDLoteEtiqueta.Params.ParamByName('D2').Value := dtFinal.Date;
  end
  else
  begin
    dmRelatorios.FDLoteEtiqueta.Params.ParamByName('D1').Value := '12/30/1899';
    dmRelatorios.FDLoteEtiqueta.Params.ParamByName('D2').Value := '12/30/1899';
  end;

  dmRelatorios.FDLoteEtiqueta.Open;

  if not dmRelatorios.FDLoteEtiqueta.IsEmpty then
  begin
    if chbTipoSintetico.Checked then
       GerarRelatorio(False);

    if chbTipoAnalitico.Checked then
       GerarRelatorio(True);
  end;

end;

procedure TFFiltroRelatorioLoteEtiqueta.FormCreate(Sender: TObject);
begin
  inherited;

  Self.Caption := 'Relat�rio de Lote de Etiqueta de Seguran�a';

  dtInicial.Date :=StartOfTheMonth(Date);
  dtFinal.Date:=EndOfTheMonth(Date);
  rgstatus.ItemIndex :=0;
end;

procedure TFFiltroRelatorioLoteEtiqueta.FormShow(Sender: TObject);
begin
  inherited;

  chbExportarExcel.Enabled := vgPrm_ExpLtEtq;

  if chbExportarExcel.Enabled then
    chbExportarExcel.Checked := False;
end;

procedure TFFiltroRelatorioLoteEtiqueta.GerarRelatorio(ImpDet: Boolean);
begin
  inherited;
    try
    sTituloRelatorio    := 'LOTE DE ETIQUETA DE SEGURAN�A';
    sSubtituloRelatorio := 'Per�odo: ' + DateToStr(dtInicial.Date)+' at� '+DateToStr(dtFinal.Date);

    if ImpDet then
    begin
      lExibeDetalhe := True;
      dmRelatorios.DefinirRelatorio(R102DET);
    end
    else
    begin
      lExibeDetalhe := False;
      dmRelatorios.DefinirRelatorio(R102);
    end;

    lPodeCalcular := True;
    dmRelatorios.ImprimirRelatorio(chbExportarPDF.Checked, chbExportarExcel.Checked);

    GravarLog;
  except
    Application.MessageBox('Erro ao gerar Lote de Etiqueta de Seguran�a.',
                           'Erro',
                           MB_OK + MB_ICONERROR);
  end;

end;

procedure TFFiltroRelatorioLoteEtiqueta.GravarLog;
begin
  inherited;
  BS.GravarUsuarioLog(5,
                      '',
                      'Impress�o de Relatorio de Lote de Etiqueta de Seguran�a (' + DateToStr(dDataRelatorio) +
                                                                     ' ' +
                                                                     TimeToStr(tHoraRelatorio) + ')',
                      0,
                      '');
end;

function TFFiltroRelatorioLoteEtiqueta.VerificarFiltro: Boolean;
var
  Msg: String;
begin
  Result := True;

  if dtInicial.Date = 0 then
  begin
    Msg := '- Informe a DATA DE IN�CIO do Per�odo.';
    Result := False;
  end;

  if dtFinal.Date = 0 then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe a DATA DE FIM do Per�odo.'
    else
      Msg := #13#10 + '- Informe a DATA DE FIM do Per�odo.';

    Result := False;
  end;

  if dtFinal.Date < dtInicial.Date then
  begin
    if Trim(Msg) = '' then
      Msg := '- A DATA DE FIM do Per�odo n�o pode inferior � DATA DE IN�CIO do mesmo.'
    else
      Msg := #13#10 + '- A DATA DE FIM do Per�odo n�o pode ser inferior � DATA DE IN�CIO do mesmo.';

    Result := False;
  end;

  if (not chbTipoAnalitico.Checked) and
    (not chbTipoSintetico.Checked) then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe o TIPO do Relat�rio de Lotes de Etiquetas.'
    else
      Msg := #13#10 + '- Informe o TIPO do Relat�rio de Lotes de Etiquetas.';

    Result := False;
  end;

  if not Result then
    Application.MessageBox(PChar(':: ATEN��O ::' + #13#10 + #13#10 + Msg),
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
end;

end.
