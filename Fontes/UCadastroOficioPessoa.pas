{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroOficioPessoa.pas
  Descricao:   Formulario de cadastro de Remetentes e Destinatarios do Oficio
  Author   :   Cristina
  Date:        23-jan-2017
  Last Update: 23-out-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroOficioPessoa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Mask, Vcl.DBCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Datasnap.DBClient,
  Datasnap.Provider, FireDAC.Comp.DataSet, FireDAC.Comp.Client, UDM, System.StrUtils;

type
  TFCadastroOficioPessoa = class(TFCadastroGeralPadrao)
    lblNome: TLabel;
    edtNome: TDBEdit;
    qryOfcPes: TFDQuery;
    dspOfcPes: TDataSetProvider;
    cdsOfcPes: TClientDataSet;
    cdsOfcPesID_OFICIO_PESSOA: TIntegerField;
    cdsOfcPesNOME_OFICIO_PESSOA: TStringField;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure edtNomeKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }

    IdNovaPessoa: Integer;
  end;

var
  FCadastroOficioPessoa: TFCadastroOficioPessoa;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UGDM, UVariaveisGlobais;

{ TFCadastroOficioPessoa }

procedure TFCadastroOficioPessoa.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroOficioPessoa.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroOficioPessoa.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtNome.MaxLength := 250;
end;

procedure TFCadastroOficioPessoa.DesabilitarComponentes;
begin
  inherited;

  btnDadosPrincipais.Visible := False;
end;

procedure TFCadastroOficioPessoa.edtNomeKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if Trim(edtNome.Text) <> '' then
      btnOk.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroOficioPessoa.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  if lPodeCancelar then
  begin
    vgOperacao     := VAZIO;
    vgIdConsulta   := 0;
    vgOrigemFiltro := False;
  end
  else
    Action := caNone;
end;

procedure TFCadastroOficioPessoa.FormCreate(Sender: TObject);
begin
  inherited;

  IdNovaPessoa := 0;
end;

procedure TFCadastroOficioPessoa.FormShow(Sender: TObject);
begin
  inherited;

  cdsOfcPes.Close;
  cdsOfcPes.Params.ParamByName('ID_OFICIO_PESSOA').Value := vgIdConsulta;
  cdsOfcPes.Open;

  if vgOperacao = I then
    cdsOfcPes.Append;

  if vgOperacao = E then
    cdsOfcPes.Edit;

  if vgOperacao = C then
    pnlDados.Enabled := False;

  if vgOfc_TpOfc = 'E' then
    Self.Caption    := 'Destinat�rio do Of�cio'
  else if vgOfc_TpOfc = 'R' then
    Self.Caption    := 'Remetente do Of�cio';

  if edtNome.CanFocus then
    edtNome.SetFocus;
end;

function TFCadastroOficioPessoa.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroOficioPessoa.GravarDadosGenerico(var Msg: String): Boolean;
begin
  Result := True;
  Msg := '';

  if vgOperacao = C then
    Exit;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    if cdsOfcPes.State in [dsInsert, dsEdit] then
    begin
      if vgOperacao = I then
      begin
        cdsOfcPes.FieldByName('ID_OFICIO_PESSOA').AsInteger := BS.ProximoId('ID_OFICIO_PESSOA', 'OFICIO_PESSOA');

        if vgOrigemCadastro then
          IdNovaPessoa := cdsOfcPes.FieldByName('ID_OFICIO_PESSOA').AsInteger;
      end;

      cdsOfcPes.Post;
      cdsOfcPes.ApplyUpdates(0);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    Msg := IfThen((vgOfc_TpOfc = 'E'),
                  'Destinat�rio',
                  'Remetente') +
           ' gravado com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na grava��o do ' +
           IfThen((vgOfc_TpOfc = 'E'),
                  'Destinat�rio',
                  'Remetente') +
           '.';
  end;
end;

procedure TFCadastroOficioPessoa.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(3, '', 'Somente consulta de ' + IfThen((vgOfc_TpOfc = 'E'),
                                                                 'Destinat�rio',
                                                                 'Remetente'),
                          vgIdConsulta, 'OFICIO_PESSOA');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(3, '', 'Somente inclus�o de ' + IfThen((vgOfc_TpOfc = 'E'),
                                                                 'Destinat�rio',
                                                                 'Remetente'),
                          vgIdConsulta, 'OFICIO_PESSOA');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO',
                                  'Por favor, indique o motivo da edi��o desse ' +
                                  IfThen((vgOfc_TpOfc = 'E'),
                                         'Destinat�rio',
                                         'Remetente') + ':',
                                  '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de ' + IfThen((vgOfc_TpOfc = 'E'),
                                                      'Destinat�rio',
                                                      'Remetente');

      BS.GravarUsuarioLog(3, '', sObservacao,
                          vgIdConsulta, 'OFICIO_PESSOA');
    end;
  end;
end;

procedure TFCadastroOficioPessoa.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroOficioPessoa.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

function TFCadastroOficioPessoa.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if Trim(edtNome.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros) + ') Falta preencher o NOME DO ' +
                               IfThen((vgOfc_TpOfc = 'E'),
                                      'DESTINAT�RIO',
                                      'REMETENTE') +
                               ' DO OF�CIO.';
    DadosMsgErro.Componente := edtNome;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroOficioPessoa.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
  //
end;

end.
