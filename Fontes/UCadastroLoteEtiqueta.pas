{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroLoteEtiqueta.pas
  Descricao:   Formulario de cadastro de Lote de Etiquetas de Segunranca
  Author   :   Pedro
  Date:        20-jun-2017
  Last Update: 23-out-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroLoteEtiqueta;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Mask, sMaskEdit, sCustomComboEdit, sToolEdit, sDBDateEdit, Vcl.DBCtrls,
  sDBEdit, sGroupBox, sDBComboBox, sDBLookupComboBox, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async,
  FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client, sComboBox;

type
  TFCadastroLoteEtiqueta = class(TFCadastroGeralPadrao)
    edLote: TsDBEdit;
    edLetra: TsDBEdit;
    edInicial: TsDBEdit;
    edQuantidade: TsDBEdit;
    edFinal: TsDBEdit;
    sDBDateEdit1: TsDBDateEdit;
    gbDetalhes: TsGroupBox;
    lblNatureza: TLabel;
    lblNomeFuncionario: TLabel;
    btnIncluirNatureza: TJvTransparentButton;
    lcbNatureza: TDBLookupComboBox;
    lcbNomeFuncionario: TDBLookupComboBox;
    dtUtilizacao: TsDateEdit;
    lcbSistema: TDBLookupComboBox;
    lblSistema: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edQuantidadeExit(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure gbDetalhesCheckBoxChanged(Sender: TObject);
    procedure btnIncluirNaturezaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lcbSistemaClick(Sender: TObject);
    procedure lcbSistemaExit(Sender: TObject);
    procedure lcbNomeFuncionarioKeyPress(Sender: TObject; var Key: Char);
    procedure lbMensagemErroDblClick(Sender: TObject);
   protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }

    procedure AbrirListaNatureza;
    procedure HabDesabBotoes(Hab:Boolean);
  public
    { Public declarations }
  end;

var
  FCadastroLoteEtiqueta: TFCadastroLoteEtiqueta;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais,
  UCadastroNaturezaAto, UDMEtiqueta;

procedure TFCadastroLoteEtiqueta.AbrirListaNatureza;
begin
  dmEtiqueta.qryNatAto.Close;

  if Trim(lcbSistema.Text) = '' then
  begin
    dmEtiqueta.qryNatAto.Params.ParamByName('ID_SIST01').Value := 0;
    dmEtiqueta.qryNatAto.Params.ParamByName('ID_SIST02').Value := 0;
  end
  else
  begin
    dmEtiqueta.qryNatAto.Params.ParamByName('ID_SIST01').Value := dmEtiqueta.qrySistema.FieldByName('ID_SISTEMA').AsInteger;
    dmEtiqueta.qryNatAto.Params.ParamByName('ID_SIST02').Value := dmEtiqueta.qrySistema.FieldByName('ID_SISTEMA').AsInteger;
  end;

  dmEtiqueta.qryNatAto.Open;
end;

procedure TFCadastroLoteEtiqueta.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroLoteEtiqueta.btnIncluirNaturezaClick(Sender: TObject);
var
  Op: TOperacao;
  IdCons: Integer;
  OrigF, OrigC: Boolean;
begin
  inherited;

  Op     := vgOperacao;
  IdCons := vgIdConsulta;
  OrigF  := vgOrigemFiltro;
  OrigC  := vgOrigemCadastro;

  vgOperacao       := I;
  vgIdConsulta     := 0;
  vgOrigemFiltro   := False;
  vgOrigemCadastro := False;

  try
    Application.CreateForm(TFCadastroNaturezaAto, FCadastroNaturezaAto);

    FCadastroNaturezaAto.iOrigem := 5;

    if lcbSistema.KeyValue > -1 then
      FCadastroNaturezaAto.iIdSistema := lcbSistema.KeyValue;

    FCadastroNaturezaAto.ShowModal;
  finally
    AbrirListaNatureza;

    if FCadastroNaturezaAto.IdNovaNatureza <> 0 then
      lcbNatureza.KeyValue := IntToStr(FCadastroNaturezaAto.IdNovaNatureza);

    FCadastroNaturezaAto.Free;
  end;

  vgOperacao       := Op;
  vgIdConsulta     := IdCons;
  vgOrigemFiltro   := OrigF;
  vgOrigemCadastro := OrigC;

end;

procedure TFCadastroLoteEtiqueta.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroLoteEtiqueta.DefinirTamanhoMaxCampos;
begin
  inherited;
//
end;

procedure TFCadastroLoteEtiqueta.DesabilitarComponentes;
begin
  inherited;

  if vgOperacao = C then
    pnlDados.Enabled := False;
end;

procedure TFCadastroLoteEtiqueta.edQuantidadeExit(Sender: TObject);
var
  iNumIni, iNumFin, iQtd:Integer;
begin
  iNumIni := dmGerencial.PegarNumero(edInicial.Text);
  iQtd := dmGerencial.PegarNumero(edQuantidade.Text);

  iNumFin := iNumIni + iQtd - 1;

  dmEtiqueta.cdsLoteEtqNUM_FINAL.AsString := dmGerencial.Zeros(IntToStr(iNumFin), 'D', Length(edInicial.Text));

  inherited;
end;

procedure TFCadastroLoteEtiqueta.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  if lPodeCancelar then
  begin
    dmEtiqueta.cdsLoteEtq.Cancel;

    vgOperacao     := VAZIO;
    vgIdConsulta   := 0;
    vgOrigemFiltro := False;
  end
  else
    Action := caNone;
end;

procedure TFCadastroLoteEtiqueta.FormCreate(Sender: TObject);
begin
  inherited;

  dmEtiqueta.qrySistema.Close;
  dmEtiqueta.qrySistema.Open;

  dmEtiqueta.qryFuncionarios.Close;
  dmEtiqueta.qryFuncionarios.Open;

  AbrirListaNatureza;

  dmEtiqueta.cdsLoteEtq.Close;
  dmEtiqueta.cdsLoteEtq.Params.ParamByName('ID_LOTEETIQUETA').AsInteger := vgIdConsulta;
  dmEtiqueta.cdsLoteEtq.Open;

  if vgOperacao = I then
  begin
    dmEtiqueta.cdsLoteEtq.Append;
    dmEtiqueta.cdsLoteEtq.FieldByName('FLG_SITUACAO').AsString    := 'N';
    dmEtiqueta.cdsLoteEtq.FieldByName('CAD_ID_USUARIO').AsInteger := vgUsu_Id;
  end;

  if vgOperacao = E then
    dmEtiqueta.cdsLoteEtq.Edit;

  gbDetalhes.Checked:=False;
  gbDetalhesCheckBoxChanged(Sender);
end;

procedure TFCadastroLoteEtiqueta.FormShow(Sender: TObject);
begin
  inherited;

  if edLote.CanFocus then
    edLote.SetFocus;
end;

procedure TFCadastroLoteEtiqueta.gbDetalhesCheckBoxChanged(
  Sender: TObject);
begin
  inherited;
  if gbDetalhes.Checked then
  begin
    dtUtilizacao.Enabled       := True;
    lcbSistema.Enabled         := True;
    lcbNatureza.Enabled        := True;
    lcbNomeFuncionario.Enabled := True;
    btnIncluirNatureza.Enabled := True;

    dtUtilizacao.Color       := clWindow;
    lcbSistema.Color         := clWindow;
    lcbNatureza.Color        := clWindow;
    lcbNomeFuncionario.Color := clWindow;
  end
  else
  begin
    dtUtilizacao.Enabled       := False;
    lcbSistema.Enabled         := False;
    lcbNatureza.Enabled        := False;
    lcbNomeFuncionario.Enabled := False;
    btnIncluirNatureza.Enabled := False;

    dtUtilizacao.Color       := clSilver;
    lcbSistema.Color         := clSilver;
    lcbNatureza.Color        := clSilver;
    lcbNomeFuncionario.Color := clSilver;

    dtUtilizacao.Clear;
    lcbSistema.KeyValue         := -1;
    lcbNatureza.KeyValue        := 0;
    lcbNomeFuncionario.KeyValue := 0;
  end;
end;

function TFCadastroLoteEtiqueta.GravarDadosAto(var Msg: String): Boolean;
begin
//
end;

function TFCadastroLoteEtiqueta.GravarDadosGenerico(var Msg: String): Boolean;
var
  nInicial, nFinal, nCount, nQtd,
  iIdLtEtq: Integer;
begin
  Result := True;
  Msg := '';

  iIdLtEtq := 0;

  if vgOperacao = C then
    Exit;

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    HabDesabBotoes(False);

    if vgOperacao = I then
    begin
      iIdLtEtq := dmGerencial.ValorGeneratorFD('GEN_LOTEETIQUETA_ID',dmPrincipal.conSISTEMA);
      dmEtiqueta.cdsLoteEtq.FieldByName('ID_LOTEETIQUETA').AsInteger := iIdLtEtq;
    end
    else
      iIdLtEtq := dmEtiqueta.cdsLoteEtq.FieldByName('ID_LOTEETIQUETA').AsInteger;

    dmEtiqueta.cdsLoteEtqFLG_SITUACAO.AsString := dmGerencial.IIF(gbDetalhes.Checked,'U','N');
    dmEtiqueta.cdsLoteEtq.Post;
    dmEtiqueta.cdsLoteEtq.ApplyUpdates(0);

    nQtd := dmGerencial.PegarNumero(edQuantidade.Text);
    nFinal := dmGerencial.PegarNumero(edFinal.Text);

    dmEtiqueta.cdsLoteEtq.Close;
    dmEtiqueta.cdsLoteEtq.Params.ParamByName('ID_LOTEETIQUETA').Value := iIdLtEtq;
    dmEtiqueta.cdsLoteEtq.Open;

    nCount := 1;

    for nInicial := dmGerencial.PegarNumero(edInicial.Text) to nFinal do
    begin
      Application.ProcessMessages;
      dmGerencial.Processando(True,'Aguarde: '+IntToStr(nInicial)+' | '+Inttostr(nFinal));

      dmEtiqueta.qryEtqAux.Close;
      dmEtiqueta.qryEtqAux.ParamByName('ID_LOTEETIQUETA').AsInteger := dmEtiqueta.cdsLoteEtqID_LOTEETIQUETA.AsInteger;
      dmEtiqueta.qryEtqAux.ParamByName('ORDEM').AsInteger           := nCount;
      dmEtiqueta.qryEtqAux.Open;

      if dmEtiqueta.qryEtqAux.IsEmpty then
      begin
        dmEtiqueta.qryEtqAux.Append;
        dmEtiqueta.qryEtqAuxID_ETIQUETA.AsInteger := dmGerencial.ValorGeneratorFD('GEN_ETIQUETA_ID',dmPrincipal.conSISTEMA);
      end
      else
        dmEtiqueta.qryEtqAux.Edit;

      dmEtiqueta.qryEtqAuxID_LOTEETIQUETA_FK.AsInteger := dmEtiqueta.cdsLoteEtqID_LOTEETIQUETA.AsInteger;
      dmEtiqueta.qryEtqAuxORDEM.AsInteger              := nCount;

      dmEtiqueta.qryEtqAuxNUM_ETIQUETA.AsString := dmEtiqueta.cdsLoteEtqNUM_LOTE.AsString +
                                                    dmEtiqueta.cdsLoteEtqLETRA.AsString +
                                                    dmGerencial.Zeros(IntToStr(nInicial), 'D', Length(edInicial.Text));

      if gbDetalhes.Checked then
      begin
        dmEtiqueta.qryEtqAuxID_SISTEMA_FK.AsInteger     := lcbSistema.KeyValue;
        dmEtiqueta.qryEtqAuxDATA_UTILIZACAO.AsDateTime  := dtUtilizacao.Date;
        dmEtiqueta.qryEtqAuxID_NATUREZAATO_FK.AsInteger := lcbNatureza.KeyValue;
        dmEtiqueta.qryEtqAuxID_FUNCIONARIO_FK.AsInteger := lcbNomeFuncionario.KeyValue;
        dmEtiqueta.qryEtqAuxCAD_ID_USUARIO.AsInteger    := vgUsu_Id;
        dmEtiqueta.qryEtqAuxFLG_SITUACAO.AsString       := 'U';
      end
      else
      begin
        dmEtiqueta.qryEtqAuxFLG_SITUACAO.AsString := 'N';
      end;

      dmEtiqueta.qryEtqAux.Post;

      Inc(nCount);
    end;

    dmGerencial.Processando(False);

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    Msg := 'Lote de Etiqueta de Seguran�a ' + edLote.Text + ' gravado com sucesso!';

    HabDesabBotoes(True);
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    HabDesabBotoes(True);

    dmGerencial.Processando(False);

    Result := False;
    Msg := 'Erro na grava��o do Lote de Etiqueta de Seguran�a ' + edLote.Text + '.';
  end;
end;

procedure TFCadastroLoteEtiqueta.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';


  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(5, '', 'Somente consulta de Dados de Lote de Etiqueta de Seguran�a',
                          vgIdConsulta, 'LOTE_ETIQUETA');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(5, '', 'Somente inclus�o de Dados de Lote de Etiqueta de Seguran�a',
                          vgIdConsulta, 'LOTE_ETIQUETA');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da edi��o desse Lote de Folha de Seguran�a:', '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de Lote de Etiqueta de Seguran�a';

      BS.GravarUsuarioLog(5, '', sObservacao,
                          vgIdConsulta, 'LOTE_ETIQUETA');
    end;
  end;
end;

procedure TFCadastroLoteEtiqueta.HabDesabBotoes(Hab: Boolean);
begin
  btnOk.Enabled:=Hab;
  btnCancelar.Enabled:=Hab;
  btnIncluirNatureza.Enabled:=Hab;

  btnIncluirNatureza.Enabled := Hab;
end;

procedure TFCadastroLoteEtiqueta.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroLoteEtiqueta.lcbNomeFuncionarioKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroLoteEtiqueta.lcbSistemaClick(Sender: TObject);
begin
  inherited;

  AbrirListaNatureza;
end;

procedure TFCadastroLoteEtiqueta.lcbSistemaExit(Sender: TObject);
begin
  inherited;

  AbrirListaNatureza;
end;

procedure TFCadastroLoteEtiqueta.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
//
end;

function TFCadastroLoteEtiqueta.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if edLote.Text = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha o campo Numera��o Lote.';
    DadosMsgErro.Componente := edLote;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if edLetra.Text ='' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo Letra.';
    DadosMsgErro.Componente := edletra;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if edInicial.Text ='' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo Numera��o Inicial.';
    DadosMsgErro.Componente := edInicial;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if (edQuantidade.Text = '') or (edQuantidade.Text='0') then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo Quantidade.';
    DadosMsgErro.Componente := edQuantidade;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if gbDetalhes.Checked then
  begin
    if (dtUtilizacao.Date = 0) then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo Data Utiliza��o.';
      DadosMsgErro.Componente := dtUtilizacao;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;

    if (lcbSistema.KeyValue = -1) then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo Sistema.';
      DadosMsgErro.Componente := lcbSistema;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;

    if (lcbNatureza.KeyValue = -1) then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo Natureza.';
      DadosMsgErro.Componente := lcbNatureza;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;

    if (lcbNomeFuncionario.KeyValue = -1) then
    begin
      DadosMsgErro := MsgErro.Create;
      DadosMsgErro.IdMsg      := QtdErros;
      DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Por favor, preencha corretamente o campo Nome do Colaborador.';
      DadosMsgErro.Componente := lcbNomeFuncionario;
      DadosMsgErro.PageIndex  := 0;
      ListaMsgErro.Add(DadosMsgErro);

      Inc(QtdErros);
    end;
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroLoteEtiqueta.VerificarLayoutAto(
  var QtdErros: Integer): Boolean;
begin
//
end;

end.
