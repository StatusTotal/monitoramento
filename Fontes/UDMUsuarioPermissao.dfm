object dmUsuarioPermissao: TdmUsuarioPermissao
  OldCreateOrder = False
  Height = 550
  Width = 782
  object qryFinOrigem: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT P.ID_PERMISSAO,'
      '       P.COD_PERMISSAO,'
      '       P.ID_MODULO_FK,'
      
        '       (P.COD_PERMISSAO || '#39' - '#39' || P.DESCR_PERMISSAO) AS DESCR_' +
        'PERMISSAO,'
      '       UP.COD_PERMISSAO AS COD_PERMISSAO_UP'
      '  FROM PERMISSAO P'
      '  LEFT JOIN USUARIO_PERMISSAO UP'
      '    ON P.COD_PERMISSAO = UP.COD_PERMISSAO'
      '   AND ID_USUARIO = :ID_USUARIO'
      ' WHERE P.ID_MODULO_FK = 2 AND P.COD_PERMISSAO <> 2000')
    Left = 50
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'ID_USUARIO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object qryOficOrigem: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT P.ID_PERMISSAO,'
      '       P.COD_PERMISSAO,'
      '       P.ID_MODULO_FK,'
      
        '       (P.COD_PERMISSAO || '#39' - '#39' || P.DESCR_PERMISSAO) AS DESCR_' +
        'PERMISSAO,'
      '       UP.COD_PERMISSAO AS COD_PERMISSAO_UP'
      '  FROM PERMISSAO P'
      '  LEFT JOIN USUARIO_PERMISSAO UP'
      '    ON P.COD_PERMISSAO = UP.COD_PERMISSAO'
      '   AND ID_USUARIO = :ID_USUARIO'
      ' WHERE P.ID_MODULO_FK = 3 AND P.COD_PERMISSAO <> 3000')
    Left = 152
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'ID_USUARIO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object qryRelOrigem: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT P.ID_PERMISSAO,'
      '       P.COD_PERMISSAO,'
      '       P.ID_MODULO_FK,'
      
        '       (P.COD_PERMISSAO || '#39' - '#39' || P.DESCR_PERMISSAO) AS DESCR_' +
        'PERMISSAO,'
      '       UP.COD_PERMISSAO AS COD_PERMISSAO_UP'
      '  FROM PERMISSAO P'
      '  LEFT JOIN USUARIO_PERMISSAO UP'
      '    ON P.COD_PERMISSAO = UP.COD_PERMISSAO'
      '   AND ID_USUARIO = :ID_USUARIO'
      ' WHERE P.ID_MODULO_FK = 50 AND P.COD_PERMISSAO <> 50000')
    Left = 448
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'ID_USUARIO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object qryConfigOrigem: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT P.ID_PERMISSAO,'
      '       P.COD_PERMISSAO,'
      '       P.ID_MODULO_FK,'
      
        '       (P.COD_PERMISSAO || '#39' - '#39' || P.DESCR_PERMISSAO) AS DESCR_' +
        'PERMISSAO,'
      '       UP.COD_PERMISSAO AS COD_PERMISSAO_UP'
      '  FROM PERMISSAO P'
      '  LEFT JOIN USUARIO_PERMISSAO UP'
      '    ON P.COD_PERMISSAO = UP.COD_PERMISSAO'
      '   AND ID_USUARIO = :ID_USUARIO'
      ' WHERE P.ID_MODULO_FK = 80 AND P.COD_PERMISSAO <> 80000')
    Left = 544
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'ID_USUARIO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspFinOrigem: TDataSetProvider
    DataSet = qryFinOrigem
    Left = 48
    Top = 64
  end
  object dspOficOrigem: TDataSetProvider
    DataSet = qryOficOrigem
    Left = 152
    Top = 64
  end
  object dspRelOrigem: TDataSetProvider
    DataSet = qryRelOrigem
    Left = 448
    Top = 64
  end
  object dspConfigOrigem: TDataSetProvider
    DataSet = qryConfigOrigem
    Left = 544
    Top = 64
  end
  object cdsFinOrigem: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_USUARIO'
        ParamType = ptInput
      end>
    ProviderName = 'dspFinOrigem'
    AfterOpen = cdsFinOrigemAfterOpen
    Left = 48
    Top = 112
    object cdsFinOrigemID_PERMISSAO: TIntegerField
      FieldName = 'ID_PERMISSAO'
      Required = True
    end
    object cdsFinOrigemID_MODULO_FK: TIntegerField
      FieldName = 'ID_MODULO_FK'
    end
    object cdsFinOrigemDESCR_PERMISSAO: TStringField
      FieldName = 'DESCR_PERMISSAO'
      ReadOnly = True
      Size = 264
    end
    object cdsFinOrigemCOD_PERMISSAO: TIntegerField
      FieldName = 'COD_PERMISSAO'
    end
    object cdsFinOrigemCOD_PERMISSAO_UP: TIntegerField
      FieldName = 'COD_PERMISSAO_UP'
      ReadOnly = True
    end
    object cdsFinOrigemSELECIONADO: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'SELECIONADO'
    end
  end
  object cdsOficOrigem: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_USUARIO'
        ParamType = ptInput
      end>
    ProviderName = 'dspOficOrigem'
    AfterOpen = cdsOficOrigemAfterOpen
    Left = 152
    Top = 112
    object cdsOficOrigemID_PERMISSAO: TIntegerField
      FieldName = 'ID_PERMISSAO'
      Required = True
    end
    object cdsOficOrigemID_MODULO_FK: TIntegerField
      FieldName = 'ID_MODULO_FK'
    end
    object cdsOficOrigemDESCR_PERMISSAO: TStringField
      FieldName = 'DESCR_PERMISSAO'
      ReadOnly = True
      Size = 264
    end
    object cdsOficOrigemCOD_PERMISSAO: TIntegerField
      FieldName = 'COD_PERMISSAO'
    end
    object cdsOficOrigemCOD_PERMISSAO_UP: TIntegerField
      FieldName = 'COD_PERMISSAO_UP'
      ReadOnly = True
    end
    object cdsOficOrigemSELECIONADO: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'SELECIONADO'
    end
  end
  object cdsRelOrigem: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_USUARIO'
        ParamType = ptInput
      end>
    ProviderName = 'dspRelOrigem'
    AfterOpen = cdsRelOrigemAfterOpen
    Left = 448
    Top = 112
    object cdsRelOrigemID_PERMISSAO: TIntegerField
      FieldName = 'ID_PERMISSAO'
      Required = True
    end
    object cdsRelOrigemID_MODULO_FK: TIntegerField
      FieldName = 'ID_MODULO_FK'
    end
    object cdsRelOrigemDESCR_PERMISSAO: TStringField
      FieldName = 'DESCR_PERMISSAO'
      ReadOnly = True
      Size = 264
    end
    object cdsRelOrigemCOD_PERMISSAO: TIntegerField
      FieldName = 'COD_PERMISSAO'
    end
    object cdsRelOrigemCOD_PERMISSAO_UP: TIntegerField
      FieldName = 'COD_PERMISSAO_UP'
      ReadOnly = True
    end
    object cdsRelOrigemSELECIONADO: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'SELECIONADO'
    end
  end
  object cdsConfigOrigem: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_USUARIO'
        ParamType = ptInput
      end>
    ProviderName = 'dspConfigOrigem'
    AfterOpen = cdsConfigOrigemAfterOpen
    Left = 544
    Top = 112
    object cdsConfigOrigemID_PERMISSAO: TIntegerField
      FieldName = 'ID_PERMISSAO'
      Required = True
    end
    object cdsConfigOrigemID_MODULO_FK: TIntegerField
      FieldName = 'ID_MODULO_FK'
    end
    object cdsConfigOrigemDESCR_PERMISSAO: TStringField
      FieldName = 'DESCR_PERMISSAO'
      ReadOnly = True
      Size = 264
    end
    object cdsConfigOrigemCOD_PERMISSAO: TIntegerField
      FieldName = 'COD_PERMISSAO'
    end
    object cdsConfigOrigemCOD_PERMISSAO_UP: TIntegerField
      FieldName = 'COD_PERMISSAO_UP'
      ReadOnly = True
    end
    object cdsConfigOrigemSELECIONADO: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'SELECIONADO'
    end
  end
  object dsFinOrigem: TDataSource
    DataSet = cdsFinOrigem
    Left = 48
    Top = 160
  end
  object dsOficOrigem: TDataSource
    DataSet = cdsOficOrigem
    Left = 152
    Top = 160
  end
  object dsRelOrigem: TDataSource
    DataSet = cdsRelOrigem
    Left = 448
    Top = 160
  end
  object dsConfigOrigem: TDataSource
    DataSet = cdsConfigOrigem
    Left = 544
    Top = 160
  end
  object qryFlsOrigem: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT P.ID_PERMISSAO,'
      '       P.COD_PERMISSAO,'
      '       P.ID_MODULO_FK,'
      
        '       (P.COD_PERMISSAO || '#39' - '#39' || P.DESCR_PERMISSAO) AS DESCR_' +
        'PERMISSAO,'
      '       UP.COD_PERMISSAO AS COD_PERMISSAO_UP'
      '  FROM PERMISSAO P'
      '  LEFT JOIN USUARIO_PERMISSAO UP'
      '    ON P.COD_PERMISSAO = UP.COD_PERMISSAO'
      '   AND ID_USUARIO = :ID_USUARIO'
      ' WHERE P.ID_MODULO_FK = 4 AND P.COD_PERMISSAO <> 4000')
    Left = 256
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'ID_USUARIO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object qryEtqOrigem: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT P.ID_PERMISSAO,'
      '       P.COD_PERMISSAO,'
      '       P.ID_MODULO_FK,'
      
        '       (P.COD_PERMISSAO || '#39' - '#39' || P.DESCR_PERMISSAO) AS DESCR_' +
        'PERMISSAO,'
      '       UP.COD_PERMISSAO AS COD_PERMISSAO_UP'
      '  FROM PERMISSAO P'
      '  LEFT JOIN USUARIO_PERMISSAO UP'
      '    ON P.COD_PERMISSAO = UP.COD_PERMISSAO'
      '   AND ID_USUARIO = :ID_USUARIO'
      ' WHERE P.ID_MODULO_FK = 5 AND P.COD_PERMISSAO <> 5000')
    Left = 352
    Top = 16
    ParamData = <
      item
        Position = 1
        Name = 'ID_USUARIO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspFlsOrigem: TDataSetProvider
    DataSet = qryFlsOrigem
    Left = 256
    Top = 64
  end
  object dspEtqOrigem: TDataSetProvider
    DataSet = qryEtqOrigem
    Left = 352
    Top = 64
  end
  object cdsFlsOrigem: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_USUARIO'
        ParamType = ptInput
      end>
    ProviderName = 'dspFlsOrigem'
    AfterOpen = cdsFlsOrigemAfterOpen
    Left = 256
    Top = 112
    object cdsFlsOrigemID_PERMISSAO: TIntegerField
      FieldName = 'ID_PERMISSAO'
      Required = True
    end
    object cdsFlsOrigemCOD_PERMISSAO: TIntegerField
      FieldName = 'COD_PERMISSAO'
    end
    object cdsFlsOrigemID_MODULO_FK: TIntegerField
      FieldName = 'ID_MODULO_FK'
    end
    object cdsFlsOrigemDESCR_PERMISSAO: TStringField
      FieldName = 'DESCR_PERMISSAO'
      ReadOnly = True
      Size = 264
    end
    object cdsFlsOrigemCOD_PERMISSAO_UP: TIntegerField
      FieldName = 'COD_PERMISSAO_UP'
      ReadOnly = True
    end
    object cdsFlsOrigemSELECIONADO: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'SELECIONADO'
    end
  end
  object cdsEtqOrigem: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_USUARIO'
        ParamType = ptInput
      end>
    ProviderName = 'dspEtqOrigem'
    AfterOpen = cdsEtqOrigemAfterOpen
    Left = 352
    Top = 112
    object cdsEtqOrigemID_PERMISSAO: TIntegerField
      FieldName = 'ID_PERMISSAO'
      Required = True
    end
    object cdsEtqOrigemCOD_PERMISSAO: TIntegerField
      FieldName = 'COD_PERMISSAO'
    end
    object cdsEtqOrigemID_MODULO_FK: TIntegerField
      FieldName = 'ID_MODULO_FK'
    end
    object cdsEtqOrigemDESCR_PERMISSAO: TStringField
      FieldName = 'DESCR_PERMISSAO'
      ReadOnly = True
      Size = 264
    end
    object cdsEtqOrigemCOD_PERMISSAO_UP: TIntegerField
      FieldName = 'COD_PERMISSAO_UP'
      ReadOnly = True
    end
    object cdsEtqOrigemSELECIONADO: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'SELECIONADO'
    end
  end
  object dsFlsOrigem: TDataSource
    DataSet = cdsFlsOrigem
    Left = 256
    Top = 160
  end
  object dsEtqOrigem: TDataSource
    DataSet = cdsEtqOrigem
    Left = 352
    Top = 160
  end
end
