inherited FCadastroAberturaCaixa: TFCadastroAberturaCaixa
  Caption = 'FCadastroAberturaCaixa'
  ClientHeight = 380
  ClientWidth = 782
  ExplicitWidth = 782
  ExplicitHeight = 380
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 776
    Height = 374
    ExplicitWidth = 776
    ExplicitHeight = 374
    inherited pnlDados: TPanel
      Width = 662
      Height = 368
      ExplicitWidth = 662
      ExplicitHeight = 368
      object lblTitulo: TLabel [0]
        Left = 0
        Top = 0
        Width = 662
        Height = 16
        Align = alTop
        Alignment = taCenter
        Caption = 'ABERTURA DE CAIXA'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitWidth = 134
      end
      object lblNomeUsuarioResponsavel: TLabel [1]
        Left = 5
        Top = 22
        Width = 109
        Height = 14
        Caption = 'Usu'#225'rio Respons'#225'vel'
      end
      object lblDataAbertura: TLabel [2]
        Left = 146
        Top = 22
        Width = 25
        Height = 14
        Caption = 'Data'
      end
      object lblHoraAbertura: TLabel [3]
        Left = 252
        Top = 22
        Width = 25
        Height = 14
        Caption = 'Hora'
      end
      object lblVlrTotalUltFechamento: TLabel [4]
        Left = 5
        Top = 66
        Width = 85
        Height = 14
        Caption = 'Tot. '#218'lt. Fech.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblVlrTotalAbertura: TLabel [5]
        Left = 5
        Top = 220
        Width = 108
        Height = 14
        Caption = 'Vlr. Tot. Abertura'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblVlrContaCorrente: TLabel [6]
        Left = 5
        Top = 176
        Width = 85
        Height = 14
        Caption = 'Vlr. C. Corrente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblDataCancelamento: TLabel [7]
        Left = 217
        Top = 264
        Width = 98
        Height = 14
        Caption = 'Dt. Cancelamento'
      end
      object mmCupomAbertura: TMemo [8]
        Left = 323
        Top = 22
        Width = 337
        Height = 280
        BorderStyle = bsNone
        Color = clMenu
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Courier New'
        Font.Style = []
        Lines.Strings = (
          '================================================'
          '                ABERTURA DE CAIXA'
          '================================================'
          'Data: 99/99/9999                  Hora: 99:99:99'
          'Usu. Respons'#225'vel: NNNNNNNNNNNNNNNNNNNNNNNNNNNNNN'
          '------------------------------------------------'
          'Descri'#231#227'o                             Valor (R$)'
          '------------------------------------------------'
          'Notas:                              9.999.999,00'
          'Moedas:                             9.999.999,00'
          'Conta Corrente:                     9.999.999,00'
          ''
          'TOTAIS'
          'Abertura                            9.999.999,00'
          ''
          #218'ltimo Fechamento                   9.999.999,00'
          '================================================')
        ParentFont = False
        ReadOnly = True
        TabOrder = 8
      end
      inherited pnlMsgErro: TPanel
        Top = 307
        Width = 662
        TabOrder = 9
        ExplicitTop = 307
        ExplicitWidth = 662
        inherited lblMensagemErro: TLabel
          Width = 662
        end
        inherited lbMensagemErro: TListBox
          Width = 662
          OnDblClick = lbMensagemErroDblClick
          ExplicitWidth = 662
        end
      end
      object dteDataAbertura: TJvDBDateEdit
        Left = 146
        Top = 38
        Width = 100
        Height = 22
        DataField = 'DATA_ABERTURA'
        DataSource = dsCadastro
        Color = clMenu
        ShowNullDate = False
        TabOrder = 1
        OnKeyPress = FormKeyPress
      end
      object medHoraAbertura: TJvDBMaskEdit
        Left = 252
        Top = 38
        Width = 65
        Height = 22
        Color = clMenu
        DataField = 'HORA_ABERTURA'
        DataSource = dsCadastro
        MaxLength = 8
        ReadOnly = True
        TabOrder = 2
        EditMask = '99:99:99;'
        OnKeyPress = FormKeyPress
      end
      object cedVlrTotalAbertura: TJvDBCalcEdit
        Left = 5
        Top = 236
        Width = 100
        Height = 22
        Color = clMenu
        DisplayFormat = ',0.00'
        ReadOnly = True
        TabOrder = 6
        DecimalPlacesAlwaysShown = False
        OnKeyPress = cedVlrTotalAberturaKeyPress
        DataField = 'VR_TOTAL_ABERTURA'
        DataSource = dsCadastro
        EmptyIsNull = False
      end
      object cedVlrTotalUltFechamento: TJvDBCalcEdit
        Left = 5
        Top = 82
        Width = 100
        Height = 22
        Color = clMenu
        DisplayFormat = ',0.00'
        ReadOnly = True
        TabOrder = 3
        DecimalPlacesAlwaysShown = False
        OnKeyPress = FormKeyPress
        DataField = 'VR_TOTAL_ULT_FECH'
        DataSource = dsCadastro
        EmptyIsNull = False
      end
      object edtNomeUsuarioResponsavel: TDBEdit
        Left = 5
        Top = 38
        Width = 135
        Height = 22
        CharCase = ecUpperCase
        Color = clMenu
        DataField = 'NOME_USUARIO'
        DataSource = dsCadastro
        ReadOnly = True
        TabOrder = 0
        OnKeyPress = FormKeyPress
      end
      object cedVlrContaCorrente: TJvDBCalcEdit
        Left = 5
        Top = 192
        Width = 100
        Height = 22
        Color = 16114127
        DisplayFormat = ',0.00'
        TabOrder = 5
        DecimalPlacesAlwaysShown = False
        OnExit = cedVlrContaCorrenteExit
        OnKeyPress = FormKeyPress
        DataField = 'VR_CONTACORRENTE'
        DataSource = dsCadastro
        EmptyIsNull = False
      end
      object dteDataCancelamento: TJvDBDateEdit
        Left = 217
        Top = 280
        Width = 100
        Height = 22
        DataField = 'DATA_CANCELAMENTO'
        DataSource = dsCadastro
        ReadOnly = True
        Color = clMenu
        ShowNullDate = False
        TabOrder = 7
        OnKeyPress = FormKeyPress
      end
      object pnlAberturaCancelada: TJvPanel
        Left = 336
        Top = 139
        Width = 305
        Height = 53
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -11
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        Transparent = True
        BevelOuter = bvNone
        Caption = 'CANCELADA'
        Color = clMenu
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -48
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentBackground = False
        ParentFont = False
        TabOrder = 10
        Visible = False
      end
      object gbDinheiro: TGroupBox
        Left = 5
        Top = 110
        Width = 216
        Height = 60
        Caption = 'Dinheiro'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        object lblVlrTotalNotas: TLabel
          Left = 5
          Top = 16
          Width = 81
          Height = 14
          Caption = 'Vlr. Tot. Notas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object lblVlrTotalMoedas: TLabel
          Left = 111
          Top = 16
          Width = 91
          Height = 14
          Caption = 'Vlr. Tot. Moedas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cedVlrTotalMoedas: TJvDBCalcEdit
          Left = 111
          Top = 32
          Width = 100
          Height = 22
          Color = 16114127
          DisplayFormat = ',0.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          DecimalPlacesAlwaysShown = False
          OnExit = cedVlrTotalMoedasExit
          OnKeyPress = FormKeyPress
          DataField = 'VR_MOEDAS'
          DataSource = dsCadastro
          EmptyIsNull = False
        end
        object cedVlrTotalNotas: TJvDBCalcEdit
          Left = 5
          Top = 32
          Width = 100
          Height = 22
          Color = 16114127
          DisplayFormat = ',0.00'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          DecimalPlacesAlwaysShown = False
          OnExit = cedVlrTotalNotasExit
          OnKeyPress = FormKeyPress
          DataField = 'VR_NOTAS'
          DataSource = dsCadastro
          EmptyIsNull = False
        end
      end
    end
    inherited pnlMenu: TPanel
      Height = 368
      ExplicitHeight = 368
      inherited btnDadosPrincipais: TJvTransparentButton
        Visible = False
      end
      inherited btnOk: TJvTransparentButton
        Top = 278
        OnClick = btnOkClick
        ExplicitTop = 123
      end
      inherited btnCancelar: TJvTransparentButton
        Top = 323
        ExplicitTop = 168
      end
    end
  end
  inherited dsCadastro: TDataSource
    DataSet = cdsAberturaCX
    Left = 726
    Top = 323
  end
  object qryAberturaCX: TFDQuery
    Connection = dmPrincipal.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM CAIXA_ABERTURA'
      ' WHERE ID_CAIXA_ABERTURA = :ID_CAIXA_ABERTURA')
    Left = 505
    Top = 323
    ParamData = <
      item
        Position = 1
        Name = 'ID_CAIXA_ABERTURA'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspAberturaCX: TDataSetProvider
    DataSet = qryAberturaCX
    Left = 582
    Top = 323
  end
  object cdsAberturaCX: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_CAIXA_ABERTURA'
        ParamType = ptInput
      end>
    ProviderName = 'dspAberturaCX'
    OnCalcFields = cdsAberturaCXCalcFields
    Left = 658
    Top = 323
    object cdsAberturaCXID_CAIXA_ABERTURA: TIntegerField
      FieldName = 'ID_CAIXA_ABERTURA'
      Required = True
    end
    object cdsAberturaCXDATA_ABERTURA: TDateField
      FieldName = 'DATA_ABERTURA'
    end
    object cdsAberturaCXHORA_ABERTURA: TTimeField
      FieldName = 'HORA_ABERTURA'
    end
    object cdsAberturaCXVR_NOTAS: TBCDField
      FieldName = 'VR_NOTAS'
      OnGetText = cdsAberturaCXVR_NOTASGetText
      Precision = 18
      Size = 2
    end
    object cdsAberturaCXVR_MOEDAS: TBCDField
      FieldName = 'VR_MOEDAS'
      OnGetText = cdsAberturaCXVR_MOEDASGetText
      Precision = 18
      Size = 2
    end
    object cdsAberturaCXVR_CONTACORRENTE: TBCDField
      FieldName = 'VR_CONTACORRENTE'
      Precision = 18
      Size = 2
    end
    object cdsAberturaCXVR_TOTAL_ABERTURA: TBCDField
      FieldName = 'VR_TOTAL_ABERTURA'
      OnGetText = cdsAberturaCXVR_TOTAL_ABERTURAGetText
      Precision = 18
      Size = 2
    end
    object cdsAberturaCXVR_TOTAL_ULT_FECH: TBCDField
      FieldName = 'VR_TOTAL_ULT_FECH'
      OnGetText = cdsAberturaCXVR_TOTAL_ULT_FECHGetText
      Precision = 18
      Size = 2
    end
    object cdsAberturaCXCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsAberturaCXFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsAberturaCXDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsAberturaCXCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsAberturaCXNOME_USUARIO: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOME_USUARIO'
      Size = 30
    end
  end
end
