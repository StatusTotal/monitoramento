{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFiltroVales.pas
  Descricao:   Tela de filtro de Vales de Funcionarios
  Author   :   Cristina
  Date:        27-mai-2016
  Last Update: 27-out-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFiltroVales;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFiltroSimplesPadrao, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExControls, JvButton, JvTransparentButton, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Mask, JvExMask, JvToolEdit, Vcl.DBCtrls,
  System.DateUtils, Datasnap.DBClient, Datasnap.Provider, frxClass, frxDBSet;

type
  TFFiltroVales = class(TFFiltroSimplesPadrao)
    lblNomeFuncionario: TLabel;
    lblPeriodoCadastro: TLabel;
    lcbNomeFuncionario: TDBLookupComboBox;
    dteIniPeriodoCadastro: TJvDateEdit;
    dteFimPeriodoCadastro: TJvDateEdit;
    Label3: TLabel;
    qryFuncionarios: TFDQuery;
    dsFuncionarios: TDataSource;
    rgSituacaoVale: TRadioGroup;
    dspGridPaiPadrao: TDataSetProvider;
    cdsGridPaiPadrao: TClientDataSet;
    cdsGridPaiPadraoID: TIntegerField;
    cdsGridPaiPadraoID_FUNCIONARIO_FK: TIntegerField;
    cdsGridPaiPadraoVR_VALE: TBCDField;
    cdsGridPaiPadraoDESCR_VALE: TStringField;
    cdsGridPaiPadraoCAD_ID_USUARIO: TIntegerField;
    cdsGridPaiPadraoDATA_QUITACAO: TDateField;
    cdsGridPaiPadraoQUIT_ID_USUARIO: TIntegerField;
    cdsGridPaiPadraoNOMEFUNCIONARIO: TStringField;
    cdsGridPaiPadraoFLG_CANCELADO: TStringField;
    cdsGridPaiPadraoDATA_CANCELAMENTO: TDateField;
    cdsGridPaiPadraoCANCEL_ID_USUARIO: TIntegerField;
    cdsGridPaiPadraoID_FECHAMENTO_SALARIO_FK: TIntegerField;
    cdsGridPaiPadraoDATA_VALE: TDateField;
    cdsGridPaiPadraoDATA_CADASTRO: TSQLTimeStampField;
    procedure FormCreate(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure lcbNomeFuncionarioKeyPress(Sender: TObject; var Key: Char);
    procedure dteIniPeriodoCadastroKeyPress(Sender: TObject; var Key: Char);
    procedure dteFimPeriodoCadastroKeyPress(Sender: TObject; var Key: Char);
    procedure rgSituacaoValeExit(Sender: TObject);
    procedure dbgGridPaiPadraoDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dbgGridPaiPadraoTitleClick(Column: TColumn);
    procedure cdsGridPaiPadraoCalcFields(DataSet: TDataSet);
    procedure cdsGridPaiPadraoVR_VALEGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure dbgGridPaiPadraoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  protected
    procedure VerificarPermissoes; override;
    procedure GravarLog; override;
    function PodeExcluir(var Msg: String): Boolean; override;
  private
    { Private declarations }

    procedure AbrirListaVales;
  public
    { Public declarations }
  end;

var
  FFiltroVales: TFFiltroVales;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais, UCadastroVale, UDMVale;

{ TFFiltroVales }

procedure TFFiltroVales.AbrirListaVales;
begin
  with qryGridPaiPadrao, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_VALE AS ID, ' +
            '       ID_FUNCIONARIO_FK, ' +
            '       DATA_VALE, ' +
            '       VR_VALE, ' +
            '       DESCR_VALE, ' +
            '       ID_FECHAMENTO_SALARIO_FK,' +
            '       DATA_CADASTRO, ' +
            '       CAD_ID_USUARIO, ' +
            '       DATA_QUITACAO, ' +
            '       QUIT_ID_USUARIO, ' +
            '       FLG_CANCELADO, ' +
            '       DATA_CANCELAMENTO, ' +
            '       CANCEL_ID_USUARIO ' +
            '  FROM VALE ' +
            ' WHERE ID_FUNCIONARIO_FK = (CASE WHEN (:ID_FUNC01 = 0) THEN ID_FUNCIONARIO_FK ELSE :ID_FUNC02 END) ' +
            '   AND DATA_VALE BETWEEN :DATA_INI AND :DATA_FIM ';

    if rgSituacaoVale.ItemIndex = 1 then
      Add(' AND DATA_QUITACAO IS NULL')
    else if rgSituacaoVale.ItemIndex = 2 then
      Add(' AND DATA_QUITACAO IS NOT NULL');

    if Trim(lcbNomeFuncionario.Text) <> '' then
    begin
      Params.ParamByName('ID_FUNC01').Value := lcbNomeFuncionario.KeyValue;
      Params.ParamByName('ID_FUNC02').Value := lcbNomeFuncionario.KeyValue;
    end
    else
    begin
      Params.ParamByName('ID_FUNC01').Value := 0;
      Params.ParamByName('ID_FUNC02').Value := 0;
    end;

    Params.ParamByName('DATA_INI').Value := FormatDateTime('YYYY-MM-DD', dteIniPeriodoCadastro.Date);
    Params.ParamByName('DATA_FIM').Value := FormatDateTime('YYYY-MM-DD', dteFimPeriodoCadastro.Date);

    Add('ORDER BY ID_FUNCIONARIO_FK ASC, DATA_VALE DESC');
  end;

  cdsGridPaiPadrao.Close;

  if Trim(lcbNomeFuncionario.Text) <> '' then
  begin
    cdsGridPaiPadrao.Params.ParamByName('ID_FUNC01').Value := lcbNomeFuncionario.KeyValue;
    cdsGridPaiPadrao.Params.ParamByName('ID_FUNC02').Value := lcbNomeFuncionario.KeyValue;
  end
  else
  begin
    cdsGridPaiPadrao.Params.ParamByName('ID_FUNC01').Value := 0;
    cdsGridPaiPadrao.Params.ParamByName('ID_FUNC02').Value := 0;
  end;

  cdsGridPaiPadrao.Params.ParamByName('DATA_INI').Value := FormatDateTime('YYYY-MM-DD', dteIniPeriodoCadastro.Date);
  cdsGridPaiPadrao.Params.ParamByName('DATA_FIM').Value := FormatDateTime('YYYY-MM-DD', dteFimPeriodoCadastro.Date);

  cdsGridPaiPadrao.Open;
end;

procedure TFFiltroVales.btnEditarClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao   := E;
  vgIdConsulta := cdsGridPaiPadrao.FieldByName('ID').AsInteger;

  Application.CreateForm(TdmVale, dmVale);
  dmGerencial.CriarForm(TFCadastroVale, FCadastroVale);
  btnFiltrar.Click;

  DefinirPosicaoGrid;
end;

procedure TFFiltroVales.btnExcluirClick(Sender: TObject);
var
  qryExclusao: TFDQuery;
  CodLanc, AnoLanc: Integer;
begin
  inherited;

  if lPodeExcluir then
  begin
    CodLanc := 0;
    AnoLanc := 0;

    qryExclusao := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    try
      if vgConSISTEMA.Connected then
        vgConSISTEMA.StartTransaction;

      with qryExclusao, SQL do
      begin
        //Lancamento (Consulta)
        Close;
        Clear;
        Text := 'SELECT COD_LANCAMENTO, ANO_LANCAMENTO ' +
                '  FROM LANCAMENTO ' +
                ' WHERE ID_ORIGEM      = :ID_ORIGEM ' +
                '   AND ID_NATUREZA_FK = :ID_NATUREZA';
        Params.ParamByName('ID_ORIGEM').Value   := cdsGridPaiPadrao.FieldByName('ID').AsInteger;
        Params.ParamByName('ID_NATUREZA').Value := 3;  //VALE
        Open;

        CodLanc := qryExclusao.FieldByName('COD_LANCAMENTO').AsInteger;
        AnoLanc := qryExclusao.FieldByName('ANO_LANCAMENTO').AsInteger;

        //Parcela Lancamento
        Close;
        Clear;

        Text := 'UPDATE LANCAMENTO_PARC ' +
                '                   SET FLG_STATUS        = :FLG_STATUS, ' +
                '                       DATA_CANCELAMENTO = :DATA_CANCELAMENTO, ' +
                '                       CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO ' +
                ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO';

        Params.ParamByName('FLG_STATUS').Value        := 'C';
        Params.ParamByName('DATA_CANCELAMENTO').Value := Now;
        Params.ParamByName('CANCEL_ID_USUARIO').Value := vgUsu_Id;
        Params.ParamByName('COD_LANCAMENTO').Value    := CodLanc;
        Params.ParamByName('ANO_LANCAMENTO').Value    := AnoLanc;
        ExecSQL;

        //Lancamento
        Close;
        Clear;

        Text := 'UPDATE LANCAMENTO ' +
                '              SET FLG_CANCELADO     = :FLG_CANCELADO, ' +
                '                  DATA_CANCELAMENTO = :DATA_CANCELAMENTO, ' +
                '                  CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO ' +
                ' WHERE COD_LANCAMENTO = :COD_LANCAMENTO ' +
                '   AND ANO_LANCAMENTO = :ANO_LANCAMENTO';

        Params.ParamByName('FLG_CANCELADO').Value     := 'S';
        Params.ParamByName('DATA_CANCELAMENTO').Value := Now;
        Params.ParamByName('CANCEL_ID_USUARIO').Value := vgUsu_Id;
        Params.ParamByName('COD_LANCAMENTO').Value    := CodLanc;
        Params.ParamByName('ANO_LANCAMENTO').Value    := AnoLanc;
        ExecSQL;

        //Vale
        Close;
        Clear;
        Text := 'UPDATE VALE ' +
                '        SET FLG_CANCELADO     = :FLG_CANCELADO, ' +
                '            DATA_CANCELAMENTO = :DATA_CANCELAMENTO, ' +
                '            CANCEL_ID_USUARIO = :CANCEL_ID_USUARIO ' +
                ' WHERE ID_VALE = :ID_VALE';
        Params.ParamByName('FLG_CANCELADO').Value     := 'S';
        Params.ParamByName('DATA_CANCELAMENTO').Value := Date;
        Params.ParamByName('CANCEL_ID_USUARIO').Value := vgUsu_Id;
        Params.ParamByName('ID_VALE').Value           := cdsGridPaiPadrao.FieldByName('ID').AsInteger;
        ExecSQL;

        dmPrincipal.RecalcularFechamentoSalarios(cdsGridPaiPadrao.FieldByName('ID_FECHAMENTO_SALARIO_FK').AsInteger,
                                                 cdsGridPaiPadrao.FieldByName('ID_FUNCIONARIO_FK').AsInteger,
                                                 True, False, False);
      end;

      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Commit;

      GravarLog;
      Application.MessageBox('Vale exclu�do com sucesso!', 'Aviso', MB_OK)
    except
      if vgConSISTEMA.InTransaction then
        vgConSISTEMA.Rollback;

      Application.MessageBox('Erro na exclus�o do Vale.', 'Erro', MB_OK + MB_ICONERROR)
    end;

    FreeAndNil(qryExclusao);

    btnFiltrar.Click;
  end;
end;

procedure TFFiltroVales.btnFiltrarClick(Sender: TObject);
begin
  AbrirListaVales;

  inherited;
end;

procedure TFFiltroVales.btnIncluirClick(Sender: TObject);
begin
  inherited;

  vgOperacao   := I;
  vgIdConsulta := 0;

  Application.CreateForm(TdmVale, dmVale);
  dmGerencial.CriarForm(TFCadastroVale, FCadastroVale);
  btnFiltrar.Click;
end;

procedure TFFiltroVales.btnLimparClick(Sender: TObject);
begin
  dteIniPeriodoCadastro.Date := StartOfTheMonth(Date);
  dteFimPeriodoCadastro.Date := EndOfTheMonth(Date);

  lcbNomeFuncionario.KeyValue := Null;
  rgSituacaoVale.ItemIndex    := 0;

  inherited;

  if dteIniPeriodoCadastro.CanFocus then
    dteIniPeriodoCadastro.SetFocus;
end;

procedure TFFiltroVales.cdsGridPaiPadraoCalcFields(DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  qryAux := dmGerencial.CriarFDQuery(nil, vgConGER);

  with qryAux, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_FUNCIONARIO, NOME_FUNCIONARIO ' +
            '  FROM FUNCIONARIO ' +
            ' WHERE ID_FUNCIONARIO = :ID_FUNCIONARIO';
    Params.ParamByName('ID_FUNCIONARIO').AsInteger := cdsGridPaiPadrao.FieldByName('ID_FUNCIONARIO_FK').AsInteger;
    Open;
  end;

  cdsGridPaiPadrao.FieldByName('NOMEFUNCIONARIO').AsString := qryAux.FieldByName('NOME_FUNCIONARIO').AsString;

  FreeAndNil(qryAux);
end;

procedure TFFiltroVales.cdsGridPaiPadraoVR_VALEGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;

  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFFiltroVales.dbgGridPaiPadraoDblClick(Sender: TObject);
begin
  inherited;

  PegarPosicaoGrid;

  vgOperacao   := C;
  vgIdConsulta := cdsGridPaiPadrao.FieldByName('ID').AsInteger;

  Application.CreateForm(TdmVale, dmVale);
  dmGerencial.CriarForm(TFCadastroVale, FCadastroVale);

  DefinirPosicaoGrid;
end;

procedure TFFiltroVales.dbgGridPaiPadraoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Grid: TDBGrid;
begin
  inherited;

  Grid := Sender as TDBGrid;

  if Grid.DataSource.DataSet.FieldByName('FLG_CANCELADO').AsString = 'S' then
  begin
    Grid.Canvas.Font.Color := clGray;
    Grid.Canvas.FillRect(Rect);
    Grid.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TFFiltroVales.dbgGridPaiPadraoTitleClick(Column: TColumn);
begin
  inherited;

  cdsGridPaiPadrao.IndexFieldNames := Column.FieldName;
end;

procedure TFFiltroVales.dteFimPeriodoCadastroKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
     if dteFimPeriodoCadastro.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteFimPeriodoCadastro.CanFocus then
        dteFimPeriodoCadastro.SetFocus;
    end
    else if dteIniPeriodoCadastro.Date > dteFimPeriodoCadastro.Date then
    begin
      Application.MessageBox('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso', MB_OK + MB_ICONWARNING);

      dteFimPeriodoCadastro.Clear;

      if dteFimPeriodoCadastro.CanFocus then
        dteFimPeriodoCadastro.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dteFimPeriodoCadastro.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Final', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteFimPeriodoCadastro.CanFocus then
        dteFimPeriodoCadastro.SetFocus;
    end
    else if dteIniPeriodoCadastro.Date > dteFimPeriodoCadastro.Date then
    begin
      Application.MessageBox('A Data Final n�o pode ser inferior � Data Inicial.', 'Aviso', MB_OK + MB_ICONWARNING);

      dteFimPeriodoCadastro.Clear;

      if dteFimPeriodoCadastro.CanFocus then
        dteFimPeriodoCadastro.SetFocus;
    end
    else
      btnFiltrar.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroVales.dteIniPeriodoCadastroKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
     if dteIniPeriodoCadastro.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteIniPeriodoCadastro.CanFocus then
        dteIniPeriodoCadastro.SetFocus;
    end
    else if dteIniPeriodoCadastro.Date > dteFimPeriodoCadastro.Date then
    begin
      Application.MessageBox('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso', MB_OK + MB_ICONWARNING);

      dteIniPeriodoCadastro.Clear;

      if dteIniPeriodoCadastro.CanFocus then
        dteIniPeriodoCadastro.SetFocus;
    end
    else
      SelectNext(ActiveControl, True, True);

     Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if dteIniPeriodoCadastro.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data Inicial', 'Aviso', MB_OK + MB_ICONWARNING);

      if dteIniPeriodoCadastro.CanFocus then
        dteIniPeriodoCadastro.SetFocus;
    end
    else if dteIniPeriodoCadastro.Date > dteFimPeriodoCadastro.Date then
    begin
      Application.MessageBox('A Data Inicial n�o pode ser superior � Data Final.', 'Aviso', MB_OK + MB_ICONWARNING);

      dteIniPeriodoCadastro.Clear;

      if dteIniPeriodoCadastro.CanFocus then
        dteIniPeriodoCadastro.SetFocus;
    end
    else
      btnFiltrar.Click;

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
    Self.Close;
end;

procedure TFFiltroVales.FormCreate(Sender: TObject);
begin
  inherited;

  qryFuncionarios.Close;
  qryFuncionarios.Open;

  dteIniPeriodoCadastro.Date := StartOfTheMonth(Date);
  dteFimPeriodoCadastro.Date := EndOfTheMonth(Date);

  AbrirListaVales;
end;

procedure TFFiltroVales.FormShow(Sender: TObject);
begin
  inherited;

  if dteIniPeriodoCadastro.CanFocus then
    dteIniPeriodoCadastro.SetFocus;
end;

procedure TFFiltroVales.GravarLog;
var
  sObservacao: String;
begin
  inherited;

  sObservacao := '';

  case vgOperacao of
    X:  //EXCLUSAO
    begin
      repeat
        sObservacao := InputBox('MOTIVO', 'Por favor, indique o motivo da exclus�o desse Vale:', '');
      until sObservacao <> '';

      BS.GravarUsuarioLog(2, '', sObservacao,
                          cdsGridPaiPadrao.FieldByName('ID').AsInteger,
                          'VALE');
    end;
    P:  //IMPRESSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente impress�o de Lista de Vales',
                          cdsGridPaiPadrao.FieldByName('ID').AsInteger,
                          'VALE');
    end;
    T:  //EXPORTACAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente exporta��o de Lista de Vales',
                          cdsGridPaiPadrao.FieldByName('ID').AsInteger,
                          'VALE');
    end;
  end;
end;

procedure TFFiltroVales.lcbNomeFuncionarioKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  CampoFiltroKeyPress(Sender, Key);
end;

function TFFiltroVales.PodeExcluir(var Msg: String): Boolean;
begin
  inherited;

  Result := True;

  if not cdsGridPaiPadrao.FieldByName('DATA_QUITACAO').IsNull then
    msg := 'N�o � poss�vel excluir o Vale informada, pois o mesmo j� est� quitado.'
  else if cdsGridPaiPadrao.FieldByName('FLG_CANCELADO').AsString = 'S' then
    msg := 'N�o � poss�vel excluir o Vale informada, pois o mesmo j� foi cancelado.';

  Result := cdsGridPaiPadrao.FieldByName('DATA_QUITACAO').IsNull and
            (cdsGridPaiPadrao.FieldByName('FLG_CANCELADO').AsString = 'N') and
            (cdsGridPaiPadrao.RecordCount > 0);
end;

procedure TFFiltroVales.rgSituacaoValeExit(Sender: TObject);
begin
  inherited;

  btnFiltrar.Click;
end;

procedure TFFiltroVales.VerificarPermissoes;
begin
  inherited;

  btnIncluir.Enabled  := vgPrm_IncVale;
  btnEditar.Enabled   := vgPrm_EdVale and (cdsGridPaiPadrao.RecordCount > 0);
  btnExcluir.Enabled  := vgPrm_ExcVale and (cdsGridPaiPadrao.RecordCount > 0);
  btnImprimir.Enabled := vgPrm_ImpVale and (cdsGridPaiPadrao.RecordCount > 0);
end;

end.
