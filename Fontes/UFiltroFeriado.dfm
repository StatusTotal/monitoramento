inherited FFiltroFeriado: TFFiltroFeriado
  Caption = 'Filtro de Feriados'
  ClientHeight = 349
  ClientWidth = 589
  ExplicitWidth = 595
  ExplicitHeight = 378
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnlPesquisa: TPanel
    Width = 583
    Height = 343
    ExplicitWidth = 583
    ExplicitHeight = 343
    inherited pnlGrid: TPanel
      Top = 52
      Width = 499
      ExplicitTop = 52
      ExplicitWidth = 499
      inherited pnlGridPai: TPanel
        Width = 499
        ExplicitWidth = 499
        inherited dbgGridPaiPadrao: TDBGrid
          Width = 499
          Columns = <
            item
              Expanded = False
              FieldName = 'ID'
              Visible = False
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'DATA_FERIADO'
              Title.Alignment = taCenter
              Title.Caption = 'Data'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DESCR_FERIADO'
              Title.Caption = 'Descri'#231#227'o'
              Width = 382
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DIA_FERIADO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'MES_FERIADO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'ANO_FERIADO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'FLG_FIXO'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'FLG_ATIVO'
              Visible = False
            end>
        end
      end
    end
    inherited pnlFiltro: TPanel
      Width = 583
      Height = 49
      ExplicitWidth = 583
      ExplicitHeight = 49
      inherited btnFiltrar: TJvTransparentButton
        Left = 408
        Top = 20
        ExplicitLeft = 408
        ExplicitTop = 20
      end
      inherited btnLimpar: TJvTransparentButton
        Left = 497
        Top = 20
        ExplicitLeft = 497
        ExplicitTop = 20
      end
      object lblDescrFeriado: TLabel
        Left = 81
        Top = 8
        Width = 39
        Height = 14
        Caption = 'Feriado'
      end
      object lblAnoFeriado: TLabel
        Left = 342
        Top = 8
        Width = 22
        Height = 14
        Caption = 'Ano'
      end
      object edtDescrFeriado: TEdit
        Left = 81
        Top = 24
        Width = 255
        Height = 22
        Color = 16114127
        TabOrder = 0
        OnKeyPress = edtDescrFeriadoKeyPress
      end
      object edtAnoFeriado: TEdit
        Left = 342
        Top = 24
        Width = 60
        Height = 22
        Color = 16114127
        NumbersOnly = True
        TabOrder = 1
        OnKeyPress = edtAnoFeriadoKeyPress
      end
    end
    inherited pnlMenu: TPanel
      Top = 52
      ExplicitTop = 52
    end
  end
  inherited dsGridPaiPadrao: TDataSource
    Left = 516
    Top = 275
  end
  inherited qryGridPaiPadrao: TFDQuery
    OnCalcFields = qryGridPaiPadraoCalcFields
    SQL.Strings = (
      'SELECT ID_FERIADO AS ID,'
      '       DESCR_FERIADO,'
      '       DIA_FERIADO,'
      '       MES_FERIADO,'
      '       ANO_FERIADO,'
      '       FLG_FIXO,'
      '       FLG_ATIVO'
      '  FROM FERIADO'
      'ORDER BY ANO_FERIADO,'
      '         MES_FERIADO,'
      '         DIA_FERIADO')
    Left = 516
    Top = 223
    object qryGridPaiPadraoID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID_FERIADO'
      Required = True
    end
    object qryGridPaiPadraoDESCR_FERIADO: TStringField
      FieldName = 'DESCR_FERIADO'
      Origin = 'DESCR_FERIADO'
      Size = 250
    end
    object qryGridPaiPadraoDIA_FERIADO: TIntegerField
      FieldName = 'DIA_FERIADO'
      Origin = 'DIA_FERIADO'
    end
    object qryGridPaiPadraoMES_FERIADO: TIntegerField
      FieldName = 'MES_FERIADO'
      Origin = 'MES_FERIADO'
    end
    object qryGridPaiPadraoANO_FERIADO: TIntegerField
      FieldName = 'ANO_FERIADO'
      Origin = 'ANO_FERIADO'
    end
    object qryGridPaiPadraoFLG_FIXO: TStringField
      FieldName = 'FLG_FIXO'
      Origin = 'FLG_FIXO'
      FixedChar = True
      Size = 1
    end
    object qryGridPaiPadraoFLG_ATIVO: TStringField
      FieldName = 'FLG_ATIVO'
      Origin = 'FLG_ATIVO'
      FixedChar = True
      Size = 1
    end
    object qryGridPaiPadraoDATA_FERIADO: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DATA_FERIADO'
      Size = 10
    end
  end
end
