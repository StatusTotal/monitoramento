{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UCadastroNatureza.pas
  Descricao:   Formulario de cadastro de Natureza Lancamentos de Despesas e Receitas
  Author   :   Cristina
  Date:        28-jul-2016
  Last Update: 23-out-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UCadastroNatureza;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UCadastroGeralPadrao, Data.DB,
  JvExControls, JvButton, JvTransparentButton, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Mask, Vcl.DBCtrls, JvExStdCtrls, JvCombobox, JvDBCombobox,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.DBClient, Datasnap.Provider,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.StrUtils, UDM;

type
  TFCadastroNatureza = class(TFCadastroGeralPadrao)
    lblDescricaoNatureza: TLabel;
    edtDescricaoNatureza: TDBEdit;
    lblTipoNatureza: TLabel;
    cbTipoNatureza: TJvDBComboBox;
    qryNatureza: TFDQuery;
    dspNatureza: TDataSetProvider;
    cdsNatureza: TClientDataSet;
    cdsNaturezaID_NATUREZA: TIntegerField;
    cdsNaturezaDESCR_NATUREZA: TStringField;
    cdsNaturezaTIPO_NATUREZA: TStringField;
    cdsNaturezaFLG_EDITAVEL: TStringField;
    gbClassificacao: TGroupBox;
    chbImpostoRenda: TDBCheckBox;
    chbAuxiliar: TDBCheckBox;
    chbPessoal: TDBCheckBox;
    cdsNaturezaFLG_IMPOSTORENDA: TStringField;
    cdsNaturezaFLG_AUXILIAR: TStringField;
    cdsNaturezaFLG_PESSOAL: TStringField;
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure cbTipoNaturezaKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure lbMensagemErroDblClick(Sender: TObject);
    procedure edtDescricaoNaturezaKeyPress(Sender: TObject; var Key: Char);
  protected
    procedure DefinirTamanhoMaxCampos; override;
    procedure DesabilitarComponentes; override;

    procedure MarcarDesmarcarAbas(Aba: TAbas); override;
    procedure GravarLog; override;

    function VerificarLayoutAto(var QtdErros: Integer): Boolean; override;
    function GravarDadosAto(var Msg: String): Boolean; override;

    function VerificarDadosGenerico(var QtdErros: Integer): Boolean; override;
    function GravarDadosGenerico(var Msg: String): Boolean; override;
  private
    { Private declarations }
  public
    { Public declarations }

    IdNovaNatureza: Integer;

    TipoNat: TTipoLancamento;
  end;

var
  FCadastroNatureza: TFCadastroNatureza;

implementation

{$R *.dfm}

uses UBibliotecaSistema, UGDM, UVariaveisGlobais;

procedure TFCadastroNatureza.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if lPodeCancelar then
    Self.Close;
end;

procedure TFCadastroNatureza.btnOkClick(Sender: TObject);
begin
  inherited;

  if vgOperacao <> C then
    GravarGenerico
  else
    Self.Close;
end;

procedure TFCadastroNatureza.cbTipoNaturezaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if cbTipoNatureza.ItemIndex = 0 then
      TipoNat := DESP
    else
      TipoNat := REC;

    btnOk.Click;
    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroNatureza.DefinirTamanhoMaxCampos;
begin
  inherited;

  edtDescricaoNatureza.MaxLength := 250;
end;

procedure TFCadastroNatureza.DesabilitarComponentes;
begin
  inherited;

  if TipoNat in [DESP, REC] then
    cbTipoNatureza.Enabled := False;

  if vgOperacao = C then
    pnlDados.Enabled := False;
end;

procedure TFCadastroNatureza.edtDescricaoNaturezaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if Key = #9 then  //TAB
  begin
    SelectNext(ActiveControl, True, True);
    Key := #0;
  end;

  if Key = #13 then  //ENTER
  begin
    if TipoNat in [DESP, REC] then
      btnOk.Click
    else
      SelectNext(ActiveControl, True, True);

    Key := #0;
  end;

  if Key = #27 then  //ESCAPE
  begin
    btnCancelar.Click;
    Key := #0;
  end;
end;

procedure TFCadastroNatureza.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  if lPodeCancelar then
  begin
    cdsNatureza.Cancel;

    vgOperacao     := VAZIO;
    vgIdConsulta   := 0;
    vgOrigemFiltro := False;
  end
  else
    Action := caNone;
end;

procedure TFCadastroNatureza.FormCreate(Sender: TObject);
begin
  inherited;

  IdNovaNatureza := 0;

  cdsNatureza.Close;
  cdsNatureza.Params.ParamByName('ID_NATUREZA').AsInteger := vgIdConsulta;
  cdsNatureza.Open;

  if vgOperacao = I then
    cdsNatureza.Append;

  if vgOperacao = E then
    cdsNatureza.Edit;
end;

procedure TFCadastroNatureza.FormShow(Sender: TObject);
begin
  inherited;

  if vgOperacao = I then
  begin
    if TipoNat = DESP then
      cdsNatureza.FieldByName('TIPO_NATUREZA').AsString  := 'D'
    else if TipoNat = REC then
      cdsNatureza.FieldByName('TIPO_NATUREZA').AsString  := 'R';

    cdsNatureza.FieldByName('FLG_EDITAVEL').AsString     := 'S';
    cdsNatureza.FieldByName('FLG_IMPOSTORENDA').AsString := 'N';
    cdsNatureza.FieldByName('FLG_AUXILIAR').AsString     := 'N';
    cdsNatureza.FieldByName('FLG_PESSOAL').AsString      := 'N';
  end;

  if edtDescricaoNatureza.CanFocus then
    edtDescricaoNatureza.SetFocus;
end;

function TFCadastroNatureza.GravarDadosAto(var Msg: String): Boolean;
begin
  //
end;

function TFCadastroNatureza.GravarDadosGenerico(var Msg: String): Boolean;
var
  sTipo: String;
  qryLancs: TFDQuery;
begin
  Result := True;
  Msg := '';
  sTipo := '';

  if vgOperacao = C then
    Exit;

  if TipoNat = NONE then
    sTipo := IfThen((cbTipoNatureza.ItemIndex = 0), 'Despesa', 'Receita')
  else
    sTipo := IfThen((TipoNat = DESP), 'Despesa', 'Receita');

  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    if cdsNatureza.State in [dsInsert, dsEdit] then
    begin
      { Grava as especificacoes da Natureza para serem usadas a partir
        dos novos Lancamentos }
      if vgOperacao = I then
        cdsNatureza.FieldByName('ID_NATUREZA').AsInteger := BS.ProximoId('ID_NATUREZA', 'NATUREZA');

      if vgOrigemCadastro then
        IdNovaNatureza := cdsNatureza.FieldByName('ID_NATUREZA').AsInteger;

      if TipoNat <> NONE then
        cdsNatureza.FieldByName('TIPO_NATUREZA').AsString := IfThen((TipoNat = DESP), 'D', 'R');

      cdsNatureza.Post;
      cdsNatureza.ApplyUpdates(0);
    end;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;

    { Estende as alteracoes de Natureza a todos os Lancamentos existentes
      no banco de dados }
    if vgOperacao = E then
    begin
      if Application.MessageBox(PChar('Deseja que essa altera��o seja refletida em todos ' +
                                      'os Lan�amentos dessa Natureza?' + #13#10 +
                                      '(Essa a��o afetar� TODOS ' +
                                      'os Lan�amentos gravados no ' +
                                      'Banco de Dados at� o presente momento.)'),
                                '',
                                MB_YESNO + MB_ICONQUESTION) = ID_YES then
      begin
        try
          cdsNatureza.Close;
          cdsNatureza.Params.ParamByName('ID_NATUREZA').AsInteger := vgIdConsulta;
          cdsNatureza.Open;

          if vgConSISTEMA.Connected then
            vgConSISTEMA.StartTransaction;

          qryLancs := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

          qryLancs.Close;
          qryLancs.SQL.Clear;
          qryLancs.SQL.Text := 'UPDATE LANCAMENTO ' +
                               '   SET FLG_IMPOSTORENDA = :FLG_IMPOSTORENDA, ' +
                               '       FLG_PESSOAL      = :FLG_PESSOAL, ' +
                               '       FLG_AUXILIAR     = :FLG_AUXILIAR ' +
                               ' WHERE ID_NATUREZA_FK = :ID_NATUREZA';
          qryLancs.Params.ParamByName('FLG_IMPOSTORENDA').Value := cdsNatureza.FieldByName('FLG_IMPOSTORENDA').AsString;
          qryLancs.Params.ParamByName('FLG_PESSOAL').Value      := cdsNatureza.FieldByName('FLG_PESSOAL').AsString;
          qryLancs.Params.ParamByName('FLG_AUXILIAR').Value     := cdsNatureza.FieldByName('FLG_AUXILIAR').AsString;
          qryLancs.Params.ParamByName('ID_NATUREZA').Value      := vgIdConsulta;
          qryLancs.ExecSQL;

          FreeAndNil(qryLancs);

          if vgConSISTEMA.InTransaction then
            vgConSISTEMA.Commit;
        except
          if vgConSISTEMA.InTransaction then
            vgConSISTEMA.Rollback;

          Result := False;
          Msg := 'Erro na grava��o da Natureza de ' + Trim(sTipo) + '.';
        end;
      end;
    end;

    Msg := 'Natureza de ' + Trim(sTipo) + ' gravada com sucesso!';
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := False;
    Msg := 'Erro na grava��o da Natureza de ' + Trim(sTipo) + '.';
  end;
end;

procedure TFCadastroNatureza.GravarLog;
var
  sObservacao: String;
  sTipo: String;
begin
  inherited;

  sObservacao := '';

  if TipoNat = NONE then
    sTipo := IfThen((cbTipoNatureza.ItemIndex = 0), 'Despesa', 'Receita')
  else
    sTipo := IfThen((TipoNat = DESP), 'Despesa', 'Receita');

  case vgOperacao of
    C:  //CONSULTA
    begin
      BS.GravarUsuarioLog(2, '', 'Somente consulta de Natureza de ' + sTipo,
                          vgIdConsulta, 'NATUREZA');
    end;
    I:  //INCLUSAO
    begin
      BS.GravarUsuarioLog(2, '', 'Somente inclus�o de Natureza de ' + sTipo,
                          vgIdConsulta, 'NATUREZA');
    end;
    E:  //EDICAO
    begin
      if vgUsu_FlgJustifEdicao = 'S' then
      begin
        repeat
          sObservacao := InputBox('MOTIVO',
                                  'Por favor, indique o motivo da edi��o dessa Natureza de ' + sTipo + ':',
                                  '');
        until sObservacao <> '';
      end
      else
        sObservacao := 'Edi��o de Dados de Natureza de ' + sTipo;

      BS.GravarUsuarioLog(2, '', sObservacao,
                          vgIdConsulta, 'NATUREZA');
    end;
  end;
end;

procedure TFCadastroNatureza.lbMensagemErroDblClick(Sender: TObject);
begin
  inherited;

  FocalizarErro(nil);
end;

procedure TFCadastroNatureza.MarcarDesmarcarAbas(Aba: TAbas);
begin
  inherited;
  //
end;

function TFCadastroNatureza.VerificarDadosGenerico(
  var QtdErros: Integer): Boolean;
begin
  Result   := True;
  QtdErros := 0;

  FinalizarComponenteMsgErro;
  InicializarComponenteMsgErro;

  if Trim(edtDescricaoNatureza.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar a DESCRI��O DA NATUREZA.';
    DadosMsgErro.Componente := edtDescricaoNatureza;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if Trim(cbTipoNatureza.Text) = '' then
  begin
    DadosMsgErro := MsgErro.Create;
    DadosMsgErro.IdMsg      := QtdErros;
    DadosMsgErro.Mensagem   := IntToStr(QtdErros + 1) + ') Falta informar o TIPO DA NATUREZA.';
    DadosMsgErro.Componente := cbTipoNatureza;
    DadosMsgErro.PageIndex  := 0;
    ListaMsgErro.Add(DadosMsgErro);

    Inc(QtdErros);
  end;

  if ListaMsgErro.Count >= 1 then
    Result := False;
end;

function TFCadastroNatureza.VerificarLayoutAto(var QtdErros: Integer): Boolean;
begin
  //
end;

end.
